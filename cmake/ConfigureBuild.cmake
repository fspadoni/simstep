#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

#######################################################################
# This file takes care of configuring Ogre to build with the settings
# given in CMake. It creates the necessary config.h file and will
# also prepare package files for pkg-config and CMake.
#######################################################################

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")

# debug information for edit and continue
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} /ZI")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /ZI")

if (SIMSTEP_BUILD_PLATFORM_APPLE_IOS)
  set(SIMSTEP_BUILD_PLATFORM_APPLE_IOS 1)
  set(SIMSTEP_STATIC TRUE)
  set(SIMSTEP_STATIC_LIB TRUE)
  set(SIMSTEP_CONFIG_ENABLE_PVRTC TRUE)
endif()

# should we build static libs?
if (SIMSTEP_STATIC)
  set(SIMSTEP_LIB_TYPE STATIC)
else ()
  set(SIMSTEP_LIB_TYPE SHARED)
endif ()

# configure threading options
set(SIMSTEP_THREAD_PROVIDER 0)
if (SIMSTEP_CONFIG_THREADS)

	if (SIMSTEP_CONFIG_THREAD_PROVIDER STREQUAL "boost")
		set(SIMSTEP_THREAD_PROVIDER 1)
		include_directories(${Boost_INCLUDE_DIRS})
		# On MSVC Boost usually tries to autolink boost libraries. However since
		# this behaviour is not available on all compilers, we need to find the libraries
		# ourselves, anyway. Disable auto-linking to avoid mess-ups.
		add_definitions(-DBOOST_ALL_NO_LIB)
        if (MINGW AND Boost_USE_STATIC_LIBS)
            # mingw needs this to link against static thread libraries
            add_definitions(-DBOOST_THREAD_USE_LIB)
        endif ()
		set(SIMSTEP_THREAD_LIBRARIES ${Boost_LIBRARIES})
	endif ()


	if (SIMSTEP_CONFIG_THREAD_PROVIDER STREQUAL "tbb")
		set(SIMSTEP_THREAD_PROVIDER 2)
		include_directories(${TBB_INCLUDE_DIRS})
		if (WIN32 AND MINGW)
			add_definitions(-D_WIN32_WINNT=0x0501)    
		endif ()

		set(SIMSTEP_THREAD_LIBRARIES ${TBB_LIBRARIES})
	endif ()
endif()

set(SIMSTEP_ASSERT_MODE 0 CACHE STRING 
	"Enable Ogre asserts and exceptions. Possible values:
	0 - Standard asserts in debug builds, nothing in release builds.
	1 - Standard asserts in debug builds, exceptions in release builds.
	2 - Exceptions in debug builds, exceptions in release builds."
)

# determine config values depending on build options
#set(SIMSTEP_SET_DOUBLE 0)
set(SIMSTEP_SET_ALLOCATOR ${SIMSTEP_CONFIG_ALLOCATOR})
set(SIMSTEP_SET_CONTAINERS_USE_ALLOCATOR 0)
set(SIMSTEP_SET_STRING_USE_ALLOCATOR 0)
set(SIMSTEP_SET_MEMTRACK_DEBUG 0)
set(SIMSTEP_SET_MEMTRACK_RELEASE 0)
set(SIMSTEP_SET_ASSERT_MODE ${SIMSTEP_ASSERT_MODE})
set(SIMSTEP_SET_THREADS ${SIMSTEP_CONFIG_THREADS})
set(SIMSTEP_SET_THREAD_PROVIDER ${SIMSTEP_THREAD_PROVIDER})
set(SIMSTEP_STATIC_LIB 0)
set(SIMSTEP_SET_USE_BOOST 0)
set(SIMSTEP_SET_PROFILING 0)

if (SIMSTEP_STATIC)
  set(SIMSTEP_STATIC_LIB 1)
endif()
if (SIMSTEP_USE_BOOST)
  set(SIMSTEP_SET_USE_BOOST 1)
endif()
if (SIMSTEP_PROFILING)
  set(SIMSTEP_SET_PROFILING 1)
endif()

if (SIMSTEP_TEST_BIG_ENDIAN)
  set(SIMSTEP_CONFIG_BIG_ENDIAN 1)
else ()
  set(SIMSTEP_CONFIG_LITTLE_ENDIAN 1)
endif ()

# generate SimStepBuildSettings.h
configure_file(${SIMSTEP_TEMPLATES_DIR}/SimStepBuildSettings.h.in ${SIMSTEP_BINARY_DIR}/include/SimStepBuildSettings.h @ONLY)
install(FILES ${SIMSTEP_BINARY_DIR}/include/SimStepBuildSettings.h DESTINATION include/)


# Create the pkg-config package files on Unix systems
if (UNIX)
  set(SIMSTEP_LIB_SUFFIX "")
  set(SIMSTEP_PLUGIN_PREFIX "")
  set(SIMSTEP_PLUGIN_EXT ".so")
  set(SIMSTEP_PAGING_ADDITIONAL_PACKAGES "")
  if (SIMSTEP_STATIC)
    set(SIMSTEP_LIB_SUFFIX "${SIMSTEP_LIB_SUFFIX}Static")
    set(SIMSTEP_PLUGIN_PREFIX "lib")
    set(SIMSTEP_PLUGIN_EXT ".a")
  endif ()
  if (SIMSTEP_BUILD_TYPE STREQUAL "debug")
    set(SIMSTEP_LIB_SUFFIX "${SIMSTEP_LIB_SUFFIX}_d")
  endif ()

  set(SIMSTEP_ADDITIONAL_LIBS "")
  set(SIMSTEP_CFLAGS "")
  set(SIMSTEP_PREFIX_PATH ${CMAKE_INSTALL_PREFIX})
  if (SIMSTEP_CONFIG_THREADS GREATER 0)
    set(SIMSTEP_CFLAGS "-pthread")
    set(SIMSTEP_ADDITIONAL_LIBS "${SIMSTEP_ADDITIONAL_LIBS} -lpthread")
  endif ()
  if (SIMSTEP_STATIC)
    if (SIMSTEP_CONFIG_THREADS)
      set(SIMSTEP_ADDITIONAL_LIBS "${SIMSTEP_ADDITIONAL_LIBS} -lboost-thread-mt")
    endif ()
    # there is no pkgconfig file for freeimage, so we need to add that lib manually
    set(SIMSTEP_ADDITIONAL_LIBS "${SIMSTEP_ADDITIONAL_LIBS} -lfreeimage")
    configure_file(${SIMSTEP_TEMPLATES_DIR}/SIMSTEPStatic.pc.in ${SIMSTEP_BINARY_DIR}/pkgconfig/SIMSTEP.pc @ONLY)
  else ()
    configure_file(${SIMSTEP_TEMPLATES_DIR}/SIMSTEP.pc.in ${SIMSTEP_BINARY_DIR}/pkgconfig/SIMSTEP.pc @ONLY)
  endif ()
  install(FILES ${SIMSTEP_BINARY_DIR}/pkgconfig/SIMSTEP.pc DESTINATION ${SIMSTEP_LIB_DIRECTORY}/pkgconfig)

  # configure additional packages

endif ()

if(SIMSTEP_CONFIG_STATIC_LINK_CRT)
#We statically link to reduce dependencies
foreach(flag_var CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO)
    if(${flag_var} MATCHES "/MD")
        string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}")
    endif(${flag_var} MATCHES "/MD")
    if(${flag_var} MATCHES "/MDd")
        string(REGEX REPLACE "/MDd" "/MTd" ${flag_var} "${${flag_var}}")
    endif(${flag_var} MATCHES "/MDd")
endforeach(flag_var)
endif(SIMSTEP_CONFIG_STATIC_LINK_CRT)

### Commented because the FindSIMSTEP script can currently fill this role better ###
# # Create the CMake package files
# if (WIN32)
#   set(SIMSTEP_CMAKE_DIR CMake)
# elseif (UNIX)
#   set(SIMSTEP_CMAKE_DIR lib/cmake)
# elseif (APPLE)
# endif ()
# configure_file(${SIMSTEP_TEMPLATES_DIR}/SIMSTEPConfig.cmake.in ${SIMSTEP_BINARY_DIR}/cmake/SIMSTEPConfig.cmake @ONLY)
# configure_file(${SIMSTEP_TEMPLATES_DIR}/SIMSTEPConfigVersion.cmake.in ${SIMSTEP_BINARY_DIR}/cmake/SIMSTEPConfigVersion.cmake @ONLY)
# install(FILES
#   ${SIMSTEP_BINARY_DIR}/cmake/SIMSTEPConfig.cmake
#   ${SIMSTEP_BINARY_DIR}/cmake/SIMSTEPConfigVersion.cmake
#   DESTINATION ${SIMSTEP_CMAKE_DIR}
# )
#
