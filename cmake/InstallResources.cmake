#-------------------------------------------------------------------
# This file is part of the CMake build system for SIMSTEP
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

##################################################################
# Generate and install the config files needed for the samples
##################################################################

if (WIN32)
  set(SIMSTEP_MEDIA_PATH "Media")
  set(SIMSTEP_MEDIA_DIR_REL "../../${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_MEDIA_DIR_DBG "../../${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_TEST_MEDIA_DIR_REL "../../Tests/${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_TEST_MEDIA_DIR_DBG "../../Tests/${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_PLUGIN_DIR_REL ".")
  set(SIMSTEP_PLUGIN_DIR_DBG ".")
  set(SIMSTEP_SAMPLES_DIR_REL ".")
  set(SIMSTEP_SAMPLES_DIR_DBG ".")
  set(SIMSTEP_CFG_INSTALL_PATH "bin")
elseif (APPLE)
  set(SIMSTEP_MEDIA_PATH "Media")
  if(SIMSTEP_BUILD_PLATFORM_APPLE_IOS)
    set(SIMSTEP_MEDIA_DIR_REL "${SIMSTEP_MEDIA_PATH}")
    set(SIMSTEP_MEDIA_DIR_DBG "${SIMSTEP_MEDIA_PATH}")
    set(SIMSTEP_TEST_MEDIA_DIR_REL "../../Tests/${SIMSTEP_MEDIA_PATH}")
    set(SIMSTEP_TEST_MEDIA_DIR_DBG "../../Tests/${SIMSTEP_MEDIA_PATH}")
  else()
    if(SIMSTEP_INSTALL_SAMPLES_SOURCE)
      set(SIMSTEP_MEDIA_DIR_REL "../../../${SIMSTEP_MEDIA_PATH}")
      set(SIMSTEP_MEDIA_DIR_DBG "../../../${SIMSTEP_MEDIA_PATH}")
    else()
      set(SIMSTEP_MEDIA_DIR_REL "../../../../Samples/${SIMSTEP_MEDIA_PATH}")
      set(SIMSTEP_MEDIA_DIR_DBG "../../../../Samples/${SIMSTEP_MEDIA_PATH}")
      set(SIMSTEP_TEST_MEDIA_DIR_REL "../../../../Tests/${SIMSTEP_MEDIA_PATH}")
      set(SIMSTEP_TEST_MEDIA_DIR_DBG "../../../../Tests/${SIMSTEP_MEDIA_PATH}")
    endif()
  endif()
  set(SIMSTEP_PLUGIN_DIR_REL "")
  set(SIMSTEP_PLUGIN_DIR_DBG "")
  set(SIMSTEP_SAMPLES_DIR_REL "")
  set(SIMSTEP_SAMPLES_DIR_DBG "")
  set(SIMSTEP_CFG_INSTALL_PATH "bin")
elseif (UNIX)
  set(SIMSTEP_MEDIA_PATH "share/SIMSTEP/Media")
  set(SIMSTEP_MEDIA_DIR_REL "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_MEDIA_DIR_DBG "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_MEDIA_PATH}")
  set(SIMSTEP_TEST_MEDIA_DIR_REL "${CMAKE_INSTALL_PREFIX}/Tests/Media")
  set(SIMSTEP_TEST_MEDIA_DIR_DBG "${CMAKE_INSTALL_PREFIX}/Tests/Media")
  set(SIMSTEP_PLUGIN_DIR_REL "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_LIB_DIRECTORY}/SIMSTEP")
  set(SIMSTEP_PLUGIN_DIR_DBG "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_LIB_DIRECTORY}/SIMSTEP")
  set(SIMSTEP_SAMPLES_DIR_REL "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_LIB_DIRECTORY}/SIMSTEP/Samples")
  set(SIMSTEP_SAMPLES_DIR_DBG "${CMAKE_INSTALL_PREFIX}/${SIMSTEP_LIB_DIRECTORY}/SIMSTEP/Samples")
  set(SIMSTEP_CFG_INSTALL_PATH "share/SIMSTEP")
endif ()

## configure plugins.cfg
# if (NOT SIMSTEP_BUILD_RENDERSYSTEM_D3D9)
#   set(SIMSTEP_COMMENT_RENDERSYSTEM_D3D9 "#")
# endif ()
# if (NOT SIMSTEP_BUILD_RENDERSYSTEM_D3D11)
#   set(SIMSTEP_COMMENT_RENDERSYSTEM_D3D11 "#")
# endif ()




# CREATE CONFIG FILES - INSTALL VERSIONS
# # create resources.cfg
# configure_file(${SIMSTEP_TEMPLATES_DIR}/resources_d.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/debug/resources_d.cfg)
# configure_file(${SIMSTEP_TEMPLATES_DIR}/resources.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/release/resources.cfg)
# # create plugins.cfg
# configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins_d.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/debug/plugins_d.cfg)
# configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/release/plugins.cfg)
# # create samples.cfg
# configure_file(${SIMSTEP_TEMPLATES_DIR}/samples_d.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/debug/samples_d.cfg)
# configure_file(${SIMSTEP_TEMPLATES_DIR}/samples.cfg.in ${SIMSTEP_BINARY_DIR}/inst/bin/release/samples.cfg)


# # install resource files
# if (SIMSTEP_INSTALL_SAMPLES OR SIMSTEP_INSTALL_SAMPLES_SOURCE)
#   install(FILES 
#     ${SIMSTEP_BINARY_DIR}/inst/bin/debug/resources_d.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/debug/plugins_d.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/debug/samples_d.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/debug/tests_d.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/debug/quakemap_d.cfg
#     DESTINATION "${SIMSTEP_CFG_INSTALL_PATH}${SIMSTEP_DEBUG_PATH}" CONFIGURATIONS Debug
#   )
#   install(FILES 
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/resources.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/plugins.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/samples.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/tests.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/quakemap.cfg
#     DESTINATION "${SIMSTEP_CFG_INSTALL_PATH}${SIMSTEP_RELEASE_PATH}" CONFIGURATIONS Release None ""
#   )
#   install(FILES 
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/resources.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/plugins.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/samples.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/tests.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/quakemap.cfg
# 	DESTINATION "${SIMSTEP_CFG_INSTALL_PATH}${SIMSTEP_RELWDBG_PATH}" CONFIGURATIONS RelWithDebInfo
#   )
#   install(FILES 
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/resources.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/plugins.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/samples.cfg
# 	${SIMSTEP_BINARY_DIR}/inst/bin/release/tests.cfg
#     ${SIMSTEP_BINARY_DIR}/inst/bin/release/quakemap.cfg
# 	DESTINATION "${SIMSTEP_CFG_INSTALL_PATH}${SIMSTEP_MINSIZE_PATH}" CONFIGURATIONS MinSizeRel
#   )

#   # Need a special case here for the iOS SDK, configuration is not being matched, could be a CMake bug.
#   if (SIMSTEP_BUILD_PLATFORM_APPLE_IOS)
#     install(FILES 
#       ${SIMSTEP_BINARY_DIR}/inst/bin/release/resources.cfg
#       ${SIMSTEP_BINARY_DIR}/inst/bin/release/plugins.cfg
#       ${SIMSTEP_BINARY_DIR}/inst/bin/release/samples.cfg
#       ${SIMSTEP_BINARY_DIR}/inst/bin/release/tests.cfg
#       ${SIMSTEP_BINARY_DIR}/inst/bin/release/quakemap.cfg
#       DESTINATION "${SIMSTEP_CFG_INSTALL_PATH}${SIMSTEP_RELEASE_PATH}"
#     )
#   endif()

# endif (SIMSTEP_INSTALL_SAMPLES OR SIMSTEP_INSTALL_SAMPLES_SOURCE)


# # CREATE CONFIG FILES - BUILD DIR VERSIONS
# if (NOT SIMSTEP_BUILD_PLATFORM_APPLE_IOS)
#   set(SIMSTEP_MEDIA_DIR_REL "${SIMSTEP_SOURCE_DIR}/Samples/Media")
#   set(SIMSTEP_MEDIA_DIR_DBG "${SIMSTEP_SOURCE_DIR}/Samples/Media")
#   set(SIMSTEP_TEST_MEDIA_DIR_REL "${SIMSTEP_SOURCE_DIR}/Tests/Media")
#   set(SIMSTEP_TEST_MEDIA_DIR_DBG "${SIMSTEP_SOURCE_DIR}/Tests/Media")
# else ()
#   # iOS needs to use relative paths in the config files
#   set(SIMSTEP_MEDIA_DIR_REL "${SIMSTEP_MEDIA_PATH}")
#   set(SIMSTEP_MEDIA_DIR_DBG "${SIMSTEP_MEDIA_PATH}")
#   set(SIMSTEP_TEST_MEDIA_DIR_REL "${SIMSTEP_MEDIA_PATH}")
#   set(SIMSTEP_TEST_MEDIA_DIR_DBG "${SIMSTEP_MEDIA_PATH}")
# endif ()

if (WIN32)
  set(SIMSTEP_PLUGIN_DIR_REL ".")
  set(SIMSTEP_PLUGIN_DIR_DBG ".")
  set(SIMSTEP_SAMPLES_DIR_REL ".")
  set(SIMSTEP_SAMPLES_DIR_DBG ".")
elseif (APPLE)
  # not used on OS X, uses Resources
  set(SIMSTEP_PLUGIN_DIR_REL "")
  set(SIMSTEP_PLUGIN_DIR_DBG "")
  set(SIMSTEP_SAMPLES_DIR_REL "")
  set(SIMSTEP_SAMPLES_DIR_DBG "")
elseif (UNIX)
  set(SIMSTEP_PLUGIN_DIR_REL "${SIMSTEP_BINARY_DIR}/lib")
  set(SIMSTEP_PLUGIN_DIR_DBG "${SIMSTEP_BINARY_DIR}/lib")
  set(SIMSTEP_SAMPLES_DIR_REL "${SIMSTEP_BINARY_DIR}/lib")
  set(SIMSTEP_SAMPLES_DIR_DBG "${SIMSTEP_BINARY_DIR}/lib")
endif ()

# if (MSVC AND NOT NMAKE)
#   # create resources.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/resources_d.cfg.in ${SIMSTEP_BINARY_DIR}/bin/debug/resources_d.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/resources.cfg.in ${SIMSTEP_BINARY_DIR}/bin/release/resources.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/resources.cfg.in ${SIMSTEP_BINARY_DIR}/bin/relwithdebinfo/resources.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/resources.cfg.in ${SIMSTEP_BINARY_DIR}/bin/minsizerel/resources.cfg)
#   # create plugins.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins_d.cfg.in ${SIMSTEP_BINARY_DIR}/bin/debug/plugins_d.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins.cfg.in ${SIMSTEP_BINARY_DIR}/bin/release/plugins.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins.cfg.in ${SIMSTEP_BINARY_DIR}/bin/relwithdebinfo/plugins.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins.cfg.in ${SIMSTEP_BINARY_DIR}/bin/minsizerel/plugins.cfg)
#   # create quakemap.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/quakemap_d.cfg.in ${SIMSTEP_BINARY_DIR}/bin/debug/quakemap_d.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/quakemap.cfg.in ${SIMSTEP_BINARY_DIR}/bin/release/quakemap.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/quakemap.cfg.in ${SIMSTEP_BINARY_DIR}/bin/relwithdebinfo/quakemap.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/quakemap.cfg.in ${SIMSTEP_BINARY_DIR}/bin/minsizerel/quakemap.cfg)
#   # create samples.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/samples_d.cfg.in ${SIMSTEP_BINARY_DIR}/bin/debug/samples_d.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/samples.cfg.in ${SIMSTEP_BINARY_DIR}/bin/release/samples.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/samples.cfg.in ${SIMSTEP_BINARY_DIR}/bin/relwithdebinfo/samples.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/samples.cfg.in ${SIMSTEP_BINARY_DIR}/bin/minsizerel/samples.cfg)
#   # create tests.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/tests_d.cfg.in ${SIMSTEP_BINARY_DIR}/bin/debug/tests_d.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/tests.cfg.in ${SIMSTEP_BINARY_DIR}/bin/release/tests.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/tests.cfg.in ${SIMSTEP_BINARY_DIR}/bin/relwithdebinfo/tests.cfg)
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/tests.cfg.in ${SIMSTEP_BINARY_DIR}/bin/minsizerel/tests.cfg)
# else() # other OS only need one cfg file
#   string(TOLOWER "${CMAKE_BUILD_TYPE}" SIMSTEP_BUILD_TYPE)
#   if (SIMSTEP_BUILD_TYPE STREQUAL "debug" AND NOT APPLE)
#     set(SIMSTEP_CFG_SUFFIX "_d")
#   endif ()
#   # create resources.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/resources${SIMSTEP_CFG_SUFFIX}.cfg.in ${SIMSTEP_BINARY_DIR}/bin/resources${SIMSTEP_CFG_SUFFIX}.cfg)
#   # create plugins.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/plugins${SIMSTEP_CFG_SUFFIX}.cfg.in ${SIMSTEP_BINARY_DIR}/bin/plugins${SIMSTEP_CFG_SUFFIX}.cfg)
#   # create quakemap.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/quakemap${SIMSTEP_CFG_SUFFIX}.cfg.in ${SIMSTEP_BINARY_DIR}/bin/quakemap${SIMSTEP_CFG_SUFFIX}.cfg)
#   # create samples.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/samples${SIMSTEP_CFG_SUFFIX}.cfg.in ${SIMSTEP_BINARY_DIR}/bin/samples${SIMSTEP_CFG_SUFFIX}.cfg)
#   # create tests.cfg
#   configure_file(${SIMSTEP_TEMPLATES_DIR}/tests${SIMSTEP_CFG_SUFFIX}.cfg.in ${SIMSTEP_BINARY_DIR}/bin/tests${SIMSTEP_CFG_SUFFIX}.cfg)
# endif ()

