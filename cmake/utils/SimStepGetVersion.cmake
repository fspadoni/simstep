#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

include(PreprocessorUtils)

macro(simstep_get_version HEADER)
  file(READ ${HEADER} TEMP_VAR_CONTENTS)
  get_preprocessor_entry(TEMP_VAR_CONTENTS SIMSTEP_VERSION_MAJOR SIMSTEP_VERSION_MAJOR)
  get_preprocessor_entry(TEMP_VAR_CONTENTS SIMSTEP_VERSION_MINOR SIMSTEP_VERSION_MINOR)
  get_preprocessor_entry(TEMP_VAR_CONTENTS SIMSTEP_VERSION_PATCH SIMSTEP_VERSION_PATCH)
  get_preprocessor_entry(TEMP_VAR_CONTENTS SIMSTEP_VERSION_NAME SIMSTEP_VERSION_NAME)
  get_preprocessor_entry(TEMP_VAR_CONTENTS SIMSTEP_VERSION_SUFFIX SIMSTEP_VERSION_SUFFIX)
  set(SIMSTEP_VERSION "${SIMSTEP_VERSION_MAJOR}.${SIMSTEP_VERSION_MINOR}.${SIMSTEP_VERSION_PATCH}${SIMSTEP_VERSION_SUFFIX}")
  set(SIMSTEP_SOVERSION "${SIMSTEP_VERSION_MAJOR}.${SIMSTEP_VERSION_MINOR}.${OSIMSTEP_VERSION_PATCH}")
  set(SIMSTEP_VERSION_DASH_SEPARATED "${SIMSTEP_VERSION_MAJOR}-${SIMSTEP_VERSION_MINOR}-${SIMSTEP_VERSION_PATCH}${SIMSTEP_VERSION_SUFFIX}")

endmacro()
