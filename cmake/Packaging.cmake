#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

set(CPACK_PACKAGE_VERSION ${SIMSTEP_VERSION})
set(CPACK_PACKAGE_VERSION_MAJOR ${SIMSTEP_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${SIMSTEP_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${SIMSTEP_VERSION_PATCH})

set(CPACK_INSTALL_CMAKE_PROJECTS "${SIMSTEP_BINARY_DIR}" "SIMSTEP" "ALL" "/")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Open Source 3D Graphics Engine")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "SIMSTEP")
set(CPACK_PACKAGE_NAME "SIMSTEP")
# set(CPACK_PACKAGE_VENDOR "Torus Knot Software")

# CPack won't allow file without recognized extension to be used as
# license file.
# configure_file("${SIMSTEP_SOURCE_DIR}/COPYING" "${SIMSTEP_BINARY_DIR}/COPYING.txt" COPYONLY)
# set(CPACK_RESOURCE_FILE_LICENSE "${SIMSTEP_BINARY_DIR}/COPYING.txt")

#set(CPACK_PACKAGE_ICON "${SIMSTEP_SOURCE_DIR}\\\\ogrelogo.gif")

set(CPACK_PACKAGE_CONTACT "SIMSTEP Team")

set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "WriteRegStr \\\${WriteEnvStr_RegKey} \\\"SIMSTEP_HOME\\\" $INSTDIR")

include(CPack)
