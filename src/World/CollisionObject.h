#ifndef SimStep_CollisionObject_h__
#define SimStep_CollisionObject_h__

#include "BulletCollision/CollisionDispatch/btCollisionObject.h"

#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

#include "Core/PhysicsClasses.h"

#include "Math/Math.h"


namespace SimStep
{ 



	class _SimStepExport CollisionObjectParams
	{
	public:
		String name;
		Vector3 mPosition;
		Matrix3 mOrientation;
		CollisionShape* mCollisionShape;
		float mCollisionMargin;
		short int collisionFilterGroup;
		short int collisionFilterMask;

		CollisionObjectParams();
		virtual ~CollisionObjectParams(){}
	};



	class _SimStepExport CollisionObject : public btCollisionObject
	{

	public:
		
		CollisionObject() : btCollisionObject() {}

		CollisionObject( const CollisionObjectParams& params);
		
		virtual ~CollisionObject();


		inline const String& getName() {return mName;}
	
	
	protected:
		String mName;

	};

	
}; // namespace SimStep

#endif // SimStep_CollisionObject_h__