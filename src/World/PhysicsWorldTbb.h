#ifndef PHYSICS_WORLD_TBB_H
#define PHYSICS_WORLD_TBB_H


#include "PhysicsWorld.h"



#include "btBulletCollisionCommon.h"
#include "Collision/btSoftBodyRigidBodyCollisionConfiguration.h"

//#include <tbb/task_scheduler_init.h>
//#include "tbb/task.h"

//#include "Utilities/CpuStatistics.h"


// forward decl
namespace tbb {
class task_scheduler_init;
class task;
}

namespace SimStep {

	// forward decl

	class RigidBodyBase;

	class CollisionSoftBody;


	class PhysicsWorldTbb : public World
	{

	public:

		PhysicsWorldTbb(
			btCollisionDispatcher* Dispatcher, 
			btBroadphaseInterface* PairCache, 
			btCollisionConfiguration* CollisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration(),
			tbb::task_scheduler_init* task_scheduler = NULL, 
			unsigned int RequestedNumberOfThreads = 0 
			);
		
		virtual ~PhysicsWorldTbb();


		float SimulateTbb( const float& TimeStep );
		
		inline unsigned int GetNbThread() const;

		void Terminate();

	protected:

		tbb::task_scheduler_init* m_pTbbScheduler;
		
		tbb::task* m_pRootTask;

		bool TbbSchedulerAllocated;
		unsigned int m_uNumberOfThreads;

		float m_SimulationTime;

		//CpuStatistics m_CpuStatistics;

		//unsigned int m_numCounters;
		//unsigned int m_CPUCount;
		//double* m_CPUPercentCounters;
		//std::vector<void *>	m_vecProcessorCounters;

		//PDH_STATUS InitializeCounters();
		//void DeAllocateCounters();

		//void UpdateCounters(float& dTime);
	
	public:
		inline const float& GetNbCPU() const;
		inline const float& GetNbCpuCounter() const;

		inline double& GetCpuPerformance(const unsigned int& nb_cpu) const;
		//tbb::task_list list;
	};


	inline unsigned int PhysicsWorldTbb::GetNbThread() const
	{
		return m_uNumberOfThreads;
	}

	//inline const float& PhysicsWorldTbb::GetNbCPU() const
	//{
	//	return m_CpuStatistics.m_CPUCount;
	//}

	//inline const float& PhysicsWorldTbb::GetNbCpuCounter() const
	//{
	//	return m_CpuStatistics.m_numCounters;
	//}

	//inline double& PhysicsWorldTbb::GetCpuPerformance(const unsigned int& nb_cpu) const
	//{
	//	assert(nb_cpu<m_CpuStatistics.m_numCounters);
	//	return m_CpuStatistics.m_CPUPercentCounters[nb_cpu];
	//}

};

#endif