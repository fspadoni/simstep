#include "World/PhysicsWorldTbbTask.h"

#include "Core/SoftBody.h"



//#ifndef _DEBUG
//#define THREAD_PROFILER 1
//#endif

#ifdef THREAD_PROFILER
#include "libittnotify.h"
#endif


namespace SimStep {
	


#ifdef THREAD_PROFILER
	//static __itt_event s_ThreadProfilerPhysicsEvent;
	static __itt_event s_ThreadProfilerAssembleMatrixEvent;
	static __itt_event s_ThreadProfilerSolveSoftBodyEvent;
	static __itt_event s_ThreadProfilerUpdateDynamicTreeEvent;
#endif




	AssembleStiffnessMatrixTask::AssembleStiffnessMatrixTask(SoftBody* sb)
		: m_softbody(sb)
	{
#ifdef THREAD_PROFILER
		s_ThreadProfilerAssembleMatrixEvent = __itt_event_createA( "AssembleMatrix", 14 );
#endif
	}

	tbb::task* AssembleStiffnessMatrixTask::execute()
	{

#ifdef THREAD_PROFILER
		__itt_event_start( s_ThreadProfilerAssembleMatrixEvent );
#endif

		m_softbody->assembleStiffnessMatrix();

#ifdef THREAD_PROFILER
		__itt_event_end( s_ThreadProfilerAssembleMatrixEvent );
#endif

		return NULL;
	}



	SoftBodySolverTask::SoftBodySolverTask(SoftBody* sb, const float& time_step)
		: m_softbody(sb), m_time_step( time_step )
	{
#ifdef THREAD_PROFILER
		s_ThreadProfilerSolveSoftBodyEvent = __itt_event_createA( "SolveSoftBody", 13 );
#endif
	}

	tbb::task* SoftBodySolverTask::execute()
	{

#ifdef THREAD_PROFILER
		__itt_event_start( s_ThreadProfilerSolveSoftBodyEvent );
#endif

		m_softbody->solve( m_time_step );

#ifdef THREAD_PROFILER
		__itt_event_end( s_ThreadProfilerSolveSoftBodyEvent );
#endif

		return NULL;
	}



	DynamicTreeUpdateTask::DynamicTreeUpdateTask(SoftBody* sb)
		: m_softbody(sb)
	{
#ifdef THREAD_PROFILER
		s_ThreadProfilerUpdateDynamicTreeEvent = __itt_event_createA( "UpdateDynamicTree", 17 );
#endif
	}

	tbb::task* DynamicTreeUpdateTask::execute()
	{

#ifdef THREAD_PROFILER
		__itt_event_start( s_ThreadProfilerUpdateDynamicTreeEvent );
#endif

		m_softbody->updateCollisionShape();

		///* Nodes				*/ 
		//SimdVector3 velocity;
		//for( int i=0, ni=m_softbody->GetNbCollisionNodes();i<ni;++i)
		//{
		//	SoftBodyBase::CollisionNode&	n= m_softbody->CollisionNodes[i];
		//	SimdVector3& vel = m_softbody->GetVelocities()[i];
		//	//fem_soft_body->NodeDynamicTree.update(	n.leaf,
		//	//	btDbvtVolume::FromCR( btVector3(n.node->getX(),n.node->getY(),n.node->getZ()), fem_soft_body->m_collisionShape->getMargin() ),
		//	//	btVector3(vel.getX(), vel.getY(), vel.getZ()) * 1.0f/* margine */,
		//	//	fem_soft_body->m_collisionShape->getMargin()/* margine */ );

		//	SimdVector3& position = m_softbody->GetNodes()[i];
		//	//n.IdNode = i;

		//	//softbody->NodeDynamicTree.update(	n.leaf,
		//	//	btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->m_collisionShape->getMargin() ),
		//	//	btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
		//	//	softbody->m_collisionShape->getMargin()/* margine */ );

		//	m_softbody->getNodeDynamicTree().update(	n.leaf,
		//		btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()),
		//		m_softbody->getCollisionShape()->getMargin() ),
		//		btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
		//		m_softbody->getCollisionShape()->getMargin()/* margine */ );
		//}

#ifdef THREAD_PROFILER
		__itt_event_end( s_ThreadProfilerUpdateDynamicTreeEvent );
#endif

		return NULL;
	}
	

}; // namespace SimStep