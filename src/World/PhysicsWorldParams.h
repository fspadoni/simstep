#ifndef SimStep_PhysicsWorldParams_h__
#define SimStep_PhysicsWorldParams_h__


#include "Core/CollisionWorld.h"
#include "Core/DynamicsWorld.h"

#include "Utilities/Definitions.h"
//#include "Common.h"

#include <Math/Math.h>

namespace SimStep
{

	enum SimulationType
	{
		//
		simType_invalid = -1,
		//execution
		serial = 0x0,
		parallel_deterministic = 0x1,
		parallel_full		= 0x3,

		//rigid body solver type
		fast_iterative_solver = 0x8,
		slow_direct_solver = 0x10
	};


	class _SimStepExport PhysicsWorldParam : public CollisionWorldParams
		, public DynamicsWorldParams
	{
	public:

		String mName;

		bool mInitializeScheduler;

		int simType;

		PhysicsWorldParam()
			: CollisionWorldParams()
			, DynamicsWorldParams()
			, mName(StringUtil::BLANK)
			, mInitializeScheduler(false)
			, simType( parallel_deterministic | slow_direct_solver )
		{

		}
		
		~PhysicsWorldParam()
		{

		}
	};



	align16_struct WorldInfo
	{

		SimdVector3 Gravity;
		float TimeStep;		

		// 
		float MaxAngularVelocity;
	};
	


	
}; // namespace SimStep



#endif // SimStep_PhysicsWorldParams_h__
