#ifndef PHYSICW_WORLD_TBB_TASK_H
#define PHYSICW_WORLD_TBB_TASK_H

#include "tbb/task.h"

namespace SimStep {

	// forward decl
	class SoftBody;


	class AssembleStiffnessMatrixTask : public tbb::task
	{

		SoftBody* m_softbody;

	public:

		AssembleStiffnessMatrixTask( SoftBody* sb);

		tbb::task* execute();
	};


	class SoftBodySolverTask : public tbb::task
	{

		SoftBody* m_softbody;

		float m_time_step;

	public:

		SoftBodySolverTask( SoftBody* sb, const float& time_step);

		tbb::task* execute();
	};


	class DynamicTreeUpdateTask : public tbb::task
	{

		SoftBody* m_softbody;

	public:

		DynamicTreeUpdateTask( SoftBody* sb );

		tbb::task* execute();
	};
	

}; // namespace SimStep



#endif