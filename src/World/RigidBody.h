#ifndef PHYSICS_RIGID_BODY
#define PHYSICS_RIGID_BODY

//#include "../DiesPhysicsModule.h"

// collision 
#include "btBulletCollisionCommon.h"

// ode test
#include "ode/ode.h"


#include "Math/Math.h"

#include "Utilities/Common.h"

#include "World/CollisionObject.h"

namespace SimStep
{
	
	// forward decl
	class RigidBody;
	// forward decl
	struct PhysicsWorldInfo;

	// let the user know the body moved
	typedef void (*MoveListenerCallback)(RigidBody*); 



	struct RigidBodyBaseParams
	{
		Vector3 m_Position;
		Matrix3 m_Orientation;
		Matrix3 m_LocalInertia;
		float m_Mass;
		btCollisionShape* mCollisionShape;
	};

	class _SimStepExport RigidBodyParams : public CollisionObjectParams
	{
	public:
		float mass;
		Matrix3 inertia;
		float linearDamping;
		float angularDamping;
		float linearDampingThreshold;
		float angularDampingThreshold;
		float maxAngularSpeed;

		RigidBodyParams();
		virtual ~RigidBodyParams() {}
	};

	//struct RigidBodyParams : public CollisionObjectParams, public DynamicsBodyParams
	//{
	//	//CollisionObjectParams* mCollObjectParams;
	//	//DynamicsBodyParams*	mDynamicsBodyParams;
	//	RigidBodyParams();
	//};

	class _SimStepExport RigidBody : public CollisionObject , public ::dBody
	{

		void syncronizeDynamics2Collision(void);

		void destroyBody(void);

	public:
		RigidBody(const RigidBodyParams& params );
		~RigidBody();

		///to keep collision detection and dynamics separate we don't store a rigidbody pointer
		///but a rigidbody is derived from btCollisionObject, so we can safely perform an upcast
		static const RigidBody*	upcast(const btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (const RigidBody*)colObj;
			return 0;
		}
		static RigidBody*	upcast(btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (RigidBody*)colObj;
			return 0;
		}

		// copy dBody posr (position and orientation) 
		// to CollisionObject m_worldTransform
		void syncronizeCollision2Dynamics(void);


		virtual CollisionObject* getCollisionObject(void) const;
		virtual dBody* getdBody(void) const;


		void setMoveListenerCallback(MoveListenerCallback moveCallback);

		void setPosition(const Vector3& position);

		Vector3 getPosition(void);
		const Vector3 getPosition(void) const;

		void setRotation(const Matrix3& rotation);
		void setRotation(const Quat& qrot );

		Matrix3 getRotation(void) const;
		Quat getRotationQuat(void) const;

		void setLinearVel(const Vector3& linVel );
		void setAngularVel(const Vector3& angVel);

		Vector3 getLinearVel(void) const;
		Vector3 getAngularVel(void) const;

		void setMass( const float& mass);
		void setInertiaTensor( const Matrix3& inertiaTensor);
		void setInertiaTensor( const Vector3& axis, const Matrix3& inertiaTensor );

		void addForce( const Vector3& force );
		void addTorque( const Vector3& torque );

		void addRelForce( const Vector3& force );
		void addRelTorque( const Vector3& torque );


		void addForceAtPos( const Vector3& force, const Vector3& pos );
		void addForceAtRelPos( const Vector3& force, const Vector3& pos);

		void addRelForceAtPos( const Vector3& force, const Vector3& pos);
		void addRelForceAtRelPos( const Vector3& force, const Vector3& pos);


		Vector3 getForce() const;
		Vector3 getTorque() const;

		void setForce(const Vector3& force);
		void setTorque(const Vector3& torque);

		Vector3 getRelPointPos(const Vector3& pos ) const;
		Vector3 getRelPointVel(const Vector3& pos ) const;

		Vector3 getPointVel(const Vector3& pos ) const;
		Vector3 getPosRelPoint(const Vector3& pos ) const;

		Vector3 vectorToWorld(const Vector3& pos ) const;
		Vector3 vectorFromWorld(const Vector3& pos ) const;

		///to keep collision detection and dynamics separate we don't store a rigidbody pointer
		///but a rigidbody is derived from btCollisionObject, so we can safely perform an upcast
		static const dBodyID	getBodyID(const btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (static_cast<const RigidBody*>(colObj))->dBody::id();
			return 0;
		}
		static dBodyID	getBodyID(btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (static_cast<RigidBody*>(colObj) )->dBody::id();
			return 0;
		}
		/////to keep collision detection and dynamics separate we don't store a rigidbody pointer
		/////but a rigidbody is derived from btCollisionObject, so we can safely perform an upcast
		//static const dBody*	upcast(const btCollisionObject* colObj)
		//{
		//	if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
		//		return static_cast<const RigidBody*>(colObj);
		//	return 0;
		//}
		//static dBody*	upcast(btCollisionObject* colObj)
		//{
		//	if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
		//		return static_cast<RigidBody*>(colObj);
		//	return 0;
		//}

		// intentionally defined private, don't use these
	private:
		// intentionally undefined, don't use these
		RigidBody() {}
		RigidBody(const RigidBody& rbody ) {};
		const RigidBody& operator= (const RigidBody& rb ) {return *this;}

		static void MoveCallback( dBodyID b );

		//static void (*MoveCallback)(dBodyID b);

		MoveListenerCallback m_MoveListenerCallback;

		friend class PhysicsWorld;
	};


	class _SimStepExport RigidBodyBase : public btCollisionObject , public ::dBody
	{
	public:

		Vector3 m_Position;
		Matrix3 m_Orientation;
		//SimdVector3 m_Gravity;
		Vector3 m_linearVelocity;
		Vector3 m_angularVelocity;
		Vector3 m_totalForce;
		Vector3 m_totalTorque;

		Matrix3	m_invInertiaTensor;
		Matrix3	m_InertiaTensor;


		float m_Mass;
		float m_invMass;

		float m_linearDamping;
		float m_angularDamping;

		float m_linearSleepingThreshold;
		float m_angularSleepingThreshold;

		PhysicsWorldInfo*	m_WorldInfo;

		void SetPosition(const Vector3& pos );

		void SetOrientation(const Matrix3& orientaion);

		void integrateVelocities(const float& timeStep);
		//damping
		void applyDamping(const float& timeStep);

		//void predictIntegratedTransform(timeStep,body->getInterpolationWorldTransform());

		Vector3 getVelocityInLocalPoint(const Vector3& rel_pos) const;

		void synchronizeCollision2Physics();

	public:

		RigidBodyBase();
		~RigidBodyBase();

		void Create( RigidBodyBaseParams& desc);
		
		///to keep collision detection and dynamics separate we don't store a rigidbody pointer
		///but a rigidbody is derived from btCollisionObject, so we can safely perform an upcast
		static const RigidBodyBase*	upcast(const btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (const RigidBodyBase*)colObj;
			return 0;
		}
		static RigidBodyBase*	upcast(btCollisionObject* colObj)
		{
			if (colObj->getInternalType()==btCollisionObject::CO_RIGID_BODY)
				return (RigidBodyBase*)colObj;
			return 0;
		}


		friend class World;
		
		// temporary FemSoftBody
		friend class FemSoftBody;

	};


}; // SimStep

#endif