#ifndef SOFT_BODY_PARAM_H
#define SOFT_BODY_PARAM_H

#include "Utilities/String.h"

#include "World/CollisionObject.h"
#include "World/CollisionSoftBody.h"

#include <Core/TetraMesh.h>
#include <Core/Material.h>

namespace SimStep {
	//	namespace SoftBody {

	// forward declaration 

	enum SystemUnits
	{
		Meter_Kilogram_Second,
		Centimeter_Kilogram_Second,
		Millimeter_Kilogram_Second,
		Millimeter_Gram_Second,
		Inch_Kilogram_Second,
		Inch_Gram_Second

	};

	struct SystemUnitsConversion
	{
		SystemUnits From;
		SystemUnits To;
	};


	struct _SimStepExport SoftBodyParams : public CollisionObjectParams
	{

		SystemUnitsConversion System_of_Units;

		float	TimeStep; // [sec]

		float   Density; // [Kg/m^3]

		float	YoungModulus;	// [N/m^2]

		float	PoissonRatio; // []

		float	Damping; // [Kg/s] [N*s/m]

		float	Yield; // plasticity

		float	MaxYield; // plasticity

		float	Creep; // plasticity

		Vector3	Gravity; // [m/s^2]

		int		Nb_Solver_Iteration;

		float	Solver_Tolerance;

		int		collisionConfig;

		const char*	MeshFile;

		const char*	MaterialName;

		const char*	resourceGroup;

		mutable TetraMeshPtr tetraMesh;

		mutable MaterialPtr material;

		// dovrebbe esere una tetraMesh
		//mutable Ogre::Mesh* Mesh3d;

		// Smoothed Particles Hydrodynamics Param
		float sphPhyxelSize;

		float sphSupportRadius;

		// default inits.

		SoftBodyParams() : CollisionObjectParams()
		{
			TimeStep	= 0.01f; 

			Density		= 1.0f;

			YoungModulus = 1e6f;

			PoissonRatio = 0.33f;

			Damping = 0.05f;

			Yield = 0.0f; // plasticity

			MaxYield = 0.0f; // plasticity

			Creep = 0.0f; // plasticity

			Gravity = Vector3(0.0f, -9.8f, 0.0f);

			Nb_Solver_Iteration = 25;

			Solver_Tolerance = 1e-6f;

			collisionConfig = CollisionSoftBody::fCollision::Default;

			MeshFile = 0;

			MaterialName = 0;

			resourceGroup = 0;

			// Smoothed Particles Hydrodynamics
			sphPhyxelSize = 1.0f;

			sphSupportRadius = 2.0f;

		}

	};



	//	} // namespace SoftBody
}; //namespace SimStep



#endif