#include "World/RigidBody.h"

#include "btBulletCollisionCommon.h"

#include "src/objects.h"

namespace SimStep
{

	RigidBodyParams::RigidBodyParams()
		: CollisionObjectParams()
		, mass (1.0f)
		, inertia( Matrix3::identity() )
		, linearDamping(0.005f)
		, angularDamping(0.005f)
		, linearDampingThreshold( 0.001f )
		, angularDampingThreshold(0.001f )
		, maxAngularSpeed(100.0f)
	{
	}



	RigidBody::RigidBody(const RigidBodyParams& params )
		: CollisionObject(params), dBody( )
	{

		btCollisionObject::m_internalType = CO_RIGID_BODY;
		
		dBody::getBody() = new dxBody(0);
		dBody::getBody()->firstjoint = 0;
		dBody::getBody()->flags = 0;
		dBody::getBody()->geom = 0;
		dBody::getBody()->average_lvel_buffer = 0;
		dBody::getBody()->average_avel_buffer = 0;

		dMass m;
		dMassSetZero( &m );
		m.mass = params.mass;
		memcpy( (void*)&m.I[0], (void*)&params.inertia, 12*sizeof(float) );
		dBodySetMass( dBody::getBody(), &m );
		
		//setMass(params.m_Mass);
		//setInertiaTensor(params.m_LocalInertia);

		syncronizeDynamics2Collision();


		dSetZero(dBody::getBody()->lvel,4);
		dSetZero(dBody::getBody()->avel,4);
		dSetZero(dBody::getBody()->facc,4);
		dSetZero(dBody::getBody()->tacc,4);
		dSetZero(dBody::getBody()->finite_rot_axis,4);

		// set auto-disable parameters
		dBody::getBody()->average_avel_buffer = dBody::getBody()->average_lvel_buffer = 0; // no buffer at beginning
		//dBodySetAutoDisableDefaults( dBody::id() );	// must do this after adding to world
		dBody::getBody()->adis_stepsleft = dBody::getBody()->adis.idle_steps;
		dBody::getBody()->adis_timeleft = dBody::getBody()->adis.idle_time;
		dBody::getBody()->average_counter = 0;
		dBody::getBody()->average_ready = 0; // average buffer not filled on the beginning
		dBodySetAutoDisableAverageSamplesCount(dBody::getBody(), 0 );

		dBody::getBody()->dampingp.linear_scale = params.linearDamping;
		dBody::getBody()->dampingp.angular_scale = params.angularDamping;

		dBody::getBody()->dampingp.linear_threshold = params.linearDampingThreshold;
		dBody::getBody()->dampingp.angular_threshold = params.angularDampingThreshold;

		dBody::getBody()->flags = dxBodyLinearDamping | dxBodyAngularDamping | dxBodyMaxAngularSpeed;

		dBody::getBody()->max_angular_speed = params.maxAngularSpeed;
		
		
		dBody::getBody()->moved_callback = 0;
		
		dBody::setData(this);

			//b->flags |= w->body_flags & mask;

		//dBodySetDampingDefaults( dBody::id() );	// must do this after adding to world

		//dBody::getBody()->flags |= w->body_flags & dxBodyMaxAngularSpeed;
		//dBody::idgetBodymax_angular_speed = w->max_angular_speed;

	}

	RigidBody::~RigidBody()
	{
		destroyBody();
	}

	void RigidBody::syncronizeDynamics2Collision(void)
	{
		memcpy( (void*)dBody::getBody()->posr.pos, (void*)&m_worldTransform.getOrigin(), sizeof(Vector3) );	
		m_worldTransform.getBasis().getOpenGLSubMatrix( dBody::getBody()->posr.R );
		btQuaternion q = m_worldTransform.getRotation();
		dBody::getBody()->q[0] = q.getW();
		dBody::getBody()->q[1] = q.getX();
		dBody::getBody()->q[2] = q.getY();
		dBody::getBody()->q[3] = q.getZ();
	}

	void RigidBody::destroyBody(void)
	{

		//// detach all neighbouring joints, then delete this body.
		//dxJointNode *n = dBody::getBody()->firstjoint;
		//while (n) {
		//	// sneaky trick to speed up removal of joint references (black magic)
		//	n->joint->node[(n == n->joint->node)].body = 0;

		//	dxJointNode *next = n->next;
		//	n->next = 0;
		//	removeJointReferencesFromAttachedBodies (n->joint);
		//	n = next;
		//}


		// delete the average buffers
		if( dBody::getBody()->average_lvel_buffer)
		{
			delete[] ( dBody::getBody()->average_lvel_buffer);
			 dBody::getBody()->average_lvel_buffer = 0;
		}
		if( dBody::getBody()->average_avel_buffer)
		{
			delete[] ( dBody::getBody()->average_avel_buffer);
			 dBody::getBody()->average_avel_buffer = 0;
		}

		delete dBody::getBody();
		dBody::getBody() = 0;
	}

	void RigidBody::syncronizeCollision2Dynamics(void)
	{
		memcpy( (void*)&m_worldTransform.getOrigin(), (void*)dBodyGetPosition( dBody::getBody() ), sizeof(Vector3) );
		//const float* pos = dBodyGetPosition( dBody::id() );
		//m_worldTransform.setOrigin(btVector3( pos[0],pos[1],pos[2] ));
		const float* rot = dBodyGetRotation( dBody::getBody() );
		//m_worldTransform.getBasis().setValue( 
		//	rot[0], rot[4], rot[8],
		//	rot[1], rot[5], rot[9],
		//	rot[2], rot[6], rot[10] );
		//m_worldTransform.getBasis().setValue( 
		//	rot[0], rot[1], rot[2],
		//	rot[4], rot[5], rot[6],
		//	rot[8], rot[9], rot[10] );
		memcpy( (void*)&m_worldTransform.getBasis(), (void*)dBodyGetRotation( dBody::id() ), sizeof(Matrix3) );

	}

	CollisionObject* RigidBody::getCollisionObject(void) const
	{
		return static_cast<CollisionObject*>( const_cast<RigidBody*>(this) );
	}

	dBody* RigidBody::getdBody(void) const
	{
		return static_cast<dBody*>( const_cast<RigidBody*>(this) );
	}


	void RigidBody::setMoveListenerCallback(MoveListenerCallback moveCallback)
	{
		m_MoveListenerCallback = moveCallback;
		dBody::setMovedCallback( RigidBody::MoveCallback );
		//dBody::id()->moved_callback = MoveCallback;
	}

	void RigidBody::MoveCallback( dBodyID b )
	{
		RigidBody* rigidBody = static_cast<RigidBody*>(b->userdata);
		rigidBody->m_MoveListenerCallback(rigidBody);
	}

	void RigidBody::setPosition(const Vector3& position)
	{
		dBodySetPosition ( dBody::id(), position.getX(), position.getY(), position.getZ() );
		m_worldTransform.setOrigin(btVector3(position.getX(), position.getY(), position.getZ()) );
		//memcpy( (void*)&m_worldTransform.getOrigin(), (void*)dBodyGetPosition( dBody::id() ), sizeof(Vector3) );
	}

	Vector3 RigidBody::getPosition(void)
	{
		const float* pos = dBody::getPosition();
		return Vector3( pos[0], pos[1], pos[2] ); //Vector3( m_worldTransform.getOrigin() );
	}
	const Vector3 RigidBody::getPosition(void) const
	{
		const float* pos = dBody::getPosition();
		return Vector3( pos[0], pos[1], pos[2] ); //Vector3( m_worldTransform.getOrigin() );
	}

	void RigidBody::setRotation(const Matrix3& rotation)
	{ 
		//const Quat qrot = Quat(rotation);
		//dQuaternion OdeQ;
		//OdeQ[0] = qrot[3];
		//OdeQ[1] = qrot[0]; OdeQ[2] = qrot[1]; OdeQ[3] = qrot[2];
		//dBodySetRotation( dBody::id(), OdeQ);
		//btQuaternion btQ = btQuaternion(qrot[0], qrot[1], qrot[2], qrot[3]);
		//m_worldTransform.setRotation(btQ);
		///*dMatrix3 R;
		//R[0] = rotation[0][0]; R[1] = rotation[1][0]; R[2] = rotation[2][0];
		//R[4] = rotation[0][1]; R[5] = rotation[1][1]; R[6] = rotation[2][1];
		//R[8] = rotation[0][2]; R[9] = rotation[1][2]; R[10] = rotation[2][2];
		//dBodySetRotation( dBody::id(), R);*/
		////m_worldTransform.setBasis( btMatrix3x3(
		////	rotation[0][0], rotation[0][1], rotation[0][2],
		////	rotation[1][0], rotation[1][1], rotation[1][2],
		////	rotation[2][0], rotation[2][1], rotation[2][2] )
		////	);

		m_worldTransform.setBasis(btMatrix3x3(
			rotation[0][0], rotation[1][0], rotation[2][0],
			rotation[0][1], rotation[1][1], rotation[2][1],
			rotation[0][2], rotation[1][2], rotation[2][2] 
		) );

		this->syncronizeDynamics2Collision();
	}

	void RigidBody::setRotation(const Quat& qrot )
	{
		dQuaternion OdeQ;
		OdeQ[0] = qrot[3];
		OdeQ[1] = qrot[0]; OdeQ[2] = qrot[1]; OdeQ[3] = qrot[2];
		dBodySetRotation( dBody::id(), OdeQ);
		btQuaternion btQ = btQuaternion(qrot[0], qrot[1], qrot[2], qrot[3]);
		m_worldTransform.setRotation(btQ);
	}

	Matrix3 RigidBody::getRotation(void) const
	{
		Matrix3 rotation;
		const Vector3& row0 = reinterpret_cast<const Vector3&>(m_worldTransform.getBasis().getRow(0) );
		const Vector3& row1 = reinterpret_cast<const Vector3&>(m_worldTransform.getBasis().getRow(1) );
		const Vector3& row2 = reinterpret_cast<const Vector3&>(m_worldTransform.getBasis().getRow(2) );
		rotation.setRow( 0, row0 );
		rotation.setRow( 1, row1 );
		rotation.setRow( 2, row2 );
		return rotation;
	}

	Quat RigidBody::getRotationQuat(void) const
	{
		//btQuaternion btQ;
		//m_worldTransform.getBasis().getRotation(btQ);
		//return Quat(btQ.getX(), btQ.getY(), btQ.getZ(), btQ.getW() );
		return Quat( dBody::id()->q[1], dBody::id()->q[2], dBody::id()->q[3], 
			dBody::id()->q[0] );
	}


	void RigidBody::setLinearVel(const Vector3& linVel )
	{
		dBodySetLinearVel( dBody::id(), linVel[0], linVel[1], linVel[2] );
	}

	void RigidBody::setAngularVel(const Vector3& angVel )
	{
		dBodySetAngularVel( dBody::id(), angVel[0], angVel[1], angVel[2] );
	}

	Vector3 RigidBody::getLinearVel(void) const
	{
		const float* linVel = dBodyGetLinearVel( dBody::id() );
		return Vector3( linVel[0], linVel[1], linVel[2] );
	}
	Vector3 RigidBody::getAngularVel(void) const
	{
		const float* angVel = dBodyGetAngularVel( dBody::id() );
		return Vector3( angVel[0], angVel[1], angVel[2] );
	}


	void RigidBody::setMass(const float& newMass)
	{
		dMass OdeMass;
		dBodyGetMass( dBody::id(), &OdeMass );
		OdeMass.mass = newMass;
		dBodySetMass( dBody::id(), &OdeMass );
	}

	void RigidBody::setInertiaTensor( const Matrix3& inertiaTensor)
	{
		dBody::id()->mass.setParameters( dBody::id()->mass.mass, 0.0f, 0.0f, 0.0f,
			inertiaTensor[0][0], inertiaTensor[1][1], inertiaTensor[2][2],
			inertiaTensor[0][1], inertiaTensor[0][2], inertiaTensor[1][2] );
	}

	void RigidBody::setInertiaTensor( const Vector3& axis, const Matrix3& inertiaTensor )
	{
		dBody::id()->mass.setParameters( dBody::id()->mass.mass, 
			axis[0], axis[1], axis[2],
			inertiaTensor[0][0], inertiaTensor[1][1], inertiaTensor[2][2],
			inertiaTensor[0][1], inertiaTensor[0][2], inertiaTensor[1][2] );
	}

	void RigidBody::addForce( const Vector3& force )
	{ 
		dBodyAddForce( dBody::id(), force[0], force[1], force[2] );
	}

	void RigidBody::addTorque( const Vector3& torque )
	{ 
		dBodyAddTorque( dBody::id(), torque[0], torque[1], torque[2] ); 
	}

	void RigidBody::addRelForce ( const Vector3& force )
	{ 
		dBodyAddRelForce( dBody::id(), force[0], force[1], force[2] ); 
	}

	void RigidBody::addRelTorque(const Vector3& torque )
	{ 
		dBodyAddRelTorque( dBody::id(), torque[0], torque[1], torque[2] ); 
	}


	void RigidBody::addForceAtPos(const Vector3& force, const Vector3& pos )
	{
		dBodyAddForceAtPos( dBody::id(), force[0],force[1],force[2], 
			pos[0],pos[1],pos[2] ); 
	}

	void RigidBody::addForceAtRelPos( const Vector3& force, const Vector3& pos)
	{
		dBodyAddForceAtRelPos( dBody::id(), force[0],force[1],force[2], 
			pos[0],pos[1],pos[2] );
	}

	void RigidBody::addRelForceAtPos( const Vector3& force, const Vector3& pos)
	{
		dBodyAddRelForceAtPos( dBody::id(), force[0],force[1],force[2], 
			pos[0],pos[1],pos[2] );
	}

	void RigidBody::addRelForceAtRelPos( const Vector3& force, const Vector3& pos)
	{
		dBodyAddRelForceAtRelPos( dBody::id(), force[0],force[1],force[2], 
			pos[0],pos[1],pos[2] );
	}


	Vector3 RigidBody::getForce() const
	{
		const float* const force = static_cast<const float*>(dBodyGetForce( dBody::id() ) );
		return Vector3( force[0], force[1], force[2] );
	}

	Vector3 RigidBody::getTorque() const
	{
		const float* const torque = static_cast<const float*>(dBodyGetTorque( dBody::id() ) );
		return Vector3( torque[0], torque[1], torque[2] );
	}

	void RigidBody::setForce(const Vector3& force)
	{ 
		dBodySetForce( dBody::id(), force[0], force[1], force[2] );
	}

	void RigidBody::setTorque(const Vector3& torque)
	{ 
		dBodySetTorque( dBody::id(), torque[0], torque[1], torque[2] );
	}

	Vector3 RigidBody::getRelPointPos(const Vector3& pos ) const
	{
		dVector3 result;
		dBodyGetRelPointPos( dBody::id(), pos[0], pos[1], pos[2], result);
		return Vector3( result[0], result[1], result[2] );
	}

	Vector3 RigidBody::getRelPointVel(const Vector3& pos ) const
	{
		dVector3 result;
		dBodyGetRelPointVel(dBody::id(), pos[0], pos[1], pos[2], result);
		return Vector3( result[0], result[1], result[2] );
	}


	Vector3 RigidBody::getPointVel(const Vector3& pos ) const
	{
		dVector3 result;
		dBodyGetPointVel( dBody::id(), pos[0], pos[1], pos[2], result );
		return Vector3( result[0], result[1], result[2] );
	}

	Vector3 RigidBody::getPosRelPoint(const Vector3& pos ) const
	{
		dVector3 result;
		dBodyGetPosRelPoint( dBody::id(), pos[0], pos[1], pos[2], result );
		return Vector3( result[0], result[1], result[2] );
	}


	Vector3 RigidBody::vectorToWorld(const Vector3& pos ) const
	{ 
		dVector3 result;
		dBodyVectorToWorld( dBody::id(), pos[0], pos[1], pos[2], result );
		return Vector3( result[0], result[1], result[2] );
	}


	Vector3 RigidBody::vectorFromWorld(const Vector3& pos ) const
	{ 
		dVector3 result;
		dBodyVectorFromWorld( dBody::id(), pos[0], pos[1], pos[2], result );
		return Vector3( result[0], result[1], result[2] );
	}




RigidBodyBase::RigidBodyBase() : btCollisionObject() , dBody()
{

	m_internalType		=	CO_RIGID_BODY;
}

RigidBodyBase::~RigidBodyBase()
{

	return;
}


void RigidBodyBase::Create( RigidBodyBaseParams& desc )
{

	m_InertiaTensor = desc.m_LocalInertia;
	m_invInertiaTensor = inverse( desc.m_LocalInertia );

	m_Mass = desc.m_Mass;
	m_invMass = (float)( 1.0 / (double)desc.m_Mass );

	//setCollisionShape( new btSoftBodyCollisionShape(this) );

	m_Position = desc.m_Position;
	memcpy( (void*)&m_worldTransform.getOrigin(), (void*)&desc.m_Position, sizeof(Vector3) );
	
	m_Orientation = desc.m_Orientation;
	memcpy( (void*)&m_worldTransform.getBasis(), (void*)&desc.m_Orientation, sizeof(Matrix3) );

	setCollisionShape( desc.mCollisionShape );
}


void RigidBodyBase::SetPosition(const Vector3& pos )
{
	m_Position =pos;
	memcpy( (void*)&m_worldTransform.getOrigin(), (void*)&pos, sizeof(Vector3) );
}


void RigidBodyBase::SetOrientation(const Matrix3& orientaion)
{
	m_Orientation = orientaion;
	memcpy( (void*)&m_worldTransform.getBasis(), (void*)&orientaion, sizeof(Matrix3) );
}


void RigidBodyBase::integrateVelocities(const float& timeStep)
{
	//if (isStaticOrKinematicObject())
	//	return;

	//m_linearVelocity += ( ( m_totalForce * m_invMass ) + m_Gravity ) * timeStep;
	//m_angularVelocity += m_invInertiaTensorWorld * m_totalTorque * timeStep;


	///// clamp angular velocity. collision calculations will fail on higher angular velocities	
	//btScalar angVelocity = m_angularVelocity.length();
	//if (angVelocity*step > MAX_ANGVEL)
	//{
	//	m_angularVelocity *= (MAX_ANGVEL/step) /angvel;
	//}

}

//damping
void RigidBodyBase::applyDamping(const float& timeStep)
{

	//m_linearVelocity *= powf( 1.0f - m_linearDamping, timeStep);
	//m_angularVelocity *= powf( 1.0f - m_angularDamping, timeStep);

}


Vector3 RigidBodyBase::getVelocityInLocalPoint(const Vector3& rel_pos) const
{
	return m_linearVelocity + cross( m_angularVelocity, rel_pos);
}


//predictIntegratedTransform(timeStep,body->getInterpolationWorldTransform());

void RigidBodyBase::synchronizeCollision2Physics()
{

	memcpy( (void*)&m_worldTransform.getOrigin(), (void*)dBodyGetPosition( dBody::id() ), sizeof(Vector3) );
	memcpy( (void*)&m_worldTransform.getBasis(), (void*)dBodyGetRotation( dBody::id() ), sizeof(Matrix3) );

	return;
}


}; // namespace SimStep