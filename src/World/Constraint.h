#ifndef SimStep_Constraint_h__
#define SimStep_Constraint_h__




namespace SimStep
{

	class ConstraintParams
	{

	};


	class Constraint
	{

	public:

		Constraint(const ConstraintParams& params );

		virtual ~Constraint();
	};

	
}; // namespace SimStep

#endif // SimStep_Constraint_h__