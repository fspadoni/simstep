
#include "World/CollisionSoftBody.h"
#include "Collision/btSoftBodyInternals.h"

#include "Collision/SoftBodyCollisionWorldInfo.h"

#include "World/CollisionObject.h"

namespace SimStep {
	//namespace FemSoftBody {



//void CollisionSoftBody::getAabb(btVector3& aabbMin,btVector3& aabbMax) const
//{
//	aabbMin = btVector3(m_Bounds[0].getX(), m_Bounds[0].getY(), m_Bounds[0].getZ() );
//	aabbMax = btVector3(m_Bounds[1].getX(), m_Bounds[1].getY(), m_Bounds[1].getZ() );
//}

//
//void CollisionSoftBody::updateBounds()
//{
//	if(NodeDynamicTree.m_root)
//	{
//		const Vector3&	mins = NodeDynamicTree.m_root->volume.Mins();
//		const Vector3&	maxs = NodeDynamicTree.m_root->volume.Maxs();
//		const float		csm=  getCollisionShape()->getMargin();
//		const Vector3		mrg= Vector3(	csm,
//			csm,
//			csm)*1; // ??? to investigate...
//		m_Bounds.mAabbMin = mins-mrg;
//		m_Bounds.mAabbMax = maxs+mrg;
//		if(0!= getBroadphaseHandle())
//		{					
//			m_CollisionWorldInfo->m_broadphase->setAabb(	getBroadphaseHandle(),
//				btVector3(m_Bounds.mAabbMin.getX(), m_Bounds.mAabbMin.getY(), m_Bounds.mAabbMin.getZ() ) ,
//				btVector3(m_Bounds.mAabbMax.getX(), m_Bounds.mAabbMax.getY(), m_Bounds.mAabbMax.getZ() ) ,
//				m_CollisionWorldInfo->m_dispatcher);
//		}
//	}
//	else
//	{
//		m_Bounds.mAabbMin = m_Bounds.mAabbMax =  Vector3(0,0,0);
//	}		
//}

//void CollisionSoftBody::defaultCollisionHandler(btCollisionObject* pco)
//{
//	switch( m_CollisionsConfig & fCollision::RVSmask)
//	{
//	case	fCollision::SDF_RS:
//		{
//			btSoftColliders::CollideSDF_RS	docollide;	
//			// da riguardare: sfruttare l'upcast sarebbe meglio
//			//RigidBody* rigid_body = RigidBody::upcast(pco);
//			RigidBody* rigid_body = static_cast<RigidBody*>(pco);
//			
//			//const btTransform	wtr=prb->getInterpolationWorldTransform();
//			//const btTransform	ctr=prb->getWorldTransform();
//			//const btScalar		timemargin=(wtr.getOrigin()-ctr.getOrigin()).length();
//			const btScalar		basemargin=getCollisionShape()->getMargin();
//			btVector3			mins;
//			btVector3			maxs;
//			btDbvtVolume		volume;
//			pco->getCollisionShape()->getAabb(	pco->getWorldTransform(),
//				mins,
//				maxs);
//			volume=btDbvtVolume::FromMM(mins,maxs);
//			volume.Expand(btVector3(basemargin,basemargin,basemargin));		
//			docollide.psb		=	this;
//			docollide.ColObject =  pco;
//			docollide.prb		=	rigid_body;
//			docollide.dynmargin	=	basemargin; // +timemargin;
//			docollide.stamargin	=	basemargin;
//			btDbvt::collideTV( NodeDynamicTree.m_root,volume,docollide);
//		}
//		break;
//	case	fCollision::CL_RS:
//		//{
//		//	btSoftColliders::CollideCL_RS	collider;
//		//	collider.Process(this,btRigidBody::upcast(pco));
//		//}
//		break;
//	}
//}
//
////
//void CollisionSoftBody::defaultCollisionHandler( CollisionSoftBody* psb)
//{
//	//const int cf=m_cfg.collisions&psb->m_cfg.collisions;
//	//switch(cf&fCollision::SVSmask)
//	//{
//	//case	fCollision::CL_SS:
//	//	{
//	//		btSoftColliders::CollideCL_SS	docollide;
//	//		docollide.Process(this,psb);
//	//	}
//	//	break;
//	//case	fCollision::VF_SS:
//	//	{
//	//		btSoftColliders::CollideVF_SS	docollide;
//	//		/* common					*/ 
//	//		docollide.mrg=	getCollisionShape()->getMargin()+
//	//			psb->getCollisionShape()->getMargin();
//	//		/* psb0 nodes vs psb1 faces	*/ 
//	//		docollide.psb[0]=this;
//	//		docollide.psb[1]=psb;
//	//		btDbvt::collideTT(	docollide.psb[0]->m_ndbvt.m_root,
//	//			docollide.psb[1]->m_fdbvt.m_root,
//	//			docollide);
//	//		/* psb1 nodes vs psb0 faces	*/ 
//	//		docollide.psb[0]=psb;
//	//		docollide.psb[1]=this;
//	//		btDbvt::collideTT(	docollide.psb[0]->m_ndbvt.m_root,
//	//			docollide.psb[1]->m_fdbvt.m_root,
//	//			docollide);
//	//	}
//	//	break;
//	//}
//}
//
//
//Bool CollisionSoftBody::CheckContact( btCollisionObject* collObj, int& IdxNode, /*const SimdVector3& pos,*/
//			 float margin, CollisionSoftBody::ContactInfo& contact_info) const
//{
//	bool ret = false;
//
//	btVector3 x = btVector3(m_Nodes[IdxNode].getX(), m_Nodes[IdxNode].getY(), m_Nodes[IdxNode].getZ() );
//	btVector3			normal;
//	btCollisionShape*	shape=  collObj->getCollisionShape(); // prb->getCollisionShape();
//	const btTransform&	wtr= collObj->getWorldTransform(); // getInterpolationWorldTransform();
//	btVector3 p = wtr.invXform( x );
//
//	btScalar distance = m_CollisionWorldInfo->m_sparsesdf.Evaluate( 
//		p,
//		shape,
//		normal,
//		margin);
//
//	if( distance <0 )
//	{
//		contact_info.m_body		= static_cast<CollisionObject*>(collObj);
//		contact_info.m_normal	=	-( wtr.getBasis()*normal );
//		contact_info.m_offset	= -dot(	contact_info.m_normal,
//			contact_info.m_normal * distance );
//			//x - contact_info.m_normal * distance );
//		ret = true;
//	}
//
//	return ret;
//}


////
//int	FemSoftBody::GenerateClusters(int k,int maxiterations)
//{
//	int i;
//	ReleaseClusters();
//	m_ClustersArray.resize( btMin( k, m_Nb_Nodes ) );
//	for(i=0;i< m_ClustersArray.size();++i)
//	{
//		m_ClustersArray[i]			=	new( btAlignedAlloc(sizeof(Cluster),16)) Cluster();
//		//m_clusters[i]->m_collide=	true;
//	}
//	k = m_ClustersArray.size();
//	if(k>0)
//	{
//		/* Initialize		*/ 
//		btAlignedObjectArray<SimdVector3>	centers;
//		SimdVector3						cog(0,0,0);
//		int								i;
//		for(i=0;i< m_Nb_Nodes;++i)
//		{
//			cog += m_NodesPtr[i];
//			m_ClustersArray[(i*29873)%m_ClustersArray.size()]->m_nodes.push_back(&m_NodesPtr[i]);
//			m_ClustersArray[(i*29873)%m_ClustersArray.size()]->m_nodes_vel.push_back(&m_NodesVelocity[i]);
//			
//		}
//		cog /= m_Nb_Nodes; // (btScalar)m_nodes.size();
//		centers.resize(k,cog);
//	//	/* Iterate			*/ 
//		const float	slope = 16;
//		bool changed;
//		int iterations = 0;
//		do	{
//			const btScalar w = 2- btMin<btScalar>(1,iterations/slope);
//			changed=false;
//			iterations++;	
//			int i;
//
//			for(i=0;i<k;++i)
//			{
//				SimdVector3	c(0,0,0);
//				for(int j=0; j < m_ClustersArray[i]->m_nodes.size();++j)
//				{
//					c += *m_ClustersArray[i]->m_nodes[j];
//				}
//				if( m_ClustersArray[i]->m_nodes.size())
//				{
//					c /= m_ClustersArray[i]->m_nodes.size();
//					c =	centers[i] + ( c-centers[i] ) * w;
//					// da controllare .length2 e lengthSqr
//					changed	 |=	(  SSE::lengthSqr(c-centers[i]) /*.length2()*/ >SIMD_EPSILON);
//					centers[i]	=	c;
//					m_ClustersArray[i]->m_nodes.resize(0);
//					m_ClustersArray[i]->m_nodes_vel.resize(0);
//				}			
//			}
//			for(i=0;i< m_Nb_Nodes; ++i)
//			{
//				const SimdVector3 nx = m_NodesPtr[i];
//				int	kbest=0;
//				btScalar kdist = ClusterMetric(centers[0], nx);
//				for(int j=1;j<k;++j)
//				{
//					const btScalar	d = ClusterMetric(centers[j],nx);
//					if( d < kdist )
//					{
//						kbest=j;
//						kdist=d;
//					}
//				}
//				m_ClustersArray[kbest]->m_nodes.push_back(&m_NodesPtr[i]);
//				m_ClustersArray[kbest]->m_nodes_vel.push_back(&m_NodesVelocity[i]);
//			}		
//		} while( changed && (iterations < maxiterations) );
//		/* Merge		*/ 
//		btAlignedObjectArray<int>	cids;
//		cids.resize( m_Nb_Nodes,-1);
//		for(i=0;i< m_ClustersArray.size();++i)
//		{
//			for(int j=0;j<m_ClustersArray[i]->m_nodes.size();++j)
//			{
//				cids[ int(m_ClustersArray[i]->m_nodes[j] - &m_NodesPtr[0])] = i;
//			}
//		}
//		//for(i=0;i<m_faces.size();++i)
//		//{
//		//	const int idx[]={	int(m_faces[i].m_n[0]-&m_nodes[0]),
//		//		int(m_faces[i].m_n[1]-&m_nodes[0]),
//		//		int(m_faces[i].m_n[2]-&m_nodes[0])};
//	//		for(int j=0;j<3;++j)
//	//		{
//	//			const int cid=cids[idx[j]];
//	//			for(int q=1;q<3;++q)
//	//			{
//	//				const int kid=idx[(j+q)%3];
//	//				if(cids[kid]!=cid)
//	//				{
//	//					if(m_clusters[cid]->m_nodes.findLinearSearch(&m_nodes[kid])==m_clusters[cid]->m_nodes.size())
//	//					{
//	//						m_clusters[cid]->m_nodes.push_back(&m_nodes[kid]);
//	//					}
//	//				}
//	//			}
//	//		}
//		//}
//		/* Master		*/ 
//		if( m_ClustersArray.size()>1)
//		{
//			Cluster* pmaster = new(btAlignedAlloc(sizeof(Cluster),16)) Cluster();
//			//pmaster->m_collide	=	false;
//			pmaster->m_nodes.reserve( m_Nb_Nodes );
//			pmaster->m_nodes_vel.reserve( m_Nb_Nodes );
//
//			for(int i=0; i< m_Nb_Nodes; ++i )
//			{
//				pmaster->m_nodes.push_back( &m_NodesPtr[i] );
//				pmaster->m_nodes_vel.push_back( &m_NodesVelocity[i] );
//			}
//			m_ClustersArray.push_back(pmaster);
//			btSwap( m_ClustersArray[0], m_ClustersArray[m_ClustersArray.size()-1] );
//		}
//		/* Terminate	*/ 
//		for(i=0; i<m_ClustersArray.size(); ++i)
//		{
//			if( m_ClustersArray[i]->m_nodes.size() == 0 )
//			{
//				ReleaseCluster(i--);
//			}
//		}
//
//		InitializeClusters();
//		UpdateClusters();
//		return( m_ClustersArray.size() );
//	}
//	return(0);
//}

////
//void FemSoftBody::ReleaseCluster(int index)
//{
//	Cluster* c = m_ClustersArray[index];
//	//if(c->m_leaf)
//	//{
//	//	ClustersDynamicTree.remove(c->m_leaf);
//	//	//m_cdbvt.remove(c->m_leaf);
//	//}
//	c->~Cluster();
//	btAlignedFree(c);
//	std::vector<Cluster*>::iterator iter = m_ClustersArray.begin();
//	//iter = std::find( m_ClustersArray.begin(), m_ClustersArray.end(), c );
//
//	iter += index;
//	m_ClustersArray.erase(iter);
//	//m_clusters.remove(c);
//}
//
//
//void FemSoftBody::ReleaseClusters()
//{
//	while(m_ClustersArray.size()>0) 
//	{
//		ReleaseCluster(0);
//	}
//}
//
//
////
//void FemSoftBody::InitializeClusters()
//{
//	int i;
//
//	for( i=0; i<m_ClustersArray.size();++i)
//	{
//		Cluster& c= *m_ClustersArray[i];
//
//		//c.m_imass=0;
//		//c.m_masses.resize(c.m_nodes.size());
//		//for(int j=0;j<c.m_nodes.size();++j)
//		//{
//		//	c.m_masses[j]	=	c.m_nodes[j]->m_im>0?1/c.m_nodes[j]->m_im:0;
//		//	c.m_imass		+=	c.m_masses[j];
//		//}
//		//c.m_imass		=	1/c.m_imass;
//		//c.m_com			=	btSoftBody::clusterCom(&c);
//		//c.m_lv			=	btVector3(0,0,0);
//		//c.m_av			=	btVector3(0,0,0);
//		c.m_leaf		=	0;
//		///* Inertia	*/ 
//		//btMatrix3x3&	ii=c.m_locii;
//		//ii[0]=ii[1]=ii[2]=btVector3(0,0,0);
//		//{
//		//	int i,ni;
//
//		//	for(i=0,ni=c.m_nodes.size();i<ni;++i)
//		//	{
//		//		const btVector3	k=c.m_nodes[i]->m_x-c.m_com;
//		//		const btVector3	q=k*k;
//		//		const btScalar	m=c.m_masses[i];
//		//		ii[0][0]	+=	m*(q[1]+q[2]);
//		//		ii[1][1]	+=	m*(q[0]+q[2]);
//		//		ii[2][2]	+=	m*(q[0]+q[1]);
//		//		ii[0][1]	-=	m*k[0]*k[1];
//		//		ii[0][2]	-=	m*k[0]*k[2];
//		//		ii[1][2]	-=	m*k[1]*k[2];
//		//	}
//		//}
//		//ii[1][0]=ii[0][1];
//		//ii[2][0]=ii[0][2];
//		//ii[2][1]=ii[1][2];
//		//ii=ii.inverse();
//		///* Frame	*/ 
//		//c.m_framexform.setIdentity();
//		//c.m_framexform.setOrigin(c.m_com);
//		//c.m_framerefs.resize(c.m_nodes.size());
//		//{
//		//	int i;
//		//	for(i=0;i<c.m_framerefs.size();++i)
//		//	{
//		//		c.m_framerefs[i]=c.m_nodes[i]->m_x-c.m_com;
//		//	}
//		//}
//	}
//}
//
////
//void FemSoftBody::UpdateClusters()
//{
//	//BT_PROFILE("UpdateClusters");
//	int i;
//
//	for(i=0;i<m_ClustersArray.size();++i)
//	{
//		FemSoftBody::Cluster& c = *m_ClustersArray[i];
//		const int n = c.m_nodes.size();
//		//const btScalar			invn=1/(btScalar)n;
//		if(n)
//		{
////			/* Frame				*/ 
////			const btScalar	eps=btScalar(0.0001);
////			btMatrix3x3		m,r,s;
////			m[0]=m[1]=m[2]=btVector3(0,0,0);
////			m[0][0]=eps*1;
////			m[1][1]=eps*2;
////			m[2][2]=eps*3;
////			c.m_com=clusterCom(&c);
////			for(int i=0;i<c.m_nodes.size();++i)
////			{
////				const btVector3		a=c.m_nodes[i]->m_x-c.m_com;
////				const btVector3&	b=c.m_framerefs[i];
////				m[0]+=a[0]*b;m[1]+=a[1]*b;m[2]+=a[2]*b;
////			}
////			PolarDecompose(m,r,s);
////			c.m_framexform.setOrigin(c.m_com);
////			c.m_framexform.setBasis(r);		
////			/* Inertia			*/ 
////#if 1/* Constant	*/ 
////			c.m_invwi=c.m_framexform.getBasis()*c.m_locii*c.m_framexform.getBasis().transpose();
////#else
////#if 0/* Sphere	*/ 
////			const btScalar	rk=(2*c.m_extents.length2())/(5*c.m_imass);
////			const btVector3	inertia(rk,rk,rk);
////			const btVector3	iin(btFabs(inertia[0])>SIMD_EPSILON?1/inertia[0]:0,
////				btFabs(inertia[1])>SIMD_EPSILON?1/inertia[1]:0,
////				btFabs(inertia[2])>SIMD_EPSILON?1/inertia[2]:0);
////
////			c.m_invwi=c.m_xform.getBasis().scaled(iin)*c.m_xform.getBasis().transpose();
////#else/* Actual	*/ 		
////			c.m_invwi[0]=c.m_invwi[1]=c.m_invwi[2]=btVector3(0,0,0);
////			for(int i=0;i<n;++i)
////			{
////				const btVector3	k=c.m_nodes[i]->m_x-c.m_com;
////				const btVector3		q=k*k;
////				const btScalar		m=1/c.m_nodes[i]->m_im;
////				c.m_invwi[0][0]	+=	m*(q[1]+q[2]);
////				c.m_invwi[1][1]	+=	m*(q[0]+q[2]);
////				c.m_invwi[2][2]	+=	m*(q[0]+q[1]);
////				c.m_invwi[0][1]	-=	m*k[0]*k[1];
////				c.m_invwi[0][2]	-=	m*k[0]*k[2];
////				c.m_invwi[1][2]	-=	m*k[1]*k[2];
////			}
////			c.m_invwi[1][0]=c.m_invwi[0][1];
////			c.m_invwi[2][0]=c.m_invwi[0][2];
////			c.m_invwi[2][1]=c.m_invwi[1][2];
////			c.m_invwi=c.m_invwi.inverse();
////#endif
////#endif
////			/* Velocities			*/ 
////			c.m_lv=btVector3(0,0,0);
////			c.m_av=btVector3(0,0,0);
////			{
//				int i;
//
//				SimdVector3 velocity = ZeroVector3;
//				btVector3 bt_velocity;
//				for(i=0;i<n;++i)
//				{
//					//const SimdVector3	v = c.m_nodes[i]->m_v*c.m_masses[i];
//					velocity += *c.m_nodes_vel[i];
//					//c.m_lv	+=	v;
//					//c.m_av	+=	cross(c.m_nodes[i]->m_x-c.m_com,v);
//				}
//				memcpy(&bt_velocity, &velocity, sizeof(btVector3) );
////			}
////			c.m_lv=c.m_imass*c.m_lv*(1-c.m_ldamping);
////			c.m_av=c.m_invwi*c.m_av*(1-c.m_adamping);
////			c.m_vimpulses[0]	=
////				c.m_vimpulses[1]	= btVector3(0,0,0);
////			c.m_dimpulses[0]	=
////				c.m_dimpulses[1]	= btVector3(0,0,0);
////			c.m_nvimpulses		= 0;
////			c.m_ndimpulses		= 0;
////			/* Matching				*/ 
////			if(c.m_matching>0)
////			{
////				for(int j=0;j<c.m_nodes.size();++j)
////				{
////					Node&			n=*c.m_nodes[j];
////					const btVector3	x=c.m_framexform*c.m_framerefs[j];
////					n.m_x=Lerp(n.m_x,x,c.m_matching);
////				}
////			}
////			/* Dbvt					*/ 
//			//if(c.m_collide)
//			{
//				SimdVector3	min_node = *c.m_nodes[0];
//				SimdVector3	max_node = min_node;
//				for(int j=1; j<n; ++j)
//				{
//					min_node = SSE::minPerElem( min_node , *c.m_nodes[j] );
//					max_node = SSE::maxPerElem( max_node , *c.m_nodes[j] );
//					//mi.setMin(c.m_nodes[j]->m_x);
//					//mx.setMax(c.m_nodes[j]->m_x);
//				}	
//				btVector3 mi;
//				btVector3 mx;
//				memcpy( &mi, &min_node, sizeof(btVector3) );
//				memcpy( &mx, &max_node, sizeof(btVector3) );
//
//				const ATTRIBUTE_ALIGNED16(btDbvtVolume)	bounds=btDbvtVolume::FromMM(mi,mx);
//				if( c.m_leaf )
//				{
//
//					ClustersDynamicTree.update(c.m_leaf,bounds, bt_velocity /*c.m_lv*m_sst.sdt*3*/,  m_collisionShape->getMargin() /*m_sst.radmrg*/);
//				}
//				else
//					c.m_leaf = ClustersDynamicTree.insert(bounds,&c);
//			}
//		}
//	}
//}


}; // SimStep 
//  }; namespace FemSoftBody