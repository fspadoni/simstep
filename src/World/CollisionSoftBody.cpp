#include "World/CollisionSoftBody.h"

#include "World/SoftBodyParam.h"

// collision
#include "Collision/SoftBodyCollisionWorldInfo.h"

#include "World/RigidBody.h"

#include <Core/SimdMemoryBufferManager.h>

#include <Core/SubTetraMesh.h>

#include <fstream>

// collision
#include "Collision/btSoftBodyInternals.h"

namespace SimStep {



	CollisionSoftBody::CollisionSoftBody(const SoftBodyParams& params,
		const DataValuePairList* otherparams )
		: CollisionObject( params )
	{

		mName = params.name;

		btCollisionObject::m_collisionShape = new btSoftBodyCollisionShape(this);
		btCollisionObject::m_internalType = CO_SOFT_BODY;
		
		mCollisionsConfig = params.collisionConfig; 
			// fCollision::Default; //FemSoftBody:: fCollision::CL_SS + 

		m_worldTransform.setIdentity();
		m_collisionShape->setMargin(  params.mCollisionMargin );


		mTetraMesh = params.tetraMesh;

		mNbVertices = 0;
		mVertexDynamicTree.clear();
		mFaceDynamicTree.clear();

		mBounds.reset();

		mDynamicContactArray.resize(0);
		mStaticContactArray.resize(0);

	}


	CollisionSoftBody::~CollisionSoftBody()
	{

	}

	const CollisionSoftBody* CollisionSoftBody::downcast(const btCollisionObject* colObj)
	{
		if (colObj->getInternalType()==btCollisionObject::CO_SOFT_BODY)
		{
			return static_cast<const CollisionSoftBody*>(colObj);
		}
		return 0;
	}

	CollisionSoftBody* CollisionSoftBody::downcast(btCollisionObject* colObj)
	{
		if (colObj->getInternalType()==btCollisionObject::CO_SOFT_BODY)
		{
			return static_cast<CollisionSoftBody*>(colObj);
		}
		return 0;
	}

	void CollisionSoftBody::initializeBuffers(void)
	{

		mNbVertices = mTetraMesh->getSubMesh(0)->mVertices.getSize(); 
		mNbFaces = mTetraMesh->getSubMesh(0)->mFaces->getSize() /3;

		mVertex = 
			BufferManager::getSingleton().createBuffer<Vector3>(mNbVertices);
		//mVertex->copyData( mTetraMesh->>getSubMesh(0)-> );

		mVertexVels = 
			BufferManager::getSingleton().createBuffer<Vector3>(mNbVertices);
		//mVertexVels->copyData( mTetraMesh-> );

		mVertexNormals = 
			BufferManager::getSingleton().createBuffer<Vector3>(3*mNbFaces);
		mVertexNormals->copyData( *mTetraMesh->getSubMesh(0)->mVertexNormals );

		mDbvtNodes = 
			BufferManager::getSingleton().createBuffer<DbvtNode*>(mNbVertices);

		mDbvtNodesId = 
			BufferManager::getSingleton().createBuffer<ushort>(mNbVertices);

		
		mFaces  = BufferManager::getSingleton().createBuffer<ushort>(3*mNbFaces);
		mFaces->copyData( *mTetraMesh->getSubMesh(0)->mFaces );



		if ( mCollisionsConfig & fCollision::VF_SS )
		{
			mDbvtFaces = 
				BufferManager::getSingleton().createBuffer<DbvtNode*>(mNbFaces);

			mDbvtFacesId = 
				BufferManager::getSingleton().createBuffer<ushort>(mNbFaces);
		}
		

	}


	void CollisionSoftBody::buildDynamicVertexTree(void)
	{

		mVertexDynamicTree.clear();

		float margin = m_collisionShape->getMargin(); 

		const Vector3* vertexbuf = 
			mVertex->lockForRead<Vector3>();

		DbvtNode** dbvtnodebuf = 
			mDbvtNodes->lockForWrite<DbvtNode*>();

		ushort* dbvtnodeIdbuf = mDbvtNodesId->lockForWrite<ushort>();

		for(int i=0,ni= mNbVertices; i<ni;++i)
		{	
			//CollisionNode&	n=CollisionNodes[i];
			//ZeroMemory( &n, sizeof(CollisionNode) );
			//n.node = &m_NodesPtr[i];
			//n.IdNode = i;
			*dbvtnodeIdbuf = i;
			*dbvtnodebuf++ = static_cast<DbvtNode*>( mVertexDynamicTree.insert(
				btDbvtVolume::FromCR( 
				btVector3( (*vertexbuf)[0], (*vertexbuf)[1], (*vertexbuf)[2] ),
				margin), dbvtnodeIdbuf /*&n*/ ) );
			//CollisionNode* cn = (CollisionNode*)n.leaf->data;
			++vertexbuf;
			++dbvtnodeIdbuf;
		}

		mDbvtNodes->unlock();
		mDbvtNodesId->unlock();
		mVertex->unlock();

		mBounds.mAabbMin = Vector3(0);
		mBounds.mAabbMax = Vector3(0);

		updateBounds();

		if ( mCollisionsConfig & fCollision::VF_SS )
		{
			buildDynamicFaceTree();
		}
	}


	void CollisionSoftBody::buildDynamicFaceTree(void)
	{

		if ( mCollisionsConfig & fCollision::VF_SS )
		{

			mFaceDynamicTree.clear();

			const Vector3* vertexbuf = mVertex->lockForRead<Vector3>();
			const ushort* facebuf = mFaces->lockForRead<ushort>();

			ushort* dbvtfaceIdbuf = mDbvtFacesId->lockForWrite<ushort>();
			DbvtNode** dbvtfacebuf = 
				mDbvtFaces->lockForWrite<DbvtNode*>();
			
			int nbFaces = mNbFaces;
			int faceid = 0;
			while ( nbFaces-- > 0 )
			{
				const Vector3& v1 = vertexbuf[*facebuf++];
				const Vector3& v2 = vertexbuf[*facebuf++];
				const Vector3& v3 = vertexbuf[*facebuf++];

				*dbvtfaceIdbuf = faceid++;
				*dbvtfacebuf++ = static_cast<DbvtNode*>( mFaceDynamicTree.insert( 
					VolumeOf( btVector3(v1[0],v1[1],v1[2]), 
					btVector3(v2[0],v2[1],v2[2]),
					btVector3(v3[0],v3[1],v3[2]), 0),
					&dbvtfaceIdbuf ) );
				dbvtfaceIdbuf++;
			}

			mDbvtFaces->unlock();
			mDbvtFacesId->unlock();
			mFaces->unlock();
			mVertex->unlock();
		}

	}

	//Bool CollisionSoftBody::InitializeCollision(void)
	//{

	//	bool ret = false;

	//	m_worldTransform.setIdentity();
	//	//m_CollisionObject.setCollisionShape( new btSoftBodyCollisionShape(this) );
	//	//setCollisionShape( new btSoftBodyCollisionShape(this) );

	//	m_collisionShape = NULL;
	//	m_collisionShape = new btSoftBodyCollisionShape(this);

	//	mVertexDynamicTree.clear();

	//	//setCollisionShape(new btSoftBodyCollisionShape(this));	
	//	m_collisionShape->setMargin(0.5);
	//	//m_collisionShape->setMargin(0.25f);
	//	///* Nodes			*/ 
	//	//const btScalar		margin=getCollisionShape()->getMargin();
	//	//m_nodes.resize(node_count);

	//	CollisionNodes = new CollisionNode[m_Nb_CollisionNodes];

	//	float margin = m_collisionShape->getMargin(); 
	//	for(int i=0,ni= m_Nb_CollisionNodes; i<ni;++i)
	//	{	
	//		CollisionNode&	n=CollisionNodes[i];
	//		ZeroMemory( &n, sizeof(CollisionNode) );
	//		//n.node = &m_NodesPtr[i];
	//		n.IdNode = i;
	//		n.leaf	=	static_cast<DbvtNode*>( mVertexDynamicTree.insert(
	//			btDbvtVolume::FromCR( 
	//			btVector3( m_Nodes[i].getX(), m_Nodes[i].getY(), m_Nodes[i].getZ() ),
	//			margin), &n ) );
	//		//CollisionNode* cn = (CollisionNode*)n.leaf->data;
	//	}

	//	mBounds.mAabbMin = Vector3(0);
	//	mBounds.mAabbMax = Vector3(0);

	//	updateBounds();


	//	mCollisionsConfig = fCollision::Default; //FemSoftBody::fCollision::CL_SS + FemSoftBody::fCollision::CL_RS;

	//	m_internalType = CO_SOFT_BODY;


	//	//ClustersDynamicTree.clear();
	//	//GenerateClusters(64);


	//	//btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	//	//btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);
	//	//btVector3	worldAabbMin(-1000,-1000,-1000);
	//	//btVector3	worldAabbMax(1000,1000,1000);
	//	//btAxisSweep3*	broadphase = new btAxisSweep3(worldAabbMin,worldAabbMax);

	//	//btCollisionWorld* collisionWorld = new btCollisionWorld(dispatcher,broadphase,collisionConfiguration);
	//	//collisionWorld->addCollisionObject(this, btBroadphaseProxy::DefaultFilter,
	//	//	btBroadphaseProxy::AllFilter );


	//	return ret;
	//}


	void CollisionSoftBody::updateBounds()
	{
		if(mVertexDynamicTree.m_root)
		{
			const btVector3&	mins = mVertexDynamicTree.m_root->volume.Mins();
			const btVector3&	maxs = mVertexDynamicTree.m_root->volume.Maxs();
			const float		csm=  getCollisionShape()->getMargin();
			const btVector3		mrg= btVector3(	csm,
				csm,
				csm)*1; // ??? to investigate...
			mBounds.mAabbMin = mins-mrg;
			mBounds.mAabbMax = maxs+mrg;
			if(0!= getBroadphaseHandle())
			{					
				mCollisionWorldInfo->m_broadphase->setAabb(	getBroadphaseHandle(),
					btVector3(mBounds.mAabbMin.getX(), mBounds.mAabbMin.getY(), mBounds.mAabbMin.getZ() ) ,
					btVector3(mBounds.mAabbMax.getX(), mBounds.mAabbMax.getY(), mBounds.mAabbMax.getZ() ) ,
					mCollisionWorldInfo->m_dispatcher);
			}
		}
		else
		{
			mBounds.mAabbMin = mBounds.mAabbMax =  Vector3(0,0,0);
		}		
	}


	void CollisionSoftBody::updateCollisionShape(void)
	{

		/* Bounds				*/ 
		//updateBounds();

		const Vector3* nodeVelBuf = mVertexVels->lockForRead<Vector3>();
		const Vector3* nodePosBuf = mVertex->lockForRead<Vector3>();
		DbvtNode* const* dbvtNodeBuf = mDbvtNodes->lockForRead<DbvtNode*>();

		for( int i=0, ni=mNbVertices;i<ni;++i)
		{
			//CollisionSoftBody::CollisionNode&	n= CollisionNodes[i];
			//DbvtNode* dbvtNode = dbvtNodeBuf[i];
			const Vector3& vel = nodeVelBuf[i];
			//fem_soft_body->mVertexDynamicTree.update(	n.leaf,
			//	btDbvtVolume::FromCR( btVector3(n.node->getX(),n.node->getY(),n.node->getZ()), fem_soft_body->m_collisionShape->getMargin() ),
			//	btVector3(vel.getX(), vel.getY(), vel.getZ()) * 1.0f/* margine */,
			//	fem_soft_body->m_collisionShape->getMargin()/* margine */ );

			const Vector3& position = nodePosBuf[i];
			//n.IdNode = i;

			//softbody->mVertexDynamicTree.update(	n.leaf,
			//	btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->m_collisionShape->getMargin() ),
			//	btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
			//	softbody->m_collisionShape->getMargin()/* margine */ );

			mVertexDynamicTree.update(	/*n.leaf*/ *dbvtNodeBuf++,
				btDbvtVolume::FromCR( btVector3(position[0],position[1],position[2]), m_collisionShape->getMargin() ),
				btVector3(vel[0], vel[1], vel[2]) /* margine */,
				m_collisionShape->getMargin()/* margine */ );

		}

		mDbvtNodes->unlock();
		mVertex->unlock();
		mVertexVels->unlock();
		
		/* Optimize dbvt's		*/
		mVertexDynamicTree.optimizeIncremental(1);
		
		/* Faces				*/ 
		if ( mCollisionsConfig & fCollision::VF_SS )
		//if( fem_soft_body->mFaceDynamicTree != NULL )
		{
		
			const Vector3* vertexbuf = mVertex->lockForRead<Vector3>();
			const ushort* facebuf = mFaces->lockForRead<ushort>();
			DbvtNode* const* dbvtFaceBuf = mDbvtFaces->lockForRead<DbvtNode*>();


			for(int i=0;i< mNbFaces;++i)
			{
				const Vector3& v1 = vertexbuf[*facebuf++];
				const Vector3& v2 = vertexbuf[*facebuf++];
				const Vector3& v3 = vertexbuf[*facebuf++];

				const btVector3& btv1 = btVector3(v1[0],v1[1],v1[2]);
				const btVector3& btv2 = btVector3(v2[0],v2[1],v2[2]);
				const btVector3& btv3 = btVector3(v3[0],v3[1],v3[2]);
				
				mFaceDynamicTree.update(	*dbvtFaceBuf++,
					VolumeOf( btv1, btv2, btv3,  m_collisionShape->getMargin() ),
					(btv1 + btv2 + btv3 ) /3 /**m_sst.velmrg*/,
					m_collisionShape->getMargin() * 0.25f /* m_sst.updmrg*/);

			}

			mDbvtFaces->unlock();
			mFaces->unlock();
			mVertex->unlock();

			/* Optimize dbvt's		*/
			mFaceDynamicTree.optimizeIncremental(1);
		}
		

	}

	Vector3 CollisionSoftBody::getVertexNormal(int vertexId) const
	{
		Vector3 normal = *mVertexNormals->lockForRead<Vector3>(vertexId);
		mVertexNormals->unlock();
		return normal;
	}

	const BufferSharedPtr<Vector3>& CollisionSoftBody::getVertexNormalBuffer(void) const
	{
		return mVertexNormals;
	}

	void CollisionSoftBody::setPosition(const Vector3& newpos )
	{
		SimdVector3 pivot(0.0f);
		const SimdVector3* cnodePosBuf = mVertex->lockForRead<SimdVector3>();

		int nbnodes = mNbVertices;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodePosBuf++;
		}
		mVertex->unlock();
		pivot /= (float)mNbVertices;
		pivot = SimdVector3(newpos[0],newpos[1],newpos[2]) - pivot;
		
		SimdVector3* nodePosBuf = mVertex->lockForWrite<SimdVector3>();
		nbnodes = mNbVertices;
		while ( nbnodes-- > 0 )
		{
			*nodePosBuf++ += pivot;
		}
		mVertex->unlock();

	}

	void CollisionSoftBody::setOrientation(const Quat& newor)
	{
		SimdVector3 pivot(0.0f);
		const SimdVector3* cnodePosBuf = mVertex->lockForRead<SimdVector3>();

		int nbnodes = mNbVertices;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodePosBuf++;
		}
		mVertex->unlock();
		pivot /= (float)mNbVertices;

		Quat orientation = normalize(newor);
		Quat q = orientation - mOrientation;
		mOrientation = orientation;

		SimdVector3* nodePosBuf = mVertex->lockForWrite<SimdVector3>();
		nbnodes = mNbVertices;
		while ( nbnodes-- > 0 )
		{
			*nodePosBuf++ = rotate( reinterpret_cast<SimdQuat&>(q), (*nodePosBuf - pivot) );
		}
		mVertex->unlock();
	}

	void CollisionSoftBody::setVelocity(const Vector3& newvel )
	{

	}

	void CollisionSoftBody::reset(void)
	{

		mDynamicContactArray.resize(0);
		mStaticContactArray.resize(0);
		mSoftContactArray.resize(0);

		buildDynamicVertexTree();
	}


	void CollisionSoftBody::defaultCollisionHandler(btCollisionObject* pco)
	{
		switch( mCollisionsConfig & fCollision::RVSmask)
		{
		case	fCollision::SDF_RS:
			{
				btSoftColliders::CollideSDF_RS	docollide;	
				// da riguardare: sfruttare l'upcast sarebbe meglio
				//RigidBodyBase* rigid_body = RigidBodyBase::upcast(pco);
				RigidBody* rigid_body = static_cast<RigidBody*>(pco);

				//const btTransform	wtr=prb->getInterpolationWorldTransform();
				//const btTransform	ctr=prb->getWorldTransform();
				//const btScalar		timemargin=(wtr.getOrigin()-ctr.getOrigin()).length();
				const btScalar		basemargin=getCollisionShape()->getMargin();
				btVector3			mins;
				btVector3			maxs;
				btDbvtVolume		volume;
				pco->getCollisionShape()->getAabb(	pco->getWorldTransform(),
					mins,
					maxs);
				volume=btDbvtVolume::FromMM(mins,maxs);
				volume.Expand(btVector3(basemargin,basemargin,basemargin));		
				docollide.psb		=	this;
				docollide.ColObject =  pco;
				docollide.prb		=	rigid_body;
				docollide.dynmargin	=	basemargin; // +timemargin;
				docollide.stamargin	=	basemargin;
				mVertexDynamicTree.collideTV( mVertexDynamicTree.m_root,volume,docollide);
			}
			break;
		case	fCollision::CL_RS:
			//{
			//	btSoftColliders::CollideCL_RS	collider;
			//	collider.Process(this,btRigidBody::upcast(pco));
			//}
			break;
		}
	}


	void CollisionSoftBody::defaultCollisionHandler( CollisionSoftBody* psb)
	{
		//const int cf = m_cfg.collisions & psb->m_cfg.collisions;
		const int cf = mCollisionsConfig & psb->mCollisionsConfig;

		switch(cf&fCollision::SVSmask)
		{
		case	fCollision::CL_SS:
			{
				//btSoftColliders::CollideCL_SS	docollide;
				//docollide.Process(this,psb);
			}
			break;
		case	fCollision::VF_SS:
			//only self-collision for Cluster, not Vertex-Face yet
			if (this!=psb)
			{
				btSoftColliders::CollideVF_SS	docollide;
				/* common					*/ 
				docollide.mrg=	getCollisionShape()->getMargin()+
					psb->getCollisionShape()->getMargin();
				/* psb0 nodes vs psb1 faces	*/ 
				docollide.psb[0]=this;
				docollide.psb[1]=psb;
				docollide.psb[0]->getNodeDynamicTree().collideTT(	
					docollide.psb[0]->getNodeDynamicTree().m_root,
					docollide.psb[1]->getFaceDynamicTree().m_root,
					docollide);
				/* psb1 nodes vs psb0 faces	*/ 
				docollide.psb[0]=psb;
				docollide.psb[1]=this;
				docollide.psb[0]->getNodeDynamicTree().collideTT(	
					docollide.psb[0]->getNodeDynamicTree().m_root,
					docollide.psb[1]->getFaceDynamicTree().m_root,
					docollide);
			}
			break;
		
		case fCollision::SDF_SS:
			if (this!=psb)
			{
				btSoftColliders::CollideSDF_SS	docollide;	
				docollide.psb[0] = this;
				docollide.psb[1] = psb;
				docollide.mrg = getCollisionShape()->getMargin() 
					+ psb->getCollisionShape()->getMargin();
				docollide.psb[0]->getNodeDynamicTree().collideTT(	
					docollide.psb[0]->getNodeDynamicTree().m_root,
					docollide.psb[1]->getNodeDynamicTree().m_root,
					docollide);

				//docollide.psb[1] = this;
				//docollide.psb[0] = psb;
				//docollide.psb[0]->getNodeDynamicTree().collideTT(	
				//	docollide.psb[0]->getNodeDynamicTree().m_root,
				//	docollide.psb[1]->getNodeDynamicTree().m_root,
				//	docollide);
			}
			break;
		}
	}


	Bool CollisionSoftBody::CheckContact( btCollisionObject* collObj, const ushort& IdxNode, /*const SimdVector3& pos,*/
		float margin, CollisionSoftBody::ContactInfo& contact_info) const
	{
		bool ret = false;

		const Vector3* nodebuf = mVertex->lockForRead<Vector3>(IdxNode);
		btVector3 x = btVector3(nodebuf->getX(), nodebuf->getY(), nodebuf->getZ() );
		mVertex->unlock();

		btVector3			normal;
		btCollisionShape*	shape=  collObj->getCollisionShape(); // prb->getCollisionShape();
		const btTransform&	wtr= collObj->getWorldTransform(); // getInterpolationWorldTransform();
		btVector3 p = wtr.invXform( x );

		btScalar distance = mCollisionWorldInfo->m_sparsesdf.Evaluate( 
			p,
			shape,
			normal,
			margin);

		if( distance <0 )
		{
			contact_info.m_body		= static_cast<CollisionObject*>(collObj);
			contact_info.m_normal	=	-( wtr.getBasis()*normal );
			contact_info.m_offset	= -dot(	contact_info.m_normal,
				contact_info.m_normal * distance );
			//x - contact_info.m_normal * distance );
			ret = true;
		}

		return ret;
	}



	//
	//bool CollisionSoftBody::rayTest(const btVector3& rayFrom,	const btVector3& rayTo,
	//	sRayCast& results)
	//{
		///*if(m_faces.size()&&m_fdbvt.empty()) 
		//	initializeFaceTree();*/

		//results.sbody	=	this;
		//results.fraction = 1.f;
		//results.feature	=	eFeature::None;
		//results.index	=	-1;

		//return(rayTest(rayFrom,rayTo,results.fraction,results.feature,results.index,false)!=0);
	//	return false;
	//}



	//int CollisionSoftBody::rayTest(const btVector3& rayFrom,const btVector3& rayTo,
	//	btScalar& mint,eFeature::_& feature,int& index,bool bcountonly) const
	//{
		//int	cnt=0;
		//if(bcountonly||m_fdbvt.empty())
		//{/* Full search	*/ 
		//	btVector3 dir = rayTo-rayFrom;
		//	dir.normalize();

		//	for(int i=0,ni=m_faces.size();i<ni;++i)
		//	{
		//		const btSoftBody::Face&	f=m_faces[i];

		//		const btScalar			t=RayFromToCaster::rayFromToTriangle(	rayFrom,rayTo,dir,
		//			f.m_n[0]->m_x,
		//			f.m_n[1]->m_x,
		//			f.m_n[2]->m_x,
		//			mint);
		//		if(t>0)
		//		{
		//			++cnt;
		//			if(!bcountonly)
		//			{
		//				feature=btSoftBody::eFeature::Face;
		//				index=i;
		//				mint=t;
		//			}
		//		}
		//	}
		//}
		//else
		//{/* Use dbvt	*/ 
		//	RayFromToCaster	collider(rayFrom,rayTo,mint);

		//	btDbvt::rayTest( mVertexDynamicTree.m_root,rayFrom,rayTo,collider);
		//	//if(collider.m_face)
		//	if(collider.m_vertexId)
		//	{
		//		mint=collider.m_mint;
		//		feature= CollisionSoftBody::eFeature::Node;
		//		//index=(int)(collider.m_node- mVertex->lockForRead(0));
		//		//index=(int)(collider.m_face-&m_faces[0]);
		//		index = static_cast<int>(*collider.m_vertexId);
		//		cnt=1;
		//	}
		//}
		//return(cnt);
	//	return 0;
	//}


	////  RayFromToCaster

	//CollisionSoftBody::RayFromToCaster::RayFromToCaster(const btVector3& rayFrom,const btVector3& rayTo,btScalar mxt)
	//{
	//	m_rayFrom = rayFrom;
	//	m_rayNormalizedDirection = (rayTo-rayFrom);
	//	m_rayTo = rayTo;
	//	m_mint	=	mxt;
	//	//m_face	=	0;
	//	m_vertexId = 0;
	//	m_tests	=	0;
	//}

	////
	//void CollisionSoftBody::RayFromToCaster::Process(const btDbvtNode* leaf)
	//{
	//	//CollisionSoftBody::Face&	f=*(CollisionSoftBody::Face*)leaf->data;
	//	
	//	m_vertexId = (ushort*)leaf->data;
	//	//const Vector3 vertex = *mVertex->lockForRead(vindex);
	//	//mVertex->unlock();

	//	//const btScalar t = rayFromToSphere( 
	//	//	m_rayFrom,	m_rayTo, m_rayNormalizedDirection,
	//	//	vertex, m_mint);

	//	//const btScalar		t=rayFromToTriangle(	m_rayFrom,m_rayTo,m_rayNormalizedDirection,
	//	//	f.m_n[0]->m_x,
	//	//	f.m_n[1]->m_x,
	//	//	f.m_n[2]->m_x,
	//	//	m_mint);
	//	//if((t>0)&&(t<m_mint)) 
	//	//{ 
	//	//	m_mint=t;m_face=&f; 
	//	//}
	//	++m_tests;
	//}

	////
	//btScalar CollisionSoftBody::RayFromToCaster::rayFromToTriangle(	const btVector3& rayFrom,
	//	const btVector3& rayTo,
	//	const btVector3& rayNormalizedDirection,
	//	const btVector3& a,
	//	const btVector3& b,
	//	const btVector3& c,
	//	btScalar maxt)
	//{
	//	static const btScalar	ceps=-SIMD_EPSILON*10;
	//	static const btScalar	teps=SIMD_EPSILON*10;

	//	const btVector3			n=btCross(b-a,c-a);
	//	const btScalar			d=btDot(a,n);
	//	const btScalar			den=btDot(rayNormalizedDirection,n);
	//	if(!btFuzzyZero(den))
	//	{
	//		const btScalar		num=btDot(rayFrom,n)-d;
	//		const btScalar		t=-num/den;
	//		if((t>teps)&&(t<maxt))
	//		{
	//			const btVector3	hit=rayFrom+rayNormalizedDirection*t;
	//			if(	(btDot(n,btCross(a-hit,b-hit))>ceps)	&&			
	//				(btDot(n,btCross(b-hit,c-hit))>ceps)	&&
	//				(btDot(n,btCross(c-hit,a-hit))>ceps))
	//			{
	//				return(t);
	//			}
	//		}
	//	}
	//	return(-1);
	//}

	//btScalar CollisionSoftBody::RayFromToCaster::rayFromToSphere( const btVector3& rayFrom,
	//	const btVector3& rayTo,
	//	const btVector3& center,
	//	btScalar maxt )
	//{
	//	const btVector3& ab = rayTo - rayFrom;
	//	const btVector3& ac = center - rayFrom;
	//	const btVector3& bc = center - rayTo;
	//	const float& e = ac.dot(ab);
	//	const float& f = ab.dot(ab);
	//	return ac.dot(ac) - e*e / f;
	//}

}; // namespace SimStep