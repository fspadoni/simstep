#ifndef SimStep_CollisionSoftBody_h__
#define SimStep_CollisionSoftBody_h__



//#include "Core/SoftBody.h"
#include "World/CollisionObject.h"

// collision 
#include "btBulletCollisionCommon.h"

#include "Core/PhysicsClasses.h"

#include "Core/CollisionShape.h"
#include "Math/Math.h"

#include "Memory/AlignedObject.h"
#include <Utilities/SimdMemoryBuffer.h>
#include <Core/TetraMesh.h>

#include "Utilities/Common.h"

#include "FemSoftBody/FemElements.h"

#include <vector>

namespace SimStep {


	struct SoftBodyCollisionWorldInfo;



	class _SimStepExport CollisionSoftBody : public CollisionObject //, public SoftBody //  , public AlignedObject16
	{

		friend class World;

	private:

		// define private
		CollisionSoftBody( const CollisionSoftBody& collsoftbody ) {};

		const CollisionSoftBody& operator= ( const CollisionSoftBody& collsoftbody ) { return *this;}

	public:


		CollisionSoftBody(const SoftBodyParams& params, const DataValuePairList* otherparams = 0 );

		virtual ~CollisionSoftBody();


		static const CollisionSoftBody*	downcast(const btCollisionObject* colObj);
		
		static CollisionSoftBody*	downcast(btCollisionObject* colObj);


		virtual void updateCollisionShape(void);

		// must be  serial !!!
		void updateBounds(void);


		// collision
		inline int getNbVertices(void) const;

		inline Vector3 getVertex(int vertexId) const;

		inline Vector3 getVertexVel(int vertexId) const;

		virtual Vector3 getVertexNormal(int vertexId) const;

		inline void getAabbBounds( Bounds3& aabbBounds ) const;

		inline TriangleFace getFace(int nodeId) const;

		inline const BufferSharedPtr<Vector3>& getVertexBuffer(void) const;

		virtual const BufferSharedPtr<Vector3>& getVertexNormalBuffer(void) const;

		inline const BufferSharedPtr<ushort>& getFacesBuffer(void) const;


		virtual void setPosition(const Vector3& newpos );
		
		virtual void setOrientation(const Quat& newor );

		virtual void setVelocity(const Vector3& newvel );

		virtual void reset(void);


		struct	ContactInfo
		{
			CollisionObject* m_body;
			//btRigidBody*	m_body;		/* Rigid body			*/ 
			Vector3		m_normal;	/* Outward normal		*/ 
			float		m_offset;	/* Offset from origin	*/ 
		};

		struct	RigidContact
		{
			ContactInfo		m_ContactInfo;			// Contact infos
			RigidBody*		m_RigidBody;
			//CollisionNode*	m_node;			// Owner node
			int m_IdNode;
			Vector3		m_RelAnchorPosition; // Relative anchor position
		};

		struct	SoftContact
		{
			ContactInfo		m_ContactInfo;			// Contact infos
			Vector3		m_normal;
			CollisionSoftBody*		m_SoftBody;
			//CollisionNode*	m_node;			// Owner node
			int m_IdFace;
			int m_IdNode;
			Vector3		m_RelAnchorPosition; // Relative anchor position
			float m_Depth;
		};


		/////eFeature
		//struct	eFeature { enum _ {
		//	None,
		//	Node,
		//	Link,
		//	Face,
		//	END
		//};};

		///* sRayCast		*/ 
		//struct sRayCast
		//{
		//	CollisionSoftBody*	sbody;		/// soft body
		//	eFeature::_	feature;	/// feature type
		//	int			index;		/// feature index
		//	float	fraction;		/// time of impact fraction (rayorg+(rayto-rayfrom)*fraction)
		//};

		/////Ray casting using rayFrom and rayTo in worldspace, (not direction!)
		//bool rayTest(const btVector3& rayFrom,const btVector3& rayTo,	sRayCast& results);


		inline std::vector<RigidContact>& getDynamicContactArray(void);

		inline std::vector<RigidContact>& getStaticContactArray(void);
		inline std::vector<SoftContact>& getSoftContactArray(void);



	protected:

	
		TetraMeshPtr mTetraMesh;

		BufferSharedPtr<Vector3> mVertex;
		BufferSharedPtr<Vector3> mVertexVels;
		BufferSharedPtr<Vector3> mVertexNormals;
		BufferSharedPtr<DbvtNode*> mDbvtNodes; // DbvtNode
		BufferSharedPtr<ushort> mDbvtNodesId;

		BufferSharedPtr<ushort> mFaces;
		BufferSharedPtr<DbvtNode*> mDbvtFaces;
		BufferSharedPtr<ushort> mDbvtFacesId;

		int mNbVertices;
		int mNbFaces;
		
		Quat mOrientation;
		
	private:

		// fCollision
		int	mCollisionsConfig;		// Collisions flags

		SoftBodyCollisionWorldInfo*	mCollisionWorldInfo;

		Bounds3 mBounds;

		btDbvt mVertexDynamicTree;
		btDbvt mFaceDynamicTree;	

		std::vector<RigidContact> mDynamicContactArray;
		std::vector<RigidContact> mStaticContactArray;
		std::vector<SoftContact> mSoftContactArray;


		// friend structure to get access to CheckContact protected function
		friend struct btSoftColliders; //::CollideSDF_RS;



	protected:


		//void updateBounds(void);

		inline SoftBodyCollisionWorldInfo* getCollisionWorldInfo();

		inline void setCollisionWorldInfo( SoftBodyCollisionWorldInfo* CollisionWorldInfo);

		virtual void initializeBuffers(void);

		void buildDynamicVertexTree(void);

		void buildDynamicFaceTree(void);

		
		//int	rayTest(const btVector3& rayFrom,const btVector3& rayTo,
		//	btScalar& mint,eFeature::_& feature,int& index,bool bcountonly) const;


	public:
		// collision
		//_______________________________________________________
		//
		// Collision: Bullet Integration 
		//_______________________________________________________

		///fCollision
		
		struct fCollision {
			enum _ {
				RVSmask	=	0x000f,	///Rigid versus soft mask
				SDF_RS	=	0x0001,	///SDF based rigid vs soft
				CL_RS	=	0x0002, ///Cluster vs convex rigid vs soft

				SVSmask	=	0x00f0,	///Rigid versus soft mask		
				VF_SS	=	0x0010,	///Vertex vs face soft vs soft handling
				CL_SS	=	0x0020, ///Cluster vs cluster soft vs soft handling
				SDF_SS	=	0x0040,	///SDF based soft vs soft
				/* presets	*/ 
				Default	=	SDF_RS,
				END
			};
		};
		



	private:

		Bool CheckContact( btCollisionObject* collObj, const ushort& IdxNode, /*const SimdVector3& pos,*/
			float margin, CollisionSoftBody::ContactInfo& contact_info) const ;
	

		inline btDbvt& getNodeDynamicTree();
		inline btDbvt& getFaceDynamicTree();



		/* defaultCollisionHandlers												*/ 
		void defaultCollisionHandler(btCollisionObject* pco);
		void defaultCollisionHandler( CollisionSoftBody* psb);


		// friend class to get access to defaultCollisionHandler protected function
		friend class btSoftSoftCollisionAlgorithm;
		friend class btSoftRigidCollisionAlgorithm;
		


		///// RayFromToCaster takes a ray from, ray to (instead of direction!)
		//struct	RayFromToCaster : btDbvt::ICollide
		//{
		//	btVector3			m_rayFrom;
		//	btVector3			m_rayTo;
		//	btVector3			m_rayNormalizedDirection;
		//	btScalar			m_mint;
		//	//Face*				m_face;
		//	ushort*			m_vertexId;
		//	int					m_tests;
		//	RayFromToCaster(const btVector3& rayFrom,const btVector3& rayTo,btScalar mxt);
		//	void					Process(const btDbvtNode* leaf);

		//	static inline btScalar	rayFromToTriangle(const btVector3& rayFrom,
		//		const btVector3& rayTo,
		//		const btVector3& rayNormalizedDirection,
		//		const btVector3& a,
		//		const btVector3& b,
		//		const btVector3& c,
		//		btScalar maxt=SIMD_INFINITY);

		//	static inline btScalar rayFromToSphere(	 const btVector3& rayFrom,
		//		const btVector3& rayTo,
		//		const btVector3& center,
		//		btScalar radius ) ;

		//};


	};


	inline int CollisionSoftBody::getNbVertices(void) const 
	{
		return mNbVertices;
	}

	inline Vector3 CollisionSoftBody::getVertex(int vertexId) const 
	{
		Vector3 vertex = *mVertex->lockForRead<Vector3>(vertexId);
		mVertex->unlock();
		return vertex;
	}

	inline Vector3 CollisionSoftBody::getVertexVel(int vertexId) const 
	{
		Vector3 vertexvel = *mVertexVels->lockForRead<Vector3>(vertexId);
		mVertex->unlock();
		return vertexvel;
	}

	

	inline void CollisionSoftBody::getAabbBounds( Bounds3& aabbBounds) const
	{
		aabbBounds.mAabbMin = mBounds.mAabbMin;
		aabbBounds.mAabbMax = mBounds.mAabbMax;
	}

	inline TriangleFace CollisionSoftBody::getFace(int nodeId) const
	{
		const TriangleFace* facebuf = mFaces->lockForRead<TriangleFace>(nodeId);
		mFaces->unlock();
		return *facebuf;
	}

	inline const BufferSharedPtr<Vector3>& CollisionSoftBody::getVertexBuffer(void) const
	{
		return mVertex;
	}

	inline const BufferSharedPtr<ushort>& CollisionSoftBody::getFacesBuffer(void) const
	{ 
		return mFaces;
	}


	inline std::vector<CollisionSoftBody::RigidContact>& CollisionSoftBody::getDynamicContactArray(void)
	{ 
		return mDynamicContactArray;
	}

	inline std::vector<CollisionSoftBody::RigidContact>& CollisionSoftBody::getStaticContactArray(void)
	{ 
		return mStaticContactArray;
	}

	inline std::vector<CollisionSoftBody::SoftContact>& CollisionSoftBody::getSoftContactArray(void)
	{ 
		return mSoftContactArray;
	}


	inline SoftBodyCollisionWorldInfo* CollisionSoftBody::getCollisionWorldInfo()
	{ 
		return mCollisionWorldInfo;
	}

	inline void CollisionSoftBody::setCollisionWorldInfo( SoftBodyCollisionWorldInfo* CollisionWorldInfo) 
	{ 
		mCollisionWorldInfo = CollisionWorldInfo;
	}


	inline btDbvt& CollisionSoftBody::getNodeDynamicTree()
	{ 
		return mVertexDynamicTree;
	}

	inline btDbvt& CollisionSoftBody::getFaceDynamicTree() 
	{
		return mFaceDynamicTree; 
	}


}; // namespace Physics



#endif // SimStep_CollisionSoftBody_h__