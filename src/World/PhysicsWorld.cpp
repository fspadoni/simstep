#include "World/PhysicsWorld.h"

#include "World/RigidBody.h"

#include "Core/SoftBody.h"

#include "World/CollisionObject.h"

#include "Utilities/StopWatch.h"
#include "Utilities/String.h"

#include "Core/SceneManager.h"
//#include "Core/StepListener.h"

//#include "../../bullet-2.72/src/btBulletCollisionCommon.h"
//#include "Collision/btSoftBodyRigidBodyCollisionConfiguration.h"

//#include "../../bullet-2.72/src/LinearMath/btPoolAllocator.h"

//#include <Base/BaseTypes.h>
//#include <Interfaces/Interface.h>
//#include <Interfaces/TaskManager.h>

// dxBody
// dxWorld
#include "src/objects.h"

//#include <Windows.h>
#include <algorithm>
#include "LinearMath/btPoolAllocator.h"

static const int sizeof_dxBody	= 384;
static const int max_nb_dxBody	= 2048;

static btPoolAllocator s_PoolAllocator( sizeof_dxBody, max_nb_dxBody );

static void*  OdeAlloc( size_t size)
{
	void* ptr;

	if ( size <= sizeof_dxBody )
	{
		ptr = s_PoolAllocator.allocate( size );
	}
	else
	{	// ode arena needs to allocate dOBSTACK_ARENA_SIZE bytes
		int n = ( size + sizeof_dxBody -1 )/ sizeof_dxBody;
		ptr = s_PoolAllocator.allocate( sizeof_dxBody );
		for (int i=1; i<n; i++)
		{
			s_PoolAllocator.allocate( sizeof_dxBody );
		}
	}

	return ptr;
}

static void * OdeRealloc(void *ptr, size_t oldsize, size_t newsize)
{
	s_PoolAllocator.freeMemory( ptr );
	return s_PoolAllocator.allocate( newsize );
}

static void OdeFree(void *ptr, size_t size)
{

	if ( size <= sizeof_dxBody )
	{
		s_PoolAllocator.freeMemory( ptr );
		//ptr = s_PoolAllocator.allocate( size );
	}
	else
	{	// ode arena needs to allocate dOBSTACK_ARENA_SIZE bytes
		int n = ( size + sizeof_dxBody -1 )/ sizeof_dxBody;
		s_PoolAllocator.freeMemory( ptr );
		//ptr = s_PoolAllocator.allocate( sizeof_dxBody );
		unsigned char* p = (unsigned char*)ptr + n*sizeof_dxBody;
		for (int i=n; i>0; i--)
		{
			s_PoolAllocator.freeMemory( p );
			p -= sizeof_dxBody;
			//s_PoolAllocator.allocate( sizeof_dxBody );
		}
	}

	return;	
}


#include "World/RigidBody.h"

namespace SimStep {


	//PhysicsWorldParam::PhysicsWorldParam() 
	//	: CollisionWorldParams()
	//	, DynamicsWorldParams()
	//	, mName(StringUtil::BLANK)
	//	, mInitializeScheduler(false)
	//{

	//}

	//PhysicsWorldParam::~PhysicsWorldParam() 
	//{

	//}


	PhysicsWorld::PhysicsWorld( const PhysicsWorldParam& physicsWorldParams,
		SceneManager* sceneMgr )
		: CollisionWorld( static_cast<const CollisionWorldParams&>(physicsWorldParams) )
		, DynamicsWorld( static_cast<const DynamicsWorldParams&>(physicsWorldParams) )
		, mSceneMgr(sceneMgr)
		, mSimType(physicsWorldParams.simType)
	{

	}

	PhysicsWorld::PhysicsWorld( const CollisionWorldParams& collWorldParams, 
		const DynamicsWorldParams& dynamicsWorldParams, SceneManager* sceneMgr )
		: CollisionWorld(collWorldParams), DynamicsWorld(dynamicsWorldParams)
		, mSceneMgr(sceneMgr)
	{

	}

	PhysicsWorld::~PhysicsWorld()
	{

	}


	const String& PhysicsWorld::getName(void) const
	{
		static String strName( "Default World");
		return strName;
	}

	void PhysicsWorld::release(void)
	{

	}

	CollisionWorld*	PhysicsWorld::getCollisionWorld()
	{
		return static_cast<CollisionWorld*>(this);
	}

	DynamicsWorld* PhysicsWorld::getDynamicsWorld()
	{
		return static_cast<DynamicsWorld*>(this);
	}



	void PhysicsWorld::addCollisionObject(  CollisionObject* collObject,
		short int collisionFilterGroup,	short int collisionFilterMask )
	{
		btCollisionWorld::addCollisionObject(collObject, collisionFilterGroup,
			collisionFilterMask );
		//CollisionWorld::add(collObject);
	}
	Bool PhysicsWorld::removeCollisionObject( CollisionObject* collObject )
	{
		Bool ret = CollisionWorld::remove(collObject);
		return ret;
	}

	void PhysicsWorld::addRigidBody(RigidBody* rbody)
	{
		//CollisionWorld::add(static_cast<CollisionObject*>(rbody) );
		DynamicsWorld::add( rbody );
		//CollisionWorld::add( rbody );
		//DynamicsWorld::add(  rbody );
	}
	Bool PhysicsWorld::removeRigidBody( RigidBody* rbody )
	{
		Bool ret = DynamicsWorld::remove( rbody );
		
		if ( static_cast<CollisionObject*>(rbody) != NULL )
		{
			CollisionWorld::remove( rbody );
		}
		return ret;
	}

	void PhysicsWorld::addSoftBody(SoftBody* sbody)
	{
		//CollisionWorld::addCollisionObject( sbody->getCollisionObject(),  btBroadphaseProxy::DefaultFilter,
		//	btBroadphaseProxy::AllFilter );
		if ( sbody != NULL )
		{
			sbody->setCollisionWorldInfo( &mCollisionWorldInfo );
			sbody->setGravity( DynamicsWorld::getGravity() );
			mSoftBodies.push_back(sbody);
		}
	}

	Bool PhysicsWorld::removeSoftBody( SoftBody* sbody )
	{
		Bool ret = ssTrue;
		//CollisionWorld::removeCollisionObject( sbody->getCollisionObject() );
		mSoftBodiesIterator = std::find( mSoftBodies.begin(), mSoftBodies.end(), sbody );
		if ( mSoftBodiesIterator == mSoftBodies.end() )
		{
			ret = ssFalse;
		}
		else
		{

			mSoftBodies.erase( mSoftBodiesIterator );

			ret = ssTrue;
		}
		if ( static_cast<CollisionObject*>(sbody) != NULL )
		{
			ret = CollisionWorld::remove( sbody );
		}
		return ret;
	}


	//void PhysicsWorld::getCollisionObjects(void)
	//{

	//	m_collisionObjects.initializeFromBuffer( mSceneMgr->getCollisionObjects(),
	//		mSceneMgr->getNbCollisionObjects(), mSceneMgr->getCollisionObjectsCapacity() );
	//}


	//void PhysicsWorld::buildOdeRigidBodyList(void)
	//{
	//	const size_t NbRigidBodies =  mSceneMgr->getNbRigidBodies();
	//	const RigidBodies::iterator const RigidBodyIter = mSceneMgr->getRigidBodies();

	//	size_t counter = NbRigidBodies-1;
	//	RigidBodies::iterator iterThis = RigidBodyIter;

	//	(*iterThis)->dBody::getBody()->tome =  0; //static_cast<dObject**>(&(*iterThis)->getBody());

	//	if ( NbRigidBodies > 1 )
	//	{
	//		RigidBodies::iterator iterNext = RigidBodyIter+1;

	//		while ( counter-- > 0 )
	//		{
	//			(*iterThis)->dBody::id()->next = (*iterNext)->dBody::id();
	//			(*iterNext)->dBody::id()->tome = &(*iterThis)->dBody::id()->next;
	//			++iterNext;
	//			++iterThis;
	//		}

	//	}

	//	(*iterThis)->dBody::id()->next = 0;

	//	dWorld::id()->firstbody = (*RigidBodyIter)->dBody::id();
	//	dWorld::id()->nb = NbRigidBodies;

	//	//addObjectToList( RigidBodyIter,(dObject **) &dWorld::id()->firstbody );
	//	//dWorld::id()->nb++;

	//	//RigidBodyIter->next = dWorld::id()->firstbody;
	//	//RigidBodyIter->tome = RigidBodyIter;
	//	//if (*first) (*first)->tome = &obj->next;
	//	//(*first) = obj;

	//}

	//void PhysicsWorld::destroyOdeRigidBodyList(void)
	//{
	//	dWorld::id()->firstbody = 0;
	//	dWorld::id()->nb = 0;
	//}


World::World(btCollisionDispatcher* Dispatcher, 
	btBroadphaseInterface* PairCache, btCollisionConfiguration* CollisionConfiguration) 
	: btCollisionWorld( Dispatcher, PairCache, CollisionConfiguration )
	, dWorld()
	//, m_PoolAllocator(  sizeof_dxBody, max_nb_dxBody )
{
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	CPU_FREQ = 1000.0 / (double)freq.QuadPart;

	//Collision 
	m_CollisionConfiguration = CollisionConfiguration;
	m_PairCache = NULL;
	m_Dispatcher = NULL;
	
	//dInitODE2(0);
	//m_OdeWorld = NULL;
	//m_OdeSpace = NULL;
	//m_OdeContactGroup = dJointGroupCreate(0);

	m_CollisionWorldInfo.m_broadphase = m_PairCache;
	m_CollisionWorldInfo.m_dispatcher = m_Dispatcher;
	m_CollisionWorldInfo.m_sparsesdf.Initialize();
	m_CollisionWorldInfo.m_sparsesdf.Reset();

	//void* mem = btAlignedAlloc(sizeof(btPoolAllocator),16);
	//m_PoolAllocator = new btPoolAllocator( sizeof_dxBody, max_nb_dxBody );

	m_OdeContactGroup = dJointGroupCreate(0);

	//dAllocFunction* fn = OdeAlloc;
	//dSetAllocHandler( fn );
	//dReallocFunction* realloc = OdeRealloc;
	//dSetReallocHandler( realloc );
	//dFreeFunction* free_fn = OdeFree;
	//dSetFreeHandler( free_fn );
	
	
	// alloco un joint per inizializzare l'arena di ode
	dContact contact;
	dJointID c = dJointCreateContactSoftRigid( dWorld::id(), m_OdeContactGroup, &contact );
	dJointGroupEmpty( m_OdeContactGroup ); // m_OdeContactGroup.empty();

}


World::~World()
{

	DeAllocate();
}

void World::DeAllocate()
{

	// Remove but do not deallocate
	const int Nb_SoftBody = SoftBodyArray.size();

	for (int i=0; i<Nb_SoftBody; i++ )
	{
		Remove( SoftBodyArray[0] );
	}

	//// Remove but do not deallocate
	//const int Nb_FemSoftBody = FemSoftBodyArray.size();

	//for (int i=0; i<Nb_FemSoftBody; i++ )
	//{
	//	Remove( FemSoftBodyArray[0] );
	//}

	const int Nb_RigidBody = RigidBodyArray.size();

	for (int i=0; i<Nb_RigidBody; i++ )
	{
		Remove( RigidBodyArray[0] );
	}

	//// Remove but do not deallocate
	//const int Nb_SphSoftBody = sphSoftBodyArray.size();

	//for (int i=0; i<Nb_SphSoftBody; i++ )
	//{
	//	Remove( sphSoftBodyArray[0] );
	//}


	const int Nb_CollisionBody = m_collisionObjects.size();


	for (int i=0; i<Nb_CollisionBody; i++ )
	{
		btCollisionWorld::removeCollisionObject( m_collisionObjects[0] );
	}

	// provvisoriamente ho creat questa funzione per 
	// deallocare la struct m_sparsesdf che non ha il distruttore
	//m_CollisionWorldInfo.m_sparsesdf.Destroy();

	Release(m_PairCache);

	Release(m_Dispatcher);

	Release(m_CollisionConfiguration);


	dJointGroupDestroy ( m_OdeContactGroup );
	//m_OdeContactGroup.empty();
	
	// release pool allocator
	// Reset pool allocator
	//s_PoolAllocator.freeMemory()
	//s_PoolAllocator.~btPoolAllocator();
	//btAlignedFree(m_PoolAllocator);

	//dJointGroupDestroy( m_OdeContactGroup.id() );
	//dWorldDestroy( dWorld::id() );
	//dWorld::~dWorld();
	//dWorldDestroy( this );

}


void World::InitializeCollision( btCollisionDispatcher* Dispatcher, 
	btBroadphaseInterface* PairCache, btCollisionConfiguration* CollisionConfiguration )
{

	if ( CollisionConfiguration != NULL )
	{
		Release(m_CollisionConfiguration);
		m_CollisionConfiguration = CollisionConfiguration;
		//m_CollisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration();
		//CollisionConfiguration = new btDefaultCollisionConfiguration();
	}
	else
	{
		//m_CollisionConfiguration = CollisionConfiguration;
	}

	if ( Dispatcher == NULL )
	{
		m_Dispatcher = new btCollisionDispatcher(m_CollisionConfiguration);
	}
	else
	{
		m_Dispatcher = Dispatcher;
	}

	// Default Init.
	if ( PairCache == NULL )
	{
		btVector3	worldAabbMin(-1000,-1000,-1000);
		btVector3	worldAabbMax(1000,1000,1000);
		m_PairCache = new btAxisSweep3(worldAabbMin,worldAabbMax);
	}
	else
	{
		m_PairCache = PairCache;
	}
	
	m_CollisionWorldInfo.m_broadphase = m_PairCache;
	m_CollisionWorldInfo.m_dispatcher = m_Dispatcher;
	m_CollisionWorldInfo.m_sparsesdf.Initialize();
	m_CollisionWorldInfo.m_sparsesdf.Reset();

	m_dispatcher1 = m_Dispatcher;
	m_broadphasePairCache = m_PairCache;
	m_stackAlloc = m_CollisionConfiguration->getStackAllocator();
	m_dispatchInfo.m_stackAllocator = m_stackAlloc;

	return;
}

void World::InitializeOdeWorld( dWorldID OdeWorldID /*,dSpaceID OdeSpaceID,*/, dJointGroupID OdeContactGroupID )
{

	//if ( OdeContactGroupID == NULL )
	//{
	//	m_OdeContactGroup = dJointGroupCreate(0);
	//}
	//else
	//{
	//	m_OdeContactGroup = OdeContactGroupID;
	//}
	
	//m_OdeContactGroup = dJointGroupCreate(0);

	return;
}



//float World::simulate( const float& TimeStep, SceneManager* scene )
//{
//
//	tick_count starTicks = tick_count::now();
//
//
//	//float TimeStep = step_event.timeSinceLastFrame;
//	static const float invTimeStep = 1.0f/TimeStep;
//
//	SoftBody* softbody;
//	const int Nb_SoftBody = SoftBodyArray.size();
//
//	//FemSoftBody* fem_soft_body;
//	RigidBodyBase* rigid_body;
//
//	//const int Nb_FemSoftBody = FemSoftBodyArray.size();
//	const int Nb_RigidBody = RigidBodyArray.size();
//
//	//SphSoftBody* sph_softbody = 0;
//	//const int Nb_SphSoftBody = sphSoftBodyArray.size();
//
//	//for (int i=0; i< Nb_RigidBody; i++)
//	//{
//	//	rigid_body = RigidBodyArray[i];
//	//	rigid_body->integrateVelocities(TimeStep);
//	//	rigid_body->applyDamping(TimeStep);
//	//}
//
//	for (int i=0; i< Nb_SoftBody; i++)
//	{
//		softbody = SoftBodyArray[i];
//
//		softbody->assembleStiffnessMatrix();
//	}
//
//	//for (int i=0; i< Nb_FemSoftBody; i++)
//	//{
//	//	fem_soft_body = FemSoftBodyArray[i];
//
//	//	fem_soft_body->AssembleStiffnessMatrix();
//	//}
//
//	//for (int i=0; i< Nb_SphSoftBody; i++)
//	//{
//	//	sph_softbody = sphSoftBodyArray[i];
//
//	//	sph_softbody->AssembleStiffnessMatrix();
//	//}
//	
//
//
//
//	// Bullet Collision Step
//	btCollisionWorld::performDiscreteCollisionDetection();
//
//
//	dBodyID** particles = (dBodyID**)_malloca(Nb_SoftBody*sizeof(dBodyID*) );
//
//	dJointFeedback** soft_contact_force = (dJointFeedback**)_malloca(Nb_SoftBody*sizeof(dJointFeedback*) );
//
//	// SoftBody - RigidBodyBase contact
//	for (int i=0; i< Nb_SoftBody; i++)
//	{
//		softbody = SoftBodyArray[i];
//
//		//int nb_contacts = softbody->m_DynamicContactArray.size();
//		int nb_contacts = softbody->getDynamicContactArray().size();
//
//		
//		//particles[i] = 0;
//		particles[i] = (dBodyID*)_malloca(nb_contacts*sizeof(dBodyID) );
//		soft_contact_force[i] = (dJointFeedback*)_malloca(nb_contacts*sizeof(dJointFeedback) );
//
//		//int rigid_body_counter = 1;
//		//while ( rigid_body_counter < nb_contacts )
//		//{
//		//	SimStep::RigidBodyBase* rb1 = fem_soft_body->m_DynamicContactArray[rigid_body_counter-1].m_RigidBody;
//		//	SimStep::RigidBodyBase* rb2 = fem_soft_body->m_DynamicContactArray[rigid_body_counter].m_RigidBody;
//		//
//		//	rigid_body_counter++;
//		//}
//
//		//  it's not correct .. fix it!
//		//float Mass = softbody->m_Mass / (float)nb_contacts;
//		float Mass = softbody->getMass()  / (float)nb_contacts;
//
//
//		for ( int j=0; j< nb_contacts; j++ )
//		{
//			//SoftBody::RigidContact& dynamic_contact = softbody->m_DynamicContactArray[j];
//
//			SoftBody::RigidContact& dynamic_contact = softbody->getDynamicContactArray()[j];
//
//
//			int id_node = dynamic_contact.m_node->IdNode;
//			particles[i][j] = dBodyCreate( dWorld::id() );
//
//			Vector3& node_pos = softbody->getPosition(id_node);
//			Vector3& node_vel = softbody->getVelocity(id_node);
//
//
//			dBodySetPosition( particles[i][j], 
//				node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//
//			dBodySetLinearVel( particles[i][j], 
//				node_vel.getX(), node_vel.getY(), node_vel.getZ() );
//
//			Vector3 node_stress = softbody->getStressForce(id_node);
//
//			dBodyAddForceAtPos( dynamic_contact.m_RigidBody->dBody::id(), 
//				node_stress.getX(), node_stress.getY(), node_stress.getZ(),
//				node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//
//			dBodySetData( particles[i][j], (void*)softbody );
//
//			dMass mass;
//
//			mass.mass = Mass;
//			//mass.mass = fem_soft_body->m_NodesMass[id_node];
//			mass.I[0] = dInfinity;
//			mass.I[1] = 0.0f;
//			mass.I[2] = 0.0f;
//
//			mass.I[4] = 0.0f;;
//			mass.I[5] = dInfinity;
//			mass.I[6] = 0.0f;
//
//			mass.I[8] = 0.0f;
//			mass.I[9] = 0.0f;
//			mass.I[10] = dInfinity;
//
//			dBodySetMass( particles[i][j], &mass );
//
//			dContact contact;
//
//			contact.surface.soft_erp = 0.8f;
//			contact.surface.soft_cfm = 1e-5;
//			contact.surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
//			contact.surface.mu = 0.1;
//			contact.surface.mu2 = 0.1;
//			contact.surface.bounce = 0.1;
//			contact.surface.bounce_vel = 0.1;
//			contact.surface.soft_cfm = 1e-5;
//
//			dContactGeom& contactgeom = contact.geom;
//
//
//			contactgeom.depth = dynamic_contact.m_ContactInfo.m_offset;
//
//			memcpy( contactgeom.pos, dynamic_contact.m_RelAnchorPosition, align16 );
//			memcpy( contactgeom.normal, dynamic_contact.m_ContactInfo.m_normal, align16 );
//
//
//			dJointID c = dJointCreateContactSoftRigid( dWorld::id(), m_OdeContactGroup, &contact );
//
//			dJointSetFeedback( c, &soft_contact_force[i][j] );
//
//			//SoftBody::RigidContact& rigid_contact = softbody->m_DynamicContactArray[j];
//
//			SoftBody::RigidContact& rigid_contact = softbody->getDynamicContactArray()[j];
//
//			dJointAttach( c, rigid_contact.m_RigidBody->dBody::id(), particles[i][j] );
//
//		}
//
//		//dBodyDestroy( nullbody );
//
//	}
//
//	//dBodyID* fem_particles = 0;
//	//dJointFeedback* fem_soft_contact_force = 0;
//
//	//// FemSoftBody - RigidBodyBase contact
//	//for (int i=0; i< Nb_FemSoftBody; i++)
//	//{
//	//	fem_soft_body = FemSoftBodyArray[i];
//
//	//	int nb_contacts = fem_soft_body->m_DynamicContactArray.size();
//
//	//	fem_particles = (dBodyID*)_malloca(nb_contacts*sizeof(dBodyID) );
//	//	fem_soft_contact_force = (dJointFeedback*)_malloca(nb_contacts*sizeof(dJointFeedback) );
//
//	//	//int rigid_body_counter = 1;
//	//	//while ( rigid_body_counter < nb_contacts )
//	//	//{
//	//	//	SimStep::RigidBodyBase* rb1 = fem_soft_body->m_DynamicContactArray[rigid_body_counter-1].m_RigidBody;
//	//	//	SimStep::RigidBodyBase* rb2 = fem_soft_body->m_DynamicContactArray[rigid_body_counter].m_RigidBody;
//	//	//
//	//	//	rigid_body_counter++;
//	//	//}
//
//	//	//  it's not correct .. fix it!
//	//	float Mass = fem_soft_body->m_Mass / (float)nb_contacts;
//
//
//	//	for ( int j=0; j< nb_contacts; j++ )
//	//	{
//	//		FemSoftBody::RigidContact& dynamic_contact = fem_soft_body->m_DynamicContactArray[j];
//	//		
//	//		int id_node = dynamic_contact.m_node->IdNode;
//	//		fem_particles[j] = dBodyCreate( dWorld::id() );
//	//	
//	//		SimdVector3& node_pos = fem_soft_body->GetNodesPtr()[id_node];
//	//		SimdVector3& node_vel = fem_soft_body->GetVelocityPtr()[id_node];
//
//	//		
//	//		dBodySetPosition( fem_particles[j], 
//	//			node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//	//		
//	//		dBodySetLinearVel( fem_particles[j], 
//	//			node_vel.getX(), node_vel.getY(), node_vel.getZ() );
//
//	//		SimdVector3 node_stress = fem_soft_body->GetStressForce(id_node);
//
//	//		dBodyAddForceAtPos( dynamic_contact.m_RigidBody->dBody::id(), 
//	//			node_stress.getX(), node_stress.getY(), node_stress.getZ(),
//	//			node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//
//	//		dBodySetData( fem_particles[j], (void*)fem_soft_body );
//
//	//		dMass mass;
//
//	//		mass.mass = Mass;
//	//		//mass.mass = fem_soft_body->m_NodesMass[id_node];
//	//		mass.I[0] = dInfinity;
//	//		mass.I[1] = 0.0f;
//	//		mass.I[2] = 0.0f;
//
//	//		mass.I[4] = 0.0f;;
//	//		mass.I[5] = dInfinity;
//	//		mass.I[6] = 0.0f;
//
//	//		mass.I[8] = 0.0f;
//	//		mass.I[9] = 0.0f;
//	//		mass.I[10] = dInfinity;
//
//	//		dBodySetMass( fem_particles[j], &mass );
//
//	//		dContact contact;
//	//		
//	//		contact.surface.soft_erp = 0.8f;
//	//		contact.surface.soft_cfm = 1e-5;
//	//		contact.surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
//	//		contact.surface.mu = 0.1;
//	//		contact.surface.mu2 = 0.1;
//	//		contact.surface.bounce = 0.1;
//	//		contact.surface.bounce_vel = 0.1;
//	//		contact.surface.soft_cfm = 1e-5;
//
//	//		dContactGeom& contactgeom = contact.geom;
//	//		
//
//	//		contactgeom.depth = dynamic_contact.m_ContactInfo.m_offset;
//	//		
//	//		memcpy( contactgeom.pos, dynamic_contact.m_RelAnchorPosition, align16 );
//	//		memcpy( contactgeom.normal, dynamic_contact.m_ContactInfo.m_normal, align16 );
//
//
//	//		dJointID c = dJointCreateContactSoftRigid( dWorld::id(), m_OdeContactGroup, &contact );
//
//	//		dJointSetFeedback( c, &fem_soft_contact_force[j] );
//
//	//		FemSoftBody::RigidContact& rigid_contact = fem_soft_body->m_DynamicContactArray[j];
//
//	//		dJointAttach( c, rigid_contact.m_RigidBody->dBody::id(), fem_particles[j] );
//
//	//	}
//
//		//dBodyDestroy( nullbody );
//
//	//}
//
//	//// SPH
//
//	//dBodyID* sph_particles = 0;
//	//dJointFeedback* sph_soft_contact_force = 0;
//
//	//// FemSoftBody - RigidBodyBase contact
//	//for (int i=0; i< Nb_SphSoftBody; i++)
//	//{
//	//	sph_softbody = sphSoftBodyArray[i];
//
//	//	int nb_contacts = sph_softbody->m_DynamicContactArray.size();
//
//	//	sph_particles = (dBodyID*)_malloca(nb_contacts*sizeof(dBodyID) );
//	//	sph_soft_contact_force = (dJointFeedback*)_malloca(nb_contacts*sizeof(dJointFeedback) );
//
//	//	//int rigid_body_counter = 1;
//	//	//while ( rigid_body_counter < nb_contacts )
//	//	//{
//	//	//	SimStep::RigidBodyBase* rb1 = fem_soft_body->m_DynamicContactArray[rigid_body_counter-1].m_RigidBody;
//	//	//	SimStep::RigidBodyBase* rb2 = fem_soft_body->m_DynamicContactArray[rigid_body_counter].m_RigidBody;
//	//	//
//	//	//	rigid_body_counter++;
//	//	//}
//
//	//	//  it's not correct .. fix it!
//	//	float Mass = sph_softbody->m_Mass / (float)nb_contacts;
//
//
//	//	for ( int j=0; j< nb_contacts; j++ )
//	//	{
//	//		SphSoftBody::RigidContact& dynamic_contact = sph_softbody->m_DynamicContactArray[j];
//
//	//		int id_node = dynamic_contact.m_node->IdNode;
//	//		sph_particles[j] = dBodyCreate( dWorld::id() );
//
//	//		SimdVector3& node_pos = sph_softbody->GetNodes()[id_node];
//	//		SimdVector3& node_vel = sph_softbody->GetVelocities()[id_node];
//
//
//	//		dBodySetPosition( sph_particles[j], 
//	//			node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//
//	//		dBodySetLinearVel( sph_particles[j], 
//	//			node_vel.getX(), node_vel.getY(), node_vel.getZ() );
//
//	//		SimdVector3 node_stress = sph_softbody->GetStressForce(id_node);
//
//	//		dBodyAddForceAtPos( dynamic_contact.m_RigidBody->dBody::id(), 
//	//			node_stress.getX(), node_stress.getY(), node_stress.getZ(),
//	//			node_pos.getX(), node_pos.getY(), node_pos.getZ() );
//
//	//		dBodySetData( sph_particles[j], (void*)sph_softbody );
//
//	//		dMass mass;
//
//	//		mass.mass = Mass;
//	//		//mass.mass = fem_soft_body->m_NodesMass[id_node];
//	//		mass.I[0] = dInfinity;
//	//		mass.I[1] = 0.0f;
//	//		mass.I[2] = 0.0f;
//
//	//		mass.I[4] = 0.0f;;
//	//		mass.I[5] = dInfinity;
//	//		mass.I[6] = 0.0f;
//
//	//		mass.I[8] = 0.0f;
//	//		mass.I[9] = 0.0f;
//	//		mass.I[10] = dInfinity;
//
//	//		dBodySetMass( sph_particles[j], &mass );
//
//	//		dContact contact;
//
//	//		contact.surface.soft_erp = 0.8f;
//	//		contact.surface.soft_cfm = 1e-5;
//	//		contact.surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
//	//		contact.surface.mu = 0.1;
//	//		contact.surface.mu2 = 0.1;
//	//		contact.surface.bounce = 0.1;
//	//		contact.surface.bounce_vel = 0.1;
//	//		contact.surface.soft_cfm = 1e-5;
//
//	//		dContactGeom& contactgeom = contact.geom;
//
//
//	//		contactgeom.depth = dynamic_contact.m_ContactInfo.m_offset;
//
//	//		memcpy( contactgeom.pos, dynamic_contact.m_RelAnchorPosition, align16 );
//	//		memcpy( contactgeom.normal, dynamic_contact.m_ContactInfo.m_normal, align16 );
//
//
//	//		dJointID c = dJointCreateContactSoftRigid( dWorld::id(), m_OdeContactGroup, &contact );
//
//	//		dJointSetFeedback( c, &sph_soft_contact_force[j] );
//
//	//		SphSoftBody::RigidContact& rigid_contact = sph_softbody->m_DynamicContactArray[j];
//
//	//		dJointAttach( c, rigid_contact.m_RigidBody->dBody::id(), sph_particles[j] );
//
//	//	}
//
//	//	//dBodyDestroy( nullbody );
//
//	//}
//
//
//
//	//btPersistentManifold** manifold = m_CollisionWorld->getDispatcher()->getInternalManifoldPointer();
//
//	int numManifolds = btCollisionWorld::getDispatcher()->getNumManifolds();
//
//	for ( int i=0;i< numManifolds ;i++)
//	{
//		btPersistentManifold* contactManifold = btCollisionWorld::getDispatcher()->getManifoldByIndexInternal(i);
//		//btCollisionObject* obA = static_cast<btCollisionObject*>(contactManifold->getBody0());
//		//btCollisionObject* obB = static_cast<btCollisionObject*>(contactManifold->getBody1());
//
//		RigidBodyBase* body1 = static_cast<RigidBodyBase*>(contactManifold->getBody0());
//		RigidBodyBase* body2 = static_cast<RigidBodyBase*>(contactManifold->getBody1());
//
//
//		dContact contact[4];
//
//		for (int k=0; k<4; k++) 
//		{
//			// find a way to set this param for every type of contacts
//			contact[k].surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
//			contact[k].surface.soft_erp = 0.9f;
//			contact[k].surface.soft_cfm = 1e-5f;
//			contact[k].surface.mu = 0.1;
//			contact[k].surface.mu2 = 0.1;
//			contact[k].surface.bounce = 0.1;
//			contact[k].surface.bounce_vel = 0.1;
//		}
//
//		int nb_ContactPoints = contactManifold->getNumContacts();
//	
//		for (int j=0;j<nb_ContactPoints;j++)
//		{
//			btManifoldPoint& pt = contactManifold->getContactPoint(j);
//			dContactGeom& contactgeom = contact[j].geom;
//
//			if ( pt.getDistance() <= 0.0f )
//			{
//
//				contactgeom.depth = pt.getDistance();
//
//				memcpy( &contactgeom.pos, &pt.getPositionWorldOnB(), sizeof(btVector3) );
//				
//				memcpy( &contactgeom.normal, &pt.m_normalWorldOnB, sizeof(btVector3) );
//
//
//				//contact[j].geom = &contactgeom[j];
//				//glBegin(GL_LINES);
//				//glColor3f(1, 0, 1);
//				//btVector3 ptA = pt.getPositionWorldOnA();
//				//btVector3 ptB = pt.getPositionWorldOnB();
//				//glVertex3d(ptA.x(),ptA.y(),ptA.z());
//				//glVertex3d(ptB.x(),ptB.y(),ptB.z());
//				//glEnd();
//
//				dJointID c = dJointCreateContact( dWorld::id() , m_OdeContactGroup, &contact[j] );
//
//				dJointAttach( c, body1->dBody::id(), body2->dBody::id() );
//			}
//		}
//
//	}
//
//	// Ode Step
//	dWorldQuickStep ( dWorld::id(), TimeStep );
//
//
//	// Apply contact forces to softbodies
//	for (int i=Nb_SoftBody-1; i>=0; i--)
//	{
//		softbody = SoftBodyArray[i];
//
//		int nb_contacts = softbody->getDynamicContactArray().size();
//		Vector3 contact_force;
//		
//		for ( int j= nb_contacts-1; j>=0 ; j-- )
//		{
//			int& id_node = softbody->getDynamicContactArray()[j].m_node->IdNode;
//			
//			memcpy( &contact_force, soft_contact_force[i][j].f2, align16 );
//			
//			softbody->addForce( id_node, contact_force);
//
//			dBodyDestroy( particles[i][j] );
//		}
//	}
//
//
//	//if ( sph_softbody != 0 )
//	//{
//	//	_freea(sph_softbody);
//	//	sph_softbody = 0;
//	//}
//	//if ( sph_particles != 0 )
//	//{
//	//	_freea(sph_particles);
//	//	sph_particles = 0;
//	//}
//	//if ( fem_soft_contact_force != 0 )
//	//{
//	//	_freea(fem_soft_contact_force);
//	//	fem_soft_contact_force = 0;
//	//}
//	//if ( fem_particles != 0 )
//	//{
//	//	_freea(fem_particles);
//	//	fem_particles = 0;
//	//}
//	
//	for (int i=Nb_SoftBody-1; i>=0; i--)
//	{
//		
//		if ( soft_contact_force != 0 )
//		{
//			_freea(soft_contact_force[i]);
//			soft_contact_force[i] = 0;
//		}
//
//		if ( particles[i] != 0 )
//		{
//			_freea(particles[i]);
//			particles[i] = 0;
//		}
//	}
//	
//	if ( soft_contact_force != 0 )
//	{
//		_freea(soft_contact_force );
//		soft_contact_force  = 0;
//	}
//
//	if ( particles  != 0 )
//	{
//		_freea(particles );
//		particles = 0;
//	}
//
//
//	// remove all contact joints
//	dJointGroupEmpty(m_OdeContactGroup);
//
//
//	for (int i=0; i< Nb_RigidBody; i++)
//	{
//		rigid_body = RigidBodyArray[i];
//		rigid_body->synchronizeCollision2Physics();
//	}
//
//
//	for (int i=0; i< Nb_SoftBody; i++)
//	{
//		softbody = SoftBodyArray[i];
//
//		softbody->solve( TimeStep );
//	}
//
//	//for (int i=0; i< Nb_SphSoftBody; i++)
//	//{
//	//	sph_softbody = sphSoftBodyArray[i];
//
//	//	sph_softbody->Solve( TimeStep );
//	//}
//
//
//	for (int i=0; i< Nb_SoftBody; i++)
//	{
//		softbody = SoftBodyArray[i];
//
//		softbody->updateCollisionShape();
//
//		////fem_soft_body->UpdateClusters();
//		///* Bounds				*/ 
//		//softbody->UpdateBounds();
//		/* Nodes				*/ 
//
//		
//
//		//SimdVector3 velocity;
//		//for( int i=0, ni=softbody->getNbCollisionNodes();i<ni;++i)
//		//{
//		//	SoftBody::CollisionNode&	n= softbody->CollisionNodes[i];
//		//	Vector3& vel = softbody->getVelocity(i);
//		//	//fem_soft_body->NodeDynamicTree.update(	n.leaf,
//		//	//	btDbvtVolume::FromCR( btVector3(n.node->getX(),n.node->getY(),n.node->getZ()), fem_soft_body->m_collisionShape->getMargin() ),
//		//	//	btVector3(vel.getX(), vel.getY(), vel.getZ()) * 1.0f/* margine */,
//		//	//	fem_soft_body->m_collisionShape->getMargin()/* margine */ );
//		//	
//		//	Vector3& position = softbody->getPosition(i);
//		//	//n.IdNode = i;
//		//	
//		//	//softbody->NodeDynamicTree.update(	n.leaf,
//		//	//	btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->m_collisionShape->getMargin() ),
//		//	//	btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
//		//	//	softbody->m_collisionShape->getMargin()/* margine */ );
//		//
//		//	softbody->getNodeDynamicTree().update(	n.leaf,
//		//		btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->getCollisionShape()->getMargin() ),
//		//		btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
//		//		softbody->getCollisionShape()->getMargin()/* margine */ );
//		//}
//
//		/* Faces				*/ 
//		//if( fem_soft_body->FaceDynamicTree != NULL )
//		//{
//		//	for(int i=0;i< fem_soft_body->m_Nb_Triangles;++i)
//		//	{
//		//		TriangleElement& t = fem_soft_body->m_TrianglesPtr[i];
//		//		const SimdVector3 v =  
//		//			( fem_soft_body->m_NodesVelocity[t.n[0]],
//		//			fem_soft_body->m_NodesVelocity[t.n[1]],
//		//			fem_soft_body->m_NodesVelocity[t.n[2]] ) / 3.0f;
//		//		//const btVector3	btv= btVector3(v.getX(), v.getY(), v.getZ() );
//		//		//Face&			f=m_faces[i];
//		//		//const btVector3	v=(	f.m_n[0]->m_v+
//		//		//	f.m_n[1]->m_v+
//		//		//	f.m_n[2]->m_v)/3;
//		//		FaceDynamicTree.update(	t.m_leaf,
//		//			VolumeOf(f,m_sst.radmrg),
//		//			v*m_sst.velmrg,
//		//			m_sst.updmrg);
//		//	}
//		//}
//	}
//	
//	
//	// clear contacts
//	for (int i=0; i< Nb_SoftBody; i++)
//	{
//		//DrawContacts( *SoftBodyArray[i] );
//		SoftBodyArray[i]->getStaticContactArray().resize(0);
//		SoftBodyArray[i]->getDynamicContactArray().resize(0);
//	}
//
//	///* Optimize dbvt's		*/ 
//	//for (int i=0; i< Nb_SoftBody; i++)
//	//{
//	//	//SoftBodyArray[i]->ClustersDynamicTree.optimizeIncremental(1);
//	//	SoftBodyArray[i]->getNodeDynamicTree().optimizeIncremental(1);
//	//}
//	////FaceDynamicTree.optimizeIncremental(1);
//
//	m_CollisionWorldInfo.m_sparsesdf.GarbageCollect();
//
//
//	return  ( tick_count::now() - starTicks ).seconds();
//}



void World::Add(SoftBody* body)
{
	if (body )
	{
		//if ( body->getInternalType() == btCollisionObject::CO_SOFT_BODY )
		//{

			////body->m_internalType = btCollisionObject::CO_SOFT_BODY;
			body->setCollisionWorldInfo( &m_CollisionWorldInfo );
			SoftBodyArray.push_back(body);

			//if ( m_CollisionWorld )
			{
				btCollisionWorld::addCollisionObject( 
					static_cast<btCollisionObject*>(body), 
					btBroadphaseProxy::DefaultFilter,
					btBroadphaseProxy::AllFilter );
			}

		//}

	}	

}


//void World::Add(FemSoftBody* body)
//{
	//if (body )
	//{
	//	body->m_internalType = btCollisionObject::CO_SOFT_BODY;
	//	body->m_CollisionWorldInfo = &m_CollisionWorldInfo;
	//	FemSoftBodyArray.push_back(body);

	//	//if ( m_CollisionWorld )
	//	{
	//		btCollisionWorld::addCollisionObject( body,  btBroadphaseProxy::DefaultFilter,
	//			btBroadphaseProxy::AllFilter );
	//	}

	//}	
//	
//}

void World::Add(RigidBodyBase* rigidbody)
{
	if (rigidbody )
	{
		//rigidbody->m_internalType = btCollisionObject::CO_RIGID_BODY;
		//rigidbody->m_CollisionWorldInfo = &m_CollisionWorldInfo;
		RigidBodyArray.push_back(rigidbody);

		//if ( m_CollisionWorld!= NULL )
		{
			btCollisionWorld::addCollisionObject( rigidbody,  btBroadphaseProxy::DefaultFilter,
				btBroadphaseProxy::AllFilter );
		}

		if ( dWorld::id() != NULL)
		{
			
			// creo e aggiungo il rigidbody a Ode

			rigidbody->dBody::dBody( dWorld::id() );

			//dGeomSetBody(0, rigidbody->OdeRigidBody);

			rigidbody->setUserPointer( (void*)rigidbody->dBody::id() );

			dBodySetData(rigidbody->dBody::id(), (void*)rigidbody );

			
			dBodySetPosition(rigidbody->dBody::id(), 
				rigidbody->getWorldTransform().getOrigin().getX(),
				rigidbody->getWorldTransform().getOrigin().getY(),
				rigidbody->getWorldTransform().getOrigin().getZ() );		


			dMatrix3 orientation;

			memcpy( &orientation, &rigidbody->getWorldTransform().getBasis(), sizeof(btMatrix3x3) );


			dBodySetRotation(rigidbody->dBody::id(), orientation );

			dMass mass;

			mass.mass = rigidbody->m_Mass;

			mass.I[0] = rigidbody->m_InertiaTensor.getElem(0,0);
			mass.I[1] = rigidbody->m_InertiaTensor.getElem(0,1);
			mass.I[2] = rigidbody->m_InertiaTensor.getElem(0,2);

			mass.I[4] = rigidbody->m_InertiaTensor.getElem(1,0);
			mass.I[5] = rigidbody->m_InertiaTensor.getElem(1,1);
			mass.I[6] = rigidbody->m_InertiaTensor.getElem(1,2);

			mass.I[8] = rigidbody->m_InertiaTensor.getElem(2,0);
			mass.I[9] = rigidbody->m_InertiaTensor.getElem(2,1);
			mass.I[10] = rigidbody->m_InertiaTensor.getElem(2,2);

			dBodySetMass( rigidbody->dBody::id(), &mass );
		}
		
	}	

}

void World::Add( btCollisionObject* CollisionObject )
{


	//if ( this->getCollisionWorld() )
	{
		btCollisionWorld::addCollisionObject( CollisionObject );
	}
	
}



//void World::Add(SphSoftBody* body)
//{
//	if (body )
//	{
//		body->m_internalType = btCollisionObject::CO_SOFT_BODY;
//		body->m_CollisionWorldInfo = &m_CollisionWorldInfo;
//		sphSoftBodyArray.push_back(body);
//
//		//if ( m_CollisionWorld )
//		{
//			btCollisionWorld::addCollisionObject( body,  btBroadphaseProxy::DefaultFilter,
//				btBroadphaseProxy::AllFilter );
//		}
//
//	}	
//
//}


BOOL World::Remove( SoftBody* body )
{
	BOOL ret = FALSE;


	if ( body != 0)
	{

		SoftBodyIterator = std::find( SoftBodyArray.begin(), SoftBodyArray.end(), body );

		if ( SoftBodyIterator == SoftBodyArray.end() )
			ret = FALSE;
		else
		{
			std::swap(SoftBodyIterator, SoftBodyArray.end() );
			SoftBodyArray.pop_back();
			ret = TRUE;
		}

		if ( static_cast<btCollisionObject*>(body) != NULL )
		{
			btCollisionWorld::removeCollisionObject( static_cast<btCollisionObject*>(body) );
		}
	}

	return ret;
}


//
//BOOL World::Remove( FemSoftBody* body )
//{
//	BOOL ret = FALSE;
//
//	
	//if ( body != 0)
	//{

	//	FemSoftBodyIterator = std::find( FemSoftBodyArray.begin(), FemSoftBodyArray.end(), body );

	//	if ( FemSoftBodyIterator == FemSoftBodyArray.end() )
	//		ret = FALSE;
	//	else
	//	{
	//		std::swap(FemSoftBodyIterator, FemSoftBodyArray.end() );
	//		FemSoftBodyArray.pop_back();
	//		ret = TRUE;
	//	}

	//	if ( static_cast<btCollisionObject*>(body) != NULL )
	//	{
	//		btCollisionWorld::removeCollisionObject( body );
	//	}
	//}
//
//	return ret;
//}



BOOL World::Remove( RigidBodyBase* body )
{
	BOOL ret = FALSE;

	if ( body != 0)
	{
		if ( dWorld::id() != NULL)
		{
			// distrugge il rigidbody a Ode
			//dWorld::removeObjectFromList()
			dBodyDestroy( body->dBody::id() );
			// to fix it
			//set dBody::id to zero calling empty constructor
			// find another way to do this
			body->dBody::dBody();
		}

		RigidBodyIterator = std::find( RigidBodyArray.begin(), RigidBodyArray.end(), body );

		if ( RigidBodyIterator == RigidBodyArray.end() )
		{
			ret = FALSE;
		}
		else
		{

			RigidBodyArray.erase( RigidBodyIterator );

			ret = TRUE;
		}

		//if ( m_CollisionWorld )
		{
			btCollisionWorld::removeCollisionObject( body );
		}

	}

	return ret;
}


BOOL World::Remove( btCollisionObject* CollisionObject )
{
	BOOL ret = FALSE;

	//if ( m_CollisionWorld )
	{
		btCollisionWorld::removeCollisionObject( CollisionObject );
	}

	return ret;
}

//
//BOOL World::Remove( SphSoftBody* body )
//{
//	BOOL ret = FALSE;
//
//
//	if ( body != 0)
//	{
//
//		sphSoftBodyIterator = std::find( sphSoftBodyArray.begin(), sphSoftBodyArray.end(), body );
//
//		if ( sphSoftBodyIterator == sphSoftBodyArray.end() )
//			ret = FALSE;
//		else
//		{
//			std::swap(sphSoftBodyIterator, sphSoftBodyArray.end() );
//			sphSoftBodyArray.pop_back();
//			ret = TRUE;
//		}
//
//		if ( static_cast<btCollisionObject*>(body) != NULL )
//		{
//			btCollisionWorld::removeCollisionObject( body );
//		}
//	}
//
//	return ret;
//}



btCollisionWorld*	World::getCollisionWorld()
{
	return static_cast<btCollisionWorld*>(this);
}

dWorldID World::getOdeWorld()
{
	return dWorld::id();
}

SoftBody** World::getSoftBodyArray()
{
	return &SoftBodyArray[0];
}

std::vector<SoftBody*>& World::getSoftBodyVector()
{
	return SoftBodyArray;
}


const String& World::getName(void) const
{
	static String strName( "Default World");
	return strName;
}

void World::release(void)
{

}


//SoftBody** World::getSoftBodyArray()
//{
//	return &SoftBodyArray[0];
//}
//
//std::vector<SoftBody*>& World::getSoftBodyVector()
//{
//	return SoftBodyArray;
//}


}; // namespace SimStep