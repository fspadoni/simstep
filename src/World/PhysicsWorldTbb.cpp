#include "World/PhysicsWorldTBB.h"

#include "World/RigidBody.h"

#include "Core/SoftBody.h"

#include "World/PhysicsWorldTbbTask.h"


#include "tbb/task_scheduler_init.h"
#include "tbb/task.h"

#include "Utilities/StopWatch.h"


namespace SimStep {


	PhysicsWorldTbb::PhysicsWorldTbb(
		btCollisionDispatcher* Dispatcher, 
		btBroadphaseInterface* PairCache, 
		btCollisionConfiguration* CollisionConfiguration ,
		tbb::task_scheduler_init* task_scheduler,
		unsigned int uRequestedNumberOfThreads  ) 
		:
	World(Dispatcher, PairCache, CollisionConfiguration )
	{

		m_uNumberOfThreads = uRequestedNumberOfThreads;

		if ( m_uNumberOfThreads == 0 )
		{
			m_uNumberOfThreads = tbb::task_scheduler_init::default_num_threads();
		}

		if (task_scheduler == NULL )
		{
	
			m_pTbbScheduler = new tbb::task_scheduler_init(m_uNumberOfThreads);
			TbbSchedulerAllocated = true;	
		}
		else 
		{
			m_pTbbScheduler = task_scheduler;
			TbbSchedulerAllocated = false;
		}

		m_pRootTask = new( tbb::task::allocate_root() ) tbb::empty_task;
		

		
	}
	

	PhysicsWorldTbb::~PhysicsWorldTbb()
	{

		Terminate();
		//DeAllocate();
		//if ( m_pRootTask != NULL )
		//{
		//	m_pRootTask->destroy( *m_pRootTask );
		//	//delete m_pRootTask;
		//	m_pRootTask = NULL;
		//}

		////m_pTbbScheduler->terminate( );

		//if ( TbbSchedulerAllocated == true )
		//{
		//	delete m_pTbbScheduler;
		//	m_pTbbScheduler = NULL;
		//}
	}

	void PhysicsWorldTbb::Terminate()
	{

		if ( m_pRootTask != NULL )
		{
			m_pRootTask->destroy( *m_pRootTask );
			//delete m_pRootTask;
			m_pRootTask = NULL;
		}

		if ( TbbSchedulerAllocated == true )
		{
			delete m_pTbbScheduler;
			m_pTbbScheduler = NULL;
		}

		//Sleep(1000);

	}

	float PhysicsWorldTbb::SimulateTbb( const float& TimeStep )
	{

		tick_count starTicks = tick_count::now();


		static tick_count oldTicks = starTicks;

		float dTime = (float)( starTicks - oldTicks).milliseconds();
		oldTicks = starTicks;

		static const float invTimeStep = 1.0f/TimeStep;

		SoftBody* softbody;
		const int Nb_SoftBody = SoftBodyArray.size();

		//FemSoftBody* fem_soft_body;
		RigidBodyBase* rigid_body;

		//const int Nb_FemSoftBody = FemSoftBodyArray.size();
		const int Nb_RigidBody = RigidBodyArray.size();

		// TBB
		tbb::task_list tasklist;
		
		//m_pRootTask->set_ref_count( 1 );

		//tbb::task& c = *new( task::allocate_continuation() ) tbb::empty_task();


		for (int i=0; i< Nb_SoftBody; i++)
		{

			tbb::task* assembleKtask = 
				new( m_pRootTask->allocate_child()) AssembleStiffnessMatrixTask( SoftBodyArray[i] );
			
			tasklist.push_back( *assembleKtask);

			//softbody = SoftBodyArray[i];

			//softbody->AssembleStiffnessMatrix();
		}

		m_pRootTask->set_ref_count( Nb_SoftBody + 1);
		m_pRootTask->spawn_and_wait_for_all( tasklist );
		//tbb::task::spawn_root_and_wait( *m_pRootTask );



		// Bullet Collision Step
		btCollisionWorld::performDiscreteCollisionDetection();


		dBodyID** particles = (dBodyID**)_malloca(Nb_SoftBody*sizeof(dBodyID*) );

		dJointFeedback** soft_contact_force = (dJointFeedback**)_malloca(Nb_SoftBody*sizeof(dJointFeedback*) );

		// SoftBodyBase - RigidBodyBase contact
		for (int i=0; i< Nb_SoftBody; i++)
		{
			softbody = SoftBodyArray[i];

			//int nb_contacts = softbody->m_DynamicContactArray.size();
			int nb_contacts = softbody->getDynamicContactArray().size();


			//particles[i] = 0;
			particles[i] = (dBodyID*)_malloca(nb_contacts*sizeof(dBodyID) );
			soft_contact_force[i] = (dJointFeedback*)_malloca(nb_contacts*sizeof(dJointFeedback) );

			//int rigid_body_counter = 1;
			//while ( rigid_body_counter < nb_contacts )
			//{
			//	SimStep::RigidBodyBase* rb1 = fem_soft_body->m_DynamicContactArray[rigid_body_counter-1].m_RigidBody;
			//	SimStep::RigidBodyBase* rb2 = fem_soft_body->m_DynamicContactArray[rigid_body_counter].m_RigidBody;
			//
			//	rigid_body_counter++;
			//}

			//  it's not correct .. fix it!
			//float Mass = softbody->m_Mass / (float)nb_contacts;
			float Mass = softbody->getMass()  / (float)nb_contacts;


			for ( int j=0; j< nb_contacts; j++ )
			{
				//SoftBodyBase::RigidContact& dynamic_contact = softbody->m_DynamicContactArray[j];

				SoftBody::RigidContact& dynamic_contact = softbody->getDynamicContactArray()[j];


				int id_node = dynamic_contact.m_IdNode;
				particles[i][j] = dBodyCreate( dWorld::id() );

				Vector3& node_pos = softbody->getPosition(id_node);
				Vector3& node_vel = softbody->getVelocity(id_node);


				dBodySetPosition( particles[i][j], 
					node_pos.getX(), node_pos.getY(), node_pos.getZ() );

				dBodySetLinearVel( particles[i][j], 
					node_vel.getX(), node_vel.getY(), node_vel.getZ() );

				Vector3 node_stress = softbody->getStressForce(id_node);

				dBodyAddForceAtPos( dynamic_contact.m_RigidBody->dBody::id(), 
					node_stress.getX(), node_stress.getY(), node_stress.getZ(),
					node_pos.getX(), node_pos.getY(), node_pos.getZ() );

				dBodySetData( particles[i][j], (void*)softbody );

				dMass mass;

				mass.mass = Mass;
				//mass.mass = fem_soft_body->m_NodesMass[id_node];
				mass.I[0] = dInfinity;
				mass.I[1] = 0.0f;
				mass.I[2] = 0.0f;

				mass.I[4] = 0.0f;;
				mass.I[5] = dInfinity;
				mass.I[6] = 0.0f;

				mass.I[8] = 0.0f;
				mass.I[9] = 0.0f;
				mass.I[10] = dInfinity;

				dBodySetMass( particles[i][j], &mass );

				dContact contact;

				contact.surface.soft_erp = 0.8f;
				contact.surface.soft_cfm = 1e-5;
				contact.surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
				contact.surface.mu = 0.1;
				contact.surface.mu2 = 0.1;
				contact.surface.bounce = 0.1;
				contact.surface.bounce_vel = 0.1;
				contact.surface.soft_cfm = 1e-5;

				dContactGeom& contactgeom = contact.geom;


				contactgeom.depth = dynamic_contact.m_ContactInfo.m_offset;

				memcpy( contactgeom.pos, dynamic_contact.m_RelAnchorPosition, Memory::SIMD_ALIGN );
				memcpy( contactgeom.normal, dynamic_contact.m_ContactInfo.m_normal, Memory::SIMD_ALIGN );


				dJointID c = dJointCreateContactSoftRigid( dWorld::id(), m_OdeContactGroup, &contact );

				dJointSetFeedback( c, &soft_contact_force[i][j] );

				//SoftBodyBase::RigidContact& rigid_contact = softbody->m_DynamicContactArray[j];

				SoftBody::RigidContact& rigid_contact = softbody->getDynamicContactArray()[j];

				dJointAttach( c, rigid_contact.m_RigidBody->dBody::id(), particles[i][j] );

			}

			//dBodyDestroy( nullbody );

		}

		//btPersistentManifold** manifold = m_CollisionWorld->getDispatcher()->getInternalManifoldPointer();

		int numManifolds = btCollisionWorld::getDispatcher()->getNumManifolds();

		for ( int i=0;i< numManifolds ;i++)
		{
			btPersistentManifold* contactManifold = btCollisionWorld::getDispatcher()->getManifoldByIndexInternal(i);
			//btCollisionObject* obA = static_cast<btCollisionObject*>(contactManifold->getBody0());
			//btCollisionObject* obB = static_cast<btCollisionObject*>(contactManifold->getBody1());

			RigidBodyBase* body1 = static_cast<RigidBodyBase*>(contactManifold->getBody0());
			RigidBodyBase* body2 = static_cast<RigidBodyBase*>(contactManifold->getBody1());


			dContact contact[4];

			for (int k=0; k<4; k++) 
			{
				// find a way to set this param for every type of contacts
				contact[k].surface.mode =  dContactSoftERP | dContactBounce | dContactSoftCFM;
				contact[k].surface.soft_erp = 0.9f;
				contact[k].surface.soft_cfm = 1e-5f;
				contact[k].surface.mu = 0.1;
				contact[k].surface.mu2 = 0.1;
				contact[k].surface.bounce = 0.1;
				contact[k].surface.bounce_vel = 0.1;
			}

			int nb_ContactPoints = contactManifold->getNumContacts();

			for (int j=0;j<nb_ContactPoints;j++)
			{
				btManifoldPoint& pt = contactManifold->getContactPoint(j);
				dContactGeom& contactgeom = contact[j].geom;

				if ( pt.getDistance() <= 0.0f )
				{

					contactgeom.depth = pt.getDistance();

					memcpy( &contactgeom.pos, &pt.getPositionWorldOnB(), sizeof(btVector3) );

					memcpy( &contactgeom.normal, &pt.m_normalWorldOnB, sizeof(btVector3) );


					//contact[j].geom = &contactgeom[j];
					//glBegin(GL_LINES);
					//glColor3f(1, 0, 1);
					//btVector3 ptA = pt.getPositionWorldOnA();
					//btVector3 ptB = pt.getPositionWorldOnB();
					//glVertex3d(ptA.x(),ptA.y(),ptA.z());
					//glVertex3d(ptB.x(),ptB.y(),ptB.z());
					//glEnd();

					dJointID c = dJointCreateContact( dWorld::id() , m_OdeContactGroup, &contact[j] );

					dJointAttach( c, body1->dBody::id(), body2->dBody::id() );
				}
			}

		}

		// Ode Step
		//dWorldQuickStep ( dWorld::id(), TimeStep );
		dWorldQuickStepTbb ( dWorld::id(), TimeStep, m_pRootTask );


		// Apply contact forces to softbodies
		for (int i=Nb_SoftBody-1; i>=0; i--)
		{
			softbody = SoftBodyArray[i];

			int nb_contacts = softbody->getDynamicContactArray().size();
			Vector3 contact_force;

			for ( int j= nb_contacts-1; j>=0 ; j-- )
			{
				int& id_node = softbody->getDynamicContactArray()[j].m_IdNode;

				memcpy( &contact_force, soft_contact_force[i][j].f2, Memory::SIMD_ALIGN );

				softbody->addForce( id_node, contact_force);

				dBodyDestroy( particles[i][j] );
			}
		}

		for (int i=Nb_SoftBody-1; i>=0; i--)
		{

			if ( soft_contact_force != 0 )
			{
				_freea(soft_contact_force[i]);
				soft_contact_force[i] = 0;
			}

			if ( particles[i] != 0 )
			{
				_freea(particles[i]);
				particles[i] = 0;
			}
		}

		if ( soft_contact_force != 0 )
		{
			_freea(soft_contact_force );
			soft_contact_force  = 0;
		}

		if ( particles  != 0 )
		{
			_freea(particles );
			particles = 0;
		}


		// remove all contact joints
		dJointGroupEmpty(m_OdeContactGroup);


		for (int i=0; i< Nb_RigidBody; i++)
		{
			rigid_body = RigidBodyArray[i];
			rigid_body->synchronizeCollision2Physics();
		}


		tasklist.empty();
		//m_pRootTask->set_ref_count(1);
		
		for (int i=0; i< Nb_SoftBody; i++)
		{
			tbb::task* solve_task = 
				new( m_pRootTask->allocate_child()) SoftBodySolverTask( SoftBodyArray[i], TimeStep );

			tasklist.push_back( *solve_task);

			//softbody = SoftBodyArray[i];

			//softbody->Solve( TimeStep );
		}

		m_pRootTask->set_ref_count( Nb_SoftBody + 1);
		m_pRootTask->spawn_and_wait_for_all( tasklist );
		//tbb::task::spawn_root_and_wait(*m_pRootTask);


		tasklist.empty();
		//m_pRootTask->set_ref_count(1);

		for (int i=0; i< Nb_SoftBody; i++)
		{
			tbb::task* dynamic_tree_update_task = 
				new( m_pRootTask->allocate_child()) DynamicTreeUpdateTask( SoftBodyArray[i] );

			tasklist.push_back( *dynamic_tree_update_task);

		}

		m_pRootTask->set_ref_count( Nb_SoftBody + 1);
		m_pRootTask->spawn_and_wait_for_all( tasklist );
		//tbb::task::spawn_root_and_wait(*m_pRootTask);


		//// Broadphase Update Bounds 
		//for (int i=0; i< Nb_SoftBody; i++)
		//{
		//	softbody = SoftBodyArray[i];

		//	//fem_soft_body->UpdateClusters();
		//	/* Bounds				*/ 
		//	softbody->updateBounds();
		//}

		//for (int i=0; i< Nb_SoftBody; i++)
		//{
		//	softbody = SoftBodyArray[i];

		//	//fem_soft_body->UpdateClusters();
		//	/* Bounds				*/ 
		//	softbody->UpdateBounds();
		//	/* Nodes				*/ 
		//	SimdVector3 velocity;
		//	for( int i=0, ni=softbody->GetNbCollisionNodes();i<ni;++i)
		//	{
		//		SoftBodyBase::CollisionNode&	n= softbody->CollisionNodes[i];
		//		SimdVector3& vel = softbody->GetVelocities()[i];
		//		//fem_soft_body->NodeDynamicTree.update(	n.leaf,
		//		//	btDbvtVolume::FromCR( btVector3(n.node->getX(),n.node->getY(),n.node->getZ()), fem_soft_body->m_collisionShape->getMargin() ),
		//		//	btVector3(vel.getX(), vel.getY(), vel.getZ()) * 1.0f/* margine */,
		//		//	fem_soft_body->m_collisionShape->getMargin()/* margine */ );

		//		SimdVector3& position = softbody->GetNodes()[i];
		//		//n.IdNode = i;

		//		//softbody->NodeDynamicTree.update(	n.leaf,
		//		//	btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->m_collisionShape->getMargin() ),
		//		//	btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
		//		//	softbody->m_collisionShape->getMargin()/* margine */ );

		//		softbody->getNodeDynamicTree().update(	n.leaf,
		//			btDbvtVolume::FromCR( btVector3(position.getX(),position.getY(),position.getZ()), softbody->getCollisionShape()->getMargin() ),
		//			btVector3(vel.getX(), vel.getY(), vel.getZ()) /* margine */,
		//			softbody->getCollisionShape()->getMargin()/* margine */ );
		//	}



			/* Faces				*/ 
			//if( fem_soft_body->FaceDynamicTree != NULL )
			//{
			//	for(int i=0;i< fem_soft_body->m_Nb_Triangles;++i)
			//	{
			//		TriangleElement& t = fem_soft_body->m_TrianglesPtr[i];
			//		const SimdVector3 v =  
			//			( fem_soft_body->m_NodesVelocity[t.n[0]],
			//			fem_soft_body->m_NodesVelocity[t.n[1]],
			//			fem_soft_body->m_NodesVelocity[t.n[2]] ) / 3.0f;
			//		//const btVector3	btv= btVector3(v.getX(), v.getY(), v.getZ() );
			//		//Face&			f=m_faces[i];
			//		//const btVector3	v=(	f.m_n[0]->m_v+
			//		//	f.m_n[1]->m_v+
			//		//	f.m_n[2]->m_v)/3;
			//		FaceDynamicTree.update(	t.m_leaf,
			//			VolumeOf(f,m_sst.radmrg),
			//			v*m_sst.velmrg,
			//			m_sst.updmrg);
			//	}
			//}
		//}


		// clear contacts
		for (int i=0; i< Nb_SoftBody; i++)
		{
			//FemSoftBodyArray[i]->DrawContacts();
			SoftBodyArray[i]->getStaticContactArray().resize(0);
			SoftBodyArray[i]->getDynamicContactArray().resize(0);
		}


		///* Optimize dbvt's		*/ 
		//for (int i=0; i< Nb_SoftBody; i++)
		//{
		//	//FemSoftBodyArray[i]->ClustersDynamicTree.optimizeIncremental(1);
		//	SoftBodyArray[i]->getNodeDynamicTree().optimizeIncremental(1);
		//}
		////FaceDynamicTree.optimizeIncremental(1);

		m_CollisionWorldInfo.m_sparsesdf.GarbageCollect();


		// Update counters
		//m_CpuStatistics.UpdateCounters( dTime);


		return  ( tick_count::now() - starTicks ).seconds();

	}


	//// Local helper class, to hold details of one processor counter.
	//class ProcessorCounter
	//{
	//public:
	//	ProcessorCounter( TCHAR *szCounterPath ) : m_hQuery(NULL), m_szCounterPath(NULL)
	//	{
	//		assert( szCounterPath != NULL );

	//		size_t pathSize = _tcslen ( szCounterPath ) + 1;
	//		m_szCounterPath = new TCHAR[pathSize];
	//		_tcscpy_s ( m_szCounterPath, pathSize, szCounterPath );
	//	}

	//	~ProcessorCounter()
	//	{
	//		if ( m_hQuery )
	//		{
	//			PdhCloseQuery( m_hQuery );
	//			m_hQuery = NULL;
	//		}

	//		if ( m_szCounterPath ) {
	//			delete [] m_szCounterPath;
	//		}
	//	}

	//	void Initialize(void)
	//	{
	//		// Create a PDH Query for reading real-time data, and add the counter to it.
	//		PDH_STATUS status = PdhOpenQuery ( NULL, 0, &m_hQuery );

	//		if ( status != ERROR_SUCCESS ) {
	//			m_hQuery = NULL;
	//		} else {
	//			// If we can't add this counter to our query, it's not a very useful
	//			// query.  Later uses of this query will always fail.  There isn't much
	//			// else we can do here, so just ignore the return value.
	//			(void) PdhAddCounter ( m_hQuery, m_szCounterPath, 0, &m_hCounter );
	//		}
	//	}

	//public:
	//	HQUERY m_hQuery;
	//	HCOUNTER m_hCounter;

	//private:
	//	ProcessorCounter() {};
	//	LPTSTR m_szCounterPath;
	//};


	//PDH_STATUS PhysicsWorldTbb::InitializeCounters()
	//{

	//	m_numCounters = 0;

	//	PDH_STATUS pdhStatus = ERROR_SUCCESS;

	//	std::vector<TCHAR*> vecCounterInstanceNames;

	//	TCHAR* szObjectNameBuffer = new TCHAR[128];
	//	static const int m_processorObjectIndex = 238;
	//	//LPTSTR szObjectNameBuffer = NULL;
	//	DWORD dwObjectNameSize = 128;

	//	//szObjectNameBuffer = new TCHAR[128];

	//	pdhStatus = PdhLookupPerfNameByIndex( NULL,		// Machine name - local machine since it's NULL
	//		m_processorObjectIndex,						// Constant index of "Processor" object
	//		szObjectNameBuffer,
	//		&dwObjectNameSize );

	//	LPTSTR szCounterListBuffer = NULL;
	//	DWORD dwCounterListSize = 0;
	//	LPTSTR szInstanceListBuffer = NULL;
	//	DWORD dwInstanceListSize = 0;

	//	if ( pdhStatus == ERROR_SUCCESS )
	//	{
	//		// Find the buffer size for the data. 
	//		pdhStatus = PdhEnumObjectItems (
	//			NULL,                   // Real time source
	//			NULL,                   // Local machine
	//			szObjectNameBuffer,		// Object to enumerate
	//			szCounterListBuffer,    // Pass NULL and 0
	//			&dwCounterListSize,     // to get length required
	//			szInstanceListBuffer,   // Buffer size 
	//			&dwInstanceListSize,    // Number of instances
	//			PERF_DETAIL_WIZARD,     // Counter detail level
	//			0);
	//	}
	//	// Under normal conditions, the Enum call will tell us that there's more data, 
	//	// and it will have set values for the sizes of buffers to return to it.
	//	// If that's not true, we'll never collect CPU data on this machine.
	//	if( pdhStatus == PDH_MORE_DATA ) 
	//	{
	//		// Allocate the buffers and try the call again.
	//		szCounterListBuffer = new TCHAR[dwCounterListSize];
	//		szInstanceListBuffer = new TCHAR[dwInstanceListSize];

	//		assert ( ( szCounterListBuffer != NULL ) && ( szInstanceListBuffer != NULL ) );
	//		
	//		if ( ( szCounterListBuffer != NULL ) && ( szInstanceListBuffer != NULL ) )
	//		{

	//			pdhStatus = PdhEnumObjectItems (
	//				NULL,                 // Real time source
	//				NULL,                 // Local machine
	//				szObjectNameBuffer,	  // Object to enumerate
	//				szCounterListBuffer,  // Buffer to receive counter list
	//				&dwCounterListSize,
	//				szInstanceListBuffer, // Buffer to receive instance list 
	//				&dwInstanceListSize,
	//				PERF_DETAIL_WIZARD,   // Counter detail level
	//				0);

	//			LPTSTR					szThisInstance          = NULL;

	//			// Now find all instances of that counter: (0), (1), ... (_Total).
	//			if ( pdhStatus == ERROR_SUCCESS ) 
	//			{
	//				// Walk the instance list. The list can contain one
	//				// or more null-terminated strings. The last string 
	//				// is followed by a second null-terminator.
	//				for ( szThisInstance = szInstanceListBuffer;
	//					*szThisInstance != _T( '\0' );
	//					szThisInstance += _tcslen( szThisInstance ) + 1 ) 
	//				{
	//						// Object name and counter list don't change, but build
	//						// a complete counter path from its various parts, which
	//						// will look like "\\Processor(_Total)\\% Processor Time" etc.
	//						TCHAR szFormat[] = _T ( "\\%s(%s)\\%s" );
	//						size_t pathSize = _tcslen ( szFormat ) +
	//							_tcslen ( szObjectNameBuffer ) +
	//							_tcslen ( szThisInstance ) +
	//							_tcslen ( szCounterListBuffer ) + 1;
	//						// The pathSize is actually a bit bigger than it needs to be, since the
	//						// format string characters (e.g. "%s") don't take any space in the
	//						// final string.

	//						LPTSTR szCounterPath = new TCHAR[ pathSize ];

	//						_stprintf_s ( szCounterPath,
	//							pathSize,
	//							szFormat,
	//							szObjectNameBuffer,				// "Processor" in English
	//							szThisInstance,					// "0", "1", etc., as well as "_Total", in all languages
	//							szCounterListBuffer );			// "% Processor Time" in English

	//						vecCounterInstanceNames.push_back ( szCounterPath );
	//						m_numCounters++;
	//				}
	//			}
	//		}
	//	}

	//	if (szCounterListBuffer != NULL)
	//	{
	//		delete [] szCounterListBuffer;
	//	}

	//	if (szInstanceListBuffer != NULL)
	//	{
	//		delete [] szInstanceListBuffer;
	//	}

	//	if ( szObjectNameBuffer != NULL )
	//	{
	//		delete [] szObjectNameBuffer;
	//	}


	//	m_CPUCount = m_numCounters - 1;
	//	if ( m_CPUCount <= 0)
	//	{
	//		m_CPUCount = 1;
	//	}

	//	m_CPUPercentCounters = new double[m_numCounters];
	//	for (int i = 0; i < m_numCounters; i++) 
	//	{
	//		m_CPUPercentCounters[i] = 0.0f;
	//	}


	//	// Initialize all processor counters and capture them in the global counter vector.
	//	if ( m_numCounters > 0 ) {
	//		std::vector< TCHAR * >::iterator iter;
	//		for( iter = vecCounterInstanceNames.begin(); iter != vecCounterInstanceNames.end(); ++iter )
	//		{
	//			ProcessorCounter *procCounter = new ProcessorCounter( *iter );
	//			procCounter->Initialize();
	//			m_vecProcessorCounters.push_back( procCounter );
	//		}

	//		// We have the counter name copied in the ProcessorCounter object, so clean up
	//		// the name we used here.
	//		TCHAR* pTemp = NULL;
	//		while( !vecCounterInstanceNames.empty() ) {
	//			pTemp = vecCounterInstanceNames.back();
	//			vecCounterInstanceNames.pop_back();
	//			delete [] pTemp;
	//		}
	//	}


	//	return pdhStatus;
	//}


	//void PhysicsWorldTbb::UpdateCounters( float& dTime)
	//{

	//	std::vector< void * >::iterator voidIter;

	//	static float time = 0.0f;
	//	time += dTime;

	//	if ( time > 500 )
	//	{

	//		time = 0.0f;

	//		if ( m_vecProcessorCounters.empty() ) 
	//		{
	//			// In the empty case (can't read counters), just return zeros for CPU percent.
	//			for ( int i = 0; i < m_numCounters; i++ )
	//			{
	//				m_CPUPercentCounters[i] = 0.0f;
	//			}
	//		} 
	//		else 
	//		{
	//			// Get current counter values.
	//			int i = 0;
	//			for( voidIter = m_vecProcessorCounters.begin();
	//				voidIter != m_vecProcessorCounters.end();
	//				++voidIter, i++ ) 
	//			{
	//				// Start with a zero, in case there's some error.
	//				m_CPUPercentCounters[i] = 0.0f;

	//				ProcessorCounter* counterInstance = (ProcessorCounter *)*voidIter;

	//				// Read all current counters via their handles.  If some error condition
	//				// has left us with a NULL query handle, the call will just fail and we'll
	//				// still have a zero counter this time.
	//				PDH_STATUS status = PdhCollectQueryData( counterInstance->m_hQuery );
	//				if ( status == ERROR_SUCCESS ) {
	//					// In rare cases, PdhGetFormattedCounterValue can return bad values (<0 or >100)
	//					// when called in this pattern.  If that becomes a problem, it's possible to
	//					// work around the problem by adding a short sleep (e.g. 10 msec) and a second
	//					// call to PdhCollectQueryData, before calling PdhGetFormattedCounterValue.
	//					// In testing, we haven't seen the problem.  Any such problem that occurs on a
	//					// single read will tend to not occur on the next read, so any such problem
	//					// is transient enough to ignore.

	//					PDH_FMT_COUNTERVALUE value;
	//					// Get the actual value out of this counter query.
	//					// Just pass a NULL for type, since we already know the type of this value.
	//					status = PdhGetFormattedCounterValue( counterInstance->m_hCounter, PDH_FMT_DOUBLE, NULL, &value );

	//					// Make sure we have valid or new data (new also implies valid), and cache it.
	//					if ( status == ERROR_SUCCESS &&
	//						( value.CStatus == PDH_CSTATUS_NEW_DATA ||
	//						value.CStatus == PDH_CSTATUS_VALID_DATA ) )
	//					{
	//						m_CPUPercentCounters[i] = (double)value.doubleValue;
	//					}
	//				}
	//			}
	//		}
	//	}
	//}

	//void PhysicsWorldTbb::DeAllocateCounters()
	//{

	//	// Delete all locally-allocated things.
	//	delete [] m_CPUPercentCounters;

	//	// Clean up queries.
	//	while( !m_vecProcessorCounters.empty() ) {
	//		ProcessorCounter *pCounterObject = (ProcessorCounter *)m_vecProcessorCounters.back();
	//		m_vecProcessorCounters.pop_back();
	//		delete pCounterObject;
	//	}

	//}
}