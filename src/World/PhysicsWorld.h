#ifndef _SOFTBODY_WORLD_
#define _SOFTBODY_WORLD_

#include <World/PhysicsWorldParams.h>

#include "Collision/SoftBodyCollisionWorldInfo.h"

#include "btBulletCollisionCommon.h"

#include "Core/PhysicsClasses.h"

#include <vector>

// ode test
//#include "ode/ode.h"
#include "ode/ode.h"


#include "Collision/btSoftBodyRigidBodyCollisionConfiguration.h"

//#include "../../bullet-2.72/src/LinearMath/btPoolAllocator.h"

#include "Utilities/Common.h"

//#include "Core/PhysicsClasses.h"

// forward decl
class btCollisionObject;

//class btPoolAllocator;

#include "Core/CollisionWorld.h"
#include "Core/DynamicsWorld.h"

//// forward decl
//namespace Interface
//{
//	class ITaskManager;
//}


namespace SimStep {



	typedef std::vector<RigidBody*> RigidBodies;
	typedef std::vector<SoftBody*> SoftBodies;
	typedef std::vector<Constraint*> Constraints;

	// forward decl
	
	class RigidBodyBase;

	class SoftBody;



	enum PhysicsWorldType
	{
		SS_COLLISION		= 1,
		SS_RIGID_BODY		= 2,
		SS_SOFT_BODY		= 4,
		SS_SINGLE_CORE		= 8,
		SS_MULTI_CORE		= 16
	};


	struct PhysicsWorldInfo
	{
		String mTypeName;
		String mDescription;
		PhysicsWorldType  mPhysicsWorldTypeMask;
	};

	//class _SimStepExport PhysicsWorldParam : public CollisionWorldParams
	//	, public DynamicsWorldParams
	//{
	//public:

	//	String mName;
	//	
	//	bool mInitializeScheduler;


	//	//Interface::ITaskManager* mTaskManager;

	//	//CollisionWorldParams collWorldParams;
	//	//DynamicsWorldParams dynamicsWorldParams;

	//	PhysicsWorldParam();
	//	~PhysicsWorldParam();
	//};

	class RigidBody;
	class CollisionObject;

	class _SimStepExport PhysicsWorld : public CollisionWorld , public DynamicsWorld
	{

	public:

		//SIMSTEP_DECLARE_CLASS_ALLOCATOR(PhysicsWorld, MEMCATEGORY_ROOT );


		PhysicsWorld( const PhysicsWorldParam& physicsWorldParams,
			SceneManager* sceneMgr = 0 );

		PhysicsWorld( const CollisionWorldParams& collWorldParams, 
			const DynamicsWorldParams& dynamicsWorldParams,
			SceneManager* sceneMgr = 0 );

		virtual ~PhysicsWorld();

		inline void setSceneManager(SceneManager* sceneMgr ) { mSceneMgr = sceneMgr;}

		virtual float simulate( const float& TimeStep, SceneManager* scene = 0, 
			int simTypeFlag = simType_invalid,	
			void* spawnedTask = NULL ) = 0;

		virtual const String& getName(void) const;
		virtual void release(void);
		virtual String getErrorDescription(long errorNumber) const = 0;


		CollisionWorld*	getCollisionWorld(void);

		DynamicsWorld* getDynamicsWorld(void);



	protected:
		virtual void addCollisionObject( CollisionObject* collObject,
			short int collisionFilterGroup = btBroadphaseProxy::DefaultFilter,
			short int collisionFilterMask = btBroadphaseProxy::AllFilter );

		Bool removeCollisionObject( CollisionObject* collObject );

		void addRigidBody(RigidBody* rbody);
		Bool removeRigidBody( RigidBody* rbody );

		void addSoftBody(SoftBody* sbody);
		Bool removeSoftBody( SoftBody* sbody );



		// utility: call this at the end of simulate
		void SDFgarbageCollect(void)
		{	mCollisionWorldInfo.m_sparsesdf.GarbageCollect(); }

		//// utility: should be inline
		//void getCollisionObjects(void);
		//void buildOdeRigidBodyList(void);
		//void destroyOdeRigidBodyList(void);

	protected:

		SceneManager* mSceneMgr;

		uint mSimType;
		PhysicsWorldInfo m_WorldInfo;

		//Interface::ITaskManager* mTaskManager;

		RigidBodies mRigidBodies;

		SoftBodies mSoftBodies;

		Constraints mConstraints;



		RigidBodies::iterator mRigidBodiesIterator;

		SoftBodies::iterator mSoftBodiesIterator;

		Constraints::iterator mConstraintsIterator;


		friend class SceneManager;

	};


	class _SimStepExport World : public btCollisionWorld , public dWorld
	{

	public:
		//World( btCollisionConfiguration* CollisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration()
		//	);

		World(
			btCollisionDispatcher* Dispatcher, 
			btBroadphaseInterface* PairCache, 
			btCollisionConfiguration* CollisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration()
			);

		virtual ~World();

		void DeAllocate();

		PhysicsWorldInfo m_WorldInfo;

		SoftBodyCollisionWorldInfo	m_CollisionWorldInfo;	// Collision World info

		void InitializeCollision( btCollisionDispatcher* dispatcher, 
			btBroadphaseInterface* broadphase, 
			btCollisionConfiguration* collisionConfiguration = NULL );

		void InitializeOdeWorld( dWorldID OdeWorldID /*, dSpaceID OdeSpaceID,*/ ,dJointGroupID OdeContactGroupID = NULL  );

		//virtual float simulate( const float& TimeStep, SceneManager* scene = 0 );

		void Add(SoftBody* body);
		void Add(RigidBodyBase* body);
		void Add(btCollisionObject* CollisionObject );


		BOOL Remove( SoftBody* body );
		BOOL Remove( RigidBodyBase* body );
		BOOL Remove( btCollisionObject* CollisionObject );


		btCollisionWorld*	getCollisionWorld();
		dWorldID getOdeWorld();

		SoftBody** getSoftBodyArray();
		std::vector<SoftBody*>& getSoftBodyVector();


		virtual const String& getName(void) const;
		virtual void release(void);
		virtual String getErrorDescription(long errorNumber) const = 0;

	protected:

		SceneManager* mSceneMgr;

		std::vector<SoftBody*> SoftBodyArray;
		std::vector<RigidBodyBase*> RigidBodyArray;

		std::vector<SoftBody*>::iterator SoftBodyIterator;
		std::vector<RigidBodyBase*>::iterator RigidBodyIterator;

		// Bullet Collision
		//btCollisionWorld*	m_CollisionWorld;
		btCollisionConfiguration* m_CollisionConfiguration;
		btBroadphaseInterface* m_PairCache;
		btCollisionDispatcher* m_Dispatcher;

		//// Ode Dynamics
		dJointGroupID m_OdeContactGroup;

		//btPoolAllocator m_PoolAllocator;

		// time utility
		float CPU_FREQ;

	};


}; // namespace SimStep

#endif