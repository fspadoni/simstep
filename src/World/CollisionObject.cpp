#include "World/CollisionObject.h"


#include "World//RigidBody.h"

#include "Core/CollisionShape.h"

namespace SimStep
{

	CollisionObjectParams::CollisionObjectParams()
		: mPosition( Vector3::Zero() )
		, mOrientation( Matrix3::identity() )
		, mCollisionShape(0)
		, mCollisionMargin( 0.05 )
		, collisionFilterGroup(btBroadphaseProxy::DefaultFilter)
		, collisionFilterMask(btBroadphaseProxy::AllFilter)
	{
		////mPosition = Vector3::Zero();
		////mOrientation = Matrix3::identity();
		////collisionFilterGroup = btBroadphaseProxy::DefaultFilter;
		////collisionFilterMask = btBroadphaseProxy::AllFilter
	}



	CollisionObject::CollisionObject( const CollisionObjectParams& params )
		: btCollisionObject()
	{
		mName = params.name;
		m_worldTransform.setOrigin(btVector3(params.mPosition[0],params.mPosition[1], params.mPosition[2]) );
		m_worldTransform.setBasis(btMatrix3x3(
			params.mOrientation[0][0], params.mOrientation[1][0], params.mOrientation[2][0],
			params.mOrientation[0][1], params.mOrientation[1][1], params.mOrientation[2][1],
			params.mOrientation[0][2], params.mOrientation[1][2], params.mOrientation[2][2] 
		) );
		
		if ( params.mCollisionShape )
		{
			setCollisionShape(params.mCollisionShape);
			m_collisionShape->setMargin( params.mCollisionMargin );
		}
		
		
		btCollisionObject::m_internalType = CO_COLLISION_OBJECT;
	}

	
	CollisionObject::~CollisionObject()
	{

	}


}; // namespace SimStep