#ifndef SimStep_XMLSceneSerializer_h__
#define SimStep_XMLSceneSerializer_h__

#include "Utilities/Common.h"
#include "Math/Math.h"

class TiXmlDocument;
class TiXmlElement;

namespace SimStep
{
	class TetraMeshPtr;


	class _SimStepExport XMLSceneSerializer
	{
	public:

		XMLSceneSerializer();
		virtual ~XMLSceneSerializer();


		//void importScene(const String& filename, NodeElementType colourElementType, TetraMesh* pMesh);

		/** Exports a mesh to the named XML file. */
		void exportSceneInfo(void);
		TiXmlElement* exportNodeTM(const Vector3& pos, const Quat& orientation, const Vector3& scale, const String& name);

		TiXmlElement* exportNodeInfo(const Vector3& pos, const Quat& orientation, 
			const Vector3& scale, const String& name, TiXmlElement* elemNode );
		
		TiXmlElement* exportPhysicsEntity( TetraMeshPtr tetraMesh, 
			const String& tetrafilename, TiXmlElement* pXmlElem );

		void writeFile(const String& filename);

	private:
		// State for export
		TiXmlDocument* mXMLDoc;

	};
	

	
}; // namespace SimStep

#endif // SimStep_XMLSceneSerializer_h__