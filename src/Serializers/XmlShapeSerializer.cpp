#include <Serializers/XmlShapeSerializer.h>

#include <Core/Shape.h>
#include <Core/LogManager.h>
#include <Core/SimdMemoryBufferManager.h>

#include <Serializers/tinyxml.h>


#include <Utilities/StringConverter.h>

namespace SimStep
{

	XmlShapeSerializer::XmlShapeSerializer()
	{
	}

	XmlShapeSerializer::~XmlShapeSerializer()
	{
	}


	void XmlShapeSerializer::import(const String& stream, Shape* pShape)
	{
		LogManager::getSingleton().logMessage("XmlShapeSerializer reading shape data from stream"/* + filename + "..." */);
		mpShape = pShape;

		TiXmlDocument xmlDoc;
		xmlDoc.Parse( stream.c_str() , 0 );

		TiXmlElement* shapeElem = xmlDoc.RootElement();

		const char* shapeType = shapeElem->Attribute("type");

		if ( strcmp(shapeType, "PolyLine") == 0 )
		{
			readPolyLine( shapeElem, mpShape);		
		}
		else
		{

		}


	}

	void XmlShapeSerializer::export(const Shape* pShape, const String& filename)
	{

		LogManager::getSingleton().logMessage("XmlShapeSerializer writing shape data to " + filename + "...");

		mpShape = const_cast<Shape*>(pShape);

		TiXmlDocument xmlDoc;
		//TiXmlElement* root = xmlDoc.RootElement();

		LogManager::getSingleton().logMessage("Populating DOM...");

		xmlDoc.InsertEndChild(TiXmlElement("shape"));
		TiXmlElement* shapeElems = xmlDoc.RootElement();
	
		const Shape::Type shapeType = pShape->getType();
		
		switch ( shapeType )
		{
		case Shape::PolyLine:
			shapeElems->SetAttribute("type", "PolyLine" );
			writePolyLine( shapeElems, mpShape);
			break;
		}

		LogManager::getSingleton().logMessage("DOM populated, writing XML file..");

		// Write out to a file
		if(!xmlDoc.SaveFile(filename.c_str()) )
		{
			LogManager::getSingleton().logMessage("XmlShapeSerializer failed writing the XML file.", LML_CRITICAL);
		}
		else
		{
			LogManager::getSingleton().logMessage("XmlShapeSerializer export successful.");
		}

	
	}

	

	void XmlShapeSerializer::writePolyLine(TiXmlElement* shapeNode, Shape* pShape )
	{
		writePoints(shapeNode,pShape->mPoints );
	}

	void XmlShapeSerializer::readPolyLine(TiXmlElement* shapeNode, Shape* pShape )
	{
		readPoints(shapeNode,pShape->mPoints );
	}
	


	void XmlShapeSerializer::writePoints(TiXmlElement* parentNode, BufferSharedPtr<Vector3>& pointsbuf)
	{
		
		TiXmlElement* pointsElems = 
			parentNode->InsertEndChild(TiXmlElement("points"))->ToElement();

		int count = pointsbuf->getSize();
		pointsElems->SetAttribute("count", StringConverter::toString( count ) );
		
		const Vector3* pPoints = pointsbuf->lockForRead();

		while ( count-- > 0 )
		{
			TiXmlElement* pointElems = 
				pointsElems->InsertEndChild(TiXmlElement("point"))->ToElement();

			pointElems->SetAttribute("x", StringConverter::toString( pPoints->getX() ) );
			pointElems->SetAttribute("y", StringConverter::toString( pPoints->getY() ) );
			pointElems->SetAttribute("z", StringConverter::toString( pPoints->getZ() ) );

			++pPoints;
		}

		pointsbuf->unlock();		
	
	}


	void XmlShapeSerializer::readPoints(TiXmlElement* parentNode, BufferSharedPtr<Vector3>& pointsbuf)
	{

		TiXmlElement* pPointsElem = parentNode->FirstChildElement();
		int claimedCount = StringConverter::parseInt( pPointsElem->Attribute("count") );

		// calculate how many vertexes there actually are
		int actualPointsCount = 0;
		for (TiXmlElement * pointElem = pPointsElem->FirstChildElement(); 
			pointElem != 0; pointElem = pointElem->NextSiblingElement() )
		{
			++actualPointsCount;
		}
		if ( claimedCount && claimedCount != actualPointsCount )
		{
			LogManager::getSingleton().stream()
				<< "WARNING: point count (" << actualPointsCount 
				<< ") is not as claimed (" << claimedCount << ")";
		}

		pointsbuf = BufferManager::getSingleton().createBuffer<Vector3>(claimedCount);
		
		Vector3* pPoints = pointsbuf->lockForWrite<Vector3>();
		// Iterate over all children (node entries) 
		for (TiXmlElement* pointElem = pPointsElem->FirstChildElement();
			pointElem != 0; pointElem = pointElem->NextSiblingElement() )
		{
			pPoints->setX( StringConverter::parseReal(pointElem->Attribute("x")) );
			pPoints->setY( StringConverter::parseReal(pointElem->Attribute("y")) );
			pPoints->setZ( StringConverter::parseReal(pointElem->Attribute("z")) );
			++pPoints;
		}
		pointsbuf->unlock();

		LogManager::getSingleton().logMessage("points done");

	}



}; // namespace SimStep