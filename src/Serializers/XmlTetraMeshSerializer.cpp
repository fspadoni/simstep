#include "Serializers/XMLTetraMeshSerializer.h"

#include "Core/TetraMesh.h"
#include "Core/SubTetraMesh.h"

#include "Serializers/tinyxml.h"

#include "Core/SimdMemoryBufferManager.h"

//#include "Utilities/NodeElementData.h"
#include "Core/LogManager.h"
#include "Utilities/String.h"
#include "Utilities/StringConverter.h"
#include "Utilities/PhysicsException.h"

#include "Ogre/ColourValue.h"

namespace SimStep
{

	XmlTetraMeshSerializer::XmlTetraMeshSerializer()
	{
	}
	
	XmlTetraMeshSerializer::~XmlTetraMeshSerializer()
	{
	}

	void XmlTetraMeshSerializer::importMesh(const String& stream,  TetraMesh* pMesh)
	{
		LogManager::getSingleton().logMessage("XMLTetraMeshSerializer reading tetra mesh data from stream"/* + filename + "..." */);
		mpMesh = pMesh;
		//mColourElementType = colourElementType;
		// import from file
		//mXMLDoc = new TiXmlDocument(filename.c_str());
		//mXMLDoc->LoadFile();
		// import from stream
		mXMLDoc = new TiXmlDocument();
		mXMLDoc->Parse( stream.c_str() , 0 );

		TiXmlElement* elem;

		TiXmlElement* rootElem = mXMLDoc->RootElement();

		int claimedCount = 0;
		// surface mesh
		elem = rootElem->FirstChildElement("sharednodes");
		if (elem)
		{
			TiXmlElement* nodeElem = elem->FirstChildElement("nodes");
			claimedCount = StringConverter::parseInt( nodeElem->Attribute("count") );
			if( claimedCount > 0)
			{
				
				mpMesh->mSharedNodes = 
					BufferManager::getSingleton().createBuffer<Vector3>(claimedCount);

				readNodes(nodeElem, mpMesh->mSharedNodes);
			}

		}

		elem = rootElem->FirstChildElement("constrainednodes");
		if (elem)
		{
			//TiXmlElement* nodeElem = elem->FirstChildElement("nodes");
			claimedCount = StringConverter::parseInt( elem->Attribute("count") );
			if( claimedCount > 0)
			{

				mpMesh->mSharedConstrainedNodes = 
					BufferManager::getSingleton().createBuffer<ushort>(claimedCount);

				readConstrainedNodes(elem, mpMesh->mSharedConstrainedNodes);
			}

		}

		// graph
		elem = rootElem->FirstChildElement("graph");
		if (elem)
		{
			readGraph(elem, &mpMesh->mGraph );
		}

		// submeshes
		elem = rootElem->FirstChildElement("surfacemesh");
		if (elem)
		{
			TiXmlElement* facesElem = elem->FirstChildElement("faces");
			claimedCount = StringConverter::parseInt( facesElem->Attribute("count") );
			if( claimedCount > 0)
			{
				mpMesh->mFaces = 
					BufferManager::getSingleton().createBuffer<ushort>(3*claimedCount);

				readSurfaceMesh(facesElem, mpMesh->mFaces);
			}
		}

		// subtetrameshes
		elem = rootElem->FirstChildElement("subtetrameshes");
		if (elem)
		{
			readSubTetraMeshes(elem);
		}

	
		// submesh names
		elem = rootElem->FirstChildElement("subtetrameshnames");
		if (elem)
		{
			readSubTetraMeshNames(elem, mpMesh);
		}

		// subtetrameshes
		elem = rootElem->FirstChildElement("submeshes");
		if (elem)
		{
			readSubMeshes(elem);
		}


		// submesh names
		elem = rootElem->FirstChildElement("submeshnames");
		if (elem)
		{
			readSubMeshNames(elem, mpMesh);
		}


		delete mXMLDoc;

		LogManager::getSingleton().logMessage("XMLTetraMeshSerializer import successful.");

	}

	
	void XmlTetraMeshSerializer::exportMesh(const TetraMesh* pMesh, const String& filename)
	{
		LogManager::getSingleton().logMessage("XMLMeshSerializer writing tetra mesh data to " + filename + "...");

		mpMesh = const_cast<TetraMesh*>(pMesh);

		mXMLDoc = new TiXmlDocument();
		mXMLDoc->InsertEndChild(TiXmlElement("mesh"));

		LogManager::getSingleton().logMessage("Populating DOM...");



		// Write to DOM
		writeMesh(pMesh);
		LogManager::getSingleton().logMessage("DOM populated, writing XML file..");

		// Write out to a file
		if(! mXMLDoc->SaveFile(filename.c_str()) )
		{
			LogManager::getSingleton().logMessage("XMLTetraMeshSerializer failed writing the XML file.", LML_CRITICAL);
		}
		else
		{
			LogManager::getSingleton().logMessage("XMLTetraMeshSerializer export successful.");
		}

		delete mXMLDoc;

	}


	void XmlTetraMeshSerializer::writeMesh(const TetraMesh* pMesh)
	{
		TiXmlElement* rootNode = mXMLDoc->RootElement();
		// Write shared nodes
		if ( !pMesh->mSharedNodes.isNull() )
		{
			
			TiXmlElement* geomNode = 
				rootNode->InsertEndChild(TiXmlElement("sharednodes"))->ToElement();
			writeNodes(geomNode, pMesh->mSharedNodes );


			//// Write Submeshes
			//TiXmlElement* surfaceMeshNode = 
			//	rootNode->InsertEndChild(TiXmlElement("surfacemesh"))->ToElement();

			//// Collision Node name
			//surfaceMeshNode->SetAttribute("collnodes", pMesh->mFaces->indexCount );

			//// Collision Node name
			//surfaceMeshNode->SetAttribute("collnodes", pMesh->mCollisionNodes->nodeCount );

			//// Material name
			//surfaceMeshNode->SetAttribute("material", pMesh->getMaterialName());			
		}

		if ( !pMesh->mSharedConstrainedNodes.isNull() )
		{

			TiXmlElement* geomNode = 
				rootNode->InsertEndChild(TiXmlElement("constrainednodes"))->ToElement();
			writeConstrainedNodes(geomNode, pMesh->mSharedConstrainedNodes );
		}

		if ( !pMesh->mGraph.columns.isNull() )
		{
			writeGraph(rootNode, pMesh->mGraph );
		}

		if ( !pMesh->mFaces.isNull() )
		{
			TiXmlElement* surfaceMeshNode = 
				rootNode->InsertEndChild(TiXmlElement("surfacemesh"))->ToElement();

			writeFaces(surfaceMeshNode, pMesh->mFaces);
		}



		// Write SubTetrameshes
		TiXmlElement* subTetraMeshesNode = 
			rootNode->InsertEndChild(TiXmlElement("subtetrameshes"))->ToElement();

		for (int i = 0; i < pMesh->getNumSubTetraMeshes(); ++i)
		{
			LogManager::getSingleton().logMessage("Writing subtetramesh...");
			writeSubTetraMesh(subTetraMeshesNode, pMesh->getSubTetraMesh(i));
			LogManager::getSingleton().logMessage("subtetramesh exported.");
		}

		// Write submesh names
		writeSubTetraMeshNames(rootNode, pMesh);

		// Write Submeshes
		TiXmlElement* subMeshesNode = 
			rootNode->InsertEndChild(TiXmlElement("submeshes"))->ToElement();
		
		for (int i = 0; i < pMesh->getNumSubMeshes(); ++i)
		{
			LogManager::getSingleton().logMessage("Writing submesh...");
			writeSubMesh(subMeshesNode, pMesh->getSubMesh(i));
			LogManager::getSingleton().logMessage("Submesh exported.");
		}

	
		// Write submesh names
		writeSubMeshNames(rootNode, pMesh);

	}

	void XmlTetraMeshSerializer::writeFaces(TiXmlElement* surfaceMeshNode, 
		const BufferSharedPtr<ushort>& facebuf)
	{

		size_t numFaces;

		//// Material name
		//surfaceMeshNode->SetAttribute("material", s->getMaterialName());


		//bool use32BitIndexes = (!pFaces->elementBuffer.isNull() && 
		//	pFaces->elementBuffer->getIndexType() == ElementMemoryBuffer::IT_32BIT);

		//surfaceMeshNode->SetAttribute("use32bitindexes", 
		//	StringConverter::toString( use32BitIndexes ));

		//surfaceMeshNode->SetAttribute("operationtype", "triangle_list");


		if ( facebuf->getSize() > 0)
		{
			// Faces
			TiXmlElement* facesNode = 
				surfaceMeshNode->InsertEndChild(TiXmlElement("faces"))->ToElement();
	
			// tri list
			numFaces = facebuf->getSize() / 3;
			
			facesNode->SetAttribute("count", 
				StringConverter::toString(numFaces));
			
			// Write each face in turn
			size_t i;
			unsigned int* pInt;
			const unsigned short* pShort;
			//ElementMemoryBufferSharedPtr ibuf = pFaces->elementBuffer;
			//if (use32BitIndexes)
			//{
			//	pInt = static_cast<unsigned int*>(
			//		ibuf->lock( 0, ibuf->getSizeInBytes(), SimdMemoryBuffer::HBL_READ_ONLY ) ); 
			//}
			//else
			{
				//pShort = static_cast<unsigned short*>(
					//ibuf->lock(0, ibuf->getSizeInBytes(), SimdMemoryBuffer::HBL_READ_ONLY) ); 
			}

			pShort = facebuf->lockForRead<unsigned short>();

			for (i = 0; i < numFaces; ++i)
			{
				TiXmlElement* faceNode = 
					facesNode->InsertEndChild(TiXmlElement("face"))->ToElement();
				//if (use32BitIndexes)
				//{
				//	faceNode->SetAttribute("n1", StringConverter::toString(*pInt++));
				//	faceNode->SetAttribute("n2", StringConverter::toString(*pInt++));
				//	faceNode->SetAttribute("n3", StringConverter::toString(*pInt++));
				//}
				//else
				{
					faceNode->SetAttribute("n1", StringConverter::toString(*pShort++));
					faceNode->SetAttribute("n2", StringConverter::toString(*pShort++));
					faceNode->SetAttribute("n3", StringConverter::toString(*pShort++));
				}
			}

			facebuf->unlock();
		}
	}


	void XmlTetraMeshSerializer::writeVertices(TiXmlElement* verticesNode, 
		const Vertices& vertices)
	{

		size_t nbVertices = vertices.getSize();
		// Set num verts on parent
		verticesNode->SetAttribute("count", StringConverter::toString( nbVertices ));

		// Write each face in turn
		size_t i;
		const ushort*  pShort;
		const Vector3* pVec;

		pShort = vertices.tetraIndex->lockForRead<ushort>();
		pVec = vertices.barycentricCoords->lockForRead<Vector3>();

		for (i = 0; i < nbVertices; ++i )
		{
			TiXmlElement* vertexnode = 
				verticesNode->InsertEndChild(TiXmlElement("vertex"))->ToElement();

			TiXmlElement* indexnode = 
				vertexnode->InsertEndChild(TiXmlElement("tetra"))->ToElement();

			indexnode->SetAttribute("index", StringConverter::toString(*pShort++) );

			TiXmlElement* barycentricsCoordsNode = 
				vertexnode->InsertEndChild(TiXmlElement("barycentricsCoords"))->ToElement();

			barycentricsCoordsNode->SetAttribute("l1", StringConverter::toString(pVec->getX()));
			barycentricsCoordsNode->SetAttribute("l2", StringConverter::toString(pVec->getY()));
			barycentricsCoordsNode->SetAttribute("l3", StringConverter::toString(pVec->getZ()));
			++pVec;
		}

		vertices.barycentricCoords->unlock();
		vertices.tetraIndex->unlock();		

	}

	void XmlTetraMeshSerializer::writeVertexNormals(TiXmlElement* normalsNode, const BufferSharedPtr<Vector3>& vNormalsBuf)
	{
		size_t nbNormals = vNormalsBuf->getSize();
		// Set num verts on parent
		normalsNode->SetAttribute("count", StringConverter::toString( nbNormals ));

		// Write each face in turn
		size_t i;
		const Vector3* pVec;

		pVec = vNormalsBuf->lockForRead<Vector3>();

		for (i = 0; i < nbNormals; ++i )
		{
			TiXmlElement* normalnode = 
				normalsNode->InsertEndChild(TiXmlElement("normal"))->ToElement();

			normalnode->SetAttribute("x", StringConverter::toString(pVec->getX()));
			normalnode->SetAttribute("y", StringConverter::toString(pVec->getY()));
			normalnode->SetAttribute("z", StringConverter::toString(pVec->getZ()));
			++pVec;
		}

		vNormalsBuf->unlock();
	}


	void XmlTetraMeshSerializer::writeSubTetraMesh(TiXmlElement* mSubTetraMeshesNode, const SubTetraMesh* s)
	{

		TiXmlElement* subTetraMeshNode = 
			mSubTetraMeshesNode->InsertEndChild(TiXmlElement("subtetramesh"))->ToElement();

		size_t numTetras;

		subTetraMeshNode->SetAttribute("type", "tetrahedron");
		
		subTetraMeshNode->SetAttribute("material", s->getMaterialName());

		subTetraMeshNode->SetAttribute("usesharednodes", 
			StringConverter::toString(s->mUseSharedNodes) );
		
		// bool use32BitIndexes
		//bool use32BitIndexes = (!s->mElements->elementBuffer.isNull() && 
			//s->mElements->elementBuffer->getIndexType() == ElementMemoryBuffer::IT_32BIT);
		
		subTetraMeshNode->SetAttribute("use32bitindexes", 
			StringConverter::toString( false ));

		if ( s->mTetrahedrons->getSize() > 0 )
			{
				// Faces
				TiXmlElement* tetrasNode = 
					subTetraMeshNode->InsertEndChild(TiXmlElement("elements"))->ToElement();
				
				const BufferSharedPtr<ushort>& tetbuf = s->mTetrahedrons;

				// tri list
				numTetras = tetbuf->getSize() / 4;

				tetrasNode->SetAttribute("count", 
					StringConverter::toString(numTetras));
				
				size_t i;
				const unsigned short* pShort;

				
				//if (use32BitIndexes)
				//{
				//	pInt = static_cast<unsigned int*>(
				//		ibuf->lock( 0, ibuf->getSizeInBytes(), SimdMemoryBuffer::HBL_READ_ONLY ) ); 
				//}
				//else
				{
					pShort = tetbuf->lockForRead<unsigned short>(); 
				}

				for (i = 0; i < numTetras; ++i)
				{
					TiXmlElement* tetraNode = 
						tetrasNode->InsertEndChild(TiXmlElement("element"))->ToElement();
					//if (use32BitIndexes)
					//{
					//	tetraNode->SetAttribute("n1", StringConverter::toString(*pInt++));
					//	tetraNode->SetAttribute("n2", StringConverter::toString(*pInt++));
					//	tetraNode->SetAttribute("n3", StringConverter::toString(*pInt++));
					//	tetraNode->SetAttribute("n4", StringConverter::toString(*pInt++));		
					//}
					//else
					{
						tetraNode->SetAttribute("n1", StringConverter::toString(*pShort++));
						tetraNode->SetAttribute("n2", StringConverter::toString(*pShort++));
						tetraNode->SetAttribute("n3", StringConverter::toString(*pShort++));
						tetraNode->SetAttribute("n4", StringConverter::toString(*pShort++));
					}
				}

				tetbuf->unlock();
			}


		if (!s->mUseSharedNodes )
		{
			TiXmlElement* geomNode = 
				subTetraMeshNode->InsertEndChild(TiXmlElement("nodes"))->ToElement();
			writeNodes(geomNode, s->mUnShareNodes );
		}


	}
	

	void XmlTetraMeshSerializer::writeSubMesh(TiXmlElement* mSubMeshesNode, const SubMesh* s)
	{
		TiXmlElement* subMeshNode = 
			mSubMeshesNode->InsertEndChild(TiXmlElement("submesh"))->ToElement();

		size_t numTriangles;

		subMeshNode->SetAttribute("type", "triangle_list");

		subMeshNode->SetAttribute("usesharedvertices", StringConverter::toString(s->mUseSharedVertices) );
		//
		 bool use32BitIndexes = false;
		//bool use32BitIndexes = (!s->mElements->elementBuffer.isNull() && 
			//s->mElements->elementBuffer->getIndexType() == ElementMemoryBuffer::IT_32BIT);
		//
		subMeshNode->SetAttribute("use32bitindexes", 
			StringConverter::toString( use32BitIndexes ));


		if ( s->mFaces->getSize() > 0)
		{
			// Faces
			TiXmlElement* facesNode = 
				subMeshNode->InsertEndChild(TiXmlElement("faces"))->ToElement();
			
			// tri list
			numTriangles = s->mFaces->getSize() / 3;

			facesNode->SetAttribute("count", 
				StringConverter::toString(numTriangles));
			// Write each face in turn
			size_t i;
			unsigned int* pInt;
			const unsigned short* pShort;
			BufferSharedPtr<ushort> facebuf = s->mFaces;
			if (use32BitIndexes)
			{
				//pInt = static_cast<unsigned int*>(
					//ibuf->lock( 0, ibuf->getSizeInBytes(), SimdMemoryBuffer::HBL_READ_ONLY ) ); 
			}
			else
			{
				//pShort = static_cast<unsigned short*>(
					//ibuf->lock(0, ibuf->getSizeInBytes(), SimdMemoryBuffer::HBL_READ_ONLY) ); 
				pShort = facebuf->lockForRead<unsigned short>();
			}

			for (i = 0; i < numTriangles; ++i)
			{
				TiXmlElement* faceNode = 
					facesNode->InsertEndChild(TiXmlElement("face"))->ToElement();
				if (use32BitIndexes)
				{
					//tetraNode->SetAttribute("n1", StringConverter::toString(*pInt++));
					//tetraNode->SetAttribute("n2", StringConverter::toString(*pInt++));
					//tetraNode->SetAttribute("n3", StringConverter::toString(*pInt++));
					//tetraNode->SetAttribute("n4", StringConverter::toString(*pInt++));		
				}
				else
				{
					faceNode->SetAttribute("v1", StringConverter::toString(*pShort++));
					faceNode->SetAttribute("v2", StringConverter::toString(*pShort++));
					faceNode->SetAttribute("v3", StringConverter::toString(*pShort++));
				}
			}

			facebuf->unlock();
		}

		// M_GEOMETRY chunk (Optional: present only if useSharedVertices = false)
		//if (!s->mUseSharedNodes)
		{
			TiXmlElement* geomNode = 
				subMeshNode->InsertEndChild(TiXmlElement("vertices"))->ToElement();
			writeVertices(geomNode, s->mVertices );
		}
		TiXmlElement* normalsNode = 
			subMeshNode->InsertEndChild(TiXmlElement("vertexnormals"))->ToElement();
		writeVertexNormals( normalsNode, s->mVertexNormals );



	}


	void XmlTetraMeshSerializer::writeNodes(TiXmlElement* mParentNode, const BufferSharedPtr<Vector3>& nodebuf)
	{

		TiXmlElement* nodes = 
			mParentNode->InsertEndChild(TiXmlElement("nodes"))->ToElement();

		// Set num verts on parent
		nodes->SetAttribute("count", StringConverter::toString(nodebuf->getSize()));

		// Write each face in turn
		size_t i;
		const Vector3* pVec;

		pVec = nodebuf->lockForRead<Vector3>();

		for (i = 0; i < nodebuf->getSize(); ++i )
		{
			TiXmlElement* node = 
				nodes->InsertEndChild(TiXmlElement("node"))->ToElement();

			node->SetAttribute("x", StringConverter::toString( pVec->getX() ));
			node->SetAttribute("y", StringConverter::toString( pVec->getY() ));
			node->SetAttribute("z", StringConverter::toString( pVec->getZ() ));
			++pVec;
		}
		nodebuf->unlock();

	}

	void XmlTetraMeshSerializer::writeConstrainedNodes(TiXmlElement* constrnodes, const BufferSharedPtr<ushort>& constrainednodebuf)
	{

		// Set num verts on parent
		constrnodes->SetAttribute("count", StringConverter::toString(constrainednodebuf->getSize()));

		// Write each face in turn
		size_t i;
		const ushort* index;

		index = constrainednodebuf->lockForRead<ushort>();

		for (i = 0; i < constrainednodebuf->getSize(); ++i )
		{
			TiXmlElement* node = 
				constrnodes->InsertEndChild(TiXmlElement("node"))->ToElement();
			node->SetAttribute("index", StringConverter::toString( *index ));
			++index;
		}
		constrainednodebuf->unlock();
	}

	void XmlTetraMeshSerializer::writeGraph(TiXmlElement* mParentNode, const Graph& graph )
	{

		TiXmlElement* graphNode = 
			mParentNode->InsertEndChild(TiXmlElement("graph"))->ToElement();

	
		graphNode->SetAttribute("size", StringConverter::toString(graph.columns->getSize()) );
		bool use32BitIndexes = false;
		graphNode->SetAttribute("use32bitindexes", StringConverter::toString( use32BitIndexes ));

		TiXmlElement* verticesNode = 
			graphNode->InsertEndChild(TiXmlElement("vertices"))->ToElement();

		size_t nbRows = graph.rows->getSize() - 1;
		verticesNode->SetAttribute("count", StringConverter::toString(nbRows) );

		// Write each vertex in turn
		size_t nbEdges;
		const ushort* pRow = graph.rows->lockForRead<ushort>();
		const ushort* pCol = graph.columns->lockForRead<ushort>();
		const ushort* pDiag = graph.diagonals->lockForRead<ushort>();

		for (int r = 0; r < nbRows; ++r )
		{
			TiXmlElement* vertex = 
				verticesNode->InsertEndChild(TiXmlElement("vertex"))->ToElement();

			size_t nbEdges = pRow[r+1] - pRow[r];
			if (nbEdges > 0 )
			{
				TiXmlElement* edgelist = 
					vertex->InsertEndChild(TiXmlElement("edgelist"))->ToElement();
				edgelist->SetAttribute("count", StringConverter::toString(nbEdges) );
				
				{

					TiXmlElement* edges = 
						edgelist->InsertEndChild(TiXmlElement("edges"))->ToElement();

					char att[8];
					char attIdx[8];

					for ( int c = 1; c <= nbEdges; ++c )
					{
						_itoa_s(c, attIdx, 8, 10 );
						strcpy_s(att, 8, "v");
						strcat_s(att,8,attIdx);
						edges->SetAttribute( att , StringConverter::toString(*pCol++) );

					}
				}

				TiXmlElement* diag = 
					vertex->InsertEndChild(TiXmlElement("diag"))->ToElement();
				diag->SetAttribute("index", StringConverter::toString(*pDiag++) );


			}
		}

		graph.diagonals->unlock();
		graph.columns->unlock();
		graph.rows->unlock();

	}

	//---------------------------------------------------------------------
	//void XMLTetraMeshSerializer::writeGeometry(TiXmlElement* mParentNode, const NodeData* nodeData)
	//{
	//	// Write a vertex buffer per element

	//	TiXmlElement *vbNode, *vertexNode, *dataNode;

	//	// Set num verts on parent
	//	mParentNode->SetAttribute("nodecount", StringConverter::toString(nodeData->nodeCount));

	//	NodeDeclaration* decl = nodeData->nodeDeclaration;
	//	NodeBufferBinding* bind = nodeData->nodeBufferBinding;

	//	NodeBufferBinding::NodeBufferBindingMap::const_iterator b, bend;
	//	bend = bind->getBindings().end();
	//	// Iterate over buffers
	//	for(b = bind->getBindings().begin(); b != bend; ++b)
	//	{
	//		vbNode = mParentNode->InsertEndChild(TiXmlElement("nodebuffer"))->ToElement();
	//		const NodeBufferSharedPtr vbuf = b->second;
	//		unsigned short bufferIdx = b->first;
	//		// Get all the elements that relate to this buffer			
	//		NodeDeclaration::NodeElementList elems = decl->findElementsBySource(bufferIdx);
	//		NodeDeclaration::NodeElementList::iterator i, iend;
	//		iend = elems.end();

	//		// Set up the data access for this buffer (lock read-only)
	//		unsigned char* pVert;
	//		float* pFloat;
	//		uint* pColour;

	//		pVert = static_cast<unsigned char*>(
	//			vbuf->lock(SimdMemoryBuffer::HBL_READ_ONLY));

	//		// Skim over the elements to set up the general data
	//		unsigned short numTextureCoords = 0;
	//		for (i = elems.begin(); i != iend; ++i)
	//		{
	//			NodeElement& elem = *i;
	//			switch(elem.getSemantic())
	//			{
	//			case NES_POSITION:
	//				vbNode->SetAttribute("positions","true");
	//				break;
	//			case NES_NORMAL:
	//				vbNode->SetAttribute("normals","true");
	//				break;
	//			//case VES_TANGENT:
	//			//	vbNode->SetAttribute("tangents","true");
	//			//	if (elem.getType() == VET_FLOAT4)
	//			//	{
	//			//		vbNode->SetAttribute("tangent_dimensions", "4");
	//			//	}
	//			//	break;
	//			//case VES_BINORMAL:
	//			//	vbNode->SetAttribute("binormals","true");
	//			//	break;
	//			case NES_DIFFUSE:
	//				vbNode->SetAttribute("colours_diffuse","true");
	//				break;
	//			case NES_SPECULAR:
	//				vbNode->SetAttribute("colours_specular","true");
	//				break;
	//			case NES_TEXTURE_COORDINATES:
	//				vbNode->SetAttribute(
	//					"texture_coord_dimensions_" + StringConverter::toString(numTextureCoords), 
	//					StringConverter::toString(NodeElement::getTypeCount(elem.getType())));
	//				++numTextureCoords;
	//				break;

	//			default:
	//				break;
	//			}
	//		}
	//		if (numTextureCoords > 0)
	//		{
	//			vbNode->SetAttribute("texture_coords", 
	//				StringConverter::toString(numTextureCoords));
	//		}

	//		// For each vertex
	//		for (size_t v = 0; v < nodeData->nodeCount; ++v)
	//		{
	//			vertexNode = 
	//				vbNode->InsertEndChild(TiXmlElement("node"))->ToElement();
	//			// Iterate over the elements
	//			for (i = elems.begin(); i != iend; ++i)
	//			{
	//				NodeElement& elem = *i;
	//				switch(elem.getSemantic())
	//				{
	//				case NES_POSITION:
	//					elem.baseNodePointerToElement(pVert, &pFloat);
	//					dataNode = 
	//						vertexNode->InsertEndChild(TiXmlElement("position"))->ToElement();
	//					dataNode->SetAttribute("x", StringConverter::toString(pFloat[0]));
	//					dataNode->SetAttribute("y", StringConverter::toString(pFloat[1]));
	//					dataNode->SetAttribute("z", StringConverter::toString(pFloat[2]));
	//					break;
	//				case NES_NORMAL:
	//					elem.baseNodePointerToElement(pVert, &pFloat);
	//					dataNode = 
	//						vertexNode->InsertEndChild(TiXmlElement("normal"))->ToElement();
	//					dataNode->SetAttribute("x", StringConverter::toString(pFloat[0]));
	//					dataNode->SetAttribute("y", StringConverter::toString(pFloat[1]));
	//					dataNode->SetAttribute("z", StringConverter::toString(pFloat[2]));
	//					break;
	//				//case VES_TANGENT:
	//				//	elem.baseVertexPointerToElement(pVert, &pFloat);
	//				//	dataNode = 
	//				//		vertexNode->InsertEndChild(TiXmlElement("tangent"))->ToElement();
	//				//	dataNode->SetAttribute("x", StringConverter::toString(pFloat[0]));
	//				//	dataNode->SetAttribute("y", StringConverter::toString(pFloat[1]));
	//				//	dataNode->SetAttribute("z", StringConverter::toString(pFloat[2]));
	//				//	if (elem.getType() == VET_FLOAT4)
	//				//	{
	//				//		dataNode->SetAttribute("w", StringConverter::toString(pFloat[3]));
	//				//	}
	//				//	break;
	//				//case VES_BINORMAL:
	//				//	elem.baseVertexPointerToElement(pVert, &pFloat);
	//				//	dataNode = 
	//				//		vertexNode->InsertEndChild(TiXmlElement("binormal"))->ToElement();
	//				//	dataNode->SetAttribute("x", StringConverter::toString(pFloat[0]));
	//				//	dataNode->SetAttribute("y", StringConverter::toString(pFloat[1]));
	//				//	dataNode->SetAttribute("z", StringConverter::toString(pFloat[2]));
	//				//	break;
	//				case NES_DIFFUSE:
	//					elem.baseNodePointerToElement(pVert, &pColour);
	//					dataNode = 
	//						vertexNode->InsertEndChild(TiXmlElement("colour_diffuse"))->ToElement();
	//					{
	//						uint rc = *pColour++;
	//						ColourValue cv;
	//						cv.b = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.g = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.r = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.a = (rc & 0xFF) / 255.0f;
	//						dataNode->SetAttribute("value", StringConverter::toString(cv));
	//					}
	//					break;
	//				case NES_SPECULAR:
	//					elem.baseNodePointerToElement(pVert, &pColour);
	//					dataNode = 
	//						vertexNode->InsertEndChild(TiXmlElement("colour_specular"))->ToElement();
	//					{
	//						uint rc = *pColour++;
	//						ColourValue cv;
	//						cv.b = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.g = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.r = (rc & 0xFF) / 255.0f;		rc >>= 8;
	//						cv.a = (rc & 0xFF) / 255.0f;
	//						dataNode->SetAttribute("value", StringConverter::toString(cv));
	//					}
	//					break;
	//				case NES_TEXTURE_COORDINATES:
	//					elem.baseNodePointerToElement(pVert, &pFloat);
	//					dataNode = 
	//						vertexNode->InsertEndChild(TiXmlElement("texcoord"))->ToElement();

	//					switch(elem.getType())
	//					{
	//					case NET_FLOAT1:
	//						dataNode->SetAttribute("u", StringConverter::toString(*pFloat++));
	//						break;
	//					case NET_FLOAT2:
	//						dataNode->SetAttribute("u", StringConverter::toString(*pFloat++));
	//						dataNode->SetAttribute("v", StringConverter::toString(*pFloat++));
	//						break;
	//					case NET_FLOAT3:
	//						dataNode->SetAttribute("u", StringConverter::toString(*pFloat++));
	//						dataNode->SetAttribute("v", StringConverter::toString(*pFloat++));
	//						dataNode->SetAttribute("w", StringConverter::toString(*pFloat++));
	//						break;
	//					default:
	//						break;
	//					}
	//					break;
	//				default:
	//					break;

	//				}
	//			}
	//			pVert += vbuf->getNodeSize();
	//		}
	//		vbuf->unlock();
	//	}

	//}


	void XmlTetraMeshSerializer::writeSubTetraMeshNames(TiXmlElement* mMeshNode, const TetraMesh* m)
	{
		const TetraMesh::SubTetraMeshNameMap& nameMap = m->getSubTetraMeshNameMap();
		if (nameMap.empty())
			return; // do nothing

		TiXmlElement* namesNode = 
			mMeshNode->InsertEndChild(TiXmlElement("subtetrameshnames"))->ToElement();
		TetraMesh::SubTetraMeshNameMap::const_iterator i, iend;
		iend = nameMap.end();
		for (i = nameMap.begin(); i != iend; ++i)
		{
			TiXmlElement* subNameNode = 
				namesNode->InsertEndChild(TiXmlElement("subtetrameshname"))->ToElement();

			subNameNode->SetAttribute("name", i->first);
			subNameNode->SetAttribute("index", 
				StringConverter::toString(i->second));
		}

	}


	void XmlTetraMeshSerializer::writeSubMeshNames(TiXmlElement* mMeshNode, const TetraMesh* m)
	{
		const TetraMesh::SubMeshNameMap& nameMap = m->getSubMeshNameMap();
		if (nameMap.empty())
			return; // do nothing

		TiXmlElement* namesNode = 
			mMeshNode->InsertEndChild(TiXmlElement("submeshnames"))->ToElement();
		TetraMesh::SubMeshNameMap::const_iterator i, iend;
		iend = nameMap.end();
		for (i = nameMap.begin(); i != iend; ++i)
		{
			TiXmlElement* subNameNode = 
				namesNode->InsertEndChild(TiXmlElement("submeshname"))->ToElement();

			subNameNode->SetAttribute("name", i->first);
			subNameNode->SetAttribute("index", 
				StringConverter::toString(i->second));
		}

	}


	
	void XmlTetraMeshSerializer::readNodes(TiXmlElement* mParentNode, const BufferSharedPtr<Vector3>& nodebuf)
	{
		LogManager::getSingleton().logMessage("Reading shared nodes...");

		int claimedCount = StringConverter::parseInt( mParentNode->Attribute("count") );

		// calculate how many vertexes there actually are
		int actualNodesCount = 0;
		for (TiXmlElement * nodeElem = mParentNode->FirstChildElement(); 
			nodeElem != 0; nodeElem = nodeElem->NextSiblingElement() )
		{
			actualNodesCount++;
		}
		if ( claimedCount && claimedCount != actualNodesCount )
		{
			LogManager::getSingleton().stream()
				<< "WARNING: node count (" << actualNodesCount 
				<< ") is not as claimed (" << claimedCount << ")";
		}


		Vector3* pVec = nodebuf->lockForWrite<Vector3>();


		// Iterate over all children (node entries) 
		for (TiXmlElement* nodeElem = mParentNode->FirstChildElement();
			nodeElem != 0; nodeElem = nodeElem->NextSiblingElement() )
		{
			pVec->setX( StringConverter::parseReal(nodeElem->Attribute("x")) );
			pVec->setY( StringConverter::parseReal(nodeElem->Attribute("y")) );
			pVec->setZ( StringConverter::parseReal(nodeElem->Attribute("z")) );
			++pVec;
		}

		nodebuf->unlock();

		LogManager::getSingleton().logMessage("shared nodes done");

	}

	void XmlTetraMeshSerializer::readConstrainedNodes(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& constrainednodebuf)
	{
		LogManager::getSingleton().logMessage("Reading shared constrained nodes...");

		int claimedCount = StringConverter::parseInt( mParentNode->Attribute("count") );

		// calculate how many vertexes there actually are
		int actualNodesCount = 0;
		for (TiXmlElement * nodeElem = mParentNode->FirstChildElement(); 
			nodeElem != 0; nodeElem = nodeElem->NextSiblingElement() )
		{
			actualNodesCount++;
		}
		if ( claimedCount && claimedCount != actualNodesCount )
		{
			LogManager::getSingleton().stream()
				<< "WARNING: node count (" << actualNodesCount 
				<< ") is not as claimed (" << claimedCount << ")";
		}


		ushort* pIndex = constrainednodebuf->lockForWrite<ushort>();


		// Iterate over all children (node entries) 
		for (TiXmlElement* nodeElem = mParentNode->FirstChildElement();
			nodeElem != 0; nodeElem = nodeElem->NextSiblingElement() )
		{
			*pIndex++ = StringConverter::parseReal(nodeElem->Attribute("index"));
		}

		constrainednodebuf->unlock();

		LogManager::getSingleton().logMessage("shared constrained nodes done");
	}


	void XmlTetraMeshSerializer::readGraph(TiXmlElement* graphNode, Graph* graph )
	{
		LogManager::getSingleton().logMessage("Reading graph ...");

		int size = StringConverter::parseInt( graphNode->Attribute("size") );
		bool use32bitindexes = StringConverter::parseBool( graphNode->Attribute("use32bitindexes") );
		use32bitindexes = false;
		if (size>0)
		{
			if ( use32bitindexes == false )
			{
				graph->columns = 
					BufferManager::getSingleton().createBuffer<ushort>(size);
			}
		}

		TiXmlElement* vertices = graphNode->FirstChildElement("vertices");
		int verticescount = StringConverter::parseInt( vertices->Attribute("count") );

		graph->rows = 
			BufferManager::getSingleton().createBuffer<ushort>(verticescount+1);
		
		graph->diagonals = 
			BufferManager::getSingleton().createBuffer<ushort>(verticescount );


		ushort* pRow = graph->rows->lockForWrite<ushort>();
		ushort* pCol = graph->columns->lockForWrite<ushort>();
		ushort* pDiag = graph->diagonals->lockForWrite<ushort>();

		size_t counter = 0;
		// Iterate over all children (vertex entries) 
		for (TiXmlElement* vertexElem = vertices->FirstChildElement();
			vertexElem != 0; vertexElem = vertexElem->NextSiblingElement() )
		{
			TiXmlElement* edgelist = vertexElem->FirstChildElement("edgelist");
			int edgecount = StringConverter::parseInt( edgelist->Attribute("count") );

			TiXmlElement* edges = edgelist->FirstChildElement("edges");

			char att[8];
			char attIdx[8];

			for (int c=1; c<= edgecount; ++c )
			{
				_itoa_s(c, attIdx, 8, 10 );
				strcpy_s(att, 8, "v");
				strcat_s(att,8,attIdx);
				*pCol++ = static_cast<ushort>(StringConverter::parseInt(edges->Attribute( att )) );
			}

			TiXmlElement* diagonal = vertexElem->FirstChildElement("diag");
			*pDiag++ = static_cast<ushort>(StringConverter::parseInt(diagonal->Attribute("index")) );

			*pRow++ = counter;
			counter += edgecount;
		}

		// finalize row
		*pRow = counter;

		graph->diagonals->unlock();
		graph->columns->unlock();
		graph->rows->unlock();

		LogManager::getSingleton().logMessage("graph done");
	}

	
	void XmlTetraMeshSerializer::readSurfaceMesh(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& facebuf)
	{
		LogManager::getSingleton().logMessage("Reading surface mesh faces ...");

		int claimedCount = StringConverter::parseInt( mParentNode->Attribute("count") );

		// calculate how many vertexes there actually are
		int actualCount = 0;
		for (TiXmlElement * faceElem = mParentNode->FirstChildElement(); 
			faceElem != 0; faceElem = faceElem->NextSiblingElement() )
		{
			actualCount++;
		}
		if ( claimedCount && claimedCount != actualCount )
		{
			LogManager::getSingleton().stream()
				<< "WARNING: face count (" << actualCount 
				<< ") is not as claimed (" << claimedCount << ")";
		}

		ushort* pShort;
		bool use32BitIndexes = false;

		if (use32BitIndexes)
		{
			//pInt = static_cast<unsigned int*>(
			//ibuf->lock(HardwareBuffer::HBL_DISCARD));
		}
		else
		{
			pShort = facebuf->lockForWrite<ushort>();
		}

		// Iterate over all children (node entries) 
		actualCount = 0;
		for (TiXmlElement * faceElem = mParentNode->FirstChildElement(); 
			faceElem != 0; faceElem = faceElem->NextSiblingElement() )
		{
			*pShort++ = static_cast<ushort>(StringConverter::parseInt(faceElem->Attribute("n1")) );
			*pShort++ = static_cast<ushort>(StringConverter::parseInt(faceElem->Attribute("n2")) );
			*pShort++ = static_cast<ushort>(StringConverter::parseInt(faceElem->Attribute("n3")) );
			++actualCount;
		}

		facebuf->unlock();

		LogManager::getSingleton().logMessage("surface mesh faces done");
	}


	//void XMLTetraMeshSerializer::readFaces(TiXmlElement* surfaceNode, ElementData* pFaces)
	//{

	//	LogManager::getSingleton().logMessage("Reading surface...");

	//	// Read operation type
	//	bool readFaces = true;
	//	const char* optype = surfaceNode->Attribute("operationtype");

	//	//const char* tmp = surfaceNode->Attribute("usesharednodes");
	//	//if (tmp)
	//	//	sm->mUseSharedNodes = StringConverter::parseBool(tmp);
	//	
	//	const char* tmp = surfaceNode->Attribute("use32bitindexes");

	//	bool use32BitIndexes = false;
	//	if (tmp)
	//	{
	//		use32BitIndexes = StringConverter::parseBool(tmp);
	//	}

	//	// Faces
	//	if (readFaces)
	//	{
	//		TiXmlElement* faces = surfaceNode->FirstChildElement("faces");
	//		size_t actualCount = 0;
	//		for (TiXmlElement *faceElem = faces->FirstChildElement(); faceElem != 0; faceElem = faceElem->NextSiblingElement())
	//		{
	//			actualCount++;
	//		}
	//		const char *claimedCount_ = faces->Attribute("count");
	//		if (claimedCount_ && StringConverter::parseInt(claimedCount_)!=actualCount)
	//		{
	//			LogManager::getSingleton().stream()
	//				<< "WARNING: face count (" << actualCount << ") " <<
	//				"is not as claimed (" << claimedCount_ << ")";
	//		}


	//		if (actualCount > 0)
	//		{
	//			// Faces
	//			pFaces->indexCount = actualCount * 3;

	//			// Allocate space
	//			ElementMemoryBufferSharedPtr ibuf = SimdMemoryBufferManager::getSingleton().
	//				createElementBuffer(
	//				ElementMemoryBuffer::SET_Triangle,
	//				use32BitIndexes? ElementMemoryBuffer::IT_32BIT : ElementMemoryBuffer::IT_16BIT, 
	//				pFaces->indexCount, 
	//				SimdMemoryBuffer::SMB_CPU_HEAP,
	//				false);
	//			pFaces->elementBuffer = ibuf;

	//			unsigned int *pInt;
	//			unsigned short *pShort;
	//			if (use32BitIndexes)
	//			{
	//				pInt = static_cast<unsigned int*>(
	//					ibuf->lock(SimdMemoryBuffer::HBL_DISCARD));
	//			}
	//			else
	//			{
	//				pShort = static_cast<unsigned short*>(
	//					ibuf->lock(SimdMemoryBuffer::HBL_DISCARD));
	//			}

	//			TiXmlElement* faceElem;
	//			bool firstTri = true;
	//			for (faceElem = faces->FirstChildElement();
	//				faceElem != 0; faceElem = faceElem->NextSiblingElement())
	//			{
	//				if (use32BitIndexes)
	//				{
	//					*pInt++ = StringConverter::parseInt(faceElem->Attribute("n1"));
	//					*pInt++ = StringConverter::parseInt(faceElem->Attribute("n2"));
	//					*pInt++ = StringConverter::parseInt(faceElem->Attribute("n3"));
	//				}
	//				else
	//				{
	//					*pShort++ = StringConverter::parseInt(faceElem->Attribute("n1"));						//{
	//					*pShort++ = StringConverter::parseInt(faceElem->Attribute("n2"));
	//					*pShort++ = StringConverter::parseInt(faceElem->Attribute("n3"));
	//				}
	//				firstTri = false;
	//			}
	//			ibuf->unlock();
	//		}
	//	}

	//	LogManager::getSingleton().logMessage("surface done.");
	//}


	void XmlTetraMeshSerializer::readSubTetraMeshes(TiXmlElement* mSubTetraMeshesNode)
	{
		LogManager::getSingleton().logMessage("Reading subtetrameshes...");


		for (TiXmlElement* smElem = mSubTetraMeshesNode->FirstChildElement();
			smElem != 0; smElem = smElem->NextSiblingElement())
		{
			// All children should be submeshes 
			SubTetraMesh* subTetraMesh = mpMesh->createSubTetraMesh();

			const char* mat = smElem->Attribute("material");
			if (mat)
			{
				subTetraMesh->setMaterialName(mat);
			}

			// Read operation type
			bool readTetras = true;
			const char* optype = smElem->Attribute("type");
			if (optype)
			{
				if (!strcmp(optype, "tetrahedron"))
				{
					//elemType = ElementMemoryBuffer::SET_Tetrahedron;
					readTetras = true;
				}
			}
			//stm->Type = ElemenType::Tetrahedron;
			//	else if (!strcmp(optype, "triangle_fan"))
			//	{
			//		sm->operationType = RenderOperation::OT_TRIANGLE_FAN;
			//	}
			//	else if (!strcmp(optype, "triangle_strip"))
			//	{
			//		sm->operationType = RenderOperation::OT_TRIANGLE_STRIP;
			//	}
			//	else if (!strcmp(optype, "line_strip"))
			//	{
			//		sm->operationType = RenderOperation::OT_LINE_STRIP;
			//		readFaces = false;
			//	}
			//	else if (!strcmp(optype, "line_list"))
			//	{
			//		sm->operationType = RenderOperation::OT_LINE_LIST;
			//		readFaces = false;
			//	}
			//	else if (!strcmp(optype, "point_list"))
			//	{
			//		sm->operationType = RenderOperation::OT_POINT_LIST;
			//		readFaces = false;
			//	}
			

			const char* usesarednodes = smElem->Attribute("usesharednodes");
			if ( usesarednodes )
			{
				subTetraMesh->mUseSharedNodes =  StringConverter::parseBool(optype);
			}
			subTetraMesh->mUseSharedNodes = true;


			TiXmlElement* elements = smElem->FirstChildElement("elements");

			int claimedCount = StringConverter::parseInt( elements->Attribute("count") );

			// calculate how many vertexes there actually are
			int actualCount = 0;
			for (TiXmlElement * elemNode = elements->FirstChildElement(); 
				elemNode != 0; elemNode = elemNode->NextSiblingElement() )
			{
				actualCount++;
			}
			if ( claimedCount && claimedCount != actualCount )
			{
				LogManager::getSingleton().stream()
					<< "WARNING: face count (" << actualCount 
					<< ") is not as claimed (" << claimedCount << ")";
			}

			ushort* pShort;
			bool use32BitIndexes = false;


			subTetraMesh->mTetrahedrons = 
				BufferManager::getSingleton().createBuffer<ushort>(4*claimedCount);

			if (use32BitIndexes)
			{
				//pInt = static_cast<unsigned int*>(
				//ibuf->lock(HardwareBuffer::HBL_DISCARD));
			}
			else
			{
				pShort = subTetraMesh->mTetrahedrons->lockForWrite<ushort>();
			}

			// Iterate over all children (node entries) 
			for (TiXmlElement* elemNode = elements->FirstChildElement();
				elemNode != 0; elemNode = elemNode->NextSiblingElement() )
			{
				*pShort++ = static_cast<ushort>(StringConverter::parseInt(elemNode->Attribute("n1")) );
				*pShort++ = static_cast<ushort>(StringConverter::parseInt(elemNode->Attribute("n2")) );
				*pShort++ = static_cast<ushort>(StringConverter::parseInt(elemNode->Attribute("n3")) );
				*pShort++ = static_cast<ushort>(StringConverter::parseInt(elemNode->Attribute("n4")) );
			}

			subTetraMesh->mTetrahedrons->unlock();

		}

		LogManager::getSingleton().logMessage("subtetrameshes done");
	}
	

	void XmlTetraMeshSerializer::readSubMeshes(TiXmlElement* mSubmeshesNode)
	{
		LogManager::getSingleton().logMessage("Reading submeshes...");

		for (TiXmlElement* smElem = mSubmeshesNode->FirstChildElement();
			smElem != 0; smElem = smElem->NextSiblingElement())
		{
			// All children should be submeshes 
			SubMesh* sm = mpMesh->createSubMesh();

			//const char* mat = smElem->Attribute("material");
			//if (mat)
			//	sm->setMaterialName(mat);


			// Read operation type
			bool bTriangleList = true;
			const char* optype = smElem->Attribute("type");
			if (optype)
			{
				if (!strcmp(optype, "triangle_list"))
				{
					bTriangleList = true;
				}
				else
				{
					PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "Unknown <type> element.",
						"XMLTetraSerializer::readSubMeshes");
				}
			}


			const char* tmp = smElem->Attribute("usesharedvertices");
			if (tmp && StringConverter::parseBool(tmp) )
			{
				PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "<usesharedvertices> submesh not supported.",
					"XMLTetraSerializer::readSubMeshes"); 
			}
			else
			{
				sm->mUseSharedVertices = false;
			}


			tmp = smElem->Attribute("use32bitindexes");

			bool use32BitIndexes = false;
			if (tmp && StringConverter::parseBool(tmp) )
			{
				PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "<use32bitindexes> = true in submesh not supported.",
					"XMLTetraSerializer::readSubMeshes"); 
			}
			else
			{
				use32BitIndexes = false;
			}


			// Faces
			if ( bTriangleList )
			{
				TiXmlElement* faces = smElem->FirstChildElement("faces");
				size_t actualCount = 0;
				for (TiXmlElement *faceElem = faces->FirstChildElement(); faceElem != 0; faceElem = faceElem->NextSiblingElement())
				{
					actualCount++;
				}
				const char* claimedCount = faces->Attribute("count");
				if (claimedCount && StringConverter::parseInt(claimedCount)!=actualCount)
				{
					LogManager::getSingleton().stream()
						<< "WARNING: face count (" << actualCount << ") " <<
						"is not as claimed (" << claimedCount << ")";
				}


				if (actualCount > 0)
				{
					sm->mFaces = 
						BufferManager::getSingleton().createBuffer<ushort>(actualCount*3);

					unsigned int *pInt;
					unsigned short *pShort;
					//if (use32BitIndexes)
					//{
					//pInt = static_cast<unsigned int*>(
					//ibuf->lock(SimdMemoryBuffer::HBL_DISCARD));
					//}
					//else
					//{
					pShort = sm->mFaces->lockForWrite<unsigned short>();
					//}
					TiXmlElement* faceElem;
					for (faceElem = faces->FirstChildElement();
						faceElem != 0; faceElem = faceElem->NextSiblingElement())
					{
						//				if (use32BitIndexes)
						//				{
						//					*pInt++ = StringConverter::parseInt(tetraElem->Attribute("n1"));
						//					*pInt++ = StringConverter::parseInt(tetraElem->Attribute("n2"));
						//					*pInt++ = StringConverter::parseInt(tetraElem->Attribute("n3"));
						//					*pInt++ = StringConverter::parseInt(tetraElem->Attribute("n4"));
						//				}
						//				else
						//				{
						*pShort++ = StringConverter::parseInt(faceElem->Attribute("v1"));
						*pShort++ = StringConverter::parseInt(faceElem->Attribute("v2"));
						*pShort++ = StringConverter::parseInt(faceElem->Attribute("v3"));
						//				}
					}
					sm->mFaces->unlock();
				}

			}

			// vertices
			// Geometry
			if (!sm->mUseSharedVertices)
			{
				TiXmlElement* vertices = smElem->FirstChildElement("vertices");

				int claimedCount = StringConverter::parseInt( vertices->Attribute("count") );

				// calculate how many vertexes there actually are
				int actualCount = 0;
				for (TiXmlElement * vertexElem = vertices->FirstChildElement(); 
					vertexElem != 0; vertexElem = vertexElem->NextSiblingElement() )
				{
					actualCount++;
				}
				if ( claimedCount && claimedCount != actualCount )
				{
					LogManager::getSingleton().stream()
						<< "WARNING: face count (" << actualCount 
						<< ") is not as claimed (" << claimedCount << ")";
				}

				if ( actualCount > 0)
				{

					sm->mVertices.tetraIndex = 
						BufferManager::getSingleton().createBuffer<ushort>(claimedCount);

					sm->mVertices.barycentricCoords = 
						BufferManager::getSingleton().createBuffer<Vector3>(claimedCount);


					unsigned short* pShort = sm->mVertices.tetraIndex->lockForWrite<unsigned short>();
					Vector3 * pVec = sm->mVertices.barycentricCoords->lockForWrite<Vector3>();

					TiXmlElement* vertexElem;
					for (vertexElem = vertices->FirstChildElement();
						vertexElem != 0; vertexElem = vertexElem->NextSiblingElement())
					{

						TiXmlElement* elem = vertexElem->FirstChildElement("tetra");
						*pShort++ = static_cast<ushort>(StringConverter::parseInt( elem->Attribute("index") ));

						elem = vertexElem->FirstChildElement("barycentricsCoords");
						pVec->setX( StringConverter::parseReal( elem->Attribute("l1") ) );
						pVec->setY( StringConverter::parseReal( elem->Attribute("l2") ) );
						pVec->setZ( StringConverter::parseReal( elem->Attribute("l3") ) );
						++pVec;
					}

					sm->mVertices.tetraIndex->unlock();
					sm->mVertices.barycentricCoords->unlock();

				}
			}



			// Normals
			TiXmlElement* Normals = smElem->FirstChildElement("vertexnormals");

			int claimedCount = StringConverter::parseInt( Normals->Attribute("count") );

			// calculate how many vertexes there actually are
			int actualCount = 0;
			for (TiXmlElement * normalElem = Normals->FirstChildElement(); 
				normalElem != 0; normalElem = normalElem->NextSiblingElement() )
			{
				actualCount++;
			}
			if ( claimedCount && claimedCount != actualCount )
			{
				LogManager::getSingleton().stream()
					<< "WARNING: face count (" << actualCount 
					<< ") is not as claimed (" << claimedCount << ")";
			}

			if ( actualCount > 0)
			{

				sm->mVertexNormals = 
					BufferManager::getSingleton().createBuffer<Vector3>(claimedCount);

				Vector3 * pVec = sm->mVertexNormals->lockForWrite<Vector3>();

				TiXmlElement* normalElem;
				for (normalElem = Normals->FirstChildElement();
					normalElem != 0; normalElem = normalElem->NextSiblingElement())
				{

					//TiXmlElement* elem = normalElem->FirstChildElement("normal");
					pVec->setX( StringConverter::parseReal( normalElem->Attribute("x") ) );
					pVec->setY( StringConverter::parseReal( normalElem->Attribute("y") ) );
					pVec->setZ( StringConverter::parseReal( normalElem->Attribute("z") ) );
					++pVec;
				}

				sm->mVertexNormals->unlock();

			}

		}			


		LogManager::getSingleton().logMessage("Submeshes done.");

	}
		




	void XmlTetraMeshSerializer::readSubTetraMeshNames(TiXmlElement* mTetraMeshNamesNode, TetraMesh *sm)
	{
		LogManager::getSingleton().logMessage("Reading tetramesh names...");

		// Iterate over all children (vertexboneassignment entries)
		for (TiXmlElement* elem = mTetraMeshNamesNode->FirstChildElement();
			elem != 0; elem = elem->NextSiblingElement())
		{
			String meshName = elem->Attribute("name");
			int index = StringConverter::parseInt(elem->Attribute("index"));

			sm->nameSubTetraMesh(meshName, index);
		}

		LogManager::getSingleton().logMessage("Tetra Mesh names done.");
	}


	void XmlTetraMeshSerializer::readSubMeshNames(TiXmlElement* mMeshNamesNode, TetraMesh *sm)
	{
		LogManager::getSingleton().logMessage("Reading tetramesh names...");

		// Iterate over all children (vertexboneassignment entries)
		for (TiXmlElement* elem = mMeshNamesNode->FirstChildElement();
			elem != 0; elem = elem->NextSiblingElement())
		{
			String meshName = elem->Attribute("name");
			int index = StringConverter::parseInt(elem->Attribute("index"));

			sm->nameSubMesh(meshName, index);
		}

		LogManager::getSingleton().logMessage("Tetra Mesh names done.");
	}


	

}; // namespace SimStep