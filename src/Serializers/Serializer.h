#ifndef SimStep_Serializer_h__
#define SimStep_Serializer_h__

#include <Utilities/Common.h>
#include <Math/Math.h>

#include <Utilities/DataStream.h>


namespace SimStep
{

	class _SimStepExport Serializer
	{
	public:
		Serializer();
		virtual ~Serializer();

		/// The endianness of written files
		enum Endian
		{
			/// Use the platform native endian
			ENDIAN_NATIVE,
			/// Use big endian (0x1000 is serialised as 0x10 0x00)
			ENDIAN_BIG,
			/// Use little endian (0x1000 is serialised as 0x00 0x10)
			ENDIAN_LITTLE
		};


	protected:

		uint mCurrentstreamLen;
		FILE* mpfFile;
		String mVersion;
		bool mFlipEndian; // default to native endian, derive from header

		// Internal methods
		virtual void writeFileHeader(void);
		virtual void writeChunkHeader(ushort id, size_t size);

		void writeFloats(const float* const pfloat, size_t count);
		void writeFloats(const double* const pfloat, size_t count);
		void writeShorts(const ushort* const pShort, size_t count);
		void writeInts(const uint* const pInt, size_t count); 
		void writeBools(const bool* const pLong, size_t count);
		void writeObject(const Vector3& vec);
		void writeObject(const Quat& q);

		void writeString(const String& string);
		void writeData(const void* const buf, size_t size, size_t count);

		virtual void readFileHeader(DataStreamPtr& stream);
		virtual unsigned short readChunk(DataStreamPtr& stream);

		void readBools(DataStreamPtr& stream, bool* pDest, size_t count);
		void readFloats(DataStreamPtr& stream, float* pDest, size_t count);
		void readFloats(DataStreamPtr& stream, double* pDest, size_t count);
		void readShorts(DataStreamPtr& stream, ushort* pDest, size_t count);
		void readInts(DataStreamPtr& stream, uint* pDest, size_t count);
		void readObject(DataStreamPtr& stream, Vector3& pDest);
		void readObject(DataStreamPtr& stream, Quat& pDest);

		String readString(DataStreamPtr& stream);
		String readString(DataStreamPtr& stream, size_t numChars);

		virtual void flipToLittleEndian(void* pData, size_t size, size_t count = 1);
		virtual void flipFromLittleEndian(void* pData, size_t size, size_t count = 1);

		virtual void flipEndian(void * pData, size_t size, size_t count);
		virtual void flipEndian(void * pData, size_t size);

		/// Determine the endianness of the incoming stream compared to native
		virtual void determineEndianness(DataStreamPtr& stream);
		/// Determine the endianness to write with based on option
		virtual void determineEndianness(Endian requestedEndian);

	};

	
}; // namespace SimStep

#endif // SimStep_Serializer_h__