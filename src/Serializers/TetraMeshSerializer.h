#ifndef SimStep_TetraMeshSerializer_h__
#define SimStep_TetraMeshSerializer_h__

#include <Serializers/Serializer.h>

#include <Core/PhysicsClasses.h>
#include <Utilities/SimdMemoryBuffer.h>


namespace SimStep
{

	
	class _SimStepExport TetraMeshSerializer : public Serializer
	{
	public:

		TetraMeshSerializer();

		virtual ~TetraMeshSerializer();


		/** Exports a mesh to the file specified. 
		@remarks
		This method takes an externally created Mesh object, and exports both it
		and optionally the Materials it uses to a .mesh file.
		@param pMesh Pointer to the Mesh to export
		@param filename The destination filename
		@param endianMode The endian mode of the written file
		*/
		void exportMesh(const TetraMesh* pMesh, const String& filename,
			Endian endianMode = ENDIAN_NATIVE);

		/** Imports Mesh and (optionally) Material data from a .mesh file DataStream.
		@remarks
		This method imports data from a DataStream opened from a .mesh file and places it's
		contents into the Mesh object which is passed in. 
		@param stream The DataStream holding the .mesh data. Must be initialised (pos at the start of the buffer).
		@param pDest Pointer to the Mesh object which will receive the data. Should be blank already.
		*/
		void importMesh(DataStreamPtr& stream, TetraMesh* pDest);

	
	protected:

		static String msCurrentVersion;

		//typedef map<String, TetraMeshSerializerImpl* >::type TetraMeshSerializerImplMap;
		//TetraMeshSerializerImplMap mImplementations;


		void writeMesh(const TetraMesh* pMesh);
		
		void writeSharedNodes( const BufferSharedPtr<Vector3>& sharednodes );
		void writeConstrainedNodes( const BufferSharedPtr<ushort>& constrainednodes );
		void writeGraph( const Graph& graph );
		void writeSurfaceMesh( const BufferSharedPtr<ushort>& faces);
		void writeSubTetraMesh( const SubTetraMesh* subtetra );
		void writeSubTetraMeshNameTable(const TetraMesh* pMesh);
		void writeSubMesh( const SubMesh* subtetra );
		void writeSubMeshNameTable(const TetraMesh* pMesh);

		void readMesh(DataStreamPtr& stream, TetraMesh* pMesh );
		
		void readSharedNodes( DataStreamPtr& stream, TetraMesh* pMesh );
		void readConstrainedNodes( DataStreamPtr& stream, TetraMesh* pMesh );
		void readGraph( DataStreamPtr& stream, TetraMesh* pMesh );
		void readSurfaceMesh( DataStreamPtr& stream, TetraMesh* pMesh );
		void readSubTetraMesh( DataStreamPtr& stream, TetraMesh* pMesh );
		void readSubTetraMeshNameTable(DataStreamPtr& stream, TetraMesh* pMesh);
		void readSubMesh( DataStreamPtr& stream, TetraMesh* pMesh );
		void readSubMeshNameTable(DataStreamPtr& stream, TetraMesh* pMesh);


		size_t calcMeshSize(const TetraMesh* pMesh);
		size_t calcSubTetraMeshSize(const SubTetraMesh* pSub);
		size_t calcSubTetraMeshNameTableSize(const TetraMesh* pMesh);
		size_t calcSubMeshSize(const SubMesh* pSub);
		size_t calcSubMeshNameTableSize(const TetraMesh* pMesh);


	};


	
}; // namespace SimStep

#endif // SimStep_TetraMeshSerializer_h__