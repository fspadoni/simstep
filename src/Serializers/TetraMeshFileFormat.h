#ifndef SimStep_TetraMeshFileFormat_h__
#define SimStep_TetraMeshFileFormat_h__

namespace SimStep
{

	enum TetraMeshFileFormat
	{

		M_HEADER                = 0x1000,
		// char*          version           : Version number check
		M_MESH                = 0x3000,

			M_SHARE_NODES             = 0x4000,

			M_CONSTRAINED_NODES		  = 0x4010,

			M_GRAPH			         = 0x5000, 

			M_SURFACE_MESH             = 0x6000, 

		// bool skeletallyAnimated   // important flag which affects h/w buffer policies
		// Optional M_GEOMETRY chunk
			M_SUBTETRAMESH             = 0x7000, 
				
					M_SUBTETRAMESH_ELEMENT_TYPE             = 0x7010, 

			M_SUBTETRAMESH_NAME_TABLE		= 0x8000,
		
					M_SUBTETRAMESH_NAME_TABLE_ELEMENT = 0x8100,
					// short index
					// char* name
			M_SUBMESH					 = 0x9000, 

					M_SUBMESH_OPERATION = 0x8010, // optional, trilist assumed if missing


					M_SUBMESH_FACES = 0x8100, // optional, trilist assumed if missing

					M_SUBMESH_VERTICES = 0x8200, // optional, trilist assumed if missing

					M_SUBMESH_NORMALS = 0x8300, // optional, trilist assumed if missing

			M_SUBMESH_NAME_TABLE		= 0xA000,
					// Subchunks of the name table. Each chunk contains an index & string
					M_SUBMESH_NAME_TABLE_ELEMENT = 0xA100,
					// short index
					// char* name
	};

	
}; // namespace SimStep

#endif // SimStep_TetraMeshFileFormat_h__