#ifndef SimStep_XmlShapeSerializer_h__
#define SimStep_XmlShapeSerializer_h__


#include <Core/PhysicsClasses.h>
#include <Utilities/Common.h>
#include <Utilities/SimdMemoryBuffer.h>

//class TiXmlDocument;
class TiXmlElement;


namespace SimStep
{

	class _SimStepExport XmlShapeSerializer
	{
	
	public:

		XmlShapeSerializer();
		~XmlShapeSerializer();
		
		void import(const String& stream, Shape* pShape);

		void export(const Shape* pShape, const String& filename);


	protected:

		void writePolyLine(TiXmlElement* shapeNode, Shape* pShape );


		void readPolyLine(TiXmlElement* shapeNode, Shape* pShape );



		void writePoints(TiXmlElement* parentNode, BufferSharedPtr<Vector3>& pointsbuf);
		
		
		void readPoints(TiXmlElement* parentNode, BufferSharedPtr<Vector3>& pointsbuf);
		// State for export
		//TiXmlDocument* mXMLDoc;
		// State for import
		Shape* mpShape;


	};


	
}; // namespace SimStep

#endif // SimStep_ShapeSerializer_h__