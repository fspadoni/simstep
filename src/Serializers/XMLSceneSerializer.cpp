#include "Serializers/XMLSceneSerializer.h"

#include "Serializers/tinyxml.h"

#include "Core/TetraMesh.h"

namespace SimStep
{

	XMLSceneSerializer::XMLSceneSerializer()
	{
		mXMLDoc = new TiXmlDocument();
	}

	XMLSceneSerializer::~XMLSceneSerializer()
	{
		delete mXMLDoc;
	}

	void XMLSceneSerializer::exportSceneInfo()
	{


		//TiXmlElement* sceneNode = mXMLDoc->InsertEndChild(TiXmlElement("scene"))->ToElement();
		//sceneNode->SetAttribute("version", "1.0" );
		//sceneNode->SetAttribute("upAxis", "y" );
		//sceneNode->SetAttribute("unitsPerMeter", "1.0" );
		//sceneNode->SetAttribute("author", "SimStep" );

	}

	TiXmlElement* XMLSceneSerializer::exportNodeTM(const Vector3& pos, const Quat& orientation, 
		const Vector3& scale, const String& name)
	{

		TiXmlElement* sceneNode = mXMLDoc->InsertEndChild(TiXmlElement("scene"))->ToElement();
		sceneNode->SetAttribute("version", "1.0" );
		sceneNode->SetAttribute("upAxis", "y" );
		sceneNode->SetAttribute("unitsPerMeter", "1.0" );
		sceneNode->SetAttribute("author", "SimStep" );

		TiXmlElement* nodesElem = 
			sceneNode->InsertEndChild(TiXmlElement("nodes"))->ToElement();


		TiXmlElement* rootnode = nodesElem->InsertEndChild(TiXmlElement("node"))->ToElement();
		rootnode->SetAttribute("name", name );

		// Collision Node name
		TiXmlElement* positionNode = rootnode->InsertEndChild(TiXmlElement("position"))->ToElement();
		positionNode->SetAttribute("x", pos[0] );
		positionNode->SetAttribute("y", pos[1] );
		positionNode->SetAttribute("z", pos[2] );

		// Collision Node name
		TiXmlElement* orientationNode = rootnode->InsertEndChild(TiXmlElement("rotation"))->ToElement();
		orientationNode->SetAttribute("qx", orientation[0] );
		orientationNode->SetAttribute("qy", orientation[1] );
		orientationNode->SetAttribute("qz", orientation[2] );
		orientationNode->SetAttribute("qw", orientation[3] );

		// Collision Node name
		TiXmlElement* scaleNode = rootnode->InsertEndChild(TiXmlElement("scale"))->ToElement();
		scaleNode->SetAttribute("x", scale[0]);
		scaleNode->SetAttribute("y", scale[1] );
		scaleNode->SetAttribute("z", scale[2] );

		return rootnode;
	}

	TiXmlElement* XMLSceneSerializer::exportNodeInfo(const Vector3& pos, const Quat& orientation, 
		const Vector3& scale, const String& name, TiXmlElement* elemNode )
	{
		TiXmlElement* rootnode = elemNode->InsertEndChild(TiXmlElement("node"))->ToElement();
		rootnode->SetAttribute("name", name );

		// Collision Node name
		TiXmlElement* positionNode = rootnode->InsertEndChild(TiXmlElement("position"))->ToElement();
		positionNode->SetAttribute("x", pos[0] );
		positionNode->SetAttribute("y", pos[1] );
		positionNode->SetAttribute("z", pos[2] );

		// Collision Node name
		TiXmlElement* orientationNode = rootnode->InsertEndChild(TiXmlElement("rotation"))->ToElement();
		orientationNode->SetAttribute("qx", orientation[0] );
		orientationNode->SetAttribute("qy", orientation[1] );
		orientationNode->SetAttribute("qz", orientation[2] );
		orientationNode->SetAttribute("qw", orientation[3] );

		// Collision Node name
		TiXmlElement* scaleNode = rootnode->InsertEndChild(TiXmlElement("scale"))->ToElement();
		scaleNode->SetAttribute("x", scale[0]);
		scaleNode->SetAttribute("y", scale[1] );
		scaleNode->SetAttribute("z", scale[2] );

		return rootnode;
	}


	TiXmlElement* XMLSceneSerializer::exportPhysicsEntity( TetraMeshPtr tetraMesh, 
		const String& tetrafilename, TiXmlElement* pXmlElem )
	{
		TiXmlElement* rootnode = pXmlElem->InsertEndChild(TiXmlElement("physicsentity"))->ToElement();
		rootnode->SetAttribute("name", tetraMesh->getName() );
		rootnode->SetAttribute("meshfile", tetrafilename );

		return rootnode;
	}


	void XMLSceneSerializer::writeFile(const String& filename)
	{
		mXMLDoc->SaveFile(filename.c_str());

	}
	
}; // namespace SimStep