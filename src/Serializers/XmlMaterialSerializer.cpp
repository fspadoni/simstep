#include <Serializers/XmlMaterialSerializer.h>

#include <Serializers/tinyxml.h>
#include <Core/LogManager.h>
#include <Core/MaterialManager.h>
#include <Core/Material.h>
#include <Utilities/StringConverter.h>

namespace SimStep
{

	XmlMaterialSerializer::XmlMaterialSerializer()
	{
	}

	XmlMaterialSerializer::~XmlMaterialSerializer()
	{
	}

	void XmlMaterialSerializer::importMaterials(const String& stream, const String& groupName )
	{
		LogManager::getSingleton().logMessage("XmlMaterialSerializer reading materials data from stream"/* + filename + "..." */);

		mXMLDoc = new TiXmlDocument();
		mXMLDoc->Parse( stream.c_str() , 0 );

		TiXmlElement* rootElem = mXMLDoc->RootElement();

		//int claimedCount = 0;
		//TiXmlElement* materials = rootElem->FirstChildElement("MATERIAL");
		//if (materials)
		//{

			// Iterate over all children (node entries) 
			for (TiXmlElement* matElem = rootElem->FirstChildElement();
				matElem != 0; matElem = matElem->NextSiblingElement() )
			{
				String materialName = matElem->Attribute("name");

				MaterialPtr mat = MaterialManager::getSingleton().create(
					/*"PhysicsMaterials/" + */materialName,  groupName );

				TiXmlElement* elem = matElem->FirstChildElement("density");
				mat->mDensity = StringConverter::parseReal( elem->FirstChild()->Value() );
		
				elem = matElem->FirstChildElement("youngs");
				mat->mYoungModulus = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("poissons");
				mat->mPoissonRatio = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("friction");
				mat->mFriction = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("toughness");
				mat->mToughness = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("yield");
				mat->mYield = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("maxYield");
				mat->mMaxYield = StringConverter::parseReal( elem->FirstChild()->Value() );

				elem = matElem->FirstChildElement("creep");
				mat->mCreep = StringConverter::parseReal( elem->FirstChild()->Value() );
			}

		//}


	}

	/** Exports a Materials to the named XML file. */
	void XmlMaterialSerializer::exportMaterials(const Material* pMaterial, const String& filename)
	{

	}



	
}; // namespace SimStep