#ifndef SimStep_XMLTetraMeshSerializer_h__
#define SimStep_XMLTetraMeshSerializer_h__

#include "Core/PhysicsClasses.h"

#include "Utilities/Common.h"

#include <Utilities/SimdMemoryBuffer.h>

class TiXmlDocument;
class TiXmlElement;

namespace SimStep
{

	class _SimStepExport XmlTetraMeshSerializer
	{

	public:
		XmlTetraMeshSerializer();
		~XmlTetraMeshSerializer();
		/** Imports a Mesh from the given XML file.
		@param filename The name of the file to import, expected to be in XML format.
		@param colourElementType The vertex element to use for packed colours
		@param pMesh The pre-created Mesh object to be populated.
		*/
		void importMesh(const String& stream, TetraMesh* pMesh);

		/** Exports a mesh to the named XML file. */
		void exportMesh(const TetraMesh* pMesh, const String& filename);


	protected:

		// State for export
		TiXmlDocument* mXMLDoc;
		// State for import
		TetraMesh* mpMesh;
		//NodeElementType mColourElementType;

		// Internal methods
		void writeMesh(const TetraMesh* pMesh);
		void writeNodes(TiXmlElement* mParentNode, const BufferSharedPtr<Vector3>& nodebuf);
		void writeConstrainedNodes(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& constrainednodebuf);

		void writeGraph(TiXmlElement* mParentNode, const Graph& graph );
		void writeFaces(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& facebuf);
		void writeVertices(TiXmlElement* verticesNode, const Vertices& vertices);
		void writeVertexNormals(TiXmlElement* verticesNode, const BufferSharedPtr<Vector3>& vNormals);

		void writeSubTetraMesh(TiXmlElement* mSubmeshesNode, const SubTetraMesh* s);
		void writeSubMesh(TiXmlElement* mSubmeshesNode, const SubMesh* s);
		//void writeGeometry(TiXmlElement* mParentNode, const NodeData* pData);
		//void writeNodeGraph(TiXmlElement* mMeshNode, const TetraMesh* m);
		//void writeLinks(TiXmlElement* mMeshNode, const TetraMesh* m);
		void writeSubTetraMeshNames(TiXmlElement* mMeshNode, const TetraMesh* m);
		void writeSubMeshNames(TiXmlElement* mMeshNode, const TetraMesh* m);

		void readNodes(TiXmlElement* mParentNode, const BufferSharedPtr<Vector3>& nodebuf);
		void readConstrainedNodes(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& constrainednodebuf);

		void readGraph(TiXmlElement* mParentNode, Graph* graph );
		void readSurfaceMesh(TiXmlElement* mParentNode, const BufferSharedPtr<ushort>& facebuf);
		//void readFaces(TiXmlElement* mParentNode, ElementData* pData);
		void readSubMeshes(TiXmlElement* mSubmeshesNode);
		//void readGeometry(TiXmlElement* mGeometryNode, NodeData* pData);
		void readSubTetraMeshNames(TiXmlElement* mTetraMeshNamesNode, TetraMesh *sm);
		void readSubMeshNames(TiXmlElement* mMeshNamesNode, TetraMesh* sm);
		void readSubTetraMeshes(TiXmlElement* mSubTetraMeshesNode);


	};

	
}; // namespace SimStep

#endif // SimStep_XMLTetraMeshSerializer_h__