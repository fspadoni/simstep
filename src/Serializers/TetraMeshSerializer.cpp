#include <Serializers/TetraMeshSerializer.h>

#include <Serializers/TetraMeshFileFormat.h>

#include <Core/TetraMesh.h>
#include <Core/SubTetraMesh.h>
#include <Core/LogManager.h>
#include <Utilities/PhysicsException.h>
#include <Core/SimdMemoryBufferManager.h>

#include <map>

namespace SimStep
{

	String TetraMeshSerializer::msCurrentVersion = "[TetraMeshSerializer_v1.0]";
	const unsigned short HEADER_CHUNK_ID = 0x1000;

	const long STREAM_OVERHEAD_SIZE = sizeof(uint16) + sizeof(uint32);


	TetraMeshSerializer::TetraMeshSerializer()
	{


	}
	
	
	TetraMeshSerializer::~TetraMeshSerializer()
	{

	}


	
	void TetraMeshSerializer::exportMesh(const TetraMesh* pMesh, const String& filename,
		Endian endianMode)
	{

		LogManager::getSingleton().logMessage("TetraMeshSerializer writing mesh data to " + filename + "...");

		// Decide on endian mode
		determineEndianness(endianMode);

		//// Check that the mesh has it's bounds set
		//if (pMesh->getBounds().isNull() || pMesh->getBoundingSphereRadius() == 0.0f)
		//{
		//	PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS, "The Mesh you have supplied does not have its"
		//		" bounds completely defined. Define them first before exporting.",
		//		"TetraMeshSerializerImpl::exportMesh");
		//}
		mpfFile = fopen(filename.c_str(), "wb");
		if (!mpfFile)
		{
			PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
				"Unable to open file " + filename + " for writing",
				"TetraMeshSerializerImpl::exportMesh");
		}

		writeFileHeader();
		LogManager::getSingleton().logMessage("File header written.");


		LogManager::getSingleton().logMessage("Writing tetra mesh data...");
		writeMesh(pMesh);
		LogManager::getSingleton().logMessage("Mesh data exported.");

		fclose(mpfFile);
		LogManager::getSingleton().logMessage("TetraMeshSerializer export successful.");

	}
	

	void TetraMeshSerializer::importMesh(DataStreamPtr& stream, TetraMesh* pMesh)
	{
		determineEndianness(stream);

		// Read header and determine the version
		unsigned short headerID;

		// Read header ID
		readShorts(stream, &headerID, 1);

		if (headerID != HEADER_CHUNK_ID)
		{
			PHYSICS_EXCEPT(Exception::ERR_INTERNAL_ERROR, "File header not found",
				"TetraMeshSerializer::importMesh");
		}
		// Read version
		String ver = readString(stream);
		// Jump back to start
		stream->seek(0);

		
		// Warn on old version of mesh
		
		// Check header
		readFileHeader(stream);

		unsigned short streamID;
		while(!stream->eof())
		{
			streamID = readChunk(stream);
			switch (streamID)
			{
			case M_MESH:
				readMesh(stream, pMesh);
				break;
			}

		}

	}



	void TetraMeshSerializer::writeMesh(const TetraMesh* pMesh)
	{
		// Header
		writeChunkHeader(M_MESH, calcMeshSize(pMesh));

		//// bool Physics - Collision - Graphics 
		//bool skelAnim = pMesh->hasSkeleton();
		//writeBools(&skelAnim, 1);

		// Write shared geometry
		if ( !pMesh->mSharedNodes.isNull()  )
		{
			writeSharedNodes(pMesh->mSharedNodes);
		}

		if ( !pMesh->mSharedConstrainedNodes.isNull() )
		{
			writeConstrainedNodes(pMesh->mSharedConstrainedNodes);
		}

		writeGraph( pMesh->mGraph );

		writeSurfaceMesh( pMesh->mFaces );

		// Write SubTetrameshes
		for (unsigned short i = 0; i < pMesh->getNumSubTetraMeshes(); ++i)
		{
			LogManager::getSingleton().logMessage("Writing subtetramesh...");
			writeSubTetraMesh( pMesh->getSubTetraMesh(i));
			LogManager::getSingleton().logMessage("Subtetramesh exported.");
		}

		// Write subtetramesh name table
		LogManager::getSingleton().logMessage("Exporting subtetramesh name table...");
		writeSubTetraMeshNameTable(pMesh);
		LogManager::getSingleton().logMessage("Subtetramesh name table exported.");


		// Write Submeshes
		for (unsigned short i = 0; i < pMesh->getNumSubMeshes(); ++i)
		{
			LogManager::getSingleton().logMessage("Writing submesh...");
			writeSubMesh(pMesh->getSubMesh(i));
			LogManager::getSingleton().logMessage("Submesh exported.");
		}

		
		//// Write bounds information
		//LogManager::getSingleton().logMessage("Exporting bounds information....");
		//writeBoundsInfo(pMesh);
		//LogManager::getSingleton().logMessage("Bounds information exported.");


		// Write submesh name table
		LogManager::getSingleton().logMessage("Exporting submesh name table...");
		writeSubMeshNameTable(pMesh);
		LogManager::getSingleton().logMessage("Submesh name table exported.");

		//// Write edge lists
		//if (pMesh->isEdgeListBuilt())
		//{
		//	LogManager::getSingleton().logMessage("Exporting edge lists...");
		//	writeEdgeList(pMesh);
		//	LogManager::getSingleton().logMessage("Edge lists exported");
		//}


		//// Write submesh extremes
		//writeExtremes(pMesh);

	}

	
	size_t TetraMeshSerializer::calcMeshSize(const TetraMesh* pMesh)
	{
		size_t size = STREAM_OVERHEAD_SIZE;

		// Num shared vertices
		size += sizeof(uint32);

		// nodes
		if ( pMesh->mSharedNodes.isNull() != 0)
		{
			size += pMesh->mSharedNodes->getSizeInBytes();
		}

		// Num graph column elements
		size += sizeof(uint32);

		// Num graph row and diagonal elements
		size += sizeof(uint32);

		// nodes
		//if (pMesh->mGraph & pMesh->mGraph-> ->getSize() > 0 )
		{
			size += pMesh->mGraph.rows->getSizeInBytes();
			size += pMesh->mGraph.columns->getSizeInBytes();
			size += pMesh->mGraph.diagonals->getSizeInBytes();
		}



		// SubTetrameshes
		for (unsigned short i = 0; i < pMesh->getNumSubTetraMeshes(); ++i)
		{
			size += calcSubTetraMeshSize(pMesh->getSubTetraMesh(i));
		}

		// SubTetramesh name table
		size += calcSubTetraMeshNameTableSize(pMesh);


		// Submeshes
		for (unsigned short i = 0; i < pMesh->getNumSubMeshes(); ++i)
		{
			size += calcSubMeshSize(pMesh->getSubMesh(i));
		}

		// Submesh name table
		size += calcSubMeshNameTableSize(pMesh);


		//// Edge list
		//if (pMesh->isEdgeListBuilt())
		//{
		//	size += calcEdgeListSize(pMesh);
		//}

		//// Animations
		//for (unsigned short a = 0; a < pMesh->getNumAnimations(); ++a)
		//{
		//	Animation* anim = pMesh->getAnimation(a);
		//	size += calcAnimationSize(anim);
		//}

		return size;
	}

	size_t TetraMeshSerializer::calcSubTetraMeshSize(const SubTetraMesh* pSub)
	{
		size_t size = STREAM_OVERHEAD_SIZE;

		bool idx32bit = false;
		
		// element type
		size += sizeof(unsigned int);

		// bool useSharedVertices
		size += sizeof(bool);
		// bool indexes32bit
		size += sizeof(bool);

		// unsigned int elementCount
		size += sizeof(unsigned int);
		
		//// unsigned int* / unsigned short* faceVertexIndices
		//if (idx32bit)
		//	size += sizeof(unsigned int) * pSub->indexData->indexCount;
		//else
		//	size += sizeof(unsigned short) * pSub->indexData->indexCount;

		size += pSub->mTetrahedrons->getSizeInBytes();


		return size;
	}


	size_t TetraMeshSerializer::calcSubTetraMeshNameTableSize(const TetraMesh* pMesh)
	{
		size_t size = STREAM_OVERHEAD_SIZE;
		// Figure out the size of the Name table.
		// Iterate through the subMeshList & add up the size of the indexes and names.
		TetraMesh::SubMeshNameMap::const_iterator it = pMesh->mSubTetraMeshNameMap.begin();
		while(it != pMesh->mSubTetraMeshNameMap.end())
		{
			// size of the index + header size for each element chunk
			size += STREAM_OVERHEAD_SIZE + sizeof(uint16);
			// name
			size += it->first.length() + 1;

			++it;
		}

		// size of the sub-mesh name table.
		return size;
	}


	size_t TetraMeshSerializer::calcSubMeshSize(const SubMesh* pSub)
	{
		size_t size = STREAM_OVERHEAD_SIZE;

		//bool idx32bit = (!pSub->indexData->indexBuffer.isNull() &&
			//pSub->indexData->indexBuffer->getType() == HardwareIndexBuffer::IT_32BIT);

		bool idx32bit = false;

		// bool useSharedVertices
		size += sizeof(bool);
		// bool indexes32bit
		size += sizeof(bool);
		// unsigned int* / unsigned short* faceVertexIndices
		
		//faces count
		size += sizeof(unsigned int);

		if (idx32bit)
			size +=  pSub->mFaces->getSizeInBytes();
		else
			size +=  pSub->mFaces->getSizeInBytes();
		

		// vertices count
		size += sizeof(unsigned int);

		size +=  pSub->mVertices.tetraIndex->getSizeInBytes();
		size +=  pSub->mVertices.barycentricCoords->getSizeInBytes();

		//normals count
		size += sizeof(unsigned int);

		size +=  pSub->mVertexNormals->getSizeInBytes();


		return size;
	}


	size_t TetraMeshSerializer::calcSubMeshNameTableSize(const TetraMesh* pMesh)
	{
		size_t size = STREAM_OVERHEAD_SIZE;
		// Figure out the size of the Name table.
		// Iterate through the subMeshList & add up the size of the indexes and names.
		TetraMesh::SubMeshNameMap::const_iterator it = pMesh->mSubTetraMeshNameMap.begin();
		while(it != pMesh->mSubTetraMeshNameMap.end())
		{
			// size of the index + header size for each element chunk
			size += STREAM_OVERHEAD_SIZE + sizeof(uint16);
			// name
			size += it->first.length() + 1;

			++it;
		}

		// size of the sub-mesh name table.
		return size;
	}


	void TetraMeshSerializer::writeSharedNodes( const BufferSharedPtr<Vector3>& sharednodes )
	{

		// Header
		size_t size = STREAM_OVERHEAD_SIZE;
		// sharednodes count
		size += sizeof(unsigned int);
		// // sharednodes buffer size
		size += sharednodes->getSizeInBytes();

		writeChunkHeader(M_SHARE_NODES, size);

		unsigned int nodeCount = sharednodes->getSize();
		writeInts(&nodeCount, 1);


		if (mFlipEndian)
		{
			// endian conversion
			// Copy data
			unsigned char* tempData = (unsigned char*)SimStepMalloc( sharednodes->getSizeInBytes(), MEMCATEGORY_GEOMETRY);
				
			memcpy(tempData, sharednodes->lockForRead<unsigned char>(), sharednodes->getSizeInBytes());
			sharednodes->unlock();

			flipToLittleEndian(
				tempData,
				sharednodes->getSize(),
				sharednodes->getDataSize() );
			writeData(tempData, sharednodes->getDataSize(), sharednodes->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( tempData, sharednodes->getSizeInBytes(), MEMCATEGORY_GEOMETRY );
		}
		else
		{
			writeData( sharednodes->lockForRead<unsigned char>(), 
				sharednodes->getDataSize(), sharednodes->getSize() );
			
			sharednodes->unlock();
		}

	}

	void TetraMeshSerializer::writeConstrainedNodes( const BufferSharedPtr<ushort>& constrainednodes )
	{
		// Header
		size_t size = STREAM_OVERHEAD_SIZE;
		// sharednodes count
		size += sizeof(unsigned int);
		// // sharednodes buffer size
		size += constrainednodes->getSizeInBytes();

		writeChunkHeader(M_CONSTRAINED_NODES, size);

		unsigned int nodeCount = constrainednodes->getSize();
		writeInts(&nodeCount, 1);


		if (mFlipEndian)
		{
			// endian conversion
			// Copy data
			unsigned char* tempData = (unsigned char*)SimStepMalloc( constrainednodes->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(tempData, constrainednodes->lockForRead<unsigned char>(), 
				constrainednodes->getSizeInBytes());
			constrainednodes->unlock();

			flipToLittleEndian(
				tempData,
				constrainednodes->getSize(),
				constrainednodes->getDataSize() );
			writeData(tempData, constrainednodes->getDataSize(), constrainednodes->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( tempData, constrainednodes->getSizeInBytes(), MEMCATEGORY_GEOMETRY );
		}
		else
		{
			writeData( constrainednodes->lockForRead<unsigned char>(), 
				constrainednodes->getDataSize(), constrainednodes->getSize() );

			constrainednodes->unlock();
		}
	}

	void TetraMeshSerializer::writeGraph( const Graph& graph )
	{

		// Header
		size_t size = STREAM_OVERHEAD_SIZE;
		// graph (columns) count
		size += sizeof(unsigned int);
		// row and diags count
		size += 2*sizeof(unsigned int);
		//  use32bit index
		size += sizeof(bool);

		// graph (columns)  size
		size += graph.columns->getSizeInBytes();
		// row and diags count
		size += graph.rows->getSizeInBytes();
		size += graph.diagonals->getSizeInBytes();


		writeChunkHeader(M_GRAPH, size);

		// 32 bitindex
		bool use32bitIndex = false;
		writeBools(&use32bitIndex, 1);
		// graph size
		unsigned int columnsCount = graph.columns->getSize();
		writeInts(&columnsCount, 1);
		unsigned int rowCount = graph.rows->getSize();
		writeInts(&rowCount, 1);
		unsigned int diagsCount = graph.diagonals->getSize();
		writeInts(&diagsCount, 1);


		if (mFlipEndian)
		{
			// endian conversion
			// Copy columns data
			unsigned char* columnsData = (unsigned char*)SimStepMalloc( graph.columns->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy( columnsData, graph.columns->lockForRead<void*>(), graph.columns->getSizeInBytes() );
			graph.columns->unlock();

			flipToLittleEndian(
				columnsData,
				graph.columns->getSize(),
				graph.columns->getDataSize() );
			writeData(columnsData, graph.columns->getDataSize(), graph.columns->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( columnsData, graph.columns->getSizeInBytes(), MEMCATEGORY_GEOMETRY );

			// Copy rows data
			unsigned char* rowsData = (unsigned char*)SimStepMalloc( graph.rows->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy( rowsData, graph.rows->lockForRead<void*>(), graph.rows->getSizeInBytes() );
			graph.columns->unlock();

			flipToLittleEndian(
				rowsData,
				graph.rows->getSize(),
				graph.rows->getDataSize() );
			writeData(rowsData, graph.rows->getDataSize(), graph.rows->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( rowsData, graph.rows->getSizeInBytes(), MEMCATEGORY_GEOMETRY );



			unsigned char* diagsData = (unsigned char*)SimStepMalloc( graph.diagonals->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy( diagsData, graph.diagonals->lockForRead<void*>(), graph.diagonals->getSizeInBytes() );
			graph.diagonals->unlock();

			flipToLittleEndian(
				diagsData,
				graph.diagonals->getSize(),
				graph.diagonals->getDataSize() );
			writeData(diagsData, graph.diagonals->getDataSize(), graph.diagonals->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( diagsData, graph.diagonals->getSizeInBytes(), MEMCATEGORY_GEOMETRY );

		}
		else
		{
			writeData( graph.columns->lockForRead<unsigned char*>(), 
				graph.columns->getDataSize(), graph.columns->getSize() );
			graph.columns->unlock();

			writeData( graph.rows->lockForRead<unsigned char*>(), 
				graph.rows->getDataSize(), graph.rows->getSize() );
			graph.rows->unlock();

			writeData( graph.diagonals->lockForRead<unsigned char*>(), 
				graph.diagonals->getDataSize(), graph.diagonals->getSize() );
			graph.diagonals->unlock();
		}

	}

	void TetraMeshSerializer::writeSurfaceMesh( const BufferSharedPtr<ushort>& faces)
	{
		// Header
		size_t size = STREAM_OVERHEAD_SIZE;
		// faces count
		size += sizeof(unsigned int);
		//  use32bit index
		size += sizeof(bool);
		// faces  size
		size += faces->getSizeInBytes();

		writeChunkHeader(M_SURFACE_MESH, size);


		bool use32bitIndex = false;
		writeBools(&use32bitIndex, 1);

		unsigned int  faceCount = faces->getSize() / 3;
		writeInts(&faceCount, 1);


		if (mFlipEndian)
		{
			// endian conversion
			// Copy data
			unsigned char* tempData = (unsigned char*)SimStepMalloc( faces->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(tempData, faces->lockForRead<unsigned char>(), faces->getSizeInBytes());
			faces->unlock();

			flipToLittleEndian(
				tempData,
				faces->getSize(),
				faces->getDataSize() );
			writeData(tempData, faces->getDataSize(), faces->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree ( tempData, faces->getSizeInBytes(), MEMCATEGORY_GEOMETRY );
		}
		else
		{
			writeData( faces->lockForRead<unsigned char>(), 
				faces->getDataSize(), faces->getSize() );

			faces->unlock();
		}


	}

	void TetraMeshSerializer::writeSubTetraMesh( const SubTetraMesh* pSubMesh )
	{
		// Header
		size_t size = STREAM_OVERHEAD_SIZE;
		// element type count
		size += sizeof(unsigned int);
		// elements count
		size += sizeof(unsigned int);
		//  use shared nodes
		size += sizeof(bool);
		//  use32bit index
		size += sizeof(bool);
		// element buffer  size
		size += pSubMesh->mTetrahedrons->getSizeInBytes();

		writeChunkHeader(M_SUBTETRAMESH, size);

		bool use32bitIndex = false;
		writeBools(&use32bitIndex, 1);

		bool useSharedNodes = pSubMesh->mUseSharedNodes;
		writeBools(&use32bitIndex, 1);

		unsigned int  meshtype = pSubMesh->mType;
		writeInts(&meshtype, 1);
		
		unsigned int  elemCount = pSubMesh->mTetrahedrons->getSize() / 4;
		writeInts(&elemCount, 1);

		if (mFlipEndian)
		{
			// endian conversion
			// Copy data
			unsigned char* tempData = (unsigned char*)SimStepMalloc( 
				pSubMesh->mTetrahedrons->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(tempData, pSubMesh->mTetrahedrons->lockForRead<unsigned char>(), 
				pSubMesh->mTetrahedrons->getSizeInBytes());
			pSubMesh->mTetrahedrons->unlock();

			flipToLittleEndian(
				tempData,
				pSubMesh->mTetrahedrons->getSize(),
				pSubMesh->mTetrahedrons->getDataSize() );
			writeData(tempData, pSubMesh->mTetrahedrons->getDataSize(), 
				pSubMesh->mTetrahedrons->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree( tempData, pSubMesh->mTetrahedrons->getSizeInBytes(), 
				MEMCATEGORY_GEOMETRY );
		}
		else
		{
			writeData( pSubMesh->mTetrahedrons->lockForRead<unsigned char>(), 
				pSubMesh->mTetrahedrons->getDataSize(), pSubMesh->mTetrahedrons->getSize() );

			pSubMesh->mTetrahedrons->unlock();
		}

	}


	void TetraMeshSerializer::writeSubTetraMeshNameTable(const TetraMesh* pMesh)
	{
		// Header
		writeChunkHeader(M_SUBTETRAMESH_NAME_TABLE, calcSubMeshNameTableSize(pMesh));

		// Loop through and save out the index and names.
		TetraMesh::SubTetraMeshNameMap::const_iterator it = pMesh->mSubTetraMeshNameMap.begin();

		while(it != pMesh->mSubTetraMeshNameMap.end())
		{
			// Header
			writeChunkHeader(M_SUBTETRAMESH_NAME_TABLE_ELEMENT, STREAM_OVERHEAD_SIZE +
				sizeof(unsigned short) + (unsigned long)it->first.length() + 1);

			// write the index
			writeShorts(&it->second, 1);
			// name
			writeString(it->first);

			++it;
		}
	}

	void TetraMeshSerializer::writeSubMesh( const SubMesh* submesh )
	{
		// Header
		writeChunkHeader(M_SUBMESH, calcSubMeshSize(submesh));

		bool use32bitIndex = false;
		writeBools(&use32bitIndex, 1);

		// bool useSharedVertices
		writeBools(&submesh->mUseSharedVertices, 1);

		unsigned int facesCount = submesh->mFaces->getSize() / 3;
		writeInts(&facesCount, 1);

		unsigned int vertexCount = submesh->mVertices.getSize();
		writeInts(&vertexCount, 1);

		unsigned int normalsCount = submesh->mVertexNormals->getSize();
		writeInts(&normalsCount, 1);


		if (mFlipEndian)
		{
			// endian conversion
			// Copy data
			unsigned char* facesData = (unsigned char*)SimStepMalloc( 
				submesh->mFaces->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(facesData, submesh->mFaces->lockForRead<unsigned char>(), 
				submesh->mFaces->getSizeInBytes());
			submesh->mFaces->unlock();

			flipToLittleEndian(
				facesData,
				submesh->mFaces->getSize(),
				submesh->mFaces->getDataSize() );
			writeData(facesData, submesh->mFaces->getDataSize(), 
				submesh->mFaces->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree( facesData, submesh->mFaces->getSizeInBytes(), 
				MEMCATEGORY_GEOMETRY );



			unsigned char* elemindexData = (unsigned char*)SimStepMalloc( 
				submesh->mVertices.tetraIndex->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(elemindexData, submesh->mVertices.tetraIndex->lockForRead<unsigned char>(), 
				submesh->mVertices.tetraIndex->getSizeInBytes());
			submesh->mVertices.tetraIndex->unlock();

			flipToLittleEndian(
				elemindexData,
				submesh->mVertices.tetraIndex->getSize(),
				submesh->mVertices.tetraIndex->getDataSize() );
			writeData(elemindexData, submesh->mVertices.tetraIndex->getDataSize(), 
				submesh->mVertices.tetraIndex->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree( elemindexData, submesh->mVertices.tetraIndex->getSizeInBytes(), 
				MEMCATEGORY_GEOMETRY );


			unsigned char* baryCoordsData = (unsigned char*)SimStepMalloc( 
				submesh->mVertices.barycentricCoords->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(baryCoordsData, submesh->mVertexNormals->lockForRead<unsigned char>(), 
				submesh->mVertices.barycentricCoords->getSizeInBytes());
			submesh->mVertices.barycentricCoords->unlock();

			flipToLittleEndian(
				baryCoordsData,
				submesh->mVertices.barycentricCoords->getSize(),
				submesh->mVertices.barycentricCoords->getDataSize() );
			writeData(baryCoordsData, submesh->mVertices.barycentricCoords->getDataSize(), 
				submesh->mVertices.barycentricCoords->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree( baryCoordsData, submesh->mVertices.barycentricCoords->getSizeInBytes(), 
				MEMCATEGORY_GEOMETRY );



			unsigned char* normalsData = (unsigned char*)SimStepMalloc( 
				submesh->mVertexNormals->getSizeInBytes(), MEMCATEGORY_GEOMETRY);

			memcpy(facesData, submesh->mVertexNormals->lockForRead<unsigned char>(), 
				submesh->mVertexNormals->getSizeInBytes());
			submesh->mVertexNormals->unlock();

			flipToLittleEndian(
				normalsData,
				submesh->mVertexNormals->getSize(),
				submesh->mVertexNormals->getDataSize() );
			writeData(normalsData, submesh->mVertexNormals->getDataSize(), 
				submesh->mVertexNormals->getSize() );
			//OGRE_FREE(tempData, MEMCATEGORY_GEOMETRY);
			SimStepFree( normalsData, submesh->mVertexNormals->getSizeInBytes(), 
				MEMCATEGORY_GEOMETRY );

		}
		else
		{
			writeData( submesh->mFaces->lockForRead<unsigned char>(), 
				submesh->mFaces->getDataSize(), submesh->mFaces->getSize() );
			submesh->mFaces->unlock();

			writeData( submesh->mVertices.tetraIndex->lockForRead<unsigned char>(), 
				submesh->mVertices.tetraIndex->getDataSize(), 
				submesh->mVertices.tetraIndex->getSize() );
			submesh->mVertices.tetraIndex->unlock();

			writeData( submesh->mVertices.barycentricCoords->lockForRead<unsigned char>(), 
				submesh->mVertices.barycentricCoords->getDataSize(), 
				submesh->mVertices.barycentricCoords->getSize() );
			submesh->mVertices.barycentricCoords->unlock();

			writeData( submesh->mVertexNormals->lockForRead<unsigned char>(), 
				submesh->mVertexNormals->getDataSize(), submesh->mVertexNormals->getSize() );
			submesh->mVertexNormals->unlock();
		}

	}


	void TetraMeshSerializer::writeSubMeshNameTable(const TetraMesh* pMesh)
	{
		// Header
		writeChunkHeader(M_SUBMESH_NAME_TABLE, calcSubMeshNameTableSize(pMesh));

		// Loop through and save out the index and names.
		TetraMesh::SubMeshNameMap::const_iterator it = pMesh->mSubMeshNameMap.begin();

		while(it != pMesh->mSubMeshNameMap.end())
		{
			// Header
			writeChunkHeader(M_SUBMESH_NAME_TABLE_ELEMENT, STREAM_OVERHEAD_SIZE +
				sizeof(unsigned short) + (unsigned long)it->first.length() + 1);

			// write the index
			writeShorts(&it->second, 1);
			// name
			writeString(it->first);

			++it;
		}
	}



	void TetraMeshSerializer::readMesh(DataStreamPtr& stream, TetraMesh* pMesh )
	{
		unsigned short streamID;

		//// bool physics collision graphics ?
		//bool skeletallyAnimated;
		//readBools(stream, &skeletallyAnimated, 1);


		// Find all substreams
		if (!stream->eof())
		{
			streamID = readChunk(stream);
			while(!stream->eof() &&
				(streamID == M_SHARE_NODES ||
				streamID == M_CONSTRAINED_NODES ||
				streamID == M_GRAPH ||
				streamID == M_SURFACE_MESH ||
				streamID == M_SUBTETRAMESH ||
				streamID == M_SUBTETRAMESH_NAME_TABLE ||
				streamID == M_SUBMESH ||
				streamID == M_SUBMESH_NAME_TABLE ) )
			{
				switch(streamID)
				{
				case M_SHARE_NODES:
					try {
						readSharedNodes(stream, pMesh );
					}
					catch (Exception& e)
					{
						if (e.getNumber() == Exception::ERR_ITEM_NOT_FOUND)
						{
							//// duff geometry data entry with 0 vertices
							//OGRE_DELETE pMesh->sharedVertexData;
							//pMesh->sharedVertexData = 0;
							//// Skip this stream (pointer will have been returned to just after header)
							//stream->skip(mCurrentstreamLen - STREAM_OVERHEAD_SIZE);
						}
						else
						{
							throw;
						}
					}
					break;
				case M_CONSTRAINED_NODES:
					readConstrainedNodes(stream, pMesh);
					break;
				case M_GRAPH:
					readGraph(stream, pMesh );
					break;
				case M_SURFACE_MESH:
					readSurfaceMesh(stream, pMesh );
					break;
				case M_SUBTETRAMESH:
					readSubTetraMesh(stream, pMesh);
					break;
				case M_SUBTETRAMESH_NAME_TABLE:
					readSubTetraMeshNameTable(stream, pMesh);
					break;
				case M_SUBMESH:
					readSubMesh(stream, pMesh);
					break;
				case M_SUBMESH_NAME_TABLE:
					readSubMeshNameTable(stream, pMesh);
					break;
				
				}

				if (!stream->eof())
				{
					streamID = readChunk(stream);
				}

			}

			if (!stream->eof())
			{
				// Backpedal back to start of stream
				stream->skip(-STREAM_OVERHEAD_SIZE);
			}
		}


	}



	void TetraMeshSerializer::readSharedNodes( DataStreamPtr& stream, TetraMesh* pMesh )
	{

		unsigned int nodeCount = 0;
		readInts(stream, &nodeCount, 1);

		BufferSharedPtr<Vector3> nodesbuf = 
			BufferManager::getSingleton().createBuffer<Vector3>( nodeCount );


		unsigned char* pNode = nodesbuf->lockForWrite<unsigned char>();

		stream->read( pNode, nodesbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pNode,
			nodesbuf->getSize(),
			nodesbuf->getDataSize() );
		
		nodesbuf->unlock();

		pMesh->mSharedNodes = nodesbuf;

	}


	void TetraMeshSerializer::readConstrainedNodes( DataStreamPtr& stream, TetraMesh* pMesh )
	{
		unsigned int nodeCount = 0;
		readInts(stream, &nodeCount, 1);

		if ( nodeCount > 0 )
		{
			BufferSharedPtr<ushort> constrainednodesbuf = 
				BufferManager::getSingleton().createBuffer<ushort>( nodeCount );

			unsigned char* pConstrNode = constrainednodesbuf->lockForWrite<unsigned char>();

			stream->read( pConstrNode, constrainednodesbuf->getSizeInBytes() );

			// endian conversion for OSX
			flipFromLittleEndian(
				pConstrNode,
				constrainednodesbuf->getSize(),
				constrainednodesbuf->getDataSize() );

			constrainednodesbuf->unlock();

			pMesh->mSharedConstrainedNodes = constrainednodesbuf;
		}
		else
		{
			//pMesh->mSharedConstrainedNodes = 0;
		}

	}

	void TetraMeshSerializer::readGraph( DataStreamPtr& stream, TetraMesh* pMesh )
	{

		// 32 bitindex
		bool use32bitIndex;
		readBools(stream, &use32bitIndex, 1);
		// graph size
		unsigned int columnsCount = 0;
		readInts(stream, &columnsCount, 1);
		unsigned int rowCount = 0;
		readInts(stream, &rowCount, 1);
		unsigned int diagsCount = 0;
		readInts(stream, &diagsCount, 1);

		BufferSharedPtr<ushort> columnsbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(columnsCount) ;

		unsigned char* pColumns = columnsbuf->lockForWrite<unsigned char>();

		stream->read( pColumns, columnsbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pColumns,
			columnsbuf->getSize(),
			columnsbuf->getDataSize() );

		columnsbuf->unlock();

		pMesh->mGraph.columns = columnsbuf;

		

		BufferSharedPtr<ushort> rowsbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(rowCount) ;

		unsigned char* pRows = rowsbuf->lockForWrite<unsigned char>();

		stream->read( pRows, rowsbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pRows,
			rowsbuf->getSize(),
			rowsbuf->getDataSize() );

		rowsbuf->unlock();

		pMesh->mGraph.rows = rowsbuf;



		BufferSharedPtr<ushort> diagsbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(diagsCount) ;

		unsigned char* pDiags = diagsbuf->lockForWrite<unsigned char>();

		stream->read( pDiags, diagsbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pDiags,
			diagsbuf->getSize(),
			diagsbuf->getDataSize() );

		diagsbuf->unlock();

		pMesh->mGraph.diagonals = diagsbuf;

	}


	void TetraMeshSerializer::readSurfaceMesh( DataStreamPtr& stream, TetraMesh* pMesh )
	{

		// 32 bitindex
		bool use32bitIndex;
		readBools(stream, &use32bitIndex, 1);
		// face size
		unsigned int facesCount = 0;
		readInts(stream, &facesCount, 1);

		BufferSharedPtr<ushort> facesbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(3*facesCount) ;

		unsigned char* pFaces = facesbuf->lockForWrite<unsigned char>();

		stream->read( pFaces, facesbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pFaces,
			facesbuf->getSize(),
			facesbuf->getDataSize() );

		facesbuf->unlock();

		pMesh->mFaces = facesbuf;

	}

	void TetraMeshSerializer::readSubTetraMesh( DataStreamPtr& stream, TetraMesh* pMesh )
	{

		SubTetraMesh* sm = pMesh->createSubTetraMesh();
		sm->mParent = pMesh;

		// 32 bitindex
		bool use32bitIndex;
		readBools(stream, &use32bitIndex, 1);

		// bool useSharedVertices
		readBools(stream, &sm->mUseSharedNodes, 1);

		unsigned int  meshtype = 0;
		readInts(stream, &meshtype, 1);
		sm->mType = static_cast<TetraMesh::Type>(meshtype);

		unsigned int  elemCount = 0;
		readInts(stream, &elemCount, 1);

		BufferSharedPtr<ushort> elementbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(elemCount*meshtype);
		
		unsigned char* pElems = elementbuf->lockForWrite<unsigned char>();

		stream->read( pElems, elementbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pElems,
			elementbuf->getSize(),
			elementbuf->getDataSize() );

		elementbuf->unlock();

		sm->mTetrahedrons = elementbuf;

	}


	void TetraMeshSerializer::readSubTetraMeshNameTable(DataStreamPtr& stream, TetraMesh* pMesh)
	{
		// The map for
		std::map<unsigned short, String> subMeshNames;
		unsigned short streamID, subMeshIndex;

		// Need something to store the index, and the objects name
		// This table is a method that imported meshes can retain their naming
		// so that the names established in the modelling software can be used
		// to get the sub-meshes by name. The exporter must support exporting
		// the optional stream M_SUBMESH_NAME_TABLE.

		// Read in all the sub-streams. Each sub-stream should contain an index and Ogre::String for the name.
		if (!stream->eof())
		{
			streamID = readChunk(stream);
			while(!stream->eof() && (streamID == M_SUBTETRAMESH_NAME_TABLE_ELEMENT ))
			{
				// Read in the index of the submesh.
				readShorts(stream, &subMeshIndex, 1);
				// Read in the String and map it to its index.
				subMeshNames[subMeshIndex] = readString(stream);

				// If we're not end of file get the next stream ID
				if (!stream->eof())
					streamID = readChunk(stream);
			}
			if (!stream->eof())
			{
				// Backpedal back to start of stream
				stream->skip(-STREAM_OVERHEAD_SIZE);
			}
		}

		// Set all the submeshes names
		// ?

		// Loop through and save out the index and names.
		std::map<unsigned short, String>::const_iterator it = subMeshNames.begin();

		while(it != subMeshNames.end())
		{
			// Name this submesh to the stored name.
			pMesh->nameSubTetraMesh(it->second, it->first);
			++it;
		}

	}


	void TetraMeshSerializer::readSubMesh( DataStreamPtr& stream, TetraMesh* pMesh )
	{
		SubMesh* sm = pMesh->createSubMesh();
		sm->mParent = pMesh;

		bool use32bitIndex;
		readBools(stream,&use32bitIndex, 1);

		// bool useSharedVertices
		readBools(stream,&sm->mUseSharedVertices, 1);


		unsigned int  faceCount = 0;
		readInts(stream, &faceCount, 1);
		unsigned int  vertexCount = 0;
		readInts(stream, &vertexCount, 1);
		unsigned int  normalsCount = 0;
		readInts(stream, &normalsCount, 1);


		BufferSharedPtr<ushort> facebuf = 
			BufferManager::getSingleton().createBuffer<ushort>(faceCount*3);

		unsigned char* pFaces = facebuf->lockForWrite<unsigned char>();

		stream->read( pFaces, facebuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pFaces,
			facebuf->getSize(),
			facebuf->getDataSize() );

		facebuf->unlock();

		sm->mFaces = facebuf;


		BufferSharedPtr<ushort> elemIndexbuf = 
			BufferManager::getSingleton().createBuffer<ushort>(vertexCount);

		unsigned char* pElemIndex = elemIndexbuf->lockForWrite<unsigned char>();

		stream->read( pElemIndex, elemIndexbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pElemIndex,
			elemIndexbuf->getSize(),
			elemIndexbuf->getDataSize() );

		elemIndexbuf->unlock();

		sm->mVertices.tetraIndex = elemIndexbuf;


		BufferSharedPtr<Vector3> baryCoordsbuf = 
			BufferManager::getSingleton().createBuffer<Vector3>(vertexCount);

		unsigned char* pBaryCoords = baryCoordsbuf->lockForWrite<unsigned char>();

		stream->read( pBaryCoords, baryCoordsbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pBaryCoords,
			baryCoordsbuf->getSize(),
			baryCoordsbuf->getDataSize() );

		baryCoordsbuf->unlock();

		sm->mVertices.barycentricCoords = baryCoordsbuf;


		BufferSharedPtr<Vector3> normalsbuf = 
			BufferManager::getSingleton().createBuffer<Vector3>(normalsCount);

		unsigned char* pNormals = normalsbuf->lockForWrite<unsigned char>();

		stream->read( pNormals, normalsbuf->getSizeInBytes() );

		// endian conversion for OSX
		flipFromLittleEndian(
			pNormals,
			normalsbuf->getSize(),
			normalsbuf->getDataSize() );

		normalsbuf->unlock();

		sm->mVertexNormals = normalsbuf;

	}


	void TetraMeshSerializer::readSubMeshNameTable(DataStreamPtr& stream, TetraMesh* pMesh)
	{
		// The map for
		std::map<unsigned short, String> subMeshNames;
		unsigned short streamID, subMeshIndex;

		// Need something to store the index, and the objects name
		// This table is a method that imported meshes can retain their naming
		// so that the names established in the modelling software can be used
		// to get the sub-meshes by name. The exporter must support exporting
		// the optional stream M_SUBMESH_NAME_TABLE.

		// Read in all the sub-streams. Each sub-stream should contain an index and Ogre::String for the name.
		if (!stream->eof())
		{
			streamID = readChunk(stream);
			while(!stream->eof() && (streamID == M_SUBMESH_NAME_TABLE_ELEMENT ))
			{
				// Read in the index of the submesh.
				readShorts(stream, &subMeshIndex, 1);
				// Read in the String and map it to its index.
				subMeshNames[subMeshIndex] = readString(stream);

				// If we're not end of file get the next stream ID
				if (!stream->eof())
					streamID = readChunk(stream);
			}
			if (!stream->eof())
			{
				// Backpedal back to start of stream
				stream->skip(-STREAM_OVERHEAD_SIZE);
			}
		}

		// Set all the submeshes names
		// ?

		// Loop through and save out the index and names.
		std::map<unsigned short, String>::const_iterator it = subMeshNames.begin();

		while(it != subMeshNames.end())
		{
			// Name this submesh to the stored name.
			pMesh->nameSubMesh(it->second, it->first);
			++it;
		}

	}


}; // namespace SimStep