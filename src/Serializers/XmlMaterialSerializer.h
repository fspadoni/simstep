#ifndef SimStep_XmlMaterialSerializer_h__
#define SimStep_XmlMaterialSerializer_h__

#include "Utilities/Common.h"

class TiXmlDocument;
class TiXmlElement;


namespace SimStep
{

	//forward decl
	class Material;

	class _SimStepExport XmlMaterialSerializer
	{

	public:

		XmlMaterialSerializer();
		
		~XmlMaterialSerializer();


		void importMaterials(const String& stream, const String& groupName );

		/** Exports a Materials to the named XML file. */
		void exportMaterials(const Material* pMaterial, const String& filename);

	private:

		TiXmlDocument* mXMLDoc;

	};

	
}; // namespace SimStep

#endif // SimStep_XmlMaterialSerializer_h__