#ifndef _ODE_UTIL_TASK_TBB_H_
#define _ODE_UTIL_TASK_TBB_H_


#include "tbb/task.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"

#include "objects.h"


typedef void (*dstepper_fn_t) (dxWorld *world, dxBody * const *body, int nb,
							   dxJoint * const *_joint, int nj, dReal stepsize);

class IslandStepperTask : public tbb::task
{

private:

	dstepper_fn_t stepper_fn;
	dxWorld* m_world;
	dxBody * const * m_body;
	int m_nb_body;
	dxJoint * const * m_joint;
	int m_nb_joint;
	dReal m_stepsize;

public:
	
	IslandStepperTask( dstepper_fn_t stepper, dxWorld *world, dxBody * const *body, int nb,
		dxJoint * const *_joint, int nj, dReal stepsize );

	tbb::task* execute();

};


class BodyStepper
{

private:

	const dstepper_fn_t stepper_fn;
	const dxWorld* m_world;
	const dxBody * const* m_body;
	const dReal m_stepsize;

public:

	BodyStepper( const dstepper_fn_t stepper, const dxWorld *world, const dxBody * const* body, const dReal stepsize );

	void operator()( const tbb::blocked_range<size_t>& r ) const;


};


class BodyStepperTask : public tbb::task
{

private:

	const dstepper_fn_t stepper_fn;
	const dxWorld* m_world;
	const dxBody * const* m_body;
	const unsigned int mNb_body;
	const dReal m_stepsize;

public:

	BodyStepperTask( dstepper_fn_t stepper, dxWorld *world, dxBody * const* body, 
		unsigned int nb_body, dReal stepsize );

	tbb::task* execute();


};



#endif