#include "util_taskTbb.h"

#include "joints/joint.h"


//#ifndef _DEBUG
//#define THREAD_PROFILER 1
//#endif

#ifdef THREAD_PROFILER
#include "libittnotify.h"
#endif

#ifdef THREAD_PROFILER
static __itt_event s_ThreadProfilerOdeSolverEvent;
#endif


IslandStepperTask::IslandStepperTask( dstepper_fn_t stepper, dxWorld *world, dxBody * const *body,
						 int nb, dxJoint * const *_joint, int nj, dReal stepsize )
{

	stepper_fn = stepper;
	m_world = world;
	m_body = body;
	m_nb_body = nb;
	m_joint = _joint;
	m_nb_joint = nj;
	m_stepsize = stepsize;

#ifdef THREAD_PROFILER
	s_ThreadProfilerOdeSolverEvent = __itt_event_createA( "OdeSolver", 9 );
#endif
}

tbb::task* IslandStepperTask::execute()
{


#ifdef THREAD_PROFILER
	__itt_event_start( s_ThreadProfilerOdeSolverEvent );
#endif

	// now do something with body and joint lists
	stepper_fn (m_world,m_body,m_nb_body,m_joint,m_nb_joint,m_stepsize);


#ifdef THREAD_PROFILER
	__itt_event_end( s_ThreadProfilerOdeSolverEvent );
#endif


	return NULL;
}



BodyStepper::BodyStepper( const dstepper_fn_t stepper, const dxWorld *world, 
						 const dxBody * const* body, const dReal stepsize )
						 : stepper_fn(stepper)
						 , m_world( world )
						 , m_body(body)
						 , m_stepsize(stepsize)
{

	//stepper_fn = stepper;
	//m_world = world;
	//m_body = body;
	//m_nb_body = nb;

	//m_stepsize = stepsize;

#ifdef THREAD_PROFILER
	s_ThreadProfilerOdeSolverEvent = __itt_event_createA( "OdeSolver", 9 );
#endif
}


void BodyStepper::operator()( const tbb::blocked_range<size_t>& r ) const
{


//#ifdef THREAD_PROFILER
//	__itt_event_start( s_ThreadProfilerOdeSolverEvent );
//#endif

	dxWorld* world = const_cast<dxWorld*>(m_world);
	dxBody * const * body = const_cast<dxBody * const *>(m_body);

	for( size_t i=r.begin(); i!=r.end( ); ++i )
	{
		stepper_fn( world, body+i, 1, 0,0, m_stepsize);
	}
	

//#ifdef THREAD_PROFILER
//	__itt_event_end( s_ThreadProfilerOdeSolverEvent );
//#endif

}


BodyStepperTask::BodyStepperTask( dstepper_fn_t stepper, dxWorld *world, dxBody * const* body,
								 unsigned int nb_body, dReal stepsize )
								 : stepper_fn(stepper)
								 , m_world( world )
								 , m_body(body)
								 , mNb_body(nb_body)
								 , m_stepsize(stepsize)
{

}

tbb::task* BodyStepperTask::execute()
{

	tbb::parallel_for( tbb::blocked_range<size_t>( 0, mNb_body, 32 ), 
		BodyStepper( stepper_fn, m_world, m_body, m_stepsize ), 
		tbb::auto_partitioner() );

	return NULL;
}

