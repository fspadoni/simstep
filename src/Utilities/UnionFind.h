#ifndef UNION_FIND_H
#define UNION_FIND_H

#include <vector>


namespace SimStep 
{

	class UnionFind
	{

	public:

		UnionFind( std::vector<int>& vec, int N );

		~UnionFind() {};

		AddConnection( int c1, int c2 );
		

	public:

		std::vector<int>& m_Parents;
		int m_NbNodes;


	};


	UnionFind::UnionFind(  std::vector<int>& vec, int N ) 
		: m_Parents(vec) , m_NbNodes(N)
	{
	}

	UnionFind::AddConnection( int c1, int c2 )
	{

		int r1 = m_Parents[c1];
		int r2 = m_Parents[c2];

		if ( r1 != r2 )
		{
			for (int i=0; i<m_NbNodes; i++ )
			{
				if ( m_Parents[i] == r1 )
				{
					m_Parents[i] = r2;
				}
			}
		}
		
	}



}

#endif