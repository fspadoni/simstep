#ifndef PHYSICS_STOP_WATCH_H
#define PHYSICS_STOP_WATCH_H



#define WIN32_LEAN_AND_MEAN
#define NOMINMAX // required to stop windows.h messing up std::min
#include <windows.h>

#include <intrin.h>
#pragma intrinsic(__rdtsc)

#include "Utilities/Definitions.h"

namespace SimStep
{


	//! Absolute timestamp  */
	class tick_count
	{
	public:

		//! Relative time interval.
		class interval_t 
		{
			unsigned __int64 value;
			// questa � usata solo nella funzione milliseconds e 
			// presuppone che la frequenza sia la stessa per tutte le cpus
			// se non si � sicuri usare solo la funzione seconds che ricalcola 
			// la frequenza a ogni chiamata su ciascuna cpus
			//double mInvCPUFrequency;


			explicit interval_t( unsigned __int64 value_ ) : value(value_) {}

		public:
			//! Construct a time interval representing zero time duration
			interval_t() : value(0) 
			{
				//LARGE_INTEGER frequency;
				//// Get the constant frequency
				//QueryPerformanceFrequency(&frequency);
				//mInvCPUFrequency = 1000.0 / (double) frequency.QuadPart;
			};

			//! Construct a time interval representing sec seconds time  duration
			explicit interval_t( double sec );

			//! Return the length of a time interval in seconds
			inline double seconds() const;

			inline double milliseconds() const;

			friend class tick_count;

			//! Extract the intervals from the tick_counts and subtract them.
			friend interval_t operator- ( const tick_count& t1, const tick_count& t0 );

			//! Add two intervals.
			friend interval_t operator+ ( const interval_t& i, const interval_t& j ) 
			{
				return interval_t(i.value+j.value);
			}

			//! Subtract two intervals.
			friend interval_t operator- ( const interval_t& i, const interval_t& j )
			{
				return interval_t(i.value-j.value);
			}

			//! Accumulation operator
			interval_t& operator+=( const interval_t& i ) 
			{
				value += i.value; 
				return *this;
			}

			//! Subtraction operator
			interval_t& operator-=( const interval_t& i ) 
			{
				value -= i.value; 
				return *this;
			}
		};

		//! Construct an absolute timestamp initialized to zero.
		tick_count() : my_count(0) 
		{
		};

		//! Return current time.
		static tick_count now();

		//! Subtract two timestamps to get the time interval between
		friend interval_t operator- ( const tick_count& t1, const tick_count& t0 );

	private:


		unsigned __int64 my_count;
	};


	inline tick_count tick_count::now()
	{
		tick_count result;
		LARGE_INTEGER qpcnt;
		QueryPerformanceCounter(&qpcnt);
		result.my_count = qpcnt.QuadPart;
		////Microsoft specific
		//tick_count result = static_cast<tick_count> __rdtsc();
		return  result;
	}

	inline tick_count::interval_t::interval_t( double sec )
	{
		LARGE_INTEGER qpfreq;
		QueryPerformanceFrequency(&qpfreq);
		value = static_cast<unsigned __int64>(sec*qpfreq.QuadPart);
	}

	inline tick_count::interval_t operator- ( const tick_count& t1, 
		const tick_count& t0 )
	{
		return tick_count::interval_t( t1.my_count-t0.my_count );
	}


	inline double tick_count::interval_t::seconds() const
	{

		LARGE_INTEGER qpfreq;
		QueryPerformanceFrequency(&qpfreq);
		return value/(double)qpfreq.QuadPart;

	}

	inline double tick_count::interval_t::milliseconds() const
	{

		LARGE_INTEGER qpfreq;
		QueryPerformanceFrequency(&qpfreq);
		return value * ( 1000.0 /(double)qpfreq.QuadPart );

	}


	class StopWatch
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( StopWatch, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


	private:

		tick_count mZeroClock;

		tick_count mStartTicks;

		LONGLONG mLastTime;
		LARGE_INTEGER mStartTime;

		double mInvCPUFrequency;

		DWORD_PTR mTimerMask;

	public:
		StopWatch();
		~StopWatch();

		void reset();

		double getSeconds();

		double getMilliseconds();


		double getIntervalSeconds();

		double getIntervalMilliseconds();

	};




}; // namespace SimStep

#endif