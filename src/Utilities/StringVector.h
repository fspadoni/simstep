#ifndef SimStep_StringVector_h__
#define SimStep_StringVector_h__

#include <vector>
#include <Utilities/SharedPointer.h>

namespace SimStep
{

	typedef std::vector<String> StringVector;
	typedef SharedPtr<StringVector> StringVectorPtr;

	
}; // namespace SimStep

#endif // SimStep_StringVector_h__