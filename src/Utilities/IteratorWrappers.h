#ifndef SimStep_IteratorWrappers_h__
#define SimStep_IteratorWrappers_h__

#include "Utilities/Common.h"
#include <vector>
#include <map>

#include "World/PhysicsWorld.h"

namespace SimStep
{

	/** Wraps iteration over a vector.
	@remarks
	This class is here just to allow clients to iterate over an internal
	vector of a class without having to have access to the vector itself
	(typically to iterate you need both the iterator and the end() iterator
	to test for the end condition, which is messy).
	No updates are allowed through this interface, it is purely for 
	iterating and reading.
	@par
	Note that like STL iterators, these iterators are only valid whilst no 
	updates are made to the underlying collection. You should not attempt to
	use this iterator if a change is made to the collection. In fact, treat this
	iterator as a transient object, do NOT store it and try to use it repeatedly.
	*/
	template <class T>
	class VectorIterator
	{
	private:
		typename T::iterator mCurrent;
		typename T::iterator mEnd;
		/// Private constructor since only the parameterised constructor should be used
		VectorIterator() {};
	public:
		typedef typename T::value_type ValueType;

		/** Constructor.
		@remarks
		Provide a start and end iterator to initialise.
		*/
		VectorIterator(typename T::iterator start, typename T::iterator end)
			: mCurrent(start), mEnd(end)
		{
		}

		/** Constructor.
		@remarks
		Provide a container to initialise.
		*/
		explicit VectorIterator(T& c)
			: mCurrent(c.begin()), mEnd(c.end())
		{
		}

		/** Returns true if there are more items in the collection. */
		bool hasMoreElements(void) const
		{
			return mCurrent != mEnd;
		}

		/** Returns the next element in the collection, and advances to the next. */
		typename T::value_type getNext(void)
		{
			return *mCurrent++;
		}
		/** Returns the next element in the collection, without advancing to the next. */
		typename T::value_type peekNext(void)
		{
			return *mCurrent;
		}
		/** Returns a pointer to the next element in the collection, without advancing to the next afterwards. */
		typename T::pointer peekNextPtr(void)
		{
			return &(*mCurrent);
		}
		/** Moves the iterator on one element. */
		void moveNext(void)
		{
			++mCurrent;
		}
	};


	typedef std::vector<PhysicsWorld*> PhysicsWorldArray;
	// specialized template
	class _SimStepExport PhysicsWorldArrayIterator : public VectorIterator<PhysicsWorldArray >
	{
	public:
		/** Constructor.
		@remarks
		Provide a start and end iterator to initialise.
		*/
		PhysicsWorldArrayIterator(PhysicsWorldArray::iterator start, PhysicsWorldArray::iterator end) 
			: VectorIterator( start, end)
		{
		}

		/** Constructor.
		@remarks
		Provide a container to initialise.
		*/
		explicit PhysicsWorldArrayIterator(PhysicsWorldArray& c ) 
			: VectorIterator( c )
		{
		}

	};

	typedef std::vector<CollisionObject*> CollisionObjectArray;
	// specialized template
	class _SimStepExport CollisionObjectArrayIterator 
		: public VectorIterator<CollisionObjectArray >
	{
	public:
		/** Constructor.
		@remarks
		Provide a start and end iterator to initialise.
		*/
		CollisionObjectArrayIterator(CollisionObjectArray::iterator start, CollisionObjectArray::iterator end) 
			: VectorIterator( start, end)
		{
		}

		/** Constructor.
		@remarks
		Provide a container to initialise.
		*/
		explicit CollisionObjectArrayIterator(CollisionObjectArray& c ) 
			: VectorIterator( c )
		{
		}

	};



	/** Wraps iteration over a map.
	@remarks
	This class is here just to allow clients to iterate over an internal
	map of a class without having to have access to the map itself
	(typically to iterate you need both the iterator and the end() iterator
	to test for the end condition, which is messy).
	No updates are allowed through this interface, it is purely for 
	iterating and reading.
	@par
	Note that like STL iterators, these iterators are only valid whilst no 
	updates are made to the underlying collection. You should not attempt to
	use this iterator if a change is made to the collection. In fact, treat this
	iterator as a transient object, do NOT store it and try to use it repeatedly.
	*/
	template <class T>
	class MapIterator
	{
	private:
		typename T::iterator mCurrent;
		typename T::iterator mEnd;
		/// Private constructor since only the parameterised constructor should be used
		MapIterator() {};
	public:
		typedef typename T::mapped_type MappedType;
		typedef typename T::key_type KeyType;

		/** Constructor.
		@remarks
		Provide a start and end iterator to initialise.
		*/
		MapIterator(typename T::iterator start, typename T::iterator end)
			: mCurrent(start), mEnd(end)
		{
		}

		/** Constructor.
		@remarks
		Provide a container to initialise.
		*/
		explicit MapIterator(T& c)
			: mCurrent(c.begin()), mEnd(c.end())
		{
		}

		/** Returns true if there are more items in the collection. */
		bool hasMoreElements(void) const
		{
			return mCurrent != mEnd;
		}

		/** Returns the next value element in the collection, and advances to the next. */
		typename T::mapped_type getNext(void)
		{
			return (mCurrent++)->second;
		}
		/** Returns the next value element in the collection, without advancing to the next. */
		typename T::mapped_type peekNextValue(void)
		{
			return mCurrent->second;
		}
		/** Returns the next key element in the collection, without advancing to the next. */
		typename T::key_type peekNextKey(void)
		{
			return mCurrent->first;
		}
		/** Required to overcome intermittent bug */
		MapIterator<T> & operator=( MapIterator<T> &rhs )
		{
			mCurrent = rhs.mCurrent;
			mEnd = rhs.mEnd;
			return *this;
		}
		/** Returns a pointer to the next value element in the collection, without 
		advancing to the next afterwards. */
		typename T::mapped_type* peekNextValuePtr(void)
		{
			return &(mCurrent->second);
		}
		/** Moves the iterator on one element. */
		void moveNext(void)
		{
			++mCurrent;
		}

	};



}; // namespace SimStep

#endif // SimStep_IteratorWrappers_h__