#ifndef PHYSICS_LOG_H
#define PHYSICS_LOG_H


#include "Core/PhysicsClasses.h"
#include "Utilities/String.h"
#include <Memory/MemoryManager.h>

#include <fstream>


namespace SimStep
{
	// LogMessageLevel + LoggingLevel > OGRE_LOG_THRESHOLD = message logged
#define PHYSICS_LOG_THRESHOLD 4

	/** The level of detail to which the log will go into.
	*/
	enum LoggingLevel
	{
		LL_LOW = 1,
		LL_NORMAL = 2,
		LL_BOREME = 3
	};

	/** The importance of a logged message.
	*/
	enum LogMessageLevel
	{
		LML_TRIVIAL = 1,
		LML_NORMAL = 2,
		LML_CRITICAL = 3
	};

	/** @remarks Pure Abstract class, derive this class and register to the Log to listen to log messages */
	class LogListener
	{
	public:
		virtual ~LogListener() {};

		/**
		@remarks
		This is called whenever the log receives a message and is about to write it out
		@param message
		The message to be logged
		@param lml
		The message level the log is using
		@param maskDebug
		If we are printing to the console or not
		@param logName
		the name of this log (so you can have several listeners for different logs, and identify them)
		*/
		virtual void messageLogged( const String& message, LogMessageLevel lml, bool maskDebug, const String &logName ) = 0;
	};


	/**
	@remarks
	Log class for writing debug/log data to files.
	@note
	<br>Should not be used directly, but trough the LogManager class.
	*/
	class Log //: public AlignedObject16 //GeneralAllocatedObject // 
	{
	public:
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Log, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL );


	protected:
		std::ofstream	m_fpLog;
		LoggingLevel	m_LogLevel;
		bool			m_DebugOut;
		bool			m_SuppressFile;
		String			m_LogName;

		typedef std::vector<LogListener*> mtLogListener;
		mtLogListener m_Listeners;

	public:

		class Stream;

		//PHYSICS_AUTO_MUTEX // public to allow external locking
		/**
		@remarks
		Usual constructor - called by LogManager.
		*/
		Log( const String& name, bool debugOutput = true, bool suppressFileOutput = false);

		/**
		@remarks
		Default destructor.
		*/
		~Log();

		/// Return the name of the log
		const String& getName() const { return m_LogName; }
		/// Get whether debug output is enabled for this log
		bool isDebugOutputEnabled() const { return m_DebugOut; }
		/// Get whether file output is suppressed for this log
		bool isFileOutputSuppressed() const { return m_SuppressFile; }

		/** Log a message to the debugger and to log file (the default is
		"<code>OGRE.log</code>"),
		*/
		void logMessage( const String& message, LogMessageLevel lml = LML_NORMAL, bool maskDebug = false );

		/** Get a stream object targetting this log. */
		Stream stream(LogMessageLevel lml = LML_NORMAL, bool maskDebug = false);

		/**
		@remarks
		Enable or disable outputting log messages to the debugger.
		*/
		void setDebugOutputEnabled(bool debugOutput);
		/**
		@remarks
		Sets the level of the log detail.
		*/
		void setLogDetail(LoggingLevel ll);
		/** Gets the level of the log detail.
		*/
		LoggingLevel getLogDetail() const { return m_LogLevel; }
		/**
		@remarks
		Register a listener to this log
		@param
		A valid listener derived class
		*/
		void addListener(LogListener* listener);

		/**
		@remarks
		Unregister a listener from this log
		@param
		A valid listener derived class
		*/
		void removeListener(LogListener* listener);

		/** Stream object which targets a log.
		@remarks
		A stream logger object makes it simpler to send various things to 
		a log. You can just use the operator<< implementation to stream 
		anything to the log, which is cached until a Stream::Flush is
		encountered, or the stream itself is destroyed, at which point the 
		cached contents are sent to the underlying log. You can use Log::stream()
		directly without assigning it to a local variable and as soon as the
		streaming is finished, the object will be destroyed and the message
		logged.
		@par
		You can stream control operations to this object too, such as 
		std::setw() and std::setfill() to control formatting.
		@note
		Each Stream object is not thread safe, so do not pass it between
		threads. Multiple threads can hold their own Stream instances pointing
		at the same Log though and that is threadsafe.
		*/
		class Stream
		{
		protected:
			Log* m_Target;
			LogMessageLevel m_Level;
			bool m_MaskDebug;
			typedef std::stringstream BaseStream;
			BaseStream m_Cache;

		public:

			/// Simple type to indicate a flush of the stream to the log
			struct Flush {};

			Stream(Log* target, LogMessageLevel lml, bool maskDebug)
				:m_Target(target), m_Level(lml), m_MaskDebug(maskDebug)
			{

			}
			// copy constructor
			Stream(const Stream& rhs) 
				: m_Target(rhs.m_Target), m_Level(rhs.m_Level),	m_MaskDebug(rhs.m_MaskDebug)
			{
				// explicit copy of stream required, gcc doesn't like implicit
				m_Cache.str(rhs.m_Cache.str());
			} 
			~Stream()
			{
				// flush on destroy
				if (m_Cache.tellp() > 0)
				{
					m_Target->logMessage(m_Cache.str(), m_Level, m_MaskDebug);
				}
			}

			template <typename T>
			Stream& operator<< (const T& v)
			{
				m_Cache << v;
				return *this;
			}

			Stream& operator<< (const Flush& v)
			{
				m_Target->logMessage(m_Cache.str(), m_Level, m_MaskDebug);
				m_Cache.str(StringUtil::BLANK);
				return *this;
			}


		};

	};

}; // namespace SimStep

#endif