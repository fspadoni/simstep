#ifndef SimStep_Singleton_h__
#define SimStep_Singleton_h__


#include <assert.h>
//#include <stdexcept>

namespace SimStep
{

	/** Template class for creating single-instance global classes.
	*/
	template <typename T> class Singleton
	{
	protected:

		static T* msSingleton;

	public:

		// not private but assert check in the implementation
		Singleton( void );

		~Singleton( void );

		Singleton( const Singleton& );

		Singleton& operator = ( const Singleton& );


		static T& getSingleton( void )
		{	
			//throw std::runtime_error("Singleton not created");
			assert( msSingleton ); 
			return ( *msSingleton ); 
		}

		static T* getSingletonPtr( void )
		{ 
			return msSingleton;
		}

	};


	/** Template class for creating single-instance global classes.
	*/
	template <typename T>
	Singleton<T>::Singleton( void )
	{
		if ( msSingleton != 0 )
		{
			//throw std::runtime_error("Singleton already created");
		}
		assert( !msSingleton );
		msSingleton = static_cast< T* >( this );
	}

	template <typename T>
	Singleton<T>::~Singleton( void )
	{ 
		assert( msSingleton );  
		msSingleton = 0;
	}


	template <typename T>
	Singleton<T>::Singleton( const Singleton& singleton )
	{
		if ( msSingleton != 0 )
		{
			//throw std::runtime_error("Singleton already created");
		}
		assert( !msSingleton );
	}

	template <typename T>
	Singleton<T>& Singleton<T>::operator = ( const Singleton<T>& )
	{
		if ( msSingleton != 0 )
		{
			//throw std::runtime_error("Singleton already created");
		}
		assert( !msSingleton );
		return static_cast< T& >( *this );
	}


}; // namespace SimStep

//#include "Singleton.inl"

#endif // SimStep_Singleton_h__