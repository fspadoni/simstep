#ifndef _DEFINITIONS_
#define _DEFINITIONS_


// Config

#ifdef _DEBUG

#define Aligned_Allocate(size, alignment)	 _aligned_malloc_dbg(size, alignment,__FILE__, __LINE__)
#define Aligned_Free(ptr)					 _aligned_free_dbg(ptr); ptr = NULL

#else

#define Aligned_Allocate(size, alignment)	 _aligned_malloc(size, alignment)
#define Aligned_Free(ptr)					 _aligned_free(ptr); ptr = NULL

#endif

#ifdef _DEBUG

//inline void* Allocate( size_t size, size_t alignment)
//{
//	return _aligned_malloc_dbg(size, alignment,__FILE__, __LINE__);
//}
//
//void Free(void* ptr)
//{
//	return _aligned_free_dbg(ptr);
//}

#else

//inline void* Allocate( size_t size, size_t alignment)
//{
//	return ::_aligned_malloc(size, alignment)
//}
//
//void Free(void* ptr)
//{
//	return ::_aligned_free(ptr);
//}

#endif

#define Release( Ptr )	if( Ptr != NULL ) {delete Ptr; Ptr = NULL; }

#define align16_struct  __declspec(align(16)) struct
#define align16_class  __declspec(align(16)) class
//#define Memory::SIMD_ALIGN 16
//#define align8 8
//#define SimdAlignment 16

// namespace
// namespace
#define SSE VectormathSSE 
#define Scalar Vectormath

#include "Memory/MemoryAllocatorConfig.h"
#include <Memory/MemoryManager.h>

#include "BulletCollision/CollisionShapes/btCollisionShape.h"



namespace SimStep
{
	typedef btCollisionShape CollisionShape;
}; // namespace SimStep


#endif