#include "Utilities/StringConverter.h"

#include "Math/Math.h"

#include "Ogre/ColourValue.h"

namespace SimStep
{


	//-----------------------------------------------------------------------
	String StringConverter::toString(Real val, unsigned short precision, 
		unsigned short width, char fill, std::ios::fmtflags flags)
	{
		std::stringstream stream;
		stream.precision(precision);
		stream.width(width);
		stream.fill(fill);
		if (flags)
			stream.setf(flags);
		stream << val;
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(int val, 
		unsigned short width, char fill, std::ios::fmtflags flags)
	{
		std::stringstream stream;
		stream.width(width);
		stream.fill(fill);
		if (flags)
			stream.setf(flags);
		stream << val;
		return stream.str();
	}


	String StringConverter::toString(size_t val, 
		unsigned short width, char fill, std::ios::fmtflags flags)
	{
		std::stringstream stream;
		stream.width(width);
		stream.fill(fill);
		if (flags)
			stream.setf(flags);
		stream << val;
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(unsigned long val, 
		unsigned short width, char fill, std::ios::fmtflags flags)
	{
		std::stringstream stream;
		stream.width(width);
		stream.fill(fill);
		if (flags)
			stream.setf(flags);
		stream << val;
		return stream.str();
	}

	String StringConverter::toString(long val, 
		unsigned short width, char fill, std::ios::fmtflags flags)
	{
		std::stringstream stream;
		stream.width(width);
		stream.fill(fill);
		if (flags)
			stream.setf(flags);
		stream << val;
		return stream.str();
	}

	//-----------------------------------------------------------------------
	String StringConverter::toString(const Vector3& val)
	{
		std::stringstream stream;
		stream << val.getX() << " " << val.getY() << " " << val.getZ();
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(const Vector4& val)
	{
		std::stringstream stream;
		stream << val.getX() << " " << val.getY() << " " << val.getZ() << " " << val.getW();
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(const Matrix3& val)
	{
		std::stringstream stream;
		stream << val[0][0] << " " 
			<< val[0][1] << " "             
			<< val[0][2] << " "             
			<< val[1][0] << " "             
			<< val[1][1] << " "             
			<< val[1][2] << " "             
			<< val[2][0] << " "             
			<< val[2][1] << " "             
			<< val[2][2];
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(bool val, bool yesNo)
	{
		if (val)
		{
			if (yesNo)
			{
				return "yes";
			}
			else
			{
				return "true";
			}
		}
		else
			if (yesNo)
			{
				return "no";
			}
			else
			{
				return "false";
			}
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(const Matrix4& val)
	{
		std::stringstream stream;
		stream << val[0][0] << " " 
			<< val[0][1] << " "             
			<< val[0][2] << " "             
			<< val[0][3] << " "             
			<< val[1][0] << " "             
			<< val[1][1] << " "             
			<< val[1][2] << " "             
			<< val[1][3] << " "             
			<< val[2][0] << " "             
			<< val[2][1] << " "             
			<< val[2][2] << " "             
			<< val[2][3] << " "             
			<< val[3][0] << " "             
			<< val[3][1] << " "             
			<< val[3][2] << " "             
			<< val[3][3];
		return stream.str();
	}
	//-----------------------------------------------------------------------
	String StringConverter::toString(const Quat& val)
	{
		std::stringstream stream;
		stream  << val.getW() << " " << val.getX() << " " << val.getY() << " " << val.getZ();
		return stream.str();
	}

	//String StringConverter::toString(const ColourValue& val)
	//{
	//	std::stringstream stream;
	//	stream << val.r << " " << val.g << " " << val.b << " " << val.a;
	//	return stream.str();
	//}

	//-----------------------------------------------------------------------
	String StringConverter::toString(const StringVector& val)
	{
		std::stringstream stream;
		StringVector::const_iterator i, iend, ibegin;
		ibegin = val.begin();
		iend = val.end();
		for (i = ibegin; i != iend; ++i)
		{
			if (i != ibegin)
				stream << " ";

			stream << *i; 
		}
		return stream.str();
	}
	//-----------------------------------------------------------------------
	Real StringConverter::parseReal(const String& val)
	{
		// Use istringstream for direct correspondence with toString
		std::istringstream str(val);
		Real ret = 0;
		str >> ret;

		return ret;
	}
	//-----------------------------------------------------------------------
	int StringConverter::parseInt(const String& val)
	{
		// Use istringstream for direct correspondence with toString
		std::istringstream str(val);
		int ret = 0;
		str >> ret;

		return ret;
	}
	//-----------------------------------------------------------------------
	unsigned int StringConverter::parseUnsignedInt(const String& val)
	{
		// Use istringstream for direct correspondence with toString
		std::istringstream str(val);
		unsigned int ret = 0;
		str >> ret;

		return ret;
	}
	//-----------------------------------------------------------------------
	long StringConverter::parseLong(const String& val)
	{
		// Use istringstream for direct correspondence with toString
		std::istringstream str(val);
		long ret = 0;
		str >> ret;

		return ret;
	}
	//-----------------------------------------------------------------------
	unsigned long StringConverter::parseUnsignedLong(const String& val)
	{
		// Use istringstream for direct correspondence with toString
		std::istringstream str(val);
		unsigned long ret = 0;
		str >> ret;

		return ret;
	}
	//-----------------------------------------------------------------------
	bool StringConverter::parseBool(const String& val)
	{
		return (StringUtil::startsWith(val, "true") || StringUtil::startsWith(val, "yes")
			|| StringUtil::startsWith(val, "1"));
	}

	//-----------------------------------------------------------------------
	Vector3 StringConverter::parseVector3(const String& val)
	{
		// Split on space
		std::vector<String> vec = StringUtil::split(val);

		if (vec.size() != 3)
		{
			return Vector3::Zero();
		}
		else
		{
			return Vector3(parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]));
		}
	}
	//-----------------------------------------------------------------------
	Vector4 StringConverter::parseVector4(const String& val)
	{
		// Split on space
		std::vector<String> vec = StringUtil::split(val);

		if (vec.size() != 4)
		{
			return Vector4::Zero();
		}
		else
		{
			return Vector4(parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]),parseReal(vec[3]));
		}
	}
	//-----------------------------------------------------------------------
	Matrix3 StringConverter::parseMatrix3(const String& val)
	{
		// Split on space
		std::vector<String> vec = StringUtil::split(val);

		if (vec.size() != 9)
		{
			return Matrix3::identity();
		}
		else
		{
			return Matrix3( Vector3( parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]) ),
				Vector3( parseReal(vec[3]),parseReal(vec[4]),parseReal(vec[5]) ),
				Vector3( parseReal(vec[6]),parseReal(vec[7]),parseReal(vec[8]) )
				);
		}
	}
	//-----------------------------------------------------------------------
	Matrix4 StringConverter::parseMatrix4(const String& val)
	{
		// Split on space
		std::vector<String> vec = StringUtil::split(val);

		if (vec.size() != 16)
		{
			return Matrix4::identity();
		}
		else
		{
			return Matrix4(  Vector4( parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]), parseReal(vec[3]) ),
				Vector4( parseReal(vec[4]),parseReal(vec[5]), parseReal(vec[6]), parseReal(vec[7]) ),
				Vector4( parseReal(vec[8]),parseReal(vec[9]), parseReal(vec[10]), parseReal(vec[11]) ),
				Vector4( parseReal(vec[12]),parseReal(vec[13]), parseReal(vec[14]), parseReal(vec[15]) )
				);
		}
	}
	//-----------------------------------------------------------------------
	Quat StringConverter::parseQuaternion(const String& val)
	{
		// Split on space
		std::vector<String> vec = StringUtil::split(val);

		if (vec.size() != 4)
		{
			return Quat::identity();
		}
		else
		{
			return Quat(parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]), parseReal(vec[3]));
		}
	}

	//ColourValue StringConverter::parseColourValue(const String& val)
	//{
	//	// Split on space
	//	std::vector<String> vec = StringUtil::split(val);

	//	if (vec.size() == 4)
	//	{
	//		return ColourValue(parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]), parseReal(vec[3]));
	//	}
	//	else if (vec.size() == 3)
	//	{
	//		return ColourValue(parseReal(vec[0]),parseReal(vec[1]),parseReal(vec[2]), 1.0f);
	//	}
	//	else
	//	{
	//		return ColourValue::Black;
	//	}
	//}

	//-----------------------------------------------------------------------
	StringVector StringConverter::parseStringVector(const String& val)
	{
		return StringUtil::split(val);
	}
	//-----------------------------------------------------------------------
	bool StringConverter::isNumber(const String& val)
	{
		std::istringstream str(val);
		float tst;
		str >> tst;
		return !str.fail() && str.eof();
	}

}; // namespace SimStep