#ifndef SimStep_SimdMemoryBuffer_h__
#define SimStep_SimdMemoryBuffer_h__

#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

#include <assert.h>

#include <Memory/MemoryManager.h>

#include <Utilities/SharedPointer.h>
#include <Core/SimdMemoryBufferManager.h>

namespace SimStep
{

	class _SimStepExport vBuffer
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( vBuffer, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )

		/// Should be called by HardwareBufferManager
		vBuffer(size_t dataSize, size_t numData  )
			: mDataSize( dataSize )
			, mNbData(numData)
		{
			// Calculate the size of the vertices
			mSizeInBytes = mDataSize * mNbData;

			// Allocate aligned memory
			mpData = SimStepAlignedMalloc(mSizeInBytes, Memory::CACHE_ALIGN
				,MEMCATEGORY_SIMD_BUFFER);

		}

		~vBuffer()
		{
			SimStepAlignedFree( mpData, mSizeInBytes, MEMCATEGORY_SIMD_BUFFER );
		}

		/// Returns the size of this buffer in bytes
		size_t getSizeInBytes(void) const { return mSizeInBytes; }

		/// Gets the size in bytes of a single vertex in this buffer
		size_t getDataSize(void) const { return mDataSize; }

		/// Get the number of vertices in this buffer
		size_t getSize(void) const { return mNbData; }


		template<typename T> 
		inline const T* lockForRead(void) const
			{return static_cast<const T*>(mpData);}
		
		template<typename T> 
		inline const T* lockForRead(const size_t& idx) const
			{return static_cast<const T*>(mpData) + idx;}


		template<typename T> 
		inline T* lockForWrite(void)
			{
				assert( sizeof(T) <= mDataSize );
				return static_cast<T*>(mpData);
			}

		template<typename T> 
		inline T* lockForWrite(const size_t& idx ) 
			{
				assert( sizeof(T) <= mDataSize );
				return static_cast<T*>(mpData) + idx;
			}


		inline const void* lockForRead(void) const {return mpData;}

		inline void unlock(void) const {};


		inline void* lockForWrite(void) {return mpData;}

		inline void unlock(void) {};


		inline void reset(const int val = 0)
		{
			unsigned char* pDst = this->lockForWrite<unsigned char>();
			memset( (void*)pDst, val, mSizeInBytes );
			this->unlock();
		}


		inline void readData( void* pDest, size_t offset, size_t length )
		{
			const unsigned char* pSrc = this->lockForRead<unsigned char>();
			memcpy(pDest, (void*)(pSrc + offset*mDataSize), length*mDataSize );
			this->unlock();
		}

		inline void writeData(  const void* pSource, size_t offset, size_t length )
		{
			unsigned char* pDst = this->lockForWrite<unsigned char>();
			memcpy((void*)(pDst + offset*mDataSize), pSource, length*mDataSize );
			this->unlock();    
		}


		//* Copy data from another buffer into this one
		inline void copyData( const vBuffer& srcBuffer, size_t srcOffset, 
			size_t dstOffset, size_t length )
		{
			const unsigned char* srcData = 
				srcBuffer.lockForRead<unsigned char>(srcOffset*mDataSize);
			this->writeData( (void*)srcData, dstOffset, length );
			srcBuffer.unlock();
		}

		
		//* Copy all data from another buffer into this one. 
		inline void copyData( const vBuffer& srcBuffer )
		{
			copyData(srcBuffer, 0, 0, mNbData );
		}


		size_t mSizeInBytes;
		size_t mNbData;
		size_t mDataSize;

		void* mpData;

	};

	class SimdChunk
	{
	public:

		SimdChunk(unsigned int dataSize, unsigned int numData/*, BufferManager* mgr*/  )
			: mDataSize( dataSize )
			, mNbData(numData)
			//, mMgr(mgr)
		{
			// Calculate the size of the vertices
			mSizeInBytes = mDataSize * mNbData;
		}

		~SimdChunk() {}

		/// Returns the size of this buffer in bytes
		unsigned int getSizeInBytes(void) const { return mSizeInBytes; }

		/// Gets the size in bytes of a single vertex in this buffer
		unsigned int getDataSize(void) const { return mDataSize; }

		/// Get the number of vertices in this buffer
		unsigned int getSize(void) const { return mNbData; }


	protected:

		unsigned int mSizeInBytes;
		unsigned int mNbData;
		unsigned int mDataSize;
		//BufferManager* mMgr;

	};

	template<typename T>
	class Buffer : public SimdChunk
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Buffer<T>, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )

			/// Should be called by HardwareBufferManager
		Buffer(unsigned int dataSize, unsigned int numData/*, BufferManager* mgr*/  )
			: SimdChunk( dataSize, numData/*, mgr*/ )
		{
			// Allocate aligned memory
			mpData = static_cast<T*>( SimStepAlignedMalloc(mSizeInBytes, Memory::CACHE_ALIGN
				,MEMCATEGORY_SIMD_BUFFER) );
		}

		~Buffer()
		{
			if (  BufferManager::getSingletonPtr() )
			{
				BufferManager::getSingletonPtr()->notifyBufferDestroyed(this);
			}
			SimStepAlignedFree( mpData, mSizeInBytes, MEMCATEGORY_SIMD_BUFFER );
		}

		
		inline const T* lockForRead(void) const
		{return mpData;}

		inline const T* lockForRead(const unsigned int& idx) const
		{return mpData + idx;}

		inline T* lockForWrite(void)
		{
			assert( sizeof(T) <= mDataSize );
			return mpData;
		}

		inline T* lockForWrite(const unsigned int& idx ) 
		{
			assert( sizeof(T) <= mDataSize );
			return mpData + idx;
		}

		template<typename T> 
		inline const T* lockForRead(void) const
		{return reinterpret_cast<const T*>(mpData);}

		template<typename T> 
		inline const T* lockForRead(const unsigned int& idx) const
		{return reinterpret_cast<const T*>(mpData) + idx;}


		template<typename T> 
		inline T* lockForWrite(void)
		{
			assert( sizeof(T) <= mDataSize );
			return reinterpret_cast<T*>(mpData);
		}

		template<typename T> 
		inline T* lockForWrite(const unsigned int& idx ) 
		{
			assert( sizeof(T) <= mDataSize );
			return reinterpret_cast<T*>(mpData) + idx;
		}


		inline void unlock(void) const {};

		inline void unlock(void) {};


		inline void reset(void)
		{
			T* pDst = this->lockForWrite();
			memset( (void*)pDst, 0, mSizeInBytes );
			this->unlock();
		}

		inline void reset(const T& val )
		{
			T* pDst = this->lockForWrite();
			int N = mNbData;
			while ( N-- > 0 )
			{
				memcpy( (void*)pDst++, (void*)&val, mDataSize );
			}
			this->unlock();
		}

		inline void readData( void* pDest, unsigned int offset, unsigned int length )
		{
			const T* pSrc = this->lockForRead();
			memcpy(pDest, (void*)(pSrc + offset), length*mDataSize );
			this->unlock();
		}

		inline void writeData(  const void* pSource, unsigned int offset, unsigned int length )
		{
			T* pDst = this->lockForWrite();
			memcpy((void*)(pDst + offset), pSource, length*mDataSize );
			this->unlock();    
		}


		//* Copy data from another buffer into this one
		inline void copyData( const Buffer<T>& srcBuffer, unsigned int srcOffset, 
			unsigned int dstOffset, unsigned int length )
		{
			const T* srcData = 
				srcBuffer.lockForRead<T>(srcOffset);
			this->writeData( (void*)srcData, dstOffset, length );
			srcBuffer.unlock();
		}


		//* Copy all data from another buffer into this one. 
		inline void copyData( const Buffer<T>& srcBuffer )
		{
			copyData(srcBuffer, 0, 0, mNbData );
		}


		T* mpData;
	};


	template<typename T>
	class BufferSharedPtr : public SharedPtr< Buffer<T> >
	{
	public:

		BufferSharedPtr() : SharedPtr< Buffer<T> >() 
		{}

		explicit BufferSharedPtr(Buffer<T>* buf) : SharedPtr< Buffer<T> >(buf) 
		{
		}

	};

	

	
	class _SimStepExport vBufferSharedPtr : public SharedPtr<vBuffer>
	{
	public:

		vBufferSharedPtr() 
			: SharedPtr<vBuffer>() 
		{}

		explicit vBufferSharedPtr(vBuffer* buf) : SharedPtr<vBuffer>(buf)
		{
		}

	};


	//class _SimStepExport SimdMemoryBuffer : public SimdBufferAllocatedObject
	//{

	//public:

	//	enum Usage
	//	{
	//		SMB_CPU_HEAP	= 1,
	//		SMB_CPU_STACK	= 2,
	//		SMB_GPU			= 4
	//	};


	//	/// Locking options
	//	enum LockOptions
	//	{
	//		/** Normal mode, ie allows read/write and contents are preserved. */
	//		HBL_NORMAL,
	//		/** Discards the <em>entire</em> buffer while locking; this allows optimisation to be 
	//		performed because synchronisation issues are relaxed. Only allowed on buffers 
	//		created with the HBU_DYNAMIC flag. 
	//		*/
	//		HBL_DISCARD,
	//		/** Lock the buffer for reading only. Not allowed in buffers which are created with HBU_WRITE_ONLY. 
	//		Mandatory on static buffers, i.e. those created without the HBU_DYNAMIC flag. 
	//		*/ 
	//		HBL_READ_ONLY,
	//		/** As HBL_NORMAL, except the application guarantees not to overwrite any 
	//		region of the buffer which has already been used in this frame, can allow
	//		some optimisation on some APIs. */
	//		HBL_NO_OVERWRITE

	//	};


	//protected:

	//	size_t mSizeInBytes;
	//	Usage mUsage;
	//	bool mIsLocked;
	//	size_t mLockStart;
	//	size_t mLockSize;
	//	bool mSystemMemory;
	//	//bool mUseShadowBuffer;
	//	//SimdMemoryBuffer* mpShadowBuffer;
	//	//bool mShadowUpdated;
	//	bool mSuppressHardwareUpdate;


	//	//virtual SimdMemoryBuffer* clone(bool copyData) const = 0;
	//
	//	/// Internal implementation of lock()
	//	virtual void* lockImpl(size_t offset, size_t length, LockOptions options) = 0;
	//	/// Internal implementation of unlock()
	//	virtual void unlockImpl(void) = 0;


	//public:
	//	/// Constructor, to be called by HardwareBufferManager only
	//	SimdMemoryBuffer( Usage usage, bool systemMemory ) 
	//		: mUsage(usage), mIsLocked(false), mSystemMemory(systemMemory), 
	//		mSuppressHardwareUpdate(false) 
	//	{
	//	}

	//	virtual ~SimdMemoryBuffer() {}

	//	/** Lock the entire buffer for (potentially) reading / writing.
	//	@param options Locking options
	//	@returns Pointer to the locked memory
	//	*/
	//	void* lock(LockOptions options)
	//	{
	//		return this->lock(0, mSizeInBytes, options);
	//	}

	//	/** Lock the buffer for (potentially) reading / writing.
	//	@param offset The byte offset from the start of the buffer to lock
	//	@param length The size of the area to lock, in bytes
	//	@param options Locking options
	//	@returns Pointer to the locked memory
	//	*/
	//	virtual void* lock(size_t offset, size_t length, LockOptions options)
	//	{
	//		assert(!isLocked() && "Cannot lock this buffer, it is already locked!");
	//		void* ret;
	//		{
	//			// Lock the real buffer if there is no shadow buffer 
	//			ret = lockImpl(offset, length, options);
	//			mIsLocked = true;
	//		}
	//		mLockStart = offset;
	//		mLockSize = length;
	//		return ret;
	//	}


	//	/** Releases the lock on this buffer. 
	//	@remarks 
	//	Locking and unlocking a buffer can, in some rare circumstances such as 
	//	switching video modes whilst the buffer is locked, corrupt the 
	//	contents of a buffer. This is pretty rare, but if it occurs, 
	//	this method will throw an exception, meaning you
	//	must re-upload the data.
	//	@par
	//	Note that using the 'read' and 'write' forms of updating the buffer does not
	//	suffer from this problem, so if you want to be 100% sure your
	//	data will not be lost, use the 'read' and 'write' forms instead.
	//	*/
	//	virtual void unlock(void)
	//	{
	//		assert(isLocked() && "Cannot unlock this buffer, it is not locked!");
	//		{
	//			// Otherwise, unlock the real one
	//			unlockImpl();
	//			mIsLocked = false;
	//		}

	//	}

	//	/** Reads data from the buffer and places it in the memory pointed to by pDest.
	//	@param offset The byte offset from the start of the buffer to read
	//	@param length The size of the area to read, in bytes
	//	@param pDest The area of memory in which to place the data, must be large enough to 
	//	accommodate the data!
	//	*/
	//	virtual void readData(size_t offset, size_t length, void* pDest) = 0;
	//	/** Writes data to the buffer from an area of system memory; note that you must
	//	ensure that your buffer is big enough.
	//	@param offset The byte offset from the start of the buffer to start writing
	//	@param length The size of the data to write to, in bytes
	//	@param pSource The source of the data to be written
	//	@param discardWholeBuffer If true, this allows the driver to discard the entire buffer when writing,
	//	such that DMA stalls can be avoided; use if you can.
	//	*/
	//	virtual void writeData(size_t offset, size_t length, const void* pSource,
	//		bool discardWholeBuffer = false) = 0;

	//	/** Copy data from another buffer into this one.
	//	@remarks
	//	Note that the source buffer must not be created with the
	//	usage HBU_WRITE_ONLY otherwise this will fail. 
	//	@param srcBuffer The buffer from which to read the copied data
	//	@param srcOffset Offset in the source buffer at which to start reading
	//	@param dstOffset Offset in the destination buffer to start writing
	//	@param length Length of the data to copy, in bytes.
	//	@param discardWholeBuffer If true, will discard the entire contents of this buffer before copying
	//	*/
	//	virtual void copyData(SimdMemoryBuffer& srcBuffer, size_t srcOffset, 
	//		size_t dstOffset, size_t length, bool discardWholeBuffer = false)
	//	{
	//		const void *srcData = srcBuffer.lock(
	//			srcOffset, length, HBL_READ_ONLY);
	//		this->writeData(dstOffset, length, srcData, discardWholeBuffer);
	//		srcBuffer.unlock();
	//	}

	//	/// Returns the size of this buffer in bytes
	//	size_t getSizeInBytes(void) const { return mSizeInBytes; }

	//	/// Returns the Usage flags with which this buffer was created
	//	Usage getUsage(void) const { return mUsage; }

	//	/// Returns whether this buffer is held in system memory
	//	bool isSystemMemory(void) const { return mSystemMemory; }

	//	/// Returns whether or not this buffer is currently locked.
	//	bool isLocked(void) const { 
	//		return mIsLocked; 
	//	}

	//};

	
}; // namespace SimStep


#endif // SimStep_SimdMemoryBuffer_h__