#include "Utilities/Profiler.h"



namespace SimStep
{

	//-----------------------------------------------------------------------
	template<> Profiler* Singleton<Profiler>::msSingleton = 0;

	Profiler* Profiler::getSingletonPtr(void)
	{
		return msSingleton;
	}
	Profiler& Profiler::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}

	//-----------------------------------------------------------------------


	Profiler::Profiler()
	{

	}

	Profiler::~Profiler()
	{

	}

	/** Sets the timer for the profiler */
	void Profiler::setStopWatch(StopWatch* t) 
	{

	};



}; // namespace SimStep