#include "Utilities/StopWatch.h"

namespace SimStep
{




	StopWatch::StopWatch()
	{
		reset();
	}


	StopWatch::~StopWatch()
	{

	}


	void StopWatch::reset()
	{

		LARGE_INTEGER frequency;
		// Get the constant frequency
		QueryPerformanceFrequency(&frequency);

		mInvCPUFrequency = 1000.0 / (double) frequency.QuadPart;

		// Query the timer
		mStartTicks = tick_count::now();

		//mStartTick = GetTickCount();

		//mLastTicks = 0;
		//mZeroClock = clock();

	}

	double StopWatch::getSeconds()
	{

		tick_count curTicks = tick_count::now();
		return ( curTicks- mStartTicks).seconds();
	}


	double StopWatch::getMilliseconds()
	{

		tick_count curTicks = tick_count::now();
		return  (curTicks - mStartTicks).milliseconds();
	}


	double StopWatch::getIntervalSeconds()
	{
		static tick_count sLastTicks = tick_count::now();
		
		tick_count curTicks = tick_count::now();
		double ret = ( curTicks- sLastTicks).seconds();
		sLastTicks = curTicks;
		return ret;
		
	}

	double StopWatch::getIntervalMilliseconds()
	{
		static tick_count sLastTicks = tick_count::now();

		tick_count curTicks = tick_count::now();
		double ret = ( curTicks- sLastTicks).milliseconds();
		sLastTicks = curTicks;
		return ret;

	}


}; // namespace SimStep