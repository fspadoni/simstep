#ifndef SimStep_Bounds_h__
#define SimStep_Bounds_h__

#include <Utilities/Definitions.h>
#include <Math/Math.h>

namespace SimStep
{

	align16_class Bounds3
	{
	public:

		inline Bounds3();

		inline ~Bounds3();

		inline void setEmpty();

		inline void setInfinite();

		inline void set(float minx, float miny, float minz, float maxx, float maxy,float maxz);
		
		inline void set(const Vector3& _min, const Vector3& _max);
		
		inline void include(const Vector3& v);


		Vector3 min, max;
	};

	inline Bounds3::Bounds3()
	{
		// Default to empty boxes for compatibility TODO: PT: remove this if useless
		setEmpty();
	}


	inline Bounds3::~Bounds3()
	{
		//nothing
	}

	inline void Bounds3::setEmpty()
	{
		// We know use this particular pattern for empty boxes
		set( FLT_MAX, FLT_MAX, FLT_MAX,
			(-FLT_MAX), (-FLT_MAX), (-FLT_MAX) ); 
	}

	inline void Bounds3::setInfinite()
	{
		set( (-FLT_MAX), (-FLT_MAX), (-FLT_MAX),
			FLT_MAX, FLT_MAX, FLT_MAX);
	}

	inline void Bounds3::set(float minx, float miny, float minz, float maxx, float maxy,float maxz)
	{
		min[0] = minx; 
		min[1] = miny;
		min[2] = minz;
		max[0] = maxx;
		max[1] = maxy;
		max[2] = maxz;
	}

	inline void Bounds3::set(const Vector3& _min, const Vector3& _max)
	{
		min = _min;
		max = _max;
	}

	inline void Bounds3::include(const Vector3& v)
	{
		min = Scalar::minPerElem(min, v);
		max = Scalar::maxPerElem(max, v);
	}

}; // namespace SimStep

#endif // SimStep_Bounds_h__