#include "Utilities/Log.h"

#include <iostream>
#include <iomanip>
#include <algorithm>

#include <time.h>


namespace SimStep
{

	//-----------------------------------------------------------------------
	Log::Log( const String& name, bool debuggerOuput, bool suppressFile )
		: 
	m_LogLevel(LL_NORMAL), m_DebugOut(debuggerOuput),
		m_SuppressFile(suppressFile), m_LogName(name)
	{
		if (!m_SuppressFile)
		{
			m_fpLog.open(name.c_str());
		}
	}
	//-----------------------------------------------------------------------
	Log::~Log()
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		if (!m_SuppressFile)
		{
			m_fpLog.close();
		}
	}
	//-----------------------------------------------------------------------
	void Log::logMessage( const String& message, LogMessageLevel lml, bool maskDebug )
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		if ((m_LogLevel + lml) >= PHYSICS_LOG_THRESHOLD)
		{
			for( mtLogListener::iterator i = m_Listeners.begin(); i != m_Listeners.end(); ++i )
				(*i)->messageLogged( message, lml, maskDebug, m_LogName );

			if (m_DebugOut && !maskDebug)
				std::cerr << message << std::endl;

			// Write time into log
			if (!m_SuppressFile)
			{
				struct tm *pTime;
				time_t ctTime; 
				time(&ctTime);
				pTime = localtime( &ctTime );
				m_fpLog << std::setw(2) << std::setfill('0') << pTime->tm_hour
					<< ":" << std::setw(2) << std::setfill('0') << pTime->tm_min
					<< ":" << std::setw(2) << std::setfill('0') << pTime->tm_sec 
					<< ": " << message << std::endl;

				// Flush stcmdream to ensure it is written (incase of a crash, we need log to be up to date)
				m_fpLog.flush();
			}
		}
	}

	//-----------------------------------------------------------------------
	void Log::setDebugOutputEnabled(bool debugOutput)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		m_DebugOut = debugOutput;
	}

	//-----------------------------------------------------------------------
	void Log::setLogDetail(LoggingLevel ll)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		m_LogLevel = ll;
	}

	//-----------------------------------------------------------------------
	void Log::addListener(LogListener* listener)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		m_Listeners.push_back(listener);
	}

	//-----------------------------------------------------------------------
	void Log::removeListener(LogListener* listener)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		m_Listeners.erase(std::find(m_Listeners.begin(), m_Listeners.end(), listener));
	}
	//---------------------------------------------------------------------
	Log::Stream Log::stream(LogMessageLevel lml, bool maskDebug) 
	{
		return Stream(this, lml, maskDebug);

	}


}; // namespace SimStep