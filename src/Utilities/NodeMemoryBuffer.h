#ifndef SimStep_NodeMemoryBuffer_h__
#define SimStep_NodeMemoryBuffer_h__

#include "Utilities/Common.h"

#include "Utilities/SimdMemoryBuffer.h"
#include "Utilities/SharedPointer.h"

//#include "../../../../OgreMain/include/OgrePrerequisites.h"
//#include "OgreSharedPtr.h"

namespace SimStep
{

	//forward decl;
	class NodeMemoryBufferSharedPtr;
	class NodeBufferSharedPtr;

	//class ColourValue;


	//class _SimStepExport NodeMemoryBuffer : public SimdMemoryBuffer
	//{
	//protected:

	//	size_t mNumNodes;
	//	size_t mNodeSize;

	//	// da eliminare
	//	unsigned char* mpData;


	//	/// Internal implementation of lock()
	//	virtual void* lockImpl(size_t offset, size_t length, LockOptions options);
	//	/// Internal implementation of unlock()
	//	virtual void unlockImpl(void);

	//public:
	//	/// Should be called by MemoryManager
	//	NodeMemoryBuffer(size_t nodeSize, size_t numNodes,
	//		NodeMemoryBuffer::Usage usage = SMB_CPU_HEAP, bool useSystemMemory = false );
	//	
	//	~NodeMemoryBuffer();
	//	
	//	//NodeMemoryBufferSharedPtr clone(bool copyData = true) const;


	//	/// Gets the size in bytes of a single node in this buffer
	//	size_t getNodeSize(void) const { return mNodeSize; }
	//	/// Get the number of nodes in this buffer
	//	size_t getNumNodes(void) const { return mNumNodes; }


	//	void* lock(size_t offset, size_t length, LockOptions options);

	//	void unlock(void);


	//	virtual void readData(size_t offset, size_t length, void* pDest);

	//	virtual void writeData(size_t offset, size_t length, const void* pSource,
	//		bool discardWholeBuffer = false);
	//};


	///** Shared pointer implementation used to share index buffers. */
	//class _SimStepExport NodeMemoryBufferSharedPtr : public Ogre::SharedPtr<NodeMemoryBuffer>
	//{
	//public:

	//	NodeMemoryBufferSharedPtr() : Ogre::SharedPtr<NodeMemoryBuffer>() {}
	//	explicit NodeMemoryBufferSharedPtr(NodeMemoryBuffer* buf);

	//};



	class _SimStepExport NodeBuffer : public SimdMemoryBuffer
	{
	protected:

		size_t mNumNodes;
		size_t mNodeSize;

		unsigned char* mpData;


		/** See HardwareBuffer. */
		void* lockImpl(size_t offset, size_t length, LockOptions options);
		/** See HardwareBuffer. */
		void unlockImpl(void);

	public:
		/// Should be called by HardwareBufferManager
		NodeBuffer(size_t vertexSize, size_t numVertices,
			NodeBuffer::Usage usage, bool useSystemMemory );
		
		~NodeBuffer();
		
		/// Gets the size in bytes of a single vertex in this buffer
		size_t getNodeSize(void) const { return mNodeSize; }
		
		/// Get the number of vertices in this buffer
		size_t getNumNodes(void) const { return mNumNodes; }


		/** See HardwareBuffer. */
		void readData(size_t offset, size_t length, void* pDest);
		/** See HardwareBuffer. */
		void writeData(size_t offset, size_t length, const void* pSource,
			bool discardWholeBuffer = false);
		/** Override HardwareBuffer to turn off all shadowing. */
		void* lock(size_t offset, size_t length, LockOptions options);
		/** Override HardwareBuffer to turn off all shadowing. */
		void unlock(void);

		/** Override HardwareBuffer to turn off all shadowing. */
		void* lock(LockOptions options);

	};


	

	/** Shared pointer implementation used to share index buffers. */
	class _SimStepExport NodeBufferSharedPtr : public Ogre::SharedPtr<NodeBuffer>
	{
	public:
		
		NodeBufferSharedPtr() 
			: Ogre::SharedPtr<NodeBuffer>() 
		{}

		explicit NodeBufferSharedPtr(NodeBuffer* buf);

	};


	/// Node element semantics, used to identify the meaning of node buffer contents
	enum NodeElementSemantic 
	{
		/// Position, 3 floats per node
		NES_POSITION = 1,
		/// Normal,  3 floats per node
		NES_NORMAL = 2,
		/// Barycentric coordinates 3 floats per node
		NES_BARYCENTRICS = 3,
		/// Node index 
		NES_NODE_INDICES = 4,
		/// Element index 
		NES_ELEM_INDICEX = 5,

		NES_DIFFUSE = 6,
		NES_SPECULAR = 7,
		NES_TEXTURE_COORDINATES

	};

	/// Node element type, used to identify the base types of the node contents
	enum NodeElementType
	{
		NET_FLOAT1 = 0,
		NET_FLOAT2 = 1,
		NET_FLOAT3 = 2,
		NET_FLOAT4 = 3,
		NET_ALIGNED16 = 4,

		// idx
		NET_USHORT1 = 5,
		NET_SHORT1	= 6,
		NET_UINT1	= 7,
		NET_INT1	= 8,

		/// D3D style compact colour
		NET_COLOUR_ARGB = 10,
		/// GL style compact colour
		NET_COLOUR_ABGR = 11
	};




	class _SimStepExport NodeElement
	{
	protected:
		/// The source node buffer, as bound to an index using NodeBufferBinding
		unsigned short mSource;
		/// The offset in the buffer that this element starts at
		size_t mOffset;
		/// The type of element
		NodeElementType mType;
		/// The meaning of the element
		NodeElementSemantic mSemantic;

		///// Index of the item, only applicable for some elements like texture coords
		//unsigned short mIndex;

	public:
		/// Constructor, should not be called directly, call VertexDeclaration::addElement
		NodeElement(unsigned short source, size_t offset, NodeElementType theType,
			NodeElementSemantic semantic, unsigned short index = 0);
		
		/// Gets the vertex buffer index from where this element draws it's values
		unsigned short getSource(void) const { return mSource; }
		
		/// Gets the offset into the buffer where this element starts
		size_t getOffset(void) const { return mOffset; }
		
		/// Gets the data format of this element
		NodeElementType getType(void) const { return mType; }
		
		/// Gets the meaning of this element
		NodeElementSemantic getSemantic(void) const { return mSemantic; }
		
		///// Gets the index of this element, only applicable for repeating elements
		//unsigned short getIndex(void) const { return mIndex; }
		
		/// Gets the size of this element in bytes
		size_t getSize(void) const;

		/// Utility method for helping to calculate offsets
		static size_t getTypeSize(NodeElementType etype);

		/// Utility method which returns the count of values in a given type
		static unsigned short getTypeCount(NodeElementType etype);

		/** Simple converter function which will turn a single-value type into a
		multi-value type based on a parameter.
		*/
		static NodeElementType multiplyTypeCount(NodeElementType baseType, unsigned short count);
		/** Simple converter function which will a type into it's single-value
		equivalent - makes switches on type easier.
		*/
		static NodeElementType getBaseType(NodeElementType multiType);

		/** Utility method for converting colour from
		one packed 32-bit colour type to another.
		@param srcType The source type
		@param dstType The destination type
		@param ptr Read / write value to change
		*/
		static void convertColourValue(NodeElementType srcType,
			NodeElementType dstType, uint* ptr);

		/** Utility method for converting colour to
		a packed 32-bit colour type.
		@param src source colour
		@param dst The destination type
		*/
		static uint convertColourValue(const ColourValue& src,
			NodeElementType dst);


		inline bool operator== (const NodeElement& rhs) const
		{
			if (mType != rhs.mType ||
				/*mIndex != rhs.mIndex ||*/
				mOffset != rhs.mOffset ||
				mSemantic != rhs.mSemantic ||
				mSource != rhs.mSource)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		/** Adjusts a pointer to the base of a node to point at this element.
		@remarks
		This variant is for void pointers, passed as a parameter because we can't
		rely on covariant return types.
		@param pBase Pointer to the start of a vertex in this buffer.
		@param pElem Pointer to a pointer which will be set to the start of this element.
		*/
		inline void baseNodePointerToElement(void* pBase, void** pElem) const
		{
			// The only way we can do this is to cast to char* in order to use byte offset
			// then cast back to void*.
			*pElem = static_cast<void*>(
				static_cast<unsigned char*>(pBase) + mOffset);
		}
		/** Adjusts a pointer to the base of a vertex to point at this element.
		@remarks
		This variant is for float pointers, passed as a parameter because we can't
		rely on covariant return types.
		@param pBase Pointer to the start of a vertex in this buffer.
		@param pElem Pointer to a pointer which will be set to the start of this element.
		*/
		inline void baseNodePointerToElement(void* pBase, float** pElem) const
		{
			// The only way we can do this is to cast to char* in order to use byte offset
			// then cast back to float*. However we have to go via void* because casting
			// directly is not allowed
			*pElem = static_cast<float*>(
				static_cast<void*>(
				static_cast<unsigned char*>(pBase) + mOffset));
		}

		/** Adjusts a pointer to the base of a vertex to point at this element.
		@remarks
		This variant is for RGBA pointers, passed as a parameter because we can't
		rely on covariant return types.
		@param pBase Pointer to the start of a vertex in this buffer.
		@param pElem Pointer to a pointer which will be set to the start of this element.
		*/
		inline void baseNodePointerToElement(void* pBase, uint** pElem) const
		{
			*pElem = static_cast<uint*>(
				static_cast<void*>(
				static_cast<unsigned char*>(pBase) + mOffset));
		}

		/** Adjusts a pointer to the base of a vertex to point at this element.
		@remarks
		This variant is for char pointers, passed as a parameter because we can't
		rely on covariant return types.
		@param pBase Pointer to the start of a vertex in this buffer.
		@param pElem Pointer to a pointer which will be set to the start of this element.
		*/
		inline void baseNodePointerToElement(void* pBase, unsigned char** pElem) const
		{
			*pElem = static_cast<unsigned char*>(pBase) + mOffset;
		}

		/** Adjusts a pointer to the base of a vertex to point at this element.
		@remarks
		This variant is for ushort pointers, passed as a parameter because we can't
		rely on covariant return types.
		@param pBase Pointer to the start of a vertex in this buffer.
		@param pElem Pointer to a pointer which will be set to the start of this element.
		*/
		inline void baseNodePointerToElement(void* pBase, unsigned short** pElem) const
		{
			*pElem = static_cast<unsigned short*>(pBase) + mOffset;
		}

	};



	class _SimStepExport NodeDeclaration : public GeneralAllocatedObject
	{
	public:
		/// Defines the list of vertex elements that makes up this declaration
		typedef std::list<NodeElement> NodeElementList;
		/// Sort routine for vertex elements
		static bool vertexElementLess(const NodeElement& e1, const NodeElement& e2);
	
	protected:
		NodeElementList mNodeElementList;
	
	public:
		/// Standard constructor, not you should use SimdNodeBufferManager::createNodeDeclaration
		NodeDeclaration();
		virtual ~NodeDeclaration();

		/** Get the number of elements in the declaration. */
		size_t getElementCount(void) { return mNodeElementList.size(); }
		
		/** Gets read-only access to the list of node elements. */
		const NodeElementList& getElements(void) const;
		
		/** Get a single element. */
		const NodeElement* getElement(unsigned short index);


		/** Gets the index of the highest source value referenced by this declaration. */
		unsigned short getMaxSource(void) const;


		
		virtual const NodeElement& addElement(unsigned short source, size_t offset, NodeElementType theType,
			NodeElementSemantic semantic, unsigned short index = 0);
		
	
		virtual const NodeElement& insertElement(unsigned short atPosition,
			unsigned short source, size_t offset, NodeElementType theType,
			NodeElementSemantic semantic, unsigned short index = 0);

		/** Remove the element at the given index from this declaration. */
		virtual void removeElement(unsigned short elem_index);

		
		virtual void removeElement(NodeElementSemantic semantic, unsigned short index = 0);

		/** Remove all elements. */
		virtual void removeAllElements(void);

		/** Modify an element in-place, params as addElement.
		@remarks
		<b>Please read the information in VertexDeclaration about
		the importance of ordering and structure for compatibility with older D3D drivers</b>.
		*/
		virtual void modifyElement(unsigned short elem_index, unsigned short source, size_t offset, NodeElementType theType,
			NodeElementSemantic semantic, unsigned short index = 0);

		/** Finds a VertexElement with the given semantic, and index if there is more than
		one element with the same semantic.
		@remarks
		If the element is not found, this method returns null.
		*/
		virtual const NodeElement* findElementBySemantic(NodeElementSemantic sem, unsigned short index = 0);
		/** Based on the current elements, gets the size of the vertex for a given buffer source.
		@param source The buffer binding index for which to get the vertex size.
		*/

		/** Gets a list of elements which use a given source.
		@remarks
		Note that the list of elements is returned by value therefore is separate from
		the declaration as soon as this method returns.
		*/
		virtual NodeElementList findElementsBySource(unsigned short source);

		/** Gets the node size defined by this declaration for a given source. */
		virtual size_t getNodeSize(unsigned short source);

		/** Clones this declaration. */
		virtual NodeDeclaration* clone(void);

		inline bool operator== (const NodeDeclaration& rhs) const
		{
			if (mNodeElementList.size() != rhs.mNodeElementList.size())
				return false;

			NodeElementList::const_iterator i, iend, rhsi, rhsiend;
			iend = mNodeElementList.end();
			rhsiend = rhs.mNodeElementList.end();
			rhsi = rhs.mNodeElementList.begin();
			for (i = mNodeElementList.begin(); i != iend && rhsi != rhsiend; ++i, ++rhsi)
			{
				if ( !(*i == *rhsi) )
					return false;
			}

			return true;
		}
		inline bool operator!= (const NodeDeclaration& rhs) const
		{
			return !(*this == rhs);
		}

	};




	class _SimStepExport NodeBufferBinding : public GeneralAllocatedObject
	{
	
	public:
		/// Defines the vertex buffer bindings used as source for node declarations
		typedef std::map<unsigned short, NodeBufferSharedPtr> NodeBufferBindingMap;

	protected:
	
		NodeBufferBindingMap mBindingMap;
		mutable unsigned short mHighIndex;


	public:

		/// Constructor, should not be called direct, use HardwareBufferManager::createVertexBufferBinding
		NodeBufferBinding();
		virtual ~NodeBufferBinding();
		/** Set a binding, associating a vertex buffer with a given index.
		@remarks
		If the index is already associated with a vertex buffer,
		the association will be replaced. This may cause the old buffer
		to be destroyed if nothing else is referring to it.
		You should assign bindings from 0 and not leave gaps, although you can
		bind them in any order.
		*/
		virtual void setBinding(unsigned short index, const NodeBufferSharedPtr& buffer);
		/** Removes an existing binding. */
		virtual void unsetBinding(unsigned short index);

		/** Removes all the bindings. */
		virtual void unsetAllBindings(void);

		/// Gets a read-only version of the buffer bindings
		virtual const NodeBufferBindingMap& getBindings(void) const;

		/// Gets the buffer bound to the given source index
		virtual const NodeBufferSharedPtr& getBuffer(unsigned short index) const;
		/// Gets whether a buffer is bound to the given source index
		virtual bool isBufferBound(unsigned short index) const;

		virtual size_t getBufferCount(void) const { return mBindingMap.size(); }

		/** Gets the highest index which has already been set, plus 1.
		@remarks
		This is to assist in binding the vertex buffers such that there are
		not gaps in the list.
		*/
		virtual unsigned short getNextIndex(void) const { return mHighIndex++; }

		/** Gets the last bound index.
		*/
		virtual unsigned short getLastBoundIndex(void) const;

		typedef std::map<ushort, ushort> BindingIndexMap;

		/** Check whether any gaps in the bindings.
		*/
		virtual bool hasGaps(void) const;

		/** Remove any gaps in the bindings.
		@remarks
		This is useful if you've removed vertex buffer from this vertex buffer
		bindings and want to remove any gaps in the bindings. Note, however,
		that if this bindings is already being used with a VertexDeclaration,
		you will need to alter that too. This method is mainly useful when
		reorganising buffers manually.
		@param
		bindingIndexMap To be retrieve the binding index map that used to
		translation old index to new index; will be cleared by this method
		before fill-in.
		*/
		virtual void closeGaps(BindingIndexMap& bindingIndexMap);

	};




}; // namespace SimStep

#endif // SimStep_NodeMemoryBuffer_h__