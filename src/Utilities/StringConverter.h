#ifndef PHYSICS_STRING_CONVERTER_H
#define PHYSICS_STRING_CONVERTER_H


#include "Utilities/Common.h"
#include "Core/PhysicsClasses.h"
#include "Utilities/String.h"
#include <Utilities/StringVector.h>

namespace SimStep
{

	////forward decl:
	//class ColourValue;

	/** Class for converting the core SimStep data types to/from Strings.
	@remarks
	The code for converting values to and from strings is here as a separate
	class to avoid coupling String to other datatypes (and vice-versa) which reduces
	compilation dependency: important given how often the core types are used.
	@par
	This class is mainly used for parsing settings in text files. External applications
	can also use it to interface with classes which use the StringInterface template
	class.
	@par
	The String formats of each of the major types is listed with the methods. The basic types
	like int and Real just use the underlying C runtime library atof and atoi family methods,
	however custom types like SimdVector3, ColourValue and Matrix4 are also supported by this class
	using custom formats.
	@author
	Steve Streeting
	*/
	class _SimStepExport StringConverter
	{
	public:

		/** Converts a Real to a String. */
		static String toString(Real val, unsigned short precision = 6, 
			unsigned short width = 0, char fill = ' ', 
			std::ios::fmtflags flags = std::ios::fmtflags(0) );
		
		
		/** Converts an int to a String. */
		static String toString(int val, unsigned short width = 0, 
			char fill = ' ', 
			std::ios::fmtflags flags = std::ios::fmtflags(0) );

		/** Converts a size_t to a String. */
		static String toString(size_t val, 
			unsigned short width = 0, char fill = ' ', 
			std::ios::fmtflags flags = std::ios::fmtflags(0) );
		/** Converts an unsigned long to a String. */
		static String toString(unsigned long val, 
			unsigned short width = 0, char fill = ' ', 
			std::ios::fmtflags flags = std::ios::fmtflags(0) );

		/** Converts a long to a String. */
		static String toString(long val, 
			unsigned short width = 0, char fill = ' ', 
			std::ios::fmtflags flags = std::ios::fmtflags(0) );
		/** Converts a boolean to a String. 
		@param yesNo If set to true, result is 'yes' or 'no' instead of 'true' or 'false'
		*/
		static String toString(bool val, bool yesNo = false);
		
		/** Converts a SimdVector3 to a String. 
		@remarks
		Format is "x y z" (i.e. 3x Real values, space delimited)
		*/
		static String toString(const Vector3& val);
		/** Converts a Vector4 to a String. 
		@remarks
		Format is "x y z w" (i.e. 4x Real values, space delimited)
		*/
		static String toString(const Vector4& val);
		/** Converts a SimdMatrix3 to a String. 
		@remarks
		Format is "00 01 02 10 11 12 20 21 22" where '01' means row 0 column 1 etc.
		*/
		static String toString(const Matrix3& val);
		/** Converts a Matrix4 to a String. 
		@remarks
		Format is "00 01 02 03 10 11 12 13 20 21 22 23 30 31 32 33" where 
		'01' means row 0 column 1 etc.
		*/
		static String toString(const Matrix4& val);
		/** Converts a Quaternion to a String. 
		@remarks
		Format is "w x y z" (i.e. 4x Real values, space delimited)
		*/
		static String toString(const Quat& val);
		
		/** Converts a ColourValue to a String. 
		@remarks
		Format is "r g b a" (i.e. 4x Real values, space delimited). 
		*/
		//static String toString(const ColourValue& val);

		/** Converts a StringVector to a string.
		@remarks
		Strings must not contain spaces since space is used as a delimiter in
		the output.
		*/
		static String toString(const StringVector& val);

		/** Converts a String to a Real. 
		@returns
		0.0 if the value could not be parsed, otherwise the Real version of the String.
		*/
		static Real parseReal(const String& val);
	
		/** Converts a String to a whole number. 
		@returns
		0.0 if the value could not be parsed, otherwise the numeric version of the String.
		*/
		static int parseInt(const String& val);
		/** Converts a String to a whole number. 
		@returns
		0.0 if the value could not be parsed, otherwise the numeric version of the String.
		*/
		static unsigned int parseUnsignedInt(const String& val);
		/** Converts a String to a whole number. 
		@returns
		0.0 if the value could not be parsed, otherwise the numeric version of the String.
		*/
		static long parseLong(const String& val);
		/** Converts a String to a whole number. 
		@returns
		0.0 if the value could not be parsed, otherwise the numeric version of the String.
		*/
		static unsigned long parseUnsignedLong(const String& val);
		/** Converts a String to a boolean. 
		@remarks
		Returns true if case-insensitive match of the start of the string
		matches "true", "yes" or "1", false otherwise.
		*/
		static bool parseBool(const String& val);
		
		/** Parses a SimdVector3 out of a String.
		@remarks
		Format is "x y z" ie. 3 Real components, space delimited. Failure to parse returns
		SimdVector3::ZERO.
		*/
		static Vector3 parseVector3(const String& val);
		/** Parses a Vector4 out of a String.
		@remarks
		Format is "x y z w" ie. 4 Real components, space delimited. Failure to parse returns
		Vector4::ZERO.
		*/
		static Vector4 parseVector4(const String& val);
		/** Parses a SimdMatrix3 out of a String.
		@remarks
		Format is "00 01 02 10 11 12 20 21 22" where '01' means row 0 column 1 etc.
		Failure to parse returns SimdMatrix3::IDENTITY.
		*/
		static Matrix3 parseMatrix3(const String& val);
		/** Parses a Matrix4 out of a String.
		@remarks
		Format is "00 01 02 03 10 11 12 13 20 21 22 23 30 31 32 33" where 
		'01' means row 0 column 1 etc. Failure to parse returns Matrix4::IDENTITY.
		*/
		static Matrix4 parseMatrix4(const String& val);
		/** Parses a Quaternion out of a String. 
		@remarks
		Format is "w x y z" (i.e. 4x Real values, space delimited). 
		Failure to parse returns Quaternion::IDENTITY.

		*/
		static Quat parseQuaternion(const String& val);

		/** Parses a ColourValue out of a String. 
		@remarks
		Format is "r g b a" (i.e. 4x Real values, space delimited), or "r g b" which implies
		an alpha value of 1.0 (opaque). Failure to parse returns ColourValue::Black.
		*/
		//static ColourValue parseColourValue(const String& val);

		/** Pareses a StringVector from a string.
		@remarks
		Strings must not contain spaces since space is used as a delimiter in
		the output.
		*/
		static StringVector parseStringVector(const String& val);
		/** Checks the String is a valid number value. */
		static bool isNumber(const String& val);
	};


} // namespace SimStep

#endif