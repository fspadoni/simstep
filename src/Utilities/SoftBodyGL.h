#ifndef SOFT_BODY_GL_H
#define SOFT_BODY_GL_H


//#include <GL/gl.h>


namespace SimStep {


	void DrawMesh( CollisionSoftBody* mesh)
	{
		int numTri = mesh->GetNbTriangles();
		TriangleElement *pTri = mesh->GetTriangles();
		SimdVector3* pNodes = mesh->GetNodes();
		unsigned int i0,i1,i2;

		SimdVector3 e1, e2, normal;

		glBegin(GL_TRIANGLES);
		for(unsigned int n=0;n<numTri;++n)
		{
			i0=pTri[n].n[0];
			i1=pTri[n].n[1];
			i2=pTri[n].n[2];

			e1 = pNodes[i1] - pNodes[i0];
			e2 = pNodes[i2] - pNodes[i0];
			normal = normalize(cross(e2,e1));

			glNormal3f(normal.getX(),normal.getY(),normal.getZ());

			float vMaterialColorDiffuse[4] = {1.0f,1.f,1.f,1.f};
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, vMaterialColorDiffuse);

			glVertex3f(pNodes[i0].getX(),pNodes[i0].getY(),pNodes[i0].getZ());
			glVertex3f(pNodes[i2].getX(),pNodes[i2].getY(),pNodes[i2].getZ());
			glVertex3f(pNodes[i1].getX(),pNodes[i1].getY(),pNodes[i1].getZ());

		}
		glEnd();
	}


	void DrawMesh(CollisionSoftBody &mesh)
	{
		int numTri = mesh.GetNbTriangles();
		TriangleElement *pTri = mesh.GetTriangles();
		SimdVector3* pNodes = mesh.GetNodes();
		unsigned int i0,i1,i2;

		SimdVector3 e1, e2, normal;

		glBegin(GL_TRIANGLES);
		for(unsigned int n=0;n<numTri;++n)
		{
			i0=pTri[n].n[0];
			i1=pTri[n].n[1];
			i2=pTri[n].n[2];

			e1 = pNodes[i1] - pNodes[i0];
			e2 = pNodes[i2] - pNodes[i0];
			normal = normalize(cross(e2,e1));

			glNormal3f(normal.getX(),normal.getY(),normal.getZ());

			float vMaterialColorDiffuse[4] = {1.0f,1.f,1.f,1.f};
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, vMaterialColorDiffuse);

			glVertex3f(pNodes[i0].getX(),pNodes[i0].getY(),pNodes[i0].getZ());
			glVertex3f(pNodes[i2].getX(),pNodes[i2].getY(),pNodes[i2].getZ());
			glVertex3f(pNodes[i1].getX(),pNodes[i1].getY(),pNodes[i1].getZ());

		}
		glEnd();
	}


	void DrawMesh(SimStep::CollisionSoftBody& mesh, SimdVector3* nodes)
	{


		SimdVector3 e1,e2,normal;


		int numTri = mesh.GetNbTriangles();
		TriangleElement *pTri = mesh.GetTriangles();
		//SimStep::SimdVector3* pNodes = mesh.GetNodesPtr();

		unsigned int i0,i1,i2;

		glBegin(GL_TRIANGLES);
		for(unsigned int n=0;n<numTri;++n)
		{
			i0=pTri[n].n[0];
			i1=pTri[n].n[1];
			i2=pTri[n].n[2];

			e1 = nodes[i1] - nodes[i0];
			e2 = nodes[i2] - nodes[i0];
			normal = normalize(cross(e2,e1));

			glNormal3f(normal.getX(),normal.getY(),normal.getZ());

			float vMaterialColorDiffuse[4] = {1.0f,1.f,1.f,1.f};
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, vMaterialColorDiffuse);

			glVertex3f(nodes[i0].getX(),nodes[i0].getY(),nodes[i0].getZ());
			glVertex3f(nodes[i2].getX(),nodes[i2].getY(),nodes[i2].getZ());
			glVertex3f(nodes[i1].getX(),nodes[i1].getY(),nodes[i1].getZ());

		}
		glEnd();
	}


	void DrawNodes( CollisionSoftBody* mesh)
	{

		float m[16];

		memset( (void*)m, 0, 16*sizeof(float) );
		m[0] = 1.0f;
		m[5] = 1.0f;
		m[10] = 1.0f;
		m[15] = 1.0;

		//SimStep::SimdVector3 stress[3*3*9];
		for (int i=0;i<mesh->GetNbNodes(); i++)
		{
			SimdVector3& pos = mesh->GetNodes()[i];
			m[12] = pos.getX();
			m[13] = pos.getY();
			m[14] = pos.getZ();

			glPushMatrix();
			glMultMatrixf(m);
			glutSolidSphere(0.25f, 8, 8);
			glPopMatrix();
		}
	}


	static void drawLine(const SimdVector3& from,const SimdVector3& to,const SimdVector3& color)
	{

		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3f(color.getX(), color.getY(), color.getZ());
		glVertex3d(from.getX(), from.getY(), from.getZ());
		glVertex3d(to.getX(), to.getY(), to.getZ());
		glEnd();
		glEnable(GL_LIGHTING);
	}

	static void	drawTriangle(const btVector3& v0,const btVector3& v1,const btVector3& v2,const btVector3& color, float alpha )
	{
		const btVector3	n=cross(v1-v0,v2-v0).normalized();
		glBegin(GL_TRIANGLES);		
		glColor4f(color.getX(), color.getY(), color.getZ(),alpha);
		glNormal3d(n.getX(),n.getY(),n.getZ());
		glVertex3d(v0.getX(),v0.getY(),v0.getZ());
		glVertex3d(v1.getX(),v1.getY(),v1.getZ());
		glVertex3d(v2.getX(),v2.getY(),v2.getZ());
		glEnd();

		//drawLine(v0,v1,color);
		//drawLine(v1,v2,color);
		//drawLine(v2,v0,color);
	}

	static void	drawBox( const btVector3& mins,	const btVector3& maxs, const btVector3& color)
	{
		const SimdVector3	c[]= {	
			SimdVector3(mins.x(),mins.y(),mins.z()),
			SimdVector3(maxs.x(),mins.y(),mins.z()),
			SimdVector3(maxs.x(),maxs.y(),mins.z()),
			SimdVector3(mins.x(),maxs.y(),mins.z()),
			SimdVector3(mins.x(),mins.y(),maxs.z()),
			SimdVector3(maxs.x(),mins.y(),maxs.z()),
			SimdVector3(maxs.x(),maxs.y(),maxs.z()),
			SimdVector3(mins.x(),maxs.y(),maxs.z())
		};
		drawLine(c[0],c[1],color);
		drawLine(c[1],c[2],color);
		drawLine(c[2],c[3],color);
		drawLine(c[3],c[0],color);
		drawLine(c[4],c[5],color);
		drawLine(c[5],c[6],color);
		drawLine(c[6],c[7],color);
		drawLine(c[7],c[4],color);
		drawLine(c[0],c[4],color);
		drawLine(c[1],c[5],color);
		drawLine(c[2],c[6],color);
		drawLine(c[3],c[7],color);
	}

	static void drawTree(	const btDbvtNode* node,	 int depth,	 const btVector3& ncolor,
		const btVector3& lcolor, int mindepth,	 int maxdepth)
	{

		if(node)
		{
			if(node->isinternal()&&((depth<maxdepth)||(maxdepth<0)))
			{
				drawTree( node->childs[0],depth+1,ncolor,lcolor,mindepth,maxdepth);
				drawTree( node->childs[1],depth+1,ncolor,lcolor,mindepth,maxdepth);
			}
			if(depth>=mindepth)
			{
				const btScalar	scl=(btScalar)(node->isinternal()?1:1);
				const btVector3	mi=node->volume.Center()-node->volume.Extents()*scl;
				const btVector3	mx=node->volume.Center()+node->volume.Extents()*scl;
				drawBox( mi,mx,node->isleaf()?lcolor:ncolor);

			}
		}
	}


	void DrawNodeTree( CollisionSoftBody* softbody,  int mindepth=0,	int maxdepth=-1 )
	{
		drawTree( softbody->getNodeDynamicTree().m_root,0,btVector3(1,0,1),btVector3(1,1,1),mindepth,maxdepth);
	}


	/* Contacts	*/ 
	void DrawContacts( CollisionSoftBody* softbody )
	{	
		static const btVector3	axis[]={btVector3(1,0,0),btVector3(0,1,0),btVector3(0,0,1)};
		SimdVector3 node, normal;

		for(int i=0;i< softbody->getDynamicContactArray().size();++i)
		{		
			RigidBodyBase* rigid_body = softbody->getDynamicContactArray()[i].m_RigidBody;

			node = softbody->GetNodes()[softbody->getDynamicContactArray()[i].m_node->IdNode];
			const SimdVector3 anchor = softbody->getDynamicContactArray()[i].m_RelAnchorPosition; // Relative anchor
			float depth = softbody->getDynamicContactArray()[i].m_ContactInfo.m_offset;
			memcpy( (void*)&normal, (void*)&softbody->getDynamicContactArray()[i].m_ContactInfo.m_normal, sizeof(btVector3) );

			normal = softbody->getDynamicContactArray()[i].m_ContactInfo.m_normal;

			drawLine( node, node+normal, SimdVector3( 1.0f,0.0f,0.0f) );
			//drawLine(o-y*nscl,o+y*nscl,ccolor);
			//drawLine(o,o+c.m_cti.m_normal*nscl*3,btVector3(1,1,0));
		}
	}


	/* Clusters	*/ 
	//#include "../../bullet-2.72/src/LinearMath/btConvexHull.h"
	//#include "../../bullet-2.72/src/LinearMath//btConvexHull.h"

	//void FemSoftBody::DrawClusters()
	//{
	//int i,j,nj;

	//for(i=0; i<m_ClustersArray.size();++i)
	//{

	//	btVector3 color( rand()/(btScalar)RAND_MAX,	rand()/(btScalar)RAND_MAX,
	//		rand()/(btScalar)RAND_MAX);
	//	color = color.normalized()*0.75;
	//	
	//	btAlignedObjectArray<SimdVector3>	vertices;
	//	vertices.resize( m_ClustersArray[i]->m_nodes.size() );
	//	
	//	for(j=0,nj=vertices.size(); j<nj; ++j)
	//	{				
	//		vertices[j] = *m_ClustersArray[i]->m_nodes[j];
	//	}

	//	HullDesc		hdsc(QF_TRIANGLES,vertices.size(),(btVector3*)&vertices[0]);
	//	HullResult		hres;
	//	HullLibrary		hlib;
	//	hdsc.mMaxVertices=vertices.size();
	//	hlib.CreateConvexHull(hdsc,hres);

	//	
	//	const btScalar	n = (btScalar)(hres.m_OutputVertices.size()>0 ? hres.m_OutputVertices.size() : 1 );
	//	// add
	//	btVector3 center = btVector3(0.0f,0.0f,0.0f);
	//	for( j=0,nj = hres.m_OutputVertices.size(); j<nj; ++j)
	//	{				
	//		center  += hres.m_OutputVertices[j];
	//	}		
	//	center /= n;

	//	//for( j=0,nj = hres.m_OutputVertices.size(); j<nj; ++j)
	//	//{				
	//	//	hres.m_OutputVertices[j]  -= center;
	//	//}		
	//	//for( j=0,nj = hres.m_OutputVertices.size(); j<nj; ++j)
	//	//{				
	//	//	hres.m_OutputVertices[j]  += center;
	//	//}

	//	for(j=0;j<(int)hres.mNumFaces;++j)
	//	{
	//		const int idx[]= {hres.m_Indices[j*3+0],hres.m_Indices[j*3+1],hres.m_Indices[j*3+2]};
	//		drawTriangle(hres.m_OutputVertices[idx[0]],
	//			hres.m_OutputVertices[idx[1]],
	//			hres.m_OutputVertices[idx[2]],
	//			color,1);
	//	}
	//	hlib.ReleaseResult(hres);
	//}

	/////* Frame		*/ 
	////btSoftBody::Cluster& c=*psb->m_clusters[i];
	////idraw->drawLine(c.m_com,c.m_framexform*btVector3(10,0,0),btVector3(1,0,0));
	////idraw->drawLine(c.m_com,c.m_framexform*btVector3(0,10,0),btVector3(0,1,0));
	////idraw->drawLine(c.m_com,c.m_framexform*btVector3(0,0,10),btVector3(0,0,1));
	//

	//}


}; // namespace SimStep

#endif