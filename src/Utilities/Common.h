#ifndef SimStep_Common_h__
#define SimStep_Common_h__

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX // required to stop windows.h messing up std::min

#include <string>
#include <map>

//#include <assert.h>
//#include <WinDef.h>

#if defined ( _TC_MALLOC )
	#pragma comment(lib, "tcmalloc_debug.lib") 
#endif


namespace SimStep
{

#   if defined( OGRE_STATIC_LIB )
        // Linux compilers don't have symbol import/export directives.
#       define _OgreExport
#       define _OgrePrivate
#   else
#       if defined( SIMSTEP_NONCLIENT_BUILD )
#           define _OgreExport __declspec( dllexport )
#       else
#           if defined( __MINGW32__ )
#               define _OgreExport
#           else
#               define _OgreExport __declspec( dllimport )
#           endif
#       endif
#       define _OgrePrivate
#   endif

#if defined( SIMSTEP_STATIC_LIB )
	#define _SimStepExport
#else
	# if defined( SIMSTEP_NONCLIENT_BUILD )
		#define _SimStepExport __declspec(dllexport)
	#else
		#define _SimStepExport __declspec(dllimport)
	#endif
#endif

	// used only for utils
	typedef float Real;

	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long ulong;
	typedef unsigned char uchar;

	typedef uint	Bool;
	#define ssTrue	uint(1)
	#define ssFalse	uint(0)


	//typedef Simd::Vector3	SimdVector3;
	//typedef Simd::Vector4	SimdVector4;
	//typedef Simd::Matrix3	SimdMatrix3;
	//typedef Simd::Matrix4	SimdMatrix4;
	//typedef Simd::Quat		SimdQuat;


	typedef std::string	String;
	//typedef std::map	Map;

	/// Name / value parameter pair (first = name, second = value)
	typedef std::map<String, String> DataValuePairList;

// Forward Declarations

//struct NodeStl;
//struct ElementStl;
//struct TriangleStl;
//struct Tetrahedron;
//struct CubeStl;
//class SoftBody;
//class RigidBody;


//struct declarations
//#include "Math.h"
//#include "Elements.h"
//#include "Triangle.h"
//#include "Tetrahedron.h"
//#include "SoftBody.h"


}; // namespace SimStep

namespace Ogre
{

#if defined (_SIMSTEP_OGRE_PLUGIN)
#define _SimStepOgreExport __declspec(dllexport)
#else
#define _SimStepOgreExport __declspec(dllimport)
#endif

};



#endif // SimStep_Common_h__