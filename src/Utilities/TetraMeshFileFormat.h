#ifndef SimStep_TetraMeshFileFormat_h__
#define SimStep_TetraMeshFileFormat_h__


namespace SimStep
{

	enum TetraMeshChunkID
	{
		TM_HEADER                = 0x0100,
		// char*          version           : Version number check
		TM_TETRAMESH                = 0x0300,
			// bool skeletallyAnimated   // important flag which affects h/w buffer policies
			// Optional M_GEOMETRY chunk
			TM_SUBTETRAMESH             = 0x0400, 
			// char* materialName
			// bool useSharedVertices
			// unsigned int indexCount
			// bool indexes32Bit
			// unsigned int* faceVertexIndices (indexCount)
			// OR
			// unsigned short* faceVertexIndices (indexCount)
			// M_GEOMETRY chunk (Optional: present only if useSharedVertices = false)
			TM_SUBMESH_OPERATION = 0x0405, // optional, trilist assumed if missing
			// unsigned short operationType


			TM_GEOMETRY          = 0x0500, // NB this chunk is embedded within M_MESH and M_SUBMESH
			// unsigned int vertexCount
			TM_GEOMETRY_NODE_DECLARATION = 0x0510,
			TM_GEOMETRY_NODE = 0x0511, // Repeating section
			// unsigned short source;  	// buffer bind source
			// unsigned short type;    	// VertexElementType
			// unsigned short semantic; // VertexElementSemantic
			// unsigned short offset;	// start offset in buffer in bytes
			// unsigned short index;	// index of the semantic (for colours and texture coords)
			TM_GEOMETRY_NODE_BUFFER = 0x0520, // Repeating section
			// unsigned short bindIndex;	// Index to bind this buffer to
			// unsigned short vertexSize;	// Per-vertex size, must agree with declaration at this index
			M_GEOMETRY_NODE_BUFFER_DATA = 0x0521,

			// Added By DrEvil
			// optional chunk that contains a table of submesh indexes and the names of
			// the sub-meshes.
			TM_SUBTETRAMESH_NAME_TABLE = 0x0A00,
			// Subchunks of the name table. Each chunk contains an index & string
			TM_SUBTETRAMESH_NAME_TABLE_ELEMENT = 0x0A10,
			// short index
			// char* name


			TM_GEOMETRY_NORMALS = 0x0515,    //(Optional)
			// float* pNormals (x, y, z order x numVertices)

			TM_BARYCENTRIC_COORDINATES = 0x0516
			// float* pBarycentricCoordinates (x, y, z order x numCollisionNodes)

	};

	
}; // namespace SimStep

#endif // SimStep_TetraMeshFileFormat_h__