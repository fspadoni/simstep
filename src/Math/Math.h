#ifndef PHYSICS_SOFTBODY_MATH
#define PHYSICS_SOFTBODY_MATH

#include <memory.h>

#include <iostream>

#include "SSE/vectormath_aos.h"
#include "Scalar/vectormath_aos.h"

#include "Utilities/Definitions.h"

#include "Memory/AlignedObject.h"

#include "Math/MathSimd.h"

#include "LinearMath/btVector3.h"

//
//// ODE vector decl
////#ifdef ODE_LIB
//#include "ode\ode.h"
////#endif

//
//#ifdef _DEBUG
//#define Allocate(size, alignment)	 _aligned_malloc_dbg(size, alignment,__FILE__, __LINE__)
//#define Free(ptr)					 _aligned_free_dbg(ptr);
//#else
//#define Allocate(size, alignment)	 _aligned_malloc(size, alignment)
//#define Free(ptr)					 _aligned_free(ptr);
//#endif

////extern  struct dVector3; // Ode vector
//class btVector3; // Bullet vector

#include <Memory/MemoryManager.h>

namespace SimStep{
	//namespace SoftBody {

	//forard decl:
	class Vector3;
	class Vector4;
	class Matrix3;
	class Matrix4;
	class Quat;

	typedef Simd::Vector3	SimdVector3;
	typedef Simd::Vector4	SimdVector4;
	typedef Simd::Matrix3	SimdMatrix3;
	typedef Simd::Matrix4	SimdMatrix4;
	typedef Simd::Quat		SimdQuat;



	align16_class  _SimStepExport Vector3 : public Scalar::Vector3 //, public AlignedObject16
	{
	public:
		////inline void* operator new(size_t sz, const char* file, int line, const char* func) {return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, file, line );}
		//	inline void* operator new(size_t sz)	{return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN,__FILE__, __LINE__);} 
		//	inline void* operator new(size_t sz, void* ptr){return ptr;} 
		//	//inline void* operator new[] ( size_t sz, const char* file, int line, const char* func ){return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, file, line );}
		//	inline void* operator new[] ( size_t sz )	{return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, __FILE__, __LINE__ );} 
		//	inline void operator delete( void* ptr ){	_aligned_free_dbg(ptr);} 
		//	inline void operator delete( void* ptr, void* ){_aligned_free_dbg(ptr);} 
		//	//inline void operator delete( void* ptr, const char* , int , const char*  )	{_aligned_free_dbg(ptr);} 
		//	inline void operator delete[] ( void* ptr ){	_aligned_free_dbg(ptr);} 
		//	//inline void operator delete[] ( void* ptr, const char* , int , const char*  ){	_aligned_free_dbg(ptr);	} 

		//DECLARE_NONVIRTUAL_CLASS_ALLOCATOR( MEMCATEGORY_GENERAL, Vector3 );

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Vector3, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		// Default constructor
		inline Vector3();

		inline Vector3(const Scalar::Vector3& vec );

		inline Vector3(const float& x, const float& y, const float& z);

		inline Vector3(const float& val);

		inline Vector3 & operator = ( const Scalar::Vector3 &vec );

		// conversion to SimdVector3
		inline operator SimdVector3 () const;
		inline operator SimdVector3* ();

		// bullet btVector
		explicit inline Vector3( const btVector3 &vec );
		inline Vector3 & operator = ( const btVector3 &vec );

		inline std::ostream& operator << ( std::ostream& strm );
		inline std::istream& operator >> ( std::istream& strm );

		static inline const Vector3 Zero();

		//// overload 
		//inline void* operator new(size_t size)
		//{
		//	return _aligned_malloc(size, Memory::SIMD_ALIGN); 
		//}

		//inline void operator delete(void *v)
		//{
		//	_aligned_free(v); 
		//}

		//inline void* operator new[](size_t size)	
		//{
		//	return _aligned_malloc(size, Memory::SIMD_ALIGN); 
		//}

		//inline void operator delete[](void *v)
		//{
		//	_aligned_free(v); 
		//}

		//inline void *  operator new(size_t, void *_Where)
		//{	// construct array with placement at _Where
		//	return (_Where);
		//}

		//inline void operator delete(void *, void *)
		//{}

	};



	align16_class  _SimStepExport Vector4 : public Scalar::Vector4 //,  public GeneralAllocatedObject
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Vector4, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		// Default constructor
		inline Vector4();

		inline Vector4(const Scalar::Vector4& vec );

		inline Vector4(const float& x, const float& y, const float& z, const float& w );

		inline Vector4(const float& val);

		// Construct a 4-D vector from a 3-D vector and a scalar
		// 
		inline Vector4( const Vector3 & xyz, float w );

		inline Vector4& operator = ( const Scalar::Vector4 &vec );

		inline std::ostream& operator << ( std::ostream& strm );
		inline std::istream& operator >> ( std::istream& strm );

		static inline const Vector4 Zero();

	};

	_SimStepExport inline std::ostream& operator << ( std::ostream& strm, const Vector4& v )
	{
		return strm << v << std::endl;
	}

	

	align16_class _SimStepExport Matrix3 : public Scalar::Matrix3 //, public AlignedObject16
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Matrix3, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		// Default constructor
		inline Matrix3();

		//inline Matrix3::Matrix3(const Vector3& x, const Vector3& y, const Vector3& z );

		inline Matrix3(
			const Scalar::Vector3& vec1, 
			const Scalar::Vector3& vec2,
			const Scalar::Vector3& vec3 );

		inline Matrix3( const Scalar::Matrix3 &mat );

		explicit inline Matrix3( const Scalar::Quat &unitQuat );

		// Assign a Vector3 to Node
		inline Matrix3 & Matrix3::operator = ( const Scalar::Matrix3 &mat );

	};




	align16_class _SimStepExport Matrix4 : public Scalar::Matrix4 //, public AlignedObject16
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Matrix4, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		// Default constructor
		inline Matrix4();

		// Copy constructor
		inline Matrix4( const Scalar::Matrix4 &mat );

		//inline Matrix4::Matrix4(const Vector3& x, const Vector3& y, const Vector3& z, const Vector3& w );

		inline Matrix4::Matrix4(
			const Scalar::Vector4& x, 
			const Scalar::Vector4& y, 
			const Scalar::Vector4& z, 
			const Scalar::Vector4& w );

		// Assign a Vector3 to Node
		inline Matrix4 & Matrix4::operator = ( const Scalar::Matrix4 &mat );

	};


	align16_class _SimStepExport Transform3 : public Scalar::Transform3 //, public AlignedObject16
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Transform3, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		// Default constructor
		inline Transform3();

		// Copy constructor
		inline Transform3( const Scalar::Transform3 &mat );

	};

	


	align16_class _SimStepExport Quat : public Scalar::Quat //, public AlignedObject16
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Quat, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		inline Quat( );

		inline Quat(const Scalar::Quat& q );

		inline Quat( float x, float y, float z, float w );

		inline Quat( const Scalar::Vector3 &xyz, float w );

		explicit inline Quat( const Scalar::Matrix3 & rotMat );

		inline Quat & operator =( const Scalar::Quat &quat );

		//explicit inline Quat( const SimdMatrix3 & rotMat );

		inline std::ostream& operator << ( std::ostream& strm );
		inline std::istream& operator >> ( std::istream& strm );

	};

	_SimStepExport inline std::ostream& operator << ( std::ostream& strm, const Quat& q )
	{
		return strm << q << std::endl;
	}



	template <typename T> static inline T Max(const T& a,const T& b)
	{ 
		return ( a > b ? a : b );
	}


	//} // namespace SoftBody
}; //namespace SimStep


#include "Math.inl"

#endif