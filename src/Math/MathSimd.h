#ifndef SimStep_MathSimd_h__
#define SimStep_MathSimd_h__

#include "SSE/vectormath_aos.h"
#include "Memory/MemoryManager.h"

#include "Utilities/Definitions.h"

#include "LinearMath/btVector3.h"

#include "Scalar/vectormath_aos.h"


namespace SimStep
{

	namespace Simd
	{
		class Vector3;
		class Vector4;
		class Matrix3;
		class Matrix4;
		class Quat;

		align16_class  Vector3 : public SSE::Vector3 //, public AlignedObject16
		{
		public:

			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Vector3, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

			// Default constructor
			inline Vector3();

			inline Vector3(const SSE::Vector3& vec );

			inline Vector3(const float& x, const float& y, const float& z);

			inline Vector3(const float& val);
			

			inline Vector3 & operator = ( const SSE::Vector3 &vec );

			// bullet btVector
			explicit inline Vector3(const btVector3 &vec );
			inline Vector3 & operator = ( const btVector3 &vec );

			inline std::ostream& operator << ( std::ostream& strm );
			inline std::istream& operator >> ( std::istream& strm );

			static inline const Vector3 Zero();

		};



		align16_class  Vector4 : public SSE::Vector4 //, public AlignedObject16
		{

		public:

			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Vector4, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


			// Default constructor
			inline Vector4();

			inline Vector4(const SSE::Vector4& vec );

			inline Vector4(const float& x, const float& y, const float& z, const float& w );

			inline Vector4(const float& val);

			// Construct a 4-D vector from a 3-D vector and a scalar
			// 
			inline Vector4( const Vector3 & xyz, float w );

			inline const Vector4& operator = ( const SSE::Vector4 &vec );

			inline std::ostream& operator << ( std::ostream& strm );
			inline std::istream& operator >> ( std::istream& strm );

			static inline const Vector4 Zero();
		};



		align16_class Matrix3 : public SSE::Matrix3 //, public AlignedObject16
		{

		public:

			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Matrix3, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


			// Default constructor
			inline Matrix3();

			//inline Matrix3::Matrix3(const Vector3& x, const Vector3& y, const Vector3& z );

			inline Matrix3(
				const Vector3& vec1, 
				const Vector3& vec2,
				const Vector3& vec3 );

			inline Matrix3( const Matrix3 &mat );

			explicit inline Matrix3( const Quat& unitQuat );

			// Assign a Vector3 to Node
			inline Matrix3 & Matrix3::operator = ( const SSE::Matrix3 &mat );

		};



		align16_class Matrix4 : public SSE::Matrix4 //, public AlignedObject16
		{

		public:

			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Matrix4, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

			// Default constructor
			inline Matrix4();

			// Copy constructor
			inline Matrix4( const SSE::Matrix4 &mat );

			//inline Matrix4::Matrix4(const Vector3& x, const Vector3& y, const Vector3& z, const Vector3& w );

			inline Matrix4::Matrix4(
				const SSE::Vector4& x, 
				const SSE::Vector4& y, 
				const SSE::Vector4& z, 
				const SSE::Vector4& w );

			// Assign a Vector3 to Node
			inline const Matrix4& operator = ( const SSE::Matrix4 &mat );

			inline const Matrix4& operator = ( const Scalar::Matrix4 &mat );

		};




		align16_class Quat : public SSE::Quat //, public AlignedObject16
		{

		public:

			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Quat, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


			inline Quat( );

			inline Quat(const Quat& q );

			inline Quat( float x, float y, float z, float w );

			inline Quat( const Vector3 &xyz, float w );

			explicit inline Quat( const Matrix3 & rotMat );

			inline Quat & operator =( const Quat &quat );

		};


	}; // namespace Simd

	
}; // namespace SimStep


#include "MathSimd.inl"

#endif // SimStep_MathSimd_h__