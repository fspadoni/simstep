#ifndef _VECTOR_12_
#define _VECTOR_12_


#include "../Utilities/Definitions.h"
#include "SSE\vectormath_aos.h"

#include "Memory/MemoryManager.h"


namespace SimStep {


	align16_struct Vector12 //: public AlignedObject16
	{
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Vector12, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

	union
	{

		struct  
		{
			SimdVector3 v[4];
		};

		struct  
		{
			SimdVector3 _1;
			SimdVector3 _2;
			SimdVector3 _3;
			SimdVector3 _4;
		};
	
		
	};
	

	//Vector12() {};
	//Vector12() : _1(0.0f), _2(0.0f), _3(0.0f), _4(0.0f) {}


};


}//  namespace SimStep 

#endif
