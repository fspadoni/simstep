
namespace SimStep
{


	inline Vector3::Vector3()
		: Scalar::Vector3() // do nothing
	{

	}

	inline Vector3::Vector3(const Scalar::Vector3& vec )
		: Scalar::Vector3() // do nothing
	{
		this->Scalar::Vector3::operator = ( vec );
	}

	inline Vector3::Vector3(const float& x, const float& y, const float& z)
		: Scalar::Vector3(x, y, z ) 
	{
		//mVec128 = _mm_setr_ps( x,  y, z, 0.0f);
		//this->SSE::Vector3::Vector3(x, y, z ); 
	}

	inline Vector3::Vector3(const float& val)
		: Scalar::Vector3( val )
	{
		//mVec128 = _mm_set_ps1( val );
		//this->SSE::Vector3::Vector3( val );
	}

	inline Vector3& Vector3::operator = ( const Scalar::Vector3 &vec )
	{
		this->Scalar::Vector3::operator = ( vec );
		return *this;
	}

	// conversion to SimdVector3
	inline Vector3::operator SimdVector3 () const
	{
		//return *reinterpret_cast<SimdVector3*>(this);
		return *reinterpret_cast<const SimdVector3*>( this );
	}

	// conversion to SimdVector3
	inline Vector3::operator SimdVector3* ()
	{
		return reinterpret_cast<SimdVector3*>(this);
	}

	inline Vector3::Vector3(const btVector3 &vec )
		: Scalar::Vector3( vec.getX(), vec.getY(), vec.getZ() ) 
	{

	}

	inline Vector3& Vector3::operator = ( const btVector3 &vec )
	{
		this->setX(vec.getX()).setY(vec.getY()).setZ(vec.getZ());
		//this->setY( vec.getY() );
		//this->setZ( vec.getZ() );
		return *this;
	}


	inline std::ostream& Vector3::operator << ( std::ostream& strm )
	{
		strm << " " << this->getX()
			<< ", " << this->getY() 
			<< ", " << this->getZ() << std::endl;

		return strm;
	}

	inline std::istream& Vector3::operator >> ( std::istream& strm )
	{
		float x,y,z;
		strm >>  x >>  y >> z;
		this->setX(x); this->setY(y); this->setZ(z);
		return strm;
	}

	inline const Vector3 Vector3::Zero()
	{
		return Vector3(0.0f);
	}



	inline Vector4::Vector4()
		: Scalar::Vector4() // do nothing
	{

	}

	inline Vector4::Vector4(const Scalar::Vector4& vec )
		: Scalar::Vector4() // do nothing
	{
		this->Scalar::Vector4::operator = ( vec );
	}

	inline Vector4::Vector4(const float& x, const float& y, const float& z, const float& w )
		: Scalar::Vector4(x, y, z, w ) 
	{
	}

	inline Vector4::Vector4(const float& val)
		: Scalar::Vector4( val )
	{
	}

	inline Vector4::Vector4( const Vector3 & xyz, float w )
		: Scalar::Vector4( xyz, w )
	{
	}

	inline Vector4& Vector4::operator = ( const Scalar::Vector4 &vec )
	{
		this->Scalar::Vector4::operator = ( vec );
		return *this;
	}


	inline std::ostream& Vector4::operator << ( std::ostream& strm )
	{
		strm << " " << this->getX()
			<< ", " << this->getY() 
			<< ", " << this->getY() 
			<< ", " << this->getW() << std::endl;
		return strm;
	}

	inline std::istream& Vector4::operator >> ( std::istream& strm )
	{
		float x,y,z,w;
		strm >>  x >>  y >> z >> w;
		this->setX(x); this->setY(y); this->setZ(z); this->setZ(w);
		return strm;
	}


	inline const Vector4 Vector4::Zero()
	{
		return Vector4(0.0f);
	}



	// ____________________________
	// matrix3


	inline Matrix3::Matrix3()
		: Scalar::Matrix3() // do nothing
	{

	}

	inline Matrix3::Matrix3(const Scalar::Vector3& vec1, 
		const Scalar::Vector3& vec2,
		const Scalar::Vector3& vec3 )
		: Scalar::Matrix3( vec1, vec2, vec3 )
	{
		//this->SSE::Matrix3::Matrix3( vec1, vec2, vec3 );
	}

	inline Matrix3::Matrix3( const Scalar::Matrix3 &mat )
		: Scalar::Matrix3()
	{
		this->Scalar::Matrix3::operator = ( mat );
	}

	inline Matrix3::Matrix3( const Scalar::Quat &unitQuat )
		: Scalar::Matrix3( unitQuat )
	{
		//this->SSE::Matrix3::Matrix3( unitQuat );
	}

	inline Matrix3 & Matrix3::operator = ( const Scalar::Matrix3 &mat )
	{
		this->Scalar::Matrix3::operator = ( mat );
		return *this;
	}




	// ____________ 
	// Matrix4

	inline Matrix4::Matrix4()
		: Scalar::Matrix4() // do nothing
	{

	}

	inline Matrix4::Matrix4(const Scalar::Vector4& x, 
		const Scalar::Vector4& y, 
		const Scalar::Vector4& z, 
		const Scalar::Vector4& w )
		: Scalar::Matrix4(x, y, z, w)
	{
		//this->SSE::Matrix4::Matrix4(x, y, z, w);
	}

	inline Matrix4::Matrix4( const Scalar::Matrix4 &mat )
		: Scalar::Matrix4()
	{
		this->Scalar::Matrix4::operator = ( mat );
	}

	inline Matrix4 & Matrix4::operator = ( const Scalar::Matrix4 &mat )
	{
		this->Scalar::Matrix4::operator = ( mat );
		return *this;
	}


	inline Transform3::Transform3()
		: Scalar::Transform3()
	{

	}

	// Copy constructor
	inline Transform3::Transform3( const Scalar::Transform3 &mat )
		: Scalar::Transform3(mat)
	{

	}



	inline Quat::Quat()
		: Scalar::Quat() // do nothing
	{

	}

	// Quaternion Copy Constructor 
	inline Quat::Quat(const Scalar::Quat& q )
		: Scalar::Quat()
	{
		this->Scalar::Quat::operator = ( q );
	}

	inline Quat::Quat( float x, float y, float z, float w )
	{
		this->Scalar::Quat::Quat( x, y, z, w );
	}

	inline Quat::Quat( const Scalar::Vector3 &xyz, float w )
	{
		this->Scalar::Quat::Quat( xyz, w );
	}

	inline Quat::Quat( const Scalar::Matrix3 & rotMat )
	{
		this->Scalar::Quat::Quat( rotMat );
	}

	inline Quat & Quat::operator = ( const Scalar::Quat &vec )
	{
		this->Scalar::Quat::operator = ( vec );
		return *this;
	}



	inline std::ostream& Quat::operator << ( std::ostream& strm )
	{
		strm << " " << this[0]
			<< ", " << this[1]
			<< ", " << this[2]
			<< ", " << this[3] << std::endl;
		return strm;
	}

	inline std::istream& Quat::operator >> ( std::istream& strm )
	{
		float x,y,z,w;
		strm >>  x >>  y >> z >> w;
		this->setX(x); this->setY(y); this->setZ(z); this->setW(w);
		return strm;
	}


}; // namespace SimStep