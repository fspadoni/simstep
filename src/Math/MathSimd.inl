namespace SimStep
{

	namespace Simd
	{
		

		inline Vector3::Vector3()
			: SSE::Vector3() // do nothing
		{

		}

		inline Vector3::Vector3(const SSE::Vector3& vec )
			: SSE::Vector3() // do nothing
		{
			this->SSE::Vector3::operator = ( vec );
		}

		inline Vector3::Vector3(const float& x, const float& y, const float& z)
			: SSE::Vector3(x, y, z ) 
		{
		}

		inline Vector3::Vector3(const float& val)
			: SSE::Vector3( val )
		{
		}

		inline Vector3& Vector3::operator = ( const SSE::Vector3 &vec )
		{
			this->SSE::Vector3::operator = ( vec );
			return *this;
		}


		inline Vector3::Vector3(const btVector3 &vec )
			: SSE::Vector3( vec.getX(), vec.getY(), vec.getZ() ) 
		{

		}

		inline Vector3& Vector3::operator = ( const btVector3 &vec )
		{
			this->setX(vec.getX()).setY(vec.getY()).setZ(vec.getZ());
			//this->setY( vec.getY() );
			//this->setZ( vec.getZ() );
			return *this;
		}

		inline std::ostream& Vector3::operator << ( std::ostream& strm )
		{
			strm << " " << this->getX()
				<< ", " << this->getY() 
				<< ", " << this->getZ() << std::endl;
			return strm;
		}

		inline std::istream& Vector3::operator >> ( std::istream& strm )
		{
			float x,y,z;
			strm >>  x >>  y >> z;
			this->setX(x); this->setY(y); this->setZ(z);
			return strm;
		}

		inline const Vector3 Vector3::Zero()
		{
			return Vector3(0.0f);
		}




		inline Vector4::Vector4()
			: SSE::Vector4() // do nothing
		{

		}

		inline Vector4::Vector4(const SSE::Vector4& vec )
			: SSE::Vector4() // do nothing
		{
			this->SSE::Vector4::operator = ( vec );
			//mVec128 = vec.get128();
		}

		inline Vector4::Vector4(const float& x, const float& y, const float& z, const float& w )
			: SSE::Vector4(x, y, z, w ) 
		{
		}

		inline Vector4::Vector4(const float& val)
			: SSE::Vector4( val )
		{
		}

		// Construct a 4-D vector from a 3-D vector and a scalar
		// 
		inline Vector4::Vector4( const Vector3 & xyz, float w )
			: SSE::Vector4( xyz, w )
		{
		}

		inline const Vector4& Vector4::operator = ( const SSE::Vector4 &vec )
		{
			this->SSE::Vector4::operator = ( vec );
			return *this;
		}


		inline std::ostream& Vector4::operator << ( std::ostream& strm )
		{
			strm << " " << this->getX()
				<< ", " << this->getY() 
				<< ", " << this->getY() 
				<< ", " << this->getW() << std::endl;
			return strm;
		}

		inline std::istream& Vector4::operator >> ( std::istream& strm )
		{
			float x,y,z,w;
			strm >>  x >>  y >> z >> w;
			this->setX(x); this->setY(y); this->setZ(z); this->setZ(w);
			return strm;
		}

		inline const Vector4 Vector4::Zero()
		{
			return Vector4(0.0f);
		}




		// ____________________________
		// matrix3

		const Matrix3 IdentityMatrix3 = Matrix3(
			Vector3(1.0f,0.0f,0.0f),
			Vector3(0.0f,1.0f,0.0f),
			Vector3(0.0f,0.0f,1.0f) );


		inline Matrix3::Matrix3()
			: SSE::Matrix3() // do nothing
		{

		}

		inline Matrix3::Matrix3(const Vector3& vec1, 
			const Vector3& vec2,
			const Vector3& vec3 )
			: SSE::Matrix3( vec1, vec2, vec3 )
		{

		}

		inline Matrix3::Matrix3( const Matrix3 &mat )
			: SSE::Matrix3()
		{
			this->SSE::Matrix3::operator = ( mat );
		}

		inline Matrix3::Matrix3( const Quat &unitQuat )
			: SSE::Matrix3( unitQuat )
		{
		}

		inline Matrix3 & Matrix3::operator = ( const SSE::Matrix3 &mat )
		{
			this->SSE::Matrix3::operator = ( mat );
			return *this;
		}



		// ____________ 
		// Matrix4

		inline Matrix4::Matrix4()
			: SSE::Matrix4() // do nothing
		{

		}

		inline Matrix4::Matrix4(const SSE::Vector4& x, 
			const SSE::Vector4& y, 
			const SSE::Vector4& z, 
			const SSE::Vector4& w )
			: SSE::Matrix4(x, y, z, w)
		{
		}

		inline Matrix4::Matrix4( const SSE::Matrix4 &mat )
			: SSE::Matrix4()
		{
			this->SSE::Matrix4::operator = ( mat );
		}

		inline const Matrix4 & Matrix4::operator = ( const SSE::Matrix4 &mat )
		{
			this->SSE::Matrix4::operator = ( mat );
			return *this;
		}

		inline const Matrix4 & Matrix4::operator = ( const Scalar::Matrix4 &mat )
		{
			this->SSE::Matrix4::operator = ( reinterpret_cast<const SSE::Matrix4&>(mat) );
			return *this;
		}



		// Quaternion Copy Constructor 

		inline Quat::Quat()
			: SSE::Quat() // do nothing
		{

		}

		inline Quat::Quat(const Quat& q )
			: SSE::Quat()
		{
			this->SSE::Quat::operator = ( q );
		}

		inline Quat::Quat( float x, float y, float z, float w )
		{
			this->SSE::Quat::Quat( x, y, z, w );
		}

		inline Quat::Quat( const Vector3 &xyz, float w )
		{
			this->SSE::Quat::Quat( xyz, w );
		}

		inline Quat::Quat( const Matrix3 & rotMat )
		{
			this->SSE::Quat::Quat( rotMat );
		}

		inline Quat & Quat::operator = ( const Quat &vec )
		{
			this->SSE::Quat::operator = ( vec );
			return *this;
		}



	}; // namespace Simd

	
}; // namespace SimStep