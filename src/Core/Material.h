#ifndef SimStep_Material_h__
#define SimStep_Material_h__

#include "Utilities/Common.h"
#include "Utilities/SharedPointer.h"

#include <Core/Resource.h>


namespace SimStep
{

	// Forward declaration
	class MaterialPtr;


	class _SimStepExport Material : public Resource
	{

		friend class MaterialManager;
		friend class XmlMaterialSerializer;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Material, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );


		Material( ResourceManager* creator, const String& name, ResourceHandle handle,
			const String& group, bool isManual = false, ManualResourceLoader* loader = 0);

		virtual ~Material();


		/** Assignment operator to allow easy copying between materials.
		*/
		Material& operator=( const Material& rhs );

		MaterialPtr clone(const String& newName, bool changeGroup = false, 
			const String& newGroup = StringUtil::BLANK) const;


		const double getDensity() const {return mDensity;}
		const double getYoungModulus() const {return mYoungModulus;}
		const double getPoissonRatio() const {return mPoissonRatio;}
		const double getFriction() const {return mFriction;}
		const double getToughness() const {return mToughness;}
		const double getYield() const {return mYield;}
		const double getMaxYield() const {return mMaxYield;}
		const double getCreep() const {return mCreep;}


	private:

		void prepareImpl(void);
		/** Destroys data cached by prepareImpl.
		*/
		void unprepareImpl(void);
		/// @copydoc Resource::loadImpl
		void loadImpl(void);
		/// @copydoc Resource::postLoadImpl
		void postLoadImpl(void);
		/// @copydoc Resource::unloadImpl
		void unloadImpl(void);
		/// @copydoc Resource::calculateSize
		size_t calculateSize(void) const;


	private:


		double mDensity; // [Kg/m^3]

		double mYoungModulus;	// [N/m^2]

		double mPoissonRatio; // []

		double mFriction;

		double mToughness;

		double mYield;

		double mMaxYield;

		double mCreep;


	};

	

	class _SimStepExport MaterialPtr : public SharedPtr<Material> 
	{
	public:

		MaterialPtr() : SharedPtr<Material>() {}

		explicit MaterialPtr(Material* rep) : SharedPtr<Material>(rep) {}

		MaterialPtr(const MaterialPtr& r) : SharedPtr<Material>(r) {} 

		MaterialPtr(const ResourcePtr& r);

		/// Operator used to convert a ResourcePtr to a MaterialPtr
		MaterialPtr& operator=(const ResourcePtr& r);

	};


}; // namespace SimStep

#endif // SimStep_Material_h__