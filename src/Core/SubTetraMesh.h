#ifndef SimStep_SubTetraMesh_h__
#define SimStep_SubTetraMesh_h__

#include <Utilities/Common.h>

#include "Core/PhysicsClasses.h"

#include "Utilities/Definitions.h"

//#include <Utilities/NodeElementData.h>

#include <Core/TetraMesh.h>

namespace SimStep
{


	class SubTetraMesh
	{
		friend class TetraMesh;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( SubTetraMesh, Memory::CACHE_ALIGN, MEMCATEGORY_GEOMETRY );


		SubTetraMesh();

		~SubTetraMesh();

		TetraMesh::Type mType;

		/// Indicates if this submesh shares vertex data with other meshes or whether it has it's own vertices.
		bool mUseSharedNodes;


		BufferSharedPtr<ushort> mTetrahedrons;

		BufferSharedPtr<Vector3> mUnShareNodes;


		TetraMesh* mParent;

		/// Sets the name of the Material which this SubMesh will use
		void setMaterialName(const String& matName);
		const String& getMaterialName(void) const;

		/** Returns true if a material has been assigned to the submesh, otherwise returns false.
		*/
		bool isMatInitialised(void) const;

	protected:

		/// Name of the material this SubMesh uses.
		String mMaterialName;

		/// Is there a material yet?
		bool mMatInitialised;

	};



	class SubMesh
	{

		friend class TetraMesh;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( SubMesh, Memory::CACHE_ALIGN, MEMCATEGORY_GEOMETRY );


		SubMesh();

		~SubMesh();

		/// Indicates if this submesh shares vertex data with other meshes or whether it has it's own vertices.
		bool mUseSharedVertices;


		Vertices	mVertices;
		BufferSharedPtr<Vector3> mVertexNormals;

		BufferSharedPtr<ushort> mFaces;


		TetraMesh* mParent;

	};

	
}; // namespace SimStep

#endif // SimStep_SubTetraMesh_h__