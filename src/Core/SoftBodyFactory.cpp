#include "Core/SoftBodyFactory.h"

#include "Core/SoftBody.h"

namespace SimStep
{

	SoftBody* SoftBodyFactory::createInstance(
		const SoftBodyParams& params,/* SceneManager* manager,*/ 
		const DataValuePairList* otherparams )
	{
		SoftBody* sb = createInstanceImpl( params, otherparams);
		//sb->_notifyCreator(this);
		//sb->_notifyManager(manager);
		return sb;
	}

	
}; // namespace SimStep