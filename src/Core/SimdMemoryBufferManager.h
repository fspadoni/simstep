#ifndef SimStep_SimdMemoryBufferManager_h__
#define SimStep_SimdMemoryBufferManager_h__

#include "Utilities/Singleton.h"
#include "Utilities/Common.h"

#include "Core/PhysicsClasses.h"

#include <Memory/MemoryManager.h>

//#include <Utilities/SimdMemoryBuffer.h>
//#include "Utilities/NodeMemoryBuffer.h"
//#include "Utilities/ElementMemoryBuffer.h"

#include <set>

namespace SimStep
{

	class SimdChunk;
	template<typename> class Buffer;

	//class _SimStepExport vBufferManager : public Singleton<vBufferManager>
	//{

	//	friend class vBufferSharedPtr;

	//public:

	//	SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( vBufferManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )

	//protected:

	//	typedef std::set<vBuffer*> BufferList;
	//	BufferList mBuffers;

	//	/// Map from original buffer to temporary buffers
	//	typedef std::multimap<vBuffer*, vBufferSharedPtr> FreeTemporaryBufferMap;
	//	/// Map of current available temp buffers 
	//	FreeTemporaryBufferMap mFreeTempBufferMap;


	//	///// Creates  a new buffer as a copy of the source, does not copy data
	//	//BufferSharedPtr makeBufferCopy(
	//	//	const BufferSharedPtr& source );

	//public:

	//	vBufferManager();
	//	virtual ~vBufferManager();
	//	/** Create a simd buffer.
	//	*/
	//	template<typename T>
	//	vBufferSharedPtr createBuffer( size_t numNodes = 1)
	//	{
	//		assert (numNodes > 0);
	//		vBuffer* buf = newSimStep vBuffer( sizeof(T), numNodes );
	//		//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
	//		mBuffers.insert(buf);
	//		return vBufferSharedPtr(buf);
	//	}


	//	///** Registers a node buffer as a copy of another.
	//	//@remarks
	//	//This is useful for registering an existing buffer as a temporary buffer
	//	//which can be allocated just like a copy.
	//	//*/
	//	//void registerBufferSourceAndCopy(
	//	//	const BufferSharedPtr& sourceBuffer,
	//	//	const BufferSharedPtr& copy);

	//	///** Allocates a copy of a given node buffer.
	//	//*/
	//	//BufferSharedPtr allocateBufferCopy(
	//	//	const BufferSharedPtr& sourceBuffer, 
	//	//	/*BufferLicenseType licenseType,*/
	//	//	/*HardwareBufferLicensee* licensee,*/
	//	//	bool copyData = false);

	//	///** Manually release a buffer copy for others to subsequently use
	//	//*/
	//	//void releaseBufferCopy(	const BufferSharedPtr& bufferCopy); 



	//	static vBufferManager& getSingleton(void);

	//	static vBufferManager* getSingletonPtr(void);
	//};


	class _SimStepExport BufferManager : public Singleton<BufferManager>
	{

		template<typename> friend class BufferSharedPtr;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( BufferManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )

	protected:

		typedef std::set<SimdChunk*> BufferList;
		BufferList mBuffers;

		/// Map from original buffer to temporary buffers
		//typedef std::multimap<class Buffer<typename>*, class BufferSharedPtr<typename>*> FreeTemporaryBufferMap;
		/// Map of current available temp buffers 
		//FreeTemporaryBufferMap mFreeTempBufferMap;


		///// Creates  a new buffer as a copy of the source, does not copy data
		//BufferSharedPtr makeBufferCopy(
		//	const BufferSharedPtr& source );

	public:

		BufferManager();
		virtual ~BufferManager();
		/** Create a simd buffer.
		*/
		template<typename T>
		BufferSharedPtr<T> createBuffer( size_t numNodes = 1)
		{
			assert (numNodes > 0);
			Buffer<T>* buf = newSimStep Buffer<T>( sizeof(T), numNodes/*, this */);
			//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
			mBuffers.insert(buf);
			return BufferSharedPtr<T>(buf);
		}


		template<typename T>
		void notifyBufferDestroyed(Buffer<T>* buf)
		{
			//OGRE_LOCK_MUTEX(mBuffersMutex)
			BufferList::iterator i = mBuffers.find(buf);
			if (i != mBuffers.end())
			{
				// release vertex buffer copies
				mBuffers.erase(i);
				//_forceReleaseBufferCopies(buf);
			}
		}


		///** Registers a node buffer as a copy of another.
		//@remarks
		//This is useful for registering an existing buffer as a temporary buffer
		//which can be allocated just like a copy.
		//*/
		//void registerBufferSourceAndCopy(
		//	const BufferSharedPtr& sourceBuffer,
		//	const BufferSharedPtr& copy);

		///** Allocates a copy of a given node buffer.
		//*/
		//BufferSharedPtr allocateBufferCopy(
		//	const BufferSharedPtr& sourceBuffer, 
		//	/*BufferLicenseType licenseType,*/
		//	/*HardwareBufferLicensee* licensee,*/
		//	bool copyData = false);

		///** Manually release a buffer copy for others to subsequently use
		//*/
		//void releaseBufferCopy(	const BufferSharedPtr<T>& bufferCopy)
		//{
		//	//mBuffers.find(bufferCopy);
		//	//mBuffers.erase(buf);
		//}



		static BufferManager& getSingleton(void);

		static BufferManager* getSingletonPtr(void);
	};


	///** Abstract singleton class for managing hardware buffers, a concrete instance
	//of this will be created by the RenderSystem. */
	//class _SimStepExport SimdMemoryBufferManager : public Singleton<SimdMemoryBufferManager>
	//	, public GeneralAllocatedObject
	//{
	//	friend class NodeBufferSharedPtr;
	//	friend class ElementMemoryBufferSharedPtr;

	//protected:
	//	/** WARNING: The following two members should place before all other members.
	//	Members destruct order is very important here, because destructing other
	//	members will cause notify back to this class, and then will access to this
	//	two members.
	//	*/
	//	typedef std::set<NodeBuffer*> NodeBufferList;
	//	typedef std::set<ElementMemoryBuffer*> ElementBufferList;
	//	NodeBufferList mNodeBuffers;
	//	ElementBufferList mElementBuffers;


	//	typedef std::set<NodeDeclaration*> NodeDeclarationList;
	//	typedef std::set<NodeBufferBinding*> NodeBufferBindingList;
	//	NodeDeclarationList mNodeDeclarations;
	//	NodeBufferBindingList mNodeBufferBindings;

	//	//// Mutexes
	//	//OGRE_MUTEX(mVertexBuffersMutex)
	//	//OGRE_MUTEX(mIndexBuffersMutex)
	//	//OGRE_MUTEX(mVertexDeclarationsMutex)
	//	//OGRE_MUTEX(mVertexBufferBindingsMutex)

	//	/// Internal method for destroys all vertex declarations
	//	virtual void destroyAllDeclarations(void);
	//	/// Internal method for destroys all vertex buffer bindings
	//	virtual void destroyAllBindings(void);


	//public:

	//	//enum BufferLicenseType
	//	//{
	//	//	/// Licensee will only release buffer when it says so
	//	//	BLT_MANUAL_RELEASE,
	//	//	/// Licensee can have license revoked
	//	//	BLT_AUTOMATIC_RELEASE
	//	//};

	//protected:

	//	///** Struct holding details of a license to use a temporary shared buffer. */
	//	//class _OgrePrivate VertexBufferLicense
	//	//{
	//	//public:
	//	//	HardwareVertexBuffer* originalBufferPtr;
	//	//	BufferLicenseType licenseType;
	//	//	size_t expiredDelay;
	//	//	HardwareVertexBufferSharedPtr buffer;
	//	//	HardwareBufferLicensee* licensee;
	//	//	VertexBufferLicense(
	//	//		HardwareVertexBuffer* orig,
	//	//		BufferLicenseType ltype, 
	//	//		size_t delay,
	//	//		HardwareVertexBufferSharedPtr buf, 
	//	//		HardwareBufferLicensee* lic) 
	//	//		: originalBufferPtr(orig)
	//	//		, licenseType(ltype)
	//	//		, expiredDelay(delay)
	//	//		, buffer(buf)
	//	//		, licensee(lic)
	//	//	{}

	//	//};

	//	/// Map from original buffer to temporary buffers
	//	typedef std::multimap<NodeBuffer*, NodeBufferSharedPtr> FreeTemporaryNodeBufferMap;
	//	/// Map of current available temp buffers 
	//	FreeTemporaryNodeBufferMap mFreeTempNodeBufferMap;
	//	///// Map from temporary buffer to details of a license
	//	//typedef std::map<NodeBuffer*, VertexBufferLicense> TemporaryVertexBufferLicenseMap;
	//	///// Map of currently licensed temporary buffers
	//	//TemporaryVertexBufferLicenseMap mTempVertexBufferLicenses;
	//	
	//	/// Number of frames elapsed since temporary buffers utilization was above half the available
	//	size_t mUnderUsedFrameCount;
	//	/// Number of frames to wait before free unused temporary buffers
	//	static const size_t UNDER_USED_FRAME_THRESHOLD;
	//	/// Frame delay for BLT_AUTOMATIC_RELEASE temporary buffers
	//	static const size_t EXPIRED_DELAY_FRAME_THRESHOLD;
	//	
	//	//// Mutexes
	//	//OGRE_MUTEX(mTempBuffersMutex)


	//	/// Creates  a new buffer as a copy of the source, does not copy data
	//	NodeBufferSharedPtr makeBufferCopy(
	//	const NodeBufferSharedPtr& source, 
	//	SimdMemoryBuffer::Usage usage, bool useShadowBuffer);

	//public:
	//	SimdMemoryBufferManager();
	//	virtual ~SimdMemoryBufferManager();
	//	/** Create a simd node buffer.
	//	*/
	//	virtual NodeBufferSharedPtr createNodeBuffer(size_t nodeSize, 
	//		size_t numNodes, SimdMemoryBuffer::Usage usage );
	//
	//	/** Create a simd element buffer.
	//	*/
	//	virtual ElementMemoryBufferSharedPtr createElementBuffer( 
	//		ElementMemoryBuffer::ElementType Elemtype, 
	//		ElementMemoryBuffer::IndexType idxType, 
	//		size_t numElements, 
	//		SimdMemoryBuffer::Usage usage,
	//		bool useSystemMemory = ssFalse);

	//	//** Creates a new vertex declaration. */
	//	virtual NodeDeclaration* createNodeDeclaration(void);
	//	//** Destroys a vertex declaration. */
	//	virtual void destroyNodeDeclaration(NodeDeclaration* decl);

	//	/** Creates a new NodeBufferBinding. */
	//	virtual NodeBufferBinding* createNodeBufferBinding(void);
	//	/** Destroys a NodeBufferBinding. */
	//	virtual void destroyNodeBufferBinding(NodeBufferBinding* binding);

	//	/** Registers a node buffer as a copy of another.
	//	@remarks
	//	This is useful for registering an existing buffer as a temporary buffer
	//	which can be allocated just like a copy.
	//	*/
	//	virtual void registerNodeBufferSourceAndCopy(
	//		const NodeBufferSharedPtr& sourceBuffer,
	//		const NodeBufferSharedPtr& copy);

	//	/** Allocates a copy of a given node buffer.
	//	*/
	//	virtual NodeBufferSharedPtr allocateNodeBufferCopy(
	//		const NodeBufferSharedPtr& sourceBuffer, 
	//		/*BufferLicenseType licenseType,*/
	//		/*HardwareBufferLicensee* licensee,*/
	//		bool copyData = false);

	//	/** Manually release a node buffer copy for others to subsequently use
	//	*/
	//	virtual void releaseNodeBufferCopy(
	//		const NodeBufferSharedPtr& bufferCopy); 

	//	/** Tell engine that the node buffer copy intent to reuse.
	//	*/
	//	virtual void touchNodeBufferCopy(
	//		const NodeBufferSharedPtr& bufferCopy);

	//	/** Free all unused node buffer copies.
	//	@remarks
	//	This method free all temporary vertex buffers that not in used.
	//	In normally, temporary vertex buffers are subsequently stored and can
	//	be made available for other purposes later without incurring the cost
	//	of construction / destruction. But in some cases you want to free them
	//	to save hardware memory (e.g. application was runs in a long time, you
	//	might free temporary buffers periodically to avoid memory overload).
	//	*/
	//	virtual void _freeUnusedBufferCopies(void);

	//	/** Internal method for releasing all temporary buffers which have been 
	//	allocated using BLT_AUTOMATIC_RELEASE; is called by OGRE.
	//	@param forceFreeUnused If true, free all unused temporary buffers.
	//	If false, auto detect and free all unused temporary buffers based on
	//	temporary buffers utilization.
	//	*/
	//	virtual void _releaseBufferCopies(bool forceFreeUnused = false);

	//	/** Internal method that forces the release of copies of a given buffer.
	//	@remarks
	//	This usually means that the buffer which the copies are based on has
	//	been changed in some fundamental way, and the owner of the original 
	//	wishes to make that known so that new copies will reflect the
	//	changes.
	//	@param sourceBuffer the source buffer as a shared pointer.  Any buffer copies created from the source buffer
	//	are deleted.
	//	*/
	//	virtual void _forceReleaseBufferCopies(
	//		const NodeBufferSharedPtr& sourceBuffer);

	//	/** Internal method that forces the release of copies of a given buffer.
	//	@remarks
	//	This usually means that the buffer which the copies are based on has
	//	been changed in some fundamental way, and the owner of the original 
	//	wishes to make that known so that new copies will reflect the
	//	changes.
	//	@param sourceBuffer the source buffer as a pointer.  Any buffer copies created from the source buffer
	//	are deleted.
	//	*/
	//	virtual void _forceReleaseBufferCopies(NodeBuffer* sourceBuffer);

	//	/// Notification that a hardware vertex buffer has been destroyed
	//	void _notifyNodeBufferDestroyed(NodeBuffer* buf);
	//	/// Notification that a hardware index buffer has been destroyed
	//	void _notifyElementBufferDestroyed(ElementMemoryBuffer* buf);

	//	/** Override standard Singleton retrieval.
	//	@remarks
	//	Why do we do this? Well, it's because the Singleton
	//	implementation is in a .h file, which means it gets compiled
	//	into anybody who includes it. This is needed for the
	//	Singleton template to work, but we actually only want it
	//	compiled into the implementation of the class based on the
	//	Singleton, not all of them. If we don't change this, we get
	//	link errors when trying to use the Singleton-based class from
	//	an outside dll.
	//	@par
	//	This method just delegates to the template version anyway,
	//	but the implementation stays in this single compilation unit,
	//	preventing link errors.
	//	*/
	//	static SimdMemoryBufferManager& getSingleton(void);
	//	/** Override standard Singleton retrieval.
	//	@remarks
	//	Why do we do this? Well, it's because the Singleton
	//	implementation is in a .h file, which means it gets compiled
	//	into anybody who includes it. This is needed for the
	//	Singleton template to work, but we actually only want it
	//	compiled into the implementation of the class based on the
	//	Singleton, not all of them. If we don't change this, we get
	//	link errors when trying to use the Singleton-based class from
	//	an outside dll.
	//	@par
	//	This method just delegates to the template version anyway,
	//	but the implementation stays in this single compilation unit,
	//	preventing link errors.
	//	*/
	//	static SimdMemoryBufferManager* getSingletonPtr(void);

	//};

	
}; // namespace SimStep

#endif // SimStep_SimdMemoryBufferManager_h__