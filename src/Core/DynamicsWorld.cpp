#include "Core/DynamicsWorld.h"


#include "Core/SceneManager.h"
#include "Utilities/StopWatch.h"

// dxObject
// dxBody
// dxWorld
#include "src/objects.h"

// inclusion for std::find
#include <algorithm>

#include "LinearMath/btPoolAllocator.h"

static const int sizeof_dxBody	= 384;
static const int max_nb_dxBody	= 2048;

static btPoolAllocator s_PoolAllocator( sizeof_dxBody, max_nb_dxBody );

static void*  OdeAlloc( size_t size)
{
	void* ptr;

	if ( size <= sizeof_dxBody )
	{
		ptr = s_PoolAllocator.allocate( size );
	}
	else
	{	// ode arena needs to allocate dOBSTACK_ARENA_SIZE bytes
		int n = ( size + sizeof_dxBody -1 )/ sizeof_dxBody;
		ptr = s_PoolAllocator.allocate( sizeof_dxBody );
		for (int i=1; i<n; i++)
		{
			s_PoolAllocator.allocate( sizeof_dxBody );
		}
	}

	return ptr;
}

static void * OdeRealloc(void *ptr, size_t oldsize, size_t newsize)
{
	s_PoolAllocator.freeMemory( ptr );
	return s_PoolAllocator.allocate( newsize );
}

static void OdeFree(void *ptr, size_t size)
{

	if ( size <= sizeof_dxBody )
	{
		s_PoolAllocator.freeMemory( ptr );
		//ptr = s_PoolAllocator.allocate( size );
	}
	else
	{	// ode arena needs to allocate dOBSTACK_ARENA_SIZE bytes
		int n = ( size + sizeof_dxBody -1 )/ sizeof_dxBody;
		s_PoolAllocator.freeMemory( ptr );
		//ptr = s_PoolAllocator.allocate( sizeof_dxBody );
		unsigned char* p = (unsigned char*)ptr + n*sizeof_dxBody;
		for (int i=n; i>0; i--)
		{
			s_PoolAllocator.freeMemory( p );
			p -= sizeof_dxBody;
			//s_PoolAllocator.allocate( sizeof_dxBody );
		}
	}

	return;	
}


namespace SimStep
{

	DynamicsWorldParams::DynamicsWorldParams() 
		: timeStep(0.01f)
		, gravity(0.0f, -9.8f, 0.0f)
		, solverNbIteration(20)
		, solverRelaxationValue(1.05)
		, solverERP( 0.2f )
		, solverCFM( 1e-5 )
		, linearDamping(0.01f)
		, angularDamping(0.01f)
		, linearDampingThreshold( 0.001f )
		, angularDampingThreshold(0.001f )
		, linearDampingThresholdDampingScale( 0.0f )
		, angularDampingThresholdDampingScale( 0.0f )
		, maxAngularSpeed(100.0f)
		, contactMaxCorrectingVel(0.1f)
		, parallelizeDynamics(false)
	{

	}
	

	DynamicsWorld::DynamicsWorld(const DynamicsWorldParams& dynamicsworldparams )
		: dWorld()
		, mTimeStep( dynamicsworldparams.timeStep )
		, mParallelizeDynamics( dynamicsworldparams.parallelizeDynamics)
	{
		
		dWorld::setGravity( dynamicsworldparams.gravity[0],
			dynamicsworldparams.gravity[1], dynamicsworldparams.gravity[2] );
		
		dWorld::setQuickStepNumIterations( dynamicsworldparams.solverNbIteration );
		dWorld::setQuickStepW( dynamicsworldparams.solverRelaxationValue );
		dWorld::setERP( dynamicsworldparams.solverERP );
		dWorld::setCFM( dynamicsworldparams.solverCFM );
		dWorld::setLinearDamping( dynamicsworldparams.linearDamping);
		dWorld::setAngularDamping( dynamicsworldparams.angularDamping );
		dWorld::setLinearDampingThreshold( dynamicsworldparams.linearDampingThreshold  );
		dWorld::setAngularDampingThreshold(dynamicsworldparams.angularDampingThreshold );
		//linearDampingThresholdDampingScale( 0.0f )
		//angularDampingThresholdDampingScale( 0.0f )

		dWorld::setMaxAngularSpeed( dynamicsworldparams.maxAngularSpeed );

		dWorldSetContactMaxCorrectingVel(dWorld::id(), dynamicsworldparams.contactMaxCorrectingVel);
		//dWorld::setContactSurfaceLayer(0.01);
		//dWorldSetContactSurfaceLayer(dWorld::id(),0.001);


		initializeOdeWorld();

	}
	

	DynamicsWorld::~DynamicsWorld()
	{

		const int Nb_RigidBody = RigidBodyArray.size();

		for (int i=0; i<Nb_RigidBody; i++ )
		{
			remove( RigidBodyArray[0] );
		}

		dJointGroupDestroy( mSoftBodySoftBodyContactGroup );
		dJointGroupDestroy( mRigidBodyRigidBodyContactGroup );
		dJointGroupDestroy ( mSoftBodyRigidBodyContactGroup );
	}


	void DynamicsWorld::initializeOdeWorld(void)
	{

		mSoftBodyRigidBodyContactGroup = dJointGroupCreate(0);
		mRigidBodyRigidBodyContactGroup = dJointGroupCreate(0);
		mSoftBodySoftBodyContactGroup = dJointGroupCreate(0);
		//dAllocFunction* fn = OdeAlloc;
		//dSetAllocHandler( fn );
		//dReallocFunction* realloc = OdeRealloc;
		//dSetReallocHandler( realloc );
		//dFreeFunction* free_fn = OdeFree;
		//dSetFreeHandler( free_fn );

		// alloco un joint in mSoftBodyRigidBodyContactGroup 
		// per inizializzare l'arena di ode
		dContact contact;
		dJointID cr = dJointCreateContactSoftRigid( dWorld::id(), mSoftBodyRigidBodyContactGroup, &contact );
		dJointGroupEmpty( mSoftBodyRigidBodyContactGroup ); // m_OdeContactGroup.empty();

		dJointID c = dJointCreateContact( dWorld::id(), mRigidBodyRigidBodyContactGroup, &contact );
		dJointGroupEmpty( mRigidBodyRigidBodyContactGroup ); // m_OdeContactGroup.empty();

		dJointID cs = dJointCreateContactSoftSoft( dWorld::id(), mSoftBodySoftBodyContactGroup, &contact );
		dJointGroupEmpty( mSoftBodySoftBodyContactGroup );
	}


	Vector3 DynamicsWorld::getGravity(void) const
	{
		float g[4];
		dWorld::getGravity(g);
		return Vector3(g[0], g[1], g[2] );
	}

	float DynamicsWorld::step( const float& timeStep, SceneManager* scene, void* spawnedTask )
	{
		tick_count starTicks = tick_count::now();

		// Ode Step
		dWorldQuickStep( dWorld::id(), timeStep );
		//dWorldQuickStepTbb( dWorld::id(), timeStep, rootTask );

		return  ( tick_count::now() - starTicks ).seconds();
	}



	void DynamicsWorld::add(RigidBody* rigidbody)
	{
		if (rigidbody != 0)
		{
			//rigidbody->m_internalType = btCollisionObject::CO_RIGID_BODY;
			//rigidbody->m_CollisionWorldInfo = &m_CollisionWorldInfo;
			RigidBodyArray.push_back(rigidbody);

			////if ( m_CollisionWorld!= NULL )
			//{
			//	btCollisionWorld::addCollisionObject( rigidbody,  btBroadphaseProxy::DefaultFilter,
			//		btBroadphaseProxy::AllFilter );
			//}

			if ( dWorld::id() != NULL)
			{

				// creo e aggiungo il rigidbody a Ode

				//rigidbody->dBody::dBody( dWorld::id() );

				rigidbody->dBody::setWorld( dWorld::id() );
				addObjectToList (rigidbody->dBody::id(),(dObject **) &dWorld::id()->firstbody);
				dWorld::id()->nb++;


				//dGeomSetBody(0, rigidbody->OdeRigidBody);

				//rigidbody->setUserPointer( (void*)rigidbody->dBody::id() );

				//dBodySetData(rigidbody->dBody::id(), (void*)rigidbody );


/*				dBodySetPosition(rigidbody->dBody::id(), 
					rigidbody->getWorldTransform().getOrigin().getX(),
					rigidbody->getWorldTransform().getOrigin().getY(),
					rigidbody->getWorldTransform().getOrigin().getZ() );*/		


				//dMatrix3 orientation;

				//memcpy( &orientation, &rigidbody->getWorldTransform().getBasis(), sizeof(btMatrix3x3) );


				//dBodySetRotation(rigidbody->dBody::id(), orientation );

				//dMass mass;

				//mass.mass = rigidbody->m_Mass;

				//mass.I[0] = rigidbody->m_InertiaTensor.getElem(0,0);
				//mass.I[1] = rigidbody->m_InertiaTensor.getElem(0,1);
				//mass.I[2] = rigidbody->m_InertiaTensor.getElem(0,2);

				//mass.I[4] = rigidbody->m_InertiaTensor.getElem(1,0);
				//mass.I[5] = rigidbody->m_InertiaTensor.getElem(1,1);
				//mass.I[6] = rigidbody->m_InertiaTensor.getElem(1,2);

				//mass.I[8] = rigidbody->m_InertiaTensor.getElem(2,0);
				//mass.I[9] = rigidbody->m_InertiaTensor.getElem(2,1);
				//mass.I[10] = rigidbody->m_InertiaTensor.getElem(2,2);

				//dBodySetMass( rigidbody->dBody::id(), &mass );
			}
		}	

	}


	Bool DynamicsWorld::remove( RigidBody* body )
	{
		Bool ret = ssFalse;

		if ( body != 0)
		{
		//	if ( dWorld::id() != NULL)
		//	{
		//		// distrugge il rigidbody a Ode
		//		//dWorld::removeObjectFromList()
		//		dBodyDestroy( body->dBody::id() );
		//		// to fix it
		//		//set dBody::id to zero calling empty constructor
		//		// find another way to do this
		//		body->dBody::dBody();
		//	}

			dBodyRemove( body->dBody::id() );

			RigidBodyIterator = std::find( RigidBodyArray.begin(), RigidBodyArray.end(), body );

			if ( RigidBodyIterator == RigidBodyArray.end() )
			{
				ret = ssFalse;
			}
			else
			{

				RigidBodyArray.erase( RigidBodyIterator );

				ret = ssTrue;
			}

			////if ( m_CollisionWorld )
			//{
			//	btCollisionWorld::removeCollisionObject( body );
			//}

		}

		return ret;
	}


}; // namespace SimStep