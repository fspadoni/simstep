#ifndef SimStep_PhysicsClasses_h__
#define SimStep_PhysicsClasses_h__



namespace SimStep
{

	// Pre-declare classes
	// Allows use of pointers in header files without including individual .h
	// so decreases dependencies between files

	class ArchiveFactory;
	class ArchiveManager;
	class BufferManager;
	class Bounds3;
	class CollisionObject;
	class CollisionObjectParams;
	class ColourValue; // da Ogre
	//class CollisionShape;
	class Constraint;
	class ConstraintParams;
	class Core;

	struct Dbvt;

	class DynLib;
	class DynLibManager;

	class ElementData;
	class ElementMemoryBuffer;
	class ElementMemoryBufferSharedPtr;
	class Graph;
	class LogManager;
	class MaterialManager;
	class Matrix3;
	class Matrix4;

	class Memory;
	
	class NodeData;
	class NodeMemoryBuffer;
	class NodeMemoryBufferSharedPtr;
	
	class PhysicsWorld;
	class PhysicsWorldFactory;
	struct PhysicsWorldInfo;
	class PhysicsWorldParam;
	class PhysicsWorldRegister;
	// da sistemare: mettere dentro PhysicsWorld
	extern enum PhysicsWorldType;
	class Plugin;
	class Profiler;
	class Quat;
	class ResourceGroupManager;
	class ResourceManager;
	class RigidBody;
	//class RigidBodyBase;
	class RigidBodyParams;
	
	class SceneManager;
	class SceneManagerFactory;
	class SceneManagerRegister;
	class Shape;
	class ShapeManager;
	class SimdMemoryBufferManager;
	class SoftBody;
	class CollisionSoftBody;
	class SoftBodyFactory;
	struct SoftBodyParams;
	class StatisticsManager;
	struct StepEvent;
	class StopWatch;
	class StringConverter;
	class StringInterface;
	class SubMesh;
	class SubTetraMesh;
	class TaskManager;
	
	class TetraMesh;
	class TetraMeshManager;
	class TetraMeshPtr;
	struct TriangleElement;
	class vBufferManager;
	class Vector3;
	class Vector4;
	class Vertices;
	class World;
	class WorldManager;
	class WorldParam;


	enum NsPhysicsSceneType;


} // namespace SimStep


namespace Ogre
{

	class PhysicsSceneManager;
	class SceneManagerRegister;
}; // namespace Ogre


namespace SimStep
{
//
//	template <typename T, typename A = STLAllocator<T, GeneralAllocPolicy> > 
//	struct deque 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::deque<T, A> type;    
//#else
//		typedef typename std::deque<T> type;    
//#endif
//	}; 
//
//	template <typename T, typename A = STLAllocator<T, GeneralAllocPolicy> > 
//	struct vector 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::vector<T, A> type;    
//#else
//		typedef typename std::vector<T> type;    
//#endif
//	}; 
//
//	template <typename T, typename A = STLAllocator<T, GeneralAllocPolicy> > 
//	struct list 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::list<T, A> type;    
//#else
//		typedef typename std::list<T> type;    
//#endif
//	}; 
//
//	template <typename T, typename P = std::less<T>, typename A = STLAllocator<T, GeneralAllocPolicy> > 
//	struct set 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::set<T, P, A> type;    
//#else
//		typedef typename std::set<T, P> type;    
//#endif
//	}; 
//
//	template <typename K, typename V, typename P = std::less<K>, typename A = STLAllocator<std::pair<const K, V>, GeneralAllocPolicy> > 
//	struct map 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::map<K, V, P, A> type; 
//#else
//		typedef typename std::map<K, V, P> type; 
//#endif
//	}; 
//
//	template <typename K, typename V, typename P = std::less<K>, typename A = STLAllocator<std::pair<const K, V>, GeneralAllocPolicy> > 
//	struct multimap 
//	{ 
//#if OGRE_CONTAINERS_USE_CUSTOM_MEMORY_ALLOCATOR
//		typedef typename std::multimap<K, V, P, A> type; 
//#else
//		typedef typename std::multimap<K, V, P> type; 
//#endif
//	}; 

	
}; // namespace SimStep
#endif // SimStep_PhysicsClasses_h__#ifndef PHYSICS_CLASSES_H