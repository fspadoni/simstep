#ifndef SimStep_Core_h__
#define SimStep_Core_h__


#include "Utilities/Singleton.h"
#include "Memory/MemoryManager.h"
#include "Utilities/Common.h"

#include "Core/PhysicsClasses.h"
#include "Core/GlobalParam.h"
#include <Core/ResourceGroupManager.h>

#include "Core/StepListener.h"

#include "Utilities/String.h"

#include "World/PhysicsWorldParams.h"

// STL
#include <map>
#include <set>

//// Ogre
//#include "Ogre/PhysicsSceneManager.h"


//#include "../../../../OgreMain/include/OgreSingleton.h"
namespace Ogre
{
	class PhysicsSceneManagerFactory;
	
}

namespace SimStep 
{



	class _SimStepExport Core : public Singleton<Core> //, public AlignedObject16
	{

	public:

		SIMSTEP_DECLARE_SYSTEM_ALLOCATOR();
		
		typedef std::vector<PhysicsWorld*> PhysicsWorldList;
		

	private:


		PhysicsWorldList mActiveWorlds;
		PhysicsWorldList::iterator mActiveWorldsIter;

		//std::vector<SceneManager*> mScene;
		String m_Version;
		String m_ConfigFileName;


		// Singletons
		Memory* m_Memory;

		LogManager* m_LogManager;
		TaskManager* m_TaskManager;

		//ControllerManager* mControllerManager;
		DynLibManager* mDynLibManager;
		//SceneManager* mCurrentSceneManager;

		SimStep::SceneManagerRegister* mSceneManagerRegister;
		Ogre::SceneManagerRegister* mOgreSceneManagerRegister;

		SceneManager* mCurrentSceneManager;

		ArchiveManager* mArchiveManager;
		//ArchiveFactory *mZipArchiveFactory;
		ArchiveFactory* mFileSystemArchiveFactory;
		
		ResourceGroupManager* mResourceGroupManager;

		TetraMeshManager* m_MeshManager;
		MaterialManager* m_MaterialManager;
		ShapeManager* mShapeManager;
		
		//SimdMemoryBufferManager* mSimdBufferManager;
		BufferManager* mBufferManager;
		//vBufferManager* mvBufferManager;

		//TetraMeshManager* m_TetraMeshManager;
		PhysicsWorldRegister* mPhysicsWorldRegister;


		Ogre::PhysicsSceneManagerFactory* m_SceneManagerFactory;



		StopWatch* m_StopWatch;
		//RenderWindow* m_AutoWindow;
		Profiler* m_Profiler;


		StatisticsManager* mStatsManager;
		Bool mUpdateCPUStats;

	//public:
		typedef std::vector<DynLib*> PluginLibList;
		typedef std::vector<Plugin*> PluginInstanceList;
	private:
		/// List of plugin DLLs loaded
		PluginLibList mPluginLibs;
		/// List of Plugin instances registered
		PluginInstanceList mPlugins;


		typedef std::map<String, SoftBodyFactory*> SoftBodyFactoryMap;
		SoftBodyFactoryMap mSoftBodyFactoryMap;
		
		//// stock soft body factories
		//SoftBodyFactory* mSoftBodyFactory;

		
		bool mIsInitialised;
		bool m_PhysicsProfiler;


		/** Set of registered step listeners */
		std::set<StepListener*> mStepListeners;


		enum StepEventTimeType {
			SETT_STEP_ANY = 0, 
			SETT_STEP_STARTED = 1, 
			SETT_STEP_SIMULATED = 2,
			SETT_STEP_ENDED = 3, 
			SETT_STEP_COUNT = 4
		};

		/// Contains the times of recently fired events
		unsigned long mStepEventTimes[SETT_STEP_COUNT];

		inline float calculateStepEventTime(float now, StepEventTimeType type);


		/** Method reads a plugins configuration file and instantiates all
		plugins.
		@param
		pluginsfile The file that contains plugins information.
		Defaults to "plugins.cfg".
		*/
		void loadPlugins( const String& pluginsfile = "plugins.cfg" );
		/** Initialise all loaded plugins - allows plugins to perform actions
		once the renderer is initialised.
		*/
		void initialisePlugins();
		/** Shuts down all loaded plugins - allows things to be tidied up whilst
		all plugins are still loaded.
		*/
		void releasePlugins();

		/** Unloads all loaded plugins.
		*/
		void unloadPlugins();

	public:

		/** Constructor
		@param configFileName The file that contains the configuration to be loaded.
		Defaults to "SimStep.cfg", may be left blank to load nothing.
		@param logFileName The logfile to create, defaults to SimStep.log, may be 
		left blank if you've already set up LogManager & Log yourself
		*/
		Core( 
			const String& configFileName = "SimStep.cfg", 
			const String& logFileName = "SimStep.log",
			const String& pluginFileName = "SimStepPlugins.cfg" );
		
		~Core();

		/** Saves the details of the current configuration
		@remarks
		Stores details of the current configuration so it may be
		restored later on.
		*/
		void saveConfig(void);

		/** Checks for saved video/sound/etc settings
		@remarks
		This method checks to see if there is a valid saved configuration
		from a previous run. If there is, the state of the system will
		be restored to that configuration.
		@returns
		If a valid configuration was found, <b>true</b> is returned.
		@par
		If there is no saved configuration, or if the system failed
		with the last config settings, <b>false</b> is returned.
		*/
		bool restoreConfig(void);

		/** Displays a dialog asking the user to choose system settings.
		@remarks
		This method displays the default dialog allowing the user to
		choose the rendering system, video mode etc. If there is are
		any settings saved already, they will be restored automatically
		before displaying the dialogue. When the user accepts a group of
		settings, this will automatically call Core::setRenderSystem,
		RenderSystem::setConfigOption and Core::saveConfig with the
		user's choices. This is the easiest way to get the system
		configured.
		@returns
		If the user clicked 'Ok', <b>true</b> is returned.
		@par
		If they clicked 'Cancel' (in which case the app should
		strongly consider terminating), <b>false</b> is returned.
		*/
		bool showConfigDialog(void);



		/** use this for initial configuration. */
		void initialise(void);



		/** Returns whether the system is initialised or not. */
		bool isInitialised(void) const { return mIsInitialised; }



		/** Shuts down the system manually.
		@remarks
		This is normally done by SimStep automatically so don't think
		you have to call this yourself. However this is here for
		convenience, especially for dealing with unexpected errors or
		for systems which need to shut down physics on demand.
		*/
		void release(void);


		/** Adds a location to the list of searchable locations for a
		Resource type.
		@remarks
		Resource files ( models etc) need to be loaded from
		specific locations. By calling this method, you add another 
		search location to the list. Locations added first are preferred
		over locations added later.
		@par
		Locations can be folders, compressed archives, even perhaps
		remote locations. Facilities for loading from different
		locations are provided by plugins which provide
		implementations of the Archive class.
		All the application user has to do is specify a 'loctype'
		string in order to indicate the type of location, which
		should map onto one of the provided plugins. SimStep comes
		configured with the 'FileSystem' (folders) and 'Zip' (archive
		compressed with the pkzip / WinZip etc utilities) types.
		@par
		You can also supply the name of a resource group which should
		have this location applied to it. The 
		ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME group is the
		default, and one resource group which will always exist. You
		should consider defining resource groups for your more specific
		resources (e.g. per level) so that you can control loading /
		unloading better.
		@param
		name The name of the location, e.g. './data' or
		'/compressed/gamedata.zip'
		@param
		locType A string identifying the location type, e.g.
		'FileSystem' (for folders), 'Zip' etc. Must map to a
		registered plugin which deals with this type (FileSystem and
		Zip should always be available)
		@param
		groupName Type of name of the resource group which this location
		should apply to; defaults to the General group which applies to
		all non-specific resources.
		@param
		recursive If the resource location has a concept of recursive
		directory traversal, enabling this option will mean you can load
		resources in subdirectories using only their unqualified name.
		The default is to disable this so that resources in subdirectories
		with the same name are still unique.
		@see
		Archive
		*/
		void addResourceLocation(const String& name, const String& locType, 
			const String& groupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
			bool recursive = false);

		/** Removes a resource location from the list.
		@see addResourceLocation
		@param name The name of the resource location as specified in addResourceLocation
		@param groupName The name of the resource group to which this location 
		was assigned.
		*/
		void removeResourceLocation(const String& name, 
			const String& groupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME
			);


		/** Helper method to assist you in creating writeable file streams.
		@remarks
		This is a high-level utility method which you can use to find a place to 
		save a file more easily. If the filename you specify is either an
		absolute or relative filename (ie it includes path separators), then
		the file will be created in the normal filesystem using that specification.
		If it doesn't, then the method will look for a writeable resource location
		via ResourceGroupManager::createResource using the other params provided.
		@param filename The name of the file to create. If it includes path separators, 
		the filesystem will be accessed direct. If no path separators are
		present the resource system is used, falling back on the raw filesystem after.
		@param groupName The name of the group in which to create the file, if the 
		resource system is used
		@param overwrite If true, an existing file will be overwritten, if false
		an error will occur if the file already exists
		@param locationPattern If the resource group contains multiple locations, 
		then usually the file will be created in the first writable location. If you 
		want to be more specific, you can include a location pattern here and 
		only locations which match that pattern (as determined by StringUtil::match)
		will be considered candidates for creation.
		*/
		DataStreamPtr createFileStream(const String& filename, const String& groupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
			bool overwrite = false, const String& locationPattern = StringUtil::BLANK);


		/** Helper method to assist you in accessing readable file streams.
		@remarks
		This is a high-level utility method which you can use to find a place to 
		open a file more easily. It checks the resource system first, and if
		that fails falls back on accessing the file system directly.
		@param filename The name of the file to open. 
		@param groupName The name of the group in which to create the file, if the 
		resource system is used
		@param locationPattern If the resource group contains multiple locations, 
		then usually the file will be created in the first writable location. If you 
		want to be more specific, you can include a location pattern here and 
		only locations which match that pattern (as determined by StringUtil::match)
		will be considered candidates for creation.
		*/		
		DataStreamPtr openFileStream(const String& filename, const String& groupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
			const String& locationPattern = StringUtil::BLANK);


		//	/** Manually load a Plugin contained in a DLL / DSO.
		//	@remarks
		//	Plugins embedded in DLLs can be loaded at startup using the plugin 
		//	configuration file specified when you create Core (default: plugins.cfg).
		//	This method allows you to load plugin DLLs directly in code.
		//	The DLL in question is expected to implement a dllStartPlugin 
		//	method which instantiates a Plugin subclass and calls Core::installPlugin.
		//	It should also implement dllStopPlugin (see Core::unloadPlugin)
		//	@param pluginName Name of the plugin library to load
		//	*/
		void loadPlugin(const String& pluginName);

		///** Manually unloads a Plugin contained in a DLL / DSO.
		//@remarks
		//Plugin DLLs are unloaded at release automatically. This method 
		//allows you to unload plugins in code, but make sure their 
		//dependencies are decoupled first. This method will call the 
		//dllStopPlugin method defined in the DLL, which in turn should call
		//Core::uninstallPlugin.
		//@param pluginName Name of the plugin library to unload
		//*/
		void unloadPlugin(const String& pluginName);

		///** Install a new plugin.
		//@remarks
		//This installs a new extension to physics. The plugin itself may be loaded
		//from a DLL / DSO, or it might be statically linked into your own 
		//application. Either way, something has to call this method to get
		//it registered and functioning. You should only call this method directly
		//if your plugin is not in a DLL that could otherwise be loaded with 
		//loadPlugin, since the DLL function dllStartPlugin should call this
		//method when the DLL is loaded. 
		//*/
		void installPlugin(Plugin* plugin);

		///** Uninstall an existing plugin.
		//@remarks
		//This uninstalls an extension to SimStep. Plugins are automatically 
		//uninstalled at release but this lets you remove them early. 
		//If the plugin was loaded from a DLL / DSO you should call unloadPlugin
		//which should result in this method getting called anyway (if the DLL
		//is well behaved).
		//*/
		void uninstallPlugin(Plugin* plugin);

		///** Gets a read-only list of the currently installed plugins. */
		const PluginInstanceList& getInstalledPlugins() const { return mPlugins; }



		/** Gets a pointer to the central StopWatch used for all SimStep timings */
		StopWatch* getStopWatch(void);


		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static Core& getSingleton(void);

		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static Core* getSingletonPtr(void);

		// SimStep API FUNCTION ( Ogre Style )

		void addSceneManagerFactory( SceneManagerFactory* fact ) const;

		void removeSceneManagerFactory( SceneManagerFactory* fact);

		// SceneManager 
		SceneManager* createSceneManager( const String& typeName, 
			const String& instanceName = StringUtil::BLANK);


		SceneManager* createSceneManager( const NsPhysicsSceneType& typeMask, 
			const String& instanceName = StringUtil::BLANK);

		/** Destroy an instance of a WorldManager. */
		void destroySceneManager( SceneManager* sm);

		/** Get an existing PhysicsSceneManager instance that has already been created,
		identified by the instance name.
		@param instanceName The name of the instance to retrieve.
		*/
		SceneManager* getSceneManager(const String& instanceName) const;


		typedef std::map<String, SceneManager*>::const_iterator SceneManagerIterator;
		SceneManagerIterator getSceneManagerIterator(void);



		// OGRE SUPPORT API FUNCTION __________________________________________

		//void addSceneManagerFactory( Ogre::PhysicsSceneManagerFactory* fact );

		//void removeSceneManagerFactory(Ogre::PhysicsSceneManagerFactory* fact);

		////-----------------------------------------------------------------------
		//const SceneManagerMetaData* getSceneManagerMetaData(const String& typeName) const;
		//SceneManagerEnumerator::MetaDataIterator getSceneManagerMetaDataIterator(void) const;
		////-----------------------------------------------------------------------

		//// SceneManager 
		//Ogre::PhysicsSceneManager* createOgreSceneManager( const String& typeName, 
		//	const String& instanceName = StringUtil::BLANK);


		//Ogre::PhysicsSceneManager* createOgreSceneManager( ushort typeMask, 
		//	const String& instanceName = StringUtil::BLANK);

		///** Destroy an instance of a WorldManager. */
		//void destroySceneManager(Ogre::PhysicsSceneManager* sm);

		/** Get an existing PhysicsSceneManager instance that has already been created,
		identified by the instance name.
		@param instanceName The name of the instance to retrieve.
		*/
		//Ogre::PhysicsSceneManager* getOgreSceneManager(const String& instanceName) const;


		//typedef std::map<String, Ogre::PhysicsSceneManager*>::const_iterator OgreSceneManagerIterator;
		//OgreSceneManagerIterator getOgreSceneManagerIterator(void);

		// OGRE __________________________________________



		Bool SimulationStep( float dtime, 
			int simType = SimulationType::simType_invalid,
			void* spawnedTask = NULL );


		Bool _fireStepStarted();
		
		Bool _fireStepEnded();

		Bool _simulate(const uint simType,	void* spawnedTask = NULL);


		Bool _fireStepStarted(StepEvent& evt);

		Bool _fireStepEnded(StepEvent& evt);




		void addPhysicsWorldFactory( PhysicsWorldFactory* fact ) const;

		void removePhysicsWorldFactory( PhysicsWorldFactory* fact);

		/** Create a PhysicsWorld instance of a given type.
		@remarks
		You can use this method to create a WorldManager instance of a 
		given specific type.
		@note
		This method throws an exception if the named type is not found.
		@param typeName String identifying a unique PhysicsWorld type
		@param instanceName Optional name to given the new instance that is
		created. If you leave this blank, an auto name will be assigned.
		*/
		PhysicsWorld* createPhysicsWorld( const String& typeName, 
			const PhysicsWorldParam& worldparam );

		PhysicsWorld* createPhysicsWorld( const PhysicsWorldType& typeMask, 
			const PhysicsWorldParam& worldparam );

		/** Destroy an instance of a PhysicsWorld. */
		void destroyPhysicsWorld(PhysicsWorld* sm);

		/** Get an existing PhysicsWorld instance that has already been created,
		identified by the instance name.
		@param instanceName The name of the instance to retrieve.
		*/
		PhysicsWorld* getPhysicsWorld(const String& instanceName) const;


		typedef std::map<String, PhysicsWorld*>::const_iterator PhysicsWorldMapIterator;
		PhysicsWorldMapIterator getPhysicsWorldIterator(void);

		PhysicsWorld* getActiveWorld(void) const;

		PhysicsWorldList& getActivePhysicsWorlds(void) const;


		/** Register a new SoftBodyFactory which will create new SoftBody
		instances of a particular type, as identified by the getType() method.
		@remarks
		Plugin creators can create subclasses of SoftBodyFactory which 
		construct custom subclasses of SoftBody for insertion in the 
		scene. This is the primary way that plugins can make custom objects
		available.
		@param fact Pointer to the factory instance
		@param overrideExisting Set this to true to override any existing 
		factories which are registered for the same type. You should only
		change this if you are very sure you know what you're doing. 
		*/
		void addSoftBodyFactory(SoftBodyFactory* fact, 
			bool overrideExisting = false );
		/** Removes a previously registered SoftBodyFactory.
		@remarks
		All instances of objects created by this factory will be destroyed
		before removing the factory (by calling back the factories 
		'destroyInstance' method). The plugin writer is responsible for actually
		destroying the factory.
		*/
		void removeSoftBodyFactory(SoftBodyFactory* fact);
		/// Checks whether a factory is registered for a given SoftBody type
		bool hasSoftBodyFactory(const String& typeName) const;
		/// Get a SoftBodyFactory for the given type
		SoftBodyFactory* getSoftBodyFactory(const String& typeName);



		typedef SoftBodyFactoryMap::const_iterator SoftBodyFactoryMapIterator;
		///** Return an iterator over all the SoftBodyFactory instances currently
		//registered.
		//*/
		SoftBodyFactoryMapIterator getSoftBodyFactoryIterator(void) const;



		bool setParameter( GlobalParam paramEnum, float paramValue) {return false;}
		float getParameter( GlobalParam paramEnum ) const {return 0.0f;}

		//PhysicsWorld* createWorld(const PhysicsWorldParam& worldparam);
		//void destroyWorld(PhysicsWorld& world);
		//PhysicsWorld* getWorldIterator() const;
		////World* getWorld(const String& world_name) const;



		/////** Adds a new world subsystem to the list of available worlds.
		////*/
		////void addWorld(PhysicsWorld* newWorld);

		///** Retrieve a list of the available worlds.
		//*/
		//PhysicsWorldList* getActiveWorlds(void);

		///** Retrieve a pointer to the world by the given name
		//@param
		//name Name of the render system intend to retrieve.
		//@returns
		//A pointer to the world, <b>NULL</b> if no found.
		//*/
		//PhysicsWorld* getWorldByName(const String& name);

		//** Adds the world to be used.
		//*/
		void addActiveWorld(PhysicsWorld* world);

		//** Removes the world to be used.
		//*/
		void removeActiveWorld(PhysicsWorld* world);

		///** Retrieve a pointer to the currently selected world.
		//*/
		//PhysicsWorld* getWorld(void);



		/** Returns the scene manager currently being used to render a frame.
		@remarks
		This is only intended for internal use; it is only valid during the
		rendering of a frame.
		*/
		SceneManager* _getCurrentSceneManager(void) const { return mCurrentSceneManager; }
		/** Sets the scene manager currently being used to render a frame.
		@remarks
		This is only intended for internal use.
		*/
		void setCurrentSceneManager(SceneManager* sm);


		//createTetraMesh(Stream& stream);
		//createClothMesh(NxStream& stream);


	}; // class Core


	// inline function
	inline float Core::calculateStepEventTime(float now, StepEventTimeType type)
	{
		// Calculate the average time passed between events of the given type
		float ret = now - mStepEventTimes[type];
		mStepEventTimes[type] = now;
		return ret;
	}


}; // namespace SimStep


#endif // SimStep_Core_h__