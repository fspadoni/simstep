#include "Core/SubTetraMesh.h"

#include "Memory/MemoryAllocatorConfig.h"

//#include "Utilities/NodeElementData.h"



namespace SimStep
{

	SubTetraMesh::SubTetraMesh()
		: mUseSharedNodes(true)
		, mMatInitialised(false)
		, mType(TetraMesh::Tetrahedral)
	{

	}


	SubTetraMesh::~SubTetraMesh()
	{

	}


	//-----------------------------------------------------------------------
	void SubTetraMesh::setMaterialName(const String& name)
	{
		mMaterialName = name;
		mMatInitialised = true;
	}


	const String& SubTetraMesh::getMaterialName() const
	{
		return mMaterialName;
	}

	bool SubTetraMesh::isMatInitialised(void) const
	{
		return mMatInitialised;

	}




	SubMesh::SubMesh()
		: mUseSharedVertices(false)
		//, mMatInitialised(false)
		//, mElements(0)
		//, mUnSharedNodes(0)		
	{

		//mElements =  newSimStep ElementData();
		//mUnSharedNodes = newSimStep NodeData();
	}

	
	SubMesh::~SubMesh()
	{
		//deleteSimStep  mElements;
		//deleteSimStep  mUnSharedNodes;

	}



}; // namespace SimStep

