#ifndef SceneManager_h__
#define SceneManager_h__

#include "Utilities/Common.h"
#include "Core/PhysicsClasses.h"

//#include "Utilities/Definitions.h"

#include <vector>

#include "Utilities/String.h"

namespace SimStep
{

	typedef std::vector<SoftBody*> SoftBodies;
	typedef std::vector<RigidBody*> RigidBodies;
	typedef std::vector<CollisionObject*> CollisionObjects;
	typedef std::vector<Constraint*> Constraints;



	class _SimStepExport SceneManager //: public GeneralAllocatedObject
	{

	protected:


		SceneManager(const String& name);
		
		virtual ~SceneManager();


		void destroyScene();


		//typedef std::map<String, CollisionObject*> CollisionObjectMap;
		///// Simple structure to hold CollisionSoftBody map and a mutex to go with it.
		//struct CollisionObjectCollection
		//{
		//	CollisionObjectMap map;
		//	//OGRE_MUTEX(mutex)
		//};
		//typedef std::map<String, CollisionObjectCollection*> CollisionObjectCollectionMap;
		//CollisionObjectCollectionMap mCollisionObjectCollectionMap;

		///** Gets the CollisionObject collection for the given type name.
		//@remarks
		//This method create new collection if the collection does not exist.
		//*/
		//CollisionObjectCollection* getCollisionObjectCollection(const String& typeName);
		///** Gets the CollisionObject collection for the given type name.
		//@remarks
		//This method throw exception if the collection does not exist.
		//*/
		//const CollisionObjectCollection* getCollisionObjectCollection(const String& typeName) const;
		/////// Mutex over the collection of CollisionObject types
		////OGRE_MUTEX(mCollisionObjectCollectionMutex)


		typedef std::map<String, CollisionObject*> PhysicsObjectMap;
		/// Simple structure to hold CollisionSoftBody map and a mutex to go with it.
		struct PhysicsObjectCollection
		{
			PhysicsObjectMap map;
			//OGRE_MUTEX(mutex)
		};
		typedef std::map<String, PhysicsObjectCollection*> PhysicsObjectCollectionMap;
		PhysicsObjectCollectionMap mPhysicsObjectCollectionMap;


		//typedef std::map<String, RigidBody*> RigidBodyMap;
		///// Simple structure to hold CollisionSoftBody map and a mutex to go with it.
		//struct RigidBodyCollection
		//{
		//	RigidBodyMap map;
		//	//OGRE_MUTEX(mutex)
		//};
		//typedef std::map<String, RigidBodyCollection*> RigidBodyCollectionMap;
		//RigidBodyCollectionMap mRigidBodyCollectionMap;

		///** Gets the RigidBodyBody collection for the given type name.
		//@remarks
		//This method create new collection if the collection does not exist.
		//*/
		PhysicsObjectCollection* getPhysicsObjectCollection(const String& typeName);
		///** Gets the RigidBody collection for the given type name.
		//@remarks
		//This method throw exception if the collection does not exist.
		//*/
		const PhysicsObjectCollection* getPhysicsObjectCollection(const String& typeName) const;
		/////// Mutex over the collection of RigidBody types
		////OGRE_MUTEX(mRigidBodyCollectionMutex)



	public:

		virtual const String& getSceneName(void) const;

		// servir�?
		inline Core* getCore() const { return mCore; }


		CollisionObject* createCollisionObject( const CollisionObjectParams& params );

		void destroyCollisionObject(CollisionObject* m);

		void destroyCollisionObject(const String& name, 
			const String& typeName = StringUtil::BLANK );

		void destroyAllCollisionObjects(void);

		void destroyAllPhysicsObjectsByType(const String& typeName);
		void destroyAllPhysicsObjects(void);


		RigidBody* createRigidBody( const RigidBodyParams& params );

		void destroyRigidBody(RigidBody* rigidObj );

		void destroyAllRigidBodies(void);

		SoftBody* createSoftBody( const String& typeName, 
			const SoftBodyParams& params );

		void destroySoftBody(SoftBody* softbody );

		void destroyAllSoftBodies(void);

		void destroyAllSoftBodiesByType(const String& typeName);


		Constraint* createConstraint( const ConstraintParams& params );


		//RigidBody* createCollisionObject( const Params& params );


		inline size_t getNbCollisionObjects(void) const
		{
			return mCollisionObjects.size();
		}
		inline void* getCollisionObjects(void)
		{
			return mCollisionObjects.size() > 0 ? &mCollisionObjects[0] : 0;
		}
		inline size_t getCollisionObjectsCapacity(void) const
		{
			return mCollisionObjects.capacity();
		}

		inline size_t getNbRigidBodies(void) const
		{
			return mRigidBodies.size();
		}

		inline RigidBodies::iterator getRigidBodies(void)
		{
			return mRigidBodies.begin();
		}

		inline size_t getNbSoftBodies(void) const
		{
			return mSoftBodies.size();
		}

		inline SoftBodies::iterator getSoftBodies(void)
		{
			return mSoftBodies.begin();
		}

		inline size_t getNbConstraints(void) const
		{
			return mConstraints.size();
		}

		inline Constraints::iterator getConstraints(void)
		{
			return mConstraints.begin();
		}





	protected:


		String mSceneName;

		Core* mCore;

		// 
		
		CollisionObjects mCollisionObjects;

		RigidBodies mRigidBodies;

		SoftBodies mSoftBodies;
		
		Constraints mConstraints;



		friend class SceneManagerFactory;
		friend class SceneManagerRegister;


	};
	

	enum NsPhysicsSceneType
	{
		NS_ONLY_COLLISION	= 1,
		NS_RIGID_BODY		= 2,
		NS_SOFT_BODY		= 4,
		NS_SINGLE_CORE		= 8,
		NS_MULTI_CORE		= 16
	};

	class SceneManagerInfo
	{
	public:
		String mTypeName;
		String mDescription;
		NsPhysicsSceneType  mSceneTypeMask;
	};


	class _SimStepExport SceneManagerFactory
	{

	protected:
		
		/** Create a new instance of a SceneManager.
		@remarks
		Don't call directly, use SceneManagerEnumerator::createSceneManager.
		*/
		virtual SceneManager* createInstance(const String& instanceName) const;
		/** Destroy an instance of a SceneManager. */
		virtual void destroyInstance(SceneManager* instance) const;

		/// Internal method to initialise the metadata
		virtual void initSceneInfo(void) const;


	public:
		SceneManagerFactory();

		virtual ~SceneManagerFactory() {};


		const SceneManagerInfo& getSceneInfo() const;

		const NsPhysicsSceneType& getSceneType() const;

		//// provvisorio da sitemare
		//virtual float Simulate( const float& timestep ) {return 0.0f;}
	
	public:

		/// Factory type name
		static const String FACTORY_TYPE_NAME;


	protected:


		mutable SceneManagerInfo mSceneInfo;


		friend class SceneManagerRegister;
	};

	
}; // namespace SimStep

#endif // SceneManager_h__