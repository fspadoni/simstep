#include "Core/SceneManagerRegister.h"

//#include "OgreRenderSystem.h"

#include "Core/LogManager.h"
#include "Utilities/PhysicsException.h"


namespace SimStep
{


	//-----------------------------------------------------------------------
	template<> SceneManagerRegister* Singleton<SceneManagerRegister>::msSingleton = 0;
	
	SceneManagerRegister* SceneManagerRegister::getSingletonPtr(void)
	{
		return msSingleton;
	}
	SceneManagerRegister& SceneManagerRegister::getSingleton(void)
	{  
		assert( msSingleton );  return ( *msSingleton );  
	}

	//-----------------------------------------------------------------------
	SceneManagerRegister::SceneManagerRegister( void )
		: mInstanceCreateCount(0), mCurrentRenderSystem(0)
	{
		addFactory(&mDefaultFactory);

	}
	//-----------------------------------------------------------------------
	SceneManagerRegister::~SceneManagerRegister()
	{

		// Destroy all remaining instances
		// Really should have shutdown and unregistered by now, but catch here in case
		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); ++i)
		{
			// destroy instances
			for( Factories::iterator f = mFactories.begin(); 
				f != mFactories.end(); ++f)
			{
				if ((*f)->getSceneInfo().mTypeName == i->second->getSceneName())
				{
					(*f)->destroyInstance(i->second);
					break;
				}
			}

		}
		mInstances.clear();

	}

	//-----------------------------------------------------------------------
	void SceneManagerRegister::addFactory( SceneManagerFactory* fact)
	{
		mFactories.push_back(fact);
		// add to metadata
		mSceneInfoList.push_back( static_cast<const SceneManagerInfo*>( &fact->getSceneInfo()) );
		// Log
		LogManager::getSingleton().logMessage("SceneManagerFactory for type '" +
			fact->getSceneInfo().mTypeName + "' registered.");
	}

	//-----------------------------------------------------------------------
	void SceneManagerRegister::removeFactory( SceneManagerFactory* fact)
	{
		// destroy all instances for this factory
		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); )
		{
			SceneManager* instance = i->second;
			if (instance->getSceneName() == fact->getSceneInfo().mTypeName)
			{
				fact->destroyInstance(instance);
				Instances::iterator deli = i++;
				mInstances.erase(deli);
			}
			else
			{
				++i;
			}
		}
		// remove from metadata
		for (SceneInfoList::iterator m = mSceneInfoList.begin(); m != mSceneInfoList.end(); ++m)
		{
			if(*m == &(fact->getSceneInfo()))
			{
				mSceneInfoList.erase(m);
				break;
			}
		}
		mFactories.remove(fact);
	}
	//-----------------------------------------------------------------------
	const SceneManagerInfo* SceneManagerRegister::getSceneInfo(const String& typeName) const
	{
		for (SceneInfoList::const_iterator i = mSceneInfoList.begin(); 
			i != mSceneInfoList.end(); ++i)
		{
			if (typeName == (*i)->mTypeName)
			{
				return *i;
			}
		}

		PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
			"No metadata found for scene manager of type '" + typeName + "'",
			"SceneManagerRegister::createSceneManager");

	}
	//-----------------------------------------------------------------------
	SceneManagerRegister::SceneInfoIterator 
		SceneManagerRegister::getSceneInfoIterator(void) const
	{
		return SceneInfoIterator(mSceneInfoList.begin() ); //, mSceneInfoList.end());

	}
	//-----------------------------------------------------------------------
	SceneManager* SceneManagerRegister::createSceneManager(
		const String& typeName, const String& instanceName)
	{
		if (mInstances.find(instanceName) != mInstances.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
				"SceneManager instance called '" + instanceName + "' already exists",
				"SceneManagerRegister::createSceneManager");
		}

		SceneManager* inst = 0;
		for( Factories::iterator i = mFactories.begin(); 
			i != mFactories.end(); ++i)
		{
			if ((*i)->getSceneInfo().mTypeName == typeName)
			{
				if (instanceName.empty())
				{
					// generate a name
					std::stringstream s;
					s << "SceneManagerInstance" << ++mInstanceCreateCount;
					inst = (*i)->createInstance(s.str());
				}
				else
				{
					inst = (*i)->createInstance(instanceName);
				}
				break;
			}
		}

		if (!inst)
		{
			// Error!
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
				"No factory found for scene manager of type '" + typeName + "'",
				"SceneManagerRegister::createSceneManager");
		}

		///// assign rs if already configured
		//if (mCurrentRenderSystem)
		//	inst->_setDestinationRenderSystem(mCurrentRenderSystem);

		mInstances[inst->getSceneName()] = inst;

		return inst;


	}
	//-----------------------------------------------------------------------
	SceneManager* SceneManagerRegister::createSceneManager(
		const NsPhysicsSceneType& typeMask, const String& instanceName)
	{
		if (mInstances.find(instanceName) != mInstances.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
				"SceneManager instance called '" + instanceName + "' already exists",
				"SceneManagerRegister::createSceneManager");
		}

		SceneManager* inst = 0;
		String name = instanceName;
		if (name.empty())
		{
			// generate a name
			std::stringstream s;
			s << "SceneManagerInstance" << ++mInstanceCreateCount;
			name = s.str();
		}

		// Iterate backwards to find the matching factory registered last
		for( Factories::reverse_iterator i = mFactories.rbegin(); 
			i != mFactories.rend(); ++i)
		{
			if ((*i)->getSceneType() & typeMask )
			{
				inst = (*i)->createInstance(name);
				break;
			}
		}

		// use default factory if none
		if (!inst)
			inst = mDefaultFactory.createInstance(name);

		///// assign rs if already configured
		//if (mCurrentRenderSystem)
		//	inst->_setDestinationRenderSystem(mCurrentRenderSystem);

		mInstances[inst->getSceneName()] = inst;

		return inst;

	}
	//-----------------------------------------------------------------------
	void SceneManagerRegister::destroySceneManager( SceneManager* sm)
	{
		// Erase instance from map
		mInstances.erase(sm->getSceneName());

		// Find factory to destroy
		for( Factories::iterator i = mFactories.begin(); 
			i != mFactories.end(); ++i)
		{
			if ((*i)->getSceneInfo().mTypeName == sm->getSceneName())
			{
				(*i)->destroyInstance(sm);
				break;
			}
		}

	}
	//-----------------------------------------------------------------------
	SceneManager* SceneManagerRegister::getSceneManager(const String& instanceName) const
	{
		Instances::const_iterator i = mInstances.find(instanceName);
		if(i != mInstances.end())
		{
			return i->second;
		}
		else
		{
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
				"SceneManager instance with name '" + instanceName + "' not found.",
				"SceneManagerRegister::getSceneManager");
		}

	}
	//-----------------------------------------------------------------------
	SceneManagerRegister::SceneManagerIterator 
		SceneManagerRegister::getSceneManagerIterator(void) const
	{
		return SceneManagerIterator(mInstances.begin()); //, mInstances.end());

	}
	//-----------------------------------------------------------------------
	void SceneManagerRegister::setRenderSystem(Ogre::RenderSystem* rs)
	{
		//mCurrentRenderSystem = rs;

		//for (Instances::iterator i = mInstances.begin(); i != mInstances.end(); ++i)
		//{
		//	i->second->_setDestinationRenderSystem(rs);
		//}

	}

	void SceneManagerRegister::setWorld( PhysicsWorld* world )
	{
		//mCurrentWorld = world;

		//for (Instances::iterator i = mInstances.begin(); i != mInstances.end(); ++i)
		//{
		//	i->second->_setDestinationWorld(world);
		//}
	}

	// 
	// Fede: se possibile eliminare le SceneManager instance utilizzando le
	// SceneManagerFactory come per la creazione in modo da eliminare la dichiarazione
	// friend SceneManagerRegister dalla classe SceneManager
	//-----------------------------------------------------------------------
	void SceneManagerRegister::shutdownAll(void)
	{

		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); ++i)
		{
			// shutdown instances (destroy scene)
			i->second->destroyScene();			
		}
	}

	//-----------------------------------------------------------------------
	const String DefaultSceneManagerFactory::FACTORY_TYPE_NAME = "PhysicsDefaultSceneManager";
	//-----------------------------------------------------------------------
	

	DefaultSceneManagerFactory::DefaultSceneManagerFactory(void)
		: SceneManagerFactory()
	{
		initSceneInfo();
	}
	
	void DefaultSceneManagerFactory::initSceneInfo(void) const
	{
		mSceneInfo.mTypeName = FACTORY_TYPE_NAME;
		mSceneInfo.mDescription = "The default physics scene manager";
		mSceneInfo.mSceneTypeMask = NS_SINGLE_CORE; // | NS_RIGID_BODY | NS_SOFT_BODY;
		//mMetaData.worldGeometrySupported = false;
	}

	//-----------------------------------------------------------------------
	SceneManager* DefaultSceneManagerFactory::createInstance(
		const String& instanceName)
	{
		return new DefaultSceneManager(instanceName);
	}
	//-----------------------------------------------------------------------
	void DefaultSceneManagerFactory::destroyInstance( SceneManager* instance)
	{
		SceneManagerFactory::destroyInstance( instance );
	}
	//-----------------------------------------------------------------------
	//-----------------------------------------------------------------------
	DefaultSceneManager::DefaultSceneManager(const String& name)
		: SceneManager(name)
	{
	}
	//-----------------------------------------------------------------------
	DefaultSceneManager::~DefaultSceneManager()
	{
	}
	//-----------------------------------------------------------------------
	const String& DefaultSceneManager::getSceneName(void) const
	{
		return DefaultSceneManagerFactory::FACTORY_TYPE_NAME;
	}
	//-----------------------------------------------------------------------



}; // namespace