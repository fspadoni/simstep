#ifndef SimStep_ShapeManager_h__
#define SimStep_ShapeManager_h__

#include <Utilities/Singleton.h>


#include <Core/ResourceManager.h>
#include <Core/Resource.h>

#include <Core/Shape.h>
#include <Utilities/DataStream.h>

namespace SimStep
{

	//forward decl
	class ShapeSerializerListener;

	
		class _SimStepExport ShapeManager : public ResourceManager, 
		public Singleton<ShapeManager>, public ManualResourceLoader
	{

	protected:

		Resource* createImpl(const String& name, ResourceHandle handle, 
			const String& group, bool isManual, ManualResourceLoader* loader,
			const DataValuePairList* params);

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( ShapeManager, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );


		ShapeManager();
		
		~ShapeManager();

		ShapePtr createManual( const String& name, const String& groupName, 
			ManualResourceLoader* loader = 0);

		/** @see ManualResourceLoader::loadResource */
		void loadResource(Resource* res);

		/** Initialises the material manager, which also triggers it to 
		* parse all available .program and .material scripts. */
		void initialise(void);


		/** Sets the listener used to control mesh loading through the serializer.
		*/
		void setListener(ShapeSerializerListener *listener);

		/** Gets the listener used to control mesh loading through the serializer.
		*/
		ShapeSerializerListener *getListener();


		static ShapeManager& getSingleton(void);

		static ShapeManager* getSingletonPtr(void);


		// The listener to pass to serializers
		ShapeSerializerListener* mListener;


	};

	
}; // namespace SimStep

#endif // SimStep_ShapeManager_h__