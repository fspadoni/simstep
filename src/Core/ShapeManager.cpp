#include <Core/ShapeManager.h>

#include <Core/ResourceGroupManager.h>

#include <Utilities/PhysicsException.h>



namespace SimStep
{

	template<> ShapeManager* Singleton<ShapeManager>::msSingleton = 0;
	ShapeManager* ShapeManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	ShapeManager& ShapeManager::getSingleton(void)
	{
		assert( msSingleton );  return ( *msSingleton );
	}


	ShapeManager::ShapeManager()
	{

		// Scripting is supported by this manager
		mScriptPatterns.push_back("*.SimStepShape");
		mScriptPatterns.push_back("*.SimStepShape.xml");
		ResourceGroupManager::getSingleton()._registerScriptLoader(this);

		// Resource type
		mResourceType = "SimStepShape";

		// Register with resource group manager
		ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);

	}


	ShapeManager::~ShapeManager()
	{
		// Resources cleared by superclass
		// Unregister with resource group manager
		ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
		
	}

	Resource* ShapeManager::createImpl(const String& name, ResourceHandle handle,
		const String& group, bool isManual, ManualResourceLoader* loader,
		const DataValuePairList* params)
	{
		return newSimStep Shape(this, name, handle, group, isManual, loader);
	}


	ShapePtr ShapeManager::createManual( const String& name, const String& groupName, 
		ManualResourceLoader* loader )
	{
		// Don't try to get existing, create should fail if already exists
		return create(name, groupName, true, loader);
	}


	void ShapeManager::setListener(ShapeSerializerListener *listener)
	{
		mListener = listener;
	}
	
	ShapeSerializerListener* ShapeManager::getListener()
	{
		return mListener;
	}


	void ShapeManager::loadResource( Resource* res)
	{
		Shape* material = static_cast<Shape*>(res);
	}



}; // namespace SimStep