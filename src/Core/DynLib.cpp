
#include "Core/DynLib.h"

#include "Utilities/PhysicsException.h"
#include "Core/LogManager.h"


#  define WIN32_LEAN_AND_MEAN
#  define NOMINMAX // required to stop windows.h messing up std::min
#  include <windows.h>

namespace SimStep
{


	DynLib::DynLib( const String& name )
	{
		mName = name;
		m_hInst = NULL;
	}



	DynLib::~DynLib()
	{
	}



	void DynLib::load()
	{
		// Log library load
		LogManager::getSingleton().logMessage("Loading library " + mName);

		String name = mName;

		m_hInst = (hInstance)LoadLibraryEx(  name.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH );

		if( !m_hInst )
			PHYSICS_EXCEPT(
			Exception::ERR_INTERNAL_ERROR, 
			"Could not load dynamic library " + mName + 
			".  System Error: " + dynlibError(),
			"DynLib::load" );
	}



	void DynLib::unload()
	{
		// Log library unload
		LogManager::getSingleton().logMessage("Unloading library " + mName);

		if( !FreeLibrary( m_hInst ) )
		{
			PHYSICS_EXCEPT(
				Exception::ERR_INTERNAL_ERROR, 
				"Could not unload dynamic library " + mName +
				".  System Error: " + dynlibError(),
				"DynLib::unload");
		}

	}


	void* DynLib::getSymbol( const String& strName ) const throw()
	{
		return (void*)GetProcAddress( m_hInst, strName.c_str() );
	}


	String DynLib::dynlibError( void ) 
	{

		LPVOID lpMsgBuf; 
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS, 
			NULL, 
			GetLastError(), 
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR) &lpMsgBuf, 
			0, 
			NULL 
			); 
		String ret = (char*)lpMsgBuf;
		// Free the buffer.
		LocalFree( lpMsgBuf );
		return ret;
	}


	
}; // namespace SimStep