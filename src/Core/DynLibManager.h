#ifndef SimStep_DynLibManager_h__
#define SimStep_DynLibManager_h__

#include "Utilities/Common.h"
#include "Utilities/Singleton.h"

#include "Core/PhysicsClasses.h"

#include "Utilities/String.h"
#include <map>

#include "Utilities/Definitions.h"

namespace SimStep
{

	/** Manager for Dynamic-loading Libraries.
	@remarks
	This manager keeps a track of all the open dynamic-loading
	libraries, opens them and returns references to already-open
	libraries.
	*/
	class _SimStepExport DynLibManager: public Singleton<DynLibManager>
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( DynLibManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


	protected:
		typedef std::map<String, DynLib*> DynLibList;
		DynLibList mLibList;
	public:
		/** Default constructor.
		@note
		<br>Should never be called as the singleton is automatically
		created during the creation of the Root object.
		@see
		Root::Root
		*/
		DynLibManager();

		/** Default destructor.
		@see
		Root::~Root
		*/
		virtual ~DynLibManager();

		/** Loads the passed library.
		@param
		filename The name of the library. The extension can be omitted
		*/
		DynLib* load(const String& filename);

		/** Unloads the passed library.
		@param
		filename The name of the library. The extension can be omitted
		*/
		void unload(DynLib* lib);

		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static DynLibManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static DynLibManager* getSingletonPtr(void);
	};

	
}; // namespace SimStep

#endif // SimStep_DynLibManager_h__