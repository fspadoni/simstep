#include "Core/SimdMemoryBufferManager.h"

#include "Utilities/SimdMemoryBuffer.h"

#include "Core/LogManager.h"

namespace SimStep
{

	//template<> vBufferManager* Singleton<vBufferManager>::msSingleton = 0;
	//vBufferManager* vBufferManager::getSingletonPtr(void)
	//{
	//	return msSingleton;
	//}
	//vBufferManager& vBufferManager::getSingleton(void)
	//{  
	//	assert( msSingleton ); 
	//	return ( *msSingleton );  
	//}

	//vBufferManager::vBufferManager()
	//{
	//}

	////-----------------------------------------------------------------------
	//vBufferManager::~vBufferManager()
	//{

	//	mBuffers.clear();
	//}

	////BufferSharedPtr BufferManager::createBuffer(
	////	size_t nodeSize, size_t numNodes )
	////{
	////	assert (numNodes > 0);

	////	Buffer* buf = newSimStep Buffer( nodeSize, numNodes );

	////	//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
	////	mBuffers.insert(buf);

	////	return BufferSharedPtr(buf);
	////}


	////void BufferManager::registerBufferSourceAndCopy(
	////	const BufferSharedPtr& sourceBuffer,
	////	const BufferSharedPtr& copy)
	////{
	////	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	////	// Add copy to free temporary vertex buffers
	////	mFreeTempBufferMap.insert(
	////		FreeTemporaryBufferMap::value_type(sourceBuffer.get(), copy));
	////}

	/////** Allocates a copy of a given node buffer.
	////*/
	////BufferSharedPtr BufferManager::allocateBufferCopy(	const BufferSharedPtr& sourceBuffer,
	////	bool copyData )
	////{
	////	//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
	////	{
	////		//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	////		BufferSharedPtr buf;

	////		// Locate existing buffer copy in temporary vertex buffers
	////		FreeTemporaryBufferMap::iterator i = 
	////			mFreeTempBufferMap.find(sourceBuffer.get());
	////		if (i == mFreeTempBufferMap.end())
	////		{

	////			buf = makeBufferCopy( sourceBuffer );
	////		}
	////		else
	////		{
	////			// Allocate existing copy
	////			buf = i->second;
	////			mFreeTempBufferMap.erase(i);
	////		}

	////		// Copy data?
	////		if (copyData)
	////		{
	////			buf->copyData(*(sourceBuffer.get()), 0, 0, sourceBuffer->getSizeInBytes() );
	////		}

	////		//// Insert copy into licensee list
	////		//mTempVertexBufferLicenses.insert(
	////		//	TemporaryVertexBufferLicenseMap::value_type(
	////		//	vbuf.get(),
	////		//	VertexBufferLicense(sourceBuffer.get(), licenseType, EXPIRED_DELAY_FRAME_THRESHOLD, vbuf, licensee)));
	////		//
	////		return buf;
	////	}
	////}


	////void BufferManager::releaseBufferCopy(	const BufferSharedPtr& bufferCopy)
	////{
	////	//OGRE_LOCK_MUTEX(mTempBuffersMutex)

	////	//TemporaryNodeBufferLicenseMap::iterator i =
	////	//mTempNodeBufferLicenses.find(bufferCopy.get());
	////	//if (i != mTempNodeBufferLicenses.end())
	////	//{
	////	//	const VertexBufferLicense& vbl = i->second;

	////	//	vbl.licensee->licenseExpired(vbl.buffer.get());

	////	//mFreeTempNodeBufferMap.insert(
	////	//FreeTemporaryVertexBufferMap::value_type(vbl.originalBufferPtr, vbl.buffer));
	////	//mTempVertexBufferLicenses.erase(i);
	////	//}
	////}


	////BufferSharedPtr BufferManager::makeBufferCopy( const BufferSharedPtr& source )
	////{
	////	return this->createBuffer( source->getDataSize(), source->getSize() );
	////}


	template<> BufferManager* Singleton<BufferManager>::msSingleton = 0;
	BufferManager* BufferManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	BufferManager& BufferManager::getSingleton(void)
	{  
		assert( msSingleton ); 
		return ( *msSingleton );  
	}

	BufferManager::BufferManager()
	{
	}

	//-----------------------------------------------------------------------
	BufferManager::~BufferManager()
	{

		mBuffers.clear();
	}




	//template<> SimdMemoryBufferManager* Singleton<SimdMemoryBufferManager>::msSingleton = 0;
	//SimdMemoryBufferManager* SimdMemoryBufferManager::getSingletonPtr(void)
	//{
	//	return msSingleton;
	//}
	//SimdMemoryBufferManager& SimdMemoryBufferManager::getSingleton(void)
	//{  
	//	assert( msSingleton ); 
	//	return ( *msSingleton );  
	//}


	//SimdMemoryBufferManager::SimdMemoryBufferManager()
	//	: mUnderUsedFrameCount(0)
	//{
	//}
	////-----------------------------------------------------------------------
	//SimdMemoryBufferManager::~SimdMemoryBufferManager()
	//{
	//	// Clear vertex/index buffer list first, avoid destroyed notify do
	//	// unnecessary work, and we'll destroy everything here.
	//	mNodeBuffers.clear();
	//	mElementBuffers.clear();

	//	// Destroy everything
	//	destroyAllDeclarations();
	//	destroyAllBindings();
	//	
	//	// No need to destroy main buffers - they will be destroyed by removal of bindings

	//	// No need to destroy temp buffers - they will be destroyed automatically.
	//}

	//NodeBufferSharedPtr SimdMemoryBufferManager::createNodeBuffer(
	//	size_t nodeSize, size_t numNodes, SimdMemoryBuffer::Usage usage )
	//{
	//	assert (numNodes > 0);

	//	NodeBuffer* nodebuf = newSimStep NodeBuffer(
	//		nodeSize, numNodes, usage, false );

	//	//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
	//	mNodeBuffers.insert(nodebuf);

	//	return NodeBufferSharedPtr(nodebuf);
	//}
	//

	//ElementMemoryBufferSharedPtr SimdMemoryBufferManager::createElementBuffer( 
	//	ElementMemoryBuffer::ElementType Elemtype, 
	//	ElementMemoryBuffer::IndexType idxType, 
	//	size_t numElements, SimdMemoryBuffer::Usage usage,bool useSystemMemory )
	//{
	//	assert (numElements > 0);

	//	ElementMemoryBuffer* elemIdx = newSimStep ElementMemoryBuffer(
	//		Elemtype, idxType, numElements, usage, useSystemMemory );
	//	{
	//		//OGRE_LOCK_MUTEX(mElementsBuffersMutex)
	//		mElementBuffers.insert(elemIdx);
	//	}
	//	return ElementMemoryBufferSharedPtr(elemIdx);

	//}



	//NodeDeclaration* SimdMemoryBufferManager::createNodeDeclaration(void)
	//{
	//	NodeDeclaration* decl = newSimStep NodeDeclaration();
	//	//OGRE_LOCK_MUTEX(mVertexDeclarationsMutex)
	//	mNodeDeclarations.insert(decl);
	//	return decl;
	//}



	//void SimdMemoryBufferManager::destroyNodeDeclaration(NodeDeclaration* decl)
	//{
	//	//OGRE_LOCK_MUTEX(mNodeDeclarationsMutex)
	//	mNodeDeclarations.erase(decl);
	//	if ( decl != NULL )
	//	{
	//		delete decl;
	//		decl = 0;
	//	}

	//}


	//NodeBufferBinding* SimdMemoryBufferManager::createNodeBufferBinding(void)
	//{
	//	NodeBufferBinding* ret = newSimStep NodeBufferBinding();
	//	//OGRE_LOCK_MUTEX(mNodeBufferBindingsMutex)
	//	mNodeBufferBindings.insert(ret);
	//	return ret;
	//}
	//
	//void SimdMemoryBufferManager::destroyNodeBufferBinding(NodeBufferBinding* binding)
	//{
	//	//OGRE_LOCK_MUTEX(mNodeBufferBindingsMutex)
	//	mNodeBufferBindings.erase(binding);
	//	if ( binding != NULL )
	//	{
	//		delete binding;
	//		binding = 0;
	//	}
	//}


	//void SimdMemoryBufferManager::destroyAllDeclarations(void)
	//{
	//	//OGRE_LOCK_MUTEX(mNodeDeclarationsMutex)
	//	NodeDeclarationList::iterator decl;
	//	for (decl = mNodeDeclarations.begin(); decl != mNodeDeclarations.end(); ++decl)
	//	{
	//		//if ( decl != NULL )
	//		//{
	//			delete (*decl);
	//		//	(*decl) = 0;
	//		//}
	//	}
	//	mNodeDeclarations.clear();
	//}


	//void SimdMemoryBufferManager::destroyAllBindings(void)
	//{
	//	//OGRE_LOCK_MUTEX(mNodeBufferBindingsMutex)
	//	NodeBufferBindingList::iterator bind;
	//	for (bind = mNodeBufferBindings.begin(); bind != mNodeBufferBindings.end(); ++bind)
	//	{
	//		delete (*bind);
	//	}
	//	mNodeBufferBindings.clear();
	//}


	//void SimdMemoryBufferManager::registerNodeBufferSourceAndCopy(
	//	const NodeBufferSharedPtr& sourceBuffer,
	//	const NodeBufferSharedPtr& copy)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//	// Add copy to free temporary vertex buffers
	//	mFreeTempNodeBufferMap.insert(
	//	FreeTemporaryNodeBufferMap::value_type(sourceBuffer.get(), copy));
	//}
	//

	//NodeBufferSharedPtr 
	//	SimdMemoryBufferManager::allocateNodeBufferCopy(
	//	const NodeBufferSharedPtr& sourceBuffer, 
	//	/*BufferLicenseType licenseType, HardwareBufferLicensee* licensee,*/
	//	bool copyData)
	//{
	//	// pre-lock the mNodeBuffers mutex, which would usually get locked in
	//	//  makeBufferCopy / createVertexBuffer
	//	// this prevents a deadlock in _notifyVertexBufferDestroyed
	//	// which locks the same mutexes (via other methods) but in reverse order
	//	//OGRE_LOCK_MUTEX(mNodeBuffersMutex)
	//	{
	//		//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//		NodeBufferSharedPtr vbuf;

	//		// Locate existing buffer copy in temporary vertex buffers
	//		FreeTemporaryNodeBufferMap::iterator i = 
	//		mFreeTempNodeBufferMap.find(sourceBuffer.get());
	//		if (i == mFreeTempNodeBufferMap.end())
	//		{
	//			// copy buffer, use shadow buffer and make dynamic
	//			vbuf = makeBufferCopy(
	//				sourceBuffer, 
	//				SimdMemoryBuffer::SMB_CPU_HEAP, 
	//				true);
	//		}
	//		else
	//		{
	//			// Allocate existing copy
	//			vbuf = i->second;
	//			mFreeTempNodeBufferMap.erase(i);
	//		}

	//		// Copy data?
	//		if (copyData)
	//		{
	//			vbuf->copyData(*(sourceBuffer.get()), 0, 0, sourceBuffer->getSizeInBytes(), true);
	//		}

	//		//// Insert copy into licensee list
	//		//mTempVertexBufferLicenses.insert(
	//		//	TemporaryVertexBufferLicenseMap::value_type(
	//		//	vbuf.get(),
	//		//	VertexBufferLicense(sourceBuffer.get(), licenseType, EXPIRED_DELAY_FRAME_THRESHOLD, vbuf, licensee)));
	//		//
	//		return vbuf;
	//	}

	//}
	////-----------------------------------------------------------------------
	//void SimdMemoryBufferManager::releaseNodeBufferCopy(
	//	const NodeBufferSharedPtr& bufferCopy)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)

	//	//TemporaryNodeBufferLicenseMap::iterator i =
	//	//mTempNodeBufferLicenses.find(bufferCopy.get());
	//	//if (i != mTempNodeBufferLicenses.end())
	//	//{
	//	//	const VertexBufferLicense& vbl = i->second;

	//	//	vbl.licensee->licenseExpired(vbl.buffer.get());

	//		//mFreeTempNodeBufferMap.insert(
	//		//FreeTemporaryVertexBufferMap::value_type(vbl.originalBufferPtr, vbl.buffer));
	//		//mTempVertexBufferLicenses.erase(i);
	//	//}
	//}


	//void SimdMemoryBufferManager::touchNodeBufferCopy(
	//	const NodeBufferSharedPtr& bufferCopy)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//	//TemporaryVertexBufferLicenseMap::iterator i =
	//	//mTempVertexBufferLicenses.find(bufferCopy.get());
	//	//if (i != mTempVertexBufferLicenses.end())
	//	//{
	//	//	VertexBufferLicense& vbl = i->second;
	//	//	assert(vbl.licenseType == BLT_AUTOMATIC_RELEASE);

	//	//	vbl.expiredDelay = EXPIRED_DELAY_FRAME_THRESHOLD;
	//	//}
	//}

	//void SimdMemoryBufferManager::_freeUnusedBufferCopies(void)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//	size_t numFreed = 0;

	//	// Free unused temporary buffers
	//	FreeTemporaryNodeBufferMap::iterator i;
	//	i = mFreeTempNodeBufferMap.begin();
	//	while (i != mFreeTempNodeBufferMap.end())
	//	{
	//		FreeTemporaryNodeBufferMap::iterator icur = i++;
	//		// Free the temporary buffer that referenced by ourself only.
	//		// TODO: Some temporary buffers are bound to vertex buffer bindings
	//		// but not checked out, need to sort out method to unbind them.
	//		if (icur->second.useCount() <= 1)
	//		{
	//			++numFreed;
	//			mFreeTempNodeBufferMap.erase(icur);
	//		}
	//	}

	//	StringStream str;
	//	if (numFreed)
	//	{
	//		str << "SimdMemoryBufferManager: Freed " << numFreed << " unused temporary vertex buffers.";
	//	}
	//	else
	//	{
	//		str << "SimdMemoryBufferManager: No unused temporary vertex buffers found.";
	//	}
	//	LogManager::getSingleton().logMessage(str.str(), LML_TRIVIAL);
	//}


	//void SimdMemoryBufferManager::_releaseBufferCopies(bool forceFreeUnused)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//	size_t numUnused = mFreeTempNodeBufferMap.size();
	//	size_t numUsed = mFreeTempNodeBufferMap.size();

	//	//// Erase the copies which are automatic licensed out
	//	//TemporaryVertexBufferLicenseMap::iterator i;
	//	//i = mTempVertexBufferLicenses.begin(); 
	//	//while (i != mTempVertexBufferLicenses.end()) 
	//	//{
	//	//	TemporaryVertexBufferLicenseMap::iterator icur = i++;
	//	//	VertexBufferLicense& vbl = icur->second;
	//	//	if (vbl.licenseType == BLT_AUTOMATIC_RELEASE &&
	//	//		(forceFreeUnused || --vbl.expiredDelay <= 0))
	//	//	{
	//	//		vbl.licensee->licenseExpired(vbl.buffer.get());

	//	//		mFreeTempVertexBufferMap.insert(
	//	//			FreeTemporaryVertexBufferMap::value_type(vbl.originalBufferPtr, vbl.buffer));
	//	//		mTempVertexBufferLicenses.erase(icur);
	//	//	}
	//	//}

	//	// Check whether or not free unused temporary vertex buffers.
	//	if (forceFreeUnused)
	//	{
	//		_freeUnusedBufferCopies();
	//		mUnderUsedFrameCount = 0;
	//	}
	//	else
	//	{
	//		//if (numUsed < numUnused)
	//		//{
	//		//	// Free temporary vertex buffers if too many unused for a long time.
	//		//	// Do overall temporary vertex buffers instead of per source buffer
	//		//	// to avoid overhead.
	//		//	++mUnderUsedFrameCount;
	//		//	if (mUnderUsedFrameCount >= UNDER_USED_FRAME_THRESHOLD)
	//		//	{
	//				_freeUnusedBufferCopies();
	//				mUnderUsedFrameCount = 0;
	//			//}
	//		//}
	//		//else
	//		//{
	//		//	mUnderUsedFrameCount = 0;
	//		//}
	//	}
	//}


	//void SimdMemoryBufferManager::_forceReleaseBufferCopies(
	//	const NodeBufferSharedPtr& sourceBuffer)
	//{
	//	_forceReleaseBufferCopies(sourceBuffer.get());
	//}


	//void SimdMemoryBufferManager::_forceReleaseBufferCopies(
	//	NodeBuffer* sourceBuffer)
	//{
	//	//OGRE_LOCK_MUTEX(mTempBuffersMutex)
	//	//// Erase the copies which are licensed out
	//	//TemporaryVertexBufferLicenseMap::iterator i;
	//	//i = mTempVertexBufferLicenses.begin();
	//	//while (i != mTempVertexBufferLicenses.end()) 
	//	//{
	//	//	TemporaryVertexBufferLicenseMap::iterator icur = i++;
	//	//	const VertexBufferLicense& vbl = icur->second;
	//	//	if (vbl.originalBufferPtr == sourceBuffer)
	//	//	{
	//	//		// Just tell the owner that this is being released
	//	//		vbl.licensee->licenseExpired(vbl.buffer.get());

	//	//		mTempVertexBufferLicenses.erase(icur);
	//	//	}
	//	//}

	//	// Erase the free copies
	//	//
	//	// Why we need this unusual code? It's for resolve reenter problem.
	//	//
	//	// Using mFreeTempVertexBufferMap.erase(sourceBuffer) directly will
	//	// cause reenter into here because vertex buffer destroyed notify.
	//	// In most time there are no problem. But when sourceBuffer is the
	//	// last item of the mFreeTempVertexBufferMap, some STL multimap
	//	// implementation (VC and STLport) will call to clear(), which will
	//	// causing intermediate state of mFreeTempVertexBufferMap, in that
	//	// time destroyed notify back to here cause illegal accessing in
	//	// the end.
	//	//
	//	// For safely reason, use following code to resolve reenter problem.
	//	//
	//	typedef FreeTemporaryNodeBufferMap::iterator _Iter;
	//	std::pair<_Iter, _Iter> range = mFreeTempNodeBufferMap.equal_range(sourceBuffer);
	//	if (range.first != range.second)
	//	{
	//		std::list<NodeBufferSharedPtr> holdForDelayDestroy;
	//		for (_Iter it = range.first; it != range.second; ++it)
	//		{
	//			if (it->second.useCount() <= 1)
	//			{
	//				holdForDelayDestroy.push_back(it->second);
	//			}
	//		}

	//		mFreeTempNodeBufferMap.erase(range.first, range.second);

	//		// holdForDelayDestroy will destroy auto.
	//	}
	//}

	//void SimdMemoryBufferManager::_notifyNodeBufferDestroyed(NodeBuffer* buf)
	//{
	//	//OGRE_LOCK_MUTEX(mNodeBuffersMutex)

	//	NodeBufferList::iterator i = mNodeBuffers.find(buf);
	//	if (i != mNodeBuffers.end())
	//	{
	//		// release vertex buffer copies
	//		mNodeBuffers.erase(i);
	//		_forceReleaseBufferCopies(buf);
	//	}
	//}

	//
	//void SimdMemoryBufferManager::_notifyElementBufferDestroyed(ElementMemoryBuffer* buf)
	//{
	//	//OGRE_LOCK_MUTEX(mElementsBuffersMutex)

	//	ElementBufferList::iterator i = mElementBuffers.find(buf);
	//	if (i != mElementBuffers.end())
	//	{
	//		mElementBuffers.erase(i);
	//	}
	//}

	//NodeBufferSharedPtr 
	//	SimdMemoryBufferManager::makeBufferCopy(
	//	const NodeBufferSharedPtr& source,
	//	SimdMemoryBuffer::Usage usage, bool useShadowBuffer)
	//{
	//	return this->createNodeBuffer(
	//		source->getNodeSize(), 
	//		source->getNumNodes(),
	//		usage );
	//}



	////TempBlendedBufferInfo::~TempBlendedBufferInfo(void)
	////{
	////	// check that temp buffers have been released
	////	SimdMemoryBufferManager &mgr = SimdMemoryBufferManager::getSingleton();
	////	if (!destPositionBuffer.isNull())
	////		mgr.releaseVertexBufferCopy(destPositionBuffer);
	////	if (!destNormalBuffer.isNull())
	////		mgr.releaseVertexBufferCopy(destNormalBuffer);

	////}
	//////-----------------------------------------------------------------------------
	////void TempBlendedBufferInfo::extractFrom(const VertexData* sourceData)
	////{
	////	// Release old buffer copies first
	////	SimdMemoryBufferManager &mgr = SimdMemoryBufferManager::getSingleton();
	////	if (!destPositionBuffer.isNull())
	////	{
	////		mgr.releaseVertexBufferCopy(destPositionBuffer);
	////		assert(destPositionBuffer.isNull());
	////	}
	////	if (!destNormalBuffer.isNull())
	////	{
	////		mgr.releaseVertexBufferCopy(destNormalBuffer);
	////		assert(destNormalBuffer.isNull());
	////	}

	////	NodeDeclaration* decl = sourceData->NodeDeclaration;
	////	NodeBufferBinding* bind = sourceData->NodeBufferBinding;
	////	const VertexElement *posElem = decl->findElementBySemantic(VES_POSITION);
	////	const VertexElement *normElem = decl->findElementBySemantic(VES_NORMAL);

	////	assert(posElem && "Positions are required");

	////	posBindIndex = posElem->getSource();
	////	srcPositionBuffer = bind->getBuffer(posBindIndex);

	////	if (!normElem)
	////	{
	////		posNormalShareBuffer = false;
	////		srcNormalBuffer.setNull();
	////	}
	////	else
	////	{
	////		normBindIndex = normElem->getSource();
	////		if (normBindIndex == posBindIndex)
	////		{
	////			posNormalShareBuffer = true;
	////			srcNormalBuffer.setNull();
	////		}
	////		else
	////		{
	////			posNormalShareBuffer = false;
	////			srcNormalBuffer = bind->getBuffer(normBindIndex);
	////		}
	////	}
	////}
	//////-----------------------------------------------------------------------------
	////void TempBlendedBufferInfo::checkoutTempCopies(bool positions, bool normals)
	////{
	////	bindPositions = positions;
	////	bindNormals = normals;

	////	SimdMemoryBufferManager &mgr = SimdMemoryBufferManager::getSingleton();

	////	if (positions && destPositionBuffer.isNull())
	////	{
	////		destPositionBuffer = mgr.allocateVertexBufferCopy(srcPositionBuffer, 
	////			SimdMemoryBufferManager::BLT_AUTOMATIC_RELEASE, this);
	////	}
	////	if (normals && !posNormalShareBuffer && !srcNormalBuffer.isNull() && destNormalBuffer.isNull())
	////	{
	////		destNormalBuffer = mgr.allocateVertexBufferCopy(srcNormalBuffer, 
	////			SimdMemoryBufferManager::BLT_AUTOMATIC_RELEASE, this);
	////	}
	////}
	//////-----------------------------------------------------------------------------
	////bool TempBlendedBufferInfo::buffersCheckedOut(bool positions, bool normals) const
	////{
	////	SimdMemoryBufferManager &mgr = SimdMemoryBufferManager::getSingleton();

	////	if (positions || (normals && posNormalShareBuffer))
	////	{
	////		if (destPositionBuffer.isNull())
	////			return false;

	////		mgr.touchVertexBufferCopy(destPositionBuffer);
	////	}

	////	if (normals && !posNormalShareBuffer)
	////	{
	////		if (destNormalBuffer.isNull())
	////			return false;

	////		mgr.touchVertexBufferCopy(destNormalBuffer);
	////	}

	////	return true;
	////}
	//////-----------------------------------------------------------------------------
	////void TempBlendedBufferInfo::bindTempCopies(VertexData* targetData, bool suppressHardwareUpload)
	////{
	////	this->destPositionBuffer->suppressHardwareUpdate(suppressHardwareUpload);
	////	targetData->NodeBufferBinding->setBinding(
	////		this->posBindIndex, this->destPositionBuffer);
	////	if (bindNormals && !posNormalShareBuffer && !destNormalBuffer.isNull())
	////	{
	////		this->destNormalBuffer->suppressHardwareUpdate(suppressHardwareUpload);
	////		targetData->NodeBufferBinding->setBinding(
	////			this->normBindIndex, this->destNormalBuffer);
	////	}
	////}
	//////-----------------------------------------------------------------------------
	////void TempBlendedBufferInfo::licenseExpired(HardwareBuffer* buffer)
	////{
	////	assert(buffer == destPositionBuffer.get()
	////		|| buffer == destNormalBuffer.get());

	////	if (buffer == destPositionBuffer.get())
	////		destPositionBuffer.setNull();
	////	if (buffer == destNormalBuffer.get())
	////		destNormalBuffer.setNull();

	////}


}; // namespace SimStep