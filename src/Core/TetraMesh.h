#ifndef SimStep_TetraMesh_h__
#define SimStep_TetraMesh_h__

#include "Utilities/Common.h"

#include <Core/Resource.h>

#include <Memory/MemoryManager.h>

#include "Utilities/SharedPointer.h"

#include "Utilities/SimdMemoryBuffer.h"
//#include "Utilities/NodeElementData.h"

#include "Core/PhysicsClasses.h"

#include <Utilities/DataStream.h>
#include "Utilities/IteratorWrappers.h"

#include <vector>
#include <hash_map>

namespace SimStep
{
	//// forward decl
	//class NodeData;
	//class ElementData;


	class Graph
	{
	public:
		BufferSharedPtr<ushort> columns;
		BufferSharedPtr<ushort> rows;
		BufferSharedPtr<ushort> diagonals;
	};


	// share vertices
	class Vertices
	{
	public:

		BufferSharedPtr<ushort> tetraIndex;
		BufferSharedPtr<Vector3> barycentricCoords;


		const unsigned int getIndexSize() const {return tetraIndex.get()->getDataSize();}

		const unsigned int getSize() const {return tetraIndex.get()->getSize();}

	};

	class _SimStepExport TetraMesh : public Resource
	{

		friend class SubTetraMesh;
		friend class SubMesh;
		friend class TetraMeshSerializer;
		friend class TetraMeshSerializerImpl;
		friend class XmlTetraMeshSerializer;
		friend class TetraMeshObj;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( TetraMesh, Memory::CACHE_ALIGN, MEMCATEGORY_GEOMETRY );


		TetraMesh( ResourceManager* creator, const String& name, ResourceHandle handle,
			const String& group, bool isManual = false, ManualResourceLoader* loader = 0);

		virtual ~TetraMesh();


		enum Type
		{
			Triangular		= 3,
			Tetrahedral		= 4,
			Hexahedral		= 8
		};

		//NodeData* mSharedNodes;

		typedef std::vector<SubTetraMesh*> SubTetraMeshList;
		/** A hashmap used to store optional SubMesh names.
		Translates a name into SubMesh index
		*/
		typedef stdext::hash_map<String, ushort> SubTetraMeshNameMap ;


		typedef std::vector<SubMesh*> SubMeshList;

		/** A hashmap used to store optional SubMesh names.
		Translates a name into SubMesh index
		*/
		typedef stdext::hash_map<String, ushort> SubMeshNameMap ;


		// share nodes
		//size_t mNbNodes;
		//Vector3* mSharedNodes;
		BufferSharedPtr<Vector3> mSharedNodes;

		BufferSharedPtr<ushort> mSharedConstrainedNodes;

		//  tetra mesh surface
		BufferSharedPtr<ushort> mFaces;


		Graph mGraph;
		
		

	protected:
		

		BufferSharedPtr<ushort> mSharedTetrahedrons;


		Vertices mShareVertices;
		// collision nodes


		SubTetraMeshList mSubTetraMeshList;

		SubTetraMeshNameMap mSubTetraMeshNameMap ;



		/** A list of submeshes which make up this mesh.
		Each mesh is made up of 1 or more submeshes, which
		are each based on a single material and can have their
		own vertex data (they may not - they can share vertex data
		from the Mesh, depending on preference).
		*/
		SubMeshList mSubMeshList;

		SubMeshNameMap mSubMeshNameMap ;




		DataStreamPtr mFreshFromDisk;


		/// Name of the material this SubMesh uses.
		String mMaterialName;

		/// Is there a material yet?
		bool mMatInitialised;

		/** Loads the mesh from disk.  This call only performs IO, it
		does not parse the bytestream or check for any errors therein.
		It also does not set up submeshes, etc.  You have to call load()
		to do that.
		*/
		void prepareImpl(void);
		/** Destroys data cached by prepareImpl.
		*/
		void unprepareImpl(void);
		/// @copydoc Resource::loadImpl
		void loadImpl(void);
		/// @copydoc Resource::postLoadImpl
		void postLoadImpl(void);
		/// @copydoc Resource::unloadImpl
		void unloadImpl(void);
		/// @copydoc Resource::calculateSize
		size_t calculateSize(void) const;

	public:

		SubTetraMesh* createSubTetraMesh(void);

		/** Creates a new SubTetraMesh and gives it a name
		*/
		SubTetraMesh* createSubTetraMesh(const String& name);

		ushort _getSubTetraMeshIndex(const String& name) const;

		unsigned short getNumSubTetraMeshes(void) const;

		/** Gives a name to a SubTetraMesh
		*/
		void nameSubTetraMesh(const String& name, ushort index);

		SubTetraMesh* getSubTetraMesh(const String& name) const;

		SubTetraMesh* getSubTetraMesh(unsigned short index) const;

		typedef VectorIterator<SubTetraMeshList> SubTetraMeshIterator;
		/// Gets an iterator over the available subtetrameshes
		SubTetraMeshIterator getSubTetraMeshIterator(void)
		{ 
			return SubTetraMeshIterator(mSubTetraMeshList.begin(), mSubTetraMeshList.end()); 
		}

		/** Gets a reference to the optional name assignments of the SubTetraMeshes. */
		const SubTetraMeshNameMap& getSubTetraMeshNameMap(void) const { return mSubTetraMeshNameMap; }



		//Bool hasCollisionMesh(void) const {return mHasCollisionMesh;}

		/** Creates a new SubMesh.
		@remarks
		Method for manually creating geometry for the mesh.
		Note - use with extreme caution - you must be sure that
		you have set up the geometry properly.
		*/
		SubMesh* createSubMesh(void);
		
		/** Creates a new SubMesh and gives it a name
		*/
		SubMesh* createSubMesh(const String& name);

		//TetraMeshPtr clone(const String& newName, const String& newGroup);


		const Bounds3& getBounds(void) const;

		/** Gets the index of a submesh with a given name.
		@remarks
		Useful if you identify the SubMeshes by name (using nameSubMesh)
		but wish to have faster repeat access.
		*/
		ushort _getSubMeshIndex(const String& name) const;

		unsigned short getNumSubMeshes(void) const;

		/** Gives a name to a SubMesh
		*/
		void nameSubMesh(const String& name, ushort index);

		SubMesh* getSubMesh(const String& name) const;

		SubMesh* getSubMesh(unsigned short index) const;

		typedef VectorIterator<SubMeshList> SubMeshIterator;
		/// Gets an iterator over the available subtetrameshes
		SubMeshIterator getSubMeshIterator(void)
		{ 
			return SubMeshIterator(mSubMeshList.begin(), mSubMeshList.end()); 
		}

		/** Gets a reference to the optional name assignments of the SubMeshes. */
		const SubMeshNameMap& getSubMeshNameMap(void) const { return mSubMeshNameMap; }


		//void setNodeBufferUsage(SimdMemoryBuffer::Usage usage );

		//void setElementBufferUsage(SimdMemoryBuffer::Usage usage );
		///** Gets the usage setting for this meshes vertex buffers. */
		//SimdMemoryBuffer::Usage getNodeBufferUsage(void) const { return mNodeBufferUsage; }
		///** Gets the usage setting for this meshes index buffers. */
		//SimdMemoryBuffer::Usage getElementBufferUsage(void) const { return mElementBufferUsage; }



		/// Sets the name of the Material which this SubMesh will use
		void setMaterialName(const String& matName);
		const String& getMaterialName(void) const;

		/** Returns true if a material has been assigned to the submesh, otherwise returns false.
		*/
		bool isMatInitialised(void) const;


	};

	

	/** Specialisation of SharedPtr to allow SharedPtr to be assigned to MeshPtr 
	@note Has to be a subclass since we need operator=.
	We could templatise this instead of repeating per Resource subclass, 
	except to do so requires a form VC6 does not support i.e.
	ResourceSubclassPtr<T> : public SharedPtr<T>
	*/
	class _SimStepExport TetraMeshPtr : public SharedPtr<TetraMesh> 
	{
	public:
		TetraMeshPtr() : SharedPtr<TetraMesh>() {}
		
		explicit TetraMeshPtr(TetraMesh* rep) : SharedPtr<TetraMesh>(rep) {}
		
		TetraMeshPtr(const TetraMeshPtr& r) : SharedPtr<TetraMesh>(r) {} 
		
		TetraMeshPtr(const ResourcePtr& r);
		
		/// Operator used to convert a ResourcePtr to a MeshPtr
		TetraMeshPtr& operator=(const ResourcePtr& r);
	
	protected:
		/// Override destroy since we need to delete Mesh after fully defined
		void destroy(void);
	};



}; // namespace SimStep

#endif // SimStep_TetraMesh_h__