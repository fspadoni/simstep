//#include "Core/StableHeaders.h"

#include "Core/Core.h"

#include "Core/TaskManagerTbb.h"
#include "Core/LogManager.h"

#include "Utilities/StopWatch.h"
#include "Utilities/StringConverter.h"
#include "Utilities/Profiler.h"

#include "Core/Version.h"
#include "Core/TetraMeshManager.h"
#include <Core/MaterialManager.h>
#include <Core/ShapeManager.h>
#include <Resource/ArchiveManager.h>
#include "Core/ResourceGroupManager.h"
#include "Core/SimdMemoryBufferManager.h"
#include "World/PhysicsWorld.h"
#include "Core/PhysicsWorldRegister.h"
#include "Core/SceneManagerRegister.h"
#include "Core/StatisticsManager.h"
#include <Resource/FileSystem.h>
#include "Core/DynLibManager.h"
#include "Core/Plugin.h"
#include "Core/DynLib.h"

//#include "Ogre/PhysicsSceneManagerRegister.h"

#include "Utilities/StringInterface.h"

#include "Utilities/ConfigFile.h"
#include "Utilities/PhysicsException.h"

#include "Core/SoftBodyFactory.h"

//#include "Utilities/NodeMemoryBuffer.h"

#include "Memory/MemoryManager.h"

//#define _TC_MALLOC
#if defined ( _TC_MALLOC )
	#include "Memory/TcMemory.h"
#else
	#include "Memory/SystemMemory.h"
#endif



// Ogre
//#include "Ogre/PhysicsSceneManager.h"



typedef void (*DLL_START_PLUGIN)(void);
typedef void (*DLL_STOP_PLUGIN)(void);


namespace SimStep 
{

#if defined ( _TC_MALLOC )
	static TcMemory mem; 
#else
	static SystemMemory mem;
#endif

	template<> Core* Singleton<Core>::msSingleton = 0;

	Core& Core::getSingleton(void)
	{
		assert( msSingleton ); 
		return ( *msSingleton );
	}

	Core* Core::getSingletonPtr(void)
	{
		return msSingleton;
	}


	//-----------------------------------------------------------------------
	Core::Core( const String& configFileName, 
		const String& logFileName,
		const String& pluginFileName )
		: m_LogManager(0)
		, mIsInitialised(false)
		, m_PhysicsProfiler(false)
	{
		// superclass will do singleton checking
		String msg;

		//mActiveWorlds = 0;
		//mActiveWorldsIter = 0;

		// Init

		m_Version = StringConverter::toString( Major >> 16 ) + "." +
			StringConverter::toString( Minor >> 8) + "." +
			StringConverter::toString( Patch >> 0) + 
			" " + "(" + VersionName + ")";
		
		m_ConfigFileName = configFileName;

		m_Memory = Memory::getSingletonPtr();

		//this->initialise();

		// Create log manager and default log file if there is no log manager yet
		if(LogManager::getSingletonPtr() == 0)
		{
			m_LogManager = newSimStep LogManager();
			m_LogManager->createLog(logFileName, true, true);
		}

		m_TaskManager = newSimStep TaskManagerTbb();

		mDynLibManager = newSimStep DynLibManager();
		//mDynLibManager = DynLibManager::getSingletonPtr();

		mArchiveManager = newSimStep ArchiveManager();
		// Resource Group manager
		mResourceGroupManager = newSimStep ResourceGroupManager();

		// Create SceneManager register (note - will be managed by singleton)
		mSceneManagerRegister = newSimStep SceneManagerRegister();

		//mOgreSceneManagerRegister = newSimStep Ogre::SceneManagerRegister();

		mCurrentSceneManager = 0;

		////test memory tracker
		//NodeMemoryBuffer* nmb = newSimStep NodeMemoryBuffer( 16, 128, SimdMemoryBuffer::SMB_CPU_HEAP, false );
		
		mFileSystemArchiveFactory = newSimStep FileSystemArchiveFactory();
		ArchiveManager::getSingleton().addArchiveFactory( mFileSystemArchiveFactory );

		// Mesh manager
		m_MeshManager = newSimStep TetraMeshManager();
		m_MaterialManager = newSimStep MaterialManager();
		mShapeManager = newSimStep ShapeManager();

		//mSimdBufferManager = newSimStep SimdMemoryBufferManager();
		//mvBufferManager = newSimStep vBufferManager();
		mBufferManager = newSimStep BufferManager();

		// World manager
		mPhysicsWorldRegister = newSimStep PhysicsWorldRegister();
		

		m_StopWatch = newSimStep StopWatch();

		if ( m_PhysicsProfiler )
		{
			m_Profiler = newSimStep Profiler();
			Profiler::getSingleton().setStopWatch(m_StopWatch);
		}
		
		mStatsManager = newSimStep StatisticsManager();
		
		//// instantiate and register base movable factories
		//mSoftBodyFactory = new SoftBodyFactory();
		//addSoftBodyFactory( mSoftBodyFactory );

		// Load plugins
		initialise();
		if (!pluginFileName.empty())
		{
			loadPlugins(pluginFileName);
		}

		LogManager::getSingleton().logMessage("*-*-* SimStep Initialising");
		msg = "*-*-* Version " + m_Version;
		LogManager::getSingleton().logMessage(msg);

	}


	Core::~Core()
	{
		release();

		unloadPlugins();

		//deleteSimStep mSoftBodyFactory;


		deleteSimStep mStatsManager;

		if (m_PhysicsProfiler )
		{
			deleteSimStep m_Profiler;
		}

		deleteSimStep m_StopWatch;

		deleteSimStep mPhysicsWorldRegister;
		
		//deleteSimStep mvBufferManager;
		deleteSimStep mBufferManager;
		//deleteSimStep mSimdBufferManager;

		deleteSimStep mShapeManager;
		deleteSimStep m_MaterialManager;
		deleteSimStep m_MeshManager;
		
		
		deleteSimStep mOgreSceneManagerRegister;
		deleteSimStep mSceneManagerRegister;

		deleteSimStep mResourceGroupManager;
		
		deleteSimStep mArchiveManager;
		deleteSimStep mFileSystemArchiveFactory;

		deleteSimStep mDynLibManager;

		deleteSimStep m_TaskManager;
		deleteSimStep m_LogManager;

		StringInterface::cleanupDictionary();
	}



	// use this for initial configuration
	void Core::initialise(void)
	{


		mIsInitialised = true;

		//initialisePlugins();

		//return mIsInitialised;
	}


	Bool Core::SimulationStep( float dtime, int simType, void* spawnedTask  )
	{

		if(!_fireStepStarted())
			return false;

		if (!_simulate(simType, spawnedTask))
			return false;

		return _fireStepEnded();
	}


	Bool Core::_fireStepStarted()
	{
		unsigned long now = m_StopWatch->getMilliseconds();
		StepEvent step_event;
		//step_event.timeSinceLastEvent = calculateEventTime(now, SETT_STEP_ANY);
		step_event.timeSinceLastFrame = calculateStepEventTime(now, SETT_STEP_STARTED);

		return _fireStepStarted(step_event);

	}

	Bool Core::_fireStepEnded()
	{

		unsigned long now = m_StopWatch->getMilliseconds();
		StepEvent step_event;
		//step_event.timeSinceLastEvent = calculateEventTime(now, SETT_STEP_ANY);
		step_event.timeSinceLastFrame = calculateStepEventTime(now, SETT_STEP_ENDED);


		return _fireStepEnded(step_event);
	}


	Bool Core::_simulate(const uint simType, void* spawnedTask )
	{

		unsigned long now = m_StopWatch->getMilliseconds();
		StepEvent step_event;
		//step_event.timeSinceLastEvent = calculateEventTime(now, SETT_STEP_ANY);
		float dTime = calculateStepEventTime(now, SETT_STEP_SIMULATED);
		step_event.timeSinceLastFrame = dTime;


		//// update all targets but don't swap buffers
		//mActiveRenderer->_updateAllRenderTargets(false);
		//// give client app opportunity to use queued GPU time
		//bool ret = _fireFrameRenderingQueued();
		//// block for final swap
		//mActiveRenderer->_swapAllRenderTargetBuffers(mActiveRenderer->getWaitForVerticalBlank());

		for ( mActiveWorldsIter = mActiveWorlds.begin()
			; mActiveWorldsIter != mActiveWorlds.end(); ++mActiveWorldsIter )
		{
			(*mActiveWorldsIter)->simulate( dTime, mCurrentSceneManager, simType, spawnedTask );
		}

		if ( mUpdateCPUStats )
		{
			StatisticsManager::getSingleton().update( step_event );
		}
		

		return ssTrue;
	}

	Bool Core::_fireStepStarted(StepEvent& evt)
	{

		std::set<StepListener*>::iterator i;
		
		// Tell all listeners
		for (i= mStepListeners.begin(); i != mStepListeners.end(); ++i)
		{
			if (!(*i)->stepStarted(evt))
				return false;
		}

		return ssTrue;
	}

	Bool Core::_fireStepEnded(StepEvent& evt)
	{
		std::set<StepListener*>::iterator i;

		// Tell all listeners
		bool ret = true;
		for (i= mStepListeners.begin(); i != mStepListeners.end(); ++i)
		{
			if (!(*i)->stepEnded(evt))
			{
				ret = false;
				break;
			}
		}

		return ssTrue;
	}



	void Core::release(void)
	{

	}


	void Core::loadPlugins( const String& pluginsfile )
	{
		StringVector pluginList;
		String pluginDir;
		ConfigFile cfg;

		try 
		{
			cfg.load( pluginsfile );
		}
		catch (Exception)
		{
			LogManager::getSingleton().logMessage(pluginsfile + " not found, automatic plugin loading disabled.");
			return;
		}

		pluginDir = cfg.getSetting("PluginFolder"); // Ignored on Mac OS X, uses Resources/ directory
		pluginList = cfg.getMultiSetting("Plugin");


		char last_char = pluginDir[pluginDir.length()-1];
		if (last_char != '/' && last_char != '\\')
		{
			pluginDir += "\\";
		}

		for( StringVector::iterator it = pluginList.begin(); it != pluginList.end(); ++it )
		{
			loadPlugin(pluginDir + (*it));
		}
	}


	void Core::initialisePlugins(void)
	{
		for (PluginInstanceList::iterator i = mPlugins.begin(); i != mPlugins.end(); ++i)
		{
			(*i)->initialise();
		}
	}

	void Core::releasePlugins()
	{
		// NB Shutdown plugins in reverse order to enforce dependencies
		for (PluginInstanceList::reverse_iterator i = mPlugins.rbegin(); i != mPlugins.rend(); ++i)
		{
			(*i)->release();
		}
	}


	void Core::addResourceLocation(const String& name, const String& locType,
		const String& groupName, bool recursive)
	{
		ResourceGroupManager::getSingleton().addResourceLocation(
			name, locType, groupName, recursive);
	}
	
	void Core::removeResourceLocation(const String& name, const String& groupName)
	{
		ResourceGroupManager::getSingleton().removeResourceLocation(
			name, groupName);
	}


	DataStreamPtr Core::createFileStream(const String& filename, const String& groupName, 
		bool overwrite, const String& locationPattern)
	{
		// Does this file include path specifiers?
		String path, basename;
		StringUtil::splitFilename(filename, basename, path);

		// no path elements, try the resource system first
		DataStreamPtr stream;
		if (path.empty())
		{
			try
			{
				stream = ResourceGroupManager::getSingleton().createResource(
					filename, groupName, overwrite, locationPattern);
			}
			catch (...) {}

		}

		if (stream.isNull())		
		{
			// save direct in filesystem
			std::fstream* fs = static_cast<std::fstream*>(
				SimStepAlignedMalloc(sizeof(std::fstream), Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL) );
			
			fs->open(filename.c_str(), std::ios::out | std::ios::binary);
			if (!*fs)
			{
				SimStepAlignedFree(fs, sizeof(std::fstream), MEMCATEGORY_GENERAL);
				PHYSICS_EXCEPT(Exception::ERR_CANNOT_WRITE_TO_FILE, 
					"Can't open " + filename + " for writing", __FUNCTION__);
			}

			stream = DataStreamPtr(newSimStep FileStreamDataStream(filename, fs));
		}

		return stream;

	}
	
	DataStreamPtr Core::openFileStream(const String& filename, const String& groupName, 
		const String& locationPattern)
	{
		DataStreamPtr stream;
		if (ResourceGroupManager::getSingleton().resourceExists(
			groupName, filename))
		{
			stream = ResourceGroupManager::getSingleton().openResource(
				filename, groupName);
		}
		else
		{
			// try direct
			std::fstream* ifs = static_cast<std::fstream*>(
				SimStepAlignedMalloc(sizeof(std::fstream), Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL) );

			ifs->open(filename.c_str(), std::ios::in | std::ios::binary);
			if(!*ifs)
			{
				//OGRE_DELETE_T(ifs, basic_ifstream, MEMCATEGORY_GENERAL);
				SimStepAlignedFree(ifs, sizeof(std::fstream), MEMCATEGORY_GENERAL);
				PHYSICS_EXCEPT(
					Exception::ERR_FILE_NOT_FOUND, "'" + filename + "' file not found!", __FUNCTION__);
			}
			stream.bind(newSimStep FileStreamDataStream(filename, ifs));
		}
		return stream;
	}


	void Core::unloadPlugins(void)
	{
		// unload dynamic libs first
		for (PluginLibList::reverse_iterator i = mPluginLibs.rbegin(); i != mPluginLibs.rend(); ++i)
		{
			// Call plugin shutdown
			DLL_STOP_PLUGIN pFunc = (DLL_STOP_PLUGIN)(*i)->getSymbol("dllStopPlugin");
			// this will call uninstallPlugin
			pFunc();
			// Unload library & destroy
			DynLibManager::getSingleton().unload(*i);

		}
		mPluginLibs.clear();

		// now deal with any remaining plugins that were registered through other means
		for (PluginInstanceList::reverse_iterator i = mPlugins.rbegin(); i != mPlugins.rend(); ++i)
		{
			// Note this does NOT call uninstallPlugin - this shutdown is for the 
			// detail objects
			(*i)->uninstall();
		}
		mPlugins.clear();

	}


	void Core::installPlugin(Plugin* plugin)
	{
		LogManager::getSingleton().logMessage("Installing plugin: " + plugin->getName());

		mPlugins.push_back(plugin);
		plugin->install();

		// if allocator is already initialised, call world init too
		if (mIsInitialised)
		{
			plugin->initialise();
		}

		LogManager::getSingleton().logMessage("Plugin successfully installed");
	}

	void Core::uninstallPlugin(Plugin* plugin)
	{
		LogManager::getSingleton().logMessage("Uninstalling plugin: " + plugin->getName());
		PluginInstanceList::iterator i = 
			std::find(mPlugins.begin(), mPlugins.end(), plugin);
		if (i != mPlugins.end())
		{
			if (mIsInitialised)
			{
				plugin->release();
			}
			plugin->uninstall();
			mPlugins.erase(i);
		}
		LogManager::getSingleton().logMessage("Plugin successfully uninstalled");

	}

	void Core::loadPlugin(const String& pluginName)
	{
		// Load plugin library
		DynLib* lib = DynLibManager::getSingleton().load( pluginName );
		// Store for later unload
		// Check for existence, because if called 2+ times DynLibManager returns existing entry
		if (std::find(mPluginLibs.begin(), mPluginLibs.end(), lib) == mPluginLibs.end())
		{
			mPluginLibs.push_back(lib);

			// Call startup function
			DLL_START_PLUGIN pFunc = (DLL_START_PLUGIN)lib->getSymbol("dllStartPlugin");

			if (!pFunc)
				PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "Cannot find symbol dllStartPlugin in library " + pluginName,
				"Root::loadPlugin");

			// This must call installPlugin
			pFunc();
		}

	}

	void Core::unloadPlugin(const String& pluginName)
	{
		PluginLibList::iterator i;

		for (i = mPluginLibs.begin(); i != mPluginLibs.end(); ++i)
		{
			if ((*i)->getName() == pluginName)
			{
				// Call plugin shutdown
				DLL_STOP_PLUGIN pFunc = (DLL_STOP_PLUGIN)(*i)->getSymbol("dllStopPlugin");
				// this must call uninstallPlugin
				pFunc();
				// Unload library (destroyed by DynLibManager)
				DynLibManager::getSingleton().unload(*i);
				mPluginLibs.erase(i);
				return;
			}

		}
	}



	void Core::addSceneManagerFactory( SceneManagerFactory* fact ) const
	{
		mSceneManagerRegister->addFactory(fact);
	}

	void Core::removeSceneManagerFactory( SceneManagerFactory* fact)
	{
		mSceneManagerRegister->removeFactory(fact);
	}

	////-----------------------------------------------------------------------
	//const SceneManagerMetaData* Core::getSceneManagerMetaData(const String& typeName) const
	//{
	//	return mOgreSceneManagerRegister->getMetaData(typeName);
	//}
	////-----------------------------------------------------------------------
	//SceneManagerEnumerator::MetaDataIterator 
	//	Core::getSceneManagerMetaDataIterator(void) const
	//{
	//	return mOgreSceneManagerRegister->getMetaDataIterator();
	//}

	// SceneManager 
	SceneManager* Core::createSceneManager( const String& typeName, 
		const String& instanceName )
	{
		return mSceneManagerRegister->createSceneManager(typeName, instanceName);
	}

	SceneManager* Core::createSceneManager( const NsPhysicsSceneType& typeMask, 
		const String& instanceName)
	{
		return mSceneManagerRegister->createSceneManager(typeMask, instanceName);
	}

	/** Destroy an instance of a WorldManager. */
	void Core::destroySceneManager( SceneManager* sm)
	{
		mSceneManagerRegister->destroySceneManager(sm);
	}

	/** Get an existing PhysicsWorldManager instance that has already been created,
	identified by the instance name.
	@param instanceName The name of the instance to retrieve.
	*/
	SceneManager* Core::getSceneManager(const String& instanceName) const
	{
		return mSceneManagerRegister->getSceneManager(instanceName);
	}
	//-----------------------------------------------------------------------
	Core::SceneManagerIterator Core::getSceneManagerIterator(void)
	{
		return mSceneManagerRegister->getSceneManagerIterator();
	}



	// OGRE __________________________________________

	//void Core::addSceneManagerFactory( Ogre::PhysicsSceneManagerFactory* fact )
	//{
	//	mOgreSceneManagerRegister->addFactory(fact);
	//}

	//void Core::removeSceneManagerFactory(Ogre::PhysicsSceneManagerFactory* fact)
	//{
		//mOgreSceneManagerRegister->removeFactory(fact);
	//}

	////-----------------------------------------------------------------------
	//const SceneManagerMetaData* Core::getSceneManagerMetaData(const String& typeName) const
	//{
	//	return mOgreSceneManagerRegister->getMetaData(typeName);
	//}
	////-----------------------------------------------------------------------
	//SceneManagerEnumerator::MetaDataIterator 
	//	Core::getSceneManagerMetaDataIterator(void) const
	//{
	//	return mOgreSceneManagerRegister->getMetaDataIterator();
	//}

	// SceneManager 
	//Ogre::PhysicsSceneManager* Core::createOgreSceneManager( const String& typeName, 
	//	const String& instanceName )
	//{
	//	return mOgreSceneManagerRegister->createSceneManager(typeName, instanceName);
	//}

	//Ogre::PhysicsSceneManager* Core::createOgreSceneManager(Ogre::SceneTypeMask typeMask, 
	//	const String& instanceName)
	//{
	//	return mOgreSceneManagerRegister->createSceneManager(typeMask, instanceName);
	//}

	/** Destroy an instance of a WorldManager. */
	//void Core::destroySceneManager(Ogre::PhysicsSceneManager* sm)
	//{
	//	mOgreSceneManagerRegister->destroySceneManager(sm);
	//}

	/** Get an existing PhysicsWorldManager instance that has already been created,
	identified by the instance name.
	@param instanceName The name of the instance to retrieve.
	*/
	//Ogre::PhysicsSceneManager* Core::getOgreSceneManager(const String& instanceName) const
	//{
	//	return mOgreSceneManagerRegister->getSceneManager(instanceName);
	//}
	////-----------------------------------------------------------------------
	//Core::OgreSceneManagerIterator Core::getOgreSceneManagerIterator(void)
	//{
	//	return mOgreSceneManagerRegister->getSceneManagerIterator();
	//}

	// OGRE __________________________________________

	
	
	void Core::addPhysicsWorldFactory( PhysicsWorldFactory* fact ) const
	{
		mPhysicsWorldRegister->addFactory(fact);
	}

	void Core::removePhysicsWorldFactory( PhysicsWorldFactory* fact)
	{
		mPhysicsWorldRegister->removeFactory(fact);
	}


	PhysicsWorld* Core::createPhysicsWorld( const String& typeName, 
		const PhysicsWorldParam& worldparam )
	{
		return mPhysicsWorldRegister->createPhysicsWorld( typeName, worldparam );
	}

	PhysicsWorld* Core::createPhysicsWorld( const PhysicsWorldType& typeMask, 
		const PhysicsWorldParam& worldparam )
	{
		return mPhysicsWorldRegister->createPhysicsWorld( typeMask,
			worldparam );
	}

	/** Destroy an instance of a PhysicsWorld. */
	void Core::destroyPhysicsWorld(PhysicsWorld* sm)
	{
		mPhysicsWorldRegister->destroyPhysicsWorld(sm);
	}

	PhysicsWorld* Core::getPhysicsWorld(const String& instanceName) const
	{
		return mPhysicsWorldRegister->getPhysicsWorld(instanceName);
	}

	Core::PhysicsWorldMapIterator Core::getPhysicsWorldIterator(void)
	{
		return mPhysicsWorldRegister->getPhysicsWorldIterator();
	}

	Core::PhysicsWorldList& Core::getActivePhysicsWorlds(void) const
	{
		return const_cast<Core::PhysicsWorldList&>(mActiveWorlds);
	}




	void Core::addSoftBodyFactory(SoftBodyFactory* fact, 
		bool overrideExisting )
	{
		SoftBodyFactoryMap::iterator factiter = mSoftBodyFactoryMap.find(
			fact->getType() );

		if (!overrideExisting && factiter != mSoftBodyFactoryMap.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM,
				"A factory of type '" + fact->getType() + "' already exists.",
				"Core::addSoftBodyFactory");
		}


		// Save
		mSoftBodyFactoryMap[fact->getType()] = fact;

		LogManager::getSingleton().logMessage("SoftBodyFactory for type '" +
			fact->getType() + "' registered.");

	}


	void Core::removeSoftBodyFactory(SoftBodyFactory* fact)
	{

		SoftBodyFactoryMap::iterator factiter = mSoftBodyFactoryMap.find(
			fact->getType());

		if (factiter != mSoftBodyFactoryMap.end())
		{
			mSoftBodyFactoryMap.erase(factiter);
		}
	}


	bool Core::hasSoftBodyFactory(const String& typeName) const
	{

		return !(mSoftBodyFactoryMap.find(typeName) == mSoftBodyFactoryMap.end());
	}


	SoftBodyFactory* Core::getSoftBodyFactory(const String& typeName)
	{
		SoftBodyFactoryMap::iterator iter =
			mSoftBodyFactoryMap.find(typeName);
		
		if ( iter == mSoftBodyFactoryMap.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND,
				"SoftBodyFactory of type " + typeName + " does not exist",
				"Core::getSoftBodyFactory");
		}
		return iter->second;
	}


	void Core::addActiveWorld(PhysicsWorld* world)
	{
		//check that the PhysicsWorld isn't already active
		if ( std::find( mActiveWorlds.begin(), mActiveWorlds.end(), world ) 
			== mActiveWorlds.end() )
		{
			mActiveWorlds.push_back(world);
		}
	}


	void Core::removeActiveWorld(PhysicsWorld* world)
	{

		PhysicsWorldList::const_iterator worlditer = 
			std::find( mActiveWorlds.begin(), mActiveWorlds.end(), world );

		if ( worlditer != mActiveWorlds.end() )
		{
			mActiveWorlds.erase(worlditer);
		}
	}


	//PhysicsWorld* Core::getWorldByName(const String& name)
	//{
	//	if (name.empty())
	//	{
	//		// No render system
	//		return NULL;
	//	}

	//	Core::PhysicsWorldList::const_iterator pWorld;
	//	for (pWorld = getAvailableWorlds()->begin(); pWorld != getAvailableWorlds()->end(); ++pWorld)
	//	{
	//		PhysicsWorld* rs = *pWorld;
	//		if (rs->getName() == name)
	//			return rs;
	//	}

	//	// Unrecognised render system
	//	return NULL;
	//}




	void Core::setCurrentSceneManager(SceneManager* sm)
	{
		mCurrentSceneManager = sm;
	}


};