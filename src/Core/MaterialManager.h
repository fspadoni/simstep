#ifndef SimStep_MaterialManager_h__
#define SimStep_MaterialManager_h__

#include "Utilities/Singleton.h"

// ogre
#include <Core/ResourceManager.h>
#include <Core/Resource.h>

#include <Core/Material.h>
#include <Utilities/DataStream.h>

namespace SimStep
{

	
	class _SimStepExport MaterialManager : public ResourceManager, 
		public Singleton<MaterialManager>, public ManualResourceLoader
	{

	protected:

		Resource* createImpl(const String& name, ResourceHandle handle, 
			const String& group, bool isManual, ManualResourceLoader* loader,
			const DataValuePairList* params);

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( MaterialManager, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );


		MaterialManager();
		
		virtual ~MaterialManager();

		MaterialPtr createManual( const String& name, const String& groupName, 
			ManualResourceLoader* loader = 0);

		/** @see ManualResourceLoader::loadResource */
		void loadResource(Resource* res);

		/** Initialises the material manager, which also triggers it to 
		* parse all available .program and .material scripts. */
		void initialise(void);

		/** @see ScriptLoader::parseScript
		*/
		void parseScript( DataStreamPtr& stream, const String& groupName);


		static MaterialManager& getSingleton(void);

		static MaterialManager* getSingletonPtr(void);

	};
	
}; // namespace SimStep

#endif // SimStep_MaterialManager_h__