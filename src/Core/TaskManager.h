#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H

//#include "Utilities/Singleton.h"
//#include "Memory/AlignedObject.h"
#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

namespace SimStep 
{

	//forward decl:
	class Task;


	class _SimStepExport TaskManager //: public GeneralAllocatedObject//: public Singleton<TaskManager>
	{

	public:
		
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( TaskManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


		TaskManager() {};
		~TaskManager() {};


		typedef void ( *JobFunction )( void* );

		typedef uint ( *JobCompletionFunction )( void* );  // return number of jobs added

		//typedef void ( *ParallelForFunction )( void* param, u32 begin, u32 end );


		virtual void NonStandardPerThreadCallback( JobFunction pfnCallback, void* pData ) = 0;

	
		virtual void SetNumberOfThreads( uint uNumberOfThreads )=0;

		//virtual void ParallelFor( ISystemTask* pSystemTask,
		//	ParallelForFunction pfnJobFunction, void* pParam, u32 begin, u32 end, u32 minGrain = 1 ) = 0;
	

		

		virtual void Init( void ) = 0;

		virtual void Shutdown( void )=0;


		/// <summary cref="TaskManager::IssueJobsForSystemTasks">
		/// Call this from the primary thread to schedule system work.
		/// </summary>
		/// <param name="pTasks">an array of <c>ISystemTask</c> objects which should have their
		/// <c>Update</c> methods called asynchronously</param>
		/// <param name="uCount">the size of the <paramref name="pTasks"/> array</param>
		/// <param name="fDeltaTime">amount of time to be passed to each system's <c>Update</c> method</param>

		virtual void IssueJobsForSystemTasks( Task** pTasks, uint uCount, float fDeltaTime )=0;

		/// <summary cref="TaskManager::WaitForSystemTasks">
		/// Call this from the primary thread to wait until all tasks spawned with <c>IssueJobsForSystemTasks</c>
		/// and all of their subtasks are complete.
		/// </summary>
		/// <param name="pTasks">an array of <c>ISystemTask</c> objects</param>
		/// <param name="uCount">the length of the <paramref name="pTasks"/> array</param>
		/// <seealso cref="TaskManager::IssueJobsForSystemTasks"/>
		virtual void WaitForSystemTasks( Task** pTasks, uint uCount )=0;


		enum JobCountInstructionHints
		{
			None, Generic, FP, SIMD_FP, SIMD_INT,
		};

		virtual uint GetRecommendedJobCount( JobCountInstructionHints Hints=None ) = 0;


	

	};


};


#endif