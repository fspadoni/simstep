#include "Core/PhysicsWorldRegister.h"

//#include "Core/Core.h"

//#include "OgreRenderSystem.h"

#include "Core/LogManager.h"
#include "Utilities/PhysicsException.h"

//#include "Ogre/RenderSystem.h"

namespace SimStep
{


	PhysicsWorldFactory::PhysicsWorldFactory(void) 
		: mPhysicsWorldInfoInit(ssFalse)
	{

	}

	const PhysicsWorldInfo& PhysicsWorldFactory::getPhysicsWorldInfo(void) const 
	{
		if (mPhysicsWorldInfoInit == ssFalse )
		{
			initPhysicsWorldInfo();
			mPhysicsWorldInfoInit = ssFalse;
		}
		return mPhysicsWorldInfo;
	}

	const PhysicsWorldType& PhysicsWorldFactory::getPhysicsWorldType(void) const 
	{
		if (mPhysicsWorldInfoInit == ssFalse )
		{
			initPhysicsWorldInfo();
			mPhysicsWorldInfoInit = ssFalse;
		}
		return mPhysicsWorldInfo.mPhysicsWorldTypeMask;
	}


	//void PhysicsWorldFactory::initPhysicsWorldInfo(void) const
	//{
	//}


	//PhysicsWorld* PhysicsWorldFactory::createInstance(
	//	const PhysicsWorldParam& worldparam ) const
	//{
	//	return new PhysicsWorld( worldparam );
	//}

	void PhysicsWorldFactory::destroyInstance(PhysicsWorld* instance) const
	{
		delete instance;
		instance = 0;
	}



	//-----------------------------------------------------------------------
	template<> PhysicsWorldRegister* Singleton<PhysicsWorldRegister>::msSingleton = 0;

	PhysicsWorldRegister* PhysicsWorldRegister::getSingletonPtr(void)
	{
		return msSingleton;
	}
	PhysicsWorldRegister& PhysicsWorldRegister::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}



	//-----------------------------------------------------------------------
	PhysicsWorldRegister::PhysicsWorldRegister( void )
		: mInstanceCreateCount(0), mCurrentRenderSystem(0)
	{
		//addFactory(&mDefaultFactory);

	}
	//-----------------------------------------------------------------------
	PhysicsWorldRegister::~PhysicsWorldRegister()
	{

		// Destroy all remaining instances
		// Really should have shutdown and unregistered by now, but catch here in case
		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); ++i)
		{
			// destroy instances
			for( Factories::iterator f = mFactories.begin(); 
				f != mFactories.end(); ++f)
			{
				if ((*f)->getPhysicsWorldInfo().mTypeName == i->second->getName() )
				{
					(*f)->destroyInstance(i->second);
					break;
				}
			}

		}
		mInstances.clear();

	}

	//-----------------------------------------------------------------------
	void PhysicsWorldRegister::addFactory( PhysicsWorldFactory* fact)
	{
		mFactories.push_back(fact);
		// add to metadata
		mPhysicsWorldInfoList.push_back( static_cast<const PhysicsWorldInfo*>( &fact->getPhysicsWorldInfo()) );
		// Log
		LogManager::getSingleton().logMessage("PhysicsWorldFactory for type '" +
			fact->getPhysicsWorldInfo().mTypeName + "' registered.");
	}

	//-----------------------------------------------------------------------
	void PhysicsWorldRegister::removeFactory( PhysicsWorldFactory* fact)
	{
		// destroy all instances for this factory
		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); )
		{
			PhysicsWorld* instance = i->second;
			if (instance->getName() == fact->getPhysicsWorldInfo().mTypeName)
			{
				fact->destroyInstance(instance);
				Instances::iterator deli = i++;
				mInstances.erase(deli);
			}
			else
			{
				++i;
			}
		}
		// remove from metadata
		for (PhysicsWorldInfoList::iterator m = mPhysicsWorldInfoList.begin();
				m != mPhysicsWorldInfoList.end(); ++m)
		{
			if(*m == &(fact->getPhysicsWorldInfo()))
			{
				mPhysicsWorldInfoList.erase(m);
				break;
			}
		}
		mFactories.remove(fact);
	}
	//-----------------------------------------------------------------------
	const PhysicsWorldInfo* PhysicsWorldRegister::getPhysicsWorldInfo(const String& typeName) const
	{
		for (PhysicsWorldInfoList::const_iterator i = mPhysicsWorldInfoList.begin(); 
			i != mPhysicsWorldInfoList.end(); ++i)
		{
			if (typeName == (*i)->mTypeName)
			{
				return *i;
			}
		}

		PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
			"No metadata found for Physics World of type '" + typeName + "'",
			"PhysicsWorldRegister::createPhysicsWorld");

	}
	//-----------------------------------------------------------------------
	PhysicsWorldRegister::PhysicsWorldInfoIterator 
		PhysicsWorldRegister::getPhysicsWorldInfoIterator(void) const
	{
		return PhysicsWorldInfoIterator(mPhysicsWorldInfoList.begin() ); //, mSceneInfoList.end());

	}
	//-----------------------------------------------------------------------
	PhysicsWorld* PhysicsWorldRegister::createPhysicsWorld(
		const String& typeName, const PhysicsWorldParam& worldparam )
	{
		
		if (mInstances.find(worldparam.mName) != mInstances.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
				"PhysicsWorld instance called '" + worldparam.mName + "' already exists",
				"PhysicsWorldRegister::createPhysicsWorld");
		}

		PhysicsWorld* inst = 0;
		for( Factories::iterator i = mFactories.begin(); 
			i != mFactories.end(); ++i)
		{
			if ((*i)->getPhysicsWorldInfo().mTypeName == typeName)
			{
				if (worldparam.mName.empty())
				{
					// generate a name
					std::stringstream s;
					s << "PhysicsWorldInstance" << ++mInstanceCreateCount;
					const_cast<String&>(worldparam.mName) = s.str();
					inst = (*i)->createInstance( worldparam );
				}
				else
				{
					inst = (*i)->createInstance(worldparam);
				}
				break;
			}
		}

		if (!inst)
		{
			// Error!
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
				"No factory found for Physics World of type '" + typeName + "'",
				"PhysicsWorldRegister::createPhysicsWorld");
		}

		///// assign rs if already configured
		//if (mCurrentRenderSystem)
		//	inst->_setDestinationRenderSystem(mCurrentRenderSystem);

		mInstances[inst->getName()] = inst;

		return inst;


	}
	//-----------------------------------------------------------------------
	PhysicsWorld* PhysicsWorldRegister::createPhysicsWorld(
		const PhysicsWorldType& typeMask, const PhysicsWorldParam& worldparam )
	{
		if (mInstances.find(worldparam.mName) != mInstances.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
				"PhysicsWorld instance called '" + worldparam.mName + "' already exists",
				"PhysicsWorldRegister::createPhysicsWorld");
		}

		PhysicsWorld* inst = 0;
		String name = worldparam.mName;
		if (name.empty())
		{
			// generate a name
			std::stringstream s;
			s << "PhysicsWorldInstance" << ++mInstanceCreateCount;
			name = s.str();
		}

		// Iterate backwards to find the matching factory registered last
		for( Factories::reverse_iterator i = mFactories.rbegin(); 
			i != mFactories.rend(); ++i)
		{
			if ((*i)->getPhysicsWorldType() & typeMask )
			{
				inst = (*i)->createInstance(worldparam);
				break;
			}
		}

		//// use default factory if none
		//if (!inst)
		//	inst = mDefaultFactory.createInstance(name);

		///// assign rs if already configured
		//if (mCurrentRenderSystem)
		//	inst->_setDestinationRenderSystem(mCurrentRenderSystem);

		mInstances[inst->getName()] = inst;

		return inst;

	}
	//-----------------------------------------------------------------------
	void PhysicsWorldRegister::destroyPhysicsWorld( PhysicsWorld* sm)
	{
		// Erase instance from map
		mInstances.erase(sm->getName());

		// Find factory to destroy
		for( Factories::iterator i = mFactories.begin(); 
			i != mFactories.end(); ++i)
		{
			if ((*i)->getPhysicsWorldInfo().mTypeName == sm->getName())
			{
				(*i)->destroyInstance(sm);
				break;
			}
		}

	}
	//-----------------------------------------------------------------------
	PhysicsWorld* PhysicsWorldRegister::getPhysicsWorld(const String& instanceName) const
	{
		Instances::const_iterator i = mInstances.find(instanceName);
		if(i != mInstances.end())
		{
			return i->second;
		}
		else
		{
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
				"PhysicsWorld instance with name '" + instanceName + "' not found.",
				"PhysicsWorldRegister::getPhysicsWorld");
		}

	}
	//-----------------------------------------------------------------------
	PhysicsWorldRegister::PhysicsWorldIterator 
		PhysicsWorldRegister::getPhysicsWorldIterator(void) const
	{
		return PhysicsWorldIterator(mInstances.begin()); //, mInstances.end());

	}
	//-----------------------------------------------------------------------
	void PhysicsWorldRegister::setRenderSystem(Ogre::RenderSystem* rs)
	{
		//mCurrentRenderSystem = rs;

		//for (Instances::iterator i = mInstances.begin(); i != mInstances.end(); ++i)
		//{
		//	i->second->_setDestinationRenderSystem(rs);
		//}

	}

	//void PhysicsWorldRegister::setSceneManager( PhysicsWorld* world )
	//{
	//	//mCurrentWorld = world;

	//	//for (Instances::iterator i = mInstances.begin(); i != mInstances.end(); ++i)
	//	//{
	//	//	i->second->_setDestinationWorld(world);
	//	//}
	//}

	// 
	// Fede: se possibile eliminare le SceneManager instance utilizzando le
	// SceneManagerFactory come per la creazione in modo da eliminare la dichiarazione
	// friend SceneManagerRegister dalla classe SceneManager
	//-----------------------------------------------------------------------
	void PhysicsWorldRegister::shutdownAll(void)
	{

		for ( Instances::iterator i = mInstances.begin(); 
			i != mInstances.end(); ++i)
		{
			// shutdown instances (destroy scene)
			i->second->release();			
		}
	}


};