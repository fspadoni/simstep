#ifndef SimStep_CollisionWorld_h__
#define SimStep_CollisionWorld_h__

#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

#include "btBulletCollisionCommon.h"

#include "Collision/btSparseSDF.h"

#include "Collision/SoftBodyCollisionWorldInfo.h"

namespace SimStep
{

	class CollisionObject;
	class SceneManager;


	// collision info
	class _SimStepExport CollisionWorldParams
	{
		btBroadphaseInterface*	m_broadphase;
		btCollisionDispatcher*	m_dispatcher;
		btCollisionConfiguration* m_collisionConfiguration;
		btSparseSdf<3>			m_sparsesdf;

		Bool mParallelizeCollision;

	public:
		CollisionWorldParams(void);
		CollisionWorldParams(const CollisionWorldParams& collWorldParams);

		~CollisionWorldParams(void);

		void initializeDefault(void);
		void release(void);

		void setBroadphase(btBroadphaseInterface* broadphase );
		void setCollisionDispatcher(btCollisionDispatcher* dispatcher );
		void setCollisionConfiguration(btCollisionConfiguration* collisionConfiguration );
	
		void parallelizeCollision(Bool parallelize ) { mParallelizeCollision = parallelize;}


		inline btBroadphaseInterface* getBroadphase(void) const 
			{return m_broadphase;}
		
		inline btCollisionDispatcher* getCollisionDispatcher(void) const 
			{return m_dispatcher;}

		inline btCollisionConfiguration* getCollisionConfiguration(void) const 
			{return m_collisionConfiguration;}

		//inline Bool isParallelizeCollision(Bool parallelize ) { mParallelizeCollision = parallelize;}


		friend class PhysicsWorld;
		friend class CollisionWorld;

	};	


	class _SimStepExport CollisionWorld : public btCollisionWorld
	{

	public:

		//this constructor doesn't own the dispatcher and paircache/broadphase
		CollisionWorld(	const CollisionWorldParams& collWorldParams );

		////this constructor doesn't own the dispatcher and paircache/broadphase
		//CollisionWorld(
		//	btCollisionDispatcher* Dispatcher, 
		//	btBroadphaseInterface* PairCache, 
		//	btCollisionConfiguration* CollisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration()
		//	);

		virtual ~CollisionWorld();


		void InitializeCollision( btCollisionDispatcher* dispatcher, 
			btBroadphaseInterface* broadphase, 
			btCollisionConfiguration* collisionConfiguration = 0 );


		const CollisionWorldParams& getCollisionWorldParams(void) const
		{	return mCollisionWorldParams; }


		virtual float check( const float& timeStep, SceneManager* scene, void* spawnedTask = NULL);

		virtual void release(void);

		//// overriding of btCollisionWorld
		//virtual void addCollisionObject( CollisionObject* collisionObject,
		//	short int collisionFilterGroup = btBroadphaseProxy::DefaultFilter,
		//	short int collisionFilterMask = btBroadphaseProxy::AllFilter );


		void add( CollisionObject* CollisionObject,
			short int collisionFilterGroup = btBroadphaseProxy::DefaultFilter,
			short int collisionFilterMask = btBroadphaseProxy::AllFilter );

		Bool remove( CollisionObject* CollisionObject );


		//virtual void rayTest(const btVector3& rayFromWorld, const btVector3& rayToWorld, RayResultCallback& resultCallback) const; 

		///// rayTestSingle performs a raycast call and calls the resultCallback. It is used internally by rayTest.
		///// In a future implementation, we consider moving the ray test as a virtual method in btCollisionShape.
		///// This allows more customization.
		//static void	rayTestSingle(const btTransform& rayFromTrans,const btTransform& rayToTrans,
		//	btCollisionObject* collisionObject,
		//	const btCollisionShape* collisionShape,
		//	const btTransform& colObjWorldTransform,
		//	RayResultCallback& resultCallback);


	protected:
		
		// Bullet Collision
		CollisionWorldParams	mCollisionWorldParams;	// Collision World info

		SoftBodyCollisionWorldInfo	mCollisionWorldInfo;	// Collision World info

		const Bool mParallelizeCollision;

		//btCollisionConfiguration* m_CollisionConfiguration;
		//btCollisionDispatcher* m_CollisionDispatcher;
		//btBroadphaseInterface* m_PairCache;
		

		//typedef std::vector<CollisionObject*> CollisionObjects;
		//CollisionObjects mCollisionObjects;

		//CollisionObjects::iterator mCollisionObjectsIterator;

	};


	
}; // namespace SimStep

#endif // SimStep_CollisionWorld_h__