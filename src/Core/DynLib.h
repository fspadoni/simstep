#ifndef SimStep_DynLib_h__
#define SimStep_DynLib_h__

#include "Utilities/Common.h"

typedef struct HINSTANCE__* hInstance;

namespace SimStep
{

	class _SimStepExport DynLib // : public DynLibAlloc
	{
	protected:
		String mName;
		/// Gets the last loading error
		String dynlibError(void);
	public:
		/** Default constructor - used by DynLibManager.
		@warning
		Do not call directly
		*/
		DynLib( const String& name );

		/** Default destructor.
		*/
		~DynLib();

		/** Load the library
		*/
		void load();
		/** Unload the library
		*/
		void unload();
		/// Get the name of the library
		const String& getName(void) const { return mName; }

		/**
		Returns the address of the given symbol from the loaded library.
		@param
		strName The name of the symbol to search for
		@returns
		If the function succeeds, the returned value is a handle to
		the symbol.
		@par
		If the function fails, the returned value is <b>NULL</b>.

		*/
		void* getSymbol( const String& strName ) const throw();

	protected:

		/// Handle to the loaded library.
		hInstance m_hInst;
	};

	
}; // namespace SimStep

#endif // SimStep_DynLib_h__