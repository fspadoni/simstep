#include "Core/TetraMeshManager.h"

#include "Utilities/String.h"

//#include "OgreMesh.h"

#include <Serializers/TetraMeshSerializer.h>

namespace SimStep
{

	//-----------------------------------------------------------------------
	template<> TetraMeshManager* Singleton<TetraMeshManager>::msSingleton = 0;

	TetraMeshManager* TetraMeshManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	TetraMeshManager& TetraMeshManager::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}

	//-----------------------------------------------------------------------


	TetraMeshManager::TetraMeshManager()
		: mBoundsPaddingFactor(0.01f), mListener(0)
	{
		mPrepAllMeshesForShadowVolumes = false;
		//mLoadOrder = 350.0f;
	
		mResourceType = "Mesh3d";
		ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
	}


	TetraMeshManager::~TetraMeshManager()
	{
		ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
	}

	//ResourceManager::ResourceCreateOrRetrieveResult createOrRetrieve(
	//	const String& name,
	//	const String& group,
	//	Bool isManual=False, ManualResourceLoader* loader=0,
	//	const DataValuePairList* params=0,
	//	SimdMemoryBuffer::Usage nodeBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP, 
	//	SimdMemoryBuffer::Usage elementBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP  )
	//{
	//	ResourceManager::ResourceCreateOrRetrieveResult res = 
	//		ResourceManager::createOrRetrieve(name,group,isManual,loader,params);
	//	
	//	//TetraMeshPtr pTetraMesh = res.first;
	//	//// Was it created?
	//	//if (res.second)
	//	//{
	//	//	pTetraMesh->setVertexBufferPolicy(vertexBufferUsage, vertexBufferShadowed);
	//	//	pTetraMesh->setIndexBufferPolicy(indexBufferUsage, indexBufferShadowed);
	//	//}
	//	return res.first;
	//}


	//TetraMeshPtr TetraMeshManager::prepare( const String& filename, const String& groupName,
	//	SimdMemoryBuffer::Usage nodeBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP, 
	//	SimdMemoryBuffer::Usage elementBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP )
	//{

	//	TetraMeshPtr pTetraMesh = createOrRetrieve( filename,groupName,false,0,0,
	//		vertexBufferUsage,indexBufferUsage,
	//		vertexBufferShadowed,indexBufferShadowed).first;
	//	
	//	pTetraMesh->prepare();
	//	return pMesh;
	//}


	//TetraMeshPtr TetraMeshManager::load( const String& filename, const String& groupName,
	//	HardwareBuffer::Usage vertexBufferUsage, 
	//	HardwareBuffer::Usage indexBufferUsage, 
	//	Bool vertexBufferShadowed, Bool indexBufferShadowed )
	//{

	//	MeshPtr pMesh = createOrRetrieve(filename,groupName,false,0,0,
	//		vertexBufferUsage,indexBufferUsage,
	//		vertexBufferShadowed,indexBufferShadowed).first;
	//	
	//	pMesh->load();
	//	return pMesh;
	//}



	TetraMeshPtr TetraMeshManager::createManual( const String& name, const String& groupName, 
		ManualResourceLoader* loader )
	{
		
		// Don't try to get existing, create should fail if already exists
		return create(name, groupName, true, loader);
	}




	void TetraMeshManager::setListener(TetraMeshSerializerListener *listener)
	{
		mListener = listener;
	}
	//-------------------------------------------------------------------------
	TetraMeshSerializerListener* TetraMeshManager::getListener()
	{
		return mListener;
	}
	
	
	void TetraMeshManager::loadResource( Resource* res)
	{
		TetraMesh* tetramesh = static_cast<TetraMesh*>(res);

		//// attempt to create a prefab mesh
		//bool createdPrefab = PrefabFactory::createPrefab(msh);

		//// the mesh was not a prefab..
		//if(!createdPrefab)
		//{
		//	// Find build parameters
		//	MeshBuildParamsMap::iterator ibld = mMeshBuildParams.find(res);
		//	if (ibld == mMeshBuildParams.end())
		//	{
		//		PHYSICS_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, 
		//			"Cannot find build parameters for " + res->getName(),
		//			"TetraMeshManager::loadResource");
		//	}
		//	MeshBuildParams& params = ibld->second;

		//	switch(params.type)
		//	{
		//	case MBT_PLANE:
		//		loadManualPlane(msh, params);
		//		break;
		//	case MBT_CURVED_ILLUSION_PLANE:
		//		loadManualCurvedIllusionPlane(msh, params);
		//		break;
		//	case MBT_CURVED_PLANE:
		//		loadManualCurvedPlane(msh, params);
		//		break;
		//	default:
		//		OGRE_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
		//			"Unknown build parameters for " + res->getName(),
		//			"TetraMeshManager::loadResource");
		//	}
		//}
		return;
	}


	Resource* TetraMeshManager::createImpl(const String& name, ResourceHandle handle, 
		const String& group, bool isManual, ManualResourceLoader* loader, 
		const DataValuePairList* createParams)
	{
		// no use for createParams here
		return newSimStep TetraMesh(this, name, handle, group, isManual, loader);
	}



}; // namespace SimStep