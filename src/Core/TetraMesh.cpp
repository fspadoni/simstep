#include "Core/TetraMesh.h"

#include "Core/TetraMeshManager.h"

#include "Utilities/String.h"

#include "Core/SubTetraMesh.h"

//#include "Utilities/NodeElementData.h"
//#include "Utilities/NodeMemoryBuffer.h"

#include "Core/LogManager.h"

#include "Utilities/PhysicsException.h"

#include <Core/ResourceManager.h>
#include <Core/ResourceGroupManager.h>

#include <Serializers/TetraMeshSerializer.h>
#include <Serializers/XMLTetraMeshSerializer.h>

#include <Utilities/Bounds.h>

namespace SimStep
{

	
	TetraMeshPtr::TetraMeshPtr(const ResourcePtr& r) 
		: SharedPtr<TetraMesh>()
	{
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
			//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
			pRep = static_cast<TetraMesh*>(r.getPointer());
			//pUseCount = pRep ? newSimStepAlloc(unsigned int, MEMCATEGORY_GENERAL)(1) : 0;
			//useFreeMethod = SPFM_DELETE;
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
	}
	
	
	TetraMeshPtr& TetraMeshPtr::operator=(const ResourcePtr& r)
	{
		if (pRep == static_cast<TetraMesh*>(r.getPointer()))
			return *this;
		release();
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		if(true)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
			//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
			pRep = static_cast<TetraMesh*>(r.getPointer());
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
		else
		{
			// RHS must be a null pointer
			assert(r.isNull() && "RHS must be null if it has no mutex!");
			setNull();
		}
		return *this;
	}
	

	void TetraMeshPtr::destroy(void)
	{
		// We're only overriding so that we can destroy after full definition of Mesh
		SharedPtr<TetraMesh>::destroy();
	}



	TetraMesh::TetraMesh(ResourceManager* creator, const String& name, ResourceHandle handle,
		const String& group, bool isManual, ManualResourceLoader* loader)
		: Resource(creator, name, handle, group, isManual, loader)
		//mNodeBufferUsage(SimdMemoryBuffer::SMB_CPU_HEAP),
		//mElementBufferUsage(SimdMemoryBuffer::SMB_CPU_HEAP),
		, mSharedNodes(0)
		, mSharedTetrahedrons(0)
		//mCollisionNodes(0),
		, mSharedConstrainedNodes(0)
		//mFaces(0),
		//mHasCollisionMesh(ssFalse),
		//mMatInitialised(false),
		//mNbCollisionNodes(0)
	{

	}
	
	TetraMesh::~TetraMesh()
	{
		// have to call this here reather than in Resource destructor
		// since calling virtual methods in base destructors causes crash
		Resource::unload();
	}

	
	SubTetraMesh* TetraMesh::createSubTetraMesh(void)
	{
		SubTetraMesh* sub = newSimStep SubTetraMesh();
		sub->mParent = this;

		mSubTetraMeshList.push_back(sub);

		if (isLoaded())
			_dirtyState();

		return sub;
	}

	SubTetraMesh* TetraMesh::createSubTetraMesh(const String& name)
	{
		SubTetraMesh *sub = createSubTetraMesh();
		nameSubTetraMesh(name, (ushort)mSubTetraMeshList.size()-1);
		return sub ;
	}

	ushort TetraMesh::_getSubTetraMeshIndex(const String& name) const
	{
		SubTetraMeshNameMap::const_iterator i = mSubTetraMeshNameMap.find(name) ;
		if (i == mSubTetraMeshNameMap.end())
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "No SubTetraMesh named " + name + " found.",
			"TetraMesh::_getSubTetraMeshIndex");

		return i->second;
	}

	unsigned short TetraMesh::getNumSubTetraMeshes(void) const
	{
		return static_cast< unsigned short >( mSubTetraMeshList.size() );
	}

	void TetraMesh::nameSubTetraMesh(const String& name, ushort index)
	{
		mSubTetraMeshNameMap[name] = index ;
	}

	SubTetraMesh* TetraMesh::getSubTetraMesh(const String& name) const
	{
		ushort index = _getSubTetraMeshIndex(name);
		return getSubTetraMesh(index);
	}

	SubTetraMesh* TetraMesh::getSubTetraMesh(unsigned short index) const
	{
		if (index >= mSubTetraMeshList.size())
		{
			PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
				"Index out of bounds.",
				"TetraMesh::getSubTetraMesh");
		}

		return mSubTetraMeshList[index];
	}

	
	SubMesh* TetraMesh::createSubMesh(void)
	{
		SubMesh* sub = newSimStep SubMesh();
		sub->mParent = this;

		mSubMeshList.push_back(sub);

		if (isLoaded())
			_dirtyState();

		return sub;
	}


	SubMesh* TetraMesh::createSubMesh(const String& name)
	{
		SubMesh *sub = createSubMesh();
		nameSubMesh(name, (ushort)mSubMeshList.size()-1);
		return sub ;
	}

	ushort TetraMesh::_getSubMeshIndex(const String& name) const
	{
		SubMeshNameMap::const_iterator i = mSubMeshNameMap.find(name) ;
		if (i == mSubMeshNameMap.end())
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "No SubMesh named " + name + " found.",
			"TetraMesh::_getSubMeshIndex");

		return i->second;
	}

	unsigned short TetraMesh::getNumSubMeshes(void) const
	{
		return static_cast< unsigned short >( mSubMeshList.size() );
	}


	void TetraMesh::nameSubMesh(const String& name, ushort index)
	{
		mSubMeshNameMap[name] = index ;
	}


	SubMesh* TetraMesh::getSubMesh(const String& name) const
	{
		ushort index = _getSubMeshIndex(name);
		return getSubMesh(index);
	}
	
	SubMesh* TetraMesh::getSubMesh(unsigned short index) const
	{
		if (index >= mSubMeshList.size())
		{
			PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
				"Index out of bounds.",
				"TetraMesh::getSubTetraMeshMesh");
		}

		return mSubMeshList[index];
	}
	
	void TetraMesh::postLoadImpl(void)
	{
	}
	
	void TetraMesh::prepareImpl()
	{
		// Load from specified 'name'
		if ( getCreator()->getVerbose())
			LogManager::getSingleton().logMessage("TetraMesh: Loading "+mName+".");

		mFreshFromDisk =
			ResourceGroupManager::getSingleton().openResource(
			mName, mGroup, true, this);

		// fully prebuffer into host RAM
		mFreshFromDisk = DataStreamPtr( newSimStep MemoryDataStream(mName,mFreshFromDisk), SPFM_DELETE );
	}
	
	void TetraMesh::unprepareImpl()
	{
		mFreshFromDisk.setNull();
	}

	void TetraMesh::loadImpl()
	{
		//TetraMeshSerializer serializer;
		//serializer.setListener(TetraMeshManager::getSingleton().getListener());

		//// If the only copy is local on the stack, it will be cleaned
		//// up reliably in case of exceptions, etc
		//DataStreamPtr data(mFreshFromDisk);
		//mFreshFromDisk.setNull();

		//if (data.isNull()) {
		//	PHYSICS_EXCEPT(Exception::ERR_INVALID_STATE,
		//		"Data doesn't appear to have been prepared in " + mName,
		//		"TetraMesh::loadImpl()");
		//}

		//serializer.importMesh(data, this);

		TetraMeshSerializer serializer;
		//serializer.setListener(TetraMeshManager::getSingleton().getListener());

		// If the only copy is local on the stack, it will be cleaned
		// up reliably in case of exceptions, etc
		DataStreamPtr data(mFreshFromDisk );
		mFreshFromDisk.setNull();

		if (data.isNull()) {
			PHYSICS_EXCEPT(Exception::ERR_INVALID_STATE,
				"Data doesn't appear to have been prepared in " + mName,
				"TetraMesh::loadImpl()");
		}

		serializer.importMesh( data , this);
		//serializer.importMesh(data->getAsString(), this);

	}

	
	void TetraMesh::unloadImpl()
	{
		// Teardown submeshes
		for (SubTetraMeshList::iterator subTetraMeshIter = mSubTetraMeshList.begin();
			subTetraMeshIter != mSubTetraMeshList.end(); ++subTetraMeshIter)
		{
			deleteSimStep (*subTetraMeshIter);
		}

		// Clear SubMesh lists
		mSubTetraMeshList.clear();
		mSubTetraMeshNameMap.clear();


		for (SubMeshList::iterator subMeshIter = mSubMeshList.begin();
			subMeshIter != mSubMeshList.end(); ++subMeshIter)
		{
			deleteSimStep (*subMeshIter);
		}
		

		//if ( !mSharedNodes.isNull() )
		//{
		//	deleteSimStep mSharedNodes;
		//	mSharedNodes = 0;
		//}

		//if ( mSharedTetrahedrons.isNull() )
		//{
		//	deleteSimStep mSharedTetrahedrons;
		//	mSharedTetrahedrons = 0;
		//}

		//if ( mCollisionNodes )
		//{
		//	deleteSimStep mCollisionNodes;
		//	mCollisionNodes = 0;
		//}

		//if ( mConstraintNodes )
		//{
		//	deleteSimStep mConstraintNodes;
		//	mConstraintNodes = 0;
		//}
		//if ( mFaces )
		//{
		//	deleteSimStep mFaces;
		//	mFaces = 0;
		//}

		// Clear SubMesh lists
		mSubMeshList.clear();
		mSubMeshNameMap.clear();

	}



	//TetraMeshPtr TetraMesh::clone(const String& newName, const String& newGroup)
	//{
	//	// This is a bit like a copy constructor, but with the additional aspect of registering the clone with
	//	//  the MeshManager

	//	// New Mesh is assumed to be manually defined rather than loaded since you're cloning it for a reason
	//	String theGroup;
	//	if (newGroup == StringUtil::BLANK)
	//	{
	//		theGroup = this->getGroup();
	//	}
	//	else
	//	{
	//		theGroup = newGroup;
	//	}
	//	TetraMeshPtr newMesh = TetraMeshManager::getSingleton().createManual(newName, theGroup);

	//	// Copy submeshes first
	//	std::vector<SubMesh*>::iterator subi;
	//	SubMesh* newSub;
	//	for (subi = mSubMeshList.begin(); subi != mSubMeshList.end(); ++subi)
	//	{
	//		newSub = newMesh->createSubMesh();

	//		// Copy index data
	//		deleteSimStep newSub->mElements;
	//		deleteSimStep newSub->mUnSharedNodes;

	//		//if (!(*subi)->mUseSharedNodes )
	//		{
	//			// Copy unique node data
	//			newSub->mElements = (*subi)->mElements->clone();
	//			newSub->mUnSharedNodes = (*subi)->mUnSharedNodes->clone();
	//		}

	//		//// Copy index data
	//		//deleteSimStep newSub->mElements;

	//		//newSub->mElements = (*subi)->mElements->clone();

	//	}

	//	// Copy shared geometry and index map, if any
	//	if ( mSharedNodes  )
	//	{
	//		newMesh->mSharedNodes = mSharedNodes->clone();
	//		newMesh->mSharedElements = mSharedElements->clone();
	//	}

	//	newMesh->mCollisionNodes = mCollisionNodes->clone();
	//	newMesh->mConstraintNodes = mConstraintNodes->clone();
	//	newMesh->mFaces = mFaces->clone();
	//	
	//	// Copy submesh names
	//	newMesh->mSubMeshNameMap = mSubMeshNameMap ;

	//	//// Copy bounds
	//	//newMesh->mAABB = mAABB;


	//	newMesh->mNodeBufferUsage = mNodeBufferUsage;
	//	newMesh->mElementBufferUsage = mElementBufferUsage;


	//	newMesh->load();
	//	newMesh->touch();

	//	return newMesh;

	//}


	//void TetraMesh::setNodeBufferUsage(SimdMemoryBuffer::Usage usage )
	//{
	//	mNodeBufferUsage = usage;
	//}

	//void TetraMesh::setElementBufferUsage(SimdMemoryBuffer::Usage usage )
	//{
	//	mElementBufferUsage = usage;
	//}


	const Bounds3& TetraMesh::getBounds(void) const
	{
		return Bounds3();
	}

	size_t TetraMesh::calculateSize(void) const
	{

		size_t ret = 0;
		//unsigned short i;
		//// Shared nodes and elements
		//if ( mSharedNodes )
		//{
		//	for (i = 0;
		//		i < mSharedNodes->nodeBufferBinding->getBufferCount();
		//		++i)
		//	{
		//		ret += mSharedNodes->nodeBufferBinding
		//			->getBuffer(i)->getSizeInBytes();
		//	}

		//	ret += mSharedElements->elementBuffer->getSizeInBytes();
		//}

		//// collision nodes
		//if ( mCollisionNodes )
		//{
		//	for (i = 0;
		//		i < mCollisionNodes->nodeBufferBinding->getBufferCount();
		//		++i)
		//	{
		//		ret += mCollisionNodes->nodeBufferBinding
		//			->getBuffer(i)->getSizeInBytes();
		//	}
		//}
		//

		//// constraint nodes
		//if ( mConstraintNodes )
		//{
		//	for (i = 0;
		//		i < mConstraintNodes->nodeBufferBinding->getBufferCount();
		//		++i)
		//	{
		//		ret += mConstraintNodes->nodeBufferBinding
		//			->getBuffer(i)->getSizeInBytes();
		//	}
		//}
		//

		//// faces
		//if ( mFaces )
		//{
		//	if ( mFaces->elementBuffer.get() )
		//	{
		//		ret += mFaces->elementBuffer->getSizeInBytes();
		//	}			
		//}
		


		//SubMeshList::const_iterator si;
		//
		//if ( mSubMeshList.size() > 0 )
		//{
		//	for (si = mSubMeshList.begin(); si != mSubMeshList.end(); ++si)
		//	{
		//		// Dedicated nodes
		//		if (!(*si)->mUseSharedNodes)
		//		{
		//			for (i = 0;
		//				i < (*si)->mUnSharedNodes->nodeBufferBinding->getBufferCount();
		//				++i)
		//			{
		//				ret += (*si)->mUnSharedNodes->nodeBufferBinding
		//					->getBuffer(i)->getSizeInBytes();
		//			}
		//		}
		//		if (!(*si)->mElements->elementBuffer.isNull())
		//		{
		//			// Index data
		//			ret += (*si)->mElements->elementBuffer->getSizeInBytes();
		//		}

		//	}
		//}
		
		return ret;
	}


	void TetraMesh::setMaterialName(const String& name)
	{
		mMaterialName = name;
		mMatInitialised = true;
	}


	//const String& TetraMesh::getMaterialName() const
	//{
	//	return mMaterialName;
	//}

	//bool TetraMesh::isMatInitialised(void) const
	//{
	//	return mMatInitialised;

	//}

}; // namespace SimStep