#ifndef SimStep_Scheduler_h__
#define SimStep_Scheduler_h__


#include <Utilities/Singleton.h>
#include <Memory/MemoryManager.h>


#include <vector>
#include <map>


// provvisori .. cancella
#include "tbb/task.h"
#include "Core/Core.h"

namespace SimStep
{

	// forward decl
	class TaskManager;

	
	class _SimStepExport Task
	{

	protected:


		String mTaskName;

		Bool mOnlyMainThread;

	public:

		const Bool IsPrimaryThreadOnly() const {return mOnlyMainThread;}

		virtual void Update( const float& TimeStep, Task* this_task = 0 ) = 0;

		virtual const String& getTaskName(void) const = 0;
	};


	class TaskTbb : public Task, public tbb::task
	{

	public:
		TaskTbb() {};
		~TaskTbb() {};

		tbb::task* execute()
		{
			Update( 0.0f, this );
			return NULL;
		}

		virtual void Update( const float& TimeStep, TaskTbb* this_task = 0 ) = 0;
	};


	class SimStepTaskTbb : public TaskTbb
	{

	private:

		Core* mCore;
	public:

		SimStepTaskTbb( Core* core ) : mCore(core) {}
		void Update( const float& TimeStep ) { mCore->getSingleton(); };
	};



	class _SimStepExport Scheduler : public Singleton<Scheduler>
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Scheduler, Memory::CACHE_ALIGN, MEMCATEGORY_ROOT );

		Scheduler( TaskManager* pTaskManager )throw ( ... );


		~Scheduler( void );



		void EnableBenchmarking( Bool bEnable= ssTrue )
		{
			mBenchmarkingEnabled = bEnable;
		}


		void EnableThreading( Bool bEnable=ssTrue )
		{
			mThreadingEnabled = bEnable;
		}


		void addTask( const Task* pTask );

		void removeTask( const Task* pTask);

		void Execute( void );


		/** Register a new SceneManagerFactory. 
		@remarks
		Plugins should call this to register as new SceneManager providers.
		*/
		void addTask( Task* fact);


		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static Scheduler& getSingleton(void);

		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static Scheduler* getSingletonPtr(void);

	protected:

		static const float  smDefaultClockFrequency;

		TaskManager* mTaskManager;

		float mClockFrequency;
		void* mExecutionTimer;

		Bool mBenchmarkingEnabled;
		Bool mThreadingEnabled;


		typedef std::map<String, Task*> TaskExecs;
		typedef TaskExecs::iterator TaskExecsIt;

		//typedef std::vector<const TaskInfo*> TaskInfoList;

		TaskExecs mTaskExecs;

	};

	
}; // namespace SimStep

#endif // SimStep_Scheduler_h__