#ifndef GLOBAL_PARAM_H
#define GLOBAL_PARAM_H


namespace SimStep
{

	enum GlobalParam
	{

		Max_Angular_Velocity,
		Visualize_Normal_Contact
	};


}; // namespace SimStep

#endif