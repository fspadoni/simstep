#ifndef SimStep_SoftBodyFactory_h__
#define SimStep_SoftBodyFactory_h__


#include "Memory/AlignedObject.h"
#include "Utilities/Common.h"

#include "Core/PhysicsClasses.h"


namespace SimStep 
{




	class _SimStepExport SoftBodyFactory //: public AlignedObject16
	{

	protected:

		/// Internal implementation of create method - must be overridden
		virtual SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0) = 0;


	public:

		SoftBodyFactory() {};
		virtual ~SoftBodyFactory() {};

		/// Get the type of the object to be created
		virtual const String& getType(void) const = 0;


		/** Create a new instance of the object.
		@param name The name of the new object
		@param manager The SceneManager instance that will be holding the
		instance once created.
		@param params Name/value pair list of additional parameters required to 
		construct the object (defined per subtype). Optional.
		*/
		virtual SoftBody* createInstance(
			const SoftBodyParams& params,/* SceneManager* manager, */
			const DataValuePairList* otherparams = 0 );


		/** Destroy an instance of the object */
		virtual void destroyInstance(SoftBody* obj) = 0;

	};


}; // namespace SimStep


#endif // SimStep_SoftBodyFactory_h__