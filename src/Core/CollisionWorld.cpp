#include "Core/CollisionWorld.h"

#include "World/CollisionObject.h"
#include <World/CollisionSoftBody.h>

#include "Collision/btSoftBodyRigidBodyCollisionConfiguration.h"

#include "Core/SceneManager.h"
#include "Utilities/StopWatch.h"

namespace SimStep
{


	CollisionWorldParams::CollisionWorldParams(void)
	{

		// Default Init.
		initializeDefault();

	}

	CollisionWorldParams::CollisionWorldParams(const CollisionWorldParams& collWorldParams)
	{
		m_broadphase = collWorldParams.m_broadphase;
		m_dispatcher = collWorldParams.m_dispatcher;
		m_collisionConfiguration = collWorldParams.m_collisionConfiguration;
		mParallelizeCollision = collWorldParams.mParallelizeCollision;

		m_sparsesdf.Initialize();
		m_sparsesdf.Reset();
	}

	CollisionWorldParams::~CollisionWorldParams(void)
	{
	}


	void CollisionWorldParams::initializeDefault(void)
	{

		// Default Init.
		m_collisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration();

		btVector3	worldAabbMin(-1000,-1000,-1000);
		btVector3	worldAabbMax(1000,1000,1000);
		m_broadphase = new btAxisSweep3(worldAabbMin,worldAabbMax);

		m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

		m_sparsesdf.Initialize();
		m_sparsesdf.Reset();

		mParallelizeCollision = false;
	}

	void CollisionWorldParams::release(void)
	{
		Release(m_dispatcher);
		Release(m_broadphase);		
		Release(m_collisionConfiguration);
	}

	void CollisionWorldParams::setBroadphase(btBroadphaseInterface* broadphase )
	{
		if ( broadphase != 0 )
		{
			Release(m_broadphase);
			m_broadphase = broadphase;
		}
	}

	void CollisionWorldParams::setCollisionDispatcher(btCollisionDispatcher* dispatcher )
	{
		if ( dispatcher != 0 )
		{
			Release(m_dispatcher);
			Release(m_collisionConfiguration);
			m_dispatcher = dispatcher;
			m_collisionConfiguration = dispatcher->getCollisionConfiguration();		
		}
	}

	void CollisionWorldParams::setCollisionConfiguration(btCollisionConfiguration* collisionConfiguration )
	{
		if ( collisionConfiguration != 0)
		{
			Release(m_collisionConfiguration);
			m_collisionConfiguration = collisionConfiguration;
			m_dispatcher->setCollisionConfiguration(collisionConfiguration);
		}
	}






	CollisionWorld::CollisionWorld(	const CollisionWorldParams& collWorldParams )
		: btCollisionWorld( 
		collWorldParams.getCollisionDispatcher(), 
		collWorldParams.getBroadphase(), 
		collWorldParams.getCollisionConfiguration() )
		, mCollisionWorldParams( collWorldParams )
		, mParallelizeCollision( collWorldParams.mParallelizeCollision )
	{

		mCollisionWorldInfo.m_broadphase = btCollisionWorld::getBroadphase();
		mCollisionWorldInfo.m_dispatcher = btCollisionWorld::getDispatcher();
		mCollisionWorldInfo.m_sparsesdf.Initialize();
		mCollisionWorldInfo.m_sparsesdf.Reset();

		//mParallelizeCollision = collWorldParams.mParallelizeCollision;
	}

	CollisionWorld::~CollisionWorld()
	{
		release();
	}


	float CollisionWorld::check( const float& timeStep, SceneManager* scene, void* spawnedTask )
	{
		tick_count starTicks = tick_count::now();

		// Bullet Collision Step
		btCollisionWorld::performDiscreteCollisionDetection();

		return  ( tick_count::now() - starTicks ).seconds();
	}



	void CollisionWorld::release(void)
	{

		const int Nb_CollisionBody = m_collisionObjects.size();

		for (int i=0; i<Nb_CollisionBody; ++i )
		{
			btCollisionWorld::removeCollisionObject( m_collisionObjects[0] );
		}

		// release CollisionWorldParams
		mCollisionWorldParams.release();
	}


	//void CollisionWorld::addCollisionObject( CollisionObject* collisionObject,
	//	short int collisionFilterGroup,	short int collisionFilterMask )
	//{
	//	btAssert(collisionObject);

	//	////check that the object isn't already added
	//	//btAssert( m_collisionObjects.findLinearSearch(collisionObject)  == m_collisionObjects.size());

	//	//m_collisionObjects.push_back(collisionObject);

	//	//calculate new AABB
	//	btTransform trans = collisionObject->getWorldTransform();

	//	btVector3	minAabb;
	//	btVector3	maxAabb;
	//	collisionObject->getCollisionShape()->getAabb(trans,minAabb,maxAabb);

	//	int type = collisionObject->getCollisionShape()->getShapeType();
	//	collisionObject->setBroadphaseHandle( getBroadphase()->createProxy(
	//		minAabb,
	//		maxAabb,
	//		type,
	//		collisionObject,
	//		collisionFilterGroup,
	//		collisionFilterMask,
	//		m_dispatcher1,0
	//		) 
	//		);
	//}


	void CollisionWorld::add( CollisionObject* CollisionObject,
		short int collisionFilterGroup,	short int collisionFilterMask )
	{
		btCollisionWorld::addCollisionObject( 
			static_cast<btCollisionObject*>(CollisionObject), 
			collisionFilterGroup, collisionFilterMask );
	}


	Bool CollisionWorld::remove( CollisionObject* CollisionObject )
	{
		btCollisionWorld::removeCollisionObject( static_cast<btCollisionObject*>(CollisionObject) );
		// what returns?
		// always True
		return ssTrue;
	}


	//CollisionWorld::CollisionWorld( 
	//	btCollisionDispatcher* Dispatcher, 
	//	btBroadphaseInterface* PairCache, 
	//	btCollisionConfiguration* CollisionConfiguration 
	//	) 
	//	: btCollisionWorld( Dispatcher, PairCache, CollisionConfiguration )
	//{

	//	//Collision 
	//	m_CollisionConfiguration = CollisionConfiguration;
	//	m_PairCache = PairCache;
	//	m_Dispatcher = Dispatcher;

	//	m_CollisionWorldInfo.m_broadphase = m_PairCache;
	//	m_CollisionWorldInfo.m_dispatcher = m_Dispatcher;
	//	m_CollisionWorldInfo.m_sparsesdf.Initialize();
	//	m_CollisionWorldInfo.m_sparsesdf.Reset();
	//};



	struct btSoftSingleRayCallback : public btBroadphaseRayCallback
	{
		btVector3	m_rayFromWorld;
		btVector3	m_rayToWorld;
		btTransform	m_rayFromTrans;
		btTransform	m_rayToTrans;
		btVector3	m_hitNormal;

		const CollisionWorld*	m_world;
		btCollisionWorld::RayResultCallback&	m_resultCallback;

		btSoftSingleRayCallback(const btVector3& rayFromWorld,const btVector3& rayToWorld,const CollisionWorld* world,btCollisionWorld::RayResultCallback& resultCallback)
			:m_rayFromWorld(rayFromWorld),
			m_rayToWorld(rayToWorld),
			m_world(world),
			m_resultCallback(resultCallback)
		{
			m_rayFromTrans.setIdentity();
			m_rayFromTrans.setOrigin(m_rayFromWorld);
			m_rayToTrans.setIdentity();
			m_rayToTrans.setOrigin(m_rayToWorld);

			btVector3 rayDir = (rayToWorld-rayFromWorld);

			rayDir.normalize ();
			///what about division by zero? --> just set rayDirection[i] to INF/1e30
			m_rayDirectionInverse[0] = rayDir[0] == btScalar(0.0) ? btScalar(1e30) : btScalar(1.0) / rayDir[0];
			m_rayDirectionInverse[1] = rayDir[1] == btScalar(0.0) ? btScalar(1e30) : btScalar(1.0) / rayDir[1];
			m_rayDirectionInverse[2] = rayDir[2] == btScalar(0.0) ? btScalar(1e30) : btScalar(1.0) / rayDir[2];
			m_signs[0] = m_rayDirectionInverse[0] < 0.0;
			m_signs[1] = m_rayDirectionInverse[1] < 0.0;
			m_signs[2] = m_rayDirectionInverse[2] < 0.0;

			m_lambda_max = rayDir.dot(m_rayToWorld-m_rayFromWorld);

		}



		virtual bool	process(const btBroadphaseProxy* proxy)
		{
			///terminate further ray tests, once the closestHitFraction reached zero
			if (m_resultCallback.m_closestHitFraction == btScalar(0.f))
				return false;

			btCollisionObject*	collisionObject = (btCollisionObject*)proxy->m_clientObject;

			//only perform raycast if filterMask matches
			if(m_resultCallback.needsCollision(collisionObject->getBroadphaseHandle())) 
			{
				//RigidcollisionObject* collisionObject = ctrl->GetRigidcollisionObject();
				//btVector3 collisionObjectAabbMin,collisionObjectAabbMax;
#if 0
#ifdef RECALCULATE_AABB
				btVector3 collisionObjectAabbMin,collisionObjectAabbMax;
				collisionObject->getCollisionShape()->getAabb(collisionObject->getWorldTransform(),collisionObjectAabbMin,collisionObjectAabbMax);
#else
				//getBroadphase()->getAabb(collisionObject->getBroadphaseHandle(),collisionObjectAabbMin,collisionObjectAabbMax);
				const btVector3& collisionObjectAabbMin = collisionObject->getBroadphaseHandle()->m_aabbMin;
				const btVector3& collisionObjectAabbMax = collisionObject->getBroadphaseHandle()->m_aabbMax;
#endif
#endif
				//btScalar hitLambda = m_resultCallback.m_closestHitFraction;
				//culling already done by broadphase
				//if (btRayAabb(m_rayFromWorld,m_rayToWorld,collisionObjectAabbMin,collisionObjectAabbMax,hitLambda,m_hitNormal))
				{
					m_world->rayTestSingle(m_rayFromTrans,m_rayToTrans,
						collisionObject,
						collisionObject->getCollisionShape(),
						collisionObject->getWorldTransform(),
						m_resultCallback);
				}
			}
			return true;
		}
	};


//	void	CollisionWorld::rayTest(const btVector3& rayFromWorld, const btVector3& rayToWorld, RayResultCallback& resultCallback) const
//	{
//		BT_PROFILE("rayTest");
//		/// use the broadphase to accelerate the search for objects, based on their aabb
//		/// and for each object with ray-aabb overlap, perform an exact ray test
//		btSoftSingleRayCallback rayCB(rayFromWorld,rayToWorld,this,resultCallback);
//
//#ifndef USE_BRUTEFORCE_RAYBROADPHASE
//		m_broadphasePairCache->rayTest(rayFromWorld,rayToWorld,rayCB);
//#else
//		for (int i=0;i<this->getNumCollisionObjects();i++)
//		{
//			rayCB.process(m_collisionObjects[i]->getBroadphaseHandle());
//		}	
//#endif //USE_BRUTEFORCE_RAYBROADPHASE
//
//	}
//
//
//	void	CollisionWorld::rayTestSingle(const btTransform& rayFromTrans,const btTransform& rayToTrans,
//		btCollisionObject* collisionObject,
//		const btCollisionShape* collisionShape,
//		const btTransform& colObjWorldTransform,
//		RayResultCallback& resultCallback)
//	{
//		if (collisionShape->isSoftBody()) {
//			CollisionSoftBody* softBody = CollisionSoftBody::upcast(collisionObject);
//			if (softBody) {
//				CollisionSoftBody::sRayCast softResult;
//				if (softBody->rayTest(rayFromTrans.getOrigin(), rayToTrans.getOrigin(), softResult)) 
//				{
//
//					if (softResult.fraction<= resultCallback.m_closestHitFraction)
//					{
//
//						btCollisionWorld::LocalShapeInfo shapeInfo;
//						shapeInfo.m_shapePart = 0;
//						shapeInfo.m_triangleIndex = softResult.index;
//						// get the normal
//						//btVector3 normal = softBody->m_faces[softResult.index].m_normal;
//						btVector3 normal(0.0f,0.0f,0.0f);
//						btVector3 rayDir = rayToTrans.getOrigin() - rayFromTrans.getOrigin();
//						//if (normal.dot(rayDir) > 0) {
//						//	// normal always point toward origin of the ray
//						//	normal = -normal;
//						//}
//						btCollisionWorld::LocalRayResult rayResult
//							(collisionObject,
//							&shapeInfo,
//							normal,
//							softResult.fraction);
//						bool	normalInWorldSpace = true;
//						resultCallback.addSingleResult(rayResult,normalInWorldSpace);
//					}
//				}
//			}
//		} 
//		else {
//			btCollisionWorld::rayTestSingle(rayFromTrans,rayToTrans,collisionObject,collisionShape,colObjWorldTransform,resultCallback);
//		}
//	}

	
}; // namespace SimStep