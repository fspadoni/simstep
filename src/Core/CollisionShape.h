#ifndef SimStep_CollisionShape_h__
#define SimStep_CollisionShape_h__


#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "BulletCollision/BroadphaseCollision/btDbvt.h"

#include "Utilities/Common.h"


namespace SimStep
{

	/*align16*/ struct _SimStepExport Bounds3
	{
		Vector3 mAabbMin;
		Vector3 mAabbMax;

		Bounds3() : mAabbMin(0.0f), mAabbMax(0.0f) {}

		inline void reset(void)
		{
			mAabbMin = Vector3::Zero();
			mAabbMax = Vector3::Zero();
		}
	};


	/*align16*/ class _SimStepExport DbvtNode : public btDbvtNode
	{
	public:
		DbvtNode() : btDbvtNode() {}
		virtual ~DbvtNode() {}

	};


	/*align16*/ class _SimStepExport Dbvt : public btDbvt
	{
	public:
		Dbvt() : btDbvt() {}
		virtual ~Dbvt() {}

	};

	//typedef btCollisionShape CollisionShape;

	//class _SimStepExport CollisionShape : public btCollisionShape
	//{
	//public:
	//	CollisionShape() : btCollisionShape() {}
	//	virtual ~CollisionShape() {}

	//	//const CollisionShape& operator = (const btCollisionShape& btCollShape)
	//	//{
	//	//	*this = static_cast<const CollisionShape&>(btCollShape);
	//	//	return *this;
	//	//}
	//};

	
}; // namespace SimStep

#endif // SimStep_CollisionShape_h__