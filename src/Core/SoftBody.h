#ifndef SimStep_SoftBody_h__
#define SimStep_SoftBody_h__

#include "Core/PhysicsClasses.h"

#include <World/CollisionSoftBody.h>
#include "Math/Math.h"
#include "Math/MathSimd.h"

//#include "Core/CollisionShape.h"
#include <Core/Material.h>

// modifica questa inclusione serve per la classe  triangle 
#include "FemSoftBody/FemElements.h"

#include <Utilities/SimdMemoryBuffer.h>

#include <vector>

namespace SimStep
{



	class _SimStepExport SoftBody  : public CollisionSoftBody
	{

		friend class PhysicsWorld;

	public:

		SoftBody(const SoftBodyParams& params, const DataValuePairList* otherparams = 0 );

		SoftBody();

		virtual ~SoftBody();

		static const SoftBody*	downcast(const CollisionObject* colObj);

		static SoftBody*	downcast(CollisionObject* colObj);

	private:

		// define private
		//SoftBody( const SoftBody& softbody ) : CollisionSoftBody(softbody) {};

		const SoftBody& operator= ( const SoftBody& softbody ) { return *this;}


	protected:

		virtual float simulate( const float& TimeStep, void* user_pointer = 0 );


	public:

		// Public Interface 
		
		// pure virtual
		virtual const String& getType(void) const = 0;

		virtual float assembleStiffnessMatrix(void* user_pointer = 0) = 0;

		virtual float solve( const float& TimeStep, void* user_pointer = 0 ) = 0;

		virtual Vector3 getStressForce( const int& elementId ) const = 0;

		//virtual void getStressForceBuffer( void* stressforce ) const = 0;

		virtual SoftBody* clone(void) = 0;

		virtual void setVelocity(const Vector3& newvel );

		virtual void reset(void);
		
		// ______________________________________________
		// inline  methods
		
		inline void setGravity(const Vector3& gravity);

		virtual void addForce(const int& nodeId, const Vector3& force );

		inline void addForces( const Buffer<Vector3>& extforce );

		inline Vector3 getExternForce(const int& nodeId ) const;

		inline const Buffer<Vector3>& getExternForces(void) const;


		inline const int getNbNodes(void) const;

		inline const int getNbElements(void) const;

		inline const int getNbTriangles(void) const;

		
		inline const int getNbConstraintNodes(void) const;

		inline void setNbConstraintNodes(const int& nb_constr_nodes);


		//inline TriangleElement getTriangle(int Id) const;

		//inline const Buffer<ushort>& getTriangles(void) const;

		//inline void* GetElements() const;
		//// specialized this function template
		template<typename T>
		inline T getElements(int elemId) const;

		//inline const Buffer<ushort>& getElements(void) const;


		// node positions
		inline void setPosition( const Vector3& vel, int nodeId );

		inline void setPositions( const Buffer<Vector3>& velbuffer );

		inline Vector3 getPosition(int nodeId) const;

		inline const BufferSharedPtr<Vector3>& getPositions(void) const;

		// velocity
		inline void setVelocity( const Vector3& vel, int nodeId );

		inline void setVelocities( const Buffer<Vector3>& velbuffer );

		inline Vector3 getVelocity( int nodeId ) const;

		inline const Buffer<Vector3>& getVelocities(void) const;

		// mass
		inline float getMass(void) const;

		inline float getNodeMass( int node_index ) const;

		inline const Buffer<float>& getNodeMasses(void) const;

		virtual float getVertexMass(int vertex) const;

		//// collision
		//inline void getAabbBounds( Bounds3& aabbBounds ) const;


		struct ConstraintNode //: public AlignedObject16
		{
			SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( ConstraintNode, Memory::SIMD_ALIGN, MEMCATEGORY_GEOMETRY );

			union
			{
				struct  
				{
					Vector3 m_value;
				};
				struct
				{
					int _float1, _float2, _float3, index;
				};
			};
		};

		inline const Buffer<ushort>& getConstraintNodes(void) const;

		inline const SoftBody::ConstraintNode& getConstraintNode(const int& i) const;



	protected:

		int mNbNodes;
		int mNbElements;
		int mNbTriangles;

		MaterialPtr mMaterial;

		BufferSharedPtr<Vector3> mNodes; // Vector3*

		//BufferSharedPtr<ushort> mElements;

		//BufferSharedPtr<ushort> mTriangles; // TriangleElement*

		BufferSharedPtr<Vector3> mNodesVelocity; // Vector3*


		//BufferSharedPtr<ConstraintNode> mConstraintNodes; // ConstraintNode*
		BufferSharedPtr<ushort> mConstraintNodes;

		int mNbConstraintNodes;


		BufferSharedPtr<Vector3> mExternForces; // Vector3*

		BufferSharedPtr<Vector3> mForces; // Vector3*

		BufferSharedPtr<Vector3> mStressForces; // Vector3*

		BufferSharedPtr<float> mNodeMasses; // float*


		float mMass;

		Vector3 m_Gravity;

	public:


		//void setMoveListenerCallback(MoveListenerCallback moveCallback);

		virtual void updateCollisionShape(void);

		virtual void setPosition(const Vector3& newpos );

		virtual void setOrientation(const Quat& newor );
		

	};

	
	inline void SoftBody::setGravity(const Vector3& gravity)
	{
		m_Gravity = gravity;
	}
	// inline function

	inline void SoftBody::addForce(const int& nodeId, const Vector3& force )
	{	
		Vector3* extforcebuf = mExternForces->lockForWrite<Vector3>(nodeId);
		*extforcebuf += force;
		mExternForces->unlock();
	}

	inline void SoftBody::addForces( const Buffer<Vector3>& extforce )
	{	
		mExternForces->copyData( extforce );
		//assert( ((int)extforce & ( Memory::SIMD_ALIGN - 1 )) == 0 );
		////SimdVector3* destvect = Simd::loadVector3(m_ExternForces);
		//SimdVector3* destvect = reinterpret_cast<SimdVector3*>( mExternForces );
		//const SimdVector3* srcvect = reinterpret_cast<const SimdVector3*>( extforce );
		//int nodeId = mNbNodes;
		//while ( nodeId-- )
		//{
		//	*destvect++ += *srcvect++;
		//}
	}


	inline Vector3 SoftBody::getExternForce(const int& nodeId ) const
	{
		const Vector3* extforcebuf = mExternForces->lockForRead<Vector3>(nodeId);
		mExternForces->unlock();
		return *extforcebuf;
	}

	inline const Buffer<Vector3>& SoftBody::getExternForces(void) const
	{
		return *mExternForces;
	}



	inline const int SoftBody::getNbNodes(void) const 
	{
		return mNbNodes;
	}


	inline const int SoftBody::getNbElements(void) const 
	{
		return mNbElements;
	}

	inline const int SoftBody::getNbTriangles(void) const 
	{
		return mNbTriangles; 
	}

	inline const int SoftBody::getNbConstraintNodes(void) const
	{		
		return mNbConstraintNodes;
	}

	inline void SoftBody::setNbConstraintNodes(const int& nb_constr_nodes)
	{
		mNbConstraintNodes = nb_constr_nodes;
	}


	//inline TriangleElement SoftBody::getTriangle(int TriId) const
	//{
	//	const TriangleElement* trianglebuf = mTriangles->lockForRead<TriangleElement>(TriId);
	//	mTriangles->unlock();
	//	return *trianglebuf;
	//}

	//inline const Buffer<ushort>& SoftBody::getTriangles() const
	//{
	//	return *mTriangles;
	//}

	template<typename T>
	inline T SoftBody::getElements(int elemId) const
	{ 
		const T* elembuf = mElements->lockForRead<T>(elemId);
		mElements->unlock();
		return *elembuf;
	}


	//inline const Buffer<ushort>& SoftBody::getElements(void) const
	//{ 
	//	return *mElements;
	//}


	inline void SoftBody::setPosition( const Vector3& pos, int nodeId )
	{
		Vector3* nodebuf = mNodes->lockForWrite<Vector3>(nodeId);
		*nodebuf = pos;
		mNodes->unlock();
	}

	inline void SoftBody::setPositions( const Buffer<Vector3>& posbuffer )
	{
		mNodes->copyData( posbuffer );
		//assert( ((int)posbuffer & ( Memory::SIMD_ALIGN - 1 )) == 0 );
		////SimdVector3* srcvect = Simd::loadVector3(posbuffer);
		//SimdVector3* destvect = reinterpret_cast<SimdVector3*>( m_Nodes );
		//const SimdVector3* srcvect = reinterpret_cast<const SimdVector3*>( posbuffer );
		//int nodeId = mNbNodes;
		//while ( nodeId-- )
		//{
		//	*destvect++ = *srcvect++;
		//}
	}

	inline Vector3 SoftBody::getPosition(int nodeId) const
	{ 
		const Vector3* nodebuf = mNodes->lockForRead<Vector3>(nodeId);
		mNodes->unlock();
		return *nodebuf;
	}

	inline const BufferSharedPtr<Vector3>& SoftBody::getPositions(void) const
	{ 
		return mNodes;
	}


	inline void SoftBody::setVelocity( const Vector3& vel, int nodeId )
	{
		Vector3* nodevelbuf = mNodesVelocity->lockForWrite<Vector3>(nodeId);
		*nodevelbuf = vel;
		mNodesVelocity->unlock();
	}

	
	inline void SoftBody::setVelocities(  const Buffer<Vector3>& velbuffer )
	{
		mNodesVelocity->copyData( velbuffer );
		//assert( ((int)velbuffer & ( Memory::SIMD_ALIGN - 1 )) == 0 );
		////SimdVector3* srcvect = Simd::loadVector3(velbuffer);
		//SimdVector3* destvect = reinterpret_cast<SimdVector3*>( mNodesVelocity );
		//const SimdVector3* srcvect = reinterpret_cast<const SimdVector3*>( velbuffer );
		//int nodeId = mNbNodes;
		//while ( nodeId-- )
		//{
		//	*destvect++ = *srcvect++;
		//}
	}

	inline Vector3 SoftBody::getVelocity( int nodeId ) const
	{
		const Vector3* nodevelbuf = mNodesVelocity->lockForRead<Vector3>(nodeId);
		mNodesVelocity->unlock();
		return *nodevelbuf;
	}

	inline const Buffer<Vector3>& SoftBody::getVelocities(void) const
	{
		return *mNodesVelocity;
	}


	inline float SoftBody::getMass(void) const
	{
		return mMass;
	}

	inline float SoftBody::getNodeMass( int nodeId ) const
	{
		const float* nodemassbuf = mNodeMasses->lockForRead<float>(nodeId);
		mNodeMasses->unlock();
		return *nodemassbuf;
	}

	inline const Buffer<float>& SoftBody::getNodeMasses(void) const
	{
		return *mNodeMasses;
	}


	inline const Buffer<ushort>& SoftBody::getConstraintNodes(void) const
	{
		return *mConstraintNodes;
	}

	inline const SoftBody::ConstraintNode& SoftBody::getConstraintNode(const int& i) const
	{
		const ConstraintNode* constrnodebuf = mConstraintNodes->lockForRead<ConstraintNode>(i);
		mConstraintNodes->unlock();
		return *constrnodebuf;
	}

	//inline void SoftBody::getAabbBounds( Bounds3& aabbBounds) const
	//{
	//	aabbBounds.mAabbMin = m_Bounds.mAabbMin;
	//	aabbBounds.mAabbMax = m_Bounds.mAabbMax;
	//}

	
}; // namespace SimStep

#endif // SimStep_SoftBody_h__