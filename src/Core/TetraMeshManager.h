#ifndef SimStep_TetraMeshManager_h__
#define SimStep_TetraMeshManager_h__


#include "Utilities/Singleton.h"
// ogre
#include <Core/ResourceGroupManager.h>
#include <Core/ResourceManager.h>

#include "Core/PhysicsClasses.h"
#include "Utilities/Common.h"
#include <Utilities/Definitions.h>

#include "Core/TetraMesh.h"

//forward ogre decl
namespace Ogre
{
	class MeshSerializerListener;
}


namespace SimStep
{


	//forward decl
	class TetraMeshSerializerListener;


	class _SimStepExport TetraMeshManager : public ResourceManager, public Singleton<TetraMeshManager>, 
		public ManualResourceLoader
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( TetraMeshManager, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );


		TetraMeshManager();
		virtual ~TetraMeshManager();

		//ResourcePtr create(const String& name, const String& group, 
		//	bool isManual = false, Ogre::ManualResourceLoader* loader = 0, 
		//	const Ogre::NameValuePairList* createParams = 0);


		/** Create a new mesh, or retrieve an existing one with the same
		name if it already exists.
		@param vertexBufferUsage The usage flags with which the vertex buffer(s)
		will be created
		@param indexBufferUsage The usage flags with which the index buffer(s) created for 
		this mesh will be created with.
		@param vertexBufferShadowed If true, the vertex buffers will be shadowed by system memory 
		copies for faster read access
		@param indexBufferShadowed If true, the index buffers will be shadowed by system memory 
		copies for faster read access
		@see ResourceManager::createOrRetrieve
		*/
		//Ogre::ResourceManager::ResourceCreateOrRetrieveResult createOrRetrieve(
		//	const String& name,
		//	const String& group,
		//	Bool isManual=False, Ogre::ManualResourceLoader* loader=0,
		//	const DataValuePairList* params=0,
		//	SimdMemoryBuffer::Usage nodeBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP, 
		//	SimdMemoryBuffer::Usage elementBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP );


		/** Prepares a mesh for loading from a file.  This does the IO in advance of the call to load().
		@note
		If the model has already been created (prepared or loaded), the existing instance
		will be returned.
		@remarks
		Ogre loads model files from it's own proprietary
		format called .mesh. This is because having a single file
		format is better for runtime performance, and we also have
		control over pre-processed data (such as
		collision boxes, LOD reductions etc).
		@param filename The name of the .mesh file
		@param groupName The name of the resource group to assign the mesh to 
		@param vertexBufferUsage The usage flags with which the vertex buffer(s)
		will be created
		@param indexBufferUsage The usage flags with which the index buffer(s) created for 
		this mesh will be created with.
		@param vertexBufferShadowed If true, the vertex buffers will be shadowed by system memory 
		copies for faster read access
		@param indexBufferShadowed If true, the index buffers will be shadowed by system memory 
		copies for faster read access
		*/
		//TetraMeshPtr prepare( const String& filename, const String& groupName,
		//	SimdMemoryBuffer::Usage nodeBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP, 
		//	SimdMemoryBuffer::Usage elementBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP );


		/** Loads a mesh from a file, making it immediately available for use.
		@note
		If the model has already been created (prepared or loaded), the existing instance
		will be returned.
		@remarks
		Ogre loads model files from it's own proprietary
		format called .mesh. This is because having a single file
		format is better for runtime performance, and we also have
		control over pre-processed data (such as
		collision boxes, LOD reductions etc).
		@param filename The name of the .mesh file
		@param groupName The name of the resource group to assign the mesh to 
		@param vertexBufferUsage The usage flags with which the vertex buffer(s)
		will be created
		@param indexBufferUsage The usage flags with which the index buffer(s) created for 
		this mesh will be created with.
		@param vertexBufferShadowed If true, the vertex buffers will be shadowed by system memory 
		copies for faster read access
		@param indexBufferShadowed If true, the index buffers will be shadowed by system memory 
		copies for faster read access
		*/
		//TetraMeshPtr load( const String& filename, const String& groupName,
		//	SimdMemoryBuffer::Usage vertexBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP, 
		//	SimdMemoryBuffer::Usage indexBufferUsage = SimdMemoryBuffer::SMB_CPU_HEAP );


		/** Creates a new Mesh specifically for manual definition rather
		than loading from an object file. 
		@remarks
		Note that once you've defined your mesh, you must call Mesh::_setBounds and
		Mesh::_setBoundingRadius in order to define the bounds of your mesh. In previous
		versions of OGRE you could call Mesh::_updateBounds, but OGRE's support of 
		write-only vertex buffers makes this no longer appropriate.
		@param name The name to give the new mesh
		@param groupName The name of the resource group to assign the mesh to 
		@param loader ManualResourceLoader which will be called to load this mesh
		when the time comes. It is recommended that you populate this field
		in order that the mesh can be rebuilt should the need arise
		*/
		TetraMeshPtr createManual( const String& name, const String& groupName, 
			ManualResourceLoader* loader = 0);

		/** Creates a basic plane, by default majoring on the x/y axes facing positive Z.
		@param
		name The name to give the resulting mesh
		@param 
		groupName The name of the resource group to assign the mesh to 
		@param
		plane The orientation of the plane and distance from the origin
		@param
		width The width of the plane in world coordinates
		@param
		height The height of the plane in world coordinates
		@param
		xsegments The number of segments to the plane in the x direction
		@param
		ysegments The number of segments to the plane in the y direction
		@param
		normals If true, normals are created perpendicular to the plane
		@param
		numTexCoordSets The number of 2D texture coordinate sets created - by default the corners
		are created to be the corner of the texture.
		@param
		uTile The number of times the texture should be repeated in the u direction
		@param
		vTile The number of times the texture should be repeated in the v direction
		@param
		upVector The 'Up' direction of the plane.
		@param
		vertexBufferUsage The usage flag with which the vertex buffer for this plane will be created
		@param
		indexBufferUsage The usage flag with which the index buffer for this plane will be created
		@param
		vertexShadowBuffer If this flag is set to true, the vertex buffer will be created 
		with a system memory shadow buffer,
		allowing you to read it back more efficiently than if it is in hardware
		@param
		indexShadowBuffer If this flag is set to true, the index buffer will be 
		created with a system memory shadow buffer,
		allowing you to read it back more efficiently than if it is in hardware
		*/
		//Ogre::MeshPtr createPlane(
		//	const String& name, const String& groupName, const Plane& plane,
		//	Real width, Real height,
		//	int xsegments = 1, int ysegments = 1,
		//	bool normals = true, int numTexCoordSets = 1,
		//	Real uTile = 1.0f, Real vTile = 1.0f, const Vector3& upVector = Vector3::UNIT_Y,
		//	Ogre::HardwareBuffer::Usage vertexBufferUsage = Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY, 
		//	Ogre::HardwareBuffer::Usage indexBufferUsage = Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
		//	bool vertexShadowBuffer = true, bool indexShadowBuffer = true);





		///** Tells the mesh manager that all future meshes should prepare themselves for
		//shadow volumes on loading.
		//*/
		//void setPrepareAllMeshesForShadowVolumes(Bool enable) 
		//	{ mPrepAllMeshesForShadowVolumes = enable; }
		///** Retrieves whether all Meshes should prepare themselves for shadow volumes. */
		//Bool getPrepareAllMeshesForShadowVolumes(void) 
		//	{return mPrepAllMeshesForShadowVolumes;}

		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static TetraMeshManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static TetraMeshManager* getSingletonPtr(void);


		/** Gets the factor by which the bounding box of an entity is padded.
		Default is 0.01
		*/
		float getBoundsPaddingFactor(void)
			{return mBoundsPaddingFactor;}

		/** Sets the factor by which the bounding box of an entity is padded
		*/
		void setBoundsPaddingFactor(float paddingFactor)
			{mBoundsPaddingFactor = paddingFactor;}

		/** Sets the listener used to control mesh loading through the serializer.
		*/
		void setListener(TetraMeshSerializerListener *listener);

		/** Gets the listener used to control mesh loading through the serializer.
		*/
		TetraMeshSerializerListener *getListener();

		/** @see ManualResourceLoader::loadResource */
		void loadResource(Resource* res);


	protected:

		/// @copydoc ResourceManager::createImpl
		Resource* createImpl(const String& name, ResourceHandle handle, 
			const String& group, bool isManual, ManualResourceLoader* loader, 
			const DataValuePairList* createParams);


		///** Utility method for tessellating 2D meshes.
		//*/
		//void tesselate2DMesh(Ogre::SubMesh* pSub, int meshWidth, int meshHeight, 
		//	bool doubleSided = false, 
		//	Ogre::HardwareBuffer::Usage indexBufferUsage = Ogre::HardwareBuffer::HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE,
		//	bool indexSysMem = false);



		bool mPrepAllMeshesForShadowVolumes;

		//the factor by which the bounding box of an entity is padded	
		Real mBoundsPaddingFactor;

		// The listener to pass to serializers
		TetraMeshSerializerListener* mListener;

	};


}; // namespace SimStep


#endif // SimStep_TetraMeshManager_h__