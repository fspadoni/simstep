#include "Core/Scheduler.h"

#include "Core/TaskManager.h"

namespace SimStep
{

	const float Scheduler::smDefaultClockFrequency = 1.0f / 120.0f;      // Set the timer to 120Hz


	//-----------------------------------------------------------------------
	template<> Scheduler* Singleton<Scheduler>::msSingleton = 0;
	

	Scheduler* Scheduler::getSingletonPtr(void)
	{
		return msSingleton;
	}

	Scheduler& Scheduler::getSingleton(void)
	{  
		assert( msSingleton );  return ( *msSingleton );  
	}




	Scheduler::Scheduler( 
		TaskManager* pTaskManager )
		: mTaskManager( pTaskManager )
		, mClockFrequency( smDefaultClockFrequency )
		//, m_hExecutionTimer( NULL )
		, mThreadingEnabled( ssTrue )
	{
		//m_hExecutionTimer = Singletons::PlatformManager.Timers().Create( m_ClockFrequency );

		mBenchmarkingEnabled = false;
			
	}


	Scheduler::~Scheduler(
		void
		)
	{
		//Singletons::PlatformManager.Timers().Destroy( m_hExecutionTimer );
	}


	void Scheduler::addTask( Task* pTask )
	{
		//
		// If we have a TaskManager, then we can thread things.
		//
		mThreadingEnabled = ( mTaskManager != NULL );

		//
		// Wait for any executing scenes to finish and clear out the list.
		//
		Task** aScenesToWaitFor = static_cast<Task**>(_malloca( mTaskExecs.size()*sizeof(Task) ) ); //[ System::Types::MAX ];
		
		uint cScenesToWaitFor = 0;

		for ( TaskExecsIt it=mTaskExecs.begin(); it != mTaskExecs.end(); it++ )
		{
			Task* pTask = it->second;

			//ASSERT( cScenesToWaitFor < System::Types::MAX );
			aScenesToWaitFor[ cScenesToWaitFor++ ] = pTask;
		}
		mTaskExecs.clear();

		if ( cScenesToWaitFor > 0 )
		{
			mTaskManager->WaitForSystemTasks( aScenesToWaitFor, cScenesToWaitFor );
		}

		//
		// Add the tasks.
		mTaskExecs[pTask->getTaskName()] = pTask;
		//


		_freea( *aScenesToWaitFor );
		//
		// Re-create the timer as a scene load may have taken a long time.
		//
		//Singletons::PlatformManager.Timers().Destroy( m_hExecutionTimer );
		//m_hExecutionTimer = Singletons::PlatformManager.Timers().Create( m_ClockFrequency );
	}


	void Scheduler::Execute( void )
	{
		//
		// Get the delta time; seconds since last Execute call.
		//
		float   DeltaTime = 0.0f;

		//DeltaTime = Singletons::PlatformManager.Timers().Wait( m_hExecutionTimer, !m_bBenchmarkingEnabled );

		//
		// Update instrumentation for this frame.
		// If we do this here, there's no thread sync to worry about since we're single-threaded here.
		//
		//Singletons::ServiceManager.Instrumentation().UpdatePeriodicData( DeltaTime );

		////
		//// Check if the execution is paused, and set delta time to 0 if so.
		////
		//if ( Singletons::EnvironmentManager.Runtime().GetStatus() ==
		//	IEnvironment::IRuntime::Status::Paused )
		//{
		//	DeltaTime = 0.0f;
		//}

		if ( mThreadingEnabled )
		{
			//
			// Schedule the scenes that are ready for execution.
			//
			Task** aScenesToExecute = static_cast<Task**>(_malloca( mTaskExecs.size()*sizeof(Task) ) );
			
			uint cScenesToExecute = 0;

			for ( TaskExecsIt it = mTaskExecs.begin(); it != mTaskExecs.end(); it++ )
			{
				//ASSERT( cScenesToExecute < System::Types::MAX );
				//Task* pTask = it->second;
				//aScenesToExecute[ cScenesToExecute++ ] = pTask->GetSystemTask();
				aScenesToExecute[ cScenesToExecute++ ] = it->second;
			}

			mTaskManager->IssueJobsForSystemTasks( aScenesToExecute, cScenesToExecute, DeltaTime );

			//
			// Wait for the scenes that will be completing execution in this frame.
			//
#if 0
			ISystemTask* aScenesToWaitFor[ System::Types::MAX ];
			u32 cScenesToWaitFor = 0;

			for ( SceneExecsIt it=m_SceneExecs.begin(); it != m_SceneExecs.end(); it++ )
			{
				ASSERT( cScenesToWaitFor < System::Types::MAX );
				ISystemScene* pSystemScene = it->second;
				aScenesToWaitFor[ cScenesToWaitFor++ ] = pSystemScene->GetSystemTask();
			}

			mTaskManager->WaitForSystemTasks( aScenesToWaitFor, cScenesToWaitFor );
#endif /* 0 */
			mTaskManager->WaitForSystemTasks( aScenesToExecute, cScenesToExecute );
		
			_freea( *aScenesToExecute );
		}
		else
		{
			for ( TaskExecsIt it=mTaskExecs.begin(); it != mTaskExecs.end(); it++ )
			{
				Task* pTask = it->second;
				//pSystemScene->GetSystemTask()->Update( DeltaTime );
				pTask->Update( DeltaTime );
			}
		}
	}


	
}; // namespace SimStep