#include "Core/SceneManager.h"

#include "Core/Core.h"
#include "Utilities/String.h"

#include "World/PhysicsWorld.h"
#include "World/RigidBody.h"
#include "Core/SoftBody.h"
#include "World/CollisionObject.h"
#include "World/Constraint.h"

#include "Utilities/PhysicsException.h"

#include "Core/SoftBodyFactory.h"
#include <Core/TetraMesh.h>
#include <Core/TetraMeshManager.h>
#include <Core/MaterialManager.h>

// aggiungere la s
#include "World/SoftBodyParam.h"

#include "Utilities/IteratorWrappers.h"

#include <algorithm>

namespace SimStep
{

	SceneManager::SceneManager(const String& name )
		: mSceneName(name), mCore( Core::getSingletonPtr() )
	{
	}

	SceneManager::~SceneManager()
	{
		destroyScene();

		// clear down physics object collection map
		{
			//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)
			for (PhysicsObjectCollectionMap::iterator i = mPhysicsObjectCollectionMap.begin();
				i != mPhysicsObjectCollectionMap.end(); ++i)
			{
				delete i->second;
				//OGRE_DELETE_T(i->second, PhysicsObjectCollection, MEMCATEGORY_SCENE_CONTROL);
				i->second = 0;
			}
			mPhysicsObjectCollectionMap.clear();
		}

	}

	void SceneManager::destroyScene(void)
	{


		for (CollisionObjects::iterator collObjIter = mCollisionObjects.begin();
			collObjIter != mCollisionObjects.end(); ++collObjIter )
		{
			//btCollisionWorld::removeCollisionObject( m_collisionObjects[0] );

			PhysicsWorldArrayIterator activeWorldsIter( mCore->getActivePhysicsWorlds() );

			while ( activeWorldsIter.hasMoreElements() )
			{
				activeWorldsIter.getNext()->removeCollisionObject( *collObjIter );
			}
			//for ( Core::PhysicsWorldList::iterator activeWorlds =  
			//	mCore->getActivePhysicsWorlds().begin();
			//	activeWorlds != mCore->getActivePhysicsWorlds().end(); 
			//++activeWorlds )
			//{
			//	(*activeWorlds)->removeCollisionObject( *collObjIter );
			//}
		}

		//for (CollisionObjects::iterator collObjIter = mCollisionObjects.begin();
		//	collObjIter != mCollisionObjects.end(); ++collObjIter )
		//{
		//	delete (*collObjIter);
		//	*collObjIter = 0;
		//}

		destroyAllCollisionObjects();
		destroyAllRigidBodies();
	}


	//SceneManager::CollisionObjectCollection* 
	//	SceneManager::getCollisionObjectCollection(const String& typeName)
	//{
	//	//// lock collection mutex
	//	//OGRE_LOCK_MUTEX(mCollisionObjectCollectionMapMutex)

	//	CollisionObjectCollectionMap::iterator i = 
	//		mCollisionObjectCollectionMap.find(typeName);

	//	if (i == mCollisionObjectCollectionMap.end())
	//	{
	//		// create
	//		CollisionObjectCollection* newCollection = new CollisionObjectCollection();
	//		mCollisionObjectCollectionMap[typeName] = newCollection;
	//		return newCollection;
	//	}
	//	else
	//	{
	//		return i->second;
	//	}
	//}


	//const SceneManager::CollisionObjectCollection* 
	//	SceneManager::getCollisionObjectCollection(const String& typeName) const
	//{
	//	//// lock collection mutex
	//	//OGRE_LOCK_MUTEX(mCollisionObjectCollectionMapMutex)

	//	CollisionObjectCollectionMap::const_iterator i = 
	//		mCollisionObjectCollectionMap.find(typeName);

	//	if (i == mCollisionObjectCollectionMap.end())
	//	{
	//		PHYSICS_EXCEPT( Exception::ERR_ITEM_NOT_FOUND, 
	//			"CollisionObjectCollection collection named '" + typeName + "' does not exist.", 
	//			"SceneManager::getCollisionObjectCollection");
	//	}
	//	else
	//	{
	//		return i->second;
	//	}
	//}


	SceneManager::PhysicsObjectCollection* 
		SceneManager::getPhysicsObjectCollection(const String& typeName)
	{
		//// lock collection mutex
		//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)

		PhysicsObjectCollectionMap::iterator i = 
			mPhysicsObjectCollectionMap.find(typeName);

		if (i == mPhysicsObjectCollectionMap.end())
		{
			// create
			PhysicsObjectCollection* newCollection = new PhysicsObjectCollection();
			mPhysicsObjectCollectionMap[typeName] = newCollection;
			return newCollection;
		}
		else
		{
			return i->second;
		}
	}


	const SceneManager::PhysicsObjectCollection* 
		SceneManager::getPhysicsObjectCollection(const String& typeName) const
	{
		//// lock collection mutex
		//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)

		PhysicsObjectCollectionMap::const_iterator i = 
			mPhysicsObjectCollectionMap.find(typeName);

		if (i == mPhysicsObjectCollectionMap.end())
		{
			PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, 
				"Physics Object collection named '" + typeName + "' does not exist.", 
				"SceneManager::getPhysicsObjectCollection");
		}
		else
		{
			return i->second;
		}
	}



	const String& SceneManager::getSceneName(void) const
	{
		return SceneManagerFactory::FACTORY_TYPE_NAME;
	}


	CollisionObject* SceneManager::createCollisionObject( const CollisionObjectParams& params )
	{
		const String typeName = "CollisionObject";
		// Check for duplicate names
		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(
			typeName );
		{
			//	//OGRE_LOCK_MUTEX(objectMap->mutex)
			if (objectMap->map.find(params.name) != objectMap->map.end())
			{
				PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
					"A Physics Object with name'" + params.name
					+ "' already exists in collection ." + typeName
					, "SceneManager::createCollisionObject");
			}
		}

		CollisionObject* collObject = new CollisionObject(params);
		mCollisionObjects.push_back(collObject);


		// notify active worlds
		for ( Core::PhysicsWorldList::iterator activeWorlds =  
			mCore->getActivePhysicsWorlds().begin();
			activeWorlds != mCore->getActivePhysicsWorlds().end(); 
			++activeWorlds )
			{
				(*activeWorlds)->addCollisionObject( collObject,
					params.collisionFilterGroup, params.collisionFilterMask );
			
			}
				
			objectMap->map[params.name] = collObject;
		

		return collObject;
	}

	void SceneManager::destroyCollisionObject(CollisionObject* collObj )
	{

		const String typeName = "CollisionObject";
		//CollisionObjectFactory* factory = 
		//	Root::getSingleton().getMovableObjectFactory(typeName);

		{
			PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);

			//OGRE_LOCK_MUTEX(objectMap->mutex)

			PhysicsObjectMap::iterator mi = objectMap->map.find(collObj->getName() );
			if (mi != objectMap->map.end())
			{
				//factory->destroyInstance(mi->second);
				//CollisionObject* collObj = mi->second;
				if ( collObj = mi->second )
				{
					PhysicsWorldArrayIterator activeWorldsIter( mCore->getActivePhysicsWorlds() );

					while ( activeWorldsIter.hasMoreElements() )
					{
						activeWorldsIter.getNext()->removeCollisionObject( collObj );
					}
					//for ( Core::PhysicsWorldList::iterator activeWorlds =  
					//	mCore->getActivePhysicsWorlds().begin();
					//	activeWorlds != mCore->getActivePhysicsWorlds().end(); 
					//++activeWorlds )
					//{
					//	(*activeWorlds)->removeCollisionObject( collObj );
					//}sio

					CollisionObjects::iterator collObjIter = 
						std::find(mCollisionObjects.begin(), mCollisionObjects.end(),
						collObj );
					mCollisionObjects.erase(collObjIter);
					
					objectMap->map.erase(mi);

					delete collObj;
					collObj = 0;
				}
				
			}
		}		
	}


	void SceneManager::destroyAllPhysicsObjectsByType(const String& typeName)
	{

		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);
		//MovableObjectFactory* factory = 
		//	Root::getSingleton().getPhysicsObjectFactory(typeName);

		{
			//OGRE_LOCK_MUTEX(objectMap->mutex)
			PhysicsObjectMap::iterator i = objectMap->map.begin();
			for (; i != objectMap->map.end(); ++i)
			{
				//// Only destroy our own
				//if (i->second->_getManager() == this)
				//{
				//	factory->destroyInstance(i->second);
				//}
				delete i->second;
				i->second = 0;
			}
			objectMap->map.clear();
		}
	}

	void SceneManager::destroyAllPhysicsObjects(void)
	{
		//// Lock collection mutex
		//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)

		PhysicsObjectCollectionMap::iterator ci = mPhysicsObjectCollectionMap.begin();

		for(;ci != mPhysicsObjectCollectionMap.end(); ++ci)
		{
			PhysicsObjectCollection* coll = ci->second;

			//// lock map mutex
			//OGRE_LOCK_MUTEX(coll->mutex)

			//if (Root::getSingleton().hasMovableObjectFactory(ci->first))
			{
				// Only destroy if we have a factory instance; otherwise must be injected
				//PhysicsObjectFactory* factory = 
				//	Root::getSingleton().getMovableObjectFactory(ci->first);
				PhysicsObjectMap::iterator i = coll->map.begin();
				for (; i != coll->map.end(); ++i)
				{
					//if (i->second->_getManager() == this)
					//{
					//	factory->destroyInstance(i->second);
					//}
					delete i->second;
					i->second = 0;
				}
			}
			coll->map.clear();
		}

	}

	void SceneManager::destroyAllCollisionObjects(void)
	{
		// Lock collection mutex
		//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)

		PhysicsObjectCollectionMap::iterator ci = mPhysicsObjectCollectionMap.begin();

		for(;ci != mPhysicsObjectCollectionMap.end(); ++ci)
		{
			PhysicsObjectCollection* coll = ci->second;

			// lock map mutex
			//OGRE_LOCK_MUTEX(coll->mutex)

			//if (Root::getSingleton().hasMovableObjectFactory(ci->first))
			{
				// Only destroy if we have a factory instance; otherwise must be injected
				//MovableObjectFactory* factory = 
				//	Root::getSingleton().getMovableObjectFactory(ci->first);
				PhysicsObjectMap::iterator i = coll->map.begin();
				for (; i != coll->map.end(); ++i)
				{
					//if (i->second->_getManager() == this)
					{
						//factory->destroyInstance(i->second);
						delete (i->second);
						i->second = 0;
					}
				}
			}
			coll->map.clear();
		}

		// clear down Collision Objects collection map
		{
			//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)
			for (PhysicsObjectCollectionMap::iterator i = mPhysicsObjectCollectionMap.begin();
				i != mPhysicsObjectCollectionMap.end(); ++i)
			{
				delete (i->second);
				i->second = 0;
			}
			mPhysicsObjectCollectionMap.clear();
		}
	}



	RigidBody* SceneManager::createRigidBody( const RigidBodyParams& params )
	{

		// Check for duplicate names
		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(
		"RigidBody"/*EntityFactory::FACTORY_TYPE_NAME*/ );
		{
			//	//OGRE_LOCK_MUTEX(objectMap->mutex)
			if (objectMap->map.find(params.name) != objectMap->map.end())
			{
				PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
					"A RigidBody with name'" + params.name
					+ "' already exists in collection ." + "RigidBody"
					, "SceneManager::createRigidBody");
			}
		}

		RigidBody* rigidbody = new RigidBody( params );
		
		mCollisionObjects.push_back( rigidbody->getCollisionObject() );
		mRigidBodies.push_back(rigidbody);
		
		// notify active worlds
		for ( Core::PhysicsWorldList::iterator activeWorlds =  
			mCore->getActivePhysicsWorlds().begin();
			activeWorlds != mCore->getActivePhysicsWorlds().end(); 
		++activeWorlds )
		{
			(*activeWorlds)->addCollisionObject( rigidbody->getCollisionObject(),
				params.collisionFilterGroup, 
				params.collisionFilterMask );

			(*activeWorlds)->addRigidBody( rigidbody );			

		}

		objectMap->map[params.name] = rigidbody;
		
		return rigidbody;
	}

	void SceneManager::destroyRigidBody(RigidBody* rigidObj )
	{

		if ( rigidObj == 0 )
		{
			return;
		}
		const String typeName = "RigidBody";
		//CollisionObjectFactory* factory = 
		//	Root::getSingleton().getMovableObjectFactory(typeName);

		CollisionObject* collObj = rigidObj->getCollisionObject();
		{
			PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);

			//OGRE_LOCK_MUTEX(objectMap->mutex)

			PhysicsObjectMap::iterator mi = objectMap->map.find(rigidObj->getName() );
			if (mi != objectMap->map.end())
			{
				//factory->destroyInstance(mi->second);
				//CollisionObject* collObj = mi->second;
				if (  collObj == mi->second )
				{
					PhysicsWorldArrayIterator activeWorldsIter( mCore->getActivePhysicsWorlds() );

					while ( activeWorldsIter.hasMoreElements() )
					{
						//activeWorldsIter.peekNext()->removeRigidBody( rigidObj );
						activeWorldsIter.peekNext()->removeRigidBody( rigidObj );

						activeWorldsIter.getNext()->removeCollisionObject( 
							rigidObj->getCollisionObject() );
					}
					//for ( Core::PhysicsWorldList::iterator activeWorlds =  
					//	mCore->getActivePhysicsWorlds().begin();
					//	activeWorlds != mCore->getActivePhysicsWorlds().end(); 
					//++activeWorlds )
					//{
					//	(*activeWorlds)->removeCollisionObject( collObj );
					//}sio

					CollisionObjects::iterator collObjIter = 
						std::find(mCollisionObjects.begin(), mCollisionObjects.end(),
						collObj );
					mCollisionObjects.erase(collObjIter);
				
					RigidBodies::iterator rigidObjIter = 
						std::find(mRigidBodies.begin(), mRigidBodies.end(),
						rigidObj );
					mRigidBodies.erase(rigidObjIter);
					
					objectMap->map.erase(mi);

					delete rigidObj;
					rigidObj = 0;
				}
				
			}
		}		
	}

	void SceneManager::destroyAllRigidBodies(void)
	{
		// Lock collection mutex
		//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)

		PhysicsObjectCollectionMap::iterator ci = mPhysicsObjectCollectionMap.begin();

		for(;ci != mPhysicsObjectCollectionMap.end(); ++ci)
		{
			PhysicsObjectCollection* coll = ci->second;

			// lock map mutex
			//OGRE_LOCK_MUTEX(coll->mutex)

			//if (Root::getSingleton().hasMovableObjectFactory(ci->first))
			{
				// Only destroy if we have a factory instance; otherwise must be injected
				//MovableObjectFactory* factory = 
				//	Root::getSingleton().getMovableObjectFactory(ci->first);
				PhysicsObjectMap::iterator i = coll->map.begin();
				for (; i != coll->map.end(); ++i)
				{
					//if (i->second->_getManager() == this)
					{
						//factory->destroyInstance(i->second);
						delete (i->second);
						i->second = 0;
					}
				}
			}
			coll->map.clear();
		}

		// clear down Collision Objects collection map
		{
			//OGRE_LOCK_MUTEX(mMovableObjectCollectionMapMutex)
			for (PhysicsObjectCollectionMap::iterator i = mPhysicsObjectCollectionMap.begin();
				i != mPhysicsObjectCollectionMap.end(); ++i)
			{
				delete (i->second);
				i->second = 0;
			}
			mPhysicsObjectCollectionMap.clear();
		}
	}


	SoftBody* SceneManager::createSoftBody( const String& typeName, 
		const SoftBodyParams& params  )
	{

		SoftBodyFactory* factory = 
			Core::getSingleton().getSoftBodyFactory(typeName);

		// Check for duplicate names
		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);

		{
			//	//OGRE_LOCK_MUTEX(objectMap->mutex)

			if (objectMap->map.find(params.name) != objectMap->map.end())
			{
				PHYSICS_EXCEPT(Exception::ERR_DUPLICATE_ITEM, 
					"A SoftBody of type '" + typeName 
					+ "' with name '" + params.name	+ "' already exists.", 
					"SceneManager::createSoftBody");
			}

			//String groupName = Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME;
			String groupName = ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
			if ( params.resourceGroup != 0 )
			{
				groupName = params.resourceGroup;
			}

			// load tetra mesh
			TetraMeshPtr pMesh;
			if (params.MeshFile != 0)
			{
				
				if ( params.MeshFile != 0)
				{
					// Get mesh (load if required)
					pMesh = TetraMeshManager::getSingleton().load(
						params.MeshFile,
						// autodetect group location
						groupName );
				}

			}
			// load material
			MaterialPtr pMaterial;
			if (params.MaterialName != 0)
			{
				// Get mesh (load if required)
				pMaterial = MaterialManager::getSingleton().load(
					params.MaterialName,
					// autodetect group location
					groupName );
			}

			if (pMesh.isNull())
			{
				PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
					"'tetramesh' parameter required when constructing a SoftBody.",
					"SceneManager::createSoftBody");
			}


			DataValuePairList otherparams;
			otherparams["Mesh"] = String(params.MeshFile);
			otherparams["Material"] = String(params.MaterialName);
			//otherparams["resourceGroup"] = String(params.resourceGroup);

			params.tetraMesh = pMesh;
			params.material = pMaterial;

			SoftBody* softbody = factory->createInstance( params, &otherparams);

			mCollisionObjects.push_back( static_cast<CollisionObject*>(softbody) );


			mSoftBodies.push_back(softbody);

			// notify active worlds
			for ( Core::PhysicsWorldList::iterator activeWorlds =  
				mCore->getActivePhysicsWorlds().begin();
				activeWorlds != mCore->getActivePhysicsWorlds().end(); 
			++activeWorlds )
			{
				(*activeWorlds)->addCollisionObject( 
					static_cast<CollisionObject*>(softbody),
					params.collisionFilterGroup, 
					params.collisionFilterMask );

				(*activeWorlds)->addSoftBody( softbody );	
			}
			objectMap->map[params.name] = static_cast<CollisionObject*>(softbody);


			return softbody;

		}

	}


	void SceneManager::destroySoftBody(SoftBody* softbody )
	{

		const String& typeName = softbody->getType();
		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);
		SoftBodyFactory* factory = 
			Core::getSingleton().getSoftBodyFactory(typeName);

		CollisionObject* collObj = static_cast<CollisionObject*>(softbody);
		{
			//OGRE_LOCK_MUTEX(objectMap->mutex)
			PhysicsObjectMap::iterator mi = objectMap->map.find(softbody->getName() );
			if (mi != objectMap->map.end())
			{
				// Only destroy our own
				//if (i->second->_getManager() == this)
				if (  collObj == mi->second )
				{
	
					PhysicsWorldArrayIterator activeWorldsIter( mCore->getActivePhysicsWorlds() );

					while ( activeWorldsIter.hasMoreElements() )
					{
						//activeWorldsIter.peekNext()->removeRigidBody( rigidObj );
						activeWorldsIter.getNext()->removeSoftBody( softbody );
					}

					CollisionObjects::iterator collObjIter = 
						std::find(mCollisionObjects.begin(), mCollisionObjects.end(),
						collObj );
					mCollisionObjects.erase(collObjIter);

					SoftBodies::iterator softbodyIter = 
						std::find(mSoftBodies.begin(), mSoftBodies.end(),
						softbody );
					mSoftBodies.erase(softbodyIter);

					factory->destroyInstance(dynamic_cast<SoftBody*>(mi->second) );

					objectMap->map.erase(mi);

				}

			}

		}
	}


	void SceneManager::destroyAllSoftBodies(void)
	{

	}

	void SceneManager::destroyAllSoftBodiesByType(const String& typeName)
	{

		PhysicsObjectCollection* objectMap = getPhysicsObjectCollection(typeName);
		SoftBodyFactory* factory = 
			Core::getSingleton().getSoftBodyFactory(typeName);

		{
			//OGRE_LOCK_MUTEX(objectMap->mutex)
			PhysicsObjectMap::iterator i = objectMap->map.begin();
			for (; i != objectMap->map.end(); ++i)
			{
				// Only destroy our own
				//if (i->second->_getManager() == this)
				{
					factory->destroyInstance( dynamic_cast<SoftBody*>(i->second) );
				}
			}
			objectMap->map.clear();
		}
	}



	Constraint* SceneManager::createConstraint( const ConstraintParams& params )
	{
		Constraint* constr = new Constraint(params);
		mConstraints.push_back( constr );
		return constr;
	}


	const String SceneManagerFactory::FACTORY_TYPE_NAME = "SimplePhysicsSceneManager";
	

	SceneManagerFactory::SceneManagerFactory(void)
	{
		initSceneInfo();
	}

	const SceneManagerInfo& SceneManagerFactory::getSceneInfo() const 
	{
		return mSceneInfo;
	}

	const NsPhysicsSceneType& SceneManagerFactory::getSceneType() const 
	{
		return mSceneInfo.mSceneTypeMask;
	}


	void SceneManagerFactory::initSceneInfo(void) const
	{
		mSceneInfo.mTypeName = FACTORY_TYPE_NAME;
		mSceneInfo.mDescription = "Simple SimStep Scene Manager";
		mSceneInfo.mSceneTypeMask = NS_SINGLE_CORE; // | NS_RIGID_BODY | NS_SOFT_BODY;
		//mMetaData.worldGeometrySupported = false;
	}


	SceneManager* SceneManagerFactory::createInstance(
		const String& instanceName) const
	{
		return new SceneManager(instanceName);
	}
	//-----------------------------------------------------------------------
	void SceneManagerFactory::destroyInstance(SceneManager* instance) const
	{
		delete instance;
		instance = 0;
	}

	
}; // namespace SimStep