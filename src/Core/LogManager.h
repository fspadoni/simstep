#ifndef LOG_MANAGER_H
#define LOG_MANAGER_H


#include "Utilities/Singleton.h"
//#include "Memory/AlignedObject.h"
#include "Utilities/Common.h"

#include "Utilities/Log.h"

//STL
#include <map>

namespace SimStep 
{


	class _SimStepExport LogManager : public Singleton<LogManager>
	{
	public:
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( LogManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


	protected:
		typedef std::map<String, Log*, std::less<String> >	LogList;

		/// A list of all the logs the manager can access
		LogList m_Logs;

		/// The default log to which output is done
		Log* m_DefaultLog;

	public:
		
		//PHYSICS_AUTO_MUTEX // public to allow external locking

		LogManager();
		~LogManager();

		/** Creates a new log with the given name.
		@param
		name The name to give the log e.g. 'SimStep.log'
		@param
		defaultLog If true, this is the default log output will be
		sent to if the generic logging methods on this class are
		used. The first log created is always the default log unless
		this parameter is set.
		@param
		debuggerOutput If true, output to this log will also be
		routed to the debugger's output window.
		@param
		suppressFileOutput If true, this is a logical rather than a physical
		log and no file output will be written. If you do this you should
		register a LogListener so log output is not lost.
		*/
		Log* createLog( const String& name, bool defaultLog = false, bool debuggerOutput = true, 
			bool suppressFileOutput = false);

		/** Retrieves a log managed by this class.
		*/
		Log* getLog( const String& name);

		/** Returns a pointer to the default log.
		*/
		Log* getDefaultLog();

		/** Closes and removes a named log. */
		void destroyLog(const String& name);
		/** Closes and removes a log. */
		void destroyLog(Log* log);

		/** Sets the passed in log as the default log.
		@returns The previous default log.
		*/
		Log* setDefaultLog(Log* newLog);

		/** Log a message to the default log.
		*/
		void logMessage( const String& message, LogMessageLevel lml = LML_NORMAL, 
			bool maskDebug = false);

		/** Log a message to the default log (signature for backward compatibility).
		*/
		void logMessage( LogMessageLevel lml, const String& message,  
			bool maskDebug = false) { logMessage(message, lml, maskDebug); }

		/** Get a stream on the default log. */
		Log::Stream stream(LogMessageLevel lml = LML_NORMAL, 
			bool maskDebug = false);

		/** Sets the level of detail of the default log.
		*/
		void setLogDetail(LoggingLevel ll);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static LogManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static LogManager* getSingletonPtr(void);

	};

};


#endif