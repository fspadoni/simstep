#ifndef SimStep_TaskManagerTbb_h__
#define SimStep_TaskManagerTbb_h__

#include "Core/TaskManager.h"



// intel TBB header
#include "tbb/task.h"
#include "tbb/task_scheduler_init.h"
//#include "tbb/tbb_thread.h"


//#if defined(USE_THREAD_PROFILER)
//#include "IttNotify.h"
//#endif

#include <vector>

namespace SimStep
{


	class TaskManagerTbb: public TaskManager/*, Singleton<TaskManagerTbb>*/
	{
	public:
		
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( TaskManagerTbb, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


		TaskManagerTbb() {};
		~TaskManagerTbb(){};

		void Init( void );

		void Shutdown( void );

		
		void IssueJobsForSystemTasks( Task** pTasks, uint uCount, float fDeltaTime );

	
		virtual void NonStandardPerThreadCallback( JobFunction pfnCallback, void* pData );

		
		virtual size_t GetRecommendedJobCount( TaskManager::JobCountInstructionHints Hints );


		//virtual void ParallelFor( ISystemTask* pSystemTask,
		//	ParallelForFunction pfnJobFunction, void* pParam, u32 begin, u32 end, u32 minGrainSize = 1 );

		/// <summary cref="TaskManagerTbb::WaitForSystemTasks">
		/// Call this from the primary thread to wait until specified tasks spawned with <c>IssueJobsForSystemTasks</c>
		/// and all of their subtasks are complete.
		/// </summary>
		/// <param name="pTasks">an array of <c>ISystemTask</c> objects</param>
		/// <param name="uCount">the length of the <paramref name="pTasks"/> array</param>
		/// <seealso cref="TaskManagerTbb::IssueJobsForSystemTasks"/>

		void WaitForSystemTasks( Task** pTasks, uint uCount );

	
		uint GetNumberOfThreads( void );

		void SetNumberOfThreads( uint uNumberOfThreads );


		Bool IsPrimaryThread( void );

		void AddStallTask( void );

		static void SystemTaskCallback( void* pData );


	private:

		//tbb::tbb_thread::id m_uPrimaryThreadID;

		void* m_hStallPoolSemaphore;
		tbb::task* m_pStallPoolParent;
		//SpinWait m_tSynchronizedCallbackMutex;

		tbb::task_scheduler_init* m_pTbbScheduler;

#if USE_MAIN_THREAD_FOR_SERIAL_TASKS_ONLY
		// IMPLEMENTATION NOTE
		// Experiments showed that temporary oversubscription during the execution 
		// of primary thread bound system tasks is even beneficial rather than adverse.
		//
		// Therefore the TBB pool is created with 1 extra thread, and the master thread 
		// after completing primary thread bound tasks is blocked on the Windows 
		// synchronization primitive instead instead of entering wait_for_all method
		// (to prevent oversubscription during the execution of the main bulk of work)
		// 
		// Doing so requires two "root" tasks instead of 1.
		tbb::empty_task* m_pRootTask;
#endif
		tbb::task* m_pSystemTasksRoot;

		Bool m_bTimeToQuit;

		// requested, maximum and current number of threads in use
		uint m_uRequestedNumberOfThreads;
		uint m_uMaxNumberOfThreads;
		uint m_uNumberOfThreads;
		uint m_uTargetNumberOfThreads;

#if defined(USE_THREAD_PROFILER)
		BOOL m_bTPEventsForTasks;
		BOOL m_bTPEventsForJobs;
		BOOL m_bTPEventsForSynchronize;

		__itt_event m_tSynchronizeTPEvent;
		__itt_event m_tpSystemTaskSpawn;

		/// <summary cref="TaskManagerTbb::SystemTaskSupport">
		/// This class is used by <c>TaskManagerTbb</c> to keep <c>SystemTasks</c> associated with <c>ISystemTasks</c>.
		/// </summary>
		/// <seealso cref="TaskManagerTbb::SystemTask"/>
		struct SystemTaskSupport
		{
			SystemTaskSupport()
				: m_bInitialized( False )
				, m_tpeSystemTask( 0 )
				, m_tpeSystemTaskJob( 0 )
				, m_tpeSystemTaskJobCompletion( 0 )
			{}

			Bool m_bInitialized;
			__itt_event m_tpeSystemTask;
			__itt_event m_tpeSystemTaskJob;
			__itt_event m_tpeSystemTaskJobCompletion;
		};

		std::map<ISystemTask*, SystemTaskSupport> m_SupportForSystemTasks;
#endif /* USE_THREAD_PROFILER */


		typedef std::vector<Task*> TasksList;

		// Holds system tasks that should be run on the primary thread
		TasksList m_primaryThreadSystemTaskList;
		TasksList m_tmpTaskList;

		float m_fDeltaTime;

		typedef std::vector<tbb::task::affinity_id> AffinityIDsList;
		AffinityIDsList m_affinityIDs;

		//SpinWait m_spinMutex;

		static void InitAffinityData( void* mgr );

#if defined(USE_THREAD_PROFILER)
		/// <summary cref="TaskManagerTbb::GetSupportForSystemTask">
		/// This method returns the associated <c>SystemTaskSupport</c> for the <c>ISystemTask</c>.
		/// </summary>
		/// <param name="pTask">an <c>ISystemTask</c>
		/// <returns>the associated <c>SystemTaskSupport</c> object
		SystemTaskSupport& GetSupportForSystemTask( ISystemTask* pTask );
#endif /* USE_THREAD_PROFILER */

		/// <summary cref="TaskManagerTbb::IsTBBThread">
		/// This method is used to determine if the calling thread is an Intel Threading Building Blocks thread.
		/// </summary>
		/// <remarks>Due to the difficulty using the Intel Threading Building Blocks API to determine this, and
		/// since no thread local storage is used as a workaround, this method always returns true.</remarks>
		/// <returns>true if the calling thread is managed by Intel Theading Building Blocks, false otherwise</returns>
		static Bool IsTBBThread( void );

		/// <summary cref="TaskManagerTbb::UpdateThreadPoolSize">
		/// This method is called within <c>IssueJobsForSystemTasks</c> to update the size of TBB thread pool
		/// if necessary.  The thread pool is resized by a call to <c>SetNumberOfThreads</c>.
		/// </summary>
		/// <seealso cref="TaskManagerTbb::SetNumberOfThreads"/>
		/// <seealso cref="TaskManagerTbb::IssueJobsForSystemTasks"/>
		void UpdateThreadPoolSize( void ) {};



		///** Override standard Singleton retrieval.
		//@remarks
		//Why do we do this? Well, it's because the Singleton
		//implementation is in a .h file, which means it gets compiled
		//into anybody who includes it. This is needed for the
		//Singleton template to work, but we actually only want it
		//compiled into the implementation of the class based on the
		//Singleton, not all of them. If we don't change this, we get
		//link errors when trying to use the Singleton-based class from
		//an outside dll.
		//@par
		//This method just delegates to the template version anyway,
		//but the implementation stays in this single compilation unit,
		//preventing link errors.
		//*/
		//static TaskManagerTbb& getSingleton(void);

		///** Override standard Singleton retrieval.
		//@remarks
		//Why do we do this? Well, it's because the Singleton
		//implementation is in a .h file, which means it gets compiled
		//into anybody who includes it. This is needed for the
		//Singleton template to work, but we actually only want it
		//compiled into the implementation of the class based on the
		//Singleton, not all of them. If we don't change this, we get
		//link errors when trying to use the Singleton-based class from
		//an outside dll.
		//@par
		//This method just delegates to the template version anyway,
		//but the implementation stays in this single compilation unit,
		//preventing link errors.
		//*/
		//static TaskManagerTbb* getSingletonPtr(void);

	};


	
}; // namespace SimStep

#endif // SimStep_TaskManagerTbb_h__