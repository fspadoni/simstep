#include "Core/LogManager.h"


namespace SimStep 
{

	//-----------------------------------------------------------------------
	template<> LogManager* Singleton<LogManager>::msSingleton = 0;
	
	LogManager* LogManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	LogManager& LogManager::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}

	//-----------------------------------------------------------------------
	LogManager::LogManager()
	{
		m_DefaultLog = NULL;
	}
	//-----------------------------------------------------------------------
	LogManager::~LogManager()
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		
		// Destroy all logs
		LogList::iterator i;
		for (i = m_Logs.begin(); i != m_Logs.end(); ++i)
		{
			deleteSimStep (i->second);
			//m_Logs.erase(i);//range.first, range.second);
		}
		//m_Logs.erase( m_Logs.begin(), m_Logs.end() );
	}

	//-----------------------------------------------------------------------
	Log* LogManager::createLog( const String& name, bool defaultLog, bool debuggerOutput, 
		bool suppressFileOutput)
	{
		//PHYSICS_LOCK_AUTO_MUTEX

		Log* newLog = newSimStep Log(name, debuggerOutput, suppressFileOutput);

		if( !m_DefaultLog || defaultLog )
		{
			m_DefaultLog = newLog;
		}

		m_Logs.insert( LogList::value_type( name, newLog ) );

		return newLog;
	}
	//-----------------------------------------------------------------------
	Log* LogManager::getDefaultLog()
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		return m_DefaultLog;
	}
	//-----------------------------------------------------------------------
	Log* LogManager::setDefaultLog(Log* newLog)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		Log* oldLog = m_DefaultLog;
		m_DefaultLog = newLog;
		return oldLog;
	}
	//-----------------------------------------------------------------------
	Log* LogManager::getLog( const String& name)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		LogList::iterator i = m_Logs.find(name);
		if (i != m_Logs.end() )
		{
			return i->second;
		}
		else
		{
			//PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS, "Log not found. ", "LogManager::getLog");
		}

	}
	//-----------------------------------------------------------------------
	void LogManager::destroyLog(const String& name)
	{
		LogList::iterator i = m_Logs.find(name);
		if (i != m_Logs.end())
		{
			if (m_DefaultLog == i->second)
			{
				delete i->second;
				m_DefaultLog = 0;
			}
			
			m_Logs.erase(i);
		}

		// Set another default log if this one removed
		if (!m_DefaultLog && !m_Logs.empty())
		{
			m_DefaultLog = m_Logs.begin()->second;
		}
	}
	//-----------------------------------------------------------------------
	void LogManager::destroyLog(Log* log)
	{
		destroyLog(log->getName());
	}
	//-----------------------------------------------------------------------
	void LogManager::logMessage( const String& message, LogMessageLevel lml, bool maskDebug)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		if (m_DefaultLog)
		{
			m_DefaultLog->logMessage(message, lml, maskDebug);
		}
	}
	//-----------------------------------------------------------------------
	void LogManager::setLogDetail(LoggingLevel ll)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		if (m_DefaultLog)
		{
			m_DefaultLog->setLogDetail(ll);
		}
	}
	//---------------------------------------------------------------------
	Log::Stream LogManager::stream(LogMessageLevel lml, bool maskDebug)
	{
		//PHYSICS_LOCK_AUTO_MUTEX
		if (m_DefaultLog)
		{
			return m_DefaultLog->stream(lml, maskDebug);
		}
		else
		{
			//PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS, "Default log not found. ", "LogManager::stream");
		}
	}

};