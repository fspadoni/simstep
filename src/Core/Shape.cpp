#include <Core/Shape.h>

#include <Core/ResourceManager.h>
#include <Core/ResourceGroupManager.h>
#include <Core/LogManager.h>

//#include <Serializers/ShapeSerializer.h>
#include <Serializers/XmlShapeSerializer.h>

#include <Utilities/Bounds.h>


namespace SimStep
{


	ShapePtr::ShapePtr(const ResourcePtr& r) 
		: SharedPtr<Shape>()
	{
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
			//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
			pRep = static_cast<Shape*>(r.getPointer());
			//pUseCount = pRep ? newSimStepAlloc(unsigned int, MEMCATEGORY_GENERAL)(1) : 0;
			useFreeMethod = SPFM_DELETE;
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
	}


	ShapePtr& ShapePtr::operator=(const ResourcePtr& r)
	{
		if (pRep == static_cast<Shape*>(r.getPointer()))
			return *this;
		release();
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		if(true)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
			//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
			pRep = static_cast<Shape*>(r.getPointer());
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
		else
		{
			// RHS must be a null pointer
			assert(r.isNull() && "RHS must be null if it has no mutex!");
			setNull();
		}
		return *this;
	}

	void ShapePtr::destroy(void)
	{
		// We're only overriding so that we can destroy after full definition of Shape
		SharedPtr<Shape>::destroy();
	}




	Shape::Shape(ResourceManager* creator, const String& name, 
		ResourceHandle handle, const String& group, bool isManual /* = false */, 
		ManualResourceLoader* loader /* = 0 */)
		: Resource(creator, name, handle, group, isManual, loader)
		, mPoints(0)
		, mShapeType(NoShape)
	{

	}

	
	Shape::~Shape()
	{
		// have to call this here reather than in Resource destructor
		// since calling virtual methods in base destructors causes crash
		Resource::unload();
	}



	void Shape::postLoadImpl(void)
	{
	}

	void Shape::prepareImpl()
	{
		// Load from specified 'name'
		if ( getCreator()->getVerbose())
			LogManager::getSingleton().logMessage("Shape: Loading "+mName+".");

		mFreshFromDisk =
			ResourceGroupManager::getSingleton().openResource(
			mName, mGroup, true, this);

		// fully prebuffer into host RAM
		mFreshFromDisk = DataStreamPtr( newSimStep MemoryDataStream(mName,mFreshFromDisk));
	}

	void Shape::unprepareImpl()
	{
		mFreshFromDisk.setNull();
	}

	void Shape::loadImpl()
	{

		XmlShapeSerializer serializer;
		//serializer.setListener(ShapeManager::getSingleton().getListener());

		// If the only copy is local on the stack, it will be cleaned
		// up reliably in case of exceptions, etc
		DataStreamPtr data(mFreshFromDisk);
		mFreshFromDisk.setNull();

		if (data.isNull()) {
			PHYSICS_EXCEPT(Exception::ERR_INVALID_STATE,
				"Data doesn't appear to have been prepared in " + mName,
				"Shape::loadImpl()");
		}

		serializer.import( data->getAsString() , this);

	}


	void Shape::unloadImpl()
	{

	}



	const Bounds3& Shape::getBounds(void) const
	{
		return Bounds3();
	}

	size_t Shape::calculateSize(void) const
	{

		size_t ret = 0;

		return ret;
	}



}; // namespace SimStep