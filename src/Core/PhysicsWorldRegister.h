#ifndef SimStep_PhysicsWorldRegister_h__
#define SimStep_PhysicsWorldRegister_h__


#include "Utilities/Singleton.h"
#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

#include "Core/PhysicsClasses.h"

#include "World/PhysicsWorld.h"
#include "World/PhysicsWorldParams.h"

#include "Utilities/String.h"

#include <map>
#include <vector>
#include <list>

namespace Ogre
{
	 class RenderSystem;
}; // namespace Ogre


namespace SimStep
{
	//class PhysicsWorldFactory;



	class _SimStepExport PhysicsWorldFactory
	{
	public:

		/** Create a new instance of a SceneManager.
		@remarks
		Don't call directly, use SceneManagerEnumerator::createSceneManager.
		*/
		virtual PhysicsWorld* createInstance( 
			const PhysicsWorldParam& worldparam ) const = 0;

		/** Destroy an instance of a SceneManager. */
		virtual void destroyInstance(PhysicsWorld* instance) const;

	protected:
		/// Internal method to initialise the metadata
		virtual void initPhysicsWorldInfo(void) const = 0;


	public:
		PhysicsWorldFactory();

		virtual ~PhysicsWorldFactory() {};


		const PhysicsWorldInfo& getPhysicsWorldInfo(void) const;

		const PhysicsWorldType& getPhysicsWorldType(void) const;


	protected:

		mutable Bool mPhysicsWorldInfoInit;

		mutable PhysicsWorldInfo mPhysicsWorldInfo;

		friend class PhysicsWorldRegister;
	};


	////forward decl
	//class Core;

	class _SimStepExport PhysicsWorldRegister : public Singleton<PhysicsWorldRegister> 
		//, public GeneralAllocatedObject
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( PhysicsWorldRegister, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )

		/// Physics  World instances, indexed by instance name
		typedef std::map<String, PhysicsWorld*> Instances;

		/// List of available Physics World types as meta data
		typedef std::vector<const PhysicsWorldInfo*> PhysicsWorldInfoList;

	private:

		/// Physics World factories
		typedef std::list<PhysicsWorldFactory*> Factories;

		//  Physics Scene Factory and Instance
		Factories mFactories;
		Instances mInstances;


		/// Stored separately to allow iteration
		PhysicsWorldInfoList mPhysicsWorldInfoList;
		
		///// Factory for default Physics World
		//DefaultPhysicsWorldFactory mDefaultFactory;
		
		/// Count of creations for auto-naming
		unsigned long mInstanceCreateCount;

		/// Currently assigned render system
		Ogre::RenderSystem* mCurrentRenderSystem;


	public:

		PhysicsWorldRegister(void);
		~PhysicsWorldRegister();
		//PhysicsWorldRegister( const Core* core );
		//~PhysicsWorldRegister();

		/** Register a new SceneManagerFactory. 
		@remarks
		Plugins should call this to register as new SceneManager providers.
		*/
		void addFactory( PhysicsWorldFactory* fact);

		/** Remove a SceneManagerFactory. 
		*/
		void removeFactory( PhysicsWorldFactory* fact);

		/** Get more information about a given type of SceneManager.
		@remarks
		The metadata returned tells you a few things about a given type 
		of SceneManager, which can be created using a factory that has been
		registered already. 
		@param typeName The type name of the SceneManager you want to enquire on.
		If you don't know the typeName already, you can iterate over the 
		metadata for all types using getMetaDataIterator.
		*/
		const PhysicsWorldInfo* getPhysicsWorldInfo(const String& typeName) const;

		typedef PhysicsWorldInfoList::const_iterator PhysicsWorldInfoIterator;
		/** Iterate over all types of SceneManager available for construction, 
		providing some information about each one.
		*/
		PhysicsWorldInfoIterator getPhysicsWorldInfoIterator(void) const;


		/** Create a PhysicsWorld instance of a given type.
		@remarks
		You can use this method to create a PhysicsWorld instance of a 
		given specific type. You may know this type already, or you may
		have discovered it by looking at the results from getMetaDataIterator.
		@note
		This method throws an exception if the named type is not found.
		@param typeName String identifying a unique SceneManager type
		@param instanceName Optional name to given the new instance that is
		created. If you leave this blank, an auto name will be assigned.
		*/
		PhysicsWorld* createPhysicsWorld(const String& typeName, 
			const PhysicsWorldParam& worldparam );

		/** Create a PhysicsWorld instance based on scene type support.
		@remarks
		Creates an instance of a PhysicsWorld which supports the scene types
		identified in the parameter. If more than one type of SceneManager 
		has been registered as handling that combination of scene types, 
		in instance of the last one registered is returned.
		@note This method always succeeds, if a specific scene manager is not
		found, the default implementation is always returned.
		@param typeMask A mask containing one or more PhysicsWorldType flags
		@param instanceName Optional name to given the new instance that is
		created. If you leave this blank, an auto name will be assigned.
		*/
		PhysicsWorld* createPhysicsWorld(const PhysicsWorldType& info, 
			const PhysicsWorldParam& worldparam );

		/** Destroy an instance of a PhysicsWorld. */
		void destroyPhysicsWorld(PhysicsWorld* sm);

		/** Get an existing SceneManager instance that has already been created,
		identified by the instance name.
		@param instanceName The name of the instance to retrieve.
		*/
		PhysicsWorld* getPhysicsWorld(const String& instanceName) const;

		//typedef MapIterator<Instances> PhysicsWorldIterator;
		typedef Instances::const_iterator PhysicsWorldIterator;
		/** Get an iterator over all the existing PhysicsWorld instances. */
		PhysicsWorldIterator getPhysicsWorldIterator(void) const;


		/** Notifies all PhysicsWorlds of the destination rendering system.
		*/
		void setRenderSystem(Ogre::RenderSystem* rs);

		void setSceneManager( SceneManager* world );


		/// Utility method to control shutdown of the managers
		void shutdownAll(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static PhysicsWorldRegister& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static PhysicsWorldRegister* getSingletonPtr(void);


		//const Core* mCore;

		////virtual
		//const Core* getCore() const 
		//{ return mCore; }


	};

}; // namespace SimStep


#endif // SimStep_PhysicsWorldRegister_h__