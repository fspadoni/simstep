#ifndef SimStep_StatisticsManager_h__
#define SimStep_StatisticsManager_h__


#include "Utilities/Singleton.h"
#include "Core/PhysicsClasses.h"

#include "Utilities/Definitions.h"

namespace SimStep
{

	class StatisticsManager : public Singleton<StatisticsManager> //, public GeneralAllocatedObject
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( StatisticsManager, Memory::SIMD_ALIGN, MEMCATEGORY_GENERAL )


		StatisticsManager();
		~StatisticsManager();


		void update( const StepEvent& dtime );

		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static StatisticsManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static StatisticsManager* getSingletonPtr(void);

	};

	
}; // namespace SimStep

#endif // SimStep_StatisticsManager_h__