#ifndef PHYSICS_VERSION_H
#define PHYSICS_VERSION_H

namespace SimStep
{

	// Define ogre version

	enum eVersion
	{
		Major = 0	<< 16,
		Minor = 1	<< 8,
		Patch = 1	<< 0
	};

	const String VersionName = "Test";

	const unsigned int PhysicsVersion = static_cast<unsigned int>
		(eVersion::Major | eVersion::Minor | eVersion::Patch );

}



#endif