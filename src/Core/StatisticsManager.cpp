#include "Core/StatisticsManager.h"

#include "Core/StepListener.h"

namespace SimStep
{


	template<> StatisticsManager* Singleton<StatisticsManager>::msSingleton = 0;

	StatisticsManager* StatisticsManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	StatisticsManager& StatisticsManager::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}

	//-----------------------------------------------------------------------


	StatisticsManager::StatisticsManager()
	{

	}

	StatisticsManager::~StatisticsManager()
	{

	}

	
	void StatisticsManager::update( const StepEvent& dtime )
	{

	}


}; // namespace SimStep