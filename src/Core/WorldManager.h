#ifndef PHYSICS_WORLD_MANAGER_H
#define PHYSICS_WORLD_MANAGER_H


#include "Utilities/Singleton.h"
//#include "Utilities/AlignedObject.h"
#include "Utilities/Common.h"

#include "Core/PhysicsClasses.h"


namespace SimStep
{

	////forward decl
	//class Core;

	class _SimStepExport WorldManager // : public Singleton<WorldManager> , public AlignedObject16
	{

	public:
		WorldManager( const Core* core );
		~WorldManager();




		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static WorldManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static WorldManager* getSingletonPtr(void);


		const Core* mCore;

		//virtual
		const Core* getCore() const 
		{ return mCore; }


	};

}; // namespace SimStep

#endif