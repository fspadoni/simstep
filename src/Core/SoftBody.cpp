#include "Core/SoftBody.h"

#include "World/SoftBodyParam.h"


#include "Utilities/StopWatch.h"



#include "World/CollisionObject.h"



namespace SimStep
{

	SoftBody::SoftBody( const SoftBodyParams& params, 
		const DataValuePairList* otherparams )
		: CollisionSoftBody(params, otherparams )
	{

		mMaterial = params.material;

		mNbNodes = 0;
		mNbElements = 0;
		mNbTriangles = 0;

		mNbConstraintNodes = 0;

		mMass = 0;

		m_Gravity = params.Gravity;
	}
	

	SoftBody::~SoftBody()
	{

	}


	const SoftBody* SoftBody::downcast(const CollisionObject* colObj)
	{
		if (colObj->getInternalType()==btCollisionObject::CO_SOFT_BODY)
		{
			return static_cast<const SoftBody*>(colObj);
		}
		return 0;
	}

	SoftBody* SoftBody::downcast(CollisionObject* colObj)
	{
		if (colObj->getInternalType()==btCollisionObject::CO_SOFT_BODY)
		{
			return static_cast<SoftBody*>(colObj);
		}
		return 0;
	}


	float SoftBody::simulate( const float& TimeStep, void* user_pointer  )
	{
		tick_count starTicks = tick_count::now();


		assembleStiffnessMatrix( user_pointer );

		solve( TimeStep, user_pointer );


		return  ( tick_count::now() - starTicks ).seconds();
	}


	void SoftBody::updateCollisionShape(void)
	{
		CollisionSoftBody::updateCollisionShape();
	}

	
	void SoftBody::setPosition(const Vector3& newpos )
	{
		CollisionSoftBody::setPosition(newpos);
	}


	void SoftBody::setOrientation(const Quat& newor )
	{
		CollisionSoftBody::setOrientation(newor);
	}

	void SoftBody::setVelocity(const Vector3& newvel )
	{

	}

	void SoftBody::reset(void)
	{

		mNodesVelocity->reset();
		mExternForces->reset();
		//mForces->reset();
		CollisionSoftBody::reset();
	}

	float SoftBody::getVertexMass(int vertex) const
	{
		return mMass / mNbVertices;
	}


}; // namespace SimStep