#ifndef SimStep_StepListener_h__
#define SimStep_StepListener_h__


//#include "OgreFrameListener.h"

namespace SimStep
{

	/** Struct containing information about a frame event.
	*/
	struct StepEvent //: public Ogre::FrameEvent
	{
		/** Elapsed time in seconds since the last event.
		This gives you time between frame start & frame end,
		and between frame end and next frame start.
		@remarks
		This may not be the elapsed time but the average
		elapsed time between recently fired events.
		*/
		float timeSinceLastEvent;
		/** Elapsed time in seconds since the last event of the same type,
		i.e. time for a complete frame.
		@remarks
		This may not be the elapsed time but the average
		elapsed time between recently fired events of the same type.
		*/
		float timeSinceLastFrame;
	};


	class StepListener //: public Ogre::FrameListener
	{
		/*
		Note that this could have been an abstract class, but I made
		the explicit choice not to do this, because I wanted to give
		people the option of only implementing the methods they wanted,
		rather than having to create 'do nothing' implementations for
		those they weren't interested in. As such this class follows
		the 'Adapter' classes in Java rather than pure interfaces.
		*/
	public:
		/** Called when a frame is about to begin rendering.
		@remarks
		This event happens before any render targets have begun updating. 
		@return
		True to go ahead, false to abort rendering and drop
		out of the rendering loop.
		*/
		virtual bool stepStarted(const StepEvent& evt)
		{ (void)evt; return true; }

		/** Called after all render targets have had their rendering commands 
		issued, but before render windows have been asked to flip their 
		buffers over.
		@remarks
		The usefulness of this event comes from the fact that rendering 
		commands are queued for the GPU to process. These can take a little
		while to finish, and so while that is happening the CPU can be doing
		useful things. Once the request to 'flip buffers' happens, the thread
		requesting it will block until the GPU is ready, which can waste CPU
		cycles. Therefore, it is often a good idea to use this callback to 
		perform per-frame processing. Of course because the frame's rendering
		commands have already been issued, any changes you make will only
		take effect from the next frame, but in most cases that's not noticeable.
		@return
		True to continue rendering, false to drop out of the rendering loop.
		*/
		virtual bool stapeRenderingQueued(const StepEvent& evt)
		{ (void)evt; return true; }

		/** Called just after a frame has been rendered.
		@remarks
		This event happens after all render targets have been fully updated
		and the buffers switched.
		@return
		True to continue with the next frame, false to drop
		out of the rendering loop.
		*/
		virtual bool stepEnded(const StepEvent& evt)
		{ (void)evt; return true; }

		virtual ~StepListener() {}
	};
	
}; // namespace SimStep

#endif // SimStep_StepListener_h__