#include "Core/TaskManagerTbb.h"
//#include <assert.h>

//#include "tbb/tbb_thread.h"

#include "Core/Scheduler.h"

namespace SimStep
{

	////-----------------------------------------------------------------------
	//template<> TaskManagerTbb* Singleton<TaskManagerTbb>::msSingleton = 0;
	//

	//TaskManagerTbb* TaskManagerTbb::getSingletonPtr(void)
	//{
	//	return msSingleton;
	//}

	//TaskManagerTbb& TaskManagerTbb::getSingleton(void)
	//{  
	//	assert( msSingleton );  return ( *msSingleton );  
	//}

	
	void TaskManagerTbb::Init( void )
	{
		//// Call this from the primary thread before calling any other TaskManagerTBB methods.
		//g_pTaskManagerTBB = this;

		//tbb::tbb_thread::id m_uPrimaryThreadID = tbb::this_tbb_thread::get_id();

		m_bTimeToQuit = ssFalse;
		//m_uRequestedNumberOfThreads = Singletons::EnvironmentManager.Variables().GetAsInt( "TaskManager::Threads", 0 );
		
		if( m_uRequestedNumberOfThreads == 0 )
		{
			// IMPLEMENTATION NOTE
			// The audio thread (which Thread Profiler shows as constantly working) 
			// has no negative impact on the performance when it causes apparent 
			// oversubscription. Shrinking the pool to avoid this apparent oversubscription
			// results in smaller FPS. So this is probably one of the cases when TP
			// misinterprets the behavior of threads created by some of the Windows 
			// subsystems (DirectX is historically its weak place).
			//
			m_uRequestedNumberOfThreads = tbb::task_scheduler_init::default_num_threads();
		}

		m_uMaxNumberOfThreads = 0;
		m_uNumberOfThreads = 0;
		m_uTargetNumberOfThreads = 0;

		m_pStallPoolParent = NULL;
		//m_hStallPoolSemaphore = CreateSemaphore( NULL, 0, m_uRequestedNumberOfThreads, NULL );
		//SynchronizeTask::m_hAllCallbacksInvokedEvent = CreateEvent( NULL, True, False, NULL );

#if defined(USE_THREAD_PROFILER)
		m_bTPEventsForTasks = Singletons::EnvironmentManager.Variables().GetAsBool( "TaskManagerTBB::TPEventsForTasks", False );
		m_bTPEventsForJobs = Singletons::EnvironmentManager.Variables().GetAsBool( "TaskManagerTBB::TPEventsForJobs", False );
		m_bTPEventsForSynchronize = Singletons::EnvironmentManager.Variables().GetAsBool( "TaskManagerTBB::TPEventsForSynchronize", False );

		if( m_bTPEventsForSynchronize )
		{
			m_tSynchronizeTPEvent = __itt_event_createA( "Synchronize", 11 );
		}

		m_tpSystemTaskSpawn = __itt_event_createA( "SystemTaskSpawn", 15 );
#endif /* USE_THREAD_PROFILER */

		m_uMaxNumberOfThreads = m_uRequestedNumberOfThreads;
		m_uTargetNumberOfThreads = m_uRequestedNumberOfThreads;
		m_uNumberOfThreads = m_uRequestedNumberOfThreads;

		m_pTbbScheduler = new tbb::task_scheduler_init( m_uRequestedNumberOfThreads );
		m_pSystemTasksRoot = new( tbb::task::allocate_root() ) tbb::empty_task;

		NonStandardPerThreadCallback( InitAffinityData, this );

		//// Cache the thread count for display.
		//Singletons::Instrumentation.setActiveThreadCount( m_uTargetNumberOfThreads );
	}


	void TaskManagerTbb::Shutdown( void )
	{

		// Call this from the primary thread as the last TaskManagerTBB call.
		//assert( IsPrimaryThread() );

		// get the callback thread to exit
		m_bTimeToQuit = ssTrue;

		// trigger the release of the stall pool
		//ReleaseSemaphore( m_hStallPoolSemaphore, m_uMaxNumberOfThreads, NULL );

		m_pSystemTasksRoot->destroy( *m_pSystemTasksRoot );

		delete m_pTbbScheduler;

		// now get rid of all the events
		//CloseHandle( m_hStallPoolSemaphore );
		m_hStallPoolSemaphore = NULL;
		//CloseHandle( SynchronizeTask::m_hAllCallbacksInvokedEvent );

#ifdef USE_THREAD_PROFILER
		m_SupportForSystemTasks.clear();
#endif
	}


	void TaskManagerTbb::IssueJobsForSystemTasks( Task** pTasks, uint uTaskCount, float fDeltaTime )
	{

		// Call this from the primary thread to schedule system work.
		( IsPrimaryThread() );

		//__ITT_EVENT_START( m_tpSystemTaskSpawn, PROFILE_TASKMANAGER );

		//assert( uTaskCount > 0 );

		m_fDeltaTime = fDeltaTime;

		UpdateThreadPoolSize();

		//assert( m_pSystemTasksRoot != NULL );

		// We'll be adding tasks to the m_pSystemTasksRoot, and we will eventually call wait_for_all on it.
		// Support the eventual wait_for_all by setting reference count to 1 now.
		m_pSystemTasksRoot->set_ref_count( 1 );

		// now schedule the tasks, based upon their PerformanceHint order
		tbb::task_list tTaskList;
		uint uAffinityCount = (uint)m_affinityIDs.size();

		uint h = 0;
		uint i = 0;
		//for( h = 0; h < Task_MAX; h++ )
		{
			for( i = 0; i < uTaskCount; i++ )
			{
				if( pTasks[i]->IsPrimaryThreadOnly() )
				{
					// put this task on the list of tasks to be run on the primary thread
					// only do this during the first outer loop
					if( h == 0)
					{
						m_primaryThreadSystemTaskList.push_back( pTasks[i] );
					}
				}
				else
				{
					// see if it's time to dispatch this task
					//if( GetPerformanceHint( pTasks[i] ) == (PerformanceHint)h )
					{
						//// this task can be run on an arbitrary thread -- allocate it 
						//Task *pTask = new( m_pSystemTasksRoot->allocate_additional_child_of( *m_pSystemTasksRoot ) )
						//	Task( SystemTaskCallback, pTasks[i]	);

						//// affinity will increase the chances that each SystemTask will be assigned
						//// to a unique thread, regardless of PerformanceHint
						//ASSERT( pTask != NULL );
						//pTask->set_affinity( m_affinityIDs[i % uAffinityCount] );
						//tTaskList.push_back( *pTask );
					}
				}
			}
		}

		// We only spawn system tasks here. They in their turn will spawn descendant tasks.
		// Waiting for the whole bunch completion happens in WaitForSystemTasks.
		m_pSystemTasksRoot->spawn( tTaskList );

		//__ITT_EVENT_END( m_tpSystemTaskSpawn, PROFILE_TASKMANAGER );
	}

	void TaskManagerTbb::NonStandardPerThreadCallback( JobFunction pfnCallback, void* pData )
	{

	}

	uint TaskManagerTbb::GetRecommendedJobCount( TaskManager::JobCountInstructionHints Hints )
	{

		return 0;
	}

	//virtual void ParallelFor( ISystemTask* pSystemTask,
	//	ParallelForFunction pfnJobFunction, void* pParam, u32 begin, u32 end, u32 minGrainSize = 1 );

	/// <summary cref="TaskManagerTbb::WaitForSystemTasks">
	/// Call this from the primary thread to wait until specified tasks spawned with <c>IssueJobsForSystemTasks</c>
	/// and all of their subtasks are complete.
	/// </summary>
	/// <param name="pTasks">an array of <c>ISystemTask</c> objects</param>
	/// <param name="uCount">the length of the <paramref name="pTasks"/> array</param>
	/// <seealso cref="TaskManagerTbb::IssueJobsForSystemTasks"/>

	void TaskManagerTbb::WaitForSystemTasks( Task** pTasks, size_t uCount )
	{

	}

	size_t TaskManagerTbb::GetNumberOfThreads( void )
	{

		return 0;
	}

	void TaskManagerTbb::SetNumberOfThreads( size_t uNumberOfThreads )
	{

	}

	Bool TaskManagerTbb::IsPrimaryThread( void )
	{
		return ssFalse;
	}

	void TaskManagerTbb::AddStallTask( void )
	{

	}

	void TaskManagerTbb::SystemTaskCallback( void* pData )
	{
		//Task* pTask = static_cast<Task*>( pData );
		//pTask->Update( this->m_fDeltaTime );
	}


	void TaskManagerTbb::InitAffinityData( void* mgr )
	{
		TaskManagerTbb* pThis = static_cast<TaskManagerTbb*>( mgr );

		//SpinWait::Lock lock( pThis->m_spinMutex );
		//pThis->m_affinityIDs.push_back( tbb::task::self().affinity() );
	}


}; // namespace SimStep