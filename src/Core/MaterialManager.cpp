#include <Core/MaterialManager.h>

#include <Core/ResourceGroupManager.h>

#include <Utilities/PhysicsException.h>
#include <Serializers/XmlMaterialSerializer.h>


namespace SimStep
{

	template<> MaterialManager* Singleton<MaterialManager>::msSingleton = 0;
	MaterialManager* MaterialManager::getSingletonPtr(void)
	{
		return msSingleton;
	}
	MaterialManager& MaterialManager::getSingleton(void)
	{
		assert( msSingleton );  return ( *msSingleton );
	}


	MaterialManager::MaterialManager()
	{
		
		// Scripting is supported by this manager
		mScriptPatterns.push_back("*.physicsmat");
		mScriptPatterns.push_back("*.physicsmat.xml");
		ResourceGroupManager::getSingleton()._registerScriptLoader(this);

		// Resource type
		mResourceType = "PhysicsMaterial";

		// Register with resource group manager
		ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);

		//ResourceGroupManager::getSingleton().createResourceGroup("SimStepMaterials");
		//ResourceGroupManager::getSingleton().addResourceLocation(
		//	"../Media/materials/physics", "FileSystem", "SimStepMaterials", false );
		//ResourceGroupManager::getSingleton().addResourceLocation(
		//	"../../Media/materials/physics", "FileSystem", "SimStepMaterials", false );
		//ResourceGroupManager::getSingleton().initialiseResourceGroup("SimStepMaterials");
	}

	
	MaterialManager::~MaterialManager()
	{
		// Resources cleared by superclass
		// Unregister with resource group manager
		ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
		ResourceGroupManager::getSingleton()._unregisterScriptLoader(this);
	}
	
	Resource* MaterialManager::createImpl(const String& name, ResourceHandle handle,
		const String& group, bool isManual, ManualResourceLoader* loader,
		const DataValuePairList* params)
	{
		return newSimStep Material(this, name, handle, group, isManual, loader);
	}
	

	MaterialPtr MaterialManager::createManual( const String& name, const String& groupName, 
		ManualResourceLoader* loader )
	{
		// Don't try to get existing, create should fail if already exists
		return create(name, groupName, true, loader);
	}

	void MaterialManager::loadResource( Resource* res)
	{
		Material* material = static_cast<Material*>(res);
	}


	void MaterialManager::initialise(void)
	{
		// Set up a lit base white material
		create("DefaultMaterial", "SimStepMaterials" );

	}
	
	void MaterialManager::parseScript( DataStreamPtr& stream, const String& groupName)
	{

		XmlMaterialSerializer serializer;

		DataStreamPtr data(stream);
		stream.setNull();

		if (data.isNull()) {
			PHYSICS_EXCEPT(Exception::ERR_INVALID_STATE,
				"Data doesn't appear to have been prepared in " + groupName,
				"MaterialManager::parseScript()");
		}

		serializer.importMaterials(data->getAsString(), groupName );

	}


	
}; // namespace SimStep