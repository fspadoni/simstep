#ifndef SimStep_DynamicsWorld_h__
#define SimStep_DynamicsWorld_h__

#include "Utilities/Common.h"
#include "Utilities/Definitions.h"

#include "ode/ode.h"
#include <World/RigidBody.h>

#include "Math/Math.h"

#include <vector>

namespace SimStep
{

	class SceneManager;


	align16_struct _SimStepExport DynamicsWorldParams
	{
		float timeStep;
		Vector3 gravity;
		size_t solverNbIteration;
		float solverRelaxationValue;
		float solverERP;
		float solverCFM;
		float linearDamping;
		float angularDamping;
		float linearDampingThreshold;
		float angularDampingThreshold;
		float linearDampingThresholdDampingScale;
		float angularDampingThresholdDampingScale;
		float maxAngularSpeed;
		// ???? 
		float contactMaxCorrectingVel;

		Bool parallelizeDynamics;

		DynamicsWorldParams();
	};



	class _SimStepExport DynamicsWorld : public dWorld
	{
	public:

		//SIMSTEP_DECLARE_CLASS_ALLOCATOR(DynamicsWorld, MEMCATEGORY_ROOT );

		DynamicsWorld(const DynamicsWorldParams& dynamicsworldparams );

		~DynamicsWorld();
	
		inline const dWorld& getOdeWorld() const
		{
			return static_cast<const dWorld&>(const_cast<const DynamicsWorld&>(*this) );
		}
	
		Vector3 getGravity(void) const;


		float step( const float& timeStep, SceneManager* scene = 0, void* spawnedTask = NULL );
	

		void add(RigidBody* body);

		Bool remove( RigidBody* body );


		dBody** getRigidBodyArray(void) const;

		std::vector<dBody*>& getRigidBodyVector(void) const;



	protected:

		void initializeOdeWorld(void);

		float mTimeStep;

		DynamicsWorldParams mDynamicsWorldParams;

		Bool mParallelizeDynamics;

		//
		dJointGroupID mRigidBodyRigidBodyContactGroup;
		dJointGroupID mSoftBodyRigidBodyContactGroup;
		dJointGroupID mSoftBodySoftBodyContactGroup;


		//std::vector<dBody*> RigidBodyArray;
		//std::vector<dBody*>::iterator RigidBodyIterator;
		std::vector<RigidBody*> RigidBodyArray;
		std::vector<RigidBody*>::iterator RigidBodyIterator;

	};

	
}; // namespace SimStep

#endif // SimStep_DynamicsWorld_h__