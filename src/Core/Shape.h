#ifndef SimStep_Shape_h__
#define SimStep_Shape_h__

#include <Utilities/Common.h>

#include <Core/Resource.h>
#include <Memory/MemoryManager.h>
#include <Utilities/SimdMemoryBuffer.h>
#include <Utilities/SharedPointer.h>
#include <Math/Math.h>
#include <Utilities/DataStream.h>

#include <Core/PhysicsClasses.h>

namespace SimStep
{

	class _SimStepExport Shape : public Resource
	{

		friend class XmlShapeSerializer;

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Shape, Memory::CACHE_ALIGN, MEMCATEGORY_GEOMETRY );


		Shape( ResourceManager* creator, const String& name, ResourceHandle handle,
			const String& group, bool isManual = false, ManualResourceLoader* loader = 0);

		virtual ~Shape();


		enum Type
		{
			NoShape,
			PolyLine,
			MaxCount
		};

		
		const Type getType(void) const {return mShapeType;}

		void setType(Shape::Type shapetype) { mShapeType = shapetype;}

		const Bounds3& getBounds(void) const;

	
	protected:

		void prepareImpl(void);
		/** Destroys data cached by prepareImpl.
		*/
		void unprepareImpl(void);
		/// @copydoc Resource::loadImpl
		void loadImpl(void);
		/// @copydoc Resource::postLoadImpl
		void postLoadImpl(void);
		/// @copydoc Resource::unloadImpl
		void unloadImpl(void);
		/// @copydoc Resource::calculateSize
		size_t calculateSize(void) const;


	public:

		BufferSharedPtr<Vector3> mPoints;


	private:

		

		Type mShapeType;

		DataStreamPtr mFreshFromDisk;

	};

	


	class _SimStepExport ShapePtr : public SharedPtr<Shape> 
	{
	public:
		ShapePtr() : SharedPtr<Shape>() {}

		explicit ShapePtr(Shape* rep) : SharedPtr<Shape>(rep) {}

		ShapePtr(const ShapePtr& r) : SharedPtr<Shape>(r) {} 

		ShapePtr(const ResourcePtr& r);

		/// Operator used to convert a ResourcePtr to a MeshPtr
		ShapePtr& operator=(const ResourcePtr& r);

	protected:
		/// Override destroy since we need to delete Mesh after fully defined
		void destroy(void);
	};



}; // namespace SimStep

#endif // SimStep_Shape_h__