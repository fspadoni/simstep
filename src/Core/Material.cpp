#include <Core/Material.h>

#include <Core/MaterialManager.h>
#include "Utilities/String.h"

namespace SimStep
{

	Material::Material( ResourceManager* creator, const String& name, ResourceHandle handle,
		const String& group, bool isManual, ManualResourceLoader* loader)
		: Resource(creator, name, handle, group, isManual, loader)
		, mDensity(1.0) // [Kg/m^3]
		, mYoungModulus(1e6) // [N/m^2]
		, mPoissonRatio(0.33) // []
		, mFriction(0.5)
		, mToughness(0.0)
		, mYield(0.0)
		, mMaxYield(0.0)
		, mCreep(0.0)
	{

	}

	Material::~Material()
	{
		// have to call this here reather than in Resource destructor
		// since calling virtual methods in base destructors causes crash
		Resource::unload();
	}
	

	Material& Material::operator=( const Material& rhs )
	{
		mName = rhs.mName;
		mGroup = rhs.mGroup;
		mCreator = rhs.mCreator;
		mIsManual = rhs.mIsManual;
		mLoader = rhs.mLoader;
		mHandle = rhs.mHandle;
		mSize = rhs.mSize;
		mLoadingState = rhs.mLoadingState;
		mIsBackgroundLoaded = rhs.mIsBackgroundLoaded;

		mDensity = rhs.getDensity();
		mYoungModulus = rhs.getYoungModulus();
		mPoissonRatio = rhs.getPoissonRatio();
		mFriction = rhs.getFriction();
		mToughness = rhs.getToughness();
		mYield = rhs.getYield();
		mMaxYield = rhs.getMaxYield();
		mCreep = rhs.getCreep();

		return *this;
	}


	MaterialPtr Material::clone(const String& newName, bool changeGroup, 
		const String& newGroup) const
	{
		MaterialPtr newMat;
		if (changeGroup)
		{
			newMat = MaterialManager::getSingleton().create(newName, newGroup);
		}
		else
		{
			newMat = MaterialManager::getSingleton().create(newName, mGroup);
		}

		// Keep handle (see below, copy overrides everything)
		ResourceHandle newHandle = newMat->getHandle();
		// Assign values from this
		*newMat = *this;
		// Restore new group if required, will have been overridden by operator
		if (changeGroup)
		{
			newMat->mGroup = newGroup;
		}

		// Correct the name & handle, they get copied too
		newMat->mName = newName;
		newMat->mHandle = newHandle;

		return newMat;
	}


	void Material::prepareImpl(void)
	{

	}

	void Material::unprepareImpl(void)
	{

	}

	void Material::loadImpl(void)
	{

	}

	void Material::postLoadImpl(void)
	{

	}

	void Material::unloadImpl(void)
	{

	}

	size_t Material::calculateSize(void) const
	{
		return 0;
	}




	MaterialPtr::MaterialPtr(const ResourcePtr& r) : SharedPtr<Material>()
	{
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
				//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
				pRep = static_cast<Material*>(r.getPointer());
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
	}

	/// Operator used to convert a ResourcePtr to a MaterialPtr
	MaterialPtr& MaterialPtr::operator=(const ResourcePtr& r)
	{
		if (pRep == static_cast<Material*>(r.getPointer()))
			return *this;
		release();
		// lock & copy other mutex pointer
		//OGRE_MUTEX_CONDITIONAL(r.OGRE_AUTO_MUTEX_NAME)
		if (true)
		{
			//OGRE_LOCK_MUTEX(*r.OGRE_AUTO_MUTEX_NAME)
				//OGRE_COPY_AUTO_SHARED_MUTEX(r.OGRE_AUTO_MUTEX_NAME)
				pRep = static_cast<Material*>(r.getPointer());
			pUseCount = r.useCountPointer();
			if (pUseCount)
			{
				++(*pUseCount);
			}
		}
		else
		{
			// RHS must be a null pointer
			assert(r.isNull() && "RHS must be null if it has no mutex!");
			setNull();
		}
		return *this;
	}


}; // namespace SimStep