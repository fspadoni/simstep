#ifndef FEM_DYNAMICS_H
#define FEM_DYNAMICS_H




#include "Utilities/Definitions.h"

#include "Math/Math.h"

#include "World/PhysicsWorld.h"

#include "World/PhysicsWorldTBB.h"

#include "World/RigidBody.h"

#include "FemSoftBody/FemSoftBody.h"

// template class
#include "SphSoftBody/SphSoftBody.inl"

// dependencies

// bullet ver. 2.72
#include "btBulletCollisionCommon.h"

// ode ver. 0.10.1
#include "ode/ode.h"

#include "FemSoftBody/FemElements.h"

//#include "Utilities/SoftBodyGL.h"

//
//#include "Core/PhysicsClasses.h"
#include "Core/Core.h"


#endif
