#ifndef PHYSICS_SOFTBODY_COLLISION_INFO
#define PHYSICS_SOFTBODY_COLLISION_INFO


#include "btBulletCollisionCommon.h"
#include "btSparseSDF.h"

namespace SimStep
{

	// collision info
	align16_struct	SoftBodyCollisionWorldInfo
	{
		btBroadphaseInterface*	m_broadphase;
		btDispatcher*	m_dispatcher;
		btSparseSdf<3>			m_sparsesdf;

		~SoftBodyCollisionWorldInfo() {}
	};	

}; // SimStep


#endif