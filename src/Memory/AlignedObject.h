#ifndef ALIGNED_OBJECT_16_H
#define ALIGNED_OBJECT_16_H

#include <malloc.h>
#include <crtdbg.h>

#include "Utilities/definitions.h"
#include "Utilities/Common.h"

//#define _CRTDBG_MAP_ALLOC



namespace SimStep {


#define declare_allocator() \
	static inline void* operator new(size_t sz, const char* file, int line, const char* func) {return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, __FILE__, __LINE__ );} \
	static inline void* operator new(size_t sz)	{return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN,__FILE__, __LINE__);} \
	static inline void* operator new(size_t sz, void* ptr){return ptr;} \
	static inline void* operator new[] ( size_t sz, const char* file, int line, const char* func ){return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, file, line );} \
	static inline void* operator new[] ( size_t sz )	{return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, __FILE__, __LINE__ );} \
	static inline void operator delete( void* ptr ){	_aligned_free_dbg(ptr);} \
	static inline void operator delete( void* ptr, void* ){_aligned_free_dbg(ptr);} \
	static inline void operator delete( void* ptr, const char* , int , const char*  )	{_aligned_free_dbg(ptr);} \
	static inline void operator delete[] ( void* ptr ){	_aligned_free_dbg(ptr);} \
	static inline void operator delete[] ( void* ptr, const char* , int , const char*  ){	_aligned_free_dbg(ptr);	} \


//class AlignedObject16
//{
//
//public:
//
//	explicit AlignedObject16()
//	{ }
//
//	~AlignedObject16()
//	{ }
//#ifndef _DEBUG
//
//	inline void* operator new(size_t size)
//	{
//		return _aligned_malloc(size, Memory::SIMD_ALIGN);
//	}
//
//	inline void operator delete(void *v)
//	{
//		_aligned_free(v); 
//		v = 0;
//	}
//
//	inline void* operator new[](size_t size)
//	{
//		return _aligned_malloc(size, Memory::SIMD_ALIGN); 
//	}
//
//	inline void operator delete[](void *v)
//	{
//		_aligned_free(v); 
//		v = 0;
//	}
//
//	inline void* operator new(size_t, void *_Where)
//	{	// construct array with placement at _Where
//		return (_Where);
//	}
//
//	inline void operator delete(void *, void *) {}
//
//#else
//
//	// be sure the compiler inlines 'operator new' in debug
//	
//	static inline void* operator new(size_t size)
//	{
//		return _aligned_malloc_dbg(size, Memory::SIMD_ALIGN,__FILE__, __LINE__);
//	}
//
//	static inline void operator delete(void *v)
//	{
//		_aligned_free_dbg(v); 
//		v = 0;
//	}
//
//	static inline void* operator new[](size_t size)
//	{
//		return _aligned_malloc_dbg(size, Memory::SIMD_ALIGN,__FILE__, __LINE__);
//	}
//
//	static inline void operator delete[](void *v)
//	{
//		_aligned_free_dbg(v); 
//		v = 0;
//	}
//
//	static inline void* operator new(size_t, void *_Where)
//	{	// construct array with placement at _Where
//		return (_Where);
//	}
//
//	static inline void operator delete(void *, void *) 
//	{}
//
//	///// operator new, with debug line info
//	//void* operator new(size_t sz, const char* file, int line, const char* func)
//	//{
//	//	return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, file, line );
//	//}
//
//	//void* operator new(size_t sz)
//	//{
//	//	return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN,__FILE__, __LINE__);
//	//}
//
//	///// placement operator new
//	//void* operator new(size_t sz, void* ptr)
//	//{
//	//	return ptr;
//	//}
//
//	///// array operator new, with debug line info
//	//void* operator new[] ( size_t sz, const char* file, int line, const char* func )
//	//{
//	//	return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, file, line );
//	//}
//
//	//void* operator new[] ( size_t sz )
//	//{
//	//	return _aligned_malloc_dbg(sz, Memory::SIMD_ALIGN, __FILE__, __LINE__ );
//	//}
//
//	//void operator delete( void* ptr )
//	//{
//	//	_aligned_free_dbg(ptr);
//	//}
//
//	//// Corresponding operator for placement delete (second param same as the first)
//	//void operator delete( void* ptr, void* )
//	//{
//	//	_aligned_free_dbg(ptr);
//	//}
//
//	//// only called if there is an exception in corresponding 'new'
//	//void operator delete( void* ptr, const char* , int , const char*  )
//	//{
//	//	_aligned_free_dbg(ptr);
//	//}
//
//	//void operator delete[] ( void* ptr )
//	//{
//	//	_aligned_free_dbg(ptr);
//	//}
//
//
//	//void operator delete[] ( void* ptr, const char* , int , const char*  )
//	//{
//	//	_aligned_free_dbg(ptr);
//	//}
//
//#endif
//
//};

//
//template class AlignedObject<typename AlignmentBound>
//{
//
//public:
//
//#ifndef _DEBUG
//
//	inline void* operator new(size_t size)
//	{
//		return _aligned_malloc(size, AlignmentBound);
//	}
//
//	inline void operator delete(void *v)
//	{
//		_aligned_free(v); 
//		v = 0;
//	}
//
//	inline void* operator new[](size_t size)
//	{
//		return _aligned_malloc(size, AlignmentBound); 
//	}
//
//	inline void operator delete[](void *v)
//	{
//		_aligned_free(v); 
//		v = 0;
//	}
//
//	inline void* operator new(size_t, void *_Where)
//	{	// construct array with placement at _Where
//		return (_Where);
//	}
//
//	inline void operator delete(void *, void *) {}
//
//#else
//
//	// be sure the compiler inlines 'operator new' in debug
//
//	static inline void* operator new(size_t size)
//	{
//		return _aligned_malloc_dbg(size, AlignmentBound,__FILE__, __LINE__);
//	}
//
//	static inline void operator delete(void *v)
//	{
//		_aligned_free_dbg(v); 
//		v = 0;
//	}
//
//	static inline void* operator new[](size_t size)
//	{
//		return _aligned_malloc_dbg(size, AlignmentBound,__FILE__, __LINE__);
//	}
//
//	static inline void operator delete[](void *v)
//	{
//		_aligned_free_dbg(v); 
//		v = 0;
//	}
//
//	static inline void* operator new(size_t, void *_Where)
//	{	// construct array with placement at _Where
//		return (_Where);
//	}
//
//	static inline void operator delete(void *, void *) 
//	{}
//
//
//#endif
//};

}; // namespace SimStep

#endif