#include "Memory/MemoryTracker.h"


#include <iostream>
#include <fstream>

#include <assert.h>

namespace SimStep
{
	// protected ctor
	MemoryTracker::MemoryTracker()
		: mLeakFileName("SimStepMemoryReport.log"), mDumpToStdOut(true),
		mTotalAllocations(0) ,mWriteAllocReport(ssTrue)
	{
		mAllocReportStream  << "Memory Report" << std::endl << std::endl << 
			"Allocation" << std::endl;
		mAllocReportStream  << "address:" << "\t" << "size:" << "\t" << "pool:"
			<< "\t\t" << "function:" << "\t\t\t" <<  "file:" << "\t\t\t\t" << "line:"
			<< std::endl;
	}
	//--------------------------------------------------------------------------
	MemoryTracker& MemoryTracker::get()
	{
		static MemoryTracker tracker;
		return tracker;
	}
	//--------------------------------------------------------------------------
	void MemoryTracker::_recordAlloc(void* ptr, size_t sz, unsigned int pool, 
		const char* file, size_t ln, const char* func)
	{
		//OGRE_s_AUTO_MUTEX

			assert(mAllocations.find(ptr) == mAllocations.end() && "Double allocation with same address - "
			"this probably means you have a mismatched allocation / deallocation style, "
			"check if you're are using OGRE_ALLOC_T / OGRE_FREE and OGRE_NEW_T / OGRE_DELETE_T consistently");

		mAllocations[ptr] = Alloc(sz, pool, file, ln, func);
		if(pool >= mAllocationsByPool.size())
			mAllocationsByPool.resize(pool+1, 0);
		mAllocationsByPool[pool] += sz;
		mTotalAllocations += sz;

		if ( mWriteAllocReport  )
		{
			mAllocReportStream  << "[0x" << ptr << "]" << "\t" << sz  << "\t" << pool 
				<< "\t" << file  << "\t" << ln << "\t" <<  func << std::endl;
	
		}
	}
	//--------------------------------------------------------------------------
	void MemoryTracker::_recordDealloc(void* ptr)
	{
		// deal cleanly with null pointers
		if (!ptr)
			return;

		//OGRE_LOCK_AUTO_MUTEX

		AllocationMap::iterator i = mAllocations.find(ptr);
		assert(i != mAllocations.end() && "Unable to locate allocation unit - "
			"this probably means you have a mismatched allocation / deallocation style, "
			"check if you're are using OGRE_ALLOC_T / OGRE_FREE and OGRE_NEW_T / OGRE_DELETE_T consistently");
		// update category stats
		mAllocationsByPool[i->second.pool] -= i->second.bytes;
		// global stats
		mTotalAllocations -= i->second.bytes;
		mAllocations.erase(i);
	}	
	//--------------------------------------------------------------------------
	size_t MemoryTracker::getTotalMemoryAllocated() const
	{
		return mTotalAllocations;
	}
	//--------------------------------------------------------------------------
	size_t MemoryTracker::getMemoryAllocatedForPool(unsigned int pool) const
	{
		return mAllocationsByPool[pool];
	}
	//--------------------------------------------------------------------------
	void MemoryTracker::writeReport(void)
	{
		std::stringstream os;

		if (mAllocations.empty())
			os << "No leaks!";
		else
		{
			size_t totalMem = 0;
			os << "Leaks detected:" << std::endl << std::endl;
			for (AllocationMap::const_iterator i = mAllocations.begin(); i != mAllocations.end(); ++i)
			{
				const Alloc& alloc = i->second;
				if (!alloc.filename.empty())
					os << "[0x" << i->first << "]" << "\t" << alloc.filename << "(" << alloc.line << ", " << alloc.function << "): ";
				else
					os << "(unknown source): ";
				os << alloc.bytes << " bytes";
				os << std::endl;
				totalMem += alloc.bytes;
			}

			os << std::endl;
			os << mAllocations.size() << " leaks detected, " << totalMem << " bytes total";
		}

		if (mDumpToStdOut)
			std::cout << os.str();

		std::ofstream of;
		of.open(mLeakFileName.c_str());
		
		if ( mWriteAllocReport  )
		{
			of << mAllocReportStream.str() << std::endl;
		}

		of << os.str();
		of.close();
	}

	
}; // namespace SimStep