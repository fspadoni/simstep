#ifndef SimStep_MemoryAllocatedObject_h__
#define SimStep_MemoryAllocatedObject_h__

#include "Utilities/Common.h"

#include "Memory/MemoryAllocator.h"

namespace SimStep
{

	/** Superclass for all objects that wish to use custom memory allocators
	when their new / delete operators are called.
	Requires a template parameter identifying the memory allocator policy 
	to use (e.g. see StdAllocPolicy). 
	*/
	template <class MemAllocPolicy>
	class _SimStepExport AllocatedObject
	{
	public:
		explicit AllocatedObject()
		{ }

		~AllocatedObject()
		{ }

		/// operator new, with debug line info
		void* operator new(size_t sz, const char* file, int line, const char* func)
		{
			return MemAllocPolicy::allocateBytes(sz, file, line, func);
		}

		void* operator new(size_t sz)
		{
			return MemAllocPolicy::allocateBytes(sz);
		}

		/// placement operator new
		void* operator new(size_t sz, void* ptr)
		{
			return ptr;
		}

		/// array operator new, with debug line info
		void* operator new[] ( size_t sz, const char* file, int line, const char* func )
		{
			return MemAllocPolicy::allocateBytes(sz, file, line, func);
		}

		void* operator new[] ( size_t sz )
		{
			return MemAllocPolicy::allocateBytes(sz);
		}

		void operator delete( void* ptr )
		{
			MemAllocPolicy::deallocateBytes(ptr);
		}

		// Corresponding operator for placement delete (second param same as the first)
		void operator delete( void* ptr, void* )
		{
			MemAllocPolicy::deallocateBytes(ptr);
		}

		// only called if there is an exception in corresponding 'new'
		void operator delete( void* ptr, const char* , int , const char*  )
		{
			MemAllocPolicy::deallocateBytes(ptr);
		}

		void operator delete[] ( void* ptr )
		{
			MemAllocPolicy::deallocateBytes(ptr);
		}


		void operator delete[] ( void* ptr, const char* , int , const char*  )
		{
			MemAllocPolicy::deallocateBytes(ptr);
		}
	};


	
}; // namespace SimStep

#endif // SimStep_MemoryAllocatedObject_h__