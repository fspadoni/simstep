#if defined ( _TC_MALLOC )

#include <Memory/TcMemory.h>

#include <..\src\google\malloc_extension_c.h>

namespace SimStep
{

	//template<> TcMemory* Singleton<TcMemory>::msSingleton = 0;


	TcMemory::TcMemory()
		: Memory()
	{

	}


	TcMemory::~TcMemory()
	{

	}


	//TcMemory* TcMemory::getSingletonPtr(void)
	//{
	//	return msSingleton;
	//}

	//TcMemory& TcMemory::getSingleton(void)
	//{  
	//	assert( msSingleton );  
	//	return ( *msSingleton );  
	//}

	void* TcMemory::allocateBytes(size_t byte_size )
	{
		return tc_malloc(byte_size);
	}

	void* TcMemory::alignedAllocateBytes(size_t byte_size, size_t alignment)
	{
		return tc_memalign( alignment, byte_size );
	}

	void* TcMemory::cacheAlignedAllocateBytes(int byte_size )
	{
		return tc_memalign( CACHE_ALIGN, byte_size );
	}

	void TcMemory::deallocateBytes(void* ptr )
	{
		tc_free(ptr);
	}

	void TcMemory::alignedDeallocateBytes(void* ptr )
	{
		tc_free(ptr);
	}

	void TcMemory::cacheAlignedDeallocateBytes(void* ptr )
	{
		tc_free(ptr);
	}

	size_t TcMemory::getMemoryUsedSize(void)
	{
		size_t memoryUsedSize;
		MallocExtension_GetNumericProperty("generic.current_allocated_bytes", &memoryUsedSize);
		return memoryUsedSize;
	}

	size_t TcMemory::getMemoryAllocatedSize(void)
	{
		size_t memoryAllocatedSize;
		MallocExtension_GetNumericProperty("generic.heap_size", &memoryAllocatedSize);
		return memoryAllocatedSize;
	}



}; // namespace SimStep

#endif