#include <Memory/SystemMemory.h>


namespace SimStep
{


	SystemMemory::SystemMemory()
		: Memory()
	{

	}


	SystemMemory::~SystemMemory()
	{

	}


	//TcMemory* TcMemory::getSingletonPtr(void)
	//{
	//	return msSingleton;
	//}

	//TcMemory& TcMemory::getSingleton(void)
	//{  
	//	assert( msSingleton );  
	//	return ( *msSingleton );  
	//}

	void* SystemMemory::allocateBytes(size_t byte_size )
	{
		return ::malloc(byte_size);
	}

	void* SystemMemory::alignedAllocateBytes(size_t byte_size, size_t alignment)
	{
		return ::_aligned_malloc( byte_size, alignment );
	}

	void* SystemMemory::cacheAlignedAllocateBytes(int byte_size )
	{
		return ::_aligned_malloc( byte_size, CACHE_ALIGN );
	}

	void SystemMemory::deallocateBytes(void* ptr )
	{
		::free(ptr);
	}

	void SystemMemory::alignedDeallocateBytes(void* ptr )
	{
		::_aligned_free(ptr);
	}

	void SystemMemory::cacheAlignedDeallocateBytes(void* ptr )
	{
		::_aligned_free(ptr);
	}

	size_t SystemMemory::getMemoryUsedSize(void)
	{
		size_t memoryUsedSize = 0;
		return memoryUsedSize;
	}

	size_t SystemMemory::getMemoryAllocatedSize(void)
	{
		size_t memoryAllocatedSize = 0;
		return memoryAllocatedSize;
	}

	
}; // namespace SimStep