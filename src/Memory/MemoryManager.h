#ifndef SimStep_MemoryManager_h__
#define SimStep_MemoryManager_h__

#include <Utilities/Singleton.h>
#include <Memory/MemoryAllocatorConfig.h>


//#include <Memory/MemoryTCMalloc.h>


namespace SimStep
{

	// This allocator forwards to system malloc
#define SIMSTEP_DECLARE_SYSTEM_ALLOCATOR() \
	inline void *operator new(size_t size)	{ return ::_aligned_malloc( size, 128); }	\
	inline void operator delete(void* ptr) { _aligned_free(ptr); }	\
	inline void *operator new(size_t size, void* ptr) { return ptr; }	\
	inline void operator delete(void*, void*) { }	\
	inline void *operator new(size_t size, const char* file, int line, const char* func ) \
	{ return ::_aligned_malloc_dbg( size, 128, file, line ); }	\
	inline void operator delete( void* ptr, const char* , int , const char*  ) \
	{ return ::_aligned_free_dbg( ptr ); }


	//class DynLib;
	//class Plugin;

	template <class MemAllocatorPolicy >
	class  MemoryTestAllocator
	{
	};

	template <typename Allocator >
	class _SimStepExport MemoryManager : public Singleton<MemoryManager<Allocator> >
	{

	public:

		enum
		{
			SIMD_ALIGN = 16,
			CACHE_ALIGN  = 128
		};

		SIMSTEP_DECLARE_SYSTEM_ALLOCATOR();

		MemoryManager() {}

		~MemoryManager() {}

		static MemoryManager& getSingleton(void)
		{
			assert( msSingleton );  
			return ( *msSingleton );
		}

		static MemoryManager* getSingletonPtr(void)
		{
			return msSingleton; 
		}

		void* allocateBytes(size_t byte_size )
		{
			return Allocator::allocateBytes(byte_size);
		}
		//void* allocateBytes(size_t byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY);

		void* allocateBytes(size_t byte_size, MemoryCategory mc,
			const char* file, int line, const char* func )
		{
			return Allocator::allocateBytes(byte_size, file, line, func );
		}

		void* alignedAllocateBytes(size_t byte_size, size_t alignment )
		{
			return Allocator::alignedAllocateBytes(byte_size, alignment);
		}
		//virtual void* alignedAllocateBytes(size_t byte_size, size_t alignment, MemoryCategory mc = MEMCATEGORY_DUMMY );

		void* alignedAllocateBytes(size_t byte_size, size_t alignment, 
			MemoryCategory mc, const char* file, int line, const char* func )
		{
			return Allocator::alignedAllocateBytes(byte_size, alignment, 
				file, line, func );
		}


		void* cacheAlignedAllocateBytes(int byte_size )
		{
			return Allocator::alignedAllocateBytes(byte_size, CACHE_ALIGN);
		}
		//void* cacheAlignedAllocateBytes(int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY);

		void* cacheAlignedAllocateBytes(size_t byte_size, MemoryCategory mc,
			const char* file, int line, const char* func )
		{
			return Allocator::alignedAllocateBytes(byte_size, CACHE_ALIGN,
				file, line, func );
		}


		void deallocateBytes(void* ptr )
		{
			return Allocator::deallocateBytes(ptr);
		}
		//void deallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		void deallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY )
		{
			return Allocator::deallocateBytes(ptr);
		}

		void alignedDeallocateBytes(void* ptr )
		{
			return Allocator::alignedDeallocateBytes(ptr);
		}
		//void alignedDeallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		void alignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY )
		{
			return Allocator::alignedDeallocateBytes(ptr);
		}


		void cacheAlignedDeallocateBytes(void* ptr )
		{
			Allocator::alignedDeallocateBytes(ptr);
		}
		//void cacheAlignedDeallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		void cacheAlignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY )
		{
			Allocator::alignedDeallocateBytes(ptr);
		}


		size_t getMemoryUsedSize(void)	{return 0;}

		size_t getMemoryAllocatedSize(void)	{return 0;}

	};





	//template <class MemAllocatorPolicy>
	class _SimStepExport Memory : public Singleton<Memory> // , MemAllocator<TCMallocPolicy>
	{
	
	//protected:
		//Allocator* mAllocator;
		//TCMallocator allocator;

		//unsigned long m_memoryUsed;
		//unsigned long m_memoryAllocated;


		//typedef std::vector<DynLib*> PluginLibList;
		//typedef std::vector<Plugin*> PluginInstanceList;
		///// List of plugin DLLs loaded
		//PluginLibList mPluginLibs;
		///// List of Plugin instances registered
		//PluginInstanceList mPlugins;

	public:

		enum
		{
			SIMD_ALIGN = 16,
			CACHE_ALIGN  = 128
		};

		SIMSTEP_DECLARE_SYSTEM_ALLOCATOR();

		Memory();

		virtual ~Memory();

		static Memory& getSingleton(void);

		static Memory* getSingletonPtr(void);

		//inline void setAllocator(Allocator* allocator) { mAllocator = allocator;}
		//
		//inline Allocator* getAllocator(void) const { return mAllocator;}

		//void addAllocator(Allocator* newAlloc) {};

		//void loadAllocators( const String& pluginsfile );

		//void installAllocator(Plugin* plugin);

		//void unloadAllocators();

		//void uninstallAllocator(Plugin* plugin);

		virtual void* allocateBytes(size_t byte_size ) = 0;
		//void* allocateBytes(size_t byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY);

		inline void* allocateBytes(size_t byte_size, MemoryCategory mc,
			const char* file, int line, const char* func );

		virtual void* alignedAllocateBytes(size_t byte_size, size_t alignment ) = 0;
		//virtual void* alignedAllocateBytes(size_t byte_size, size_t alignment, MemoryCategory mc = MEMCATEGORY_DUMMY );

		inline void* alignedAllocateBytes(size_t byte_size, size_t alignment, 
			MemoryCategory mc, const char* file, int line, const char* func );
			
	
		virtual void* cacheAlignedAllocateBytes(int byte_size ) = 0;
		//void* cacheAlignedAllocateBytes(int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY);

		inline void* cacheAlignedAllocateBytes(size_t byte_size, MemoryCategory mc,
			const char* file, int line, const char* func );


		virtual void deallocateBytes(void* ptr ) = 0;
		//void deallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		inline void deallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY );


		virtual void alignedDeallocateBytes(void* ptr ) = 0;
		//void alignedDeallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		inline void alignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY );


		virtual void cacheAlignedDeallocateBytes(void* ptr ) = 0;
		//void cacheAlignedDeallocateBytes(void* ptr, MemoryCategory mc = MEMCATEGORY_DUMMY );

		inline void cacheAlignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc = MEMCATEGORY_DUMMY );

		virtual size_t getMemoryUsedSize(void)	{return 0;}

		virtual size_t getMemoryAllocatedSize(void)	{return 0;}
		
		

	protected:



		size_t mMemAllocatedSize;

		inline void collectAllocStatistics(size_t byte_size, MemoryCategory mc );
		inline void collectDeallocStatistics(size_t byte_size, MemoryCategory mc );

		class MemoryStatistics 
		{
		public:
			int m_size_in_use;
			int m_n_allocates;
			int m_blocks_in_use;
			int m_max_size_in_use;
		};

		MemoryStatistics m_statistics[MEMCATEGORY_COUNT];


	};




	class _SimStepExport MemoryPool : public Singleton<MemoryPool>
	{


		enum
		{

			MEMORY_CACHE_ALIGN  = 128,


			MEMORY_MAX_SIZE_SMALL_BLOCK = 512,

			// The number of small chunk sizes
			MEMORY_MAX_SMALL_ROW  = 12,

			//// The number of small and large chunk sizes
			//MEMORY_MAX_ALL_ROW = (MEMORY_MAX_SMALL_ROW+4),

			//// The largest small block we allocate from this pool
			//MEMORY_MAX_SIZE_SMALL_BLOCK  = 512,

			//// The largest large block we allocate from this pool
			//MEMORY_MAX_SIZE_LARGE_BLOCK = 8192,

			//// The low bits we ignore when indexing into the large arrays
			//MEMORY_LARGE_BLOCK_RSHIFT_BITS = 10,

			// How much we allocate when the small pool becomes full
			MEMORY_SYSTEM_PAGE_SIZE = 8192,

			// Debugging
			MEMORY_ALLOC_MAGIC_NUMBER = 0x3425232,
			MEMORY_FREE_MAGIC_NUMBER = 0x3425234

		};
	
	public:

		MemoryPool();
		//: a really empty constructor

		MemoryPool(char *buffer, int buffer_size);

		void init_memory( char *buffer, int buffer_size );
		//: initialized the memory pool


		~MemoryPool();

	
		//static MemoryPool* getSingletonPtr();

		static MemoryPool& getSingleton(void);

		static MemoryPool* getSingletonPtr(void);



		void* allocate(int byte_size, MemoryCategory mc);
		//: allocate a piece of memory
		//: Note: the size of that piece is not stored, so
		//: the user has to do remember the size !!!
		//: the memory class is currently used only for statistics

		void  deallocate(void*, int byte_size, MemoryCategory mc);
		//: deallocate a piece of memory 


		void* allocateShare(int byte_size, MemoryCategory mc);
		//: allocate a piece of memory
		//: Note: the size of that piece is not stored, so
		//: the user has to do remember the size !!!
		//: the memory class is currently used only for statistics

		void  deallocateShare(void*, int byte_size, MemoryCategory mc);
		//: deallocate a piece of memory 


		void* allocate_and_store_size(int byte_size, MemoryCategory mc);
		//: allocate a piece of memory
		//: Note: the size of that piece is stored, so
		//: 16 bytes of memory are wasted
		void  deallocate_stored_size(void*, MemoryCategory mc);
		//: deallocate a piece of memory which has been allocated of allocate_and_store_size

		void* allocate_debug(int n, const char* file, int line);
		void  deallocate_debug(void*, int n,const char* file,int line);


		//void print_statistics(class hk_Console *); // see Memory_Util

	
	public: // THE interfaces to the system allocate, change this if you want to add in your own big block memory allocation
	
		static void *system_malloc( size_t size, size_t alignment);
		static void system_free(	 void *data );
		//static inline void* memcpy(void* dest,const void* src,int size);
		//static inline void* memset(void* dest, uchar val, int32 size);

	private:
		void* allocate_real(int size);
		inline int size_to_row(int size);

	protected:
		//friend class Memory_Util;
		class MemoryElem {
		public:
			MemoryElem *m_next;
			int m_magic;
		};

		class MemoryStatistics {
		public:
			int m_size_in_use;
			int m_n_allocates;
			int m_blocks_in_use;
			int m_max_size_in_use;
		};

		class MemoryBlock
		{
		public:
			MemoryBlock *m_next;
			int		m_pad[(MEMORY_CACHE_ALIGN - sizeof(MemoryBlock *))/ sizeof(int)];
		};

	protected:
		MemoryElem *m_free_list[MEMORY_MAX_SMALL_ROW];
		MemoryBlock *m_allocated_memory_blocks;

		int  m_blocks_in_use[MEMORY_MAX_SMALL_ROW];
		char *m_memory_start;
		char *m_memory_end;
		char *m_used_end;
		int m_row_to_size[MEMORY_MAX_SMALL_ROW];
		MemoryStatistics m_statistics[MEMCATEGORY_COUNT];
		char m_size_to_row[ MEMORY_MAX_SIZE_SMALL_BLOCK+1 ];

	};

	


#define SIMSTEP_DECLARE_CLASS_ALLOCATOR( Class, category )							\
	inline void *operator new(size_t size, const char* file, int line, const char* func ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().allocateBytes( size, category, file, line, func );			\
	return object;																		\
	}																						\
	inline void *operator new(size_t size ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().allocateBytes( size );			\
	return object;																		\
	}																						\
	inline void *operator new[](size_t size, const char* file, int line, const char* func ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().allocateBytes( size, category, file, line, func );			\
	return object;																		\
	}																						\
	inline void *operator new[](size_t size ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().allocateBytes( size );			\
	return object;																		\
	}																						\
	\
	inline void operator delete(void *ptr, size_t size ){											\
	Memory::getSingletonPtr()->deallocateBytes( ptr, size );				\
	}	\
	\
	inline void operator delete[](void *ptr, size_t size ){											\
	Memory::getSingletonPtr()->deallocateBytes( ptr, size );				\
	}	\
	\
	inline void operator delete( void* ptr, const char* , int , const char*  ){	\
		Memory::getSingletonPtr()->deallocateBytes( ptr, sizeof( Class ), category );				\
	}	\
	\
	inline void operator delete[]( void* ptr, const char* , int , const char*  ){	\
		Memory::getSingletonPtr()->deallocateBytes( ptr, sizeof( Class ), category );				\
	}// only called if there is an exception in corresponding 'new'



#define SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Class, alignment, category )							\
	inline void *operator new(size_t size, const char* file, int line, const char* func ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().alignedAllocateBytes( size, alignment, category, file, line, func );			\
	return object;																		\
	}																						\
	inline void *operator new(size_t size ){ \
	assert ( sizeof( Class ) == size );											\
	void *object = Memory::getSingleton().alignedAllocateBytes( size, alignment );			\
	return object;																		\
	}																						\
	inline void *operator new[](size_t size, const char* file, int line, const char* func ){ \
	/*assert ( sizeof( Class ) == size );*/											\
	void *object = Memory::getSingleton().alignedAllocateBytes( size, alignment, category, file, line, func );			\
	return object;																		\
	}																						\
	inline void *operator new[](size_t size ){ \
	/*assert ( sizeof( Class ) == size );	*/										\
	void *object = Memory::getSingleton().alignedAllocateBytes( size, alignment );			\
	return object;																		\
	}																						\
	\
	inline void operator delete(void *ptr, size_t size){											\
	Memory::getSingleton().alignedDeallocateBytes( ptr, size, category );				\
	}	\
	\
	inline void operator delete[](void *ptr, size_t size){											\
	Memory::getSingleton().alignedDeallocateBytes( ptr, size, category );				\
	} \
	\
	inline void operator delete( void* ptr, const char* , int , const char*  ){	\
		Memory::getSingleton().alignedDeallocateBytes( ptr, sizeof( Class ), category );				\
	} \
	\
	inline void operator delete[](void *ptr, const char* , int , const char* ){			\
		Memory::getSingleton().alignedDeallocateBytes( ptr, sizeof( Class ), category );				\
	} \


//#define SIMSTEP_DECLARE_VIRTUAL_CLASS_ALLOCATOR( Class, category )							\
//	inline void *operator new(size_t size, const char* file, int line, const char* func ){ \
//	assert ( sizeof( Class ) == size );											\
//	void *object = Memory::getSingleton().allocateBytes( size, category, file, line, func );			\
//	return object;																		\
//	}																						\
//	inline void *operator new(size_t size ){ \
//	assert ( sizeof( Class ) == size );											\
//	void *object = Memory::getSingleton().allocateBytes( size );			\
//	return object;																		\
//	}																						\
//	inline void *operator new[](size_t size, const char* file, int line, const char* func ){ \
//	assert ( sizeof( Class ) == size );											\
//	void *object = Memory::getSingleton().allocateBytes( size, category, file, line, func );			\
//	return object;																		\
//	}																						\
//	inline void *operator new[](size_t size ){ \
//	assert ( sizeof( Class ) == size );											\
//	void *object = Memory::getSingleton().allocateBytes( size );			\
//	return object;																		\
//	}																						\
//	\
//	inline void operator delete(void *ptr ){											\
//	Memory::getSingletonPtr()->deallocateBytes( ptr );				\
//	}	\
//	\
//	inline void operator delete[](void *ptr){											\
//	Memory::getSingletonPtr()->deallocateBytes( ptr );				\
//	}	\
//	\
//	inline void operator delete( void* ptr, const char* , int , const char*  ){	\
//	Memory::getSingletonPtr()->deallocateBytes( ptr, sizeof( Class ), category );				\
//	}	\
//	\
//	inline void operator delete[]( void* ptr, const char* , int , const char*  ){	\
//	Memory::getSingletonPtr()->deallocateBytes( ptr, sizeof( Class ), category );				\
//	}// only called if there is an exception in corresponding 'new'



	/// Convenience function to allocate memory of the correct type
	//template <typename TYPE>
#define SimStepMalloc( nbytes, category ) \
	SimStep::Memory::getSingleton().allocateBytes(nbytes, category, __FILE__, __LINE__, __FUNCTION__)


	/// Convenience function to deallocate memory of the correct type
	//template <typename TYPE>
#define SimStepFree( ptr, nbytes, category ) \
	SimStep::Memory::getSingleton().deallocateBytes(static_cast<void*>(ptr), nbytes, category)



#define SimStepAlignedMalloc( nbytes, alignment, category ) \
	SimStep::Memory::getSingleton().alignedAllocateBytes(nbytes, alignment, category, __FILE__, __LINE__, __FUNCTION__)


	/// Convenience function to deallocate memory of the correct type
	//template <typename TYPE>
#define SimStepAlignedFree( ptr, nbytes, category ) \
	SimStep::Memory::getSingleton().alignedDeallocateBytes(static_cast<void*>(ptr), nbytes, category);



}; // namespace SimStep

#endif // SimStep_Memory_h__