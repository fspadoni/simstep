#include <Memory/MemoryManager.h>
#include <Memory/MemoryManager.inl>

//#include <../dependencies/google-perftools-1.6/include/tcmalloc.h>
//#include <../../SimStepPlugins/Allocator/dependencies/google-perftools-1.6/include/tcmalloc.h>

#include <Core/PlugIn.h>
#include <Utilities/ConfigFile.h>
#include <Utilities/PhysicsException.h>
#include <Core/DynLibManager.h>
#include <Core/DynLib.h>
#include <Core/LogManager.h>

typedef void (*DLL_START_PLUGIN)(void);
typedef void (*DLL_STOP_PLUGIN)(void);

namespace SimStep
{

	template<>
	MemoryManager<typename Allocator>* Singleton<MemoryManager<typename Allocator>>::msSingleton = 0;

	MemoryManager<StdAllocPolicy> StdMemory;

	template<>
	MemoryManager<StdAllocPolicy>* Singleton<MemoryManager<StdAllocPolicy>>::msSingleton = 0;



	template<> Memory* Singleton<Memory>::msSingleton = 0;


	Memory::Memory() 
	{
		//loadAllocators("SimStepPlugins.cfg");
	}


	Memory::~Memory()
	{

	}


	Memory* Memory::getSingletonPtr(void)
	{
		return msSingleton;
	}
	Memory& Memory::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}

	//void Memory::loadAllocators( const String& pluginsfile )
	//{
	//	StringVector pluginList;
	//	String pluginDir;
	//	ConfigFile cfg;

	//	try 
	//	{
	//		cfg.load( pluginsfile );
	//	}
	//	catch (Exception)
	//	{
	//		//LogManager::getSingleton().logMessage(pluginsfile + " not found, automatic plugin loading disabled.");
	//		return;
	//	}

	//	pluginDir = cfg.getSetting("PluginFolder"); // Ignored on Mac OS X, uses Resources/ directory
	//	pluginList = cfg.getMultiSetting("Allocator");


	//	char last_char = pluginDir[pluginDir.length()-1];
	//	if (last_char != '/' && last_char != '\\')
	//	{
	//		pluginDir += "\\";
	//	}

	//	for( StringVector::iterator it = pluginList.begin(); it != pluginList.end(); ++it )
	//	{
	//		String pluginName = pluginDir + (*it);
	//		// Load plugin library
	//		DynLib* lib = DynLibManager::getSingleton().load( pluginName );
	//		// Store for later unload
	//		// Check for existence, because if called 2+ times DynLibManager returns existing entry
	//		if (std::find(mPluginLibs.begin(), mPluginLibs.end(), lib) == mPluginLibs.end())
	//		{
	//			mPluginLibs.push_back(lib);

	//			// Call startup function
	//			DLL_START_PLUGIN pFunc = (DLL_START_PLUGIN)lib->getSymbol("dllStartPlugin");

	//			if (!pFunc)
	//				PHYSICS_EXCEPT(Exception::ERR_ITEM_NOT_FOUND, "Cannot find symbol dllStartPlugin in library " + pluginName,
	//				"Root::loadPlugin");

	//			// This must call installPlugin
	//			pFunc();
	//		}
	//	}
	//}

	//void Memory::installAllocator(Plugin* plugin)
	//{
	//	LogManager::getSingleton().logMessage("Installing allocator: " + plugin->getName());

	//	mPlugins.push_back(plugin);
	//	plugin->install();

	//	// if allocator is already initialised, call world init too
	//	//if (mIsInitialised)
	//	{
	//		plugin->initialise();
	//	}

	//	LogManager::getSingleton().logMessage("Allocator successfully installed");
	//}


	//void Memory::unloadAllocators(void)
	//{
	//	// unload dynamic libs first
	//	for (PluginLibList::reverse_iterator i = mPluginLibs.rbegin(); i != mPluginLibs.rend(); ++i)
	//	{
	//		// Call plugin shutdown
	//		DLL_STOP_PLUGIN pFunc = (DLL_STOP_PLUGIN)(*i)->getSymbol("dllStopPlugin");
	//		// this will call uninstallPlugin
	//		pFunc();
	//		// Unload library & destroy
	//		DynLibManager::getSingleton().unload(*i);

	//	}
	//	mPluginLibs.clear();

	//	// now deal with any remaining plugins that were registered through other means
	//	for (PluginInstanceList::reverse_iterator i = mPlugins.rbegin(); i != mPlugins.rend(); ++i)
	//	{
	//		// Note this does NOT call uninstallPlugin - this shutdown is for the 
	//		// detail objects
	//		(*i)->uninstall();
	//	}
	//	mPlugins.clear();

	//}

	//void Memory::uninstallAllocator(Plugin* plugin)
	//{
	//	LogManager::getSingleton().logMessage("Uninstalling plugin: " + plugin->getName());
	//	PluginInstanceList::iterator i = 
	//		std::find(mPlugins.begin(), mPlugins.end(), plugin);
	//	if (i != mPlugins.end())
	//	{
	//		//if (mIsInitialised)
	//		{
	//			plugin->release();
	//		}
	//		plugin->uninstall();
	//		mPlugins.erase(i);
	//	}
	//	LogManager::getSingleton().logMessage("Plugin successfully uninstalled");

	//}




//	void* Memory::allocateBytes(size_t byte_size, MemoryCategory mc )
//	{
//#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
//		collectAllocStatistics( byte_size, mc );
//#endif
//		return mAllocator->allocateBytes(byte_size);
//		//return tc_malloc( byte_size );
//	}

	void* Memory::allocateBytes(size_t byte_size, MemoryCategory mc,
		const char* file, int line, const char* func)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectAllocStatistics( byte_size, mc );
#endif
		void* ptr = this->allocateBytes(byte_size);
		//void* ptr = tc_malloc(byte_size);
		MemoryTracker::get()._recordAlloc(ptr, byte_size, 0, file, line, func);
		return ptr;
	}

//	void* Memory::alignedAllocateBytes(size_t byte_size, size_t alignment, MemoryCategory mc )
//	{
//#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
//		collectAllocStatistics( byte_size, mc );
//#endif
//		return mAllocator->alignedAllocateBytes( byte_size, alignment );
//		//return tc_memalign( alignment, byte_size);
//	}

	void* Memory::alignedAllocateBytes(size_t byte_size, size_t alignment, 
		MemoryCategory mc, const char* file, int line, const char* func)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectAllocStatistics( byte_size, mc );
#endif
		void* ptr = this->alignedAllocateBytes( byte_size, alignment );
		//void* ptr = tc_memalign( alignment, byte_size);
		MemoryTracker::get()._recordAlloc(ptr, byte_size, 0, file, line, func);
		return ptr;
	}


//	void* Memory::cacheAlignedAllocateBytes(int byte_size, MemoryCategory mc)
//	{
//#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
//		collectAllocStatistics( byte_size, mc );
//#endif
//		return mAllocator->alignedAllocateBytes( byte_size, CACHE_ALIGN );
//		//return tc_memalign( CACHE_ALIGN, byte_size);
//	}

	void* Memory::cacheAlignedAllocateBytes(size_t byte_size, MemoryCategory mc, 
		const char* file, int line, const char* func )
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectAllocStatistics( byte_size, mc );
#endif
		void* ptr = this->alignedAllocateBytes( byte_size, CACHE_ALIGN );
		//void* ptr = tc_memalign( MEMORY_CACHE_ALIGN, byte_size);
		MemoryTracker::get()._recordAlloc(ptr, byte_size, 0, file, line, func);
		return ptr;
	}

	//void  Memory::deallocateBytes(void* ptr, MemoryCategory mc)
	//{
	//	mAllocator->deallocateBytes(ptr);
	//}

	void  Memory::deallocateBytes(void* ptr, int byte_size, MemoryCategory mc)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectDeallocStatistics( byte_size, mc );
#endif
		MemoryTracker::get()._recordDealloc(ptr);
		this->deallocateBytes(ptr);
		//tc_free( ptr );
	}

//	void  Memory::alignedDeallocateBytes(void* ptr, MemoryCategory mc)
//	{
//#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
//		collectDeallocStatistics( byte_size, mc );
//#endif
//		mAllocator->alignedDeallocateBytes(ptr);
//		//tc_free( ptr );
//	}

	void  Memory::alignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectDeallocStatistics( byte_size, mc );
#endif
		MemoryTracker::get()._recordDealloc(ptr);
		this->alignedDeallocateBytes(ptr );
		//tc_free( ptr );
	}

	//void  Memory::cacheAlignedDeallocateBytes(void* ptr, MemoryCategory mc)
	//{
	//	mAllocator->alignedDeallocateBytes(ptr);
	//}

	void  Memory::cacheAlignedDeallocateBytes(void* ptr, int byte_size, MemoryCategory mc)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS
		collectDeallocStatistics( byte_size, mc );
#endif
		MemoryTracker::get()._recordDealloc(ptr);
		this->cacheAlignedDeallocateBytes(ptr, mc);
		//tc_free( ptr );
	}




	void  Memory::collectAllocStatistics(size_t byte_size, MemoryCategory mc )
	{
		MemoryStatistics &s = m_statistics[mc];
		s.m_size_in_use += byte_size;
		s.m_blocks_in_use += 1;
		s.m_n_allocates += 1;
		if ( s.m_size_in_use > 	s.m_max_size_in_use)
		{
			s.m_max_size_in_use = s.m_size_in_use;
		}
	}

	void  Memory::collectDeallocStatistics(size_t byte_size, MemoryCategory mc )
	{
		MemoryStatistics &s = m_statistics[mc];
		s.m_size_in_use -= byte_size;
		s.m_blocks_in_use -= 1;
	}



	class MemoryWithSize
	{
	public:
		int m_size;
		enum 
		{
			MAGIC_MEMORY_WITH_SIZE = 0x2345656
		} m_magic;
		int m_dummy[2];
	};



	template<> MemoryPool* Singleton<MemoryPool>::msSingleton = 0;

	MemoryPool* MemoryPool::getSingletonPtr(void)
	{
		return msSingleton;
	}
	MemoryPool& MemoryPool::getSingleton(void)
	{  
		assert( msSingleton );  
		return ( *msSingleton );  
	}



	MemoryPool::MemoryPool(char *buffer, int buffer_size)
	{
		init_memory( buffer, buffer_size );
	}


	MemoryPool::~MemoryPool()
	{
		while ( m_allocated_memory_blocks )
		{
			MemoryBlock *b = m_allocated_memory_blocks;
			m_allocated_memory_blocks = m_allocated_memory_blocks->m_next;
			MemoryPool::system_free( (void *)b );
		}
	}


	void MemoryPool::init_memory( char *buffer, int buffer_size )
	{
		m_memory_start = buffer;
		m_used_end = buffer;
		m_memory_end = m_used_end + buffer_size;
		m_allocated_memory_blocks = 0;

		for (int i = MEMORY_MAX_SMALL_ROW-1; i>=0 ; i-- )
		{
			m_free_list[i] = NULL;
			m_blocks_in_use[i] = 0;
		}

		for (int j = 0; j <= MEMORY_MAX_SIZE_SMALL_BLOCK; j++ )
		{
			int row = size_to_row(j);
			m_size_to_row[ j ] = row;
			m_row_to_size[row] = j;
		}
		{ // statistics
			for (int i = 0; i< MEMCATEGORY_COUNT; i++)
			{
				MemoryStatistics &s = m_statistics[i];
				s.m_max_size_in_use = 0;
				s.m_size_in_use = 0;
				s.m_n_allocates = 0;
				s.m_blocks_in_use = 0;
			}
		}
	}



	void *MemoryPool::allocate_real( int size )
	{
		if ( size > MEMORY_MAX_SIZE_SMALL_BLOCK)
		{
			// aggiungi al Log
			//hk_Console::get_instance()->printf("system_malloc: big block allocated size %i\n", size );

			return MemoryPool::system_malloc( size, MEMORY_CACHE_ALIGN );
		}


		int row = m_size_to_row[ size ];
		size = m_row_to_size[ row ];

		int allocated_size = size;
		void *result;

		// allocate first block
		if ( size + m_used_end > m_memory_end)
		{
			// aggiungi al Log
			//hk_Console::get_instance()->printf("running out of space: block size %i\n", size );

			MemoryBlock *b = (MemoryBlock *)MemoryPool::system_malloc( sizeof(MemoryBlock) + MEMORY_SYSTEM_PAGE_SIZE, MEMORY_CACHE_ALIGN );

			b->m_next = m_allocated_memory_blocks;
			m_allocated_memory_blocks = b;
			m_memory_start = (char *)(b+1);
			m_used_end = m_memory_start;
			m_memory_end = m_used_end + MEMORY_SYSTEM_PAGE_SIZE;
		}

		result = (void *)m_used_end;
		MemoryElem *el = (MemoryElem *) m_used_end;
		el->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
		m_used_end += size;

		// allocate rest to get make sure the alignment is ok
		int biu = m_blocks_in_use[row];
		while ( allocated_size < 0 /*256*/ )
		{
			if ( size + m_used_end < m_memory_end){
				MemoryElem *el = (MemoryElem *) m_used_end;
				el->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
				this->deallocate( m_used_end, size, MEMCATEGORY_DUMMY );
				m_used_end += size;
			}else{
				break;
			}		
			allocated_size += size;
		}
		m_blocks_in_use[row] = biu;
		return result;
	}

	void* MemoryPool::allocate(int size, MemoryCategory cl)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS 
		MemoryStatistics &s = m_statistics[cl];
		s.m_size_in_use += size;
		s.m_blocks_in_use += 1;
		s.m_n_allocates += 1;
		if ( s.m_size_in_use > 	s.m_max_size_in_use)
		{
			s.m_max_size_in_use = s.m_size_in_use;
		}
#endif

#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
		MemoryWithSize *x = (MemoryWithSize *)this->MemoryPool::system_malloc( size + sizeof( MemoryWithSize ), MEMORY_CACHE_ALIGNMENT );
		x->m_size = size; 
		x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
		return (void *)(x+1);
#else

		if ( size <= MEMORY_MAX_SIZE_SMALL_BLOCK){
			int row = m_size_to_row[size];
			m_blocks_in_use[row]++;
			MemoryElem *n = m_free_list[row];
			if ( n ){
				m_free_list[row] = n->m_next;
				n->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
				return (void *)n;
			}
		}
		return allocate_real( size );
#endif
	}


	void* MemoryPool::allocateShare(int size, MemoryCategory mc)
	{

#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS 
		MemoryStatistics &s = m_statistics[cl];
		s.m_size_in_use += size;
		s.m_blocks_in_use += 1;
		s.m_n_allocates += 1;
		if ( s.m_size_in_use > 	s.m_max_size_in_use)
		{
			s.m_max_size_in_use = s.m_size_in_use;
		}
#endif

#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
		MemoryWithSize *x = (MemoryWithSize *)this->MemoryPool::system_malloc( size + sizeof( MemoryWithSize ), MEMORY_CACHE_ALIGNMENT );
		x->m_size = size; 
		x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
		return (void *)(x+1);
#else

		int sizeAligned = ( MEMORY_CACHE_ALIGN > size) ? MEMORY_CACHE_ALIGN : size;

		if ( size <= MEMORY_MAX_SIZE_SMALL_BLOCK)
		{
			int row = m_size_to_row[sizeAligned];
			m_blocks_in_use[row]++;
			MemoryElem *n = m_free_list[row];
			if ( n ){
				m_free_list[row] = n->m_next;
				n->m_magic = MEMORY_ALLOC_MAGIC_NUMBER;
				return (void *)n;
			}
		}
		return allocate_real( sizeAligned );
#endif
	}

	void  MemoryPool::deallocateShare(void* p, int size, MemoryCategory cl)
	{

#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS 
		MemoryStatistics &s = m_statistics[cl];
		s.m_size_in_use -= size;
		s.m_blocks_in_use -= 1;
#endif

#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
		{
			MemoryWithSize *x = (MemoryWithSize *)p;
			x--;
			assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
			assert(  x->m_size == size );
			this->system_free( x );
		}
#else
		int sizeAligned = ( MEMORY_CACHE_ALIGN > size) ? MEMORY_CACHE_ALIGN : size;

		if ( size <= MEMORY_MAX_SIZE_SMALL_BLOCK )
		{
			MemoryElem *me = (MemoryElem *)p;
			int row = m_size_to_row[sizeAligned];
			m_blocks_in_use[row]--;
			me->m_next = m_free_list[row];
			assert( me->m_magic != MEMORY_FREE_MAGIC_NUMBER);
			me->m_magic = MEMORY_FREE_MAGIC_NUMBER;
			m_free_list[row] = me;
		}
		else
		{
			MemoryPool::system_free((char *)p);
		}
#endif
	}


	void MemoryPool::deallocate(void* p, int size, MemoryCategory cl)
	{
#ifdef SIMSTEP_MEMORY_ENABLE_STATISTICS 
		MemoryStatistics &s = m_statistics[cl];
		s.m_size_in_use -= size;
		s.m_blocks_in_use -= 1;
#endif

#ifdef SIMSTEP_MEMORY_ENABLE_DEBUG_CHECK
		{
			MemoryWithSize *x = (MemoryWithSize *)p;
			x--;
			assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
			assert(  x->m_size == size );
			this->system_free( x );
		}
#else
		if ( size <= MEMORY_MAX_SIZE_SMALL_BLOCK )
		{
			MemoryElem *me = (MemoryElem *)p;
			int row = m_size_to_row[size];
			m_blocks_in_use[row]--;
			me->m_next = m_free_list[row];
			assert( me->m_magic != MEMORY_FREE_MAGIC_NUMBER);
			me->m_magic = MEMORY_FREE_MAGIC_NUMBER;
			m_free_list[row] = me;
		}
		else
		{
			MemoryPool::system_free((char *)p);
		}
#endif
	}

	int MemoryPool::size_to_row( int size )
	{
		assert (MEMORY_MAX_SMALL_ROW == 12 );
		if (size <= 8 ) return 1;
		else if (size <= 16 ) return 2;
		else if (size <= 32 ) return 3;
		else if (size <= 48 ) return 4;
		else if (size <= 64 ) return 5;
		else if (size <= 96 ) return 6;
		else if (size <= 128 ) return 7;
		else if (size <= 160 ) return 8;
		else if (size <= 192 ) return 9;
		else if (size <= 256 ) return 10;
		else if (size <= 512 ) return 11;
		else
		{
			/*break;*/
			return -1;
		}
	}



	void* MemoryPool::allocate_debug(int n,
		const char* file,
		int line)
	{
		return new char[n];
	}

	void MemoryPool::deallocate_debug(
		void* p,
		int n,
		const char* file,
		int line)
	{
		delete[] static_cast<char*>(p);
	}



	void* MemoryPool::allocate_and_store_size(int byte_size, MemoryCategory cl)
	{
		MemoryWithSize *x = (MemoryWithSize *)this->allocate( byte_size + sizeof( MemoryWithSize ), cl);
		x->m_size = byte_size; 
		x->m_magic = MemoryWithSize::MAGIC_MEMORY_WITH_SIZE;
		return (void *)(x+1);
	}

	void  MemoryPool::deallocate_stored_size(void* p , MemoryCategory cl)
	{
		if(p)
		{
			MemoryWithSize *x = (MemoryWithSize *)p;
			x--;
			assert( x->m_magic == MemoryWithSize::MAGIC_MEMORY_WITH_SIZE );
			this->deallocate( x, x->m_size + sizeof( MemoryWithSize ), cl );
		}
	}



	void *MemoryPool::system_malloc( size_t size, size_t alignment)
	{
//#if defined(WIN32_)
		return _aligned_malloc ( size, alignment );
//#else
//		return ::malloc(size);
//#endif
	}

	void MemoryPool::system_free( void *data )
	{
//#if defined(WIN32_)
		_aligned_free ( data );
//#else
//		::free(data);
//#endif

	}


	//MemoryPool *MemoryPool::getSingletonPtr()
	//{
	//	static MemoryPool s_memory_instance;
	//	return &s_memory_instance;
	//}

}; // namespace SimStep