#ifndef ALIGNED_STACK_ALLOCATOR
#define ALIGNED_STACK_ALLOCATOR


#include <new.h>
#include <memory>


namespace SimStep {


	template<class T, std::size_t alignment = 0>
	class AlignedStackAllocator   : public std::allocator<T>
	{

	public:
		typedef const T*         const_pointer;
		typedef const T&         const_reference;
		typedef T*               pointer;
		typedef T&               reference;
		typedef T                value_type;

		typedef std::size_t		size_type;
		typedef std::ptrdiff_t	difference_type;

		typedef AlignedStackAllocator< T , alignment > self_type;
		typedef std::allocator<T> BaseAllocator;

		size_t Alignment;
		size_t usedMem;
		unsigned int Nb_Allocations;

		template<class _Other>
		struct rebind
		{	// convert an allocator<void> to an allocator <_Other>
			typedef AlignedStackAllocator<_Other> other;
		};


		AlignedStackAllocator(std::size_t alignment = 0)
		{
			Alignment = alignment;
			usedMem = 0;		
			Nb_Allocations = 0;
		};

		//AlignedStackAllocator()
		//{
		//	usedMem = 0;
		//	Alignment = alignment;
		//};



		const_pointer address(const_reference _Val) const
		{	// return address of nonmutable _Val
			return (&_Val);
		}

		template<class T, std::size_t alignment>
		AlignedStackAllocator(const AlignedStackAllocator<T,alignment>& ) 
		{	// construct by copying (do nothing)
			Alignment = alignment;
			usedMem = 0;		
			Nb_Allocations = 0;
		}

		template<class _Other>
		AlignedStackAllocator(const AlignedStackAllocator<_Other>&)
		{	// construct from a related allocator (do nothing)
		}

		template<class _Other>
		AlignedStackAllocator<T>& operator=(const AlignedStackAllocator<_Other>&)
		{	// assign from a related allocator (do nothing)
			return (*this);
		}

		void deallocate(pointer _Ptr, size_type _count)
		{	
			// deallocate object at _Ptr, ignore size
			::_freea( _Ptr );
		}

		pointer allocate(size_type _Count)
		{	// allocate array of _Count elements
			// allocate storage for _Count elements of type _Ty
			if (Alignment == 0)
			{
				usedMem += _Count * sizeof (T);
				return (T *)_malloca(_Count * sizeof (T));
			}
			//else
			//{
			//	usedMem += _Count * sizeof (T) + Alignment -1;
			//	T* mem = (T *)_malloca( _Count * sizeof (T) + Alignment -1 );
			//	return (T *) ( ( (int)mem + align16 - 1) & ~(align16 - 1) );
			//}

			Nb_Allocations++;

		}

		pointer allocate(size_type _Count, const void _FARQ *)
		{	// allocate array of _Count elements, ignore hint
			return (allocate(_Count));
		}

		void construct(pointer _Ptr, const T& _Val)
		{	// construct object at _Ptr with value _Val
			//_Construct(_Ptr, _Val);
			new(_Ptr) value_type(_Val);
		}

		void destroy(pointer _Ptr)
		{	// destroy object at _Ptr
			//_Destroy(_Ptr);
			_Ptr->~value_type();
		}

		size_type  max_size() const 
		{
			return static_cast<size_type>(-1) /sizeof(T); //	BaseAllocator::max_size();; //
		}


	};

}; // namespace SimStep


#endif