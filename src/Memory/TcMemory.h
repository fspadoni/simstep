#ifndef SimStep_TcMemory_h__
#define SimStep_TcMemory_h__

#include <Utilities/Singleton.h>
#include <Memory/MemoryManager.h>

#include <tcmalloc.h>

namespace SimStep
{


	class TcMemory : public Memory
	{
	public:

		TcMemory();

		~TcMemory();
	
		//static TcMemory& getSingleton(void);

		//static TcMemory* getSingletonPtr(void);
	
		void* allocateBytes(size_t byte_size );

		void* alignedAllocateBytes(size_t byte_size, size_t alignment );

		void* cacheAlignedAllocateBytes(int byte_size );

		void deallocateBytes(void* ptr );

		void alignedDeallocateBytes(void* ptr );

		void cacheAlignedDeallocateBytes(void* ptr );

		size_t getMemoryUsedSize(void);

		size_t getMemoryAllocatedSize(void);


	};


	
	class TCMallocPolicy
	{
	public:
		static inline void* allocateBytes(size_t count,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = tc_malloc(count);
			// this alloc policy doesn't do pools
			MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);
			return ptr;
		}

		static inline void* allocateAlignBytes(size_t count, size_t alignment = 128,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = tc_memalign( alignment, count );
			// this alloc policy doesn't do pools
			MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);
			return ptr;
		}

		static inline void deallocateBytes(void* ptr,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			MemoryTracker::get()._recordDealloc(ptr);
			tc_free(ptr);
		}

		static inline void deallocateBytes(void* ptr )
		{
			tc_free(ptr);
		}


		static inline void deallocateAlignBytes(void* ptr,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			MemoryTracker::get()._recordDealloc(ptr);
			tc_free(ptr);
		}

		/// Get the maximum size of a single allocation
		static inline size_t getMaxAllocationSize()
		{
			//return std::numeric_limits<size_t>::max();
			return 0;
		}
	private:
		// no instantiation
		TCMallocPolicy()
		{ 
			//const char *dmodule = "libtcmalloc_minimal-debug.dll";
			//HMODULE module = LoadLibrary( dmodule );
			//if ( module != NULL )
			//{
			//	void* TCmalloc = GetProcAddress(module,"tc_malloc");
			//	void *TCfree = GetProcAddress(module,"tc_free");
			//	if ( proc )
			//	{
			//		typedef NxTetraInterface * (__cdecl * NX_GetToolkit)();
			//		ret = ((NX_GetToolkit)proc)();
			//	}
			//}
		}

	};


	//MemAllocator<TCMallocPolicy> TCMallocator;

	
}; // namespace SimStep

#endif // SimStep_TcMemory_h__