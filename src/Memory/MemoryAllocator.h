#ifndef SimStep_MemoryAllocator_h__
#define SimStep_MemoryAllocator_h__

#include "Memory/MemoryTracker.h"

#include "Memory/AlignedAllocator.h"

#  define WIN32_LEAN_AND_MEAN
#  define NOMINMAX // required to stop windows.h messing up std::min
#include <limits>

namespace SimStep
{


	//Memory allocator interface
	class _SimStepExport Allocator
	{

	public:
		virtual const String& getName(void) const = 0;

	private:
		//pure virtual
		virtual void* allocateBytes(size_t byte_size ) = 0;

		virtual void* alignedAllocateBytes(size_t byte_size, size_t alignment ) = 0;
		

		virtual void deallocateBytes(void* ptr) = 0;

		virtual void alignedDeallocateBytes(void* ptr ) = 0;

		friend class Memory;
	};


	//Memory allocator interface
	class _SimStepExport SystemAllocator : public Allocator
	{
	
		String mName;

	public:

		SystemAllocator() : Allocator(), mName("SystemAllocator") {}

		inline const String& getName(void) const { return mName; }

		
	private:

		//pure virtual
		void* allocateBytes(size_t byte_size )
		{
			return ::malloc(byte_size);
		}

		void* alignedAllocateBytes(size_t byte_size, size_t alignment )
		{
			return ::_aligned_malloc(byte_size, alignment);
		}


		void deallocateBytes(void* ptr)
		{
			::free(ptr);
		}

		void alignedDeallocateBytes(void* ptr )
		{
			::_aligned_free(ptr);
		}

	};

	//const String SystemAllocator::mName = "SystemAllocator";


	/**	A "standard" allocation policy for use with AllocatedObject and 
	STLAllocator. This is the class that actually does the allocation
	and deallocation of physical memory, and is what you will want to 
	provide a custom version of if you wish to change how memory is allocated.
	@par
	This class just delegates to the global malloc/free.
	*/
	class _SimStepExport StdAllocPolicy
	{
	public:
		static inline void* allocateBytes(size_t count,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = malloc(count);

			// this alloc policy doesn't do pools
			MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);

			return ptr;
		}

		static inline void deallocateBytes(void* ptr)
		{

			MemoryTracker::get()._recordDealloc(ptr);

			free(ptr);
		}


		static inline void* alignedAllocateBytes(size_t count, size_t alignment,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = _aligned_malloc(count, alignment);

			// this alloc policy doesn't do pools
			MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);

			return ptr;
		}

		static inline void alignedDeallocateBytes(void* ptr)
		{

			MemoryTracker::get()._recordDealloc(ptr);

			_aligned_free(ptr);
		}

		/// Get the maximum size of a single allocation
		static inline size_t getMaxAllocationSize()
		{
			//return std::numeric_limits<size_t>::max();
			return 0;
		}
	private:
		// no instantiation
		StdAllocPolicy()
		{ }
	};

	/**	A "standard" allocation policy for use with AllocatedObject and 
	STLAllocator, which aligns memory at a given boundary (which should be
	a power of 2). This is the class that actually does the allocation
	and deallocation of physical memory, and is what you will want to 
	provide a custom version of if you wish to change how memory is allocated.
	@par
	This class just delegates to the global malloc/free, via AlignedMemory.
	@note
	template parameter Alignment equal to zero means use default
	platform dependent alignment.

	*/
	template <size_t Alignment = 0>
	class StdAlignedAllocPolicy
	{
	public:
		// compile-time check alignment is available.
		typedef int IsValidAlignment
			[Alignment <= 128 && ((Alignment & (Alignment-1)) == 0) ? +1 : -1];

		static inline void* allocateBytes(size_t count,
			const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = Alignment ? AlignedMemory::allocate(count, Alignment)
				: AlignedMemory::allocate(count);

			// this alloc policy doesn't do pools
			MemoryTracker::get()._recordAlloc(ptr, count, 0, file, line, func);

			return ptr;
		}

		static inline void deallocateBytes(void* ptr)
		{

			MemoryTracker::get()._recordDealloc(ptr);

			AlignedMemory::deallocate(ptr);
		}

		/// Get the maximum size of a single allocation
		static inline size_t getMaxAllocationSize()
		{
			return std::numeric_limits<size_t>::max();
		}
	private:
		// No instantiation
		StdAlignedAllocPolicy()
		{ }

	};


	
}; // namespace SimStep

#endif // SimStep_MemoryAllocator_h__