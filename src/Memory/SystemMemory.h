#ifndef SimStep_SystemMemory_h__
#define SimStep_SystemMemory_h__

#include <Memory/MemoryManager.h>

namespace SimStep
{


	class SystemMemory : public Memory
	{
	public:

		SystemMemory();

		~SystemMemory();

		//static TcMemory& getSingleton(void);

		//static TcMemory* getSingletonPtr(void);

		void* allocateBytes(size_t byte_size );

		void* alignedAllocateBytes(size_t byte_size, size_t alignment );

		void* cacheAlignedAllocateBytes(int byte_size );

		void deallocateBytes(void* ptr );

		void alignedDeallocateBytes(void* ptr );

		void cacheAlignedDeallocateBytes(void* ptr );

		size_t getMemoryUsedSize(void);

		size_t getMemoryAllocatedSize(void);


	};

	
}; // namespace SimStep

#endif // SimStep_SystemMemory_h__