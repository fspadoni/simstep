#ifndef SimStep_MemoryAllocatorConfig_h__
#define SimStep_MemoryAllocatorConfig_h__

#include "Memory/MemoryAllocatedObject.h"

#include "Memory/MemoryAllocator.h"

//#include "Memory/MemoryManager.h"

namespace SimStep
{
	enum MemoryCategory
	{
		
		// DUMMY
		MEMCATEGORY_DUMMY = -1,
		//MEMORY_CLASS_UNKNOWN = 0,
		//MEMORY_CLASS_DUMMY,
		/// General purpose
		MEMCATEGORY_GENERAL = 0,
		MEMCATEGORY_ROOT = 1,

		/// Geometry held in main memory
		MEMCATEGORY_GEOMETRY = 2, 
		/// Animation data like tracks, bone matrices
		MEMCATEGORY_SIMD_BUFFER = 3, 
		/// Scene object instances
		MEMCATEGORY_SCENE_OBJECTS = 4,
		/// Other resources
		MEMCATEGORY_RESOURCE = 5,
		/// Scripting
		MEMCATEGORY_SCRIPTING = 6,
		/// Rendersystem structures
		MEMCATEGORY_RENDERSYS = 7,

		MEMCATEGORY_MATH = 8,

		MEMCATEGORY_PHYSICS = 9,

		// sentinel value, do not use 
		MEMCATEGORY_COUNT = 10
	};


	//template <MemoryCategory Cat> class CategorisedAllocPolicy : public StdAllocPolicy{};
	//template <MemoryCategory Cat, size_t align = 16> class CategorisedAlignAllocPolicy : public StdAlignedAllocPolicy<align>{};
	
	//template <MemoryCategory Cat, size_t align = 0> class testCategorisedAllocPolicy: public StdAlignedAllocPolicy<align>{};
	//// specialize template
	//template <MemoryCategory Cat, 0> class testCategorisedAllocPolicy
	//{

	//};
	//template <MemoryCategory Cat, align16> class testCategorisedAllocPolicy : public StdAlignedAllocPolicy<16>{};
	//template <MemoryCategory Cat, 32 > class testCategorisedAllocPolicy : public StdAlignedAllocPolicy<32>{};
	//template <MemoryCategory Cat, 64 > class testCategorisedAllocPolicy : public StdAlignedAllocPolicy<64>{};


	//typedef CategorisedAllocPolicy<MEMCATEGORY_GENERAL> GeneralAllocPolicy;
	//typedef AllocatedObject<GeneralAllocPolicy> GeneralAllocatedObject;


	//typedef CategorisedAlignAllocPolicy<MEMCATEGORY_SIMD_BUFFER, 16> SimdBufferAllocPolicy;
	//typedef AllocatedObject<SimdBufferAllocPolicy> SimdBufferAllocatedObject;

}; // namespace SimStep

#ifdef _DEBUG
	// Allocate a block of raw memory aligned to SIMD boundaries, and indicate the category of usage
	#define newSimStep new( __FILE__, __LINE__, __FUNCTION__)
	#define deleteSimStep  delete
#else
	//#define newSimStep new
	#define newSimStep new( __FILE__, __LINE__, __FUNCTION__)
	#define deleteSimStep  delete
#endif
//static inline void* newSimStep() 
//{
//	new 
//}
//#define newSimStepAlloc(T, category) new (::SimStep::CategorisedAllocPolicy<category>::allocateBytes(sizeof(T), __FILE__, __LINE__, __FUNCTION__)) T
//#define newSimStepAllocT(T, size, category) new (::SimStep::CategorisedAllocPolicy<category>::allocateBytes( size, __FILE__, __LINE__, __FUNCTION__)) T
//// aligned allocation
//#define newSimStepSimdAlloc(bytes,category)  (::SimStep::CategorisedAlignAllocPolicy<category>::allocateBytes( bytes, __FILE__, __LINE__, __FUNCTION__))
//
//#define newSimStepAlignedAlloc(bytes,category,align)  (::SimStep::CategorisedAlignAllocPolicy<category, align>::allocateBytes( bytes, __FILE__, __LINE__, __FUNCTION__))
//
//// aligned Deallocation
//#define freeSimStepSimdAlloc(ptr, category) ::SimStep::CategorisedAlignAllocPolicy<category>::deallocateBytes(ptr)
///// Free the memory allocated with either OGRE_MALLOC_ALIGN or OGRE_ALLOC_T_ALIGN. Category is required to be restated to ensure the matching policy is used
//#define freeSimStepAlignedAlloc(ptr, category, align) ::SimStep::CategorisedAlignAllocPolicy<category, align>::deallocateBytes(ptr)


//
//#ifdef _DEBUG
////#   define DECLARE_CLASS_ALLOCATOR(category)
//#   define DECLARE_NONVIRTUAL_CLASS_ALLOCATOR(category, Class) \
//		DECLARE_NONVIRTUAL_CLASS_ALLOCATOR_BY_SIZE_UNCHECKED(category, sizeof(Class))
//#else
////#   define DECLARE_CLASS_ALLOCATOR(category)
//#   define DECLARE_NONVIRTUAL_CLASS_ALLOCATOR(category,Class) \
//		DECLARE_NONVIRTUAL_CLASS_ALLOCATOR_BY_SIZE_UNCHECKED(category, sizeof(Class))
//#endif
//
//#define DECLARE_NONVIRTUAL_CLASS_ALLOCATOR_BY_SIZE_UNCHECKED(category,ClassSize) \
//	void* operator new(size_t sz, const char* file, int line, const char* func) \
//		{ return SimStep::testCategorisedAllocPolicy<category>::allocateBytes(sz, file, line, func);} \
//	void* operator new(size_t sz) \
//		{ return SimStep::testCategorisedAllocPolicy<category>::allocateBytes(sz,  __FILE__, __LINE__, __FUNCTION__);} \
//	void* operator new(size_t sz, void* ptr) \
//		{ return ptr;} \
//	void* operator new[] ( size_t sz, const char* file, int line, const char* func ) \
//		{ return SimStep::testCategorisedAllocPolicy<category>::allocateBytes(sz, file, line, func);} \
//	void* operator new[] ( size_t sz ) \
//		{ return SimStep::testCategorisedAllocPolicy<category>::allocateBytes(sz, __FILE__, __LINE__, __FUNCTION__);} \
//	void operator delete( void* ptr ) \
//		{ SimStep::testCategorisedAllocPolicy<category>::deallocateBytes(ptr);} \
//	void operator delete( void* ptr, void* ) \
//		{ SimStep::testCategorisedAllocPolicy<category>::deallocateBytes(ptr);} \
//	void operator delete( void* ptr, const char* , int , const char*  ) \
//		{ SimStep::testCategorisedAllocPolicy<category>::deallocateBytes(ptr);} \
//	void operator delete[] ( void* ptr ) \
//		{ SimStep::testCategorisedAllocPolicy<category>::deallocateBytes(ptr);} \
//	void operator delete[] ( void* ptr, const char* , int , const char*  ) \
//		{ SimStep::testCategorisedAllocPolicy<category>::deallocateBytes(ptr);} \
//
//	//inline void* operator new( size_t nbytes)	{ assert( nbytes == ClassSize ); return ::SimStep::CategorisedAllocPolicy<category>::allocateBytes(nbytes, __FILE__, __LINE__, __FUNCTION__); }	\
//	//inline void  operator delete(void* p)			{ return ::SimStep::CategorisedAllocPolicy<category>::deallocateBytes(p); }	 \
//	//inline void* operator new[]( size_t nbytes)	{ return ::SimStep::CategorisedAllocPolicy<category>::allocateBytes( nbytes, __FILE__, __LINE__, __FUNCTION__); }	 \
//	//inline void  operator delete[](void* p)		{ return ::SimStep::CategorisedAllocPolicy<category>::deallocateBytes(p); } \
//	//inline void* operator new( size_t n, void* p){ assert(  n == ClassSize); return p; } \
//	//inline void* operator new[]( size_t n, void* p){ return p;	} \
//	////DECLARE_OPERATOR_NONVIRTUAL_DELETE
//
//
//#define DECLARE_OPERATOR_DELETE \
//	inline void  operator delete(void*, void*)		{ }	\
//	inline void  operator delete[](void*, void*)	{ }
//#define DECLARE_OPERATOR_NONVIRTUAL_DELETE \
//	inline void  operator delete(void*, void*)		{ } \
//	inline void  operator delete[](void*, void*)	{ }


//	/// Convenience function to allocate memory of the correct type
//	//template <typename TYPE>
//#define SimStepMalloc( T, nbytes, category ) \
//	(static_cast<T*>(SimStep::Memory::getSingleton().allocateBytes(nbytes*sizeof(T), category, __FILE__, __LINE__, __FUNCTION__) ) );
//
//
//	/// Convenience function to deallocate memory of the correct type
//	//template <typename TYPE>
//#define SimStepFree( ptr, T, nbytes, category ) \
//	SimStep::Memory::getSingleton().deallocateBytes(static_cast<void*>(ptr), nbytes*sizeof(T), category);
//
//
//
//#define SimStepMallocShare( T, nbytes, category ) \
//	(static_cast<T*>(SimStep::Memory::getSingleton().cacheAlignedAllocateBytes(nbytes*sizeof(T), category, __FILE__, __LINE__, __FUNCTION__) ) );
//
//
//	/// Convenience function to deallocate memory of the correct type
//	//template <typename TYPE>
//#define SimStepFreeShare( ptr, T, nbytes, category ) \
//	SimStep::Memory::getSingleton().cacheAlignedDeallocateBytes(static_cast<void*>(ptr), nbytes*sizeof(T), category);
//



#endif // SimStep_MemoryAllocatorConfig_h__