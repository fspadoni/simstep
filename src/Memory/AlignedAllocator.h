#ifndef SimStep_AlignedAllocator_h__
#define SimStep_AlignedAllocator_h__




#include <new.h>
#include <memory>

#include "Utilities/Common.h"

namespace SimStep {


	class _SimStepExport AlignedMemory
	{
	public:
		/** Allocate memory with given alignment.
		@param
		size The size of memory need to allocate.
		@param
		alignment The alignment of result pointer, must be power of two
		and in range [1, 128].
		@returns
		The allocated memory pointer.
		@par
		On failure, exception will be throw.
		*/
		static void* allocate(size_t size, size_t alignment);

		/** Allocate memory with default platform dependent alignment.
		@remarks
		The default alignment depend on target machine, this function
		guarantee aligned memory according with SIMD processing and
		cache boundary friendly.
		@param
		size The size of memory need to allocate.
		@returns
		The allocated memory pointer.
		@par
		On failure, exception will be throw.
		*/
		static void* allocate(size_t size);

		/** Deallocate memory that allocated by this class.
		@param
		p Pointer to the memory allocated by this class or <b>NULL</b> pointer.
		@par
		On <b>NULL</b> pointer, nothing happen.
		*/
		static void deallocate(void* p);
	};



	template<class T, std::size_t alignment = 0>
	class AlignedAllocator   : public std::allocator<T>
	{

	public:
		typedef const T*         const_pointer;
		typedef const T&         const_reference;
		typedef T*               pointer;
		typedef T&               reference;
		typedef T                value_type;

		typedef std::size_t		size_type;
		typedef std::ptrdiff_t	difference_type;

		typedef AlignedAllocator< T , alignment > self_type;
		typedef std::allocator<T> BaseAllocator;

		size_t Alignment;
		size_t usedMem;
		unsigned int Nb_Allocations;

		template<class _Other>
		struct rebind
		{	// convert an allocator<void> to an allocator <_Other>
			typedef AlignedAllocator<_Other> other;
		};


		AlignedAllocator(std::size_t alignment = 0)
		{
			Alignment = alignment;
			usedMem = 0;		
			Nb_Allocations = 0;
		};

		//AlignedAllocator()
		//{
		//	usedMem = 0;
		//	Alignment = alignment;
		//};



		const_pointer address(const_reference _Val) const
		{	// return address of nonmutable _Val
			return (&_Val);
		}

		template<class T, std::size_t alignment>
		AlignedAllocator(const AlignedAllocator<T,alignment>& ) 
		{	// construct by copying (do nothing)
			Alignment = alignment;
			usedMem = 0;		
			Nb_Allocations = 0;
		}

		template<class _Other>
		AlignedAllocator(const AlignedAllocator<_Other>&)
		{	// construct from a related allocator (do nothing)
		}

		template<class _Other>
		AlignedAllocator<T>& operator=(const AlignedAllocator<_Other>&)
		{	// assign from a related allocator (do nothing)
			return (*this);
		}

		void deallocate(pointer _Ptr, size_type _count)
		{	// deallocate object at _Ptr, ignore size
			//if (Alignment == 0)
			//{
			//	usedMem -= _count* sizeof(T);
			//	::operator delete(_Ptr);
			//}
			//else
			//{
			usedMem -= _count* sizeof(T);

#ifdef _DEBUG
			::_aligned_free_dbg(_Ptr);
#else
			::_aligned_free(_Ptr);
#endif
			//}

		}

		pointer allocate(size_type _Count)
		{	// allocate array of _Count elements
			// allocate storage for _Count elements of type _Ty
			//if (Alignment == 0)
			//{
			//	usedMem += _Count * sizeof (T);
			//	return (T *)::operator new(_Count * sizeof (T));
			//}
			//else
			usedMem += _Count * sizeof(T);

#ifdef _DEBUG
			return (T *)::_aligned_malloc_dbg(_Count*sizeof(T), Alignment, __FILE__, __LINE__);
			//// chiamare il construct ?
			//new( .. ) value_type( .. );
#else
			return (T *)::_aligned_malloc(_Count * sizeof(T), Alignment);
			//// chiamare il construct ?
			//new( .. ) value_type( .. );
#endif

			Nb_Allocations++;

		}

		pointer allocate(size_type _Count, const void*)
		{	// allocate array of _Count elements, ignore hint
			return (allocate(_Count));
		}

		void construct(pointer _Ptr, const T& _Val)
		{	// construct object at _Ptr with value _Val
			//_Construct(_Ptr, _Val);
			new(_Ptr) value_type(_Val);
		}

		void destroy(pointer _Ptr)
		{	// destroy object at _Ptr
			//_Destroy(_Ptr);
			_Ptr->~value_type();
		}

		size_type  max_size() const 
		{
			return static_cast<size_type>(-1) /sizeof(T); //	BaseAllocator::max_size();; //
		}


	};

}; // namespace SimStep

#endif // SimStep_AlignedAllocator_h__