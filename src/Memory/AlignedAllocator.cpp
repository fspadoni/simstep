#include "Memory/AlignedAllocator.h"
#include <Memory/MemoryManager.h>


namespace SimStep
{

	void* AlignedMemory::allocate(size_t size, size_t alignment)
	{
		assert(0 < alignment && alignment <= 128 && ( (alignment & (Memory::SIMD_ALIGN -1)) == 0 ) );

		unsigned char* p = new unsigned char[size + alignment];
		size_t offset = alignment - (size_t(p) & (alignment-1));

		unsigned char* result = p + offset;
		result[-1] = (unsigned char)offset;

		return result;
	}


	void* AlignedMemory::allocate(size_t size)
	{
		return allocate(size, 8); // new unsigned char[size];
	}


	void AlignedMemory::deallocate(void* p)
	{
		if (p)
		{
			unsigned char* mem = (unsigned char*)p;
			mem = mem - mem[-1];
			delete [] mem;
		}
	}

	
}; // namespace SimStep