#ifndef SimStep_ArchiveManager_h__
#define SimStep_ArchiveManager_h__

#include <Utilities/Common.h>
#include <Utilities/Singleton.h>
#include <Utilities/FactoryObj.h>
#include <Core/ResourceManager.h>

#include <map>

namespace SimStep
{

	// forward decl:
	class Archive;
	


	/** \addtogroup Core
	*  @{
	*/
	/** \addtogroup Resources
	*  @{
	*/
	/** Abstract factory class, archive codec plugins can register concrete
	subclasses of this.
	@remarks
	All access to 'archives' (collections of files, compressed or
	just folders, maybe even remote) is managed via the abstract
	Archive class. Plugins are expected to provide the
	implementation for the actual codec itself, but because a
	subclass of Archive has to be created for every archive, a
	factory class is required to create the appropriate subclass.
	@par
	So archive plugins create a subclass of Archive AND a subclass
	of ArchiveFactory which creates instances of the Archive
	subclass. See the 'Zip' and 'FileSystem' plugins for examples.
	Each Archive and ArchiveFactory subclass pair deal with a
	single archive type (identified by a string).
	*/
	class _SimStepExport ArchiveFactory : public FactoryObj< Archive >
	{
	public:
		virtual ~ArchiveFactory() {}
		// No methods, must just override all methods inherited from FactoryObj
	};



	/** \addtogroup Core
	*  @{
	*/
	/** \addtogroup Resources
	*  @{
	*/
	/** This class manages the available ArchiveFactory plugins. 
	*/
	class _SimStepExport ArchiveManager : public Singleton<ArchiveManager>
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( ArchiveManager, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );

	protected:
		typedef std::map<String, ArchiveFactory*> ArchiveFactoryMap;
		/// Factories available to create archives, indexed by archive type (String identifier e.g. 'Zip')
		ArchiveFactoryMap mArchFactories;
		/// Currently loaded archives
		typedef std::map<String, Archive*> ArchiveMap;
		ArchiveMap mArchives;

	public:
		/** Default constructor - should never get called by a client app.
		*/
		ArchiveManager();
		/** Default destructor.
		*/
		virtual ~ArchiveManager();

		/** Opens an archive for file reading.
		@remarks
		The archives are created using class factories within
		extension libraries.
		@param filename
		The filename that will be opened
		@param refLibrary
		The library that contains the data-handling code
		@returns
		If the function succeeds, a valid pointer to an Archive
		object is returned.
		@par
		If the function fails, an exception is thrown.
		*/
		Archive* load( const String& filename, const String& archiveType);

		/** Unloads an archive.
		@remarks
		You must ensure that this archive is not being used before removing it.
		*/
		void unload(Archive* arch);
		/** Unloads an archive by name.
		@remarks
		You must ensure that this archive is not being used before removing it.
		*/
		void unload(const String& filename);
		typedef MapIterator<ArchiveMap> ArchiveMapIterator;
		/** Get an iterator over the Archives in this Manager. */
		ArchiveMapIterator getArchiveIterator(void);

		/** Adds a new ArchiveFactory to the list of available factories.
		@remarks
		Plugin developers who add new archive codecs need to call
		this after defining their ArchiveFactory subclass and
		Archive subclasses for their archive type.
		*/
		void addArchiveFactory(ArchiveFactory* factory);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static ArchiveManager& getSingleton(void);
		/** Override standard Singleton retrieval.
		@remarks
		Why do we do this? Well, it's because the Singleton
		implementation is in a .h file, which means it gets compiled
		into anybody who includes it. This is needed for the
		Singleton template to work, but we actually only want it
		compiled into the implementation of the class based on the
		Singleton, not all of them. If we don't change this, we get
		link errors when trying to use the Singleton-based class from
		an outside dll.
		@par
		This method just delegates to the template version anyway,
		but the implementation stays in this single compilation unit,
		preventing link errors.
		*/
		static ArchiveManager* getSingletonPtr(void);
	};

	



}; // namespace SimStep

#endif // SimStep_ArchiveManager_h__