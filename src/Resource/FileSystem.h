#ifndef SimStep_FileSystem_h__
#define SimStep_FileSystem_h__

#include <Utilities/Common.h>
#include <Resource/Archive.h>
#include <Resource/ArchiveManager.h>
#include <Utilities/DataStream.h>
#include <Utilities/StringVector.h>

namespace SimStep
{

	/** \addtogroup Core
	*  @{
	*/
	/** \addtogroup Resources
	*  @{
	*/
	/** Specialisation of the Archive class to allow reading of files from 
	filesystem folders / directories.
	*/
	class _SimStepExport FileSystemArchive : public Archive 
	{
	public:
		
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( FileSystemArchive, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );
	
	protected:
		/** Utility method to retrieve all files in a directory matching pattern.
		@param pattern File pattern
		@param recursive Whether to cascade down directories
		@param dirs Set to true if you want the directories to be listed
		instead of files
		@param simpleList Populated if retrieving a simple list
		@param detailList Populated if retrieving a detailed list
		@param currentDir The current directory relative to the base of the 
		archive, for file naming
		*/
		void findFiles(const String& pattern, bool recursive, bool dirs,
			StringVector* simpleList, FileInfoList* detailList);

		//OGRE_AUTO_MUTEX
	public:
		FileSystemArchive(const String& name, const String& archType );
		~FileSystemArchive();

		/// @copydoc Archive::isCaseSensitive
		bool isCaseSensitive(void) const;

		/// @copydoc Archive::load
		void load();
		/// @copydoc Archive::unload
		void unload();

		/// @copydoc Archive::open
		DataStreamPtr open(const String& filename, bool readOnly = true) const;

		/// @copydoc Archive::create
		DataStreamPtr create(const String& filename) const;

		/// @copydoc Archive::delete
		void remove(const String& filename) const;

		/// @copydoc Archive::list
		StringVectorPtr list(bool recursive = true, bool dirs = false);

		/// @copydoc Archive::listFileInfo
		FileInfoListPtr listFileInfo(bool recursive = true, bool dirs = false);

		/// @copydoc Archive::find
		StringVectorPtr find(const String& pattern, bool recursive = true,
			bool dirs = false);

		/// @copydoc Archive::findFileInfo
		FileInfoListPtr findFileInfo(const String& pattern, bool recursive = true,
			bool dirs = false);

		/// @copydoc Archive::exists
		bool exists(const String& filename);

		/// @copydoc Archive::getModifiedTime
		time_t getModifiedTime(const String& filename);

		/// Set whether filesystem enumeration will include hidden files or not.
		/// This should be called prior to declaring and/or initializing filesystem
		/// resource locations. The default is true (ignore hidden files).
		static void setIgnoreHidden(bool ignore)
		{
			ms_IgnoreHidden = ignore;
		}

		/// Get whether hidden files are ignored during filesystem enumeration.
		static bool getIgnoreHidden()
		{
			return ms_IgnoreHidden;
		}

		static bool ms_IgnoreHidden;
	};

	/** Specialisation of ArchiveFactory for FileSystem files. */
	//class _OgrePrivate FileSystemArchiveFactory : public ArchiveFactory
	class _SimStepExport FileSystemArchiveFactory : public ArchiveFactory
	{

	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( FileSystemArchiveFactory, Memory::CACHE_ALIGN, MEMCATEGORY_RESOURCE );


		virtual ~FileSystemArchiveFactory() {}
		/// @copydoc FactoryObj::getType
		const String& getType(void) const;
		/// @copydoc FactoryObj::createInstance
		Archive *createInstance( const String& name ) 
		{
			return newSimStep FileSystemArchive(name, "FileSystem");
		}
		/// @copydoc FactoryObj::destroyInstance
		void destroyInstance( Archive* arch) { delete arch; }
	};
	



	
}; // namespace SimStep

#endif // SimStep_FileSystem_h__