#ifndef SimStep_SphPhyxelVector_h__
#define SimStep_SphPhyxelVector_h__


// Align16Obj
//#include "Utilities/AlignedObject.h"

namespace SimStep
{

	template< unsigned int size >
	class PhyxelVector //: public AlignedObject16
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( PhyxelStrainMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		SimdVector3 m[size];

		void print(const char* file_name )
		{
			union { __m128 v; float s[4]; } tmp;

			FILE* pFile;
			fopen_s(&pFile, file_name,"w");

			for (int i=0; i<size; i++ )
			{

				tmp.v = m[i].get128();
				fprintf(pFile, " %5.9f \n", tmp.s[0] );
				fprintf(pFile, " %5.9f \n", tmp.s[1] );
				fprintf(pFile, " %5.9f \n", tmp.s[2] );

			}

			fclose(pFile);
		}

	};

	struct Neighbors //: AlignedObject16
	{
		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Neighbors, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		union
		{
			struct
			{
				SimdVector3 pos;
				//SSE::SimdVector3 m_Position;
			};
			struct
			{
				int _float1, _float2, _float3, idx;
			};
		};

		inline Neighbors & Neighbors::operator =( const SimdVector3 & x )
		{
			pos = x;
			return *this;
		}

		inline Neighbors & Neighbors::operator =( const int & index )
		{
			idx = index;
			return *this;
		}

	};

	template< unsigned int size >
	class PhyxelNeighborsVector //: public AlignedObject16
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( PhyxelNeighborsVector, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		Neighbors m[size];

		void print(const char* file_name )
		{
			union { __m128 v; float s[4]; } tmp;

			FILE* pFile;
			fopen_s(&pFile, file_name,"w");

			for (int i=0; i<size; i++ )
			{

				tmp.v = m[i].get128();
				fprintf(pFile, " %5.9f \n", tmp.s[0] );
				fprintf(pFile, " %5.9f \n", tmp.s[1] );
				fprintf(pFile, " %5.9f \n", tmp.s[2] );

			}

			fclose(pFile);
		}

	};

	
}; // namespace SimStep

#endif // SimStep_SphPhyxelVector_h__