#include "SphSoftBody.h"

#include <World/SoftBodyParam.h>

#include "SphNeighbors.h"

#include <Core/SubTetraMesh.h>

#include <Utilities/StopWatch.h>

#include <algorithm>

#include <FemSoftBody/Decomposition.h>

#define align16 16

namespace SimStep
{


	const double PiG = 3.14159265358979323846264;
	const float KernelConstant = (float)(315.0/64.0/PiG);


	// polynomial Kernel
	//template<unsigned int NB_NEIGHBORS>
	//inline float SphSoftBody<NB_NEIGHBORS>::kernel( float& r2, const int& idx)
	//{
	//	return (   r2 < m_h2[idx] ) ?
	//		m_kernel_const[idx] * 
	//		powf( m_h2[idx] - r2 , 3 )
	//		: 
	//	0.0f ;
	//}

	// polynomial Kernel
	template<unsigned int NB_NEIGHBORS>
	inline float SphSoftBody<NB_NEIGHBORS>::kernel( float& r2, const int& idx)
	{
		return (   r2 < *mh2->lockForRead(idx) ) ?
			*mKernelConst->lockForRead(idx) * 
			powf( *mh2->lockForRead(idx) - r2 , 3 )
			: 
		0.0f ;
	}

	//template<unsigned int NB_NEIGHBORS>
	//inline SimdVector3 SphSoftBody<NB_NEIGHBORS>::gradKernel( SimdVector3& r, const int& idx)
	//{
	//	float r2 = SSE::lengthSqr(r);
	//	return (   r2 < m_h2[idx] ) ?
	//		r * m_kernel_gradconst[idx] 
	//	* powf( m_h2[idx] - r2 , 2 )
	//		: 
	//	SSE::Vector3(0.0);
	//}

	template<unsigned int NB_NEIGHBORS>
	inline SimdVector3 SphSoftBody<NB_NEIGHBORS>::gradKernel( SimdVector3& r, const int& idx)
	{
		float r2 = SSE::lengthSqr(r);
		return (   r2 < *mh2->lockForRead(idx) ) ?
			r * (*mKernelGradConst->lockForRead(idx)) 
		* powf( *mh2->lockForRead(idx) - r2 , 2 )
			: 
		SSE::Vector3(0.0);
	}

	//// Elasticity Kernel
	//template<unsigned int NB_NEIGHBORS>
	//inline float SphSoftBody<NB_NEIGHBORS>::elasticityKernel( float& r, const int& idx)
	//{
	//	return (   r*r < m_h2[idx] ) ?
	//		m_elastkernel_const1[idx] * cosf( (r + m_h[idx])*m_elastkernel_const2[idx] )
	//		+ m_elastkernel_const1[idx]
	//	: 
	//	0.0f ;
	//}
	// Elasticity Kernel
	template<unsigned int NB_NEIGHBORS>
	inline float SphSoftBody<NB_NEIGHBORS>::elasticityKernel( float& r, const int& idx)
	{
		return (   r*r < *mh2->lockForRead(idx) ) ?
			*mElastKernelConst1->lockForRead(idx) * cosf( (r + (*mh->lockForRead(idx)) )* (*mElastKernelConst2->lockForRead(idx)) )
			+ (*mElastKernelConst1->lockForRead(idx))
		: 
		0.0f ;
	}


	//template<unsigned int NB_NEIGHBORS>
	//inline SimdVector3 SphSoftBody<NB_NEIGHBORS>::gradElasticityKernel( SimdVector3& r, const int& idx)
	//{
	//	float r2 = SSE::lengthSqr(r);
	//	return (   r2 < m_h2[idx] ) ?	
	//		r * m_elastkernel_gradconst[idx] 
	//	* sinf( ( sqrt(r2) + m_h[idx])*m_elastkernel_const2[idx] )
	//		: 
	//	SSE::Vector3(0.0);
	//}

	template<unsigned int NB_NEIGHBORS>
	inline SimdVector3 SphSoftBody<NB_NEIGHBORS>::gradElasticityKernel( SimdVector3& r, const int& idx)
	{
		float r2 = SSE::lengthSqr(r);
		return (   r2 < *mh2->lockForRead(idx) ) ?	
			r * (*mElasticKernelGradConst->lockForRead(idx))
		* sinf( ( sqrt(r2) + (*mh->lockForRead(idx)) )* (*mElastKernelConst2->lockForRead(idx)) )
			: 
		SSE::Vector3(0.0);
	}


	template<unsigned int NB_NEIGHBORS>
	SphSoftBody<NB_NEIGHBORS>::SphSoftBody( const SoftBodyParams& params, const DataValuePairList* otherparams  )
		: SoftBody( params, otherparams )
	{

		create( params );


		//m_StrainMatrix = NULL;
		//m_X0 = NULL;
		//m_KX0 = NULL;
		//m_UndeformedState = NULL;
		//m_PhyxelStiffnessMatrix = NULL;
		//m_RKX0 = NULL;

		//m_Volume = NULL;

		// solver
		//m_NewPhyxelVelocity = NULL;
		//m_RHS_Vector = NULL;
		//m_ResidualVector = NULL;
		//m_DirectionVector = NULL;
		//m_TempVector = NULL;

		//m_StiffnessMatrix = NULL;
		//m_PhyxK2StiffnessMatrixMap = NULL;
		//m_Tetra2StiffnessMatrixMap = NULL;

	}



	template<unsigned int NB_NEIGHBORS>
	SphSoftBody<NB_NEIGHBORS>::~SphSoftBody()
	{

		//deAllocateMem();
	};

	// Public Interface
	template<unsigned int NB_NEIGHBORS>
	const String& SphSoftBody<NB_NEIGHBORS>::getType(void) const
	{
		return SphSoftBodyFactory<NB_NEIGHBORS>::FACTORY_TYPE_NAME;
	}


	// Public Interface

	template<unsigned int NB_NEIGHBORS>
	float SphSoftBody<NB_NEIGHBORS>::assembleStiffnessMatrix(void* user_pointer )
	{

		tick_count starTicks = tick_count::now();

		// Reset
		// Reset Stiffness Matrix and Load Vector
		mForces->reset();
		mStiffnessMatrix->reset();


		SimdMatrix3 rot, invRot;
		Quat quat;



		const Kphyxel<NB_NEIGHBORS+1>* mapbuf = 
			mPhyxK2StiffnessMatrixMap->lockForRead();

		const PhyxelStiffnessMatrix<NB_NEIGHBORS+1>* phyxelstiffnessbuf = 
			mKphyxel->lockForRead();

		const PhyxelVector<NB_NEIGHBORS+1>* pKX0buf = mKX0->lockForRead();

		const PhyxelNeighborsVector<NB_NEIGHBORS+1>* pX0buf = mX0->lockForRead();

		const float* massbuf = mNodeMasses->lockForRead<float>();

		const SimdMatrix3* undeformedbuf = mUndeformedStateVector->lockForRead<SimdMatrix3>();
		
		const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();



		SimdVector3* forcebuf = mForces->lockForWrite<SimdVector3>();
		SimdMatrix3* stiffnessbuff = mStiffnessMatrix->lockForWrite<SimdMatrix3>();
		SimdMatrix3* rotationbuf = mPhyxelRotations->lockForWrite<SimdMatrix3>();

		
		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{

			memset( (void*)&rot, 0, sizeof(SimdMatrix3));

			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *pX0buf;
			

			//SimdVector3& Xi = reinterpret_cast<SimdVector3&>(mNodes[PhyxIdx]);
			//SimdVector3& Xi0 = X0.m[0].pos;

			const SimdVector3& Xi = nodebuf[PhyxIdx];
			const SimdVector3& Xi0 = pX0buf->m[0].pos;

			const SimdMatrix3& UndeformedState = *undeformedbuf;

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{

				//const int& neigh_idx = X0.m[j+1].idx;
				const int& neigh_idx = pX0buf->m[j+1].idx;

				//SimdVector3& Xj =  reinterpret_cast<SimdVector3&>(mNodes[neigh_idx]);//  neighbors[j].m_idx; // .m_Position;
				//SimdVector3& Xj0 = X0.m[j+1].pos;
				const SimdVector3& Xj =  nodebuf[neigh_idx];//  neighbors[j].m_idx; // .m_Position;
				const SimdVector3& Xj0 = pX0buf->m[j+1].pos;

				rot += (*massbuf) *
					kernel( (float&)SSE::lengthSqr( Xj0 - Xi0 ), PhyxIdx ) * SSE::outer( Xj - Xi  , Xj0 - Xi0 ); //  * UndeformedState;
				// ElasticityKernel( (float&)SSE::lengthSqr( Xj0 - Xi0 ), PhyxIdx ) * SSE::outer( Xj - Xi  , Xj0 - Xi0 ); //  * UndeformedState;


			}

			rot *= UndeformedState;

			//*rotationbuf = rot = reinterpret_cast<const SimdMatrix3&>(
			//	PolarDecomposeRotationPart(reinterpret_cast<const Matrix3&>(rot)) );

			quat = Quat( reinterpret_cast<const Matrix3&>(rot) );

			quat = normalize( quat );

			*rotationbuf = rot = SimdMatrix3( reinterpret_cast<const SimdQuat&>(quat) );
		
			invRot = SSE::transpose(rot);



			for (int i=0; i<NB_NEIGHBORS+1; ++i)
			{

				for (int j=0; j<NB_NEIGHBORS+1; ++j)
				{

					//*m_PhyxK2StiffnessMatrixMap[PhyxIdx].ij[i][j] 
					//+= rot * m_PhyxelStiffnessMatrix[PhyxIdx].m[i][j] *  invRot;

					stiffnessbuff[ mapbuf->ij[i][j] ] += rot * phyxelstiffnessbuf->m[i][j] *  invRot;


					//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\K.txt");

				}

				//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\K.txt");
			}

			//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\K.txt");

			//m_RKX0[PhyxIdx] -= 
			//	rot * m_KX0[PhyxIdx].m[0];

			//for (int i=0; i<NB_NEIGHBORS; i++)
			//{

			//	const int& neigh_idx = m_X0[PhyxIdx].m[i+1].idx;

			//	m_RKX0[neigh_idx] -= rot * m_KX0[PhyxIdx].m[i+1];
			//}

			forcebuf[PhyxIdx] -= rot * pKX0buf->m[0];
			for (int i=0; i<NB_NEIGHBORS; ++i)
			{
				const int& neigh_idx = pX0buf->m[i+1].idx;
				forcebuf[neigh_idx] -= rot * pKX0buf->m[i+1];
			}


			++pX0buf;
			++pKX0buf;
			++mapbuf;
			++phyxelstiffnessbuf;
			++rotationbuf;
			++undeformedbuf;
			++massbuf;

		}

		 mPhyxelRotations->unlock();
		 mX0->unlock();
		 mNodes->unlock();
		 mUndeformedStateVector->unlock();
		 mNodeMasses->unlock();
		 mForces->unlock();
		 mX0->unlock();
		 mKX0->unlock();
		 mKphyxel->unlock();
		 mPhyxK2StiffnessMatrixMap->unlock();
		 mStiffnessMatrix->unlock();

		 //PrintStiffnessMatrix( "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\A.txt");

		 //SphSoftBody::computeStressForces();

		//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\K.txt");

		return  (float)( tick_count::now() - starTicks ).seconds();
	}


	template<unsigned int NB_NEIGHBORS>
	float SphSoftBody<NB_NEIGHBORS>::solve( const float& timeStep, void* user_pointer )
	{
		tick_count starTicks = tick_count::now();

		ConjGradSolver();

		mExternForces->reset();

		return  (float)( tick_count::now() - starTicks ).seconds();

	}


	template<unsigned int NB_NEIGHBORS>
	Vector3 SphSoftBody<NB_NEIGHBORS>::getStressForce( const int& phyxelIdx ) const
	{
		Vector3 StressForce(0.0);

		//SimdMatrix3* PhyxelRow =  m_StiffnessMatrix[phyxelIdx];
		//const std::vector<unsigned short int>& Map2Phyxels = MapStiffnessMatrix2Phyxels[phyxelIdx];

		//const SimdVector3* const nodes = reinterpret_cast<SimdVector3*>(mNodes);

		//int Nb_Elms = Map2Phyxels.size();

		//ZeroMemory( &SimdStressForce, align16 );

		//for (int j=0; j<Nb_Elms; j++)
		//{
		//	SimdStressForce -= PhyxelRow[j]* nodes[Map2Phyxels[j]];
		//}

		//Vector3 StressForce( reinterpret_cast<const Vector3&>
		//	(SimdStressForce - m_RKX0[phyxelIdx]) );

		return StressForce;

	}


	template<unsigned int NB_NEIGHBORS>
	const Buffer<Vector3>& SphSoftBody<NB_NEIGHBORS>::getStressForces(void) const
	{
		return *mStressForces;
	}


	template<unsigned int NB_NEIGHBORS>
	SoftBody* SphSoftBody<NB_NEIGHBORS>::clone(void)
	{

		return 0;
	}


	template<unsigned int NB_NEIGHBORS>
	float SphSoftBody<NB_NEIGHBORS>::getVertexMass(int vertexId) const
	{
		float mass;

		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>(vertexId);
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>(vertexId);
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>(*indexbuf);
		const float* nodemassbuf = 
			mNodeMasses->lockForRead<float>();

		mass = 
			coordbuf->l1 * nodemassbuf[tetrabuf->n1] +
			coordbuf->l2 * nodemassbuf[tetrabuf->n2] +
			coordbuf->l3 * nodemassbuf[tetrabuf->n3] +
			(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * nodemassbuf[tetrabuf->n4];

		mNodeMasses->unlock();
		mElements->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();

		return mass;
	}

	
	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::updateCollisionShape(void)
	{

		updateVertices();
		updateVertexVels();
		updateVertexNormals();

		SoftBody::updateCollisionShape();
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::setPosition(const Vector3& newpos )
	{
		SimdVector3 pivot(0.0f);
		const SimdVector3* cnodesbuf = mNodes->lockForRead<SimdVector3>();

		int nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodesbuf++;
		}
		mNodes->unlock();
		pivot /= (float)mNbNodes;
		pivot = SimdVector3(newpos[0],newpos[1],newpos[2]) - pivot;

		SimdVector3* nodesbuf = mNodes->lockForWrite<SimdVector3>();
		nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			*nodesbuf++ += pivot;
		}
		mNodes->unlock();

		//updateVertices();

		//CollisionSoftBody::buildDynamicVertexTree();
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::setOrientation(const Quat& newor )
	{
		Vector3 pivot(0.0f);
		const Vector3* cnodesbuf = mNodes->lockForRead<Vector3>();

		int nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodesbuf++;
		}
		mNodes->unlock();
		pivot /= (float)mNbNodes;

		Quat orientation = normalize(newor);
		Quat q = slerp( 1.0f, mOrientation, orientation);
		mOrientation = orientation;

		Vector3* nodesbuf = mNodes->lockForWrite<Vector3>();
		nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			*nodesbuf = pivot + rotate( q, (*nodesbuf - pivot) );
			++nodesbuf;

		}
		mNodes->unlock();

		Vector3* normalsbuf = mVertexNormals->lockForWrite<Vector3>();
		int nbvertices = mVertexNormals->getSize();
		while ( nbvertices-- > 0 )
		{
			*normalsbuf = rotate( q, *normalsbuf );
			++normalsbuf;

		}
		mVertexNormals->unlock();

		//updateVertices();

		//CollisionSoftBody::buildDynamicVertexTree();
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::reset(void)
	{

		mNodes->copyData( *mTetraMesh->mSharedNodes );

		mNodesVelocity->reset();
		mExternForces->reset();

		CollisionSoftBody::updateCollisionShape();

		//CollisionSoftBody::reset();
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::setVelocity(const Vector3& newvel )
	{
		mNodesVelocity->reset(newvel);
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::addForce(const int& nodeId, const Vector3& force )
	{
		const ushort* tetraindexbuf = 
			mBarycentricIndex->lockForRead<ushort>(nodeId);
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>(nodeId);
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>(*tetraindexbuf);
		Vector3* extforcebuf = 
			mExternForces->lockForWrite<Vector3>();

		extforcebuf[tetrabuf->n1] += coordbuf->l1 * force;
		extforcebuf[tetrabuf->n2] += coordbuf->l2 * force;
		extforcebuf[tetrabuf->n3] += coordbuf->l3 * force;
		extforcebuf[tetrabuf->n4] +=
			(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * force;

		mExternForces->unlock();
		mElements->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
	}

	template<unsigned int NB_NEIGHBORS>
	Vector3 SphSoftBody<NB_NEIGHBORS>::getVertexNormal(int vertexId) const
	{
		Vector3 normal = *mNewVertexNormals->lockForRead<Vector3>(vertexId);
		mNewVertexNormals->unlock();
		return normal;
	}

	template<unsigned int NB_NEIGHBORS>
	const BufferSharedPtr<Vector3>& SphSoftBody<NB_NEIGHBORS>::getVertexNormalBuffer(void) const
	{
		return mNewVertexNormals;
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::computeStressForces(void)
	{
		//const ushort* rowsbuf = mRows->lockForRead<ushort>();
		//const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		//const SimdMatrix3* matbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		//const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();
		//const SimdVector3* forcebuf = mForces->lockForRead<SimdVector3>();
		////const float* massbuf = mNodeMasses->lockForRead<float>();

		//SimdVector3* stressforcebuf = mStressForces->lockForWrite<SimdVector3>();

		//ushort nbcols;
		//int nbNodes = mNbNodes;
		//while ( nbNodes-- > 0 )
		//{
		//	*stressforcebuf = - (*forcebuf);
		//	nbcols = *(rowsbuf+1) - *rowsbuf;

		//	while (nbcols-- > 0)
		//	{
		//		*stressforcebuf -= (*matbuf++) * nodebuf[(*colsbuf++)];
		//	}
		//	//*stressforcebuf *= (1.0f- m_Damping);

		//	// RHS = -K*Xi -F0  
		//	++stressforcebuf;
		//	++forcebuf;
		//	++rowsbuf;			
		//}

		//mStressForces->unlock();
		////mNodeMasses->unlock();
		//mForces->unlock();
		//mNodes->unlock();
		//mStiffnessMatrix->unlock();
		//mCloumns->unlock();
		//mRows->unlock();

	}



	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::create(const SoftBodyParams& params)
	{

		Bool ret = false;

		// Desc
		m_TimeStep = params.TimeStep;
		m_TimeStep2 = params.TimeStep*params.TimeStep;
		m_Density = static_cast<float>( mMaterial->getDensity() );
		m_YoungModulus = static_cast<float>( mMaterial->getYoungModulus() );
		m_PoissonRatio = static_cast<float>( mMaterial->getPoissonRatio() );
		m_Damping = static_cast<float>( mMaterial->getFriction() );
		m_Gravity = params.Gravity;
		m_Nb_Solver_Iteration = params.Nb_Solver_Iteration;
		m_Solver_Tolerance = params.Solver_Tolerance;

		// SPH desc
		m_PhyxelSize = params.sphPhyxelSize;
		m_SupportRadius = params.sphSupportRadius;

		// TimeStepVector(dt, dt, dt )
		mTimeStepVector = SimdVector3( m_TimeStep );

		// TimeStepMatrix	| dt, 1, 1 |
		//					| 1, dt, 1 |
		//					| 1, 1, dt |	

		mTimeStepMatrix.setCol0( SimdVector3(m_TimeStep,1.0f,1.0f) );
		mTimeStepMatrix.setCol1( SimdVector3(1.0f,m_TimeStep,1.0f) );
		mTimeStepMatrix.setCol2(SimdVector3(1.0f,1.0f,m_TimeStep) );

		// TimeStep2Matrix	| dt*dt,  1   ,    1  |
		//					| 1    , dt*dt,    1  |
		//					| 1    ,  1   , dt*dt |	

		mTimeStep2Matrix.setCol0( SimdVector3(m_TimeStep2,1.0f,1.0f) );
		mTimeStep2Matrix.setCol1( SimdVector3(1.0f,m_TimeStep2,1.0f) );
		mTimeStep2Matrix.setCol2( SimdVector3(1.0f,1.0f,m_TimeStep2) );


		m_DampingMatrix.setCol0( SimdVector3(m_Damping,    0.0f   ,  0.0f   ) );
		m_DampingMatrix.setCol1( SimdVector3(0.0f	   ,  m_Damping,  0.0f   ) );
		m_DampingMatrix.setCol2( SimdVector3(0.0f     ,	0.0f   ,m_Damping) );

		mMass = 0.0f;

		SphSoftBody<NB_NEIGHBORS>::initializeBuffers();

		updateVertices();

		if ( params.mPosition[0] != Vector3::Zero()[0] ||
			params.mPosition[1] != Vector3::Zero()[1] ||
			params.mPosition[2] != Vector3::Zero()[2] )
		{
			SphSoftBody<NB_NEIGHBORS>::setPosition( params.mPosition );
		}

		const Matrix3& id = Matrix3::identity();
		if ( params.mOrientation[0][0] != id[0][0] ||
			params.mOrientation[0][1] != id[0][1] ||
			params.mOrientation[0][2] != id[0][2] ||
			params.mOrientation[1][1] != id[1][1] ||
			params.mOrientation[1][2] != id[1][2] || 
			params.mOrientation[2][2] != id[2][2] )
		{
			mOrientation = Quat( params.mOrientation );
			SphSoftBody<NB_NEIGHBORS>::setOrientation( mOrientation );
		}

		updateVertices();

		CollisionSoftBody::buildDynamicVertexTree();


		//// find neighbors phyel
		initializePhyxelsNeighbors();

		initializePhyxels();

		initializeGraph();

		initializeStiffnessMap();

		return;
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::updateVertices(void)
	{

		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>();
		const SimdVector3* nodebuf = 
			mNodes->lockForRead<SimdVector3>();
		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>();
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>();
		SimdVector3* vertexbuf = 
			mVertex->lockForWrite<SimdVector3>();
		
		for (int i=0; i<mNbVertices; ++i )
		{
			const TetrahedronElement& tetra = tetrabuf[*indexbuf++];
			const SimdVector3& n1 = nodebuf[tetra.n1];
			const SimdVector3& n2 = nodebuf[tetra.n2];
			const SimdVector3& n3 = nodebuf[tetra.n3];
			const SimdVector3& n4 = nodebuf[tetra.n4];
			
			//*vertexbuf++ = 
			//	coordbuf->l1 * n1 + 
			//	coordbuf->l2 * n2 + 
			//	coordbuf->l3 * n3 + 
			//	(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * n4;

			// all simds
			*vertexbuf++ = 
				coordbuf->l1 * ( n1 - n4 ) + 
				coordbuf->l2 * ( n2 - n4 ) +
				coordbuf->l3 * ( n3 - n4 ) + 
				n4;

			++coordbuf;
		}

		mVertex->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
		mNodes->unlock();
		mElements->unlock();
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::updateVertexVels(void)
	{
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>();
		const SimdVector3* nodevelbuf = 
			mNodesVelocity->lockForRead<SimdVector3>();
		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>();
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>();
		SimdVector3* collnodevelbuf = 
			mVertexVels->lockForWrite<SimdVector3>();

		for (int i=0; i<mNbVertices; ++i )
		{
			const TetrahedronElement& tetra = tetrabuf[*indexbuf++];
			const SimdVector3& v1 = nodevelbuf[tetra.n1];
			const SimdVector3& v2 = nodevelbuf[tetra.n2];
			const SimdVector3& v3 = nodevelbuf[tetra.n3];
			const SimdVector3& v4 = nodevelbuf[tetra.n4];

			//*vertexbuf++ = 
			//	coordbuf->l1 * n1 + 
			//	coordbuf->l2 * n2 + 
			//	coordbuf->l3 * n3 + 
			//	(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * n4;

			// all simds
			*collnodevelbuf++ = 
				coordbuf->l1 * ( v1 - v4 ) + 
				coordbuf->l2 * ( v2 - v4 ) +
				coordbuf->l3 * ( v3 - v4 ) + 
				v4;

			++coordbuf;
		}

		mVertexVels->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
		mNodesVelocity->unlock();
		mElements->unlock();
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::updateVertexNormals(void)
	{

		const SimdVector3* vNormalbuf = mVertexNormals->lockForRead<SimdVector3>();

		const ushort* vindexbuf = mBarycentricIndex->lockForRead();
		
		const BarycentricCoords* coordbuf = mBarycentricCoords->lockForRead<BarycentricCoords>();

		const TetrahedronElement* tetrabuf = mElements->lockForRead<TetrahedronElement>();

		const SimdMatrix3* phyxelsRotbuf = 	mPhyxelRotations->lockForRead<SimdMatrix3>();

		const ushort* facebuf = mFaces->lockForRead();

		SimdVector3* deformedNormalbuf = mNewVertexNormals->lockForWrite<SimdVector3>();
		//mNewVertexNormals->copyData( *mVertexNormals );

		int N = mVertexNormals->getSize();

		while ( N-- > 0 )
		{
			const TetrahedronElement& tetra = tetrabuf[vindexbuf[*facebuf++]];
			const SimdMatrix3& r1 = phyxelsRotbuf[tetra.n1];
			const SimdMatrix3& r2 = phyxelsRotbuf[tetra.n2];
			const SimdMatrix3& r3 = phyxelsRotbuf[tetra.n3];
			const SimdMatrix3& r4 = phyxelsRotbuf[tetra.n4];

			*deformedNormalbuf++ = 
				( coordbuf->l1 * ( r1 - r4 ) + 
				coordbuf->l2 * ( r2 - r4 ) +
				coordbuf->l3 * ( r3 - r4 ) + 
				r4 ) * (*vNormalbuf++);

			//*deformedNormalbuf++ = 
			//	coordbuf->l1 * ( r1 * (*vNormalbuf) ) + 
			//	coordbuf->l2 * ( r2 * (*vNormalbuf) ) +
			//	coordbuf->l3 * ( r3 * (*vNormalbuf) ) + 
			//	( 1 - coordbuf->l1 - coordbuf->l2 - coordbuf->l3 )* (r4 * (*vNormalbuf));
			//++vNormalbuf;

			++coordbuf;
		}

		mNewVertexNormals->unlock();
		mPhyxelRotations->unlock();
		mElements->unlock();
		mFaces->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
		mVertexNormals->unlock();
		
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::initializeBuffers(void)
	{

		CollisionSoftBody::initializeBuffers();

		mNbNodes = mTetraMesh->mSharedNodes->getSize();
		mNodes = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNodes->copyData( *mTetraMesh->mSharedNodes );

		mNbElements = mTetraMesh->getSubTetraMesh(0)->mTetrahedrons->getSize()/4;
		mElements = BufferManager::getSingleton().createBuffer<TetrahedronElement>(mNbElements);
		mElements->copyData(  reinterpret_cast<Buffer<TetrahedronElement>&>(*mTetraMesh->getSubTetraMesh(0)->mTetrahedrons) );

		mPhyxelRotations = 
			BufferManager::getSingleton().createBuffer<Matrix3>(mNbNodes);;

		mNewVertexNormals = 
			BufferManager::getSingleton().createBuffer<Vector3>(3*mNbFaces);


		//mNbTriangles = mTetraMesh->mFaces->getSize() /3;
		//mTriangles  = BufferManager::getSingleton().createBuffer<ushort>(3*mNbTriangles);
		//mTriangles->copyData( *mTetraMesh->mFaces );


		mNodesVelocity = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNodesVelocity->reset();


		//mNbVertices = mTetraMesh->getSubMesh(0)->mVertices.getSize(); 

		mBarycentricCoords = 
			BufferManager::getSingleton().createBuffer<BarycentricCoords>(mNbVertices);
		mBarycentricCoords->copyData( 
			reinterpret_cast<Buffer<BarycentricCoords>&>(*mTetraMesh->getSubMesh(0)->mVertices.barycentricCoords) );

		mBarycentricIndex = 
			BufferManager::getSingleton().createBuffer<ushort>(mNbVertices);
		mBarycentricIndex->copyData( *mTetraMesh->getSubMesh(0)->mVertices.tetraIndex );

		mNbConstraintNodes = mTetraMesh->mSharedConstrainedNodes->getSize();
		if ( mNbConstraintNodes )
		{
			mConstraintNodes = BufferManager::getSingleton().createBuffer<ushort>(mNbConstraintNodes);
			mConstraintNodes->copyData( *mTetraMesh->mSharedConstrainedNodes );
		}


		mExternForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mExternForces->reset();

		mForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mForces->reset();

		mStressForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mStressForces->reset();

		mNodeMasses = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mNodeMasses->reset();

		mX0 = 
			BufferManager::getSingleton().createBuffer<PhyxelNeighborsVector<NB_NEIGHBORS+1>>(mNbNodes);
		mX0->reset();

		mStrainMatrix  = 
			BufferManager::getSingleton().createBuffer<PhyxelStrainMatrix<NB_NEIGHBORS+1>>(mNbNodes);
		mStrainMatrix->reset();

		mKphyxel = 
			BufferManager::getSingleton().createBuffer<PhyxelStiffnessMatrix<NB_NEIGHBORS+1>>(mNbNodes);
		mKphyxel->reset();

		mUndeformedStateVector = 
			BufferManager::getSingleton().createBuffer<SimdMatrix3>(mNbNodes);
		mUndeformedStateVector->reset();

		mKX0 = 
			BufferManager::getSingleton().createBuffer<PhyxelVector<NB_NEIGHBORS+1>>(mNbNodes);
		mKX0->reset();



		mh = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mh2 = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mKernelConst = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mKernelGradConst = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mElastKernelConst1 = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mElastKernelConst2 = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mElasticKernelGradConst = BufferManager::getSingleton().createBuffer<float>(mNbNodes);

		mVolume = BufferManager::getSingleton().createBuffer<float>(mNbNodes);


		////Graph
		//unsigned int graphSize = mTetraMesh->mGraph.columns->getSize();
		//mCloumns = 
		//	BufferManager::getSingleton().createBuffer<ushort>( graphSize );
		//mCloumns->copyData( *mTetraMesh->mGraph.columns );

		//mRows = 
		//	BufferManager::getSingleton().createBuffer<ushort>( mNbNodes + 1 );
		//mRows->copyData( *mTetraMesh->mGraph.rows );

		//mDiagonals = 
		//	BufferManager::getSingleton().createBuffer<ushort>( mNbNodes );
		//mDiagonals->copyData( *mTetraMesh->mGraph.diagonals );

		mPhyxK2StiffnessMatrixMap = 
			BufferManager::getSingleton().createBuffer<Kphyxel<NB_NEIGHBORS+1>>( mNbNodes );

		//mStiffnessMatrix = BufferManager::getSingleton().createBuffer<Matrix3>(graphSize);
		//mStiffnessMatrix->reset(); 


		//    used by solver
		mRHSVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mRHSVector->reset(); 
		mNewNodeVelocity = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNewNodeVelocity->reset(); 
		mResidualVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mResidualVector->reset(); 
		mDirectionVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mDirectionVector->reset(); 
		mTemp = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mTemp->reset(); 


	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::initializePhyxelsNeighbors(void)
	{

		// neighbors search
		int index;
		std::vector<NeighborsDistance> neighbors_vector;
		neighbors_vector.reserve( mNbNodes );

		const SimdVector3* nodesbuf = mNodes->lockForRead<SimdVector3>();
		//const ushort* rowbuf = mTetraMesh->mGraph.rows->lockForRead<ushort>();
		//const ushort* colsbuf = mTetraMesh->mGraph.columns->lockForRead<ushort>();
		PhyxelNeighborsVector<NB_NEIGHBORS+1>* x0buf = mX0->lockForWrite();

		//ushort nbblock;
		//for (ushort NodeIndex=0; NodeIndex<mNbNodes; ++NodeIndex)
		//{
		//	neighbors_vector.clear();

		//	const SimdVector3& Pi = nodesbuf[NodeIndex];
		//	nbblock = *(rowbuf+1) - *rowbuf;
		//	while ( nbblock-- > 0)
		//	{
		//		if ( *colsbuf != NodeIndex )
		//		{
		//			const SimdVector3& Pj = nodesbuf[*colsbuf];
		//			float square_distance = SSE::lengthSqr( Pi - Pj );// (rj-ri).norm2();
		//			NeighborsDistance neigh;
		//			neigh.idx = *colsbuf;
		//			neigh.dist = square_distance;
		//			neighbors_vector.push_back( neigh );
		//			//index++;
		//		}
		//		++colsbuf;
		//	}

		//	++rowbuf;
		////}

		for (int i=0; i<mNbNodes; ++i)
		{
			const SimdVector3& Pi = nodesbuf[i];
			index = 0;
			//neighbors = m_ParticleSystem->m_NeighBors + MAX_NEIGHBOR_PARTICLES*i;
			neighbors_vector.clear();
			for (int j=0;j<mNbNodes;++j)
			{
				if ( j != i)
				{
					const SimdVector3& Pj = nodesbuf[j];
					float square_distance = SSE::lengthSqr( Pi - Pj );// (rj-ri).norm2();
					NeighborsDistance neigh;
					neigh.idx = j;
					neigh.dist = square_distance;
					neighbors_vector.push_back( neigh );
					index++;
				}
			}
			std::sort( neighbors_vector.begin(), neighbors_vector.end() );

			// X0
			x0buf->m[0].pos = nodesbuf[i];
			x0buf->m[0].idx = i;

			//x0buf->m[0].pos = nodesbuf[NodeIndex];
			//x0buf->m[0].idx = NodeIndex;

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				//neighbors[j].m_Position = particles[neighbors_vector[j].idx].m_Position;
				// overloading operator =
				//m_X0[i].m[j+1].pos = m_Nodes[neighbors_vector[j].idx];
				//m_X0[i].m[j+1].idx = neighbors_vector[j].idx;
				x0buf->m[j+1] = nodesbuf[neighbors_vector[j].idx];
				x0buf->m[j+1] = neighbors_vector[j].idx;
				//neighbors[j].m_idx = neighbors_vector[j].idx;
			}

			++x0buf;

		}

		mX0->unlock();
		//mTetraMesh->mGraph.columns->unlock();
		//mTetraMesh->mGraph.rows->unlock();
		mNodes->unlock();
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::initializePhyxels(void)
	{


		// Elasticity Matrix
		mD = BufferManager::getSingleton().createBuffer<ElasticityMatrix>();
		ElasticityMatrix* elasticitymatrix = mD->lockForWrite();
		elasticitymatrix->ComputeMatrix(m_YoungModulus, m_PoissonRatio);
		mD->unlock();

		


		// compute support radius
		float mean_radius;
		float support_radius;
		
		const Vector3* nodesbuf = mNodes->lockForRead();
		const PhyxelNeighborsVector<NB_NEIGHBORS+1>* x0buf = mX0->lockForRead();

		float* massbuf = mNodeMasses->lockForWrite();
		float* hbuf = mh->lockForWrite();
		float* hbuf2 = mh2->lockForWrite();
		float* kernelConstbuf = mKernelConst->lockForWrite();
		float* kernelGradConstbuf = mKernelGradConst->lockForWrite();
		float* elasticKernelConst1buf = mElastKernelConst1->lockForWrite();
		float* elasticKernelConst2buf = mElastKernelConst2->lockForWrite();
		float* elasticKernelGradbuf = mElasticKernelGradConst->lockForWrite();

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			mean_radius = 0.0f;
			const SimdVector3& Pi = *nodesbuf++; //nodesbuf[PhyxIdx];
			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const SimdVector3& Pj = (SimdVector3&)X0.m[j+1];
				mean_radius += SSE::lengthSqr( Pi - Pj );
			}

			mean_radius /= NB_NEIGHBORS;
			mean_radius = sqrt(mean_radius);

			// theoretical support radius 
			support_radius = m_SupportRadius * mean_radius;

			*hbuf++ = support_radius;
			*hbuf2++ = support_radius*support_radius;
			*kernelConstbuf = KernelConstant / powf(support_radius, 9);
			*kernelGradConstbuf++ = -6.0f * (*kernelConstbuf++);

			*elasticKernelConst1buf++ = 
				(float)( 1.0/(4.0*support_radius*support_radius*support_radius*(PiG/3.0-8.0/PiG+16.0/(PiG*PiG)) ) );

			*elasticKernelGradbuf++ = 
				(float)( - PiG/(8.0*support_radius*support_radius*support_radius*support_radius*(PiG/3.0-8.0/PiG+16.0/(PiG*PiG)) ) );

			*elasticKernelConst2buf++ = (float)( PiG/(2.0*support_radius));

			// la massa dovrebbe essere mi
			*massbuf++ = mean_radius*mean_radius*mean_radius* m_Density;

			//++nodesbuf;
			//++x0buf;
		}
		mElasticKernelGradConst->unlock();
		mElastKernelConst2->unlock();
		mElastKernelConst1->unlock();
		mKernelGradConst->unlock();
		mKernelConst->unlock();
		mh2->unlock();
		mh->unlock();
		mNodeMasses->unlock();
		mX0->unlock();
		mNodes->unlock();



		// compute the mass scaling factor 
		float density_temp;
		float scal_factor = 0.0f;
		int index = 0;

		nodesbuf = mNodes->lockForRead();
		x0buf = mX0->lockForRead();
		const float* massconstbuf = mNodeMasses->lockForRead();

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{

			const SimdVector3& Pi = *nodesbuf++;
			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;

			density_temp = 0.0f;

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const SimdVector3& Pj = X0.m[j+1].pos;
				density_temp += massconstbuf[X0.m[j+1].idx] 
				* kernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
				//* ElasticityKernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );

			}
			assert(density_temp > 1e-6 );
			scal_factor += ( m_Density /  density_temp ) ;
		}
		mNodeMasses->unlock();
		mX0->unlock();
		mNodes->unlock();

		// adjust mass
		//scal_factor /= index;
		scal_factor /= mNbNodes;
		massbuf = mNodeMasses->lockForWrite();
		for (int i=0; i<mNbNodes; ++i)
		{
			*massbuf *= scal_factor;
			mMass += *massbuf++;
		}
		mNodeMasses->unlock();


		// compute volume 
		density_temp = 0.0f;

		nodesbuf = mNodes->lockForRead();
		x0buf = mX0->lockForRead();
		massconstbuf = mNodeMasses->lockForRead();
		
		float* volumebuf = mVolume->lockForWrite();
		
		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{

			const SimdVector3& Pi = *nodesbuf++;

			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;
			density_temp = 0.0f;

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const SimdVector3& Pj = X0.m[j+1].pos;
				density_temp += massconstbuf[X0.m[j+1].idx] 
				* kernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
				//* ElasticityKernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
			}

			*volumebuf++ = massconstbuf[PhyxIdx] / density_temp;
		}

		mVolume->unlock();
		mNodeMasses->unlock();
		mX0->unlock();
		mNodes->unlock();


		// compute Undeformed State Matrix
		SimdMatrix3 UndeformedState_temp;

		nodesbuf = mNodes->lockForRead();
		x0buf = mX0->lockForRead();
		massconstbuf = mNodeMasses->lockForRead();

		SimdMatrix3* undeformedStatebuf = mUndeformedStateVector->lockForWrite<SimdMatrix3>();

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			const SimdVector3& Pi0 = *nodesbuf++;
			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;
			memset((void*)&UndeformedState_temp, 0, sizeof(SimdMatrix3) );
			
			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const SimdVector3& Pj0 = X0.m[j+1].pos;
				UndeformedState_temp += massconstbuf[X0.m[j+1].idx] * 
					kernel( (float&)SSE::lengthSqr( Pj0 - Pi0 ), PhyxIdx )* SSE::outer( ( Pj0 - Pi0 ), ( Pj0 - Pi0 ) );
				//ElasticityKernel( (float&)SSE::lengthSqr( Pj0 - Pi0 ), PhyxIdx )* SSE::outer( ( Pj0 - Pi0 ), ( Pj0 - Pi0 ) );

			}

			*undeformedStatebuf++ = SSE::inverse( UndeformedState_temp );

		}
		mUndeformedStateVector->unlock();
		mNodeMasses->unlock();
		mX0->unlock();
		mNodes->unlock();

	//}

	//template<unsigned int NB_NEIGHBORS>
	//void SphSoftBody<NB_NEIGHBORS>::initializeElements(void)
	//{
		

		nodesbuf = mNodes->lockForRead();
		x0buf = mX0->lockForRead();
		const float* volumeconstbuf = mVolume->lockForRead();
	
		const ElasticityMatrix* elasticity = mD->lockForRead();

		PhyxelStiffnessMatrix<NB_NEIGHBORS+1>* Kphyxelbuf =	mKphyxel->lockForWrite();

		PhyxelVector<NB_NEIGHBORS+1>* kx0buf =  mKX0->lockForWrite();

		PhyxelStrainMatrix<NB_NEIGHBORS+1>* strainbuf = mStrainMatrix->lockForWrite();
		
		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			const SimdVector3& Xi = *nodesbuf++;
			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;

			SimdVector3 gradW;
			//memset((void*)&m_StrainMatrix[PhyxIdx], 0, sizeof(PhyxelStrainMatrix<NB_NEIGHBORS+1>) );

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const SimdVector3& Xj = X0.m[j+1].pos;
				const float& Volj = volumeconstbuf[X0.m[j+1].idx];
				gradW =  gradKernel( (SimdVector3&)(Xj - Xi), PhyxIdx );
				//gradW =  GradElasticityKernel( (SimdVector3&)(Xj - Xi), PhyxIdx );

				strainbuf->m[0][j+1].setElem( 0,0, gradW.getX() );
				strainbuf->m[0][j+1].setElem( 1,1, gradW.getY() );
				strainbuf->m[0][j+1].setElem( 2,2, gradW.getZ() );

				strainbuf->m[1][j+1].setElem( 0,0, 0.5f*gradW.getY() );
				strainbuf->m[1][j+1].setElem( 1,0, 0.5f*gradW.getX() );

				strainbuf->m[1][j+1].setElem( 1,1, 0.5f*gradW.getZ() );
				strainbuf->m[1][j+1].setElem( 2,1, 0.5f*gradW.getY() );

				strainbuf->m[1][j+1].setElem( 0,2, 0.5f*gradW.getZ() );
				strainbuf->m[1][j+1].setElem( 2,2, 0.5f*gradW.getX() );

				strainbuf->m[0][j+1] *= Volj;
				strainbuf->m[1][j+1] *= Volj;

				strainbuf->m[0][0] -= strainbuf->m[0][j+1];
				strainbuf->m[1][0] -= strainbuf->m[1][j+1];

				//m_StrainMatrix[PhyxIdx].print( "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\strain.txt" );
			}
			//m_StrainMatrix[PhyxIdx].print( "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\strain.txt" );
			// C*strain

			PhyxelStrainMatrix<NB_NEIGHBORS+1> temp;
			for ( int j=0; j< NB_NEIGHBORS+1; j++ )
			{
				//temp.m[0][j] = m_ElasticityMatrix[0] * strainbuf->m[0][j];
				temp.m[0][j] = elasticity->m[0][0] * strainbuf->m[0][j];
				strainbuf->m[0][j] = transpose( strainbuf->m[0][j] );
				//temp.m[1][j] = m_ElasticityMatrix[1] * strainbuf->m[1][j];
				temp.m[1][j] = elasticity->m[1][1] * strainbuf->m[1][j];
				strainbuf->m[1][j] = transpose( strainbuf->m[1][j] );
			}


			const float& Vol_i = volumeconstbuf[PhyxIdx];

			for ( int j=0; j< NB_NEIGHBORS+1; ++j )
			{
				for ( int k=0; k< NB_NEIGHBORS+1; ++k )
				{

					Kphyxelbuf->m[k][j] = strainbuf->m[0][k] * temp.m[0][j];
					Kphyxelbuf->m[k][j] += strainbuf->m[1][k] * temp.m[1][j];
					Kphyxelbuf->m[k][j] *= Vol_i *0.5f;
				}
			}

			// compute K*X0
			//memset( (void*)&m_KX0[PhyxIdx], 0, sizeof(PhyxelVector<NB_NEIGHBORS+1>) );

			for ( int j=0; j< NB_NEIGHBORS+1; ++j )
			{
				for ( int k=0; k<NB_NEIGHBORS+1; ++k )
				{

					kx0buf->m[j] += Kphyxelbuf->m[j][k] * X0.m[k].pos;

				}
			}

			++kx0buf;
			++Kphyxelbuf;
			++strainbuf;
		}

		mKphyxel->unlock();
		mKX0->unlock();
		mStrainMatrix->unlock();
		mD->unlock();
		mVolume->unlock();
		mX0->unlock();
		mNodes->unlock();

	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::initializeGraph(void)
	{

		std::vector< std::vector<unsigned short int> > graph;

		graph.reserve( mNbNodes );

		std::vector<unsigned short int> int0;

		std::vector<unsigned short int>::iterator iter;

		for (int i=0; i<mNbNodes; ++i)
		{
			graph.push_back( int0 );
			graph[i].reserve( 32 );
		}

		const PhyxelNeighborsVector<NB_NEIGHBORS+1>* x0buf = mX0->lockForRead();

		for (int PhyxIdx=0; PhyxIdx< mNbNodes; ++PhyxIdx)
		{
			// main phyxel
			iter = std::find ( graph[PhyxIdx].begin(), 
				graph[PhyxIdx].end(), PhyxIdx );

			if ( iter == graph[PhyxIdx].end() )
			{
				graph[PhyxIdx].push_back( PhyxIdx );
			}

			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf++;
			//neighbors = m_ParticleSystem->m_NeighBors + MAX_NEIGHBOR_PARTICLES*i;


			for (int j=0; j< NB_NEIGHBORS; j++)
			{
				const int& idx = X0.m[j+1].idx;
				iter = std::find ( graph[PhyxIdx].begin(), 
					graph[PhyxIdx].end(), idx );

				if ( iter == graph[PhyxIdx].end() )
				{
					graph[PhyxIdx].push_back( idx );
					graph[idx].push_back( PhyxIdx );
				}
			}

			for (int j=0; j< NB_NEIGHBORS; ++j)
			{
				const int& n_i = X0.m[j+1].idx;
				iter = std::find ( graph[n_i].begin(), graph[n_i].end(), n_i );

				if ( iter == graph[n_i].end() )
				{
					graph[n_i].push_back( n_i );
				}

				for (int k=j+1; k< NB_NEIGHBORS; ++k)
				{
					const int& n_j = X0.m[k+1].idx; // neighbors[k].m_idx;

					iter = std::find ( graph[n_j].begin(), graph[n_j].end(), n_i );

					if ( iter == graph[n_j].end() )
					{
						graph[n_j].push_back( n_i );
						graph[n_i].push_back( n_j );
					}
				}

			}

			sort (graph[PhyxIdx].begin(), graph[PhyxIdx].end() );
		}

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx )
		{
			sort (graph[PhyxIdx].begin(), graph[PhyxIdx].end() );
		}

		// CreateDiagMap

		std::vector<unsigned short> diags;
		diags.reserve(mNbNodes);
		
		size_t sz;

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			sz = graph[PhyxIdx].size();

			for ( size_t j=0; j<sz; ++j )
			{
				if ( graph[PhyxIdx][j] == PhyxIdx )
				{
					//diags[PhyxIdx] = j;
					diags.push_back( (ushort)j );
					break;
				}

			}
		}


		// build graph structure
		//allocate
		mRows = BufferManager::getSingleton().createBuffer<ushort>(mNbNodes+1);

		size_t count = 0;
		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			count += graph[PhyxIdx].size();
		}

		mCloumns = BufferManager::getSingleton().createBuffer<ushort>(count);
		mDiagonals = BufferManager::getSingleton().createBuffer<ushort>(mNbNodes);
		
		// fill buffers
		ushort* rowsbuf = mRows->lockForWrite();
		ushort* colsbuf = mCloumns->lockForWrite();
		ushort* diagsbuf = mDiagonals->lockForWrite();

		count = 0;
		size_t rowsize = 0;
		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx)
		{
			*rowsbuf++ = static_cast<ushort>(count);
			*diagsbuf++ = static_cast<ushort>(count + diags[PhyxIdx]);
			
			rowsize = graph[PhyxIdx].size();			
			const std::vector<unsigned short>& graphrow = graph[PhyxIdx];
			
			for ( size_t i=0; i<rowsize; ++i)
			{
				*colsbuf++ = static_cast<ushort>(graphrow[i]);
			}
			
			count += rowsize;			
		}
		// last row elem
		*rowsbuf = (ushort)count;

		mDiagonals->unlock();
		mCloumns->unlock();
		mRows->unlock();

		mStiffnessMatrix = 
			BufferManager::getSingleton().createBuffer<Matrix3>(mCloumns->getSize());
		
		mStiffnessMatrix->reset(); 

	}



	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::initializeStiffnessMap(void)
	{

		unsigned short phyxelIdx_i, phyxelIdx_j;

		const PhyxelNeighborsVector<NB_NEIGHBORS+1>* x0buf = mX0->lockForRead();

		const ushort* rowbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();

		Kphyxel<NB_NEIGHBORS+1>* mapbuf = mPhyxK2StiffnessMatrixMap->lockForWrite();

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; ++PhyxIdx )
		{
		
			const PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = *x0buf;
			//phyxelIdx_i = PhyxIdx;

			for (int i=0; i<NB_NEIGHBORS+1; ++i )
			{
				phyxelIdx_i = X0.m[i].idx;

				for (int j=0; j<NB_NEIGHBORS+1; ++j )
				{

					phyxelIdx_j = X0.m[j].idx;

					ushort startId = rowbuf[phyxelIdx_i];
					ushort endId = rowbuf[phyxelIdx_i+1];
					ushort id=startId;

					while ( colsbuf[id] != phyxelIdx_j )
					{
						assert( id != endId-1 );
						++id;						
					}
					mapbuf->ij[i][j] = id;

				}
			}

			++mapbuf;
			++x0buf;
		}

		mPhyxK2StiffnessMatrixMap->unlock();
		mCloumns->unlock();
		mRows->unlock();
		mX0->unlock();

	}


	template<unsigned int NB_NEIGHBORS>
	float SphSoftBody<NB_NEIGHBORS>::computeRhs(void)
	{

		tick_count starTicks = tick_count::now();


		//SimdMatrix3* NodeRow;

		//int Nb_Elms;

		// RHS = Mv0 - dt*(K*Xi + F0 - Fext + (to do)lambda*Fvinc )

		// Reset RHS Vector
		//memset(m_RHS_Vector, 0.0f, m_Nb_Nodes*sizeof(SimdVector3) );
		mRHSVector->reset();

		const ushort* rowsbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const SimdMatrix3* matbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();
		const SimdVector3* forcebuf = mForces->lockForRead<SimdVector3>();
		const SimdVector3* extforcebuf = mExternForces->lockForRead<SimdVector3>();
		const float* nodemassbuf = mNodeMasses->lockForRead<float>();
		const SimdVector3* nodevelbuf = mNodesVelocity->lockForRead<SimdVector3>();


		SimdVector3* rhsbuf = mRHSVector->lockForWrite<SimdVector3>();

		ushort nbcols;
		int nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			nbcols = *(rowsbuf+1) - *rowsbuf;

			// RHS = K*Xi 
			while (nbcols-- > 0)
			{
				*rhsbuf -= (*matbuf++) * nodebuf[(*colsbuf++)];
			}

			// RHS = -K*Xi -F0  
			*rhsbuf -= *forcebuf;

			// RHS = -K*Xi -F0  - Fext
			*rhsbuf += *extforcebuf;

			// add Gravity;
			// RHS = -K*Xi -F0  - Fext - mg
			*rhsbuf += reinterpret_cast<const SimdVector3&>(m_Gravity) * (*nodemassbuf); //SimdVector3( 0.0f, m_Gravity*(*nodemassbuf), 0.0f );

			// RHS = dt*( -K*Xi -F0 )
			*rhsbuf = SSE::mulPerElem( (*rhsbuf), mTimeStepVector);

			*rhsbuf += SSE::mulPerElem( *nodevelbuf, SSE::Vector3(*nodemassbuf) );


			++nodevelbuf;
			++nodemassbuf;
			++extforcebuf;
			++forcebuf;
			++rhsbuf;
			++rowsbuf;	
		}

		mRHSVector->unlock();
		mNodesVelocity->unlock();
		mNodeMasses->unlock();
		mExternForces->unlock();
		mForces->unlock();
		mNodes->unlock();
		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

		return  (float)( tick_count::now() - starTicks ).seconds();

	}

	template<unsigned int NB_NEIGHBORS>
	float SphSoftBody<NB_NEIGHBORS>::computeLhs(void)
	{

		tick_count starTicks = tick_count::now();

		// LHS = M + dt*C + dt*dt*K

		int nbblocks = mStiffnessMatrix->getSize();
		SimdMatrix3* matbuf = mStiffnessMatrix->lockForWrite<SimdMatrix3>();
		//  LHS = dt*dt*K		
		while ( nbblocks-- > 0 )
		{
			*matbuf++ *= m_TimeStep2;
		}
		mStiffnessMatrix->unlock();


		const ushort* diagsbuf = mDiagonals->lockForRead<ushort>();
		const float* nodemassbuf = mNodeMasses->lockForRead<float>();
		matbuf = mStiffnessMatrix->lockForWrite<SimdMatrix3>();

		//ushort nbcols;
		int nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			matbuf[*diagsbuf] += SimdMatrix3(
				SimdVector3( *nodemassbuf, 0.0f, 0.0f ),
				SimdVector3( 0.0f, *nodemassbuf, 0.0f ),
				SimdVector3( 0.0f, 0.0f, *nodemassbuf )
				);
			matbuf[*diagsbuf] += SSE::mulPerElem(m_DampingMatrix,mTimeStepMatrix);

			++diagsbuf;
			++nodemassbuf;
		}

		mStiffnessMatrix->unlock();
		mNodeMasses->unlock();
		mDiagonals->unlock();

		return  (float)( tick_count::now() - starTicks ).seconds();
	}



	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::ConjGradSolver(void)
	{

		static const float invTimeStep = 1.0f / m_TimeStep;

		computeRhs();
		computeLhs();


		//SimdMatrix3* NodeRow;
		SimdVector3 TempVector;

		//int NbElms;

		// initial guess: x0
		mNewNodeVelocity->copyData( *mNodesVelocity );

		//project to constraint space
		//int constrIndex;
		//int IdxNode;

		if ( mNbConstraintNodes > 0 )
		{
			//const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
			Vector3* newvelbuf = mNewNodeVelocity->lockForWrite<Vector3>();

			//mNbConstraintNodes = 0;
			for (int i=0; i< mNbConstraintNodes; ++i)
			{
				//newvelbuf[contrbuf->index] = contrbuf->m_value;
				//++contrbuf;
				memset( &newvelbuf[*contrbuf++], 0, sizeof(Vector3) );
			}
			mNewNodeVelocity->unlock();
			mConstraintNodes->unlock();
		}


		// Compute initial residual e p (auxiliary) vector 
		mResidualVector->copyData( *mRHSVector );

		const ushort* rowbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const SimdMatrix3* stiffmatbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* nodevelbuf = mNodesVelocity->lockForRead<SimdVector3>();
		SimdVector3* residualbuf = mResidualVector->lockForWrite<SimdVector3>();

		ushort nbblock;
		for (int NodeIndex=0; NodeIndex<mNbNodes; ++NodeIndex)
		{
			nbblock = *(rowbuf+1) - *rowbuf;

			while ( nbblock-- > 0)
			{
				*residualbuf -= (*stiffmatbuf++) * nodevelbuf[(*colsbuf++)];
			}
			++residualbuf;
			++rowbuf;
		}
		mResidualVector->unlock();
		mNodesVelocity->unlock();
		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

		if ( mNbConstraintNodes > 0 )
		{
			const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
			Vector3* residualbuf = mResidualVector->lockForWrite<Vector3>();
			for (int i=0; i< mNbConstraintNodes; ++i)
			{
				memset( &residualbuf[*contrbuf++], 0, sizeof(Vector3) );
			}
			mResidualVector->unlock();
			mConstraintNodes->unlock();
		}


		// initial auxiliary vector
		mDirectionVector->copyData( *mResidualVector );

		double rho = InnProd(
			mResidualVector->lockForRead<SimdVector3>(), 
			mResidualVector->lockForRead<SimdVector3>(), 
			mNbNodes);
		mResidualVector->unlock();


		double invNorm_b = InnProd(
			mRHSVector->lockForRead<SimdVector3>(), 
			mRHSVector->lockForRead<SimdVector3>(), 
			mNbNodes);
		mRHSVector->unlock();

		// 1.0/ ||b||
		invNorm_b = 1.0/invNorm_b;


		// ______________________________________________________________

		// -- solve the system using a conjugate gradient solution
		double  alpha, beta;
		double denominator = 0.0;
		double new_rho = 0.0;
		SimdVector3 denominator_acc;

		//PrintStiffnessMatrix( "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\A.txt");
		//printVector(mRHSVector->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\b.txt");
		//printVector(mNodesVelocity->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\v.txt");
		//printVector(mDirectionVector->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\dir.txt");

		for (int iter=0; iter < m_Nb_Solver_Iteration; ++iter)
		{

			MatVecProd(
				mStiffnessMatrix->lockForRead<SimdMatrix3>(),
				mDirectionVector->lockForRead<SimdVector3>(),
				mNbNodes,
				mRows->lockForRead<ushort>(),
				mCloumns->lockForRead<ushort>(),
				mTemp->lockForWrite<SimdVector3>()
				);

			mTemp->unlock();
			mCloumns->unlock();
			mRows->unlock();
			mDirectionVector->unlock();
			mStiffnessMatrix->unlock();


			//if ( mNbConstraintNodes > 0 )
			//{
			//	const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			//	Vector3* tempbuf = mTemp->lockForWrite<Vector3>();

			//	for (int i=0; i< mNbConstraintNodes; ++i)
			//	{
			//		tempbuf[contrbuf->index] = contrbuf->m_value;
			//		++contrbuf;
			//	}
			//	mTemp->unlock();
			//	mConstraintNodes->unlock();
			//}
			if ( mNbConstraintNodes > 0 )
			{
				const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
				Vector3* tempbuf = mTemp->lockForWrite<Vector3>();
				for (int i=0; i< mNbConstraintNodes; ++i)
				{
					memset( &tempbuf[*contrbuf++], 0, sizeof(Vector3) );
				}
				mTemp->unlock();
				mConstraintNodes->unlock();
			}

			denominator = InnProd(
				mDirectionVector->lockForRead<SimdVector3>(),
				mTemp->lockForWrite<SimdVector3>(),  
				mNbNodes 
				);
			mTemp->unlock();
			mDirectionVector->unlock();

			// 
			if (fabs(denominator)  < m_Solver_Tolerance ) // 1e-5f
			{
				//continue;
				break;
			}

			// new alpha
			alpha = rho / denominator;


			// Update velocity 
			const SimdVector3* cdirbuf = mDirectionVector->lockForRead<SimdVector3>();
			SimdVector3* newvelbuf = mNewNodeVelocity->lockForWrite<SimdVector3>();
			int NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*newvelbuf++ +=  (float)alpha * (*cdirbuf++);
			}
			mNewNodeVelocity->unlock();
			mDirectionVector->unlock();


			// Update residual vector
			const SimdVector3* ctempbuf = mTemp->lockForRead<SimdVector3>();
			SimdVector3* residualbuf = mResidualVector->lockForWrite<SimdVector3>();

			NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*residualbuf++ -=  alpha * (*ctempbuf++);
			}
			mResidualVector->unlock();
			mTemp->unlock();


			new_rho = InnProd( 
				mResidualVector->lockForRead<SimdVector3>(),
				mResidualVector->lockForRead<SimdVector3>(),
				mNbNodes
				);


			// new beta
			beta = new_rho / rho;

			// update rho
			rho = new_rho;

			double err = new_rho * invNorm_b;
			// check 
			if ( err  < 1e-7 /*m_Solver_Tolerance*/ ) // 1e-5
			{
				//continue;
				// exit 
				break;
			}

			// update p vector (auxiliary)
			const SimdVector3* cresidualbuf = mResidualVector->lockForRead<SimdVector3>();
			SimdVector3* dirbuf = mDirectionVector->lockForWrite<SimdVector3>();

			NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*dirbuf = beta * (*dirbuf) + (*cresidualbuf++);
				++dirbuf;
			}
			mDirectionVector->unlock();
			mResidualVector->unlock();

			//if ( mNbConstraintNodes > 0 )
			//{
			//	const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			//	dirbuf = mDirectionVector->lockForWrite<SimdVector3>();

			//	for (int i=0; i< mNbConstraintNodes; ++i)
			//	{
			//		dirbuf[contrbuf->index] = contrbuf->m_value;
			//		++contrbuf;
			//	}
			//	mDirectionVector->unlock();
			//	mConstraintNodes->unlock();
			//}
			if ( mNbConstraintNodes > 0 )
			{
				const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
				Vector3* dirbuf = mDirectionVector->lockForWrite<Vector3>();
				for (int i=0; i< mNbConstraintNodes; ++i)
				{
					memset( &dirbuf[*contrbuf++], 0, sizeof(Vector3) );
				}
				mDirectionVector->unlock();
				mConstraintNodes->unlock();
			}

		}

		mNodesVelocity->copyData( *mNewNodeVelocity );

		const SimdVector3* cnodevelbuf = mNewNodeVelocity->lockForRead<SimdVector3>();
		SimdVector3* nodesbuf = mNodes->lockForWrite<SimdVector3>();

		int nb_nodes = mNbNodes;
		while ( nb_nodes-- > 0 )
		{
			(*nodesbuf++) +=  SSE::mulPerElem( *cnodevelbuf++, mTimeStepVector);
		}
		mNodes->unlock();
		mNewNodeVelocity->unlock();
	}

	//template<unsigned int NB_NEIGHBORS>
	//Bool SphSoftBody<NB_NEIGHBORS>::loadMesh(char *filename)
	//{

	//	char szFilename[100];

	//	sprintf_s(szFilename,"%s.1.node",filename);
	//	if(!LoadPhyxels(szFilename))
	//		return false;

	//	//sprintf_s(szFilename,"%s.1.ele",filename);	
	//	//if(!LoadElements(szFilename))
	//	//	return false;

	//	sprintf_s(szFilename,"%s.1.face",filename);
	//	if(!LoadTriangles(szFilename))
	//		return false;

	//	sprintf_s(szFilename,"%s.smesh",filename);	
	//	if(!LoadCollisionNodes(szFilename))
	//		return false;

	//	sprintf_s(szFilename,"%s.1.constr",filename);	
	//	if(!LoadConstraints(szFilename))
	//		return false;


	//	return true;

	//}



	//template<unsigned int NB_NEIGHBORS>
	//void SphSoftBody<NB_NEIGHBORS>::initialize(void)
	//{

	//	// material MAtrix

	//	SimdMatrix3& elasticity1 = m_ElasticityMatrix[0];
	//	SimdMatrix3& elasticity2 = m_ElasticityMatrix[1];

	//	memset((void*)&elasticity1, 0, sizeof(SimdMatrix3) );
	//	memset((void*)&elasticity2, 0, sizeof(SimdMatrix3) );

	//	// Zienckievicw vol.1
	//	float value = 1.0f - m_PoissonRatio;

	//	elasticity1.setElem( 0,0, value);
	//	elasticity1.setElem( 1,1, value);
	//	elasticity1.setElem( 2,2, value);

	//	value = m_PoissonRatio;
	//	elasticity1.setElem( 0,1, value);
	//	elasticity1.setElem( 0,2, value);
	//	elasticity1.setElem( 1,2, value);
	//	elasticity1.setElem( 1,0, value);
	//	elasticity1.setElem( 2,0, value);
	//	elasticity1.setElem( 2,1, value);

	//	value = (1 - 2.0f*m_PoissonRatio)/ 2.0f;
	//	elasticity2.setElem( 0,0, value);
	//	elasticity2.setElem( 1,1, value);
	//	elasticity2.setElem( 2,2, value);

	//	value = m_YoungModulus / ( (1.0f + m_PoissonRatio)*(1.0f - 2*m_PoissonRatio ) )  ;

	//	elasticity1 *= value;
	//	elasticity2 *= value;

	//	//// neighbors search
	//	//Phyxel* neighbors = m_ParticleSystem->m_NeighBors;
	//	//unsigned int* nb_neighbors = m_ParticleSystem->m_Nb_Neighbors;

	//	int index;

	//	std::vector<NeighborsDistance> neighbors_vector;

	//	neighbors_vector.reserve( m_Nb_Nodes );

	//	for (int i=0; i<m_Nb_Nodes; i++)
	//	{
	//		const SimdVector3& Pi = m_Nodes[i];

	//		index = 0;

	//		//neighbors = m_ParticleSystem->m_NeighBors + MAX_NEIGHBOR_PARTICLES*i;

	//		neighbors_vector.clear();

	//		for (int j=0;j<m_Nb_Nodes;j++)
	//		{
	//			if ( j != i)
	//			{
	//				const SimdVector3& Pj = m_Nodes[j];
	//				float square_distance = SSE::lengthSqr( Pi - Pj );// (rj-ri).norm2();
	//				//if (square_distance < square_particle_size )
	//				//{

	//				NeighborsDistance neigh;
	//				neigh.idx = j;
	//				neigh.dist = square_distance;
	//				neighbors_vector.push_back( neigh );

	//				index++;
	//				//}
	//			}
	//		}
	//		std::sort( neighbors_vector.begin(), neighbors_vector.end() );

	//		// X0
	//		m_X0[i].m[0] = m_Nodes[i];
	//		m_X0[i].m[0] = i;

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{
	//			//neighbors[j].m_Position = particles[neighbors_vector[j].idx].m_Position;
	//			// overloading operator =
	//			//m_X0[i].m[j+1].pos = m_Nodes[neighbors_vector[j].idx];
	//			//m_X0[i].m[j+1].idx = neighbors_vector[j].idx;
	//			m_X0[i].m[j+1] = m_Nodes[neighbors_vector[j].idx];
	//			m_X0[i].m[j+1] = neighbors_vector[j].idx;
	//			//neighbors[j].m_idx = neighbors_vector[j].idx;
	//		}

	//	}


	//	// compute support radius
	//	float mean_radius;
	//	float support_radius;

	//	for (int PhyxIdx=0; PhyxIdx<m_Nb_Nodes; PhyxIdx++)
	//	{

	//		mean_radius = 0.0f;

	//		const SimdVector3& Pi = m_Nodes[PhyxIdx];

	//		PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{

	//			const SimdVector3& Pj = (SimdVector3&)X0.m[j+1];
	//			mean_radius += SSE::lengthSqr( Pi - Pj );
	//		}

	//		mean_radius /= NB_NEIGHBORS;
	//		mean_radius = sqrt(mean_radius);


	//		// theoretical support radius 
	//		support_radius = m_SupportRadius * mean_radius;

	//		m_h[PhyxIdx] = support_radius;
	//		m_h2[PhyxIdx] = support_radius*support_radius;
	//		m_kernel_const[PhyxIdx] = KernelConstant / powf(support_radius, 9);
	//		m_kernel_gradconst[PhyxIdx] = -6.0f * m_kernel_const[PhyxIdx];

	//		m_elastkernel_const1[PhyxIdx] = 
	//			(float)( 1.0/(4.0*support_radius*support_radius*support_radius*(PiG/3.0-8.0/PiG+16.0/(PiG*PiG)) ) );

	//		m_elastkernel_gradconst[PhyxIdx] = 
	//			(float)( - PiG/(8.0*support_radius*support_radius*support_radius*support_radius*(PiG/3.0-8.0/PiG+16.0/(PiG*PiG)) ) );

	//		m_elastkernel_const2[PhyxIdx] = (float)( PiG/(2.0*support_radius));

	//		// la massa dovrebbe essere mi
	//		m_NodesMass[PhyxIdx] = mean_radius*mean_radius*mean_radius* m_Density;

	//	}

	//	// compute the mass scaling factor 
	//	float density_temp;

	//	float scal_factor = 0.0f;

	//	index = 0;

	//	for (int PhyxIdx=0; PhyxIdx<m_Nb_Nodes; PhyxIdx++)
	//	{

	//		const SimdVector3& Pi = m_Nodes[PhyxIdx];

	//		PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];

	//		density_temp = 0.0f;

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{

	//			const SimdVector3& Pj = X0.m[j+1].pos;

	//			density_temp += m_NodesMass[X0.m[j+1].idx] 
	//			* Kernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
	//			//* ElasticityKernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );

	//		}

	//		assert(density_temp > 1e-6 );
	//		scal_factor += ( m_Density /  density_temp ) ;
	//	}

	//	// adjust mass
	//	//scal_factor /= index;
	//	scal_factor /= m_Nb_Nodes;

	//	for (int i=0; i<m_Nb_Nodes; i++)
	//	{
	//		m_NodesMass[i] *= scal_factor;
	//		m_Mass += m_NodesMass[i];
	//	}


	//	// compute volume 
	//	density_temp = 0.0f;

	//	for (int PhyxIdx=0; PhyxIdx<m_Nb_Nodes; PhyxIdx++)
	//	{

	//		const SimdVector3& Pi = m_Nodes[PhyxIdx];

	//		PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];
	//		density_temp = 0.0f;

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{

	//			const SimdVector3& Pj = X0.m[j+1].pos;

	//			density_temp += m_NodesMass[X0.m[j+1].idx] 
	//			* Kernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
	//			//* ElasticityKernel( (float&)SSE::lengthSqr( Pj - Pi ), PhyxIdx );
	//		}

	//		m_Volume[PhyxIdx] = m_NodesMass[PhyxIdx] / density_temp;
	//	}


	//	// compute Undeformed State Matrix
	//	SimdMatrix3 UndeformedState_temp;

	//	for (int PhyxIdx=0; PhyxIdx<m_Nb_Nodes; PhyxIdx++)
	//	{

	//		const SimdVector3& Pi0 = m_Nodes[PhyxIdx];

	//		PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];

	//		memset((void*)&UndeformedState_temp, 0, sizeof(SimdMatrix3) );

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{

	//			const SimdVector3& Pj0 = X0.m[j+1].pos;

	//			UndeformedState_temp += m_NodesMass[X0.m[j+1].idx] * 
	//				Kernel( (float&)SSE::lengthSqr( Pj0 - Pi0 ), PhyxIdx )* SSE::outer( ( Pj0 - Pi0 ), ( Pj0 - Pi0 ) );
	//			//ElasticityKernel( (float&)SSE::lengthSqr( Pj0 - Pi0 ), PhyxIdx )* SSE::outer( ( Pj0 - Pi0 ), ( Pj0 - Pi0 ) );

	//		}

	//		m_UndeformedState[PhyxIdx] = SSE::inverse( UndeformedState_temp );

	//	}


	//	for (int PhyxIdx=0; PhyxIdx<m_Nb_Nodes; PhyxIdx++)
	//	{

	//		const SimdVector3& Xi = m_Nodes[PhyxIdx];

	//		PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];

	//		SimdVector3 gradW;

	//		memset((void*)&m_StrainMatrix[PhyxIdx], 0, sizeof(PhyxelStrainMatrix<NB_NEIGHBORS+1>) );

	//		for (int j=0; j< NB_NEIGHBORS; j++)
	//		{

	//			const SimdVector3& Xj = X0.m[j+1].pos;

	//			const float& Volj = m_Volume[X0.m[j+1].idx];

	//			gradW =  GradKernel( (SimdVector3&)(Xj - Xi), PhyxIdx );
	//			//gradW =  GradElasticityKernel( (SimdVector3&)(Xj - Xi), PhyxIdx );

	//			m_StrainMatrix[PhyxIdx].m[0][j+1].setElem( 0,0, gradW.getX() );
	//			m_StrainMatrix[PhyxIdx].m[0][j+1].setElem( 1,1, gradW.getY() );
	//			m_StrainMatrix[PhyxIdx].m[0][j+1].setElem( 2,2, gradW.getZ() );

	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 0,0, 0.5f*gradW.getY() );
	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 1,0, 0.5f*gradW.getX() );

	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 1,1, 0.5f*gradW.getZ() );
	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 2,1, 0.5f*gradW.getY() );

	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 0,2, 0.5f*gradW.getZ() );
	//			m_StrainMatrix[PhyxIdx].m[1][j+1].setElem( 2,2, 0.5f*gradW.getX() );

	//			m_StrainMatrix[PhyxIdx].m[0][j+1] *= Volj;
	//			m_StrainMatrix[PhyxIdx].m[1][j+1] *= Volj;

	//			m_StrainMatrix[PhyxIdx].m[0][0] -= m_StrainMatrix[PhyxIdx].m[0][j+1];
	//			m_StrainMatrix[PhyxIdx].m[1][0] -= m_StrainMatrix[PhyxIdx].m[1][j+1];

	//			//m_StrainMatrix[PhyxIdx].print( "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\strain.txt" );
	//		}

	//		//m_StrainMatrix[PhyxIdx].print( "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\strain.txt" );
	//		// C*strain

	//		PhyxelStrainMatrix<NB_NEIGHBORS+1> temp;
	//		for ( int j=0; j< NB_NEIGHBORS+1; j++ )
	//		{

	//			temp.m[0][j] = m_ElasticityMatrix[0] * m_StrainMatrix[PhyxIdx].m[0][j];

	//			m_StrainMatrix[PhyxIdx].m[0][j] = transpose( m_StrainMatrix[PhyxIdx].m[0][j] );

	//			temp.m[1][j] = m_ElasticityMatrix[1] * m_StrainMatrix[PhyxIdx].m[1][j];

	//			m_StrainMatrix[PhyxIdx].m[1][j] = transpose( m_StrainMatrix[PhyxIdx].m[1][j] );
	//		}


	//		const float& Vol_i = m_Volume[PhyxIdx];

	//		for ( int j=0; j< NB_NEIGHBORS+1; j++ )
	//		{
	//			for ( int k=0; k< NB_NEIGHBORS+1; k++ )
	//			{

	//				m_PhyxelStiffnessMatrix[PhyxIdx].m[k][j] = m_StrainMatrix[PhyxIdx].m[0][k] * temp.m[0][j];
	//				m_PhyxelStiffnessMatrix[PhyxIdx].m[k][j] += m_StrainMatrix[PhyxIdx].m[1][k] * temp.m[1][j];
	//				m_PhyxelStiffnessMatrix[PhyxIdx].m[k][j] *= Vol_i *0.5f;

	//			}
	//		}


	//		// compute K*X0
	//		memset( (void*)&m_KX0[PhyxIdx], 0, sizeof(PhyxelVector<NB_NEIGHBORS+1>) );

	//		for ( int j=0; j< NB_NEIGHBORS+1; j++ )
	//		{
	//			for ( int k=0; k<NB_NEIGHBORS+1; k++ )
	//			{

	//				m_KX0[PhyxIdx].m[j] += m_PhyxelStiffnessMatrix[PhyxIdx].m[j][k] * X0.m[k].pos;

	//			}
	//		}
	//	}

	//	//________________________________________________________

	//	createStiffness2PhyxelMap();


	//	if ( m_StiffnessMatrix != NULL )
	//	{
	//		for (int i=0; i< m_Nb_Nodes; i++)
	//		{
	//			if ( m_StiffnessMatrix[i] != NULL )
	//			{
	//				delete [] m_StiffnessMatrix[i];
	//				m_StiffnessMatrix[i] = NULL;
	//			}
	//		}
	//		delete [] m_StiffnessMatrix;
	//		m_StiffnessMatrix = NULL;
	//	}

	//	m_StiffnessMatrix = new SimdMatrix3*[m_Nb_Nodes];


	//	for (int i=0; i<m_Nb_Nodes; i++)
	//	{

	//		m_StiffnessMatrix[i] = new SimdMatrix3[MapStiffnessMatrix2Phyxels[i].size()];

	//		//memset(
	//		//	m_StiffnessMatrix[i], 
	//		//	0.0f, 
	//		//	MapStiffnessMatrix2Phyxels[i].size()* sizeof(SimdMatrix3) 
	//		//	);

	//	}

	//	createPhyxel2StiffnessMap();

	//}


	//template<unsigned int NB_NEIGHBORS>
	//void SphSoftBody<NB_NEIGHBORS>::allocateMem( const int& Nb_Phyxels, const int& Nb_Elements, const int& Nb_ConstraintNodes )
	//{

	//	m_Nb_Nodes		= Nb_Phyxels;

	//	// _______________________________________________________
	//	// Allocate Strain Matrix ( B )
	//	if (m_NodesVelocity != NULL) 
	//	{
	//		delete [] m_NodesVelocity;
	//	}
	//	m_NodesVelocity = new SimdVector3[Nb_Phyxels];
	//	memset(m_NodesVelocity, 0.0f, Nb_Phyxels*sizeof(SimdVector3) );


	//	if (m_StrainMatrix != NULL)
	//	{
	//		delete [] m_StrainMatrix;
	//	}
	//	m_StrainMatrix = new PhyxelStrainMatrix<NB_NEIGHBORS+1>[Nb_Phyxels];



	//	// _______________________________________________________
	//	// Allocate Element Stiffness Matrix
	//	if ( m_PhyxelStiffnessMatrix != NULL )
	//	{
	//		delete [] m_PhyxelStiffnessMatrix;
	//	}
	//	m_PhyxelStiffnessMatrix = new PhyxelStiffnessMatrix<NB_NEIGHBORS+1>[Nb_Phyxels];

	//	// _______________________________________________________
	//	// Allocate Undeformed State Matrix ( invP )
	//	if ( m_UndeformedState != NULL )
	//	{
	//		delete [] m_UndeformedState;
	//	}

	//	m_UndeformedState = new SimdMatrix3[Nb_Phyxels];

	//	memset(m_UndeformedState, 0, Nb_Phyxels * sizeof( SimdMatrix3 ) );


	//	// _______________________________________________________
	//	//  initials positions 
	//	if ( m_X0 != NULL )
	//	{
	//		delete [] m_X0;
	//	}
	//	m_X0 = new PhyxelNeighborsVector<NB_NEIGHBORS+1>[Nb_Phyxels];

	//	// _______________________________________________________
	//	// Allocate Force Vector
	//	if ( m_KX0 != NULL )
	//	{
	//		delete [] m_KX0;
	//	}
	//	m_KX0 = new PhyxelVector<NB_NEIGHBORS+1>[Nb_Phyxels];

	//	memset( m_KX0, 0.0f, Nb_Phyxels * sizeof(PhyxelVector<NB_NEIGHBORS+1>) );


	//	if ( m_RKX0 != NULL )
	//	{
	//		delete [] m_RKX0;
	//	}
	//	m_RKX0 = new SimdVector3[Nb_Phyxels];


	//	// _______________________________________________________
	//	// Allocate Extern Force Vector
	//	if ( m_ExternForces != NULL )
	//	{
	//		delete [] m_ExternForces;
	//	} 
	//	m_ExternForces = new SimdVector3[Nb_Phyxels];

	//	memset(m_ExternForces, 0.0f, Nb_Phyxels*sizeof(SimdVector3) );


	//	if ( m_NodesMass != NULL )
	//	{
	//		delete [] m_NodesMass;
	//	} 
	//	m_NodesMass = new float[Nb_Phyxels];

	//	memset(m_NodesMass, 1.0f, Nb_Phyxels*sizeof(float) );


	//	if ( m_Volume != NULL )
	//	{
	//		delete [] m_Volume;
	//	} 
	//	m_Volume = new float[Nb_Phyxels];



	//	//    used by solver
	//	// _______________________________________________________
	//	if ( m_RHS_Vector != NULL )
	//	{
	//		delete [] m_RHS_Vector;
	//	} 

	//	m_RHS_Vector = new SimdVector3[Nb_Phyxels];

	//	memset( m_RHS_Vector, 0.0f,   Nb_Phyxels * sizeof(SimdVector3) );


	//	if ( m_NewPhyxelVelocity != NULL )
	//	{
	//		delete [] m_NewPhyxelVelocity;
	//	} 

	//	m_NewPhyxelVelocity = new SimdVector3[Nb_Phyxels];

	//	memset(m_NewPhyxelVelocity, 0.0f, Nb_Phyxels * sizeof(SimdVector3) ); 


	//	if ( m_ResidualVector != NULL )
	//	{
	//		delete [] m_ResidualVector;
	//	} 

	//	m_ResidualVector = new SimdVector3[Nb_Phyxels];

	//	memset(m_ResidualVector, 0.0f, m_Nb_Nodes * sizeof(SimdVector3) );



	//	if ( m_DirectionVector != NULL )
	//	{
	//		delete [] m_DirectionVector;
	//	} 

	//	m_DirectionVector = new SimdVector3[Nb_Phyxels];

	//	memset(m_DirectionVector, 0.0f, m_Nb_Nodes*sizeof(SimdVector3) );


	//	if ( m_TempVector != NULL )
	//	{
	//		delete [] m_TempVector;
	//	} 

	//	m_TempVector = new SimdVector3[Nb_Phyxels];


	//	if ( MapDiagonalStiffnessMatrix2Phyxels != NULL )
	//	{
	//		delete [] MapDiagonalStiffnessMatrix2Phyxels;
	//	}
	//	MapDiagonalStiffnessMatrix2Phyxels = new unsigned short int[m_Nb_Nodes];


	//	// Kernels constants
	//	m_h = new float[Nb_Phyxels];

	//	m_h2 = new float[Nb_Phyxels];

	//	m_kernel_const = new float[Nb_Phyxels];

	//	m_kernel_gradconst = new float[Nb_Phyxels];

	//	m_elastkernel_const1 = new float[Nb_Phyxels];
	//	m_elastkernel_const2 = new float[Nb_Phyxels];

	//	m_elastkernel_gradconst = new float[Nb_Phyxels];

	//}


	//template<unsigned int NB_NEIGHBORS>
	//void SphSoftBody<NB_NEIGHBORS>::deAllocateMem(void)
	//{


	//	if (m_StrainMatrix != NULL)
	//	{
	//		delete [] m_StrainMatrix;
	//	}
	//	m_StrainMatrix = NULL;


	//	// _______________________________________________________
	//	// Allocate Element Stiffness Matrix
	//	if ( m_PhyxelStiffnessMatrix != NULL )
	//	{
	//		delete [] m_PhyxelStiffnessMatrix;
	//	}
	//	m_PhyxelStiffnessMatrix = NULL;

	//	// _______________________________________________________
	//	// Allocate Undeformed State Matrix ( invP )
	//	if ( m_UndeformedState != NULL )
	//	{
	//		delete [] m_UndeformedState;
	//	}

	//	m_UndeformedState = NULL;


	//	// _______________________________________________________
	//	//  initials positions 
	//	if ( m_X0 != NULL )
	//	{
	//		delete [] m_X0;
	//	}
	//	m_X0 = NULL;

	//	// _______________________________________________________
	//	// Allocate Force Vector
	//	if ( m_KX0 != NULL )
	//	{
	//		delete [] m_KX0;
	//	}
	//	m_KX0 = NULL;


	//	if ( m_RKX0 != NULL )
	//	{
	//		delete [] m_RKX0;
	//	}
	//	m_RKX0 = NULL;



	//	if ( m_Volume != NULL )
	//	{
	//		delete [] m_Volume;
	//	} 
	//	m_Volume = NULL;



	//	//    used by solver
	//	// _______________________________________________________
	//	if ( m_RHS_Vector != NULL )
	//	{
	//		delete [] m_RHS_Vector;
	//	} 

	//	m_RHS_Vector = NULL;


	//	if ( m_NewPhyxelVelocity != NULL )
	//	{
	//		delete [] m_NewPhyxelVelocity;
	//	} 

	//	m_NewPhyxelVelocity = NULL;


	//	if ( m_ResidualVector != NULL )
	//	{
	//		delete [] m_ResidualVector;
	//	} 

	//	m_ResidualVector = NULL;


	//	if ( m_DirectionVector != NULL )
	//	{
	//		delete [] m_DirectionVector;
	//	} 

	//	m_DirectionVector = NULL;


	//	if ( m_TempVector != NULL )
	//	{
	//		delete [] m_TempVector;
	//	} 
	//	m_TempVector = NULL;


	//	if ( MapDiagonalStiffnessMatrix2Phyxels != NULL )
	//	{
	//		delete [] MapDiagonalStiffnessMatrix2Phyxels;
	//	}
	//	MapDiagonalStiffnessMatrix2Phyxels = NULL;

	//	if ( m_PhyxK2StiffnessMatrixMap != NULL )
	//	{
	//		delete [] m_PhyxK2StiffnessMatrixMap;
	//	}
	//	m_PhyxK2StiffnessMatrixMap = NULL;

	//	//________________________________________________________


	//	if ( m_StiffnessMatrix != NULL )
	//	{
	//		for (int i=0; i<mNbNodes; i++)
	//		{
	//			if ( m_StiffnessMatrix[i] != NULL )
	//			{
	//				delete [] m_StiffnessMatrix[i];
	//				m_StiffnessMatrix[i] = NULL;
	//			}
	//		}
	//		delete [] m_StiffnessMatrix;
	//		m_StiffnessMatrix = NULL;
	//	}


	//	// da eliminare .. viene fatto nella classe base
	//	if ( m_collisionShape != NULL )
	//	{
	//		delete m_collisionShape;
	//		m_collisionShape;
	//	}

	//	// Kernels constants
	//	delete [] m_h;

	//	delete [] m_h2;

	//	delete [] m_kernel_const;

	//	delete [] m_kernel_gradconst;

	//	delete [] m_elastkernel_const1;
	//	delete [] m_elastkernel_const2;

	//	delete [] m_elastkernel_gradconst;


	//}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::createPhyxel2StiffnessMap(void)
	{


		if ( m_PhyxK2StiffnessMatrixMap != NULL )
		{
			delete [] m_PhyxK2StiffnessMatrixMap;
		}
		m_PhyxK2StiffnessMatrixMap = 
			new PhyxK<NB_NEIGHBORS+1>[mNbNodes];


		unsigned short int pos = 0;

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; PhyxIdx++)
		{

			PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];

			// main phyxel
			pos = 0;
			while ( PhyxIdx !=  MapStiffnessMatrix2Phyxels[PhyxIdx][pos])
			{
				pos++;
			}
			m_PhyxK2StiffnessMatrixMap[PhyxIdx].ij[0][0] = 
				&m_StiffnessMatrix[PhyxIdx][pos];


			for (int j=0; j< NB_NEIGHBORS; j++)
			{
				int& idx = X0.m[j+1].idx; // neighbors[j].m_idx;

				pos = 0;
				while ( idx !=  MapStiffnessMatrix2Phyxels[PhyxIdx][pos])
				{
					pos++;
				}
				m_PhyxK2StiffnessMatrixMap[PhyxIdx].ij[0][j+1] = 
					&m_StiffnessMatrix[PhyxIdx][pos];

				pos = 0;
				while ( PhyxIdx !=  MapStiffnessMatrix2Phyxels[idx][pos])
				{
					pos++;
				}
				m_PhyxK2StiffnessMatrixMap[PhyxIdx].ij[j+1][0] = 
					&m_StiffnessMatrix[idx][pos];
			}

			// neighbors phyxel
			for (int j=0; j<NB_NEIGHBORS; j++)
			{
				int& n_i = X0.m[j+1].idx; // neighbors[j].m_idx;

				for (int k=0; k<NB_NEIGHBORS; k++)
				{
					int& n_j = X0.m[k+1].idx; // neighbors[k].m_idx;

					pos = 0;
					while ( n_j !=  MapStiffnessMatrix2Phyxels[n_i][pos])
					{
						pos++;
					}

					m_PhyxK2StiffnessMatrixMap[PhyxIdx].ij[j+1][k+1] = 
						&m_StiffnessMatrix[n_i][pos];

				}
			}

		}


	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::createStiffness2PhyxelMap(void)
	{

		MapStiffnessMatrix2Phyxels.reserve( mNbNodes );

		std::vector<unsigned short int> int0;

		std::vector<unsigned short int>::iterator iter;

		for (int i=0; i<mNbNodes; i++)
		{
			MapStiffnessMatrix2Phyxels.push_back( int0 );
			MapStiffnessMatrix2Phyxels[i].reserve( 32 );
		}


		for (int PhyxIdx=0; PhyxIdx< mNbNodes; PhyxIdx++)
		{

			// main phyxel
			iter = std::find ( MapStiffnessMatrix2Phyxels[PhyxIdx].begin(), 
				MapStiffnessMatrix2Phyxels[PhyxIdx].end(), PhyxIdx );

			if ( iter == MapStiffnessMatrix2Phyxels[PhyxIdx].end() )
			{
				MapStiffnessMatrix2Phyxels[PhyxIdx].push_back( PhyxIdx );
			}

			PhyxelNeighborsVector<NB_NEIGHBORS+1>& X0 = m_X0[PhyxIdx];
			//neighbors = m_ParticleSystem->m_NeighBors + MAX_NEIGHBOR_PARTICLES*i;


			for (int j=0; j< NB_NEIGHBORS; j++)
			{

				int& idx = X0.m[j+1].idx;


				iter = std::find ( MapStiffnessMatrix2Phyxels[PhyxIdx].begin(), 
					MapStiffnessMatrix2Phyxels[PhyxIdx].end(), idx );

				if ( iter == MapStiffnessMatrix2Phyxels[PhyxIdx].end() )
				{
					MapStiffnessMatrix2Phyxels[PhyxIdx].push_back( idx );
					MapStiffnessMatrix2Phyxels[idx].push_back( PhyxIdx );
				}

			}

			for (int j=0; j< NB_NEIGHBORS; j++)
			{

				int& n_i = X0.m[j+1].idx;

				iter = std::find ( MapStiffnessMatrix2Phyxels[n_i].begin(), 
					MapStiffnessMatrix2Phyxels[n_i].end(), n_i );

				if ( iter == MapStiffnessMatrix2Phyxels[n_i].end() )
				{
					MapStiffnessMatrix2Phyxels[n_i].push_back( n_i );
				}

				for (int k=j+1; k< NB_NEIGHBORS; k++)
				{
					int& n_j = X0.m[k+1].idx; // neighbors[k].m_idx;

					iter = std::find ( MapStiffnessMatrix2Phyxels[n_j].begin(), 
						MapStiffnessMatrix2Phyxels[n_j].end(), n_i );

					if ( iter == MapStiffnessMatrix2Phyxels[n_j].end() )
					{
						MapStiffnessMatrix2Phyxels[n_j].push_back( n_i );
						MapStiffnessMatrix2Phyxels[n_i].push_back( n_j );
					}

				}

			}

			sort (MapStiffnessMatrix2Phyxels[PhyxIdx].begin(),
				MapStiffnessMatrix2Phyxels[PhyxIdx].end() );
		}

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; PhyxIdx++)
		{
			sort (MapStiffnessMatrix2Phyxels[PhyxIdx].begin(),
				MapStiffnessMatrix2Phyxels[PhyxIdx].end() );
		}

		// CreateDiagMap
		size_t sz;

		for (int PhyxIdx=0; PhyxIdx<mNbNodes; PhyxIdx++)
		{
			sz = MapStiffnessMatrix2Phyxels[PhyxIdx].size();

			for ( int j=0; j<sz; j++ )
			{
				if ( MapStiffnessMatrix2Phyxels[PhyxIdx][j] == PhyxIdx )
				{
					MapDiagonalStiffnessMatrix2Phyxels[PhyxIdx] = j;
					break;
				}

			}
		}

	}




	static inline void MatVecProd( const SimdMatrix3* mat, const SimdVector3* vec, 
		int NbRows,	const ushort* rows, const ushort* cols, 
		SimdVector3* res )
	{
		ushort nbcols;

		while ( NbRows-- > 0 )
		{
			nbcols = *(rows+1) - *rows;
			memset( res, 0, 16 );

			while (nbcols-- > 0)
			{
				*res += (*mat++) * vec[(*cols++)];
			}
			++res;
			++rows;			
		}
	}


	static inline float InnProd( const SimdVector3* vec1, const SimdVector3* vec2,
		int NbElems )
	{
		SimdVector3 dot_acc = SimdVector3::Zero();
		while ( NbElems-- > 0)
		{
			dot_acc += SSE::mulPerElem( (*vec1++), (*vec2++) );
		}
		return dot_acc[0] + dot_acc[1] + dot_acc[2];
	}



	//template<unsigned int NB_NEIGHBORS>
	//float SphSoftBody<NB_NEIGHBORS>::simulate( const float& TimeStep )
	//{
	//	tick_count starTicks = tick_count::now();


	//	//PrintVector( m_RHS_Vector, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\b.txt");


	//	//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\A.txt");


	//	computeRHS();

	//	computeLHS();

	//	//PrintVector( m_RHS_Vector, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\b.txt");


	//	//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\A.txt");

	//	SimdMatrix3* PhyxelRow;
	//	unsigned int Nb_Elms;

	//	memcpy( m_ResidualVector, m_RHS_Vector, mNbNodes*sizeof(SimdVector3) );

	//	for (int PhyxelIdx=0; PhyxelIdx<mNbNodes; PhyxelIdx++)
	//	{

	//		PhyxelRow =  m_StiffnessMatrix[PhyxelIdx];

	//		std::vector<unsigned short int>& Map2Phyxels = 
	//			MapStiffnessMatrix2Phyxels[PhyxelIdx];

	//		Nb_Elms = Map2Phyxels.size();

	//		for (int j=0; j<Nb_Elms; j++)
	//		{
	//			m_ResidualVector[PhyxelIdx] -= PhyxelRow[j]* mNodesVelocity[Map2Phyxels[j]];
	//		}
	//	}

	//	for (int PhyxIdx=0; PhyxIdx< mNbConstraintNodes; PhyxIdx++)
	//	{
	//		const int& idx = mConstraintNodes[PhyxIdx].index;
	//		m_ResidualVector[idx] = SimdVector3::Zero(); // m_ConstraintNodes[i].m_value;
	//	}

	//	// initial auxiliary vector

	//	memcpy( m_DirectionVector, 
	//		m_ResidualVector, mNbNodes*sizeof(SimdVector3) );


	//	double rho = innerProduct(m_ResidualVector, 
	//		m_ResidualVector, mNbNodes );

	//	double invNorm_b = innerProduct( m_RHS_Vector, 
	//		m_RHS_Vector, mNbNodes );

	//	// 1.0/ ||b||
	//	invNorm_b = 1.0/invNorm_b;


	//	double  alpha, beta;
	//	double denominator = 0.0;
	//	double new_rho = 0.0;
	//	double error;

	//	for (int iter=0; iter < m_Nb_Solver_Iteration; iter++)
	//	{

	//		// m_TempVector = A*p

	//		//PrintVector( m_DirectionVector, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\b.txt");


	//		//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\A.txt");


	//		matrixVectorProduct( m_StiffnessMatrix, 
	//			m_DirectionVector, mNbNodes, 
	//			MapStiffnessMatrix2Phyxels, 
	//			m_TempVector );

	//		//PrintVector( m_TempVector, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\v.txt");


	//		for (int PhyxIdx=0; PhyxIdx< mNbConstraintNodes; PhyxIdx++)
	//		{
	//			const int& idx = mConstraintNodes[PhyxIdx].index;
	//			m_TempVector[idx] = SimdVector3::Zero(); // m_ConstraintNodes[i].m_value;
	//		}

	//		denominator = innerProduct( m_DirectionVector,
	//			m_TempVector,  mNbNodes );

	//		// 
	//		if ( fabs(denominator)  < m_Solver_Tolerance ) // 1e-5f
	//		{
	//			//continue;
	//			break;
	//		}

	//		// new alpha
	//		alpha = rho / denominator;

	//		// Update velocity 
	//		for (int PhyxelIdx=0; PhyxelIdx<mNbNodes; PhyxelIdx++)
	//		{
	//			m_NewPhyxelVelocity[PhyxelIdx] += alpha*m_DirectionVector[PhyxelIdx];
	//		}

	//		// Update residual vector
	//		for (int PhyxelIdx=0; PhyxelIdx<mNbNodes; PhyxelIdx++)
	//		{
	//			m_ResidualVector[PhyxelIdx] -= alpha * m_TempVector[PhyxelIdx];
	//		}

	//		new_rho = innerProduct( m_ResidualVector, m_ResidualVector, mNbNodes);


	//		// new beta
	//		beta = new_rho / rho;

	//		// update rho
	//		rho = new_rho;

	//		error = new_rho * invNorm_b;
	//		// check 
	//		if ( error  < m_Solver_Tolerance )
	//		{

	//			break;
	//		}

	//		// update direction vector (auxiliary)
	//		for (int PhyxelIdx=0; PhyxelIdx< mNbNodes; PhyxelIdx++)
	//		{
	//			m_DirectionVector[PhyxelIdx] = 
	//				beta * m_DirectionVector[PhyxelIdx]
	//			+ m_ResidualVector[PhyxelIdx];
	//		}

	//		//project to constraint space
	//		for (int PhyxIdx=0; PhyxIdx< mNbConstraintNodes; PhyxIdx++)
	//		{
	//			const int& idx = mConstraintNodes[PhyxIdx].index;
	//			m_DirectionVector[idx] =SimdVector3::Zero(); // m_ConstraintNodes[i].m_value;
	//		}

	//	}


	//	memcpy( mNodesVelocity, 
	//		m_NewPhyxelVelocity, mNbNodes*sizeof(SimdVector3));

	//	//PrintVector( m_NewPhyxelVelocity, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\v.txt");


	//	SimdVector3* nodepositions = reinterpret_cast<SimdVector3*>(mNodes);
	//	const SimdVector3* PhyxelVelocities = m_NewPhyxelVelocity;

	//	int nb_nodes = mNbNodes;
	//	while ( nb_nodes-- > 0 )
	//	{
	//		(*nodepositions++) +=  SSE::mulPerElem( *PhyxelVelocities++, m_TimeStepVector);
	//	}

	//	//for (int PhyxelIdx=0; PhyxelIdx<m_Nb_Nodes; PhyxelIdx++)
	//	//{
	//	//	m_Nodes[PhyxelIdx] +=  
	//	//		SSE::mulPerElem( m_NewPhyxelVelocity[PhyxelIdx], m_TimeStepVector);
	//	//}


	//	return  ( tick_count::now() - starTicks ).seconds();
	//}



	//template<unsigned int NB_NEIGHBORS>
	//float SphSoftBody<NB_NEIGHBORS>::computeRHS()
	//{
	//	tick_count starTicks = tick_count::now();


	//	SimdMatrix3* PhyxelRow;

	//	unsigned int Nb_Elms;

	//	memset((void*)m_RHS_Vector, 0, mNbNodes*sizeof(SimdVector3) );


	//	for (int PhyxelIdx=0; PhyxelIdx<mNbNodes; PhyxelIdx++)
	//	{

	//		PhyxelRow = m_StiffnessMatrix[PhyxelIdx];

	//		std::vector<unsigned short int>& Map2Phyxels = MapStiffnessMatrix2Phyxels[PhyxelIdx];

	//		Nb_Elms = Map2Phyxels.size();

	//		// RHS = K*Xi 
	//		for (int j=0; j<Nb_Elms; j++)
	//		{
	//			m_RHS_Vector[PhyxelIdx] -= PhyxelRow[j]* mNodes[Map2Phyxels[j]];
	//		}

	//		// RHS = -K*Xi -F0  

	//		m_RHS_Vector[PhyxelIdx] -= m_RKX0[PhyxelIdx];


	//		// RHS = -K*Xi -F0  - Fext
	//		m_RHS_Vector[PhyxelIdx] += mExternForces[PhyxelIdx];

	//		// add Gravity;
	//		// RHS = -K*Xi -F0  - Fext - mg
	//		m_RHS_Vector[PhyxelIdx] += 
	//			SimdVector3( 0.0f, m_Gravity*mNodeMasses[PhyxelIdx], 0.0f );

	//		// RHS = dt*( -K*Xi -F0 )
	//		m_RHS_Vector[PhyxelIdx] = 
	//			SSE::mulPerElem( m_RHS_Vector[PhyxelIdx], m_TimeStepVector);


	//		m_RHS_Vector[PhyxelIdx] += 
	//			SSE::mulPerElem( mNodesVelocity[PhyxelIdx], SimdVector3( mNodeMasses[PhyxelIdx]) );

	//	}


	//	return  ( tick_count::now() - starTicks ).seconds();
	//}


	//template<unsigned int NB_NEIGHBORS>
	//float SphSoftBody<NB_NEIGHBORS>::computeLHS(void)
	//{

	//	tick_count starTicks = tick_count::now();


	//	SimdMatrix3* PhyxelRow;

	//	int MapDiag2Phyxels;

	//	int Nb_Elms;

	//	const int& nb_particles = mNbNodes;

	//	//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\K.txt");


	//	// LHS = M + dt*C + dt*dt*K

	//	//  LHS = dt*dt*K
	//	for (int PhyxelIdx=0; PhyxelIdx<nb_particles; PhyxelIdx++)
	//	{

	//		PhyxelRow =  m_StiffnessMatrix[PhyxelIdx];

	//		Nb_Elms = MapStiffnessMatrix2Phyxels[PhyxelIdx].size();

	//		for (int j=0; j<Nb_Elms; j++ )
	//		{
	//			PhyxelRow[j] *=  m_TimeStep2;
	//		}

	//	}


	//	// LHS = M + dt*C + dt*dt*K
	//	for (int PhyxelIdx=0; PhyxelIdx<mNbNodes; PhyxelIdx++)
	//	{

	//		PhyxelRow =  m_StiffnessMatrix[PhyxelIdx];

	//		MapDiag2Phyxels = MapDiagonalStiffnessMatrix2Phyxels[PhyxelIdx];

	//		float& mass =  mNodeMasses[PhyxelIdx];

	//		PhyxelRow[MapDiag2Phyxels] += SimdMatrix3(
	//			SimdVector3( mass, 0.0f, 0.0f ),
	//			SimdVector3( 0.0f, mass, 0.0f ),
	//			SimdVector3( 0.0f, 0.0f, mass )
	//			);

	//		// damping
	//		PhyxelRow[MapDiag2Phyxels] += 
	//			SSE::mulPerElem(m_DampingMatrix, m_TimeStepMatrix);
	//	}


	//	return  ( tick_count::now() - starTicks ).seconds();
	//}



	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::PrintStiffnessMatrix( const char* file_name )
	{
		Matrix3* NodeRow;

		//union { Vector3 v; float s[4]; } tmp;
		//Vector3* tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");

		const ushort* rowbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const Matrix3* stiffmatbuf = mStiffnessMatrix->lockForRead<Matrix3>();

		for (int Index=0; Index<mNbNodes; ++Index )
		{
			const ushort* startId = &colsbuf[(*rowbuf)];
			const ushort* endId = &colsbuf[(*rowbuf)+1];
			const ushort nbId = ( *(rowbuf+1) - (*rowbuf)) -1;
			const Matrix3* NodeRow =  &stiffmatbuf[ (*rowbuf) ];
			//std::vector<int>& Map2Nodes = MapStiffnessMatrix2Nodes[Index];


			int n = 0;
			for (int j=0; j<mNbNodes; ++j )
			{

				if ( j == startId[n] )
				{
					const Vector3& tmp = NodeRow[n].getRow(0);				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp[0], tmp[1], tmp[2] );
					if ( n < nbId )
					{
						++n ;
					}
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}

			n = 0;
			fprintf(pFile,"\n");
			for (int j=0; j<mNbNodes; ++j )
			{
				if ( j == startId[n] )
				{
					const Vector3& tmp = NodeRow[n].getRow(1);				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp[0], tmp[1], tmp[2] );
					if ( n < nbId )
						++n;
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}

			n = 0;
			fprintf(pFile,"\n");
			for (int j=0; j<mNbNodes; j++ )
			{
				if ( j == startId[n] )
				{
					const Vector3& tmp = NodeRow[n].getRow(2) ;				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp[0], tmp[1], tmp[2] );
					if ( n < nbId )
						++n;
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}
			fprintf(pFile,"\n");

			++rowbuf;
		}

		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

		fclose(pFile);
	}


}; // namespace SimStep