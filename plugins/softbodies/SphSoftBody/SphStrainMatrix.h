#ifndef SimStep_SphStrainMatrix_h__
#define SimStep_SphStrainMatrix_h__


//#include "Utilities/AlignedObject.h"


namespace SimStep
{

	template<unsigned int size>
	//align16_struct 
	class PhyxelStrainMatrix //: public AlignedObject16
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( PhyxelStrainMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		SimdMatrix3 m[2][size];


		void print(const char* file_name )
		{
			union { __m128 v; float s[4]; } tmp;

			FILE* pFile;
			fopen_s(&pFile, file_name,"w");

			for (int i=0; i<2; i++ )
			{

				for (int j=0; j<size; j++ )
				{
					tmp.v = m[i][j].getRow(0).get128();		
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
				}

				fprintf(pFile,"\n");


				for (int j=0; j<size; j++ )
				{
					tmp.v = m[i][j].getRow(1).get128();		
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
				}

				fprintf(pFile,"\n");


				for (int j=0; j<size; j++ )
				{
					tmp.v = m[i][j].getRow(2).get128();		
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
				}

				fprintf(pFile,"\n");

			}

			fclose(pFile);
		}
	};

	
}; // namespace SimStep

#endif // SimStep_SphStrainMatrix_h__