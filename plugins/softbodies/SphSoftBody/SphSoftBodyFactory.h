#ifndef SimStep_SphSoftBodyFactory_h__
#define SimStep_SphSoftBodyFactory_h__

#include <SoftBodyDefinition.h>

#include <Core/SoftBodyFactory.h>

#include <Utilities/String.h>

namespace SimStep
{

	template<unsigned int NB_NEIGHBORS>
	class SphSoftBodyFactory : public SoftBodyFactory
	{
	public:

		static String FACTORY_TYPE_NAME;

	protected:

		/// Internal implementation of create method - must be overridden
		SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0);


	public:

		SphSoftBodyFactory(void) : SoftBodyFactory() {}
		virtual ~SphSoftBodyFactory(void) {}


		/// Get the type of the object to be created
		virtual const String& getType(void) const;


		/** Destroy an instance of the object */
		void destroyInstance(SoftBody* softbody);

	};



	//class _SoftBodyExport SphSoftBodyFactory5 : public SoftBodyFactory
	class _SoftBodyExport SphSoftBodyFactory5 : public SphSoftBodyFactory<5>
	{
	public:

		//static String FACTORY_TYPE_NAME;


	protected:

		/// Internal implementation of create method - must be overridden
		SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0);


	public:

		//SphSoftBodyFactory5(void) : SoftBodyFactory() {}
		SphSoftBodyFactory5(void) : SphSoftBodyFactory() {}
		virtual ~SphSoftBodyFactory5(void) {}


		/// Get the type of the object to be created
		virtual const String& getType(void) const;



		/** Destroy an instance of the object */
		void destroyInstance(SoftBody* softbody);

	};


	//class _SoftBodyExport SphSoftBodyFactory6 : public SoftBodyFactory
	class _SoftBodyExport SphSoftBodyFactory6 : public SphSoftBodyFactory<6>
	{
	public:

		//static String FACTORY_TYPE_NAME;


	protected:

		/// Internal implementation of create method - must be overridden
		SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0);


	public:

		//SphSoftBodyFactory6(void) : SoftBodyFactory() {}
		SphSoftBodyFactory6(void) : SphSoftBodyFactory() {}
		virtual ~SphSoftBodyFactory6(void) {}


		/// Get the type of the object to be created
		virtual const String& getType(void) const;


		/** Destroy an instance of the object */
		void destroyInstance(SoftBody* softbody);

	};
	

	class _SoftBodyExport SphSoftBodyFactory7 : public SphSoftBodyFactory<7>
	{
	public:

		//static String FACTORY_TYPE_NAME;


	protected:

		/// Internal implementation of create method - must be overridden
		SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0);


	public:


		SphSoftBodyFactory7(void) : SphSoftBodyFactory() {}
		virtual ~SphSoftBodyFactory7(void) {}


		/// Get the type of the object to be created
		virtual const String& getType(void) const;


		/** Destroy an instance of the object */
		void destroyInstance(SoftBody* softbody);

	};

}; // namespace SimStep

#endif // SimStep_SphSoftBodyFactory_h__