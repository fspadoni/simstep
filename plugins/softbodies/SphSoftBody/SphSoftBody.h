#ifndef SimStep_SphSoftBody_h__
#define SimStep_SphSoftBody_h__

#include <SoftBodyDefinition.h>

#include <Core/SoftBody.h>

#include <FemSoftBody/FemElasticityMatrix.h>
#include <FemSoftBody/Tetrahedron.h>
#include "SphStrainMatrix.h"
#include "SphPhyxelVector.h"
#include "SphStiffnessMatrix.h"


namespace SimStep
{

	// __________________________________________________
	// 
	//	Class: SphSoftBody
	//____________________________________________________

	template<unsigned int NB_NEIGHBORS>
	class _SoftBodyExport SphSoftBody : public SoftBody
	{
	private:

		//SphSoftBody() {}
		//SphSoftBody(const SphSoftBody& sb ) {}
		//const SphSoftBody& operator = (const SphSoftBody& sb ) {return *this;}

	public:

		SphSoftBody( const SoftBodyParams& params, const DataValuePairList* otherparams = 0 );

		virtual ~SphSoftBody();


		// Public Interface 
		virtual const String& getType(void) const;

		virtual float assembleStiffnessMatrix(void* user_pointer = 0);

		virtual float solve( const float& TimeStep, void* user_pointer = 0 );

		virtual Vector3 getStressForce( const int& nodeId ) const;

		virtual const Buffer<Vector3>& getStressForces( void ) const;

		virtual float getVertexMass(int vertex) const;

		virtual SoftBody* clone(void);

		virtual void updateCollisionShape(void);

		virtual void setPosition(const Vector3& newpos );

		virtual void setOrientation(const Quat& newor );

		virtual void addForce(const int& nodeId, const Vector3& force );


		virtual Vector3 getVertexNormal(int vertexId) const;

		virtual const BufferSharedPtr<Vector3>& getVertexNormalBuffer(void) const;

		virtual void setVelocity(const Vector3& newvel );

		virtual void reset(void);

		// __________________


	protected:



		// ElasticityMatrix
		//SimdMatrix3 m_ElasticityMatrix[2];
		BufferSharedPtr<ElasticityMatrix> mD;

		// StrainMatrix
		BufferSharedPtr<PhyxelStrainMatrix<NB_NEIGHBORS+1> > mStrainMatrix;
		//PhyxelStrainMatrix<NB_NEIGHBORS+1>* m_StrainMatrix;


		// The initials positions in its frame
		//PhyxelNeighborsVector<NB_NEIGHBORS+1>* m_X0;
		BufferSharedPtr<PhyxelNeighborsVector<NB_NEIGHBORS+1> > mX0;
		

		//PhyxelVector<NB_NEIGHBORS+1>* m_KX0;
		BufferSharedPtr<PhyxelVector<NB_NEIGHBORS+1> > mKX0;


		//SimdMatrix3* m_UndeformedState;
		BufferSharedPtr<SimdMatrix3> mUndeformedStateVector; 


		//PhyxelStiffnessMatrix
		//PhyxelStiffnessMatrix<NB_NEIGHBORS+1>* m_PhyxelStiffnessMatrix;
		BufferSharedPtr<PhyxelStiffnessMatrix<NB_NEIGHBORS+1>> mKphyxel;

		//StiffnessMatrix
		//SimdMatrix3** m_StiffnessMatrix;
		BufferSharedPtr<Matrix3> mStiffnessMatrix;

		//SimdVector3* m_RKX0; // F0


		BufferSharedPtr<Matrix3> mPhyxelRotations;


	protected:


		// solver 
		// NewNodeVelocity
		BufferSharedPtr<Vector3> mNewNodeVelocity; // Vector3

		// RHS_Vector
		BufferSharedPtr<Vector3> mRHSVector; // Vector3

		// ResidualVector
		BufferSharedPtr<Vector3> mResidualVector; // Vector3

		// DirectionVector
		BufferSharedPtr<Vector3> mDirectionVector; // Vector3

		BufferSharedPtr<Vector3> mTemp; // Vector3


		// _________
		//SimdVector3* m_NewPhyxelVelocity;
		//SimdVector3* m_RHS_Vector;
		//SimdVector3* m_ResidualVector;
		//SimdVector3* m_DirectionVector;
		//SimdVector3* m_TempVector;
		// _________


		float* m_Volume;
		BufferSharedPtr<float> mVolume;

		//std::vector< std::vector<unsigned short int> > MapStiffnessMatrix2Phyxels;

		//unsigned short int* MapDiagonalStiffnessMatrix2Phyxels;


		//template<unsigned int size > class PhyxK
		//{
		//public:
		//	SimdMatrix3* ij[size][size];
		//};

		//PhyxK<NB_NEIGHBORS+1>* m_PhyxK2StiffnessMatrixMap;


		// connection graph
		BufferSharedPtr<ushort> mCloumns; //std::vector<std::vector<ushort>>
		BufferSharedPtr<ushort> mRows; // ushort
		BufferSharedPtr<ushort> mDiagonals; //ushort 
		
		template<unsigned int size > class Kphyxel
		{
		public:
			ushort ij[size][size];
		};
		BufferSharedPtr< Kphyxel<NB_NEIGHBORS+1> > mPhyxK2StiffnessMatrixMap;


		BufferSharedPtr<TetrahedronElement> mElements;

		BufferSharedPtr<BarycentricCoords> mBarycentricCoords;

		BufferSharedPtr<ushort> mBarycentricIndex;

		BufferSharedPtr<Vector3> mNewVertexNormals;


		void computeStressForces(void);
		void updateVertices(void);
		void updateVertexVels(void);
		void updateVertexNormals(void);

		// _____________________________________

		// dt Matrix
		SimdVector3 mTimeStepVector;
		SimdMatrix3 mTimeStepMatrix;
		SimdMatrix3 mTimeStep2Matrix;

		// C Matrix
		SimdMatrix3 m_DampingMatrix;


	protected:

		// KernelSupport
		//float* m_h;
		BufferSharedPtr<float> mh;

		//float* m_h2;
		BufferSharedPtr<float> mh2;

		//float* m_kernel_const;
		BufferSharedPtr<float> mKernelConst;

		//float* m_kernel_gradconst;
		BufferSharedPtr<float> mKernelGradConst;

		//float* m_elastkernel_const1;
		BufferSharedPtr<float> mElastKernelConst1;

		//float* m_elastkernel_const2;
		BufferSharedPtr<float> mElastKernelConst2;

		//float* m_elastkernel_gradconst;
		BufferSharedPtr<float> mElasticKernelGradConst;



		// polynomial Kernel
		inline float kernel( float& r2, const int& idx);

		inline SimdVector3 gradKernel( SimdVector3& r, const int& idx);

		// Elasticity Kernel
		inline float elasticityKernel( float& r, const int& idx);

		inline SimdVector3 gradElasticityKernel( SimdVector3& r, const int& idx);



		// _____________________________________
		// Desc
		float m_TimeStep;
		float m_TimeStep2;
		float m_Density;
		float m_YoungModulus;
		float m_PoissonRatio;
		float m_Damping;
		//float m_Gravity;
		int	  m_Nb_Solver_Iteration;
		float m_Solver_Tolerance;
		char* m_MeshFile;


		// SPH desc
		float m_PhyxelSize;
		float m_SupportRadius;

		
	protected:
		//Bool create(const SoftBodyParams& param);

		//void allocateMem( const int& Nb_Nodes, const int& nb_Neighbors, const int& Nb_ConstraintNodes );
		//void deAllocateMem(void);

		float simulate( const float& TimeStep );


		//Bool loadMesh(char *filename);

		//Bool loadPhyxels(char *filename);
		//bool LoadElements(char *filename);
		//Bool loadTriangles(char *filename);
		//Bool loadCollisionNodes(char *filename);
		//Bool loadConstraints(char *filename);

		//void initialize(void);

		void createPhyxel2StiffnessMap(void);
		void createStiffness2PhyxelMap(void);


		float computeRHS(void);
		float computeLHS(void);


		void printMatrix( SimdMatrix3**  mat,
			const char* file_name );

		void printVector( SimdVector3* vec,
			const char* file_name );



		// -----------------------

		void create(const SoftBodyParams& param);

		virtual void initializeBuffers(void);

		void initializePhyxelsNeighbors(void);
		void initializePhyxels(void);
		//void initializeElements(void);
		void initializeGraph(void);
		void initializeStiffnessMap(void);


		float computeRhs(void);
		float computeLhs(void);

		void PrintStiffnessMatrix( const char* file_name );
		void ConjGradSolver(void);

	};

	
	class _SoftBodyExport SphSoftBody5 : public SphSoftBody<5>
	{
	public:
		SphSoftBody5( const SoftBodyParams& params, const DataValuePairList* otherparams = 0 ) 
			: SphSoftBody<5>(params, otherparams )
		{

		}

		virtual ~SphSoftBody5() 
		{
			//SphSoftBody<5>::deAllocateMem();
		}

		//const String& getType(void) const;

	};

	class _SoftBodyExport SphSoftBody6 : public SphSoftBody<6>
	{
	public:
		SphSoftBody6( const SoftBodyParams& params, const DataValuePairList* otherparams = 0 ) 
			: SphSoftBody<6>(params, otherparams )
		{
		}

		virtual ~SphSoftBody6() 
		{
			//SphSoftBody<6>::deAllocateMem();
		}

		//const String& getType(void) const;

	};



	class _SoftBodyExport SphSoftBody7 : public SphSoftBody<7>
	{
	public:
		SphSoftBody7( const SoftBodyParams& params, const DataValuePairList* otherparams = 0 ) 
			: SphSoftBody<7>(params, otherparams )
		{
		}

		virtual ~SphSoftBody7() 
		{
		}

		//const String& getType(void) const;

	};


}; // namespace SimStep

#endif // SimStep_SphSoftBody_h__