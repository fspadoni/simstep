#include "SphSoftBodyFactory.h"
#include "SphSoftBodyFactory.inl"

#include "SphSoftBody.h"

#include <sstream>

namespace SimStep
{

	//template<size_t NB_NEIGHBORS>
	//const char* factoryName(const char* name)
	//{
	//	//char nbNeigh[3];
	//	//itoa()
	//	std::stringstream ret;
	//	ret << name << NB_NEIGHBORS;
	//	return ret.str().c_str();
	//}

	template<size_t NB_NEIGHBORS>
	String SphSoftBodyFactory<NB_NEIGHBORS>::FACTORY_TYPE_NAME = factoryName<NB_NEIGHBORS>("SphSoftBody");


	//String SphSoftBodyFactory5::FACTORY_TYPE_NAME = "SphSoftBody5";


	/// Get the type of the object to be created
	const String& SphSoftBodyFactory5::getType(void) const
	{
		return FACTORY_TYPE_NAME;
	}

	/// Internal implementation of create method - must be overridden
	SoftBody* SphSoftBodyFactory5::createInstanceImpl(
		const SoftBodyParams& params, 
		const DataValuePairList* otherparams )
	{

		//// must have mesh parameter
		//MeshPtr pMesh;
		//if (params != 0)
		//{
		//	NameValuePairList::const_iterator ni = params->find("mesh");
		//	if (ni != params->end())
		//	{
		//		// Get mesh (load if required)
		//		pMesh = MeshManager::getSingleton().load(
		//			ni->second,
		//			// autodetect group location
		//			ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
		//	}

		//}
		//if (pMesh.isNull())
		//{
		//	PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
		//		"'mesh' parameter required when constructing an Entity.",
		//		"SoftBodyFactory::createInstance");
		//}

		return new SphSoftBody5( params, otherparams );
	}


	void SphSoftBodyFactory5::destroyInstance(SoftBody* softbody)
	{
		delete softbody;
	}



	//String SphSoftBodyFactory6::FACTORY_TYPE_NAME = "SphSoftBody6";


	/// Get the type of the object to be created
	const String& SphSoftBodyFactory6::getType(void) const
	{
		return FACTORY_TYPE_NAME;
	}

	/// Internal implementation of create method - must be overridden
	SoftBody* SphSoftBodyFactory6::createInstanceImpl(
		const SoftBodyParams& params, 
		const DataValuePairList* otherparams )
	{

		//// must have mesh parameter
		//MeshPtr pMesh;
		//if (params != 0)
		//{
		//	NameValuePairList::const_iterator ni = params->find("mesh");
		//	if (ni != params->end())
		//	{
		//		// Get mesh (load if required)
		//		pMesh = MeshManager::getSingleton().load(
		//			ni->second,
		//			// autodetect group location
		//			ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
		//	}

		//}
		//if (pMesh.isNull())
		//{
		//	PHYSICS_EXCEPT(Exception::ERR_INVALIDPARAMS,
		//		"'mesh' parameter required when constructing an Entity.",
		//		"SoftBodyFactory::createInstance");
		//}

		return new SphSoftBody6( params, otherparams );
	}


	void SphSoftBodyFactory6::destroyInstance(SoftBody* softbody)
	{
		delete softbody;
	}
	


	;
	/// Get the type of the object to be created
	const String& SphSoftBodyFactory7::getType(void) const
	{
		return FACTORY_TYPE_NAME;
	}

	/// Internal implementation of create method - must be overridden
	SoftBody* SphSoftBodyFactory7::createInstanceImpl(
		const SoftBodyParams& params, 
		const DataValuePairList* otherparams )
	{

		return new SphSoftBody7( params, otherparams );
	}


	void SphSoftBodyFactory7::destroyInstance(SoftBody* softbody)
	{
		delete softbody;
	}


}; // namespace SimStep