#ifndef SimStep_SphNeighbors_h__
#define SimStep_SphNeighbors_h__



namespace SimStep
{


	struct NeighborsDistance 
	{
		int idx;
		float dist;
	};

	bool operator < (const NeighborsDistance& n1, const NeighborsDistance& n2 );
	//{
	//	return ( n1.dist < n2.dist );
	//}

}; // namespace SimStep

#endif // SimStep_SphNeighbors_h__