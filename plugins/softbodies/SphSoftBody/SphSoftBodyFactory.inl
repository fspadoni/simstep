#include "SphSoftBodyFactory.h"

namespace SimStep
{

	template<unsigned int NB_NEIGHBORS>
	String factoryName(const char* name)
	{
		std::stringstream ret;
		ret << name << NB_NEIGHBORS;
		return ret.str();
	}



	template<unsigned int NB_NEIGHBORS>
	SoftBody* SphSoftBodyFactory<NB_NEIGHBORS>::createInstanceImpl(
		const SoftBodyParams& params, 
		const DataValuePairList* otherparams = 0 )
	{
		return new SphSoftBody<NB_NEIGHBORS>( params, otherparams );
	}


	template<unsigned int NB_NEIGHBORS>
	const String& SphSoftBodyFactory<NB_NEIGHBORS>::getType(void) const
	{
		return FACTORY_TYPE_NAME;
	}


	template<unsigned int NB_NEIGHBORS>
	void SphSoftBodyFactory<NB_NEIGHBORS>::destroyInstance(SoftBody* softbody)
	{
		delete softbody;
	}


	
}; // namespace SimStep