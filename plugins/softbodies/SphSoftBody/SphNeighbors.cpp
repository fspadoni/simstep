#include <SphSoftBody/SphNeighbors.h>


namespace SimStep
{


	bool operator < (const NeighborsDistance& n1, const NeighborsDistance& n2 )
	{
		return ( n1.dist < n2.dist );
	}


}; // namespace SimStep