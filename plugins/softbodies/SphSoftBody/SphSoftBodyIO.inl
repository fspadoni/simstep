#include "SphSoftBody/SphSoftBody.h"


namespace SimStep
{


	template<unsigned int NB_NEIGHBORS>
	Bool SphSoftBody<NB_NEIGHBORS>::loadPhyxels(char *filename)
	{
		std::ifstream fin(filename,std::ios::in);
		if(!fin)
			return false;

		int dummy1,dummy2,dummy3;

		fin >> m_Nb_Nodes >> dummy1 >> dummy2 >> dummy3;

		m_Nodes = new SimdVector3[m_Nb_Nodes];

		float n,x,y,z;
		for(int i=0; i<m_Nb_Nodes; ++i)
		{
			fin >> n >> x >> y >> z;
			m_Nodes[i] = SimdVector3(x,y,z);
		}

		fin.close();
		return true;
	}


	template<unsigned int NB_NEIGHBORS>
	Bool SphSoftBody<NB_NEIGHBORS>::loadTriangles(char *filename)
	{
		std::ifstream fin(filename,std::ios::in);
		if(!fin)
			return false;

		int dummy;

		fin >> m_Nb_Triangles >> dummy;
		m_Triangles = new TriangleElement[m_Nb_Triangles];


		int n,i0,i1,i2;
		for(int i=0; i<m_Nb_Triangles; ++i)
		{
			fin >> n >> i0 >> i1 >> i2;
			m_Triangles[i].n[0] = i0;
			m_Triangles[i].n[1] = i1;
			m_Triangles[i].n[2] = i2;
		}

		fin.close();
		return true;
	}


	template<unsigned int NB_NEIGHBORS>
	Bool SphSoftBody<NB_NEIGHBORS>::loadCollisionNodes(char *filename)
	{
		std::ifstream fin(filename,std::ios::in);
		if(!fin)
			return false;

		int Nb_CollisionNodes;

		//fin.seekg('\n'  , std::ios_base::cur );
		fin.ignore( 1024,'\n');
		fin   >> Nb_CollisionNodes;

		m_Nb_CollisionNodes = Nb_CollisionNodes;

		//char buf[128];

		//while( fin >> buf )
		//{
		//	//if(strcmp(buf,"\n")==0)
		//	{
		//		fin >> m_Nb_CollisionNodes;
		//		break;
		//	}
		//}

		return true;
	}


	template<unsigned int NB_NEIGHBORS>
	Bool SphSoftBody<NB_NEIGHBORS>::loadConstraints(char *filename)
	{
		std::ifstream fin(filename,std::ios::in);
		if(!fin)
		{
			m_Nb_ConstraintNodes = 0;
			return false;
		}

		fin >> m_Nb_ConstraintNodes;
		m_ConstraintNodes = new ConstraintNode[ m_Nb_ConstraintNodes];

		int n;
		for(int i=0; i<m_Nb_ConstraintNodes; ++i)
		{
			fin >> n;
			m_ConstraintNodes[i].m_value = SimdVector3(0);
			m_ConstraintNodes[i].index = n;
		}

		fin.close();
		return true;
	}

	
	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::printMatrix( SimdMatrix3** mat, const char* file_name )
	{


		SSE::Matrix3* PhyxelRow;

		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");


		for (int Index=0; Index< m_Nb_Nodes; Index++ )
		{
			PhyxelRow =  m_StiffnessMatrix[Index];
			std::vector<unsigned short int>& Map2Phyxels = MapStiffnessMatrix2Phyxels[Index];


			int n = 0;
			for (int j=0; j<m_Nb_Nodes; j++ )
			{

				if ( j == Map2Phyxels[n] )
				{
					tmp.v = PhyxelRow[n].getRow(0).get128();				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
					if ( n < Map2Phyxels.size()-1 )
						n++ ;
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}

			n = 0;
			fprintf(pFile,"\n");
			for (int j=0; j<m_Nb_Nodes; j++ )
			{
				if ( j == Map2Phyxels[n] )
				{
					tmp.v = PhyxelRow[n].getRow(1).get128();				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
					if ( n < Map2Phyxels.size()-1 )
						n++ ;
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}

			n = 0;
			fprintf(pFile,"\n");
			for (int j=0; j<m_Nb_Nodes; j++ )
			{
				if ( j == Map2Phyxels[n] )
				{
					tmp.v = PhyxelRow[n].getRow(2).get128();				
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
					if ( n < Map2Phyxels.size()-1 )
						n++ ;
					else
						continue;
				}
				else
				{
					fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", 0.0f, 0.0f, 0.0f );
				}
			}
			fprintf(pFile,"\n");
		}

		fclose(pFile);
	}

	template<unsigned int NB_NEIGHBORS>
	void SphSoftBody<NB_NEIGHBORS>::printVector( SimdVector3* vec,
		const char* file_name )
	{

		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");


		for (int Index=0; Index<m_Nb_Nodes; Index++ )
		{

			tmp.v = vec[Index].get128();
			fprintf(pFile, " %5.9f \n %5.9f \n %5.9f \n", tmp.s[0], tmp.s[1], tmp.s[2] );
		}

		fclose(pFile);
	}

}; // namespace SimStep