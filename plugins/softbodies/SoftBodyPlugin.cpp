#include "SoftBodyPlugin.h"

#include "Core/Core.h"

#include "FemSoftBody/FemSoftBodyFactory.h"

#include "SphSoftBody/SphSoftBodyFactory.h"

namespace SimStep
{

	const String sPluginName = "SoftBody";


	//---------------------------------------------------------------------
	SoftBodyPlugin::SoftBodyPlugin()
		: mFemSoftBodyFactory(0)
		, mSphSoftBodyFactory5(0)
		, mSphSoftBodyFactory6(0)
		, mSphSoftBodyFactory7(0)
	{

	}
	//---------------------------------------------------------------------
	const String& SoftBodyPlugin::getName() const
	{
		return sPluginName;
	}
	//---------------------------------------------------------------------
	void SoftBodyPlugin::install()
	{
		
		mFemSoftBodyFactory = new FemSoftBodyFactory();
		mSphSoftBodyFactory5 = new SphSoftBodyFactory5();
		mSphSoftBodyFactory6 = new SphSoftBodyFactory6();
		mSphSoftBodyFactory7 = new SphSoftBodyFactory7();


	}

	//---------------------------------------------------------------------
	void SoftBodyPlugin::initialise()
	{
		// Register factories
		Core::getSingleton().addSoftBodyFactory( mFemSoftBodyFactory );
		Core::getSingleton().addSoftBodyFactory( mSphSoftBodyFactory5 );
		Core::getSingleton().addSoftBodyFactory( mSphSoftBodyFactory6 );
		Core::getSingleton().addSoftBodyFactory( mSphSoftBodyFactory7 );

	}

	//---------------------------------------------------------------------
	void SoftBodyPlugin::release()
	{
		// UnRegister factories
		Core::getSingleton().removeSoftBodyFactory( mFemSoftBodyFactory );
		Core::getSingleton().removeSoftBodyFactory( mSphSoftBodyFactory5 );
		Core::getSingleton().removeSoftBodyFactory( mSphSoftBodyFactory6 );
		Core::getSingleton().removeSoftBodyFactory( mSphSoftBodyFactory7 );
	}

	//---------------------------------------------------------------------
	void SoftBodyPlugin::uninstall()
	{

		delete mFemSoftBodyFactory;
		delete mSphSoftBodyFactory5;
		delete mSphSoftBodyFactory6;
		delete mSphSoftBodyFactory7;
	}

	
}; // namespace SimStep