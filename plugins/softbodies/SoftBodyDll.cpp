#include "SoftBodyPlugin.h"
#include "SoftBodyDefinition.h"

#include "Core/Core.h"


namespace SimStep
{

	SoftBodyPlugin* pSoftBodyPlugin;


	extern "C" void _SoftBodyExport dllStartPlugin( void )
	{
		// Create new SoftBody plugin
		pSoftBodyPlugin = new SoftBodyPlugin();

		// Register
		Core::getSingleton().installPlugin(pSoftBodyPlugin);

	}
	extern "C" void _SoftBodyExport dllStopPlugin( void )
	{
		Core::getSingleton().uninstallPlugin(pSoftBodyPlugin);
		delete pSoftBodyPlugin;
	}



}; // namespace SimStep