#ifndef SimStep_SoftBodyPlugin_h__
#define SimStep_SoftBodyPlugin_h__

#include "Core/PlugIn.h"

namespace SimStep
{

	class FemSoftBodyFactory;
	class SphSoftBodyFactory5;
	class SphSoftBodyFactory6;
	class SphSoftBodyFactory7;

	
	class SoftBodyPlugin : public Plugin // , public Core
	{
	public:

		SoftBodyPlugin();


		/// @copydoc Plugin::getName
		const String& getName() const;

		/// @copydoc Plugin::install
		void install();

		/// @copydoc Plugin::initialise
		void initialise();

		/// @copydoc Plugin::shutdown
		void release();

		/// @copydoc Plugin::uninstall
		void uninstall();


	private:


		FemSoftBodyFactory* mFemSoftBodyFactory;
		SphSoftBodyFactory5* mSphSoftBodyFactory5;
		SphSoftBodyFactory6* mSphSoftBodyFactory6;
		SphSoftBodyFactory7* mSphSoftBodyFactory7;


	};

	
}; // namespace SimStep

#endif // SimStep_SoftBodyPlugin_h__