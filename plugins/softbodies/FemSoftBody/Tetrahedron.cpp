#include "FemSoftBody/Tetrahedron.h"

#include "FemSoftBody/Decomposition.h"

namespace SimStep
{

	// Polar Decomposition
	const Matrix3 Tetrahedron::extractRotation(void) const
	{
		Matrix3 rotation;

		rotation.setRow(0,n2-n1);
		rotation.setRow(1,n3-n1);
		rotation.setRow(2,n4-n1);


		return PolarDecomposeRotationPart( rotation );
		//return PolarDecomposeRotationPart( rotation.setRow(0, edge1).setRow(1, edge2).setRow(2, edge3) );
	}

	
}; // namespace SimStep