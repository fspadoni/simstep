#ifndef SimStep_FemElementStiffnessMatrix_h__
#define SimStep_FemElementStiffnessMatrix_h__


//#include "Utilities/AlignedObject.h"
#include "Math/Math.h"



namespace SimStep {


	class ElasticityMatrix;
	class StrainMatrix;


	align16_class ElementStiffnessMatrix
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( ElementStiffnessMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		SSE::Matrix3 m[4][4];


		void computeMatrix(const ElasticityMatrix& D, const StrainMatrix& B, const float& volume );


		void print(const char* file_name );


	};

}; // namespace SimStep


#endif // SimStep_FemElementStiffnessMatrix_h__