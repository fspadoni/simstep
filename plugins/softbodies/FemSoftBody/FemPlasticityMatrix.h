#ifndef SimStep_FemPlasticityMatrix_h__
#define SimStep_FemPlasticityMatrix_h__

#include <Math/Math.h>

namespace SimStep
{

	class ElasticityMatrix;
	class StrainMatrix;

	align16_class PlasticityMatrix
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( PlasticityMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		SimdMatrix3 m[4][2];


		void computeMatrix(const ElasticityMatrix& D, const StrainMatrix& B, const float& volume );


		void print(const char* file_name );

	};

	
}; // namespace SimStep

#endif // SimStep_FemPlasticityMatrix_h__