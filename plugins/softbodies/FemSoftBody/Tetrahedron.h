#ifndef SimStep_Tetrahedron_h__
#define SimStep_Tetrahedron_h__


#include "Utilities/Definitions.h"

#include "Math/Math.h"


namespace SimStep
{

	struct TetrahedronElement
	{
		
		union
		{
			struct  
			{
				unsigned short n1,n2,n3,n4;
			};

			struct 
			{
				unsigned short n[4];
			};
		};

	};


	align16_struct Tetrahedron
	{

		union
		{
			struct  
			{
				Vector3 n[4];
			};

			struct 
			{
				Vector3 n1;
				Vector3 n2;
				Vector3 n3;
				Vector3 n4;
			};		

		};


	public:

		inline Tetrahedron(void);

		inline Tetrahedron(const Vector3& n1, const Vector3& n2, const Vector3& n3, const Vector3& n4 );


		//inline Tetrahedron::Tetrahedron(const SimStep::SoftBody::SimdVector3& n1, 
		//	const SimStep::SoftBody::SimdVector3& n2,
		//	const SimStep::SoftBody::SimdVector3& n3, 
		//	const SimStep::SoftBody::SimdVector3& n4 );


		inline const float computeVolume(void) const;

		inline const Matrix3 computeRotation(void) const;

		const Matrix3 extractRotation(void) const;

		inline const SimdMatrix4 getP(void) const; 

		inline const Matrix4 getInvP(void) const;

	};


	// ______________________________________________________________________
	// Tetra inline

	inline Tetrahedron::Tetrahedron() : n1(0.0f), n2(0.0f), n3(0.0f), n4(0.0f)
	{}


	inline Tetrahedron::Tetrahedron( const Vector3& n1, const Vector3& n2, 
		const Vector3& n3, const Vector3& n4 )
		: n1( n1 )
		, n2( n2 )
		, n3( n3 )
		, n4( n4 )
	{

	}



	inline const float Tetrahedron::computeVolume(void) const
	{
		return ::fabsf( dot( cross( n2 - n1, n3 - n1 ), n4 - n1) ) / 6.0f;
	}


	inline const Matrix3 Tetrahedron::computeRotation(void) const
	{

		// first vector on first edge
		// second vector in the plane of the two first edges
		// third vector orthogonal to first and second	

		// QR decomposition 
		Matrix3 rotation;

		Vector3 edge1 = normalize( n2-n1 );
		Vector3 edge2 = normalize( n3-n1 );
		Vector3 edge3 = normalize( cross( edge1, edge2 ) );
		edge2 = normalize( cross( edge3, edge1 ) );

		return rotation.setRow(0, edge1).setRow(1, edge2).setRow(2, edge3);
		return transpose( Matrix3( edge1, edge2, edge3 ) );
	}


	inline const SimdMatrix4 Tetrahedron::getP(void) const
	{
		return  SimdMatrix4(
			SimdVector4(n1, 1.0f),
			SimdVector4(n2, 1.0f),
			SimdVector4(n3, 1.0f),
			SimdVector4(n4, 1.0f) );
	}


	inline const Matrix4 Tetrahedron::getInvP() const
	{

		return inverse( Matrix4(
			Vector4(n1, 1.0f),
			Vector4(n2, 1.0f),
			Vector4(n3, 1.0f),
			Vector4(n4, 1.0f) ) );

	}

	
}; // namespace SimStep

#endif // SimStep_Tetrahedron_h__