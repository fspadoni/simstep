#ifndef SimStep_Decompose_h__
#define SimStep_Decompose_h__


#include "Utilities/Definitions.h"
#include "Math/Math.h"

namespace SimStep
{

	float PolarDecompose(const Matrix3& A,
		Matrix3* Rotation,
		Matrix3* Stretch );


	Matrix3 PolarDecomposeRotationPart(const Matrix3& M );

	Matrix3 PolarDecomposeRotationPartOptimize(const Matrix3& M );

	inline float norm_one(const Matrix3& M);
	inline float norm_inf(const Matrix3& M);
	//
	inline Matrix3 adjoint_transpose(const Matrix3& M);


	inline static Matrix3 ExtractRotationFast( const Matrix3& M )
	{
		Vector3 r0, r1, r2;

		r0 = normalize( M.getCol0() );
		r1 = normalize( M.getCol1() );
		r2 = cross( r0, r1 );
		r1 = cross( r2, r0 );
		r1 = normalize( r1 );

		return Matrix3( r0, r1, r2 );

	}

	
}; // namespace SimStep

#endif // SimStep_Decompose_h__