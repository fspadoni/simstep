#include "FemStrainMatrix.h"

#include <Math/Vector12.h>


namespace SimStep 
{


	//float Strain::computeNorm(void) const
	//{
	//	return sqrtf( ( lengthSqr( v[0] ) + lengthSqr( v[1]) ) );
	//}


	void StrainMatrix::computeMatrix(const Vector12& vec12)
	{

		SSE::Matrix3 Mat3x3;

		memset((void*)&m, 0, 8*sizeof(SimdMatrix3) );


		float det;

		// Zienckiewicz vol.1

		// first node
		SSE::Vector3 vecX = SSE::Vector3(vec12._2.getX(), vec12._3.getX(), vec12._4.getX() );
		SSE::Vector3 vecY = SSE::Vector3(vec12._2.getY(), vec12._3.getY(), vec12._4.getY() );
		SSE::Vector3 vecZ = SSE::Vector3(vec12._2.getZ(), vec12._3.getZ(), vec12._4.getZ() );
		SSE::Vector3 vec1 = SSE::Vector3(1.0f);

		Mat3x3.setCol0( vecY );
		Mat3x3.setCol1( vecZ );
		Mat3x3.setCol2( vec1 );
		det = determinant( Mat3x3 );

		m[0][0].setElem(0,0, -det );
		m[1][0].setElem(1,0, -det );
		m[1][0].setElem(2,2, -det );

		Mat3x3.setCol0( vecX );
		det = determinant( Mat3x3 );
		m[0][0].setElem(1,1, det );
		m[1][0].setElem(0,0, det );
		m[1][0].setElem(2,1, det );

		Mat3x3.setCol1( vecY  );
		det = determinant( Mat3x3 );
		m[0][0].setElem(2,2, -det);
		m[1][0].setElem(0,2, -det);
		m[1][0].setElem(1,1, -det);


		// second node
		vecX = SSE::Vector3(vec12._3.getX(), vec12._4.getX(), vec12._1.getX() );
		vecY = SSE::Vector3(vec12._3.getY(), vec12._4.getY(), vec12._1.getY() );
		vecZ = SSE::Vector3(vec12._3.getZ(), vec12._4.getZ(), vec12._1.getZ() );

		Mat3x3.setCol0( vecY );
		Mat3x3.setCol1( vecZ );
		det = determinant( Mat3x3 );
		m[0][1].setElem(0,0, det );
		m[1][1].setElem(1,0, det );
		m[1][1].setElem(2,2, det );

		Mat3x3.setCol0( vecX );
		det =  determinant( Mat3x3 );
		m[0][1].setElem(1,1, -det );
		m[1][1].setElem(0,0, -det );
		m[1][1].setElem(2,1, -det );

		Mat3x3.setCol1( vecY  );
		det =  determinant( Mat3x3 );
		m[0][1].setElem(2,2, det);
		m[1][1].setElem(0,2, det);
		m[1][1].setElem(1,1, det);


		// third node
		vecX = SSE::Vector3(vec12._4.getX(), vec12._1.getX(), vec12._2.getX() );
		vecY = SSE::Vector3(vec12._4.getY(), vec12._1.getY(), vec12._2.getY() );
		vecZ = SSE::Vector3(vec12._4.getZ(), vec12._1.getZ(), vec12._2.getZ() );

		Mat3x3.setCol0( vecY );
		Mat3x3.setCol1( vecZ );
		det = determinant( Mat3x3 );

		m[0][2].setElem(0,0, -det );
		m[1][2].setElem(1,0, -det );
		m[1][2].setElem(2,2, -det );

		Mat3x3.setCol0( vecX );
		det = determinant( Mat3x3 );
		m[0][2].setElem(1,1, det );
		m[1][2].setElem(0,0, det );
		m[1][2].setElem(2,1, det );

		Mat3x3.setCol1( vecY  );
		det = determinant( Mat3x3 );
		m[0][2].setElem(2,2, -det);
		m[1][2].setElem(0,2, -det);
		m[1][2].setElem(1,1, -det);


		// fourth node
		vecX = SSE::Vector3(vec12._1.getX(), vec12._2.getX(), vec12._3.getX() );
		vecY = SSE::Vector3(vec12._1.getY(), vec12._2.getY(), vec12._3.getY() );
		vecZ = SSE::Vector3(vec12._1.getZ(), vec12._2.getZ(), vec12._3.getZ() );

		Mat3x3.setCol0( vecY );
		Mat3x3.setCol1( vecZ );
		det = determinant( Mat3x3 );
		m[0][3].setElem(0,0, det );
		m[1][3].setElem(1,0, det );
		m[1][3].setElem(2,2, det );

		Mat3x3.setCol0( vecX );
		det =  determinant( Mat3x3 );
		m[0][3].setElem(1,1, -det );
		m[1][3].setElem(0,0, -det );
		m[1][3].setElem(2,1, -det );

		Mat3x3.setCol1( vecY  );
		det =  determinant( Mat3x3 );
		m[0][3].setElem(2,2, det);
		m[1][3].setElem(0,2, det);
		m[1][3].setElem(1,1, det);


		float volume_x6;

		volume_x6 = 1.0f / fabsf( dot( cross( vec12._2 - vec12._1, vec12._3 - vec12._1 ), vec12._4 - vec12._1) );

		m[0][0] *= volume_x6;
		m[0][1] *= volume_x6;
		m[0][2] *= volume_x6;
		m[0][3] *= volume_x6;
		m[1][0] *= volume_x6;
		m[1][1] *= volume_x6;
		m[1][2] *= volume_x6;
		m[1][3] *= volume_x6;

	}



	void StrainMatrix::print(const char* file_name )
	{
		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");

		for (int i=0; i<2; i++ )
		{

			for (int j=0; j<4; j++ )
			{
				tmp.v = m[i][j].getRow(0).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");


			for (int j=0; j<4; j++ )
			{
				tmp.v = m[i][j].getRow(1).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");


			for (int j=0; j<4; j++ )
			{
				tmp.v = m[i][j].getRow(2).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");

		}

		fclose(pFile);
	}


}; // namespace SimStep 