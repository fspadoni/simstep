#include "FemElementStiffnessMatrix.h"


#include "FemElasticityMatrix.h"
#include "FemStrainMatrix.h"


namespace SimStep {


	void ElementStiffnessMatrix::computeMatrix(const ElasticityMatrix& D, const StrainMatrix& B, const float& volume)
	{

		SimdMatrix3 temp1, temp2;

		for (int i=0; i<4; i++)
		{
			// temp = D*B
			temp1 =  D.m[0][0] * B.m[0][i] /*+  D.m[0][1] * B.m[1][i]*/;
			temp2 =  /*D.m[1][0] * B.m[0][i] +*/  D.m[1][1] * B.m[1][i];

			// Ke = B'* (D*B)
			for (int j=0; j<4; j++)
			{
				this->m[j][i] =  volume *
					( transpose( B.m[0][j]) * temp1  + transpose( B.m[1][j]) * temp2 );
			}
		}

	}



	void ElementStiffnessMatrix::print(const char* file_name )
	{
		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");

		for (int i=0; i<4; i++ )
		{

			tmp.v = m[i][0].getRow(0).get128();
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(0).get128();			
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][2].getRow(0).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][3].getRow(0).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			fprintf(pFile,"\n");


			tmp.v = m[i][0].getRow(1).get128();
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(1).get128();			
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][2].getRow(1).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][3].getRow(1).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			fprintf(pFile,"\n");


			tmp.v = m[i][0].getRow(2).get128();
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(2).get128();			
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][2].getRow(2).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			tmp.v = m[i][3].getRow(2).get128();				
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			fprintf(pFile,"\n");
		}

		fclose(pFile);
	}


}; // namespace SimStep