#ifndef SimStep_FemElasticityMatrix_h__
#define SimStep_FemElasticityMatrix_h__



//#include "Utilities/AlignedObject.h"
#include "Math/Math.h"


namespace SimStep {


align16_class ElasticityMatrix
{
public:

	SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( ElasticityMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

	
	SimdMatrix3 m[2][2];

	
	void ComputeMatrix(const float& YoungModulus, const float& PoissontRatio );


	void print(const char* file_name );

};


}; // namespace SimStep

#endif // SimStep_FemElasticityMatrix_h__