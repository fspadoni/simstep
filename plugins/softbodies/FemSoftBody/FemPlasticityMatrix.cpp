#include "FemPlasticityMatrix.h"

#include "FemElasticityMatrix.h"
#include "FemStrainMatrix.h"

namespace SimStep
{


	void PlasticityMatrix::computeMatrix(const ElasticityMatrix& D, const StrainMatrix& B, const float& volume)
	{

		//Matrix3 traspB1, traspB2;

		for (int i=0; i<4; ++i)
		{
			//traspB1 = transpose(B.m[0][i]);
			//traspB2 = transpose(B.m[1][i]);

			// Pe = v (B'* D)
			this->m[i][0] = volume*( transpose(B.m[0][i]) * D.m[0][0] /*+ transpose(B.m[1][i]) * D.m[1][0]*/ );
			this->m[i][1] = volume*( /*transpose(B.m[0][i]) * D.m[0][1] +*/ transpose(B.m[1][i]) * D.m[1][1] );

			//for (int j=0; j<2; ++j)
			//{
			//	this->m[i][j] =  volume * 
			//		( traspB1 * D.m[0][j] + traspB2 * D.m[1][j] );
			//}
		}

	}

	void PlasticityMatrix::print(const char* file_name )
	{
		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");

		for (int i=0; i<4; i++ )
		{

			for (int j=0; j<2; j++ )
			{
				tmp.v = m[i][j].getRow(0).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");


			for (int j=0; j<4; j++ )
			{
				tmp.v = m[i][j].getRow(1).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");


			for (int j=0; j<4; j++ )
			{
				tmp.v = m[i][j].getRow(2).get128();		
				fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );
			}

			fprintf(pFile,"\n");

		}

		fclose(pFile);
	}

	
}; // namespace SimStep