#ifndef SimStep_FemStrainMatrix_h__
#define SimStep_FemStrainMatrix_h__


//#include "Utilities/AlignedObject.h"
#include "Math/Math.h"


namespace SimStep {


	// forward decl:
	struct Vector12;

	align16_class Strain
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( Strain, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );

		SimdVector3 v[2];

		inline float computeNorm(void) const
		{
			return sqrtf( ( lengthSqr( v[0] ) + lengthSqr( v[1]) ) );
		}

	};


	align16_class StrainMatrix
	{
	public:

		SIMSTEP_DECLARE_CLASS_ALIGNED_ALLOCATOR( StrainMatrix, Memory::SIMD_ALIGN, MEMCATEGORY_MATH );


		SimdMatrix3 m[2][4];


		void computeMatrix(const Vector12& vec12);

		void print(const char* file_name );

	};



}; // namespace SimStep


#endif // SimStep_FemStrainMatrix_h__