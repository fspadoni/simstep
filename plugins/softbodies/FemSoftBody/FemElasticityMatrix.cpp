#include "FemSoftBody/FemElasticityMatrix.h"



namespace SimStep 
{


	void ElasticityMatrix::ComputeMatrix(const float& YoungModulus, const float& PoissontRatio )
	{

		memset((void*)&m, 0, 4*sizeof(SimdMatrix3) );

		// Zienckievicw vol.1
		float value = 1.0f - PoissontRatio;

		m[0][0].setElem( 0,0, value);
		m[0][0].setElem( 1,1, value);
		m[0][0].setElem( 2,2, value);

		value = PoissontRatio;
		m[0][0].setElem( 0,1, value);
		m[0][0].setElem( 0,2, value);
		m[0][0].setElem( 1,2, value);
		m[0][0].setElem( 1,0, value);
		m[0][0].setElem( 2,0, value);
		m[0][0].setElem( 2,1, value);

		value = (1 - 2.0f*PoissontRatio)/ 2.0f;
		m[1][1].setElem( 0,0, value);
		m[1][1].setElem( 1,1, value);
		m[1][1].setElem( 2,2, value);

		value = YoungModulus / ( (1.0f + PoissontRatio)*(1.0f - 2*PoissontRatio) )  ;

		m[0][0] *= value;
		m[0][1] *= value;
		m[1][0] *= value;
		m[1][1] *= value;
	}



	void ElasticityMatrix::print(const char* file_name )
	{
		union { __m128 v; float s[4]; } tmp;

		FILE* pFile;
		fopen_s(&pFile, file_name,"w");

		for (int i=0; i<2; i++ )
		{

			tmp.v = m[i][0].getRow(0).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(0).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			fprintf(pFile,"\n");

			tmp.v = m[i][0].getRow(1).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(1).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			fprintf(pFile,"\n");

			tmp.v = m[i][0].getRow(2).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );

			tmp.v = m[i][1].getRow(2).get128();		
			fprintf(pFile, " %5.9f \t %5.9f \t %5.9f \t ", tmp.s[0], tmp.s[1], tmp.s[2] );


			fprintf(pFile,"\n");
		}

		fclose(pFile);
	}


}; // namespace SimStep