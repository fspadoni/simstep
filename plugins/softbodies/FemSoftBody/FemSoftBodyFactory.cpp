#include "FemSoftBody/FemSoftBodyFactory.h"

#include "FemSoftBody/FemSoftBody.h"

//#include "Core/MeshManager.h"

#include "Utilities/PhysicsException.h"


// for Ogre::MeshPtr
//#include "OgreMesh.h"
// for Ogre::ResourceGroupManager
//#include "OgreResourceGroupManager.h"


namespace SimStep
{

	String FemSoftBodyFactory::FACTORY_TYPE_NAME = "FemSoftBody";


	/// Get the type of the object to be created
	const String& FemSoftBodyFactory::getType(void) const
	{
		return FACTORY_TYPE_NAME;
	}

	/// Internal implementation of create method - must be overridden
	SoftBody* FemSoftBodyFactory::createInstanceImpl(
		const SoftBodyParams& params, 
		const DataValuePairList* otherparams )
	{

		//// must have mesh parameter
		//Ogre::MeshPtr pMesh;
		//if (params.MeshFile != "" )
		//{
		//	//DataValuePairList::const_iterator ni = params->find("mesh");
		//	//if (ni != params->end())
		//	{
		//		// Get mesh (load if required)
		//		pMesh = TetraMeshManager::getSingleton().load(
		//			params.MeshFile,
		//			// autodetect group location
		//			Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME );
		//	}

		//}
		//if (pMesh.isNull())
		//{
		//	PHYSICS_EXCEPT( Exception::ERR_INVALIDPARAMS,
		//		"'mesh' parameter required when constructing a FemSoftBody.",
		//		"FemSoftBody::createInstance");
		//}

		////// delegate to factory implementation
		////otherparams["mesh3d"] = pMesh;

		//params.Mesh3d = pMesh.getPointer();
		return new FemSoftBody( params, otherparams );
	}


	void FemSoftBodyFactory::destroyInstance(SoftBody* softbody)
	{
		delete softbody;
	}

	
}; // namespace SimStep