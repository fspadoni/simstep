#include "FemSoftBody.h"

//#include "FemSoftBody/Tetrahedron.h"

#include "Utilities/StopWatch.h"

#include "Math/Vector12.h"

#include "Utilities/Definitions.h"

#include <algorithm>


// for getType FemSoftBodyFactory::FACTORY_TYPE_NAME
#include "FemSoftBody/FemSoftBodyFactory.h"

// da togliere perch� da la dipendenza da ogre
#include <World/SoftBodyParam.h>
#include <Core/SimdMemoryBufferManager.h>
#include <Core/SubTetraMesh.h>

// da togliere da dipendenza da collision softbody
// collision
#include "Collision/btSoftBodyInternals.h"

namespace SimStep
{


	FemSoftBody::FemSoftBody(const SoftBodyParams& params, const DataValuePairList* otherparams )
		:	SoftBody( params, otherparams)
		//, mIsPlastic( (params.Yield>0.0f) || (params.MaxYield>0.0f) )
	{

		create( params );
	}



	FemSoftBody::~FemSoftBody()
	{
		
	}


	// Public Interface 
	const String& FemSoftBody::getType(void) const
	{
		return FemSoftBodyFactory::FACTORY_TYPE_NAME;
	}

	float FemSoftBody::assembleStiffnessMatrix(void* user_pointer )
	{

		tick_count starTicks = tick_count::now();

		float ret = 0.0f;
		
		// Reset Stiffness Matrix and Load Vector
		mForces->reset();
		mStiffnessMatrix->reset();

		TetraElement* elem;

		SimdMatrix3 tet;
		SimdMatrix3 Rot;
		SimdMatrix3 invRot;

		Quat q;

		//SimdMatrix3 rotQ, rotQR;

		const unsigned short* tetrabuf = mElements->lockForRead<unsigned short>();

		const Vector3* nodebuf = mNodes->lockForRead<Vector3>();

		const SimdMatrix4* undeformedbuf = mUndeformedStateVector->lockForRead<SimdMatrix4>();

		const Ke* mapbuf = mKe2StiffnessMatrixMap->lockForRead<Ke>();
		
		const ElementStiffnessMatrix* elemstiffnessbuf = mKe->lockForRead<ElementStiffnessMatrix>();

	
		//const Vector12* kx0buf = mKX0->lockForRead<Vector12>();

		SimdMatrix3* stiffnessbuff = mStiffnessMatrix->lockForWrite<SimdMatrix3>();

		//SimdVector3* forcebuf = mForces->lockForWrite<SimdVector3>();

		SimdMatrix3* rotationbuf = mElemRotations->lockForWrite<SimdMatrix3>();

		//const StrainMatrix* strainsbuf;
		//const Vector12* x0buf;
		//const PlasticityMatrix* plastmatbuf;
		//Strain* totalStrainbuf;
		//Strain* elasticStrainbuf;
		//Strain* plasticStrainbuf;
		//SimdVector3* plasticforcesbuf;

		//if ( mIsPlastic )
		//{
		//	strainsbuf = mStrainMatrix->lockForRead();
		//	x0buf = mX0->lockForRead();
		//	plastmatbuf = mPe->lockForRead();
		//	totalStrainbuf = mTotalStrain->lockForWrite<Strain>();
		//	elasticStrainbuf = mElasticStrain->lockForWrite<Strain>();
		//	plasticStrainbuf = mPlasticStrain->lockForWrite<Strain>();
		//	plasticforcesbuf = mPlasticForce->lockForWrite<SimdVector3>();
		//	mPlasticForce->reset();
		//}

		for (int ElementIndex= 0; ElementIndex<mNbElements; ++ElementIndex )
		{


			const Vector3& n1 = nodebuf[*tetrabuf++];
			const Vector3& n2 = nodebuf[*tetrabuf++];
			const Vector3& n3 = nodebuf[*tetrabuf++];
			const Vector3& n4 = nodebuf[*tetrabuf++];

			tet = ( ( SimStep::Tetrahedron( n1, n2, n3, n4 ) ).getP() * 
				(*undeformedbuf) ).getUpper3x3();

			q = Quat( reinterpret_cast<const Matrix3&>(tet) );

			q = normalize(q);

			*rotationbuf = Rot = SimdMatrix3( reinterpret_cast<const SimdQuat&>(q) );

			//rotQ = SimStep::ExtractRotationFast(tet);
			//rotQ = SimStep::PolarDecomposeRotationPart(tet);

			invRot = transpose(Rot);

			// multiply Re * Ke * Re'

			//_mm_prefetch((CHAR CONST *) a, l)

			for (int i=0; i<4; ++i)
			{

				for (int j=0; j<4; ++j)
				{

					stiffnessbuff[ mapbuf->ij[i][j] ] += Rot * elemstiffnessbuf->m[i][j] *  invRot;
	
				}

				//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\Federico\\Documenti\\MATLAB\\A.txt");
			}

			//PrintMatrix(m_StiffnessMatrix, "C:\\Documents and Settings\\f.spadoni\\Documenti\\MATLAB\\A.txt");

			// _____________________________

			//forcebuf[tetrabuf->n1] -= Rot * kx0buf->_1; 
			//forcebuf[tetrabuf->n2] -= Rot * kx0buf->_2; 
			//forcebuf[tetrabuf->n3] -= Rot * kx0buf->_3; 
			//forcebuf[tetrabuf->n4] -= Rot * kx0buf->_4; 

			// plasticity

			
			//++kx0buf;
			++mapbuf;
			++elemstiffnessbuf;
			++undeformedbuf;
			//++tetrabuf;
			++rotationbuf;
		}


		mElemRotations->unlock();
		//mForces->unlock();
		mStiffnessMatrix->unlock();
		//mKX0->unlock();
		mKe->unlock();
		mKe2StiffnessMatrixMap->unlock();
		mUndeformedStateVector->unlock();
		mNodes->unlock();
		mElements->unlock();



		const SimdMatrix3* rotationbufconst = mElemRotations->lockForRead<SimdMatrix3>();
		const SimdVector3* kx0buf = mKX0->lockForRead<SimdVector3>();
		tetrabuf = mElements->lockForRead<unsigned short>();

		SimdVector3* forcebuf = mForces->lockForWrite<SimdVector3>();

		int N = mNbElements;
		while ( N-- > 0)
		{
			//const unsigned short& n1 = tetrabuf->n1;
			//const unsigned short& n2 = tetrabuf->n2;
			//const unsigned short& n3 = tetrabuf->n3;
			//const unsigned short& n4 = tetrabuf->n4;
	
			forcebuf[*tetrabuf++] -= (*rotationbufconst) * (*kx0buf++); 
			forcebuf[*tetrabuf++] -= (*rotationbufconst) * (*kx0buf++);
			forcebuf[*tetrabuf++] -= (*rotationbufconst) * (*kx0buf++); 
			forcebuf[*tetrabuf++] -= (*rotationbufconst) * (*kx0buf++);
			++rotationbufconst;
			//++kx0buf;
			//++tetrabuf;
		}
		mForces->unlock();
		mElements->unlock();
		mKX0->unlock();
		mElemRotations->unlock();
		

		if ( mIsPlastic )
		{
			computePlasticity( user_pointer );
		}

		

		//PrintStiffnessMatrix( "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\A.txt");

		//FemSoftBody::computeStressForces();


		return  ( tick_count::now() - starTicks ).seconds();
	}

	float FemSoftBody::solve( const float& timeStep, void* user_pointer )
	{
		tick_count starTicks = tick_count::now();


		ConjGradSolver();

		mExternForces->reset();

		return  ( tick_count::now() - starTicks ).seconds();
	}


	Vector3 FemSoftBody::getStressForce( const int& nodeId ) const
	{

		Vector3 SimdStressForce;

		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>(nodeId);
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>(nodeId);
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>(*indexbuf);
		const Vector3* stressforcebuf = 
			mStressForces->lockForRead<Vector3>();

		SimdStressForce = 
			coordbuf->l1 * stressforcebuf[tetrabuf->n1] +
			coordbuf->l2 * stressforcebuf[tetrabuf->n2] +
			coordbuf->l3 * stressforcebuf[tetrabuf->n3] +
			(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * stressforcebuf[tetrabuf->n4];

		mExternForces->unlock();
		mElements->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();

		return SimdStressForce;
	}

	const Buffer<Vector3>& FemSoftBody::getStressForces( void ) const
	{
		return *mStressForces;
	}

	float FemSoftBody::getVertexMass(int vertexId) const
	{
		float mass;

		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>(vertexId);
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>(vertexId);
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>(*indexbuf);
		const float* nodemassbuf = 
			mNodeMasses->lockForRead<float>();

		mass = 
			coordbuf->l1 * nodemassbuf[tetrabuf->n1] +
			coordbuf->l2 * nodemassbuf[tetrabuf->n2] +
			coordbuf->l3 * nodemassbuf[tetrabuf->n3] +
			(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * nodemassbuf[tetrabuf->n4];

		mNodeMasses->unlock();
		mElements->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();

		return mass;
	}

	//void FemSoftBody::getStressForces( void* stressforce ) const
	//{

	//}

	SoftBody* FemSoftBody::clone(void)
	{
		// to do
		return 0;
	}

	void FemSoftBody::updateCollisionShape(void)
	{

		updateVertices();
		updateVertexVels();
		updateVertexNormals();


		SoftBody::updateCollisionShape();
	}


	void FemSoftBody::setPosition(const Vector3& newpos )
	{
		SimdVector3 pivot(0.0f);
		const SimdVector3* cnodesbuf = mNodes->lockForRead<SimdVector3>();

		int nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodesbuf++;
		}
		mNodes->unlock();
		pivot /= mNbNodes;
		pivot = SimdVector3(newpos[0],newpos[1],newpos[2]) - pivot;

		SimdVector3* nodesbuf = mNodes->lockForWrite<SimdVector3>();
		nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			*nodesbuf++ += pivot;
		}
		mNodes->unlock();

		//updateVertices();

		//CollisionSoftBody::buildDynamicVertexTree();
	}


	void FemSoftBody::setOrientation(const Quat& newor )
	{
		Vector3 pivot(0.0f);
		const Vector3* cnodesbuf = mNodes->lockForRead<Vector3>();

		int nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			pivot += *cnodesbuf++;
		}
		mNodes->unlock();
		pivot /= mNbNodes;

		Quat orientation = normalize(newor);
		Quat q = slerp( 1.0f, mOrientation, orientation);
		mOrientation = orientation;

		Vector3* nodesbuf = mNodes->lockForWrite<Vector3>();
		nbnodes = mNbNodes;
		while ( nbnodes-- > 0 )
		{
			*nodesbuf = pivot + rotate( q, (*nodesbuf - pivot) );
			++nodesbuf;

		}
		mNodes->unlock();

		Vector3* normalsbuf = mVertexNormals->lockForWrite<Vector3>();
		int nbvertices = mVertexNormals->getSize();
		while ( nbvertices-- > 0 )
		{
			*normalsbuf = rotate( q, *normalsbuf );
			++normalsbuf;

		}
		mVertexNormals->unlock();

		//updateVertices();

		//CollisionSoftBody::buildDynamicVertexTree();
	}


	void FemSoftBody::reset(void)
	{

		mNodes->copyData( *mTetraMesh->mSharedNodes );

		mNodesVelocity->reset();
		mExternForces->reset();

		if ( mPosition[0] != Vector3::Zero()[0] ||
			mPosition[1] != Vector3::Zero()[1] ||
			mPosition[2] != Vector3::Zero()[2] )
		{
			FemSoftBody::setPosition( mPosition );
		}

		const Matrix3& id = Matrix3::identity();
		if ( mRotation[0][0] != id[0][0] ||
			mRotation[0][1] != id[0][1] ||
			mRotation[0][2] != id[0][2] ||
			mRotation[1][1] != id[1][1] ||
			mRotation[1][2] != id[1][2] || 
			mRotation[2][2] != id[2][2] )
		{
			mOrientation = Quat( mRotation );
			FemSoftBody::setOrientation( mOrientation );
		}

		if ( mIsPlastic )
		{
			mPlasticStrain->reset();
			mPlasticForce->reset();
		}
		
		updateVertices();

		//CollisionSoftBody::updateCollisionShape();

		CollisionSoftBody::reset();
	}


	void FemSoftBody::setVelocity(const Vector3& newvel )
	{
		mNodesVelocity->reset(newvel);
	}

	void FemSoftBody::addForce(const int& nodeId, const Vector3& force )
	{
		const ushort* tetraindexbuf = 
			mBarycentricIndex->lockForRead<ushort>(nodeId);
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>(nodeId);
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>(*tetraindexbuf);
		Vector3* extforcebuf = 
			mExternForces->lockForWrite<Vector3>();

		extforcebuf[tetrabuf->n1] += coordbuf->l1 * force;
		extforcebuf[tetrabuf->n2] += coordbuf->l2 * force;
		extforcebuf[tetrabuf->n3] += coordbuf->l3 * force;
		extforcebuf[tetrabuf->n4] +=
			(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * force;

		mExternForces->unlock();
		mElements->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
	}

	Vector3 FemSoftBody::getVertexNormal(int vertexId) const
	{
		Vector3 normal = *mNewVertexNormals->lockForRead<Vector3>(vertexId);
		mNewVertexNormals->unlock();
		return normal;
	}

	const BufferSharedPtr<Vector3>& FemSoftBody::getVertexNormalBuffer(void) const
	{
		return mNewVertexNormals;
	}


	void FemSoftBody::computeStressForces(void)
	{
		const ushort* rowsbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const SimdMatrix3* matbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();
		const SimdVector3* forcebuf = mForces->lockForRead<SimdVector3>();
		//const float* massbuf = mNodeMasses->lockForRead<float>();

		SimdVector3* stressforcebuf = mStressForces->lockForWrite<SimdVector3>();

		ushort nbcols;
		int nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			*stressforcebuf = - (*forcebuf);
			nbcols = *(rowsbuf+1) - *rowsbuf;

			while (nbcols-- > 0)
			{
				*stressforcebuf -= (*matbuf++) * nodebuf[(*colsbuf++)];
			}
			//*stressforcebuf *= (1.0f- m_Damping);

			// RHS = -K*Xi -F0  
			++stressforcebuf;
			++forcebuf;
			++rowsbuf;			
		}

		mStressForces->unlock();
		//mNodeMasses->unlock();
		mForces->unlock();
		mNodes->unlock();
		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

	}


	void FemSoftBody::create(const SoftBodyParams& params )
	{

		// Desc
		mPosition = params.mPosition;
		mRotation = params.mOrientation;
		m_TimeStep = params.TimeStep;
		m_TimeStep2 = params.TimeStep*params.TimeStep;
		//m_Density = params.Density;
		m_Density = static_cast<float>( mMaterial->getDensity() );
		//m_YoungModulus = params.YoungModulus;
		m_YoungModulus = static_cast<float>( mMaterial->getYoungModulus() );
		//m_PoissonRatio = params.PoissonRatio;
		m_PoissonRatio = static_cast<float>( mMaterial->getPoissonRatio() );
		//m_Damping = params.Damping;
		m_Damping = static_cast<float>( mMaterial->getFriction() );
		mYield = static_cast<float>( mMaterial->getYield() );
		mMaxYield = static_cast<float>( mMaterial->getMaxYield() );
		mCreepDt  =  static_cast<float>( m_TimeStep* mMaterial->getCreep() );
		mIsPlastic = ( (mYield>0.0f) || (mMaxYield>0.0f) );

		m_Gravity = params.Gravity;
		m_Nb_Solver_Iteration = params.Nb_Solver_Iteration;
		m_Solver_Tolerance = params.Solver_Tolerance;


		// TimeStepVector(dt, dt, dt )
		TimeStepVector = SimdVector3( m_TimeStep );

		// TimeStepMatrix	| dt, 1, 1 |
		//					| 1, dt, 1 |
		//					| 1, 1, dt |	

		TimeStepMatrix.setCol0( SimdVector3(m_TimeStep,1.0f,1.0f) );
		TimeStepMatrix.setCol1( SimdVector3(1.0f,m_TimeStep,1.0f) );
		TimeStepMatrix.setCol2(SimdVector3(1.0f,1.0f,m_TimeStep) );

		// TimeStep2Matrix	| dt*dt,  1   ,    1  |
		//					| 1    , dt*dt,    1  |
		//					| 1    ,  1   , dt*dt |	

		TimeStep2Matrix.setCol0( SimdVector3(m_TimeStep2,1.0f,1.0f) );
		TimeStep2Matrix.setCol1( SimdVector3(1.0f,m_TimeStep2,1.0f) );
		TimeStep2Matrix.setCol2( SimdVector3(1.0f,1.0f,m_TimeStep2) );


		m_DampingMatrix.setCol0( SimdVector3(m_Damping,    0.0f   ,  0.0f   ) );
		m_DampingMatrix.setCol1( SimdVector3(0.0f	   ,  m_Damping,  0.0f   ) );
		m_DampingMatrix.setCol2( SimdVector3(0.0f     ,	0.0f   ,m_Damping) );

		mMass = 0.0f;


		FemSoftBody::initializeBuffers();
		
		updateVertices();

		if ( params.mPosition[0] != Vector3::Zero()[0] ||
			params.mPosition[1] != Vector3::Zero()[1] ||
			params.mPosition[2] != Vector3::Zero()[2] )
		{
			FemSoftBody::setPosition( params.mPosition );
		}

		const Matrix3& id = Matrix3::identity();
		if ( params.mOrientation[0][0] != id[0][0] ||
			params.mOrientation[0][1] != id[0][1] ||
			params.mOrientation[0][2] != id[0][2] ||
			params.mOrientation[1][1] != id[1][1] ||
			params.mOrientation[1][2] != id[1][2] || 
			params.mOrientation[2][2] != id[2][2] )
		{
			mOrientation = Quat( params.mOrientation );
			FemSoftBody::setOrientation( mOrientation );
		}

		updateVertices();

		CollisionSoftBody::buildDynamicVertexTree();

		initializeElements();
		initializeStiffnessMap();

		return;
	}


	void FemSoftBody::updateVertices(void)
	{

		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>();
		const SimdVector3* nodebuf = 
			mNodes->lockForRead<SimdVector3>();
		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>();
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>();
		SimdVector3* vertexbuf = 
			mVertex->lockForWrite<SimdVector3>();
		
		for (int i=0; i<mNbVertices; ++i )
		{
			const TetrahedronElement& tetra = tetrabuf[*indexbuf++];
			const SimdVector3& n1 = nodebuf[tetra.n1];
			const SimdVector3& n2 = nodebuf[tetra.n2];
			const SimdVector3& n3 = nodebuf[tetra.n3];
			const SimdVector3& n4 = nodebuf[tetra.n4];
			
			//*vertexbuf++ = 
			//	coordbuf->l1 * n1 + 
			//	coordbuf->l2 * n2 + 
			//	coordbuf->l3 * n3 + 
			//	(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * n4;

			// all simds
			*vertexbuf++ = 
				coordbuf->l1 * ( n1 - n4 ) + 
				coordbuf->l2 * ( n2 - n4 ) +
				coordbuf->l3 * ( n3 - n4 ) + 
				n4;

			++coordbuf;
		}

		mVertex->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
		mNodes->unlock();
		mElements->unlock();
	}


	void FemSoftBody::updateVertexVels(void)
	{
		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>();
		const SimdVector3* nodevelbuf = 
			mNodesVelocity->lockForRead<SimdVector3>();
		const ushort* indexbuf = 
			mBarycentricIndex->lockForRead<ushort>();
		const BarycentricCoords* coordbuf = 
			mBarycentricCoords->lockForRead<BarycentricCoords>();
		SimdVector3* collnodevelbuf = 
			mVertexVels->lockForWrite<SimdVector3>();

		for (int i=0; i<mNbVertices; ++i )
		{
			const TetrahedronElement& tetra = tetrabuf[*indexbuf++];
			const SimdVector3& v1 = nodevelbuf[tetra.n1];
			const SimdVector3& v2 = nodevelbuf[tetra.n2];
			const SimdVector3& v3 = nodevelbuf[tetra.n3];
			const SimdVector3& v4 = nodevelbuf[tetra.n4];

			//*vertexbuf++ = 
			//	coordbuf->l1 * n1 + 
			//	coordbuf->l2 * n2 + 
			//	coordbuf->l3 * n3 + 
			//	(1.0f - coordbuf->l1 - coordbuf->l2 - coordbuf->l3) * n4;

			// all simds
			*collnodevelbuf++ = 
				coordbuf->l1 * ( v1 - v4 ) + 
				coordbuf->l2 * ( v2 - v4 ) +
				coordbuf->l3 * ( v3 - v4 ) + 
				v4;

			++coordbuf;
		}

		mVertexVels->unlock();
		mBarycentricCoords->unlock();
		mBarycentricIndex->unlock();
		mNodesVelocity->unlock();
		mElements->unlock();
	}

	void FemSoftBody::updateVertexNormals(void)
	{
		const SimdVector3* vNormalbuf = 
			mVertexNormals->lockForRead<SimdVector3>();
		
		const ushort* vindexbuf = 
			mBarycentricIndex->lockForRead<ushort>();
		
		const ushort* facebuf = 
			mFaces->lockForRead<ushort>();

		const SimdMatrix3* elembuf = 
			mElemRotations->lockForRead<SimdMatrix3>();

		SimdVector3* deformedNormalbuf = 
			mNewVertexNormals->lockForWrite<SimdVector3>();
		//mNewVertexNormals->copyData( *mVertexNormals );

		int N = mVertexNormals->getSize();

		while ( N-- > 0 )
		{
			*deformedNormalbuf++ = elembuf[vindexbuf[*facebuf++]] * (*vNormalbuf++);
		}

		mNewVertexNormals->unlock();
		mElemRotations->unlock();
		mFaces->unlock();
		mBarycentricIndex->unlock();
		mVertexNormals->unlock();

	}


	void FemSoftBody::initializeBuffers(void)
	{

		CollisionSoftBody::initializeBuffers();

		mNbNodes = mTetraMesh->mSharedNodes->getSize();
		mNodes = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNodes->copyData( *mTetraMesh->mSharedNodes );

		mNbElements = mTetraMesh->getSubTetraMesh(0)->mTetrahedrons->getSize()/4;
		mElements = BufferManager::getSingleton().createBuffer<TetrahedronElement>(mNbElements);
		mElements->copyData(  reinterpret_cast<Buffer<TetrahedronElement>&>(*mTetraMesh->getSubTetraMesh(0)->mTetrahedrons) );

		mElemRotations = 
			BufferManager::getSingleton().createBuffer<Matrix3>(mNbElements);;

		mNewVertexNormals = 
			BufferManager::getSingleton().createBuffer<Vector3>(3*mNbFaces);


		mNbTriangles = mTetraMesh->mFaces->getSize() /3;
		mTriangles  = BufferManager::getSingleton().createBuffer<ushort>(3*mNbTriangles);
		mTriangles->copyData( *mTetraMesh->mFaces );


		mNodesVelocity = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNodesVelocity->reset();


		//mNbVertices = mTetraMesh->getSubMesh(0)->mVertices.getSize(); 

		mBarycentricCoords = 
			BufferManager::getSingleton().createBuffer<BarycentricCoords>(mNbVertices);
		mBarycentricCoords->copyData( 
			reinterpret_cast<Buffer<BarycentricCoords>&>(*mTetraMesh->getSubMesh(0)->mVertices.barycentricCoords) );

		mBarycentricIndex = 
			BufferManager::getSingleton().createBuffer<ushort>(mNbVertices);
		mBarycentricIndex->copyData( *mTetraMesh->getSubMesh(0)->mVertices.tetraIndex );

		//mVertex = 
		//	BufferManager::getSingleton().createBuffer<Vector3>(mNbVertices);

		//mVertexVels = 
		//	BufferManager::getSingleton().createBuffer<Vector3>(mNbVertices);

		//mNbFaces = mTetraMesh->getSubMesh(0)->mFaces->getSize() /3;
		//mFaces  = BufferManager::getSingleton().createBuffer<TriangleElement>(mNbFaces);
		////mFaces  = BufferManager::getSingleton().createBuffer<ushort>(mNbTriangles);
		//mFaces->copyData( *mTetraMesh->getSubMesh(0)->mFaces );

		if ( !mTetraMesh->mSharedConstrainedNodes.isNull() )
		{
			mNbConstraintNodes = mTetraMesh->mSharedConstrainedNodes->getSize();
			if ( mNbConstraintNodes )
			{
				//mConstraintNodes = BufferManager::getSingleton().createBuffer<ConstraintNode>(mNbConstraintNodes);
				//mConstraintNodes->reset();
				mConstraintNodes = BufferManager::getSingleton().createBuffer<ushort>(mNbConstraintNodes);
				mConstraintNodes->copyData( *mTetraMesh->mSharedConstrainedNodes );
			}
		}

		


		mExternForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mExternForces->reset();

		mForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mForces->reset();

		mStressForces = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mStressForces->reset();

		mNodeMasses = BufferManager::getSingleton().createBuffer<float>(mNbNodes);
		mNodeMasses->reset();

		// femsoftody 
		mStrainMatrix  = BufferManager::getSingleton().createBuffer<StrainMatrix>(mNbElements);
		mStrainMatrix->reset();

		mKe = BufferManager::getSingleton().createBuffer<ElementStiffnessMatrix>(mNbElements);
		mKe->reset();

		mUndeformedStateVector = BufferManager::getSingleton().createBuffer<Matrix4>(mNbElements);
		mUndeformedStateVector->reset();

		mKX0 = BufferManager::getSingleton().createBuffer<Vector12>(mNbElements);
		mKX0->reset();


		// Plasticity Matrix
		if ( mIsPlastic )
		{
			mPe = BufferManager::getSingleton().createBuffer<PlasticityMatrix>(mNbElements);
			mPe->reset();

			mX0 = BufferManager::getSingleton().createBuffer<Vector12>(mNbElements);

			mTotalStrain = BufferManager::getSingleton().createBuffer<Strain>(mNbElements);
			mTotalStrain->reset();

			mElasticStrain = BufferManager::getSingleton().createBuffer<Strain>(mNbElements);
			mElasticStrain->reset();

			mPlasticStrain = BufferManager::getSingleton().createBuffer<Strain>(mNbElements);
			mPlasticStrain->reset();

			mPlasticForce = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
			mPlasticForce->reset();
		}
		

		//Graph
		size_t graphSize = mTetraMesh->mGraph.columns->getSize();
		mCloumns = 
			BufferManager::getSingleton().createBuffer<ushort>( graphSize );
		mCloumns->copyData( *mTetraMesh->mGraph.columns );

		mRows = 
			BufferManager::getSingleton().createBuffer<ushort>( mNbNodes + 1 );
		mRows->copyData( *mTetraMesh->mGraph.rows );

		mDiagonals = 
			BufferManager::getSingleton().createBuffer<ushort>( mNbNodes );
		mDiagonals->copyData( *mTetraMesh->mGraph.diagonals );

		mKe2StiffnessMatrixMap = 
			BufferManager::getSingleton().createBuffer<Ke>( mNbElements );

		mStiffnessMatrix = BufferManager::getSingleton().createBuffer<Matrix3>(graphSize);
		mStiffnessMatrix->reset(); 


		//    used by solver
		mRHSVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mRHSVector->reset(); 
		mNewNodeVelocity = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mNewNodeVelocity->reset(); 
		mResidualVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mResidualVector->reset(); 
		mDirectionVector = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mDirectionVector->reset(); 
		mTemp = BufferManager::getSingleton().createBuffer<Vector3>(mNbNodes);
		mTemp->reset(); 

		

	}


	void FemSoftBody::initializeElements(void)
	{

		float ElementVolume;
		float ElementMass;

		mMass = 0.0f;

		Tetrahedron T4;

		Vector12 X0;

		//TetraElement* elem;

		mD = BufferManager::getSingleton().createBuffer<ElasticityMatrix>();
		ElasticityMatrix* elasticitymatrix = mD->lockForWrite();
		elasticitymatrix->ComputeMatrix(m_YoungModulus, m_PoissonRatio);
		mD->unlock();

		//mTest = TBufferManager::getSingleton().createBuffer<ElasticityMatrix>();
		//elasticitymatrix = mTest->lockForWrite();
		//elasticitymatrix->ComputeMatrix(m_YoungModulus, m_PoissonRatio);
		//mTest->unlock();

		//m_D.ComputeMatrix(m_YoungModulus, m_PoissonRatio);

		const TetrahedronElement* tetrabuf = 
			mElements->lockForRead<TetrahedronElement>();

		const Vector3* nodebuf = 
			mNodes->lockForRead<Vector3>();

		Matrix4* undeformedbuf = mUndeformedStateVector->lockForWrite<Matrix4>();

		float* massbuf = mNodeMasses->lockForWrite<float>();

		StrainMatrix* strainbuf = mStrainMatrix->lockForWrite<StrainMatrix>();

		Vector12* x0buf;
		PlasticityMatrix* plasticitybuf;
		if ( mIsPlastic )
		{
			x0buf = mX0->lockForWrite();
			plasticitybuf =  mPe->lockForWrite<PlasticityMatrix>();
		}
		

		ElementStiffnessMatrix* elemstiffnessbuf = mKe->lockForWrite<ElementStiffnessMatrix>();

		Vector12* kx0buf = mKX0->lockForWrite<Vector12>();

		for (int ElementIndex=0; ElementIndex<mNbElements; ++ElementIndex)
		{
			//elem =  &m_TetraElements[ElementIndex];

			T4.n1 =  nodebuf[tetrabuf->n1];
			T4.n2 =  nodebuf[tetrabuf->n2];
			T4.n3 =  nodebuf[tetrabuf->n3];
			T4.n4 =  nodebuf[tetrabuf->n4];

			*undeformedbuf++ = T4.getInvP(); 


			ElementVolume = T4.computeVolume();

			ElementMass = ElementVolume * m_Density;

			mMass += ElementMass;

			ElementMass *= 0.25f;
			massbuf[tetrabuf->n1] += ElementMass;
			massbuf[tetrabuf->n2] += ElementMass;
			massbuf[tetrabuf->n3] += ElementMass;
			massbuf[tetrabuf->n4] += ElementMass;

			X0._1 = T4.n1;
			X0._2 = T4.n2;
			X0._3 = T4.n3;
			X0._4 = T4.n4;

			
			strainbuf->computeMatrix( X0 );

			elemstiffnessbuf->computeMatrix( *elasticitymatrix,  *strainbuf, ElementVolume);
			
			if ( mIsPlastic )
			{
				x0buf->v[0] = X0.v[0];
				x0buf->v[1] = X0.v[1];
				x0buf->v[2] = X0.v[2];
				x0buf->v[3] = X0.v[3];
				++x0buf;

				plasticitybuf->computeMatrix( *elasticitymatrix,  *strainbuf, ElementVolume );	
				
				//strainbuf->print("C:\\Documents and Settings\\Federico\\Documenti\\MATLAB\\strain.txt");
				//elasticitymatrix->print("C:\\Documents and Settings\\Federico\\Documenti\\MATLAB\\elasticity.txt");
				//plasticitybuf->print("C:\\Documents and Settings\\Federico\\Documenti\\MATLAB\\plasticity.txt");

				++plasticitybuf;
			}
			
			for (int i=0; i<4; i++ )
			{

				kx0buf->v[i] = elemstiffnessbuf->m[i][0] * X0._1 + 
					elemstiffnessbuf->m[i][1] * X0._2 + 
					elemstiffnessbuf->m[i][2] * X0._3 +
					elemstiffnessbuf->m[i][3] * X0._4;
			}

			//elemstiffnessbuf->print("C:\\Documents and Settings\\Federico\\Documenti\\MATLAB\\K.txt");

			++kx0buf;
			++strainbuf;
			//++x0buf;
			++elemstiffnessbuf;
			++tetrabuf;
			
		}

		mKX0->unlock();
		mKe->unlock();

		if ( mIsPlastic )
		{
			mPe->unlock();
			mX0->unlock();
			
		}
		mStrainMatrix->unlock();
		mNodeMasses->unlock();
		mUndeformedStateVector->unlock();

		mNodes->unlock();
		mElements->unlock();
		mD->unlock();
	}


	//void FemSoftBody::computeStrain(void)
	//{


	//}

	float FemSoftBody::computePlasticity(void* user_pointer )
	{
		tick_count starTicks = tick_count::now();


		const SimdMatrix3* strainsbuf = mStrainMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* x0buf = mX0->lockForRead<SimdVector3>();
		const SimdMatrix3* rotationbufconst = mElemRotations->lockForRead<SimdMatrix3>();
		const unsigned short* tetrabuf = mElements->lockForRead<unsigned short>();
		const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();

		SimdVector3* totalStrainbuf = mTotalStrain->lockForWrite<SimdVector3>();
		

		SimdVector3 temp[4];
		SimdVector3 ltemp(0.1f);

		int N = mNbElements;
		while ( N-- > 0)
		{
			//invRot = transpose(*rotationbufconst);

			//const Vector3& n1 = nodebuf[tetrabuf->n1];
			//const Vector3& n2 = nodebuf[tetrabuf->n2];
			//const Vector3& n3 = nodebuf[tetrabuf->n3];
			//const Vector3& n4 = nodebuf[tetrabuf->n4];
			
			//const SimdVector3* x0 = x0buf->v;
			//const unsigned short* n = &(tetrabuf->n1);

			temp[0] = ( (*rotationbufconst) % nodebuf[*tetrabuf++] ) - (*x0buf++);
			*totalStrainbuf = (*strainsbuf++) * temp[0];
			temp[1] = ( (*rotationbufconst) % nodebuf[*tetrabuf++] ) - (*x0buf++);
			*totalStrainbuf += (*strainsbuf++) * temp[1];
			temp[2] = ( (*rotationbufconst) % nodebuf[*tetrabuf++] ) - (*x0buf++);
			*totalStrainbuf += (*strainsbuf++) * temp[2];
			temp[3] = ( (*rotationbufconst++) % nodebuf[*tetrabuf++] ) - (*x0buf++);
			(*totalStrainbuf++) += (*strainsbuf++) * temp[3];


			*totalStrainbuf =	(*strainsbuf++) * temp[0];
			*totalStrainbuf += (*strainsbuf++) * temp[1];
			*totalStrainbuf += (*strainsbuf++) * temp[2];
			(*totalStrainbuf++) += (*strainsbuf++) * temp[3];

			//*totalStrainbuf =	(*strainsbuf++) * temp[0];
			//*totalStrainbuf += (*strainsbuf++) * temp[1];
			//*totalStrainbuf += (*strainsbuf++) * temp[2];
			//(*totalStrainbuf++) += (*strainsbuf++) * temp[3];

			//*totalStrainbuf =	(*strainsbuf++) * temp[0];
			//*totalStrainbuf += (*strainsbuf++) * temp[1];
			//*totalStrainbuf += (*strainsbuf++) * temp[2];
			//(*totalStrainbuf++) += (*strainsbuf++) * temp[3];


			//totalStrainbuf->v[0] =	strainsbuf->m[0][0] * temp[0]
			//	+ strainsbuf->m[0][1] * temp[1]
			//	+ strainsbuf->m[0][2] * temp[2]
			//	+ strainsbuf->m[0][3] * temp[3];

			//	totalStrainbuf->v[1] =	strainsbuf->m[1][0] * temp[0]
			//	+ strainsbuf->m[1][1] * temp[1]
			//	+ strainsbuf->m[1][2] * temp[2]
			//	+ strainsbuf->m[1][3] * temp[3];

			//++totalStrainbuf;
			//++strainsbuf;
			//++rotationbufconst;
			//++x0buf;	
			//++tetrabuf;
		}
		mElemRotations->unlock();
		mNodes->unlock();
		mElements->unlock();
		mTotalStrain->unlock();
		mX0->unlock();
		mStrainMatrix->unlock();


		const Strain* totalStrainbufconst = mTotalStrain->lockForRead<Strain>();
		Strain* elasticStrainbuf = mElasticStrain->lockForWrite<Strain>();
		Strain* plasticStrainbuf = mPlasticStrain->lockForWrite<Strain>();

		N = mNbElements;
		while ( N-- > 0)
		{
			elasticStrainbuf->v[0] = totalStrainbufconst->v[0] - plasticStrainbuf->v[0];
			elasticStrainbuf->v[1] = totalStrainbufconst->v[1] - plasticStrainbuf->v[1];

			const float& elasticStrainNorm = elasticStrainbuf->computeNorm();
			const float& plasticStrainNorm = plasticStrainbuf->computeNorm();

			const SimdVector3& yieldStrain1 = mCreepDt * elasticStrainbuf->v[0];
			const SimdVector3& yieldStrain2 = mCreepDt * elasticStrainbuf->v[1];

			if ( elasticStrainNorm > mYield )
			{
				//const float& temp = mCreepDt*(elasticStrainNorm - mYield)/elasticStrainNorm;
				plasticStrainbuf->v[0] += yieldStrain1;
				plasticStrainbuf->v[1] += yieldStrain2;
			}

			const float& maxYieldStrain = mMaxYield / plasticStrainNorm;

			if ( plasticStrainNorm > mMaxYield )
			{
				plasticStrainbuf->v[0] *=  maxYieldStrain;
				plasticStrainbuf->v[1] *=  maxYieldStrain;
			}

			++totalStrainbufconst;
			++elasticStrainbuf;
			++plasticStrainbuf;

		}
		mElasticStrain->unlock();
		mPlasticStrain->unlock();


		const SimdMatrix3* plastmatbuf = mPe->lockForRead<SimdMatrix3>();
		const Strain* plasticStrainbufconst = mPlasticStrain->lockForRead();
		tetrabuf = mElements->lockForRead<unsigned short>();
		rotationbufconst = mElemRotations->lockForRead<SimdMatrix3>();

		SimdVector3* plasticforcesbuf = mPlasticForce->lockForWrite<SimdVector3>();
		mPlasticForce->reset();

		N = mNbElements;
		while ( N-- > 0)
		{

			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[0] );
			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[1] );
			++tetrabuf;

			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[0] );
			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[1] );
			++tetrabuf;
			
			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[0] );
			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[1] );
			++tetrabuf;

			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[0] );
			plasticforcesbuf[*tetrabuf] += (*rotationbufconst) * ( (*plastmatbuf++) * plasticStrainbufconst->v[1] );
			++tetrabuf;


			//plasticforcesbuf[*tetrabuf++] += 
			//	(*rotationbufconst) * (plastmatbuf->m[0][0]*plasticStrainbufconst->v[0] + plastmatbuf->m[0][1]*plasticStrainbufconst->v[1] );

			//plasticforcesbuf[*tetrabuf++] += 
			//	(*rotationbufconst) * (plastmatbuf->m[1][0]*plasticStrainbufconst->v[0] + plastmatbuf->m[1][1]*plasticStrainbufconst->v[1] );

			//plasticforcesbuf[*tetrabuf++] += 
			//	(*rotationbufconst) * (plastmatbuf->m[2][0]*plasticStrainbufconst->v[0] + plastmatbuf->m[2][1]*plasticStrainbufconst->v[1] );

			//plasticforcesbuf[*tetrabuf++] += 
			//	(*rotationbufconst) * (plastmatbuf->m[3][0]*plasticStrainbufconst->v[0] + plastmatbuf->m[3][1]*plasticStrainbufconst->v[1] );

			++rotationbufconst;
			//++plastmatbuf;
			++plasticStrainbufconst;
			//++tetrabuf;
		}
		mPlasticStrain->unlock();
		mPlasticForce->unlock();
		mPe->unlock();
		mElements->unlock();
		mElemRotations->unlock();

		return  (float)( tick_count::now() - starTicks ).seconds();
	}

	float FemSoftBody::computeRhs(void)
	{

		tick_count starTicks = tick_count::now();


		//SimdMatrix3* NodeRow;

		//int Nb_Elms;

		// RHS = Mv0 - dt*(K*Xi + F0 - Fext + (to do)lambda*Fvinc )

		// Reset RHS Vector
		//memset(m_RHS_Vector, 0.0f, m_Nb_Nodes*sizeof(SimdVector3) );
		// RHS = K*Xi
		mRHSVector->reset();

		const ushort* rowsbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const SimdMatrix3* matbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* nodebuf = mNodes->lockForRead<SimdVector3>();
		
		
		SimdVector3* rhsbuf = mRHSVector->lockForWrite<SimdVector3>();

		ushort nbcols;
		int nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			nbcols = *(rowsbuf+1) - *rowsbuf;

			++rowsbuf;
			// RHS = K*Xi 
			while (nbcols-- > 0)
			{
				*rhsbuf -= (*matbuf++) * nodebuf[(*colsbuf++)];
			}
			++rhsbuf;
		}
		mRHSVector->unlock();
		mNodes->unlock();
		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

		const SimdVector3* forcebuf = mForces->lockForRead<SimdVector3>();
		const SimdVector3* extforcebuf = mExternForces->lockForRead<SimdVector3>();
		const float* nodemassbuf = mNodeMasses->lockForRead<float>();
		const SimdVector3* nodevelbuf = mNodesVelocity->lockForRead<SimdVector3>();
		
		const SimdVector3* plasticforcebuf = 0;
		if ( mIsPlastic )
		{
			plasticforcebuf = mPlasticForce->lockForRead<SimdVector3>();
		}
		
		const SimdVector3& gravity = reinterpret_cast<const SimdVector3&>(m_Gravity);

		const SimdVector3& timeStep = TimeStepVector;

		rhsbuf = mRHSVector->lockForWrite<SimdVector3>();

		nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			//nbcols = *(rowsbuf+1) - *rowsbuf;

			//// RHS = K*Xi 
			//while (nbcols-- > 0)
			//{
			//	*rhsbuf -= (*matbuf++) * nodebuf[(*colsbuf++)];
			//}

			// RHS = -K*Xi -F0  
			*rhsbuf -= *forcebuf++;

			if ( mIsPlastic )
			{
				*rhsbuf += *plasticforcebuf++;
			}
			
			// RHS = -K*Xi -F0  - Fext
			*rhsbuf += *extforcebuf++;

			// add Gravity;
			// RHS = -K*Xi -F0  - Fext - mg
			*rhsbuf += gravity * (*nodemassbuf); //SimdVector3( 0.0f, m_Gravity*(*nodemassbuf), 0.0f );

			// RHS = dt*( -K*Xi -F0 )
			*rhsbuf = SSE::mulPerElem( (*rhsbuf), timeStep);

			*rhsbuf += SSE::mulPerElem( *nodevelbuf++, SSE::Vector3(*nodemassbuf++) );


			//++nodevelbuf;
			//++nodemassbuf;
			//++extforcebuf;
			//++forcebuf;
			++rhsbuf;
			//++rowsbuf;	
		}

		mRHSVector->unlock();
		if ( mIsPlastic )
		{
			mPlasticForce->unlock();
		}		
		mNodesVelocity->unlock();
		mNodeMasses->unlock();
		mExternForces->unlock();
		mForces->unlock();
		//mNodes->unlock();
		//mStiffnessMatrix->unlock();
		//mCloumns->unlock();
		//mRows->unlock();

		return  (float)( tick_count::now() - starTicks ).seconds();

	}


	float FemSoftBody::computeLhs(void)
	{

		tick_count starTicks = tick_count::now();

		// LHS = M + dt*C + dt*dt*K

		int nbblocks = mStiffnessMatrix->getSize();
		SimdMatrix3* matbuf = mStiffnessMatrix->lockForWrite<SimdMatrix3>();
		//  LHS = dt*dt*K		
		while ( nbblocks-- > 0 )
		{
			*matbuf++ *= m_TimeStep2;
		}
		mStiffnessMatrix->unlock();


		const ushort* diagsbuf = mDiagonals->lockForRead<ushort>();
		const float* nodemassbuf = mNodeMasses->lockForRead<float>();
		matbuf = mStiffnessMatrix->lockForWrite<SimdMatrix3>();
		
		//ushort nbcols;
		int nbNodes = mNbNodes;
		while ( nbNodes-- > 0 )
		{
			matbuf[*diagsbuf] += SimdMatrix3(
				SimdVector3( *nodemassbuf, 0.0f, 0.0f ),
				SimdVector3( 0.0f, *nodemassbuf, 0.0f ),
				SimdVector3( 0.0f, 0.0f, *nodemassbuf )
				);
			matbuf[*diagsbuf] += SSE::mulPerElem(m_DampingMatrix,TimeStepMatrix);

			++diagsbuf;
			++nodemassbuf;
		}

		mStiffnessMatrix->unlock();
		mNodeMasses->unlock();
		mDiagonals->unlock();

		return  ( tick_count::now() - starTicks ).seconds();
	}


	void FemSoftBody::initializeStiffnessMap(void)
	{

		ushort NodeIndex_i,NodeIndex_j;

		const TetrahedronElement* elembuf = 
			mElements->lockForRead<TetrahedronElement>();

		const ushort* rowbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();

		Ke* mapbuf = mKe2StiffnessMatrixMap->lockForWrite<Ke>();

		for  (int ElementIndex=0; ElementIndex<mNbElements; ++ElementIndex )
		{

			for (int i=0; i<4; ++i )
			{
				NodeIndex_i = elembuf->n[i];

				for (int j=0; j<4; ++j )
				{

					NodeIndex_j = elembuf->n[j];

					ushort startId = rowbuf[NodeIndex_i];
					ushort endId = rowbuf[NodeIndex_i+1];
					ushort id=startId;

					while ( colsbuf[id] != NodeIndex_j )
					{
						assert( id != endId-1 );
						++id;						
					}
					mapbuf->ij[i][j] = id;
				
				}
			}

			++mapbuf;
			++elembuf;
		}

		mKe2StiffnessMatrixMap->unlock();
		mCloumns->unlock();
		mRows->unlock();
		mElements->unlock();
	}


	static inline void MatVecProd( const SimdMatrix3* mat, const SimdVector3* vec, 
		int NbRows,	const ushort* rows, const ushort* cols, 
		SimdVector3* res )
	{
		ushort nbcols;

		while ( NbRows-- > 0 )
		{
			nbcols = *(rows+1) - *rows;
			memset( res, 0, 16 );

			while (nbcols-- > 0)
			{
				*res += (*mat++) * vec[(*cols++)];
			}
			++res;
			++rows;			
		}
	}


	static inline float InnProd( const SimdVector3* vec1, const SimdVector3* vec2,
		int NbElems )
	{
		SimdVector3 dot_acc = SimdVector3::Zero();
		while ( NbElems-- > 0)
		{
			dot_acc += SSE::mulPerElem( (*vec1++), (*vec2++) );
		}
		return dot_acc[0] + dot_acc[1] + dot_acc[2];
	}


	void FemSoftBody::ConjGradSolver(void)
	{

		static const float invTimeStep = 1.0f / m_TimeStep;

		computeRhs();
		computeLhs();


		//SimdMatrix3* NodeRow;
		SimdVector3 TempVector;

		//int NbElms;

		// initial guess: x0
		mNewNodeVelocity->copyData( *mNodesVelocity );

		//project to constraint space
		//int constrIndex;
		//int IdxNode;

		if ( mNbConstraintNodes > 0 )
		{
			//const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
			Vector3* newvelbuf = mNewNodeVelocity->lockForWrite<Vector3>();

			//mNbConstraintNodes = 0;
			for (int i=0; i< mNbConstraintNodes; ++i)
			{
				//newvelbuf[contrbuf->index] = contrbuf->m_value;
				//++contrbuf;
				memset( &newvelbuf[*contrbuf++], 0, sizeof(Vector3) );
			}
			mNewNodeVelocity->unlock();
			mConstraintNodes->unlock();
		}
		

		// Compute initial residual e p (auxiliary) vector 
		mResidualVector->copyData( *mRHSVector );

		const ushort* rowbuf = mRows->lockForRead<ushort>();
		const ushort* colsbuf = mCloumns->lockForRead<ushort>();
		const SimdMatrix3* stiffmatbuf = mStiffnessMatrix->lockForRead<SimdMatrix3>();
		const SimdVector3* nodevelbuf = mNodesVelocity->lockForRead<SimdVector3>();
		SimdVector3* residualbuf = mResidualVector->lockForWrite<SimdVector3>();

		ushort nbblock;
		for (int NodeIndex=0; NodeIndex<mNbNodes; ++NodeIndex)
		{
			nbblock = *(rowbuf+1) - *rowbuf;

			while ( nbblock-- > 0)
			{
				*residualbuf -= (*stiffmatbuf++) * nodevelbuf[(*colsbuf++)];
			}
			++residualbuf;
			++rowbuf;
		}
		mResidualVector->unlock();
		mNodesVelocity->unlock();
		mStiffnessMatrix->unlock();
		mCloumns->unlock();
		mRows->unlock();

		if ( mNbConstraintNodes > 0 )
		{
			const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
			Vector3* residualbuf = mResidualVector->lockForWrite<Vector3>();
			for (int i=0; i< mNbConstraintNodes; ++i)
			{
				memset( &residualbuf[*contrbuf++], 0, sizeof(Vector3) );
			}
			mResidualVector->unlock();
			mConstraintNodes->unlock();
		}


		// initial auxiliary vector
		mDirectionVector->copyData( *mResidualVector );

		double rho = InnProd(
			mResidualVector->lockForRead<SimdVector3>(), 
			mResidualVector->lockForRead<SimdVector3>(), 
			mNbNodes);
		mResidualVector->unlock();


		double invNorm_b = InnProd(
			mRHSVector->lockForRead<SimdVector3>(), 
			mRHSVector->lockForRead<SimdVector3>(), 
			mNbNodes);
		mRHSVector->unlock();

		// 1.0/ ||b||
		invNorm_b = 1.0/invNorm_b;


		// ______________________________________________________________

		// -- solve the system using a conjugate gradient solution
		double  alpha, beta;
		double denominator = 0.0;
		double new_rho = 0.0;
		SimdVector3 denominator_acc;

		//PrintStiffnessMatrix( "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\A.txt");
		//printVector(mRHSVector->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\b.txt");
		//printVector(mNodesVelocity->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\v.txt");
		//printVector(mDirectionVector->lockForRead<Vector3>(), "C:\\Documents and Settings\\federico\\Documenti\\MATLAB\\dir.txt");

		for (int iter=0; iter < m_Nb_Solver_Iteration; ++iter)
		{

			MatVecProd(
				mStiffnessMatrix->lockForRead<SimdMatrix3>(),
				mDirectionVector->lockForRead<SimdVector3>(),
				mNbNodes,
				mRows->lockForRead<ushort>(),
				mCloumns->lockForRead<ushort>(),
				mTemp->lockForWrite<SimdVector3>()
				);

			mTemp->unlock();
			mCloumns->unlock();
			mRows->unlock();
			mDirectionVector->unlock();
			mStiffnessMatrix->unlock();


			//if ( mNbConstraintNodes > 0 )
			//{
			//	const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			//	Vector3* tempbuf = mTemp->lockForWrite<Vector3>();

			//	for (int i=0; i< mNbConstraintNodes; ++i)
			//	{
			//		tempbuf[contrbuf->index] = contrbuf->m_value;
			//		++contrbuf;
			//	}
			//	mTemp->unlock();
			//	mConstraintNodes->unlock();
			//}
			if ( mNbConstraintNodes > 0 )
			{
				const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
				Vector3* tempbuf = mTemp->lockForWrite<Vector3>();
				for (int i=0; i< mNbConstraintNodes; ++i)
				{
					memset( &tempbuf[*contrbuf++], 0, sizeof(Vector3) );
				}
				mTemp->unlock();
				mConstraintNodes->unlock();
			}

			denominator = InnProd(
				mDirectionVector->lockForRead<SimdVector3>(),
				mTemp->lockForWrite<SimdVector3>(),  
				mNbNodes 
				);
			mTemp->unlock();
			mDirectionVector->unlock();

			// 
			if (fabs(denominator)  < m_Solver_Tolerance ) // 1e-5f
			{
				//continue;
				break;
			}

			// new alpha
			alpha = rho / denominator;


			// Update velocity 
			const SimdVector3* cdirbuf = mDirectionVector->lockForRead<SimdVector3>();
			SimdVector3* newvelbuf = mNewNodeVelocity->lockForWrite<SimdVector3>();
			int NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*newvelbuf++ +=  (float)alpha * (*cdirbuf++);
			}
			mNewNodeVelocity->unlock();
			mDirectionVector->unlock();


			// Update residual vector
			const SimdVector3* ctempbuf = mTemp->lockForRead<SimdVector3>();
			SimdVector3* residualbuf = mResidualVector->lockForWrite<SimdVector3>();

			NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*residualbuf++ -=  (float)alpha * (*ctempbuf++);
			}
			mResidualVector->unlock();
			mTemp->unlock();

			
			new_rho = InnProd( 
				mResidualVector->lockForRead<SimdVector3>(),
				mResidualVector->lockForRead<SimdVector3>(),
				mNbNodes
				);


			// new beta
			beta = new_rho / rho;

			// update rho
			rho = new_rho;

			double err = new_rho * invNorm_b;
			// check 
			if ( err  < 1e-7 /*m_Solver_Tolerance*/ ) // 1e-5
			{
				//continue;
				// exit 
				break;
			}

			// update p vector (auxiliary)
			const SimdVector3* cresidualbuf = mResidualVector->lockForRead<SimdVector3>();
			SimdVector3* dirbuf = mDirectionVector->lockForWrite<SimdVector3>();

			NodeIndex = mNbNodes;
			while (NodeIndex-- > 0)
			{
				*dirbuf = (float)beta * (*dirbuf) + (*cresidualbuf++);
				++dirbuf;
			}
			mDirectionVector->unlock();
			mResidualVector->unlock();

			//if ( mNbConstraintNodes > 0 )
			//{
			//	const ConstraintNode* contrbuf = mConstraintNodes->lockForRead<ConstraintNode>();
			//	dirbuf = mDirectionVector->lockForWrite<SimdVector3>();

			//	for (int i=0; i< mNbConstraintNodes; ++i)
			//	{
			//		dirbuf[contrbuf->index] = contrbuf->m_value;
			//		++contrbuf;
			//	}
			//	mDirectionVector->unlock();
			//	mConstraintNodes->unlock();
			//}
			if ( mNbConstraintNodes > 0 )
			{
				const ushort* contrbuf = mConstraintNodes->lockForRead<ushort>();
				Vector3* dirbuf = mDirectionVector->lockForWrite<Vector3>();
				for (int i=0; i< mNbConstraintNodes; ++i)
				{
					memset( &dirbuf[*contrbuf++], 0, sizeof(Vector3) );
				}
				mDirectionVector->unlock();
				mConstraintNodes->unlock();
			}
			
		}

		mNodesVelocity->copyData( *mNewNodeVelocity );

		const SimdVector3* cnodevelbuf = mNewNodeVelocity->lockForRead<SimdVector3>();
		SimdVector3* nodesbuf = mNodes->lockForWrite<SimdVector3>();

		int nb_nodes = mNbNodes;
		while ( nb_nodes-- > 0 )
		{
			(*nodesbuf++) +=  SSE::mulPerElem( *cnodevelbuf++, TimeStepVector);
		}
		mNodes->unlock();
		mNewNodeVelocity->unlock();
	}

}; // namespace SimStep