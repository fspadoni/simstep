#ifndef SimStep_FemSoftBody_h__
#define SimStep_FemSoftBody_h__


#include <Core/SoftBody.h>

#include <Math/Math.h>

#include "FemElasticityMatrix.h"
#include "FemStrainMatrix.h"
#include "FemElementStiffnessMatrix.h"
#include "Tetrahedron.h"
#include "FemPlasticityMatrix.h"

#include <SoftBodyDefinition.h>

#include <Utilities/SimdMemoryBuffer.h>
#include <Core/TetraMesh.h>

namespace SimStep
{


	class _SoftBodyExport FemSoftBody : public SoftBody
	{

	public:


		FemSoftBody( const SoftBodyParams& params, const DataValuePairList* otherparams = 0 );


		virtual ~FemSoftBody();


		// Public Interface 
		virtual const String& getType(void) const;

		virtual float assembleStiffnessMatrix(void* user_pointer = 0);

		virtual float solve( const float& TimeStep, void* user_pointer = 0 );

		virtual Vector3 getStressForce( const int& nodeId ) const;

		virtual const Buffer<Vector3>& getStressForces( void ) const;

		virtual float getVertexMass(int vertex) const;

		virtual SoftBody* clone(void);

		virtual void updateCollisionShape(void);

		virtual void setPosition(const Vector3& newpos );

		virtual void setOrientation(const Quat& newor );

		virtual void addForce(const int& nodeId, const Vector3& force );


		virtual Vector3 getVertexNormal(int vertexId) const;

		virtual const BufferSharedPtr<Vector3>& getVertexNormalBuffer(void) const;
	
		virtual void setVelocity(const Vector3& newvel );

		virtual void reset(void);

		// _________________


		inline TetrahedronElement getElement(int Id) const;

		inline const Buffer<TetrahedronElement>& getElements(void) const 
			{return *mElements;}

		//inline TriangleElement getTriangle(int Id) const;

		//inline const Buffer<TriangleElement>& getTriangles(void) const
		//	{return *mTriangles;}


	private:
		//void PConjugateGradient();
		//void PConjugateGradient2();


	protected:
		
		BufferSharedPtr<TetrahedronElement> mElements;

		BufferSharedPtr<Matrix3> mElemRotations;

		BufferSharedPtr<Vector3> mNewVertexNormals;

		//  tetra mesh surface triangle
		BufferSharedPtr<ushort> mTriangles;


		BufferSharedPtr<BarycentricCoords> mBarycentricCoords;
		
		BufferSharedPtr<ushort> mBarycentricIndex;


		// ElasticityMatrix
		BufferSharedPtr<ElasticityMatrix> mD;
		
		// StrainMatrix
		BufferSharedPtr<StrainMatrix> mStrainMatrix;

		// The initials positions in its frame
		BufferSharedPtr<Vector12> mKX0; //Vector12
		
		BufferSharedPtr<Matrix4> mUndeformedStateVector; // SimdMatrix4

		//ElementStiffnessMatrix
		BufferSharedPtr<ElementStiffnessMatrix> mKe;

		//StiffnessMatrix
		BufferSharedPtr<Matrix3> mStiffnessMatrix;


		// ElementPlasticityMatrix
		BufferSharedPtr<PlasticityMatrix> mPe; // precompute Pe = VeBTe
		BufferSharedPtr<Vector12> mX0;
		BufferSharedPtr<Strain> mTotalStrain;
		BufferSharedPtr<Strain> mElasticStrain;
		BufferSharedPtr<Strain> mPlasticStrain;
		BufferSharedPtr<Vector3> mPlasticForce;



		void computeStressForces(void);
		void updateVertices(void);
		void updateVertexVels(void);
		void updateVertexNormals(void);
		//void InitializeCollision(void);

		// solver 
	private:
		
		// NewNodeVelocity
		BufferSharedPtr<Vector3> mNewNodeVelocity; // Vector3

		// RHS_Vector
		BufferSharedPtr<Vector3> mRHSVector; // Vector3

		// ResidualVector
		BufferSharedPtr<Vector3> mResidualVector; // Vector3

		// DirectionVector
		BufferSharedPtr<Vector3> mDirectionVector; // Vector3

		BufferSharedPtr<Vector3> mTemp; // Vector3

		// connection graph
		BufferSharedPtr<ushort> mCloumns; //std::vector<std::vector<ushort>>
		BufferSharedPtr<ushort> mRows; // ushort
		BufferSharedPtr<ushort> mDiagonals; //ushort 

		// ______________________
		struct Ke
		{
			ushort ij[4][4];
		};

		BufferSharedPtr<Ke> mKe2StiffnessMatrixMap;


	protected:


		bool m_GraphLoad;


		// _____________________________________
		// Desc
		Vector3 mPosition;
		Matrix3 mRotation;
		float m_TimeStep;
		float m_TimeStep2;
		float m_Density;
		float m_YoungModulus;
		float m_PoissonRatio;
		float m_Damping;
		//SimdVector3 m_GravitySimd;
		int	  m_Nb_Solver_Iteration;
		float m_Solver_Tolerance;

		// plasticity
		float mYield;
		float mMaxYield;
		float mCreepDt;
		/*const*/ bool mIsPlastic;

		// _____________________________________

		// dt Matrix
		SimdVector3 TimeStepVector;
		SimdMatrix3 TimeStepMatrix;
		SimdMatrix3 TimeStep2Matrix;

		// C Matrix
		SimdMatrix3 m_DampingMatrix;


		void create(const SoftBodyParams& param);
		
		virtual void initializeBuffers(void);

		void initializeElements(void);
		void initializeStiffnessMap(void);


		//void computeStrain(void);

		float computePlasticity(void* user_pointer );

		float computeRhs(void);
		float computeLhs(void);

		

		//// da qui in poi 
		//// spostare in una classe utilityIO 
		//Bool loadMesh(const char *filename);

		//Bool loadNodes(const char *filename);
		//Bool loadElements(const char *filename);
		//Bool loadCollisionNodes(const char *filename);
		//Bool loadTriangles(const char *filename);
		//Bool loadGraph(const char *filename);
		//Bool loadConstraints(const char *filename);

		//Bool printNodes(const char *filename);
		//Bool printMeshElements(const char *filename);
		//Bool printElements(const char *filename);
		//Bool printTriangles(const char *filename);
		//Bool printGraph(const char *filename, unsigned short int n );
		//Bool printConstraints(const char *filename);

		//Bool reOrderNodes(const char *filename, unsigned short int n);
		//Bool reOrderNodes(const char *filename);

		void PrintStiffnessMatrix( const char* file_name );
		//void printMatrix( SimdMatrix3**  mat, const char* file_name );
		//void printVector( const Vector3* vec,	const char* file_name );


		//void conjugateGradientFemSolver(void);
		void ConjGradSolver(void);
	};


	inline TetrahedronElement FemSoftBody::getElement(int Id) const
	{ 
		const TetrahedronElement* elembuf = mElements->lockForRead(Id);
		mElements->unlock();
		return *elembuf;
	}

	//inline TriangleElement FemSoftBody::getTriangle(int Id) const
	//{
	//	const TriangleElement* trianglebuf = mTriangles->lockForRead(Id);
	//	mTriangles->unlock();
	//	return *trianglebuf;
	//}


}; // namespace SimStep

#endif // SimStep_FemSoftBody_h__