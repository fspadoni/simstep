#ifndef SimStep_FemSoftBodyFactory_h__
#define SimStep_FemSoftBodyFactory_h__

#include "Core/SoftBodyFactory.h"

#include "Utilities/String.h"

#include "SoftBodyDefinition.h"

namespace SimStep
{


	class _SoftBodyExport FemSoftBodyFactory : public SoftBodyFactory
	{
	public:

		static String FACTORY_TYPE_NAME;


	protected:

		/// Internal implementation of create method - must be overridden
		virtual SoftBody* createInstanceImpl(
			const SoftBodyParams& params, 
			const DataValuePairList* otherparams = 0);


	public:

		FemSoftBodyFactory(void) : SoftBodyFactory() {}
		virtual ~FemSoftBodyFactory(void) {}


		/// Get the type of the object to be created
		virtual const String& getType(void) const;



		/** Destroy an instance of the object */
		void destroyInstance(SoftBody* softbody);

	};

	
}; // namespace SimStep

#endif // SimStep_FemSoftBodyFactory_h__