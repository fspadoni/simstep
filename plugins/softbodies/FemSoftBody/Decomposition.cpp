#include "Decomposition.h"




namespace SimStep
{

	inline float norm_one(const Matrix3& M) 
	{
		//Real sum, max = 0;
		//for (int i=0;i<3;i++)
		//{
		//	sum = (Real)(fabs(M[0][i])+fabs(M[1][i])+fabs(M[2][i]));
		//	if (max<sum) max = sum;
		//}
		//return max;

		__declspec(align(16)) float result[4];

		const SimdMatrix3& SimdM = reinterpret_cast<const SimdMatrix3&>(M);
		
		_mm_store_ps( result, _mm_add_ps( fabsf4( SimdM.getRow(2).get128() ),
			_mm_add_ps( fabsf4( SimdM.getRow(0).get128()),
			fabsf4( SimdM.getRow(1).get128() ) ) ) );

		return  Max(result[2],  Max(result[0],result[1]) );
	}

	inline float norm_inf(const Matrix3& M) 
	{
		__declspec(align(16)) float result[4];

		const SimdMatrix3& SimdM = reinterpret_cast<const SimdMatrix3&>(M);

		_mm_store_ps( result, _mm_add_ps( fabsf4( SimdM.getCol2().get128() ),
			_mm_add_ps( fabsf4( SimdM.getCol0().get128()),
			fabsf4( SimdM.getCol1().get128() ) ) ) );

		return Max(result[2], Max(result[0],result[1]) );
	}

	/** Set MadjT to transpose of inverse of M times determinant of M **/
	inline Matrix3 adjoint_transpose(const Matrix3& M)
	{

		return Matrix3( 
			cross(M.getCol1(), M.getCol2() ),
			cross(M.getCol2(), M.getCol0() ),
			cross(M.getCol0(), M.getCol1() )
			);
	}


	const Matrix3 do_rank2(const Matrix3& M, 
		const Matrix3& M_adj_tras )
	{
		Matrix3 ret;

		return ret;
	}


	float PolarDecompose(const Matrix3& M,
		Matrix3* Rotation,
		Matrix3* Stretch )
	{

		Matrix3 M_tras, M_adj_tras, Ek;
		float  det, M_one, M_inf, MadjT_one, MadjT_inf, E_one, gamma, g1, g2;

		M_tras = transpose( M );
		M_one = norm_one(M_tras); 
		M_inf = norm_inf(M_tras);

		do 
		{
			M_adj_tras = adjoint_transpose(M_tras);
			det = dot( M_tras.getRow(0), M_adj_tras.getRow(0) );
			//if ( det == 0.0f )
			//{
			//	M_tras = do_rank2(M_tras, M_adj_tras);
			//	break;
			//}

			MadjT_one = norm_one(M_adj_tras); 
			MadjT_inf = norm_inf(M_adj_tras);

			gamma = sqrtf( sqrtf( (MadjT_one*MadjT_inf)/(M_one*M_inf) ) /fabs(det) );

			g1 = gamma*0.5f;
			g2 = 0.5f/(gamma*det);
			Ek = M_tras;
			M_tras = M_tras*g1 + M_adj_tras*g2;
			Ek -= M_tras;
			E_one = norm_one(Ek);
			M_one = norm_one(M_tras);
			M_inf = norm_inf(M_tras);

		} while ( E_one> (M_one*1.0e-6f) );

		*Rotation = transpose( M_tras );
		*Stretch = M_tras * M;
		return det;
	}


	Matrix3 PolarDecomposeRotationPart(
		const Matrix3& M )
	{
		Matrix3 M_tras, M_adj_tras, Ek;
		float  det, M_one, M_inf, MadjT_one, MadjT_inf, E_one, gamma, g1, g2;

		M_tras = transpose( M );
		M_one = norm_one(M_tras); 
		M_inf = norm_inf(M_tras);

		do 
		{
			M_adj_tras = adjoint_transpose(M_tras);
			det = dot( M_tras.getRow(0), M_adj_tras.getRow(0) );

			// in quale caso si verifica questa condizione?
			//if ( det == 0.0f )
			//{
			//	M_tras = do_rank2(M_tras, M_adj_tras);
			//	break;
			//}

			MadjT_one = norm_one(M_adj_tras); 
			MadjT_inf = norm_inf(M_adj_tras);

			gamma = sqrtf( sqrtf( (MadjT_one*MadjT_inf)/(M_one*M_inf) ) /fabs(det) );

			g1 = gamma*0.5f;
			g2 = 0.5f/(gamma*det);
			Ek = M_tras;
			M_tras = M_tras*g1 + M_adj_tras*g2;
			Ek -= M_tras;
			E_one = norm_one(Ek);
			M_one = norm_one(M_tras);
			M_inf = norm_inf(M_tras);

		} while ( E_one> (M_one*1.0e-6f) );

		return transpose( M_tras );
	}


	Matrix3 PolarDecomposeRotationPartOptimize(	const Matrix3& M )
	{
		Matrix3 M_tras, M_adj_tras, Ek;
		float  det, M_one, M_inf, E_one, gamma, g1, g2;

		M_tras = transpose( M );
		M_one = norm_one(M_tras); 
		M_inf = norm_inf(M_tras);

		do 
		{
			M_adj_tras = adjoint_transpose(M_tras);
			det = dot( M_tras.getRow(0), M_adj_tras.getRow(0) );

			// in quale caso si verifica questa condizione?
			//if ( det == 0.0f )
			//{
			//	M_tras = do_rank2(M_tras, M_adj_tras);
			//	break;
			//}

			//MadjT_one = norm_one(M_adj_tras); 
			//MadjT_inf = norm_inf(M_adj_tras);

			gamma = sqrtf( sqrtf( (norm_one(M_adj_tras)*norm_inf(M_adj_tras))/
				(M_one*M_inf)  ) /fabs(det) );

			g1 = gamma*0.5f;
			g2 = 0.5f/(gamma*det);
			Ek = M_tras;
			M_tras = M_tras*g1 + M_adj_tras*g2;
			Ek -= M_tras;
			E_one = norm_one(Ek);
			M_one = norm_one(M_tras);
			M_inf = norm_inf(M_tras);

		} while ( E_one> (M_one*1.0e-6f) );

		return transpose( M_tras );
	}
	
}; // namespace SimStep